﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// AIStateMachine
struct AIStateMachine_t2425577084;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// BossTankController
struct BossTankController_t1902331492;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// BackgroundScroller
struct BackgroundScroller_t2508866296;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.String
struct String_t;
// EnemyShoot
struct EnemyShoot_t243830779;
// BossUI
struct BossUI_t1795207708;
// UnityEngine.UI.Image
struct Image_t2670269651;
// Bullet
struct Bullet_t1042140031;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// Bullet/<Fade>d__7
struct U3CFadeU3Ed__7_t626104978;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// UnityEngine.Object
struct Object_t631007953;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// DamageEnemy
struct DamageEnemy_t2198349935;
// HealthUI
struct HealthUI_t1908446248;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// EnemyStats
struct EnemyStats_t4187841152;
// DamagePlayer
struct DamagePlayer_t2033828012;
// PlayerStats
struct PlayerStats_t2044123780;
// EnableCameraDepthInForward
struct EnableCameraDepthInForward_t3351256833;
// UnityEngine.Camera
struct Camera_t4157153871;
// EnemyController
struct EnemyController_t2191660454;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t1276799816;
// Gun
struct Gun_t783153271;
// UnityEngine.BoxCollider
struct BoxCollider_t1640800422;
// EnemyCount
struct EnemyCount_t3730988989;
// LevelClear
struct LevelClear_t1949285084;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// EnemyFuzzyState
struct EnemyFuzzyState_t3026123389;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// FuzzySample
struct FuzzySample_t2008138351;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// GameCamera
struct GameCamera_t2564829307;
// GameOverScreen
struct GameOverScreen_t2285777029;
// LevelChanger
struct LevelChanger_t225386971;
// UnityEngine.Sprite
struct Sprite_t280657092;
// Grid
struct Grid_t1081586032;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// Grid/TerrainType
struct TerrainType_t3627529997;
// Node
struct Node_t2989995042;
// System.Collections.Generic.List`1<Node>
struct List_1_t167102488;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// HealingSpot
struct HealingSpot_t2443304191;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// Interactible
struct Interactible_t1060880155;
// Inventory
struct Inventory_t1050226016;
// Item
struct Item_t2953980098;
// Inventory/OnItemChanged
struct OnItemChanged_t22848112;
// System.Delegate
struct Delegate_t1188392813;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// InventorySlot
struct InventorySlot_t3299309524;
// InventoryUI
struct InventoryUI_t1983315844;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522;
// ItemPickup
struct ItemPickup_t4277059944;
// UnityEngine.Animator
struct Animator_t434523843;
// LevelSelect
struct LevelSelect_t2669120976;
// LevelSelectPanel
struct LevelSelectPanel_t2626311185;
// Path
struct Path_t2615110272;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// Pathfinding
struct Pathfinding_t1696161914;
// System.Action`1<PathResult>
struct Action_1_t1700480823;
// System.Diagnostics.Stopwatch
struct Stopwatch_t305734070;
// Heap`1<Node>
struct Heap_1_t911941676;
// Heap`1<System.Object>
struct Heap_1_t1002052798;
// System.Collections.Generic.HashSet`1<Node>
struct HashSet_1_t1554944516;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t1645055638;
// System.Action`2<UnityEngine.Vector3[],System.Boolean>
struct Action_2_t1484661638;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// PathRequestManager
struct PathRequestManager_t4163282249;
// System.Collections.Generic.Queue`1<PathResult>
struct Queue_1_t1374272722;
// System.Action`2<System.Object,System.Boolean>
struct Action_2_t3782157935;
// PathRequestManager/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t786576806;
// System.Threading.ThreadStart
struct ThreadStart_t1006689297;
// PauseMenu
struct PauseMenu_t3916167947;
// PlayerController
struct PlayerController_t2064355688;
// PlayerHealthUI
struct PlayerHealthUI_t636700963;
// PlayerManager
struct PlayerManager_t1349889689;
// RotatingLights
struct RotatingLights_t2402942693;
// SelectTitleOptions
struct SelectTitleOptions_t2341392585;
// Shield
struct Shield_t1860136854;
// SoftNormalsToVertexColor
struct SoftNormalsToVertexColor_t279782399;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Int32>>
struct List_1_t1600127941;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// Unit
struct Unit_t4139495810;
// Unit/<UpdatePath>d__10
struct U3CUpdatePathU3Ed__10_t1880251700;
// Unit/<FollowPath>d__11
struct U3CFollowPathU3Ed__11_t3675468882;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.List`1<System.Int32>[]
struct List_1U5BU5D_t3580830742;
// System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Int32>[]
struct EntryU5BU5D_t742410749;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t763310475;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>
struct KeyCollection_t2029334555;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>
struct ValueCollection_t3555703402;
// Line[]
struct LineU5BU5D_t3212499767;
// Node[]
struct NodeU5BU5D_t1457036567;
// PathResult[]
struct PathResultU5BU5D_t135407141;
// System.Collections.Generic.HashSet`1/Slot<Node>[]
struct SlotU5BU5D_t4025953330;
// System.Collections.Generic.IEqualityComparer`1<Node>
struct IEqualityComparer_1_t802359764;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t1677636661;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t1059417452;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// Grid/TerrainType[]
struct TerrainTypeU5BU5D_t1748095520;
// Node[0...,0...]
struct NodeU5B0___U2C0___U5D_t1457036568;
// UnityEngine.AudioSourceExtension
struct AudioSourceExtension_t3064908834;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t2439009922;
// LevelSelectPanel[]
struct LevelSelectPanelU5BU5D_t1747102860;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t731888065;
// UnityEngine.UI.InputField/SubmitEvent
struct SubmitEvent_t648412432;
// UnityEngine.UI.InputField/OnChangeEvent
struct OnChangeEvent_t467195904;
// UnityEngine.UI.InputField/OnValidateInput
struct OnValidateInput_t2355412304;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.Event
struct Event_t2956885303;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t1457185986;
// UnityEngine.UI.FontData
struct FontData_t746620069;

extern RuntimeClass* AIStateMachine_t2425577084_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisBossTankController_t1902331492_m3861119340_RuntimeMethod_var;
extern const uint32_t AIStateMachine_Start_m2140641173_MetadataUsageId;
extern const uint32_t AIStateMachine_Update_m3439513021_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var;
extern const uint32_t BackgroundScroller_Start_m2345096096_MetadataUsageId;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern const uint32_t BackgroundScroller_Update_m1770000666_MetadataUsageId;
extern RuntimeClass* AIStates_t2852279327_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern RuntimeClass* EnemyFuzzyState_t3026123389_il2cpp_TypeInfo_var;
extern RuntimeClass* FuzzyStates_t3314355612_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral2278281217;
extern const uint32_t BossTankController_Update_m1715711242_MetadataUsageId;
extern const uint32_t BossTankController_CheckBossStats_m1300902599_MetadataUsageId;
extern String_t* _stringLiteral833246398;
extern const uint32_t BossUI_Update_m1511382654_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var;
extern String_t* _stringLiteral1985039568;
extern const uint32_t Bullet_Start_m1480719556_MetadataUsageId;
extern RuntimeClass* U3CFadeU3Ed__7_t626104978_il2cpp_TypeInfo_var;
extern const uint32_t Bullet_Fade_m3288204635_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody_t3916780224_m279685075_RuntimeMethod_var;
extern String_t* _stringLiteral3128803744;
extern const uint32_t Bullet_OnTriggerEnter_m3806200962_MetadataUsageId;
extern RuntimeClass* WaitForSeconds_t1699091251_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern const uint32_t U3CFadeU3Ed__7_MoveNext_m1628721258_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CFadeU3Ed__7_System_Collections_IEnumerator_Reset_m3612806028_RuntimeMethod_var;
extern const uint32_t U3CFadeU3Ed__7_System_Collections_IEnumerator_Reset_m3612806028_MetadataUsageId;
extern const RuntimeMethod* Object_FindObjectOfType_TisHealthUI_t1908446248_m1692366917_RuntimeMethod_var;
extern const uint32_t DamageEnemy_Start_m4217926186_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisEnemyStats_t4187841152_m2036182613_RuntimeMethod_var;
extern String_t* _stringLiteral760905195;
extern const uint32_t DamageEnemy_OnTriggerEnter_m228673893_MetadataUsageId;
extern const uint32_t DamagePlayer_Start_m900557365_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisPlayerStats_t2044123780_m4080514128_RuntimeMethod_var;
extern String_t* _stringLiteral2261822918;
extern const uint32_t DamagePlayer_OnTriggerEnter_m878711311_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var;
extern const uint32_t EnableCameraDepthInForward_Set_m3860174829_MetadataUsageId;
extern RuntimeClass* PlayerManager_t1349889689_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisNavMeshAgent_t1276799816_m597731532_RuntimeMethod_var;
extern const uint32_t EnemyController_Start_m1137308130_MetadataUsageId;
extern const uint32_t EnemyController_MoveTowardsTarget_m1657189778_MetadataUsageId;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern const uint32_t EnemyController_FaceTarget_m3754874152_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisBoxCollider_t1640800422_m1661675543_RuntimeMethod_var;
extern const uint32_t EnemyController_CreateFOV_m1491746096_MetadataUsageId;
extern const RuntimeMethod* Object_FindObjectOfType_TisLevelClear_t1949285084_m2992299238_RuntimeMethod_var;
extern const uint32_t EnemyCount_Start_m3648910747_MetadataUsageId;
extern String_t* _stringLiteral2662547089;
extern const uint32_t EnemyCount_Update_m2074001267_MetadataUsageId;
extern String_t* _stringLiteral323012186;
extern String_t* _stringLiteral1441634946;
extern String_t* _stringLiteral1978273798;
extern const uint32_t EnemyFuzzyState_SetLabels_m2897062058_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisRigidbody_t3916780224_m564316479_RuntimeMethod_var;
extern const uint32_t EnemyShoot_Shoot_m569725038_MetadataUsageId;
extern const uint32_t EnemyShoot_ShootLaser_m1234821334_MetadataUsageId;
extern const uint32_t EnemyShoot_ShootContinuous_m784199287_MetadataUsageId;
extern const RuntimeMethod* Object_FindObjectOfType_TisEnemyCount_t3730988989_m432317606_RuntimeMethod_var;
extern const uint32_t EnemyStats_Start_m165439304_MetadataUsageId;
extern const uint32_t EnemyStats_Update_m2668008344_MetadataUsageId;
extern RuntimeClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1142555903;
extern String_t* _stringLiteral136024746;
extern String_t* _stringLiteral1212946936;
extern String_t* _stringLiteral915598988;
extern const uint32_t FuzzySample_SetLabels_m3256301443_MetadataUsageId;
extern const uint32_t GameCamera_Start_m2841233292_MetadataUsageId;
extern const uint32_t GameCamera_Update_m3438868780_MetadataUsageId;
extern const RuntimeMethod* Object_FindObjectOfType_TisLevelChanger_t225386971_m2918287313_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var;
extern const uint32_t GameOverScreen_Start_m2140908633_MetadataUsageId;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral767005334;
extern String_t* _stringLiteral2984908384;
extern const uint32_t GameOverScreen_UpdateOptions_m2082488164_MetadataUsageId;
extern String_t* _stringLiteral1187062204;
extern String_t* _stringLiteral1842748674;
extern String_t* _stringLiteral1055811909;
extern String_t* _stringLiteral3963994475;
extern String_t* _stringLiteral1538482293;
extern String_t* _stringLiteral3588955337;
extern const uint32_t GameOverScreen_SelectOptions_m3194635162_MetadataUsageId;
extern const uint32_t GameOverScreen_DeselectAll_m311764793_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_Add_m1535364901_RuntimeMethod_var;
extern const uint32_t Grid_Awake_m2133102238_MetadataUsageId;
extern RuntimeClass* NodeU5B0___U2C0___U5D_t1457036568_il2cpp_TypeInfo_var;
extern RuntimeClass* Node_t2989995042_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_TryGetValue_m1682688660_RuntimeMethod_var;
extern const uint32_t Grid_CreateGrid_m256214910_MetadataUsageId;
extern RuntimeClass* Int32U5B0___U2C0___U5D_t385246373_il2cpp_TypeInfo_var;
extern const uint32_t Grid_BlurPenaltyMap_m2132322175_MetadataUsageId;
extern RuntimeClass* List_1_t167102488_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m3067334415_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m4134525979_RuntimeMethod_var;
extern const uint32_t Grid_GetNeighbours_m4128505182_MetadataUsageId;
extern const uint32_t Grid_NodeFromWorldPoint_m3738070334_MetadataUsageId;
extern const uint32_t Grid_OnDrawGizmos_m569343316_MetadataUsageId;
extern RuntimeClass* Dictionary_2_t1839659084_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2__ctor_m1287366611_RuntimeMethod_var;
extern const uint32_t Grid__ctor_m209885941_MetadataUsageId;
extern const uint32_t Gun_Shoot_m3160136974_MetadataUsageId;
extern const uint32_t Gun_ShootLaser_m1916414356_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisEnemyStats_t4187841152_m1077339419_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisPlayerStats_t2044123780_m1979177729_RuntimeMethod_var;
extern String_t* _stringLiteral3450517380;
extern const uint32_t HealingSpot_OnTriggerEnter_m4057859058_MetadataUsageId;
extern RuntimeClass* Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var;
extern const uint32_t HealingSpot__ctor_m656588571_MetadataUsageId;
extern String_t* _stringLiteral2295225452;
extern const uint32_t Interactible_Interact_m2171514313_MetadataUsageId;
extern const uint32_t Interactible_OnDrawGizmos_m2396099765_MetadataUsageId;
extern RuntimeClass* Inventory_t1050226016_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1456734628;
extern const uint32_t Inventory_Awake_m1952883581_MetadataUsageId;
extern String_t* _stringLiteral2285872367;
extern const uint32_t Inventory_AddItem_m2364679108_MetadataUsageId;
extern String_t* _stringLiteral757602046;
extern const uint32_t InventorySlot_ClearSlot_m1469806239_MetadataUsageId;
extern RuntimeClass* OnItemChanged_t22848112_il2cpp_TypeInfo_var;
extern const RuntimeMethod* InventoryUI_UpdateUI_m4132935956_RuntimeMethod_var;
extern const uint32_t InventoryUI_Start_m1998966654_MetadataUsageId;
extern String_t* _stringLiteral32543115;
extern const uint32_t InventoryUI_UpdateUI_m4132935956_MetadataUsageId;
extern String_t* _stringLiteral3636330245;
extern const uint32_t Item__ctor_m64206515_MetadataUsageId;
extern String_t* _stringLiteral2101004758;
extern const uint32_t ItemPickup_PickUp_m3865268335_MetadataUsageId;
extern const uint32_t ItemPickup_OnTriggerEnter_m2453855028_MetadataUsageId;
extern String_t* _stringLiteral1012806073;
extern const uint32_t LevelChanger_FadeToLevel_m2444280689_MetadataUsageId;
extern const uint32_t LevelChanger_FadeToLevel_m359947979_MetadataUsageId;
extern const uint32_t LevelClear_Start_m1882581272_MetadataUsageId;
extern RuntimeClass* PauseMenu_t3916167947_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3886292556;
extern const uint32_t LevelClear_FadeToLevel_m1500572359_MetadataUsageId;
extern const uint32_t LevelClear_FadeToLevel_m1424934385_MetadataUsageId;
extern const uint32_t LevelSelect_Start_m703985854_MetadataUsageId;
extern String_t* _stringLiteral2219833165;
extern String_t* _stringLiteral1828639942;
extern const uint32_t LevelSelect_UpdateOptions_m4096405336_MetadataUsageId;
extern String_t* _stringLiteral3862077153;
extern String_t* _stringLiteral1699060144;
extern String_t* _stringLiteral2406229878;
extern const uint32_t LevelSelect_SelectOptions_m312907934_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var;
extern const uint32_t LevelSelectPanel_Start_m2274833742_MetadataUsageId;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern const uint32_t Line__ctor_m1943978764_MetadataUsageId;
extern const uint32_t Line_DistanceFromPoint_m4294230093_MetadataUsageId;
extern const uint32_t Line_DrawWithGizmos_m1752959317_MetadataUsageId;
extern RuntimeClass* LineU5BU5D_t3212499767_il2cpp_TypeInfo_var;
extern const uint32_t Path__ctor_m3753908269_MetadataUsageId;
extern const uint32_t Path_DrawWithGizmos_m2056486393_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisGrid_t1081586032_m3342251133_RuntimeMethod_var;
extern const uint32_t Pathfinding_Awake_m1184086700_MetadataUsageId;
extern RuntimeClass* Stopwatch_t305734070_il2cpp_TypeInfo_var;
extern RuntimeClass* Heap_1_t911941676_il2cpp_TypeInfo_var;
extern RuntimeClass* HashSet_1_t1554944516_il2cpp_TypeInfo_var;
extern RuntimeClass* Int64_t3736567304_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Heap_1__ctor_m297432033_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1__ctor_m3938937520_RuntimeMethod_var;
extern const RuntimeMethod* Heap_1_Add_m2689756234_RuntimeMethod_var;
extern const RuntimeMethod* Heap_1_RemoveFirst_m1685389745_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1_Add_m87732039_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m3741205568_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m3563945012_RuntimeMethod_var;
extern const RuntimeMethod* HashSet_1_Contains_m1749837864_RuntimeMethod_var;
extern const RuntimeMethod* Heap_1_Contains_m3137522954_RuntimeMethod_var;
extern const RuntimeMethod* Heap_1_UpdateItem_m3101882114_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1693710810_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m205031767_RuntimeMethod_var;
extern const RuntimeMethod* Heap_1_get_Count_m149300838_RuntimeMethod_var;
extern const RuntimeMethod* Action_1_Invoke_m2478306080_RuntimeMethod_var;
extern String_t* _stringLiteral1946022780;
extern String_t* _stringLiteral3455563715;
extern const uint32_t Pathfinding_FindPath_m1666106048_MetadataUsageId;
extern const uint32_t Pathfinding_RetracePath_m276304888_MetadataUsageId;
extern RuntimeClass* List_1_t899420910_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m1536473967_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m1948408520_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1524640104_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m2928376997_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ToArray_m1691996104_RuntimeMethod_var;
extern const uint32_t Pathfinding_SimplifyPath_m3637710884_MetadataUsageId;
extern const uint32_t Pathfinding_GetDistance_m3683541895_MetadataUsageId;
extern RuntimeClass* Action_2_t1484661638_il2cpp_TypeInfo_var;
extern const uint32_t PathRequest_t2117613800_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t PathRequest_t2117613800_com_FromNativeMethodDefinition_MetadataUsageId;
extern RuntimeClass* PathRequestManager_t4163282249_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisPathfinding_t1696161914_m4281442259_RuntimeMethod_var;
extern const uint32_t PathRequestManager_Awake_m1065215676_MetadataUsageId;
extern const RuntimeMethod* Queue_1_get_Count_m3067217344_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1_Dequeue_m920511108_RuntimeMethod_var;
extern const RuntimeMethod* Action_2_Invoke_m2062908132_RuntimeMethod_var;
extern const uint32_t PathRequestManager_Update_m529797885_MetadataUsageId;
extern RuntimeClass* U3CU3Ec__DisplayClass5_0_t786576806_il2cpp_TypeInfo_var;
extern RuntimeClass* ThreadStart_t1006689297_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CU3Ec__DisplayClass5_0_U3CRequestPathU3Eb__0_m3190873041_RuntimeMethod_var;
extern const uint32_t PathRequestManager_RequestPath_m3072144720_MetadataUsageId;
extern const RuntimeMethod* Queue_1_Enqueue_m1828253350_RuntimeMethod_var;
extern const uint32_t PathRequestManager_FinishedProcessingPath_m1346038950_MetadataUsageId;
extern RuntimeClass* Queue_1_t1374272722_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Queue_1__ctor_m534127087_RuntimeMethod_var;
extern const uint32_t PathRequestManager__ctor_m2599481441_MetadataUsageId;
extern RuntimeClass* Action_1_t1700480823_il2cpp_TypeInfo_var;
extern const RuntimeMethod* PathRequestManager_FinishedProcessingPath_m1346038950_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m1884953144_RuntimeMethod_var;
extern const uint32_t U3CU3Ec__DisplayClass5_0_U3CRequestPathU3Eb__0_m3190873041_MetadataUsageId;
extern const uint32_t PauseMenu_Start_m517218302_MetadataUsageId;
extern String_t* _stringLiteral1985564044;
extern const uint32_t PauseMenu_DetectInput_m2353324690_MetadataUsageId;
extern const uint32_t PauseMenu_WhenPaused_m3668710605_MetadataUsageId;
extern const uint32_t PauseMenu_PauseGame_m3294073383_MetadataUsageId;
extern const uint32_t PauseMenu_ResumeGame_m1593775081_MetadataUsageId;
extern const uint32_t PauseMenu_DeselectAll_m424026074_MetadataUsageId;
extern const uint32_t PauseMenu__cctor_m1544404692_MetadataUsageId;
extern String_t* _stringLiteral1198113798;
extern const uint32_t PlayerController_FireGun_m2156033678_MetadataUsageId;
extern String_t* _stringLiteral4073296194;
extern String_t* _stringLiteral3596229116;
extern String_t* _stringLiteral2483366004;
extern const uint32_t PlayerController_MovePlayer_m2560970386_MetadataUsageId;
extern String_t* _stringLiteral524066506;
extern String_t* _stringLiteral4119010799;
extern const uint32_t PlayerController_UseWeapon_m3970795754_MetadataUsageId;
extern String_t* _stringLiteral717525408;
extern String_t* _stringLiteral3040548699;
extern const uint32_t PlayerController_UseShield_m304736563_MetadataUsageId;
extern const uint32_t PlayerHealthUI_Update_m1728854074_MetadataUsageId;
extern const uint32_t PlayerManager_Awake_m3341273811_MetadataUsageId;
extern const uint32_t PlayerStats_Update_m4084213154_MetadataUsageId;
extern const uint32_t RotatingLights_Update_m3387491593_MetadataUsageId;
extern const uint32_t SelectTitleOptions_Start_m504681833_MetadataUsageId;
extern const uint32_t SelectTitleOptions_UpdateOptions_m3436397335_MetadataUsageId;
extern String_t* _stringLiteral515201583;
extern const uint32_t SelectTitleOptions_SelectOptions_m1091832534_MetadataUsageId;
extern const uint32_t SelectTitleOptions_DeselectAll_m2671487348_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisMeshFilter_t3523625662_m1718783796_RuntimeMethod_var;
extern String_t* _stringLiteral3233433636;
extern String_t* _stringLiteral907351806;
extern String_t* _stringLiteral2904180494;
extern const uint32_t SoftNormalsToVertexColor_TryGenerate_m1356071037_MetadataUsageId;
extern RuntimeClass* ColorU5BU5D_t941916413_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1600127941_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t128053199_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m612749431_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m784752458_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2870147329_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m888956288_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2080863212_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1436172384_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m28313383_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1204004817_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3830006445_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m2838255531_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m2612064142_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1050804954_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m222348240_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m361000296_RuntimeMethod_var;
extern const uint32_t SoftNormalsToVertexColor_Generate_m3390156695_MetadataUsageId;
extern RuntimeClass* Path_t2615110272_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral3056858982;
extern const uint32_t Unit_OnPathFound_m1311327391_MetadataUsageId;
extern RuntimeClass* U3CUpdatePathU3Ed__10_t1880251700_il2cpp_TypeInfo_var;
extern const uint32_t Unit_UpdatePath_m2481450807_MetadataUsageId;
extern RuntimeClass* U3CFollowPathU3Ed__11_t3675468882_il2cpp_TypeInfo_var;
extern const uint32_t Unit_FollowPath_m1231934346_MetadataUsageId;
extern const uint32_t U3CFollowPathU3Ed__11_MoveNext_m796168624_MetadataUsageId;
extern const RuntimeMethod* U3CFollowPathU3Ed__11_System_Collections_IEnumerator_Reset_m1351258946_RuntimeMethod_var;
extern const uint32_t U3CFollowPathU3Ed__11_System_Collections_IEnumerator_Reset_m1351258946_MetadataUsageId;
extern const RuntimeMethod* Unit_OnPathFound_m1311327391_RuntimeMethod_var;
extern const RuntimeMethod* Action_2__ctor_m2824747351_RuntimeMethod_var;
extern const uint32_t U3CUpdatePathU3Ed__10_MoveNext_m3059736206_MetadataUsageId;
extern const RuntimeMethod* U3CUpdatePathU3Ed__10_System_Collections_IEnumerator_Reset_m2433452750_RuntimeMethod_var;
extern const uint32_t U3CUpdatePathU3Ed__10_System_Collections_IEnumerator_Reset_m2433452750_MetadataUsageId;
struct Exception_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Vector3_t3722313464 ;

struct GameObjectU5BU5D_t3328599146;
struct SpriteU5BU5D_t2581906349;
struct TerrainTypeU5BU5D_t1748095520;
struct NodeU5B0___U2C0___U5D_t1457036568;
struct Int32U5B0___U2C0___U5D_t385246373;
struct Vector3U5BU5D_t1718750761;
struct ObjectU5BU5D_t2843939325;
struct ImageU5BU5D_t2439009922;
struct DelegateU5BU5D_t1703627840;
struct LevelSelectPanelU5BU5D_t1747102860;
struct LineU5BU5D_t3212499767;
struct ColorU5BU5D_t941916413;


#ifndef U3CMODULEU3E_T692745562_H
#define U3CMODULEU3E_T692745562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745562 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745562_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef LIST_1_T128053199_H
#define LIST_1_T128053199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t128053199  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t385246372* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t128053199, ____items_1)); }
	inline Int32U5BU5D_t385246372* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t385246372** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t385246372* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t128053199, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t128053199, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t128053199, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t128053199_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t385246372* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t128053199_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t385246372* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t385246372** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t385246372* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T128053199_H
#ifndef LIST_1_T1600127941_H
#define LIST_1_T1600127941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Int32>>
struct  List_1_t1600127941  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	List_1U5BU5D_t3580830742* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1600127941, ____items_1)); }
	inline List_1U5BU5D_t3580830742* get__items_1() const { return ____items_1; }
	inline List_1U5BU5D_t3580830742** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(List_1U5BU5D_t3580830742* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1600127941, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1600127941, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t1600127941, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t1600127941_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	List_1U5BU5D_t3580830742* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t1600127941_StaticFields, ____emptyArray_5)); }
	inline List_1U5BU5D_t3580830742* get__emptyArray_5() const { return ____emptyArray_5; }
	inline List_1U5BU5D_t3580830742** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(List_1U5BU5D_t3580830742* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1600127941_H
#ifndef U3CFADEU3ED__7_T626104978_H
#define U3CFADEU3ED__7_T626104978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bullet/<Fade>d__7
struct  U3CFadeU3Ed__7_t626104978  : public RuntimeObject
{
public:
	// System.Int32 Bullet/<Fade>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Bullet/<Fade>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Bullet Bullet/<Fade>d__7::<>4__this
	Bullet_t1042140031 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFadeU3Ed__7_t626104978, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFadeU3Ed__7_t626104978, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFadeU3Ed__7_t626104978, ___U3CU3E4__this_2)); }
	inline Bullet_t1042140031 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Bullet_t1042140031 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Bullet_t1042140031 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEU3ED__7_T626104978_H
#ifndef U3CFOLLOWPATHU3ED__11_T3675468882_H
#define U3CFOLLOWPATHU3ED__11_T3675468882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unit/<FollowPath>d__11
struct  U3CFollowPathU3Ed__11_t3675468882  : public RuntimeObject
{
public:
	// System.Int32 Unit/<FollowPath>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Unit/<FollowPath>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Unit Unit/<FollowPath>d__11::<>4__this
	Unit_t4139495810 * ___U3CU3E4__this_2;
	// System.Int32 Unit/<FollowPath>d__11::<pathIndex>5__1
	int32_t ___U3CpathIndexU3E5__1_3;
	// System.Boolean Unit/<FollowPath>d__11::<followingPath>5__2
	bool ___U3CfollowingPathU3E5__2_4;
	// System.Single Unit/<FollowPath>d__11::<speedPercent>5__3
	float ___U3CspeedPercentU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFollowPathU3Ed__11_t3675468882, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFollowPathU3Ed__11_t3675468882, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFollowPathU3Ed__11_t3675468882, ___U3CU3E4__this_2)); }
	inline Unit_t4139495810 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Unit_t4139495810 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Unit_t4139495810 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CpathIndexU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CFollowPathU3Ed__11_t3675468882, ___U3CpathIndexU3E5__1_3)); }
	inline int32_t get_U3CpathIndexU3E5__1_3() const { return ___U3CpathIndexU3E5__1_3; }
	inline int32_t* get_address_of_U3CpathIndexU3E5__1_3() { return &___U3CpathIndexU3E5__1_3; }
	inline void set_U3CpathIndexU3E5__1_3(int32_t value)
	{
		___U3CpathIndexU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CfollowingPathU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CFollowPathU3Ed__11_t3675468882, ___U3CfollowingPathU3E5__2_4)); }
	inline bool get_U3CfollowingPathU3E5__2_4() const { return ___U3CfollowingPathU3E5__2_4; }
	inline bool* get_address_of_U3CfollowingPathU3E5__2_4() { return &___U3CfollowingPathU3E5__2_4; }
	inline void set_U3CfollowingPathU3E5__2_4(bool value)
	{
		___U3CfollowingPathU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CspeedPercentU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CFollowPathU3Ed__11_t3675468882, ___U3CspeedPercentU3E5__3_5)); }
	inline float get_U3CspeedPercentU3E5__3_5() const { return ___U3CspeedPercentU3E5__3_5; }
	inline float* get_address_of_U3CspeedPercentU3E5__3_5() { return &___U3CspeedPercentU3E5__3_5; }
	inline void set_U3CspeedPercentU3E5__3_5(float value)
	{
		___U3CspeedPercentU3E5__3_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOLLOWPATHU3ED__11_T3675468882_H
#ifndef DICTIONARY_2_T1839659084_H
#define DICTIONARY_2_T1839659084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct  Dictionary_2_t1839659084  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t385246372* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t742410749* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t2029334555 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t3555703402 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t1839659084, ___buckets_0)); }
	inline Int32U5BU5D_t385246372* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t385246372** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t385246372* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t1839659084, ___entries_1)); }
	inline EntryU5BU5D_t742410749* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t742410749** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t742410749* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t1839659084, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t1839659084, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1839659084, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1839659084, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1839659084, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1839659084, ___keys_7)); }
	inline KeyCollection_t2029334555 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t2029334555 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t2029334555 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1839659084, ___values_8)); }
	inline ValueCollection_t3555703402 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t3555703402 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t3555703402 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1839659084, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1839659084_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef STOPWATCH_T305734070_H
#define STOPWATCH_T305734070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.Stopwatch
struct  Stopwatch_t305734070  : public RuntimeObject
{
public:
	// System.Int64 System.Diagnostics.Stopwatch::elapsed
	int64_t ___elapsed_2;
	// System.Int64 System.Diagnostics.Stopwatch::started
	int64_t ___started_3;
	// System.Boolean System.Diagnostics.Stopwatch::is_running
	bool ___is_running_4;

public:
	inline static int32_t get_offset_of_elapsed_2() { return static_cast<int32_t>(offsetof(Stopwatch_t305734070, ___elapsed_2)); }
	inline int64_t get_elapsed_2() const { return ___elapsed_2; }
	inline int64_t* get_address_of_elapsed_2() { return &___elapsed_2; }
	inline void set_elapsed_2(int64_t value)
	{
		___elapsed_2 = value;
	}

	inline static int32_t get_offset_of_started_3() { return static_cast<int32_t>(offsetof(Stopwatch_t305734070, ___started_3)); }
	inline int64_t get_started_3() const { return ___started_3; }
	inline int64_t* get_address_of_started_3() { return &___started_3; }
	inline void set_started_3(int64_t value)
	{
		___started_3 = value;
	}

	inline static int32_t get_offset_of_is_running_4() { return static_cast<int32_t>(offsetof(Stopwatch_t305734070, ___is_running_4)); }
	inline bool get_is_running_4() const { return ___is_running_4; }
	inline bool* get_address_of_is_running_4() { return &___is_running_4; }
	inline void set_is_running_4(bool value)
	{
		___is_running_4 = value;
	}
};

struct Stopwatch_t305734070_StaticFields
{
public:
	// System.Int64 System.Diagnostics.Stopwatch::Frequency
	int64_t ___Frequency_0;
	// System.Boolean System.Diagnostics.Stopwatch::IsHighResolution
	bool ___IsHighResolution_1;

public:
	inline static int32_t get_offset_of_Frequency_0() { return static_cast<int32_t>(offsetof(Stopwatch_t305734070_StaticFields, ___Frequency_0)); }
	inline int64_t get_Frequency_0() const { return ___Frequency_0; }
	inline int64_t* get_address_of_Frequency_0() { return &___Frequency_0; }
	inline void set_Frequency_0(int64_t value)
	{
		___Frequency_0 = value;
	}

	inline static int32_t get_offset_of_IsHighResolution_1() { return static_cast<int32_t>(offsetof(Stopwatch_t305734070_StaticFields, ___IsHighResolution_1)); }
	inline bool get_IsHighResolution_1() const { return ___IsHighResolution_1; }
	inline bool* get_address_of_IsHighResolution_1() { return &___IsHighResolution_1; }
	inline void set_IsHighResolution_1(bool value)
	{
		___IsHighResolution_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STOPWATCH_T305734070_H
#ifndef PATH_T2615110272_H
#define PATH_T2615110272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Path
struct  Path_t2615110272  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] Path::lookPoints
	Vector3U5BU5D_t1718750761* ___lookPoints_0;
	// Line[] Path::turnBoundaries
	LineU5BU5D_t3212499767* ___turnBoundaries_1;
	// System.Int32 Path::finishLineIndex
	int32_t ___finishLineIndex_2;
	// System.Int32 Path::slowDownIndex
	int32_t ___slowDownIndex_3;

public:
	inline static int32_t get_offset_of_lookPoints_0() { return static_cast<int32_t>(offsetof(Path_t2615110272, ___lookPoints_0)); }
	inline Vector3U5BU5D_t1718750761* get_lookPoints_0() const { return ___lookPoints_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_lookPoints_0() { return &___lookPoints_0; }
	inline void set_lookPoints_0(Vector3U5BU5D_t1718750761* value)
	{
		___lookPoints_0 = value;
		Il2CppCodeGenWriteBarrier((&___lookPoints_0), value);
	}

	inline static int32_t get_offset_of_turnBoundaries_1() { return static_cast<int32_t>(offsetof(Path_t2615110272, ___turnBoundaries_1)); }
	inline LineU5BU5D_t3212499767* get_turnBoundaries_1() const { return ___turnBoundaries_1; }
	inline LineU5BU5D_t3212499767** get_address_of_turnBoundaries_1() { return &___turnBoundaries_1; }
	inline void set_turnBoundaries_1(LineU5BU5D_t3212499767* value)
	{
		___turnBoundaries_1 = value;
		Il2CppCodeGenWriteBarrier((&___turnBoundaries_1), value);
	}

	inline static int32_t get_offset_of_finishLineIndex_2() { return static_cast<int32_t>(offsetof(Path_t2615110272, ___finishLineIndex_2)); }
	inline int32_t get_finishLineIndex_2() const { return ___finishLineIndex_2; }
	inline int32_t* get_address_of_finishLineIndex_2() { return &___finishLineIndex_2; }
	inline void set_finishLineIndex_2(int32_t value)
	{
		___finishLineIndex_2 = value;
	}

	inline static int32_t get_offset_of_slowDownIndex_3() { return static_cast<int32_t>(offsetof(Path_t2615110272, ___slowDownIndex_3)); }
	inline int32_t get_slowDownIndex_3() const { return ___slowDownIndex_3; }
	inline int32_t* get_address_of_slowDownIndex_3() { return &___slowDownIndex_3; }
	inline void set_slowDownIndex_3(int32_t value)
	{
		___slowDownIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATH_T2615110272_H
#ifndef LIST_1_T167102488_H
#define LIST_1_T167102488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Node>
struct  List_1_t167102488  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	NodeU5BU5D_t1457036567* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t167102488, ____items_1)); }
	inline NodeU5BU5D_t1457036567* get__items_1() const { return ____items_1; }
	inline NodeU5BU5D_t1457036567** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(NodeU5BU5D_t1457036567* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t167102488, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t167102488, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t167102488, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t167102488_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	NodeU5BU5D_t1457036567* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t167102488_StaticFields, ____emptyArray_5)); }
	inline NodeU5BU5D_t1457036567* get__emptyArray_5() const { return ____emptyArray_5; }
	inline NodeU5BU5D_t1457036567** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(NodeU5BU5D_t1457036567* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T167102488_H
#ifndef HEAP_1_T911941676_H
#define HEAP_1_T911941676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Heap`1<Node>
struct  Heap_1_t911941676  : public RuntimeObject
{
public:
	// T[] Heap`1::items
	NodeU5BU5D_t1457036567* ___items_0;
	// System.Int32 Heap`1::currentItemCount
	int32_t ___currentItemCount_1;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(Heap_1_t911941676, ___items_0)); }
	inline NodeU5BU5D_t1457036567* get_items_0() const { return ___items_0; }
	inline NodeU5BU5D_t1457036567** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(NodeU5BU5D_t1457036567* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}

	inline static int32_t get_offset_of_currentItemCount_1() { return static_cast<int32_t>(offsetof(Heap_1_t911941676, ___currentItemCount_1)); }
	inline int32_t get_currentItemCount_1() const { return ___currentItemCount_1; }
	inline int32_t* get_address_of_currentItemCount_1() { return &___currentItemCount_1; }
	inline void set_currentItemCount_1(int32_t value)
	{
		___currentItemCount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEAP_1_T911941676_H
#ifndef QUEUE_1_T1374272722_H
#define QUEUE_1_T1374272722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1<PathResult>
struct  Queue_1_t1374272722  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Queue`1::_array
	PathResultU5BU5D_t135407141* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_tail
	int32_t ____tail_2;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_3;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_4;
	// System.Object System.Collections.Generic.Queue`1::_syncRoot
	RuntimeObject * ____syncRoot_5;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Queue_1_t1374272722, ____array_0)); }
	inline PathResultU5BU5D_t135407141* get__array_0() const { return ____array_0; }
	inline PathResultU5BU5D_t135407141** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(PathResultU5BU5D_t135407141* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__head_1() { return static_cast<int32_t>(offsetof(Queue_1_t1374272722, ____head_1)); }
	inline int32_t get__head_1() const { return ____head_1; }
	inline int32_t* get_address_of__head_1() { return &____head_1; }
	inline void set__head_1(int32_t value)
	{
		____head_1 = value;
	}

	inline static int32_t get_offset_of__tail_2() { return static_cast<int32_t>(offsetof(Queue_1_t1374272722, ____tail_2)); }
	inline int32_t get__tail_2() const { return ____tail_2; }
	inline int32_t* get_address_of__tail_2() { return &____tail_2; }
	inline void set__tail_2(int32_t value)
	{
		____tail_2 = value;
	}

	inline static int32_t get_offset_of__size_3() { return static_cast<int32_t>(offsetof(Queue_1_t1374272722, ____size_3)); }
	inline int32_t get__size_3() const { return ____size_3; }
	inline int32_t* get_address_of__size_3() { return &____size_3; }
	inline void set__size_3(int32_t value)
	{
		____size_3 = value;
	}

	inline static int32_t get_offset_of__version_4() { return static_cast<int32_t>(offsetof(Queue_1_t1374272722, ____version_4)); }
	inline int32_t get__version_4() const { return ____version_4; }
	inline int32_t* get_address_of__version_4() { return &____version_4; }
	inline void set__version_4(int32_t value)
	{
		____version_4 = value;
	}

	inline static int32_t get_offset_of__syncRoot_5() { return static_cast<int32_t>(offsetof(Queue_1_t1374272722, ____syncRoot_5)); }
	inline RuntimeObject * get__syncRoot_5() const { return ____syncRoot_5; }
	inline RuntimeObject ** get_address_of__syncRoot_5() { return &____syncRoot_5; }
	inline void set__syncRoot_5(RuntimeObject * value)
	{
		____syncRoot_5 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUE_1_T1374272722_H
#ifndef LIST_1_T899420910_H
#define LIST_1_T899420910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_t899420910  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_t1718750761* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____items_1)); }
	inline Vector3U5BU5D_t1718750761* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_t1718750761* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t899420910, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t899420910_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector3U5BU5D_t1718750761* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t899420910_StaticFields, ____emptyArray_5)); }
	inline Vector3U5BU5D_t1718750761* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector3U5BU5D_t1718750761* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T899420910_H
#ifndef HASHSET_1_T1554944516_H
#define HASHSET_1_T1554944516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.HashSet`1<Node>
struct  HashSet_1_t1554944516  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.HashSet`1::_buckets
	Int32U5BU5D_t385246372* ____buckets_7;
	// System.Collections.Generic.HashSet`1/Slot<T>[] System.Collections.Generic.HashSet`1::_slots
	SlotU5BU5D_t4025953330* ____slots_8;
	// System.Int32 System.Collections.Generic.HashSet`1::_count
	int32_t ____count_9;
	// System.Int32 System.Collections.Generic.HashSet`1::_lastIndex
	int32_t ____lastIndex_10;
	// System.Int32 System.Collections.Generic.HashSet`1::_freeList
	int32_t ____freeList_11;
	// System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::_comparer
	RuntimeObject* ____comparer_12;
	// System.Int32 System.Collections.Generic.HashSet`1::_version
	int32_t ____version_13;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.HashSet`1::_siInfo
	SerializationInfo_t950877179 * ____siInfo_14;

public:
	inline static int32_t get_offset_of__buckets_7() { return static_cast<int32_t>(offsetof(HashSet_1_t1554944516, ____buckets_7)); }
	inline Int32U5BU5D_t385246372* get__buckets_7() const { return ____buckets_7; }
	inline Int32U5BU5D_t385246372** get_address_of__buckets_7() { return &____buckets_7; }
	inline void set__buckets_7(Int32U5BU5D_t385246372* value)
	{
		____buckets_7 = value;
		Il2CppCodeGenWriteBarrier((&____buckets_7), value);
	}

	inline static int32_t get_offset_of__slots_8() { return static_cast<int32_t>(offsetof(HashSet_1_t1554944516, ____slots_8)); }
	inline SlotU5BU5D_t4025953330* get__slots_8() const { return ____slots_8; }
	inline SlotU5BU5D_t4025953330** get_address_of__slots_8() { return &____slots_8; }
	inline void set__slots_8(SlotU5BU5D_t4025953330* value)
	{
		____slots_8 = value;
		Il2CppCodeGenWriteBarrier((&____slots_8), value);
	}

	inline static int32_t get_offset_of__count_9() { return static_cast<int32_t>(offsetof(HashSet_1_t1554944516, ____count_9)); }
	inline int32_t get__count_9() const { return ____count_9; }
	inline int32_t* get_address_of__count_9() { return &____count_9; }
	inline void set__count_9(int32_t value)
	{
		____count_9 = value;
	}

	inline static int32_t get_offset_of__lastIndex_10() { return static_cast<int32_t>(offsetof(HashSet_1_t1554944516, ____lastIndex_10)); }
	inline int32_t get__lastIndex_10() const { return ____lastIndex_10; }
	inline int32_t* get_address_of__lastIndex_10() { return &____lastIndex_10; }
	inline void set__lastIndex_10(int32_t value)
	{
		____lastIndex_10 = value;
	}

	inline static int32_t get_offset_of__freeList_11() { return static_cast<int32_t>(offsetof(HashSet_1_t1554944516, ____freeList_11)); }
	inline int32_t get__freeList_11() const { return ____freeList_11; }
	inline int32_t* get_address_of__freeList_11() { return &____freeList_11; }
	inline void set__freeList_11(int32_t value)
	{
		____freeList_11 = value;
	}

	inline static int32_t get_offset_of__comparer_12() { return static_cast<int32_t>(offsetof(HashSet_1_t1554944516, ____comparer_12)); }
	inline RuntimeObject* get__comparer_12() const { return ____comparer_12; }
	inline RuntimeObject** get_address_of__comparer_12() { return &____comparer_12; }
	inline void set__comparer_12(RuntimeObject* value)
	{
		____comparer_12 = value;
		Il2CppCodeGenWriteBarrier((&____comparer_12), value);
	}

	inline static int32_t get_offset_of__version_13() { return static_cast<int32_t>(offsetof(HashSet_1_t1554944516, ____version_13)); }
	inline int32_t get__version_13() const { return ____version_13; }
	inline int32_t* get_address_of__version_13() { return &____version_13; }
	inline void set__version_13(int32_t value)
	{
		____version_13 = value;
	}

	inline static int32_t get_offset_of__siInfo_14() { return static_cast<int32_t>(offsetof(HashSet_1_t1554944516, ____siInfo_14)); }
	inline SerializationInfo_t950877179 * get__siInfo_14() const { return ____siInfo_14; }
	inline SerializationInfo_t950877179 ** get_address_of__siInfo_14() { return &____siInfo_14; }
	inline void set__siInfo_14(SerializationInfo_t950877179 * value)
	{
		____siInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&____siInfo_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHSET_1_T1554944516_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef WAITFORSECONDS_T1699091251_H
#define WAITFORSECONDS_T1699091251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t1699091251  : public YieldInstruction_t403091072
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t1699091251, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T1699091251_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ENUMERATOR_T2017297076_H
#define ENUMERATOR_T2017297076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Int32>
struct  Enumerator_t2017297076 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t128053199 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t2017297076, ___list_0)); }
	inline List_1_t128053199 * get_list_0() const { return ___list_0; }
	inline List_1_t128053199 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t128053199 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t2017297076, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t2017297076, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2017297076, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2017297076_H
#ifndef ENUMERATOR_T3489371818_H
#define ENUMERATOR_T3489371818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<System.Int32>>
struct  Enumerator_t3489371818 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t1600127941 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	List_1_t128053199 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t3489371818, ___list_0)); }
	inline List_1_t1600127941 * get_list_0() const { return ___list_0; }
	inline List_1_t1600127941 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t1600127941 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t3489371818, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t3489371818, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3489371818, ___current_3)); }
	inline List_1_t128053199 * get_current_3() const { return ___current_3; }
	inline List_1_t128053199 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(List_1_t128053199 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3489371818_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef PATHRESULT_T1528013228_H
#define PATHRESULT_T1528013228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PathResult
struct  PathResult_t1528013228 
{
public:
	// UnityEngine.Vector3[] PathResult::path
	Vector3U5BU5D_t1718750761* ___path_0;
	// System.Boolean PathResult::success
	bool ___success_1;
	// System.Action`2<UnityEngine.Vector3[],System.Boolean> PathResult::callback
	Action_2_t1484661638 * ___callback_2;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(PathResult_t1528013228, ___path_0)); }
	inline Vector3U5BU5D_t1718750761* get_path_0() const { return ___path_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(Vector3U5BU5D_t1718750761* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier((&___path_0), value);
	}

	inline static int32_t get_offset_of_success_1() { return static_cast<int32_t>(offsetof(PathResult_t1528013228, ___success_1)); }
	inline bool get_success_1() const { return ___success_1; }
	inline bool* get_address_of_success_1() { return &___success_1; }
	inline void set_success_1(bool value)
	{
		___success_1 = value;
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(PathResult_t1528013228, ___callback_2)); }
	inline Action_2_t1484661638 * get_callback_2() const { return ___callback_2; }
	inline Action_2_t1484661638 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_2_t1484661638 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PathResult
struct PathResult_t1528013228_marshaled_pinvoke
{
	Vector3_t3722313464 * ___path_0;
	int32_t ___success_1;
	Il2CppMethodPointer ___callback_2;
};
// Native definition for COM marshalling of PathResult
struct PathResult_t1528013228_marshaled_com
{
	Vector3_t3722313464 * ___path_0;
	int32_t ___success_1;
	Il2CppMethodPointer ___callback_2;
};
#endif // PATHRESULT_T1528013228_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t257213610 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___list_0)); }
	inline List_1_t257213610 * get_list_0() const { return ___list_0; }
	inline List_1_t257213610 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t257213610 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef ENUMERATOR_T2056346365_H
#define ENUMERATOR_T2056346365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Node>
struct  Enumerator_t2056346365 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t167102488 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	Node_t2989995042 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t2056346365, ___list_0)); }
	inline List_1_t167102488 * get_list_0() const { return ___list_0; }
	inline List_1_t167102488 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t167102488 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t2056346365, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t2056346365, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2056346365, ___current_3)); }
	inline Node_t2989995042 * get_current_3() const { return ___current_3; }
	inline Node_t2989995042 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Node_t2989995042 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2056346365_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef LINE_T3796957314_H
#define LINE_T3796957314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Line
struct  Line_t3796957314 
{
public:
	// System.Single Line::gradient
	float ___gradient_1;
	// System.Single Line::yIntercept
	float ___yIntercept_2;
	// UnityEngine.Vector2 Line::pointOnLine_1
	Vector2_t2156229523  ___pointOnLine_1_3;
	// UnityEngine.Vector2 Line::pointOnLine_2
	Vector2_t2156229523  ___pointOnLine_2_4;
	// System.Single Line::gradientPerpendicular
	float ___gradientPerpendicular_5;
	// System.Boolean Line::approachSide
	bool ___approachSide_6;

public:
	inline static int32_t get_offset_of_gradient_1() { return static_cast<int32_t>(offsetof(Line_t3796957314, ___gradient_1)); }
	inline float get_gradient_1() const { return ___gradient_1; }
	inline float* get_address_of_gradient_1() { return &___gradient_1; }
	inline void set_gradient_1(float value)
	{
		___gradient_1 = value;
	}

	inline static int32_t get_offset_of_yIntercept_2() { return static_cast<int32_t>(offsetof(Line_t3796957314, ___yIntercept_2)); }
	inline float get_yIntercept_2() const { return ___yIntercept_2; }
	inline float* get_address_of_yIntercept_2() { return &___yIntercept_2; }
	inline void set_yIntercept_2(float value)
	{
		___yIntercept_2 = value;
	}

	inline static int32_t get_offset_of_pointOnLine_1_3() { return static_cast<int32_t>(offsetof(Line_t3796957314, ___pointOnLine_1_3)); }
	inline Vector2_t2156229523  get_pointOnLine_1_3() const { return ___pointOnLine_1_3; }
	inline Vector2_t2156229523 * get_address_of_pointOnLine_1_3() { return &___pointOnLine_1_3; }
	inline void set_pointOnLine_1_3(Vector2_t2156229523  value)
	{
		___pointOnLine_1_3 = value;
	}

	inline static int32_t get_offset_of_pointOnLine_2_4() { return static_cast<int32_t>(offsetof(Line_t3796957314, ___pointOnLine_2_4)); }
	inline Vector2_t2156229523  get_pointOnLine_2_4() const { return ___pointOnLine_2_4; }
	inline Vector2_t2156229523 * get_address_of_pointOnLine_2_4() { return &___pointOnLine_2_4; }
	inline void set_pointOnLine_2_4(Vector2_t2156229523  value)
	{
		___pointOnLine_2_4 = value;
	}

	inline static int32_t get_offset_of_gradientPerpendicular_5() { return static_cast<int32_t>(offsetof(Line_t3796957314, ___gradientPerpendicular_5)); }
	inline float get_gradientPerpendicular_5() const { return ___gradientPerpendicular_5; }
	inline float* get_address_of_gradientPerpendicular_5() { return &___gradientPerpendicular_5; }
	inline void set_gradientPerpendicular_5(float value)
	{
		___gradientPerpendicular_5 = value;
	}

	inline static int32_t get_offset_of_approachSide_6() { return static_cast<int32_t>(offsetof(Line_t3796957314, ___approachSide_6)); }
	inline bool get_approachSide_6() const { return ___approachSide_6; }
	inline bool* get_address_of_approachSide_6() { return &___approachSide_6; }
	inline void set_approachSide_6(bool value)
	{
		___approachSide_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Line
struct Line_t3796957314_marshaled_pinvoke
{
	float ___gradient_1;
	float ___yIntercept_2;
	Vector2_t2156229523  ___pointOnLine_1_3;
	Vector2_t2156229523  ___pointOnLine_2_4;
	float ___gradientPerpendicular_5;
	int32_t ___approachSide_6;
};
// Native definition for COM marshalling of Line
struct Line_t3796957314_marshaled_com
{
	float ___gradient_1;
	float ___yIntercept_2;
	Vector2_t2156229523  ___pointOnLine_1_3;
	Vector2_t2156229523  ___pointOnLine_2_4;
	float ___gradientPerpendicular_5;
	int32_t ___approachSide_6;
};
#endif // LINE_T3796957314_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef NODE_T2989995042_H
#define NODE_T2989995042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Node
struct  Node_t2989995042  : public RuntimeObject
{
public:
	// System.Boolean Node::walkable
	bool ___walkable_0;
	// UnityEngine.Vector3 Node::worldPosition
	Vector3_t3722313464  ___worldPosition_1;
	// System.Int32 Node::gridX
	int32_t ___gridX_2;
	// System.Int32 Node::gridY
	int32_t ___gridY_3;
	// System.Int32 Node::movementPenalty
	int32_t ___movementPenalty_4;
	// System.Int32 Node::gCost
	int32_t ___gCost_5;
	// System.Int32 Node::hCost
	int32_t ___hCost_6;
	// Node Node::parent
	Node_t2989995042 * ___parent_7;
	// System.Int32 Node::heapIndex
	int32_t ___heapIndex_8;

public:
	inline static int32_t get_offset_of_walkable_0() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___walkable_0)); }
	inline bool get_walkable_0() const { return ___walkable_0; }
	inline bool* get_address_of_walkable_0() { return &___walkable_0; }
	inline void set_walkable_0(bool value)
	{
		___walkable_0 = value;
	}

	inline static int32_t get_offset_of_worldPosition_1() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___worldPosition_1)); }
	inline Vector3_t3722313464  get_worldPosition_1() const { return ___worldPosition_1; }
	inline Vector3_t3722313464 * get_address_of_worldPosition_1() { return &___worldPosition_1; }
	inline void set_worldPosition_1(Vector3_t3722313464  value)
	{
		___worldPosition_1 = value;
	}

	inline static int32_t get_offset_of_gridX_2() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___gridX_2)); }
	inline int32_t get_gridX_2() const { return ___gridX_2; }
	inline int32_t* get_address_of_gridX_2() { return &___gridX_2; }
	inline void set_gridX_2(int32_t value)
	{
		___gridX_2 = value;
	}

	inline static int32_t get_offset_of_gridY_3() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___gridY_3)); }
	inline int32_t get_gridY_3() const { return ___gridY_3; }
	inline int32_t* get_address_of_gridY_3() { return &___gridY_3; }
	inline void set_gridY_3(int32_t value)
	{
		___gridY_3 = value;
	}

	inline static int32_t get_offset_of_movementPenalty_4() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___movementPenalty_4)); }
	inline int32_t get_movementPenalty_4() const { return ___movementPenalty_4; }
	inline int32_t* get_address_of_movementPenalty_4() { return &___movementPenalty_4; }
	inline void set_movementPenalty_4(int32_t value)
	{
		___movementPenalty_4 = value;
	}

	inline static int32_t get_offset_of_gCost_5() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___gCost_5)); }
	inline int32_t get_gCost_5() const { return ___gCost_5; }
	inline int32_t* get_address_of_gCost_5() { return &___gCost_5; }
	inline void set_gCost_5(int32_t value)
	{
		___gCost_5 = value;
	}

	inline static int32_t get_offset_of_hCost_6() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___hCost_6)); }
	inline int32_t get_hCost_6() const { return ___hCost_6; }
	inline int32_t* get_address_of_hCost_6() { return &___hCost_6; }
	inline void set_hCost_6(int32_t value)
	{
		___hCost_6 = value;
	}

	inline static int32_t get_offset_of_parent_7() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___parent_7)); }
	inline Node_t2989995042 * get_parent_7() const { return ___parent_7; }
	inline Node_t2989995042 ** get_address_of_parent_7() { return &___parent_7; }
	inline void set_parent_7(Node_t2989995042 * value)
	{
		___parent_7 = value;
		Il2CppCodeGenWriteBarrier((&___parent_7), value);
	}

	inline static int32_t get_offset_of_heapIndex_8() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___heapIndex_8)); }
	inline int32_t get_heapIndex_8() const { return ___heapIndex_8; }
	inline int32_t* get_address_of_heapIndex_8() { return &___heapIndex_8; }
	inline void set_heapIndex_8(int32_t value)
	{
		___heapIndex_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODE_T2989995042_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef GUNTYPE_T2644971432_H
#define GUNTYPE_T2644971432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gun/GunType
struct  GunType_t2644971432 
{
public:
	// System.Int32 Gun/GunType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GunType_t2644971432, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUNTYPE_T2644971432_H
#ifndef SLOTTYPE_T413958767_H
#define SLOTTYPE_T413958767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventorySlot/SlotType
struct  SlotType_t413958767 
{
public:
	// System.Int32 InventorySlot/SlotType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SlotType_t413958767, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTTYPE_T413958767_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef TERRAINTYPE_T3627529997_H
#define TERRAINTYPE_T3627529997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Grid/TerrainType
struct  TerrainType_t3627529997  : public RuntimeObject
{
public:
	// UnityEngine.LayerMask Grid/TerrainType::terrainMask
	LayerMask_t3493934918  ___terrainMask_0;
	// System.Int32 Grid/TerrainType::terrainPenalty
	int32_t ___terrainPenalty_1;

public:
	inline static int32_t get_offset_of_terrainMask_0() { return static_cast<int32_t>(offsetof(TerrainType_t3627529997, ___terrainMask_0)); }
	inline LayerMask_t3493934918  get_terrainMask_0() const { return ___terrainMask_0; }
	inline LayerMask_t3493934918 * get_address_of_terrainMask_0() { return &___terrainMask_0; }
	inline void set_terrainMask_0(LayerMask_t3493934918  value)
	{
		___terrainMask_0 = value;
	}

	inline static int32_t get_offset_of_terrainPenalty_1() { return static_cast<int32_t>(offsetof(TerrainType_t3627529997, ___terrainPenalty_1)); }
	inline int32_t get_terrainPenalty_1() const { return ___terrainPenalty_1; }
	inline int32_t* get_address_of_terrainPenalty_1() { return &___terrainPenalty_1; }
	inline void set_terrainPenalty_1(int32_t value)
	{
		___terrainPenalty_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAINTYPE_T3627529997_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#define TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboardType
struct  TouchScreenKeyboardType_t1530597702 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_t1530597702, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARDTYPE_T1530597702_H
#ifndef U3CUPDATEPATHU3ED__10_T1880251700_H
#define U3CUPDATEPATHU3ED__10_T1880251700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unit/<UpdatePath>d__10
struct  U3CUpdatePathU3Ed__10_t1880251700  : public RuntimeObject
{
public:
	// System.Int32 Unit/<UpdatePath>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Unit/<UpdatePath>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Unit Unit/<UpdatePath>d__10::<>4__this
	Unit_t4139495810 * ___U3CU3E4__this_2;
	// UnityEngine.Vector3 Unit/<UpdatePath>d__10::<targetPosOld>5__1
	Vector3_t3722313464  ___U3CtargetPosOldU3E5__1_3;
	// System.Single Unit/<UpdatePath>d__10::<squareMoveThreshhold>5__2
	float ___U3CsquareMoveThreshholdU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdatePathU3Ed__10_t1880251700, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdatePathU3Ed__10_t1880251700, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdatePathU3Ed__10_t1880251700, ___U3CU3E4__this_2)); }
	inline Unit_t4139495810 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Unit_t4139495810 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Unit_t4139495810 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CtargetPosOldU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CUpdatePathU3Ed__10_t1880251700, ___U3CtargetPosOldU3E5__1_3)); }
	inline Vector3_t3722313464  get_U3CtargetPosOldU3E5__1_3() const { return ___U3CtargetPosOldU3E5__1_3; }
	inline Vector3_t3722313464 * get_address_of_U3CtargetPosOldU3E5__1_3() { return &___U3CtargetPosOldU3E5__1_3; }
	inline void set_U3CtargetPosOldU3E5__1_3(Vector3_t3722313464  value)
	{
		___U3CtargetPosOldU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CsquareMoveThreshholdU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CUpdatePathU3Ed__10_t1880251700, ___U3CsquareMoveThreshholdU3E5__2_4)); }
	inline float get_U3CsquareMoveThreshholdU3E5__2_4() const { return ___U3CsquareMoveThreshholdU3E5__2_4; }
	inline float* get_address_of_U3CsquareMoveThreshholdU3E5__2_4() { return &___U3CsquareMoveThreshholdU3E5__2_4; }
	inline void set_U3CsquareMoveThreshholdU3E5__2_4(float value)
	{
		___U3CsquareMoveThreshholdU3E5__2_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEPATHU3ED__10_T1880251700_H
#ifndef FILLMETHOD_T1167457570_H
#define FILLMETHOD_T1167457570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1167457570 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_t1167457570, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1167457570_H
#ifndef TYPE_T1152881528_H
#define TYPE_T1152881528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t1152881528 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t1152881528, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1152881528_H
#ifndef INPUTTYPE_T1770400679_H
#define INPUTTYPE_T1770400679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/InputType
struct  InputType_t1770400679 
{
public:
	// System.Int32 UnityEngine.UI.InputField/InputType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputType_t1770400679, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTYPE_T1770400679_H
#ifndef METHOD_T2777851340_H
#define METHOD_T2777851340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoftNormalsToVertexColor/Method
struct  Method_t2777851340 
{
public:
	// System.Int32 SoftNormalsToVertexColor/Method::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Method_t2777851340, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHOD_T2777851340_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef PATHREQUEST_T2117613800_H
#define PATHREQUEST_T2117613800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PathRequest
struct  PathRequest_t2117613800 
{
public:
	// UnityEngine.Vector3 PathRequest::pathStart
	Vector3_t3722313464  ___pathStart_0;
	// UnityEngine.Vector3 PathRequest::pathEnd
	Vector3_t3722313464  ___pathEnd_1;
	// System.Action`2<UnityEngine.Vector3[],System.Boolean> PathRequest::callback
	Action_2_t1484661638 * ___callback_2;

public:
	inline static int32_t get_offset_of_pathStart_0() { return static_cast<int32_t>(offsetof(PathRequest_t2117613800, ___pathStart_0)); }
	inline Vector3_t3722313464  get_pathStart_0() const { return ___pathStart_0; }
	inline Vector3_t3722313464 * get_address_of_pathStart_0() { return &___pathStart_0; }
	inline void set_pathStart_0(Vector3_t3722313464  value)
	{
		___pathStart_0 = value;
	}

	inline static int32_t get_offset_of_pathEnd_1() { return static_cast<int32_t>(offsetof(PathRequest_t2117613800, ___pathEnd_1)); }
	inline Vector3_t3722313464  get_pathEnd_1() const { return ___pathEnd_1; }
	inline Vector3_t3722313464 * get_address_of_pathEnd_1() { return &___pathEnd_1; }
	inline void set_pathEnd_1(Vector3_t3722313464  value)
	{
		___pathEnd_1 = value;
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(PathRequest_t2117613800, ___callback_2)); }
	inline Action_2_t1484661638 * get_callback_2() const { return ___callback_2; }
	inline Action_2_t1484661638 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_2_t1484661638 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PathRequest
struct PathRequest_t2117613800_marshaled_pinvoke
{
	Vector3_t3722313464  ___pathStart_0;
	Vector3_t3722313464  ___pathEnd_1;
	Il2CppMethodPointer ___callback_2;
};
// Native definition for COM marshalling of PathRequest
struct PathRequest_t2117613800_marshaled_com
{
	Vector3_t3722313464  ___pathStart_0;
	Vector3_t3722313464  ___pathEnd_1;
	Il2CppMethodPointer ___callback_2;
};
#endif // PATHREQUEST_T2117613800_H
#ifndef LINETYPE_T4214648469_H
#define LINETYPE_T4214648469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/LineType
struct  LineType_t4214648469 
{
public:
	// System.Int32 UnityEngine.UI.InputField/LineType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LineType_t4214648469, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETYPE_T4214648469_H
#ifndef CHARACTERVALIDATION_T4051914437_H
#define CHARACTERVALIDATION_T4051914437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/CharacterValidation
struct  CharacterValidation_t4051914437 
{
public:
	// System.Int32 UnityEngine.UI.InputField/CharacterValidation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CharacterValidation_t4051914437, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERVALIDATION_T4051914437_H
#ifndef CONTENTTYPE_T1787303396_H
#define CONTENTTYPE_T1787303396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField/ContentType
struct  ContentType_t1787303396 
{
public:
	// System.Int32 UnityEngine.UI.InputField/ContentType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ContentType_t1787303396, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T1787303396_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T1056001966_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef FUZZYSTATES_T3314355612_H
#define FUZZYSTATES_T3314355612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyFuzzyState/FuzzyStates
struct  FuzzyStates_t3314355612 
{
public:
	// System.Int32 EnemyFuzzyState/FuzzyStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FuzzyStates_t3314355612, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUZZYSTATES_T3314355612_H
#ifndef ANIMATIONCURVE_T3046754366_H
#define ANIMATIONCURVE_T3046754366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t3046754366  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t3046754366, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T3046754366_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef DEPTHTEXTUREMODE_T4161834719_H
#define DEPTHTEXTUREMODE_T4161834719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DepthTextureMode
struct  DepthTextureMode_t4161834719 
{
public:
	// System.Int32 UnityEngine.DepthTextureMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DepthTextureMode_t4161834719, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHTEXTUREMODE_T4161834719_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef AISTATES_T2852279327_H
#define AISTATES_T2852279327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AIStateMachine/AIStates
struct  AIStates_t2852279327 
{
public:
	// System.Int32 AIStateMachine/AIStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AIStates_t2852279327, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AISTATES_T2852279327_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T786576806_H
#define U3CU3EC__DISPLAYCLASS5_0_T786576806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PathRequestManager/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t786576806  : public RuntimeObject
{
public:
	// PathRequest PathRequestManager/<>c__DisplayClass5_0::request
	PathRequest_t2117613800  ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t786576806, ___request_0)); }
	inline PathRequest_t2117613800  get_request_0() const { return ___request_0; }
	inline PathRequest_t2117613800 * get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(PathRequest_t2117613800  value)
	{
		___request_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T786576806_H
#ifndef MESH_T3648964284_H
#define MESH_T3648964284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh
struct  Mesh_t3648964284  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T3648964284_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef AUDIOCLIP_T3680889665_H
#define AUDIOCLIP_T3680889665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioClip
struct  AudioClip_t3680889665  : public Object_t631007953
{
public:
	// UnityEngine.AudioClip/PCMReaderCallback UnityEngine.AudioClip::m_PCMReaderCallback
	PCMReaderCallback_t1677636661 * ___m_PCMReaderCallback_2;
	// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine.AudioClip::m_PCMSetPositionCallback
	PCMSetPositionCallback_t1059417452 * ___m_PCMSetPositionCallback_3;

public:
	inline static int32_t get_offset_of_m_PCMReaderCallback_2() { return static_cast<int32_t>(offsetof(AudioClip_t3680889665, ___m_PCMReaderCallback_2)); }
	inline PCMReaderCallback_t1677636661 * get_m_PCMReaderCallback_2() const { return ___m_PCMReaderCallback_2; }
	inline PCMReaderCallback_t1677636661 ** get_address_of_m_PCMReaderCallback_2() { return &___m_PCMReaderCallback_2; }
	inline void set_m_PCMReaderCallback_2(PCMReaderCallback_t1677636661 * value)
	{
		___m_PCMReaderCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMReaderCallback_2), value);
	}

	inline static int32_t get_offset_of_m_PCMSetPositionCallback_3() { return static_cast<int32_t>(offsetof(AudioClip_t3680889665, ___m_PCMSetPositionCallback_3)); }
	inline PCMSetPositionCallback_t1059417452 * get_m_PCMSetPositionCallback_3() const { return ___m_PCMSetPositionCallback_3; }
	inline PCMSetPositionCallback_t1059417452 ** get_address_of_m_PCMSetPositionCallback_3() { return &___m_PCMSetPositionCallback_3; }
	inline void set_m_PCMSetPositionCallback_3(PCMSetPositionCallback_t1059417452 * value)
	{
		___m_PCMSetPositionCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PCMSetPositionCallback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOCLIP_T3680889665_H
#ifndef SPRITE_T280657092_H
#define SPRITE_T280657092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t280657092  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T280657092_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MATERIAL_T340375123_H
#define MATERIAL_T340375123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t340375123  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T340375123_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef ACTION_1_T1700480823_H
#define ACTION_1_T1700480823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<PathResult>
struct  Action_1_t1700480823  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T1700480823_H
#ifndef ACTION_2_T1484661638_H
#define ACTION_2_T1484661638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`2<UnityEngine.Vector3[],System.Boolean>
struct  Action_2_t1484661638  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_2_T1484661638_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MESHFILTER_T3523625662_H
#define MESHFILTER_T3523625662_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshFilter
struct  MeshFilter_t3523625662  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHFILTER_T3523625662_H
#ifndef RIGIDBODY_T3916780224_H
#define RIGIDBODY_T3916780224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody
struct  Rigidbody_t3916780224  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY_T3916780224_H
#ifndef COLLIDER_T1773347010_H
#define COLLIDER_T1773347010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t1773347010  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T1773347010_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef THREADSTART_T1006689297_H
#define THREADSTART_T1006689297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadStart
struct  ThreadStart_t1006689297  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADSTART_T1006689297_H
#ifndef ITEM_T2953980098_H
#define ITEM_T2953980098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Item
struct  Item_t2953980098  : public ScriptableObject_t2528358522
{
public:
	// System.String Item::name
	String_t* ___name_2;
	// UnityEngine.Sprite Item::icon
	Sprite_t280657092 * ___icon_3;
	// System.Boolean Item::isWeapon
	bool ___isWeapon_4;
	// System.Boolean Item::isDefaultItem
	bool ___isDefaultItem_5;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Item_t2953980098, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_icon_3() { return static_cast<int32_t>(offsetof(Item_t2953980098, ___icon_3)); }
	inline Sprite_t280657092 * get_icon_3() const { return ___icon_3; }
	inline Sprite_t280657092 ** get_address_of_icon_3() { return &___icon_3; }
	inline void set_icon_3(Sprite_t280657092 * value)
	{
		___icon_3 = value;
		Il2CppCodeGenWriteBarrier((&___icon_3), value);
	}

	inline static int32_t get_offset_of_isWeapon_4() { return static_cast<int32_t>(offsetof(Item_t2953980098, ___isWeapon_4)); }
	inline bool get_isWeapon_4() const { return ___isWeapon_4; }
	inline bool* get_address_of_isWeapon_4() { return &___isWeapon_4; }
	inline void set_isWeapon_4(bool value)
	{
		___isWeapon_4 = value;
	}

	inline static int32_t get_offset_of_isDefaultItem_5() { return static_cast<int32_t>(offsetof(Item_t2953980098, ___isDefaultItem_5)); }
	inline bool get_isDefaultItem_5() const { return ___isDefaultItem_5; }
	inline bool* get_address_of_isDefaultItem_5() { return &___isDefaultItem_5; }
	inline void set_isDefaultItem_5(bool value)
	{
		___isDefaultItem_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEM_T2953980098_H
#ifndef ONITEMCHANGED_T22848112_H
#define ONITEMCHANGED_T22848112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Inventory/OnItemChanged
struct  OnItemChanged_t22848112  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONITEMCHANGED_T22848112_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef NAVMESHAGENT_T1276799816_H
#define NAVMESHAGENT_T1276799816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AI.NavMeshAgent
struct  NavMeshAgent_t1276799816  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAVMESHAGENT_T1276799816_H
#ifndef RECTTRANSFORM_T3704657025_H
#define RECTTRANSFORM_T3704657025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3704657025  : public Transform_t3600365921
{
public:

public:
};

struct RectTransform_t3704657025_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1258266594 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t3704657025_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t1258266594 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t1258266594 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3704657025_H
#ifndef ANIMATOR_T434523843_H
#define ANIMATOR_T434523843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_t434523843  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_T434523843_H
#ifndef BOXCOLLIDER_T1640800422_H
#define BOXCOLLIDER_T1640800422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.BoxCollider
struct  BoxCollider_t1640800422  : public Collider_t1773347010
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXCOLLIDER_T1640800422_H
#ifndef CAMERA_T4157153871_H
#define CAMERA_T4157153871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t4157153871  : public Behaviour_t1437897464
{
public:

public:
};

struct Camera_t4157153871_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t190067161 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t190067161 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t190067161 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t190067161 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t190067161 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t190067161 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t190067161 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t190067161 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t190067161 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t190067161 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t190067161 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t190067161 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4157153871_H
#ifndef AUDIOBEHAVIOUR_T2879336574_H
#define AUDIOBEHAVIOUR_T2879336574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioBehaviour
struct  AudioBehaviour_t2879336574  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOBEHAVIOUR_T2879336574_H
#ifndef ENEMYCOUNT_T3730988989_H
#define ENEMYCOUNT_T3730988989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyCount
struct  EnemyCount_t3730988989  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] EnemyCount::enemy
	GameObjectU5BU5D_t3328599146* ___enemy_2;
	// System.Int32 EnemyCount::enemyNumber
	int32_t ___enemyNumber_3;
	// LevelClear EnemyCount::levelClear
	LevelClear_t1949285084 * ___levelClear_4;

public:
	inline static int32_t get_offset_of_enemy_2() { return static_cast<int32_t>(offsetof(EnemyCount_t3730988989, ___enemy_2)); }
	inline GameObjectU5BU5D_t3328599146* get_enemy_2() const { return ___enemy_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_enemy_2() { return &___enemy_2; }
	inline void set_enemy_2(GameObjectU5BU5D_t3328599146* value)
	{
		___enemy_2 = value;
		Il2CppCodeGenWriteBarrier((&___enemy_2), value);
	}

	inline static int32_t get_offset_of_enemyNumber_3() { return static_cast<int32_t>(offsetof(EnemyCount_t3730988989, ___enemyNumber_3)); }
	inline int32_t get_enemyNumber_3() const { return ___enemyNumber_3; }
	inline int32_t* get_address_of_enemyNumber_3() { return &___enemyNumber_3; }
	inline void set_enemyNumber_3(int32_t value)
	{
		___enemyNumber_3 = value;
	}

	inline static int32_t get_offset_of_levelClear_4() { return static_cast<int32_t>(offsetof(EnemyCount_t3730988989, ___levelClear_4)); }
	inline LevelClear_t1949285084 * get_levelClear_4() const { return ___levelClear_4; }
	inline LevelClear_t1949285084 ** get_address_of_levelClear_4() { return &___levelClear_4; }
	inline void set_levelClear_4(LevelClear_t1949285084 * value)
	{
		___levelClear_4 = value;
		Il2CppCodeGenWriteBarrier((&___levelClear_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYCOUNT_T3730988989_H
#ifndef UNIT_T4139495810_H
#define UNIT_T4139495810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unit
struct  Unit_t4139495810  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform Unit::target
	Transform_t3600365921 * ___target_4;
	// System.Single Unit::speed
	float ___speed_5;
	// System.Single Unit::turnSpeed
	float ___turnSpeed_6;
	// System.Single Unit::turnDst
	float ___turnDst_7;
	// System.Single Unit::stoppingDst
	float ___stoppingDst_8;
	// Path Unit::path
	Path_t2615110272 * ___path_9;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(Unit_t4139495810, ___target_4)); }
	inline Transform_t3600365921 * get_target_4() const { return ___target_4; }
	inline Transform_t3600365921 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3600365921 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(Unit_t4139495810, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_turnSpeed_6() { return static_cast<int32_t>(offsetof(Unit_t4139495810, ___turnSpeed_6)); }
	inline float get_turnSpeed_6() const { return ___turnSpeed_6; }
	inline float* get_address_of_turnSpeed_6() { return &___turnSpeed_6; }
	inline void set_turnSpeed_6(float value)
	{
		___turnSpeed_6 = value;
	}

	inline static int32_t get_offset_of_turnDst_7() { return static_cast<int32_t>(offsetof(Unit_t4139495810, ___turnDst_7)); }
	inline float get_turnDst_7() const { return ___turnDst_7; }
	inline float* get_address_of_turnDst_7() { return &___turnDst_7; }
	inline void set_turnDst_7(float value)
	{
		___turnDst_7 = value;
	}

	inline static int32_t get_offset_of_stoppingDst_8() { return static_cast<int32_t>(offsetof(Unit_t4139495810, ___stoppingDst_8)); }
	inline float get_stoppingDst_8() const { return ___stoppingDst_8; }
	inline float* get_address_of_stoppingDst_8() { return &___stoppingDst_8; }
	inline void set_stoppingDst_8(float value)
	{
		___stoppingDst_8 = value;
	}

	inline static int32_t get_offset_of_path_9() { return static_cast<int32_t>(offsetof(Unit_t4139495810, ___path_9)); }
	inline Path_t2615110272 * get_path_9() const { return ___path_9; }
	inline Path_t2615110272 ** get_address_of_path_9() { return &___path_9; }
	inline void set_path_9(Path_t2615110272 * value)
	{
		___path_9 = value;
		Il2CppCodeGenWriteBarrier((&___path_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIT_T4139495810_H
#ifndef ENEMYSTATS_T4187841152_H
#define ENEMYSTATS_T4187841152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyStats
struct  EnemyStats_t4187841152  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 EnemyStats::eCurrentHealth
	int32_t ___eCurrentHealth_2;
	// System.Int32 EnemyStats::eMaximumHealth
	int32_t ___eMaximumHealth_3;
	// EnemyCount EnemyStats::eCount
	EnemyCount_t3730988989 * ___eCount_4;

public:
	inline static int32_t get_offset_of_eCurrentHealth_2() { return static_cast<int32_t>(offsetof(EnemyStats_t4187841152, ___eCurrentHealth_2)); }
	inline int32_t get_eCurrentHealth_2() const { return ___eCurrentHealth_2; }
	inline int32_t* get_address_of_eCurrentHealth_2() { return &___eCurrentHealth_2; }
	inline void set_eCurrentHealth_2(int32_t value)
	{
		___eCurrentHealth_2 = value;
	}

	inline static int32_t get_offset_of_eMaximumHealth_3() { return static_cast<int32_t>(offsetof(EnemyStats_t4187841152, ___eMaximumHealth_3)); }
	inline int32_t get_eMaximumHealth_3() const { return ___eMaximumHealth_3; }
	inline int32_t* get_address_of_eMaximumHealth_3() { return &___eMaximumHealth_3; }
	inline void set_eMaximumHealth_3(int32_t value)
	{
		___eMaximumHealth_3 = value;
	}

	inline static int32_t get_offset_of_eCount_4() { return static_cast<int32_t>(offsetof(EnemyStats_t4187841152, ___eCount_4)); }
	inline EnemyCount_t3730988989 * get_eCount_4() const { return ___eCount_4; }
	inline EnemyCount_t3730988989 ** get_address_of_eCount_4() { return &___eCount_4; }
	inline void set_eCount_4(EnemyCount_t3730988989 * value)
	{
		___eCount_4 = value;
		Il2CppCodeGenWriteBarrier((&___eCount_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYSTATS_T4187841152_H
#ifndef BULLET_T1042140031_H
#define BULLET_T1042140031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bullet
struct  Bullet_t1042140031  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Bullet::lifeTime
	float ___lifeTime_2;
	// UnityEngine.Material Bullet::material
	Material_t340375123 * ___material_3;
	// UnityEngine.Color Bullet::originalCol
	Color_t2555686324  ___originalCol_4;
	// System.Single Bullet::fadePercent
	float ___fadePercent_5;
	// System.Single Bullet::disposeTime
	float ___disposeTime_6;
	// System.Boolean Bullet::fading
	bool ___fading_7;

public:
	inline static int32_t get_offset_of_lifeTime_2() { return static_cast<int32_t>(offsetof(Bullet_t1042140031, ___lifeTime_2)); }
	inline float get_lifeTime_2() const { return ___lifeTime_2; }
	inline float* get_address_of_lifeTime_2() { return &___lifeTime_2; }
	inline void set_lifeTime_2(float value)
	{
		___lifeTime_2 = value;
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(Bullet_t1042140031, ___material_3)); }
	inline Material_t340375123 * get_material_3() const { return ___material_3; }
	inline Material_t340375123 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t340375123 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_originalCol_4() { return static_cast<int32_t>(offsetof(Bullet_t1042140031, ___originalCol_4)); }
	inline Color_t2555686324  get_originalCol_4() const { return ___originalCol_4; }
	inline Color_t2555686324 * get_address_of_originalCol_4() { return &___originalCol_4; }
	inline void set_originalCol_4(Color_t2555686324  value)
	{
		___originalCol_4 = value;
	}

	inline static int32_t get_offset_of_fadePercent_5() { return static_cast<int32_t>(offsetof(Bullet_t1042140031, ___fadePercent_5)); }
	inline float get_fadePercent_5() const { return ___fadePercent_5; }
	inline float* get_address_of_fadePercent_5() { return &___fadePercent_5; }
	inline void set_fadePercent_5(float value)
	{
		___fadePercent_5 = value;
	}

	inline static int32_t get_offset_of_disposeTime_6() { return static_cast<int32_t>(offsetof(Bullet_t1042140031, ___disposeTime_6)); }
	inline float get_disposeTime_6() const { return ___disposeTime_6; }
	inline float* get_address_of_disposeTime_6() { return &___disposeTime_6; }
	inline void set_disposeTime_6(float value)
	{
		___disposeTime_6 = value;
	}

	inline static int32_t get_offset_of_fading_7() { return static_cast<int32_t>(offsetof(Bullet_t1042140031, ___fading_7)); }
	inline bool get_fading_7() const { return ___fading_7; }
	inline bool* get_address_of_fading_7() { return &___fading_7; }
	inline void set_fading_7(bool value)
	{
		___fading_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BULLET_T1042140031_H
#ifndef HEALINGSPOT_T2443304191_H
#define HEALINGSPOT_T2443304191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HealingSpot
struct  HealingSpot_t2443304191  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 HealingSpot::positionsIndex
	int32_t ___positionsIndex_2;
	// UnityEngine.Vector3[] HealingSpot::positions
	Vector3U5BU5D_t1718750761* ___positions_3;

public:
	inline static int32_t get_offset_of_positionsIndex_2() { return static_cast<int32_t>(offsetof(HealingSpot_t2443304191, ___positionsIndex_2)); }
	inline int32_t get_positionsIndex_2() const { return ___positionsIndex_2; }
	inline int32_t* get_address_of_positionsIndex_2() { return &___positionsIndex_2; }
	inline void set_positionsIndex_2(int32_t value)
	{
		___positionsIndex_2 = value;
	}

	inline static int32_t get_offset_of_positions_3() { return static_cast<int32_t>(offsetof(HealingSpot_t2443304191, ___positions_3)); }
	inline Vector3U5BU5D_t1718750761* get_positions_3() const { return ___positions_3; }
	inline Vector3U5BU5D_t1718750761** get_address_of_positions_3() { return &___positions_3; }
	inline void set_positions_3(Vector3U5BU5D_t1718750761* value)
	{
		___positions_3 = value;
		Il2CppCodeGenWriteBarrier((&___positions_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEALINGSPOT_T2443304191_H
#ifndef PLAYERSTATS_T2044123780_H
#define PLAYERSTATS_T2044123780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerStats
struct  PlayerStats_t2044123780  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 PlayerStats::pCurrentHealth
	int32_t ___pCurrentHealth_2;
	// System.Int32 PlayerStats::pMaximumHealth
	int32_t ___pMaximumHealth_3;
	// System.String PlayerStats::gameOverScene
	String_t* ___gameOverScene_4;

public:
	inline static int32_t get_offset_of_pCurrentHealth_2() { return static_cast<int32_t>(offsetof(PlayerStats_t2044123780, ___pCurrentHealth_2)); }
	inline int32_t get_pCurrentHealth_2() const { return ___pCurrentHealth_2; }
	inline int32_t* get_address_of_pCurrentHealth_2() { return &___pCurrentHealth_2; }
	inline void set_pCurrentHealth_2(int32_t value)
	{
		___pCurrentHealth_2 = value;
	}

	inline static int32_t get_offset_of_pMaximumHealth_3() { return static_cast<int32_t>(offsetof(PlayerStats_t2044123780, ___pMaximumHealth_3)); }
	inline int32_t get_pMaximumHealth_3() const { return ___pMaximumHealth_3; }
	inline int32_t* get_address_of_pMaximumHealth_3() { return &___pMaximumHealth_3; }
	inline void set_pMaximumHealth_3(int32_t value)
	{
		___pMaximumHealth_3 = value;
	}

	inline static int32_t get_offset_of_gameOverScene_4() { return static_cast<int32_t>(offsetof(PlayerStats_t2044123780, ___gameOverScene_4)); }
	inline String_t* get_gameOverScene_4() const { return ___gameOverScene_4; }
	inline String_t** get_address_of_gameOverScene_4() { return &___gameOverScene_4; }
	inline void set_gameOverScene_4(String_t* value)
	{
		___gameOverScene_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverScene_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSTATS_T2044123780_H
#ifndef ENEMYSHOOT_T243830779_H
#define ENEMYSHOOT_T243830779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyShoot
struct  EnemyShoot_t243830779  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioClip EnemyShoot::gunShot
	AudioClip_t3680889665 * ___gunShot_2;
	// UnityEngine.AudioClip EnemyShoot::laserShot
	AudioClip_t3680889665 * ___laserShot_3;
	// UnityEngine.GameObject EnemyShoot::bullet
	GameObject_t1113636619 * ___bullet_4;
	// UnityEngine.GameObject EnemyShoot::laser
	GameObject_t1113636619 * ___laser_5;
	// UnityEngine.Transform EnemyShoot::spawn
	Transform_t3600365921 * ___spawn_6;
	// System.Single EnemyShoot::bulletSpeed
	float ___bulletSpeed_7;
	// System.Single EnemyShoot::cooldownTime
	float ___cooldownTime_8;
	// System.Boolean EnemyShoot::firedGun
	bool ___firedGun_9;
	// System.Single EnemyShoot::cooldownTimeMax
	float ___cooldownTimeMax_10;

public:
	inline static int32_t get_offset_of_gunShot_2() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___gunShot_2)); }
	inline AudioClip_t3680889665 * get_gunShot_2() const { return ___gunShot_2; }
	inline AudioClip_t3680889665 ** get_address_of_gunShot_2() { return &___gunShot_2; }
	inline void set_gunShot_2(AudioClip_t3680889665 * value)
	{
		___gunShot_2 = value;
		Il2CppCodeGenWriteBarrier((&___gunShot_2), value);
	}

	inline static int32_t get_offset_of_laserShot_3() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___laserShot_3)); }
	inline AudioClip_t3680889665 * get_laserShot_3() const { return ___laserShot_3; }
	inline AudioClip_t3680889665 ** get_address_of_laserShot_3() { return &___laserShot_3; }
	inline void set_laserShot_3(AudioClip_t3680889665 * value)
	{
		___laserShot_3 = value;
		Il2CppCodeGenWriteBarrier((&___laserShot_3), value);
	}

	inline static int32_t get_offset_of_bullet_4() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___bullet_4)); }
	inline GameObject_t1113636619 * get_bullet_4() const { return ___bullet_4; }
	inline GameObject_t1113636619 ** get_address_of_bullet_4() { return &___bullet_4; }
	inline void set_bullet_4(GameObject_t1113636619 * value)
	{
		___bullet_4 = value;
		Il2CppCodeGenWriteBarrier((&___bullet_4), value);
	}

	inline static int32_t get_offset_of_laser_5() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___laser_5)); }
	inline GameObject_t1113636619 * get_laser_5() const { return ___laser_5; }
	inline GameObject_t1113636619 ** get_address_of_laser_5() { return &___laser_5; }
	inline void set_laser_5(GameObject_t1113636619 * value)
	{
		___laser_5 = value;
		Il2CppCodeGenWriteBarrier((&___laser_5), value);
	}

	inline static int32_t get_offset_of_spawn_6() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___spawn_6)); }
	inline Transform_t3600365921 * get_spawn_6() const { return ___spawn_6; }
	inline Transform_t3600365921 ** get_address_of_spawn_6() { return &___spawn_6; }
	inline void set_spawn_6(Transform_t3600365921 * value)
	{
		___spawn_6 = value;
		Il2CppCodeGenWriteBarrier((&___spawn_6), value);
	}

	inline static int32_t get_offset_of_bulletSpeed_7() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___bulletSpeed_7)); }
	inline float get_bulletSpeed_7() const { return ___bulletSpeed_7; }
	inline float* get_address_of_bulletSpeed_7() { return &___bulletSpeed_7; }
	inline void set_bulletSpeed_7(float value)
	{
		___bulletSpeed_7 = value;
	}

	inline static int32_t get_offset_of_cooldownTime_8() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___cooldownTime_8)); }
	inline float get_cooldownTime_8() const { return ___cooldownTime_8; }
	inline float* get_address_of_cooldownTime_8() { return &___cooldownTime_8; }
	inline void set_cooldownTime_8(float value)
	{
		___cooldownTime_8 = value;
	}

	inline static int32_t get_offset_of_firedGun_9() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___firedGun_9)); }
	inline bool get_firedGun_9() const { return ___firedGun_9; }
	inline bool* get_address_of_firedGun_9() { return &___firedGun_9; }
	inline void set_firedGun_9(bool value)
	{
		___firedGun_9 = value;
	}

	inline static int32_t get_offset_of_cooldownTimeMax_10() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___cooldownTimeMax_10)); }
	inline float get_cooldownTimeMax_10() const { return ___cooldownTimeMax_10; }
	inline float* get_address_of_cooldownTimeMax_10() { return &___cooldownTimeMax_10; }
	inline void set_cooldownTimeMax_10(float value)
	{
		___cooldownTimeMax_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYSHOOT_T243830779_H
#ifndef BOSSUI_T1795207708_H
#define BOSSUI_T1795207708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BossUI
struct  BossUI_t1795207708  : public MonoBehaviour_t3962482529
{
public:
	// EnemyStats BossUI::enemyTank
	EnemyStats_t4187841152 * ___enemyTank_2;
	// UnityEngine.UI.Image BossUI::enemyHealthBar
	Image_t2670269651 * ___enemyHealthBar_3;
	// System.Single BossUI::enemyHealthBarValue
	float ___enemyHealthBarValue_4;
	// PlayerStats BossUI::playerTank
	PlayerStats_t2044123780 * ___playerTank_5;
	// UnityEngine.UI.Text BossUI::playerHealthText
	Text_t1901882714 * ___playerHealthText_6;

public:
	inline static int32_t get_offset_of_enemyTank_2() { return static_cast<int32_t>(offsetof(BossUI_t1795207708, ___enemyTank_2)); }
	inline EnemyStats_t4187841152 * get_enemyTank_2() const { return ___enemyTank_2; }
	inline EnemyStats_t4187841152 ** get_address_of_enemyTank_2() { return &___enemyTank_2; }
	inline void set_enemyTank_2(EnemyStats_t4187841152 * value)
	{
		___enemyTank_2 = value;
		Il2CppCodeGenWriteBarrier((&___enemyTank_2), value);
	}

	inline static int32_t get_offset_of_enemyHealthBar_3() { return static_cast<int32_t>(offsetof(BossUI_t1795207708, ___enemyHealthBar_3)); }
	inline Image_t2670269651 * get_enemyHealthBar_3() const { return ___enemyHealthBar_3; }
	inline Image_t2670269651 ** get_address_of_enemyHealthBar_3() { return &___enemyHealthBar_3; }
	inline void set_enemyHealthBar_3(Image_t2670269651 * value)
	{
		___enemyHealthBar_3 = value;
		Il2CppCodeGenWriteBarrier((&___enemyHealthBar_3), value);
	}

	inline static int32_t get_offset_of_enemyHealthBarValue_4() { return static_cast<int32_t>(offsetof(BossUI_t1795207708, ___enemyHealthBarValue_4)); }
	inline float get_enemyHealthBarValue_4() const { return ___enemyHealthBarValue_4; }
	inline float* get_address_of_enemyHealthBarValue_4() { return &___enemyHealthBarValue_4; }
	inline void set_enemyHealthBarValue_4(float value)
	{
		___enemyHealthBarValue_4 = value;
	}

	inline static int32_t get_offset_of_playerTank_5() { return static_cast<int32_t>(offsetof(BossUI_t1795207708, ___playerTank_5)); }
	inline PlayerStats_t2044123780 * get_playerTank_5() const { return ___playerTank_5; }
	inline PlayerStats_t2044123780 ** get_address_of_playerTank_5() { return &___playerTank_5; }
	inline void set_playerTank_5(PlayerStats_t2044123780 * value)
	{
		___playerTank_5 = value;
		Il2CppCodeGenWriteBarrier((&___playerTank_5), value);
	}

	inline static int32_t get_offset_of_playerHealthText_6() { return static_cast<int32_t>(offsetof(BossUI_t1795207708, ___playerHealthText_6)); }
	inline Text_t1901882714 * get_playerHealthText_6() const { return ___playerHealthText_6; }
	inline Text_t1901882714 ** get_address_of_playerHealthText_6() { return &___playerHealthText_6; }
	inline void set_playerHealthText_6(Text_t1901882714 * value)
	{
		___playerHealthText_6 = value;
		Il2CppCodeGenWriteBarrier((&___playerHealthText_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOSSUI_T1795207708_H
#ifndef LEVELCLEAR_T1949285084_H
#define LEVELCLEAR_T1949285084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelClear
struct  LevelClear_t1949285084  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator LevelClear::animator
	Animator_t434523843 * ___animator_2;
	// LevelChanger LevelClear::levelChanger
	LevelChanger_t225386971 * ___levelChanger_3;
	// System.String LevelClear::levelNameToLoad
	String_t* ___levelNameToLoad_4;
	// System.Int32 LevelClear::levelIndexToLoad
	int32_t ___levelIndexToLoad_5;
	// System.Boolean LevelClear::loadByName
	bool ___loadByName_6;

public:
	inline static int32_t get_offset_of_animator_2() { return static_cast<int32_t>(offsetof(LevelClear_t1949285084, ___animator_2)); }
	inline Animator_t434523843 * get_animator_2() const { return ___animator_2; }
	inline Animator_t434523843 ** get_address_of_animator_2() { return &___animator_2; }
	inline void set_animator_2(Animator_t434523843 * value)
	{
		___animator_2 = value;
		Il2CppCodeGenWriteBarrier((&___animator_2), value);
	}

	inline static int32_t get_offset_of_levelChanger_3() { return static_cast<int32_t>(offsetof(LevelClear_t1949285084, ___levelChanger_3)); }
	inline LevelChanger_t225386971 * get_levelChanger_3() const { return ___levelChanger_3; }
	inline LevelChanger_t225386971 ** get_address_of_levelChanger_3() { return &___levelChanger_3; }
	inline void set_levelChanger_3(LevelChanger_t225386971 * value)
	{
		___levelChanger_3 = value;
		Il2CppCodeGenWriteBarrier((&___levelChanger_3), value);
	}

	inline static int32_t get_offset_of_levelNameToLoad_4() { return static_cast<int32_t>(offsetof(LevelClear_t1949285084, ___levelNameToLoad_4)); }
	inline String_t* get_levelNameToLoad_4() const { return ___levelNameToLoad_4; }
	inline String_t** get_address_of_levelNameToLoad_4() { return &___levelNameToLoad_4; }
	inline void set_levelNameToLoad_4(String_t* value)
	{
		___levelNameToLoad_4 = value;
		Il2CppCodeGenWriteBarrier((&___levelNameToLoad_4), value);
	}

	inline static int32_t get_offset_of_levelIndexToLoad_5() { return static_cast<int32_t>(offsetof(LevelClear_t1949285084, ___levelIndexToLoad_5)); }
	inline int32_t get_levelIndexToLoad_5() const { return ___levelIndexToLoad_5; }
	inline int32_t* get_address_of_levelIndexToLoad_5() { return &___levelIndexToLoad_5; }
	inline void set_levelIndexToLoad_5(int32_t value)
	{
		___levelIndexToLoad_5 = value;
	}

	inline static int32_t get_offset_of_loadByName_6() { return static_cast<int32_t>(offsetof(LevelClear_t1949285084, ___loadByName_6)); }
	inline bool get_loadByName_6() const { return ___loadByName_6; }
	inline bool* get_address_of_loadByName_6() { return &___loadByName_6; }
	inline void set_loadByName_6(bool value)
	{
		___loadByName_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCLEAR_T1949285084_H
#ifndef GAMEOVERSCREEN_T2285777029_H
#define GAMEOVERSCREEN_T2285777029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameOverScreen
struct  GameOverScreen_t2285777029  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 GameOverScreen::index
	int32_t ___index_2;
	// UnityEngine.AudioClip GameOverScreen::selectSound
	AudioClip_t3680889665 * ___selectSound_3;
	// UnityEngine.AudioClip GameOverScreen::confirmSound
	AudioClip_t3680889665 * ___confirmSound_4;
	// UnityEngine.GameObject[] GameOverScreen::options
	GameObjectU5BU5D_t3328599146* ___options_5;
	// UnityEngine.Sprite[] GameOverScreen::optionBox
	SpriteU5BU5D_t2581906349* ___optionBox_6;
	// LevelChanger GameOverScreen::levelChanger
	LevelChanger_t225386971 * ___levelChanger_7;
	// System.Boolean GameOverScreen::leftAxesInUse
	bool ___leftAxesInUse_8;
	// System.Boolean GameOverScreen::rightAxesInUse
	bool ___rightAxesInUse_9;
	// System.Boolean GameOverScreen::confirmAxesInUse
	bool ___confirmAxesInUse_10;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_selectSound_3() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___selectSound_3)); }
	inline AudioClip_t3680889665 * get_selectSound_3() const { return ___selectSound_3; }
	inline AudioClip_t3680889665 ** get_address_of_selectSound_3() { return &___selectSound_3; }
	inline void set_selectSound_3(AudioClip_t3680889665 * value)
	{
		___selectSound_3 = value;
		Il2CppCodeGenWriteBarrier((&___selectSound_3), value);
	}

	inline static int32_t get_offset_of_confirmSound_4() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___confirmSound_4)); }
	inline AudioClip_t3680889665 * get_confirmSound_4() const { return ___confirmSound_4; }
	inline AudioClip_t3680889665 ** get_address_of_confirmSound_4() { return &___confirmSound_4; }
	inline void set_confirmSound_4(AudioClip_t3680889665 * value)
	{
		___confirmSound_4 = value;
		Il2CppCodeGenWriteBarrier((&___confirmSound_4), value);
	}

	inline static int32_t get_offset_of_options_5() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___options_5)); }
	inline GameObjectU5BU5D_t3328599146* get_options_5() const { return ___options_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_options_5() { return &___options_5; }
	inline void set_options_5(GameObjectU5BU5D_t3328599146* value)
	{
		___options_5 = value;
		Il2CppCodeGenWriteBarrier((&___options_5), value);
	}

	inline static int32_t get_offset_of_optionBox_6() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___optionBox_6)); }
	inline SpriteU5BU5D_t2581906349* get_optionBox_6() const { return ___optionBox_6; }
	inline SpriteU5BU5D_t2581906349** get_address_of_optionBox_6() { return &___optionBox_6; }
	inline void set_optionBox_6(SpriteU5BU5D_t2581906349* value)
	{
		___optionBox_6 = value;
		Il2CppCodeGenWriteBarrier((&___optionBox_6), value);
	}

	inline static int32_t get_offset_of_levelChanger_7() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___levelChanger_7)); }
	inline LevelChanger_t225386971 * get_levelChanger_7() const { return ___levelChanger_7; }
	inline LevelChanger_t225386971 ** get_address_of_levelChanger_7() { return &___levelChanger_7; }
	inline void set_levelChanger_7(LevelChanger_t225386971 * value)
	{
		___levelChanger_7 = value;
		Il2CppCodeGenWriteBarrier((&___levelChanger_7), value);
	}

	inline static int32_t get_offset_of_leftAxesInUse_8() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___leftAxesInUse_8)); }
	inline bool get_leftAxesInUse_8() const { return ___leftAxesInUse_8; }
	inline bool* get_address_of_leftAxesInUse_8() { return &___leftAxesInUse_8; }
	inline void set_leftAxesInUse_8(bool value)
	{
		___leftAxesInUse_8 = value;
	}

	inline static int32_t get_offset_of_rightAxesInUse_9() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___rightAxesInUse_9)); }
	inline bool get_rightAxesInUse_9() const { return ___rightAxesInUse_9; }
	inline bool* get_address_of_rightAxesInUse_9() { return &___rightAxesInUse_9; }
	inline void set_rightAxesInUse_9(bool value)
	{
		___rightAxesInUse_9 = value;
	}

	inline static int32_t get_offset_of_confirmAxesInUse_10() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___confirmAxesInUse_10)); }
	inline bool get_confirmAxesInUse_10() const { return ___confirmAxesInUse_10; }
	inline bool* get_address_of_confirmAxesInUse_10() { return &___confirmAxesInUse_10; }
	inline void set_confirmAxesInUse_10(bool value)
	{
		___confirmAxesInUse_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOVERSCREEN_T2285777029_H
#ifndef BOSSTANKCONTROLLER_T1902331492_H
#define BOSSTANKCONTROLLER_T1902331492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BossTankController
struct  BossTankController_t1902331492  : public MonoBehaviour_t3962482529
{
public:
	// EnemyStats BossTankController::eStats
	EnemyStats_t4187841152 * ___eStats_2;
	// EnemyShoot BossTankController::eShoot
	EnemyShoot_t243830779 * ___eShoot_3;
	// Unit BossTankController::unit
	Unit_t4139495810 * ___unit_4;
	// UnityEngine.Transform BossTankController::player
	Transform_t3600365921 * ___player_5;
	// UnityEngine.Transform BossTankController::healingSpot
	Transform_t3600365921 * ___healingSpot_6;
	// UnityEngine.GameObject BossTankController::bullet
	GameObject_t1113636619 * ___bullet_7;
	// UnityEngine.GameObject BossTankController::laser
	GameObject_t1113636619 * ___laser_8;
	// UnityEngine.GameObject BossTankController::centerPoint
	GameObject_t1113636619 * ___centerPoint_9;

public:
	inline static int32_t get_offset_of_eStats_2() { return static_cast<int32_t>(offsetof(BossTankController_t1902331492, ___eStats_2)); }
	inline EnemyStats_t4187841152 * get_eStats_2() const { return ___eStats_2; }
	inline EnemyStats_t4187841152 ** get_address_of_eStats_2() { return &___eStats_2; }
	inline void set_eStats_2(EnemyStats_t4187841152 * value)
	{
		___eStats_2 = value;
		Il2CppCodeGenWriteBarrier((&___eStats_2), value);
	}

	inline static int32_t get_offset_of_eShoot_3() { return static_cast<int32_t>(offsetof(BossTankController_t1902331492, ___eShoot_3)); }
	inline EnemyShoot_t243830779 * get_eShoot_3() const { return ___eShoot_3; }
	inline EnemyShoot_t243830779 ** get_address_of_eShoot_3() { return &___eShoot_3; }
	inline void set_eShoot_3(EnemyShoot_t243830779 * value)
	{
		___eShoot_3 = value;
		Il2CppCodeGenWriteBarrier((&___eShoot_3), value);
	}

	inline static int32_t get_offset_of_unit_4() { return static_cast<int32_t>(offsetof(BossTankController_t1902331492, ___unit_4)); }
	inline Unit_t4139495810 * get_unit_4() const { return ___unit_4; }
	inline Unit_t4139495810 ** get_address_of_unit_4() { return &___unit_4; }
	inline void set_unit_4(Unit_t4139495810 * value)
	{
		___unit_4 = value;
		Il2CppCodeGenWriteBarrier((&___unit_4), value);
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(BossTankController_t1902331492, ___player_5)); }
	inline Transform_t3600365921 * get_player_5() const { return ___player_5; }
	inline Transform_t3600365921 ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(Transform_t3600365921 * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier((&___player_5), value);
	}

	inline static int32_t get_offset_of_healingSpot_6() { return static_cast<int32_t>(offsetof(BossTankController_t1902331492, ___healingSpot_6)); }
	inline Transform_t3600365921 * get_healingSpot_6() const { return ___healingSpot_6; }
	inline Transform_t3600365921 ** get_address_of_healingSpot_6() { return &___healingSpot_6; }
	inline void set_healingSpot_6(Transform_t3600365921 * value)
	{
		___healingSpot_6 = value;
		Il2CppCodeGenWriteBarrier((&___healingSpot_6), value);
	}

	inline static int32_t get_offset_of_bullet_7() { return static_cast<int32_t>(offsetof(BossTankController_t1902331492, ___bullet_7)); }
	inline GameObject_t1113636619 * get_bullet_7() const { return ___bullet_7; }
	inline GameObject_t1113636619 ** get_address_of_bullet_7() { return &___bullet_7; }
	inline void set_bullet_7(GameObject_t1113636619 * value)
	{
		___bullet_7 = value;
		Il2CppCodeGenWriteBarrier((&___bullet_7), value);
	}

	inline static int32_t get_offset_of_laser_8() { return static_cast<int32_t>(offsetof(BossTankController_t1902331492, ___laser_8)); }
	inline GameObject_t1113636619 * get_laser_8() const { return ___laser_8; }
	inline GameObject_t1113636619 ** get_address_of_laser_8() { return &___laser_8; }
	inline void set_laser_8(GameObject_t1113636619 * value)
	{
		___laser_8 = value;
		Il2CppCodeGenWriteBarrier((&___laser_8), value);
	}

	inline static int32_t get_offset_of_centerPoint_9() { return static_cast<int32_t>(offsetof(BossTankController_t1902331492, ___centerPoint_9)); }
	inline GameObject_t1113636619 * get_centerPoint_9() const { return ___centerPoint_9; }
	inline GameObject_t1113636619 ** get_address_of_centerPoint_9() { return &___centerPoint_9; }
	inline void set_centerPoint_9(GameObject_t1113636619 * value)
	{
		___centerPoint_9 = value;
		Il2CppCodeGenWriteBarrier((&___centerPoint_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOSSTANKCONTROLLER_T1902331492_H
#ifndef LEVELCHANGER_T225386971_H
#define LEVELCHANGER_T225386971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelChanger
struct  LevelChanger_t225386971  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator LevelChanger::animator
	Animator_t434523843 * ___animator_2;
	// System.String LevelChanger::levelNameToLoad
	String_t* ___levelNameToLoad_3;
	// System.Int32 LevelChanger::levelIndexToLoad
	int32_t ___levelIndexToLoad_4;
	// System.Boolean LevelChanger::loadByName
	bool ___loadByName_5;

public:
	inline static int32_t get_offset_of_animator_2() { return static_cast<int32_t>(offsetof(LevelChanger_t225386971, ___animator_2)); }
	inline Animator_t434523843 * get_animator_2() const { return ___animator_2; }
	inline Animator_t434523843 ** get_address_of_animator_2() { return &___animator_2; }
	inline void set_animator_2(Animator_t434523843 * value)
	{
		___animator_2 = value;
		Il2CppCodeGenWriteBarrier((&___animator_2), value);
	}

	inline static int32_t get_offset_of_levelNameToLoad_3() { return static_cast<int32_t>(offsetof(LevelChanger_t225386971, ___levelNameToLoad_3)); }
	inline String_t* get_levelNameToLoad_3() const { return ___levelNameToLoad_3; }
	inline String_t** get_address_of_levelNameToLoad_3() { return &___levelNameToLoad_3; }
	inline void set_levelNameToLoad_3(String_t* value)
	{
		___levelNameToLoad_3 = value;
		Il2CppCodeGenWriteBarrier((&___levelNameToLoad_3), value);
	}

	inline static int32_t get_offset_of_levelIndexToLoad_4() { return static_cast<int32_t>(offsetof(LevelChanger_t225386971, ___levelIndexToLoad_4)); }
	inline int32_t get_levelIndexToLoad_4() const { return ___levelIndexToLoad_4; }
	inline int32_t* get_address_of_levelIndexToLoad_4() { return &___levelIndexToLoad_4; }
	inline void set_levelIndexToLoad_4(int32_t value)
	{
		___levelIndexToLoad_4 = value;
	}

	inline static int32_t get_offset_of_loadByName_5() { return static_cast<int32_t>(offsetof(LevelChanger_t225386971, ___loadByName_5)); }
	inline bool get_loadByName_5() const { return ___loadByName_5; }
	inline bool* get_address_of_loadByName_5() { return &___loadByName_5; }
	inline void set_loadByName_5(bool value)
	{
		___loadByName_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCHANGER_T225386971_H
#ifndef PATHREQUESTMANAGER_T4163282249_H
#define PATHREQUESTMANAGER_T4163282249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PathRequestManager
struct  PathRequestManager_t4163282249  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.Queue`1<PathResult> PathRequestManager::results
	Queue_1_t1374272722 * ___results_2;
	// Pathfinding PathRequestManager::pathfinding
	Pathfinding_t1696161914 * ___pathfinding_4;

public:
	inline static int32_t get_offset_of_results_2() { return static_cast<int32_t>(offsetof(PathRequestManager_t4163282249, ___results_2)); }
	inline Queue_1_t1374272722 * get_results_2() const { return ___results_2; }
	inline Queue_1_t1374272722 ** get_address_of_results_2() { return &___results_2; }
	inline void set_results_2(Queue_1_t1374272722 * value)
	{
		___results_2 = value;
		Il2CppCodeGenWriteBarrier((&___results_2), value);
	}

	inline static int32_t get_offset_of_pathfinding_4() { return static_cast<int32_t>(offsetof(PathRequestManager_t4163282249, ___pathfinding_4)); }
	inline Pathfinding_t1696161914 * get_pathfinding_4() const { return ___pathfinding_4; }
	inline Pathfinding_t1696161914 ** get_address_of_pathfinding_4() { return &___pathfinding_4; }
	inline void set_pathfinding_4(Pathfinding_t1696161914 * value)
	{
		___pathfinding_4 = value;
		Il2CppCodeGenWriteBarrier((&___pathfinding_4), value);
	}
};

struct PathRequestManager_t4163282249_StaticFields
{
public:
	// PathRequestManager PathRequestManager::instance
	PathRequestManager_t4163282249 * ___instance_3;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(PathRequestManager_t4163282249_StaticFields, ___instance_3)); }
	inline PathRequestManager_t4163282249 * get_instance_3() const { return ___instance_3; }
	inline PathRequestManager_t4163282249 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(PathRequestManager_t4163282249 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHREQUESTMANAGER_T4163282249_H
#ifndef AISTATEMACHINE_T2425577084_H
#define AISTATEMACHINE_T2425577084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AIStateMachine
struct  AIStateMachine_t2425577084  : public MonoBehaviour_t3962482529
{
public:
	// BossTankController AIStateMachine::controller
	BossTankController_t1902331492 * ___controller_6;

public:
	inline static int32_t get_offset_of_controller_6() { return static_cast<int32_t>(offsetof(AIStateMachine_t2425577084, ___controller_6)); }
	inline BossTankController_t1902331492 * get_controller_6() const { return ___controller_6; }
	inline BossTankController_t1902331492 ** get_address_of_controller_6() { return &___controller_6; }
	inline void set_controller_6(BossTankController_t1902331492 * value)
	{
		___controller_6 = value;
		Il2CppCodeGenWriteBarrier((&___controller_6), value);
	}
};

struct AIStateMachine_t2425577084_StaticFields
{
public:
	// AIStateMachine/AIStates AIStateMachine::currentState
	int32_t ___currentState_2;
	// System.Boolean AIStateMachine::phaseOne
	bool ___phaseOne_3;
	// System.Boolean AIStateMachine::phaseTwo
	bool ___phaseTwo_4;
	// System.Boolean AIStateMachine::phaseThree
	bool ___phaseThree_5;

public:
	inline static int32_t get_offset_of_currentState_2() { return static_cast<int32_t>(offsetof(AIStateMachine_t2425577084_StaticFields, ___currentState_2)); }
	inline int32_t get_currentState_2() const { return ___currentState_2; }
	inline int32_t* get_address_of_currentState_2() { return &___currentState_2; }
	inline void set_currentState_2(int32_t value)
	{
		___currentState_2 = value;
	}

	inline static int32_t get_offset_of_phaseOne_3() { return static_cast<int32_t>(offsetof(AIStateMachine_t2425577084_StaticFields, ___phaseOne_3)); }
	inline bool get_phaseOne_3() const { return ___phaseOne_3; }
	inline bool* get_address_of_phaseOne_3() { return &___phaseOne_3; }
	inline void set_phaseOne_3(bool value)
	{
		___phaseOne_3 = value;
	}

	inline static int32_t get_offset_of_phaseTwo_4() { return static_cast<int32_t>(offsetof(AIStateMachine_t2425577084_StaticFields, ___phaseTwo_4)); }
	inline bool get_phaseTwo_4() const { return ___phaseTwo_4; }
	inline bool* get_address_of_phaseTwo_4() { return &___phaseTwo_4; }
	inline void set_phaseTwo_4(bool value)
	{
		___phaseTwo_4 = value;
	}

	inline static int32_t get_offset_of_phaseThree_5() { return static_cast<int32_t>(offsetof(AIStateMachine_t2425577084_StaticFields, ___phaseThree_5)); }
	inline bool get_phaseThree_5() const { return ___phaseThree_5; }
	inline bool* get_address_of_phaseThree_5() { return &___phaseThree_5; }
	inline void set_phaseThree_5(bool value)
	{
		___phaseThree_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AISTATEMACHINE_T2425577084_H
#ifndef GRID_T1081586032_H
#define GRID_T1081586032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Grid
struct  Grid_t1081586032  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Grid::displayGridGizmos
	bool ___displayGridGizmos_2;
	// UnityEngine.LayerMask Grid::unwalkableMask
	LayerMask_t3493934918  ___unwalkableMask_3;
	// UnityEngine.Vector2 Grid::gridWorldSize
	Vector2_t2156229523  ___gridWorldSize_4;
	// System.Single Grid::nodeRadius
	float ___nodeRadius_5;
	// Grid/TerrainType[] Grid::walkableRegions
	TerrainTypeU5BU5D_t1748095520* ___walkableRegions_6;
	// System.Int32 Grid::obstacleProximityPenalty
	int32_t ___obstacleProximityPenalty_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> Grid::walkableRegionsDict
	Dictionary_2_t1839659084 * ___walkableRegionsDict_8;
	// UnityEngine.LayerMask Grid::walkableMask
	LayerMask_t3493934918  ___walkableMask_9;
	// Node[0...,0...] Grid::grid
	NodeU5B0___U2C0___U5D_t1457036568* ___grid_10;
	// System.Single Grid::nodeDiameter
	float ___nodeDiameter_11;
	// System.Int32 Grid::gridSizeX
	int32_t ___gridSizeX_12;
	// System.Int32 Grid::gridSizeY
	int32_t ___gridSizeY_13;
	// System.Int32 Grid::penaltyMin
	int32_t ___penaltyMin_14;
	// System.Int32 Grid::penaltyMax
	int32_t ___penaltyMax_15;

public:
	inline static int32_t get_offset_of_displayGridGizmos_2() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___displayGridGizmos_2)); }
	inline bool get_displayGridGizmos_2() const { return ___displayGridGizmos_2; }
	inline bool* get_address_of_displayGridGizmos_2() { return &___displayGridGizmos_2; }
	inline void set_displayGridGizmos_2(bool value)
	{
		___displayGridGizmos_2 = value;
	}

	inline static int32_t get_offset_of_unwalkableMask_3() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___unwalkableMask_3)); }
	inline LayerMask_t3493934918  get_unwalkableMask_3() const { return ___unwalkableMask_3; }
	inline LayerMask_t3493934918 * get_address_of_unwalkableMask_3() { return &___unwalkableMask_3; }
	inline void set_unwalkableMask_3(LayerMask_t3493934918  value)
	{
		___unwalkableMask_3 = value;
	}

	inline static int32_t get_offset_of_gridWorldSize_4() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___gridWorldSize_4)); }
	inline Vector2_t2156229523  get_gridWorldSize_4() const { return ___gridWorldSize_4; }
	inline Vector2_t2156229523 * get_address_of_gridWorldSize_4() { return &___gridWorldSize_4; }
	inline void set_gridWorldSize_4(Vector2_t2156229523  value)
	{
		___gridWorldSize_4 = value;
	}

	inline static int32_t get_offset_of_nodeRadius_5() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___nodeRadius_5)); }
	inline float get_nodeRadius_5() const { return ___nodeRadius_5; }
	inline float* get_address_of_nodeRadius_5() { return &___nodeRadius_5; }
	inline void set_nodeRadius_5(float value)
	{
		___nodeRadius_5 = value;
	}

	inline static int32_t get_offset_of_walkableRegions_6() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___walkableRegions_6)); }
	inline TerrainTypeU5BU5D_t1748095520* get_walkableRegions_6() const { return ___walkableRegions_6; }
	inline TerrainTypeU5BU5D_t1748095520** get_address_of_walkableRegions_6() { return &___walkableRegions_6; }
	inline void set_walkableRegions_6(TerrainTypeU5BU5D_t1748095520* value)
	{
		___walkableRegions_6 = value;
		Il2CppCodeGenWriteBarrier((&___walkableRegions_6), value);
	}

	inline static int32_t get_offset_of_obstacleProximityPenalty_7() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___obstacleProximityPenalty_7)); }
	inline int32_t get_obstacleProximityPenalty_7() const { return ___obstacleProximityPenalty_7; }
	inline int32_t* get_address_of_obstacleProximityPenalty_7() { return &___obstacleProximityPenalty_7; }
	inline void set_obstacleProximityPenalty_7(int32_t value)
	{
		___obstacleProximityPenalty_7 = value;
	}

	inline static int32_t get_offset_of_walkableRegionsDict_8() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___walkableRegionsDict_8)); }
	inline Dictionary_2_t1839659084 * get_walkableRegionsDict_8() const { return ___walkableRegionsDict_8; }
	inline Dictionary_2_t1839659084 ** get_address_of_walkableRegionsDict_8() { return &___walkableRegionsDict_8; }
	inline void set_walkableRegionsDict_8(Dictionary_2_t1839659084 * value)
	{
		___walkableRegionsDict_8 = value;
		Il2CppCodeGenWriteBarrier((&___walkableRegionsDict_8), value);
	}

	inline static int32_t get_offset_of_walkableMask_9() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___walkableMask_9)); }
	inline LayerMask_t3493934918  get_walkableMask_9() const { return ___walkableMask_9; }
	inline LayerMask_t3493934918 * get_address_of_walkableMask_9() { return &___walkableMask_9; }
	inline void set_walkableMask_9(LayerMask_t3493934918  value)
	{
		___walkableMask_9 = value;
	}

	inline static int32_t get_offset_of_grid_10() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___grid_10)); }
	inline NodeU5B0___U2C0___U5D_t1457036568* get_grid_10() const { return ___grid_10; }
	inline NodeU5B0___U2C0___U5D_t1457036568** get_address_of_grid_10() { return &___grid_10; }
	inline void set_grid_10(NodeU5B0___U2C0___U5D_t1457036568* value)
	{
		___grid_10 = value;
		Il2CppCodeGenWriteBarrier((&___grid_10), value);
	}

	inline static int32_t get_offset_of_nodeDiameter_11() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___nodeDiameter_11)); }
	inline float get_nodeDiameter_11() const { return ___nodeDiameter_11; }
	inline float* get_address_of_nodeDiameter_11() { return &___nodeDiameter_11; }
	inline void set_nodeDiameter_11(float value)
	{
		___nodeDiameter_11 = value;
	}

	inline static int32_t get_offset_of_gridSizeX_12() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___gridSizeX_12)); }
	inline int32_t get_gridSizeX_12() const { return ___gridSizeX_12; }
	inline int32_t* get_address_of_gridSizeX_12() { return &___gridSizeX_12; }
	inline void set_gridSizeX_12(int32_t value)
	{
		___gridSizeX_12 = value;
	}

	inline static int32_t get_offset_of_gridSizeY_13() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___gridSizeY_13)); }
	inline int32_t get_gridSizeY_13() const { return ___gridSizeY_13; }
	inline int32_t* get_address_of_gridSizeY_13() { return &___gridSizeY_13; }
	inline void set_gridSizeY_13(int32_t value)
	{
		___gridSizeY_13 = value;
	}

	inline static int32_t get_offset_of_penaltyMin_14() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___penaltyMin_14)); }
	inline int32_t get_penaltyMin_14() const { return ___penaltyMin_14; }
	inline int32_t* get_address_of_penaltyMin_14() { return &___penaltyMin_14; }
	inline void set_penaltyMin_14(int32_t value)
	{
		___penaltyMin_14 = value;
	}

	inline static int32_t get_offset_of_penaltyMax_15() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___penaltyMax_15)); }
	inline int32_t get_penaltyMax_15() const { return ___penaltyMax_15; }
	inline int32_t* get_address_of_penaltyMax_15() { return &___penaltyMax_15; }
	inline void set_penaltyMax_15(int32_t value)
	{
		___penaltyMax_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRID_T1081586032_H
#ifndef FUZZYSAMPLE_T2008138351_H
#define FUZZYSAMPLE_T2008138351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FuzzySample
struct  FuzzySample_t2008138351  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AnimationCurve FuzzySample::critical
	AnimationCurve_t3046754366 * ___critical_2;
	// UnityEngine.AnimationCurve FuzzySample::hurt
	AnimationCurve_t3046754366 * ___hurt_3;
	// UnityEngine.AnimationCurve FuzzySample::healthy
	AnimationCurve_t3046754366 * ___healthy_4;
	// UnityEngine.UI.InputField FuzzySample::healthInput
	InputField_t3762917431 * ___healthInput_5;
	// UnityEngine.UI.Text FuzzySample::labelHealthy
	Text_t1901882714 * ___labelHealthy_6;
	// UnityEngine.UI.Text FuzzySample::labelHurt
	Text_t1901882714 * ___labelHurt_7;
	// UnityEngine.UI.Text FuzzySample::labelCritical
	Text_t1901882714 * ___labelCritical_8;
	// UnityEngine.UI.Text FuzzySample::fuzzyState
	Text_t1901882714 * ___fuzzyState_9;
	// System.Single FuzzySample::valueHealthy
	float ___valueHealthy_11;
	// System.Single FuzzySample::valueHurt
	float ___valueHurt_12;
	// System.Single FuzzySample::valueCritical
	float ___valueCritical_13;

public:
	inline static int32_t get_offset_of_critical_2() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___critical_2)); }
	inline AnimationCurve_t3046754366 * get_critical_2() const { return ___critical_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_critical_2() { return &___critical_2; }
	inline void set_critical_2(AnimationCurve_t3046754366 * value)
	{
		___critical_2 = value;
		Il2CppCodeGenWriteBarrier((&___critical_2), value);
	}

	inline static int32_t get_offset_of_hurt_3() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___hurt_3)); }
	inline AnimationCurve_t3046754366 * get_hurt_3() const { return ___hurt_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_hurt_3() { return &___hurt_3; }
	inline void set_hurt_3(AnimationCurve_t3046754366 * value)
	{
		___hurt_3 = value;
		Il2CppCodeGenWriteBarrier((&___hurt_3), value);
	}

	inline static int32_t get_offset_of_healthy_4() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___healthy_4)); }
	inline AnimationCurve_t3046754366 * get_healthy_4() const { return ___healthy_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_healthy_4() { return &___healthy_4; }
	inline void set_healthy_4(AnimationCurve_t3046754366 * value)
	{
		___healthy_4 = value;
		Il2CppCodeGenWriteBarrier((&___healthy_4), value);
	}

	inline static int32_t get_offset_of_healthInput_5() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___healthInput_5)); }
	inline InputField_t3762917431 * get_healthInput_5() const { return ___healthInput_5; }
	inline InputField_t3762917431 ** get_address_of_healthInput_5() { return &___healthInput_5; }
	inline void set_healthInput_5(InputField_t3762917431 * value)
	{
		___healthInput_5 = value;
		Il2CppCodeGenWriteBarrier((&___healthInput_5), value);
	}

	inline static int32_t get_offset_of_labelHealthy_6() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___labelHealthy_6)); }
	inline Text_t1901882714 * get_labelHealthy_6() const { return ___labelHealthy_6; }
	inline Text_t1901882714 ** get_address_of_labelHealthy_6() { return &___labelHealthy_6; }
	inline void set_labelHealthy_6(Text_t1901882714 * value)
	{
		___labelHealthy_6 = value;
		Il2CppCodeGenWriteBarrier((&___labelHealthy_6), value);
	}

	inline static int32_t get_offset_of_labelHurt_7() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___labelHurt_7)); }
	inline Text_t1901882714 * get_labelHurt_7() const { return ___labelHurt_7; }
	inline Text_t1901882714 ** get_address_of_labelHurt_7() { return &___labelHurt_7; }
	inline void set_labelHurt_7(Text_t1901882714 * value)
	{
		___labelHurt_7 = value;
		Il2CppCodeGenWriteBarrier((&___labelHurt_7), value);
	}

	inline static int32_t get_offset_of_labelCritical_8() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___labelCritical_8)); }
	inline Text_t1901882714 * get_labelCritical_8() const { return ___labelCritical_8; }
	inline Text_t1901882714 ** get_address_of_labelCritical_8() { return &___labelCritical_8; }
	inline void set_labelCritical_8(Text_t1901882714 * value)
	{
		___labelCritical_8 = value;
		Il2CppCodeGenWriteBarrier((&___labelCritical_8), value);
	}

	inline static int32_t get_offset_of_fuzzyState_9() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___fuzzyState_9)); }
	inline Text_t1901882714 * get_fuzzyState_9() const { return ___fuzzyState_9; }
	inline Text_t1901882714 ** get_address_of_fuzzyState_9() { return &___fuzzyState_9; }
	inline void set_fuzzyState_9(Text_t1901882714 * value)
	{
		___fuzzyState_9 = value;
		Il2CppCodeGenWriteBarrier((&___fuzzyState_9), value);
	}

	inline static int32_t get_offset_of_valueHealthy_11() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___valueHealthy_11)); }
	inline float get_valueHealthy_11() const { return ___valueHealthy_11; }
	inline float* get_address_of_valueHealthy_11() { return &___valueHealthy_11; }
	inline void set_valueHealthy_11(float value)
	{
		___valueHealthy_11 = value;
	}

	inline static int32_t get_offset_of_valueHurt_12() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___valueHurt_12)); }
	inline float get_valueHurt_12() const { return ___valueHurt_12; }
	inline float* get_address_of_valueHurt_12() { return &___valueHurt_12; }
	inline void set_valueHurt_12(float value)
	{
		___valueHurt_12 = value;
	}

	inline static int32_t get_offset_of_valueCritical_13() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___valueCritical_13)); }
	inline float get_valueCritical_13() const { return ___valueCritical_13; }
	inline float* get_address_of_valueCritical_13() { return &___valueCritical_13; }
	inline void set_valueCritical_13(float value)
	{
		___valueCritical_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUZZYSAMPLE_T2008138351_H
#ifndef ENEMYFUZZYSTATE_T3026123389_H
#define ENEMYFUZZYSTATE_T3026123389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyFuzzyState
struct  EnemyFuzzyState_t3026123389  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AnimationCurve EnemyFuzzyState::critical
	AnimationCurve_t3046754366 * ___critical_2;
	// UnityEngine.AnimationCurve EnemyFuzzyState::hurt
	AnimationCurve_t3046754366 * ___hurt_3;
	// UnityEngine.AnimationCurve EnemyFuzzyState::healthy
	AnimationCurve_t3046754366 * ___healthy_4;
	// EnemyStats EnemyFuzzyState::enemyTank
	EnemyStats_t4187841152 * ___enemyTank_5;
	// UnityEngine.UI.Text EnemyFuzzyState::fuzzyState
	Text_t1901882714 * ___fuzzyState_6;
	// System.Single EnemyFuzzyState::enemyHealth
	float ___enemyHealth_7;
	// System.Single EnemyFuzzyState::valueHealthy
	float ___valueHealthy_9;
	// System.Single EnemyFuzzyState::valueHurt
	float ___valueHurt_10;
	// System.Single EnemyFuzzyState::valueCritical
	float ___valueCritical_11;

public:
	inline static int32_t get_offset_of_critical_2() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___critical_2)); }
	inline AnimationCurve_t3046754366 * get_critical_2() const { return ___critical_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_critical_2() { return &___critical_2; }
	inline void set_critical_2(AnimationCurve_t3046754366 * value)
	{
		___critical_2 = value;
		Il2CppCodeGenWriteBarrier((&___critical_2), value);
	}

	inline static int32_t get_offset_of_hurt_3() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___hurt_3)); }
	inline AnimationCurve_t3046754366 * get_hurt_3() const { return ___hurt_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_hurt_3() { return &___hurt_3; }
	inline void set_hurt_3(AnimationCurve_t3046754366 * value)
	{
		___hurt_3 = value;
		Il2CppCodeGenWriteBarrier((&___hurt_3), value);
	}

	inline static int32_t get_offset_of_healthy_4() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___healthy_4)); }
	inline AnimationCurve_t3046754366 * get_healthy_4() const { return ___healthy_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_healthy_4() { return &___healthy_4; }
	inline void set_healthy_4(AnimationCurve_t3046754366 * value)
	{
		___healthy_4 = value;
		Il2CppCodeGenWriteBarrier((&___healthy_4), value);
	}

	inline static int32_t get_offset_of_enemyTank_5() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___enemyTank_5)); }
	inline EnemyStats_t4187841152 * get_enemyTank_5() const { return ___enemyTank_5; }
	inline EnemyStats_t4187841152 ** get_address_of_enemyTank_5() { return &___enemyTank_5; }
	inline void set_enemyTank_5(EnemyStats_t4187841152 * value)
	{
		___enemyTank_5 = value;
		Il2CppCodeGenWriteBarrier((&___enemyTank_5), value);
	}

	inline static int32_t get_offset_of_fuzzyState_6() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___fuzzyState_6)); }
	inline Text_t1901882714 * get_fuzzyState_6() const { return ___fuzzyState_6; }
	inline Text_t1901882714 ** get_address_of_fuzzyState_6() { return &___fuzzyState_6; }
	inline void set_fuzzyState_6(Text_t1901882714 * value)
	{
		___fuzzyState_6 = value;
		Il2CppCodeGenWriteBarrier((&___fuzzyState_6), value);
	}

	inline static int32_t get_offset_of_enemyHealth_7() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___enemyHealth_7)); }
	inline float get_enemyHealth_7() const { return ___enemyHealth_7; }
	inline float* get_address_of_enemyHealth_7() { return &___enemyHealth_7; }
	inline void set_enemyHealth_7(float value)
	{
		___enemyHealth_7 = value;
	}

	inline static int32_t get_offset_of_valueHealthy_9() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___valueHealthy_9)); }
	inline float get_valueHealthy_9() const { return ___valueHealthy_9; }
	inline float* get_address_of_valueHealthy_9() { return &___valueHealthy_9; }
	inline void set_valueHealthy_9(float value)
	{
		___valueHealthy_9 = value;
	}

	inline static int32_t get_offset_of_valueHurt_10() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___valueHurt_10)); }
	inline float get_valueHurt_10() const { return ___valueHurt_10; }
	inline float* get_address_of_valueHurt_10() { return &___valueHurt_10; }
	inline void set_valueHurt_10(float value)
	{
		___valueHurt_10 = value;
	}

	inline static int32_t get_offset_of_valueCritical_11() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___valueCritical_11)); }
	inline float get_valueCritical_11() const { return ___valueCritical_11; }
	inline float* get_address_of_valueCritical_11() { return &___valueCritical_11; }
	inline void set_valueCritical_11(float value)
	{
		___valueCritical_11 = value;
	}
};

struct EnemyFuzzyState_t3026123389_StaticFields
{
public:
	// EnemyFuzzyState/FuzzyStates EnemyFuzzyState::currentFuzzyState
	int32_t ___currentFuzzyState_8;

public:
	inline static int32_t get_offset_of_currentFuzzyState_8() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389_StaticFields, ___currentFuzzyState_8)); }
	inline int32_t get_currentFuzzyState_8() const { return ___currentFuzzyState_8; }
	inline int32_t* get_address_of_currentFuzzyState_8() { return &___currentFuzzyState_8; }
	inline void set_currentFuzzyState_8(int32_t value)
	{
		___currentFuzzyState_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYFUZZYSTATE_T3026123389_H
#ifndef BACKGROUNDSCROLLER_T2508866296_H
#define BACKGROUNDSCROLLER_T2508866296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackgroundScroller
struct  BackgroundScroller_t2508866296  : public MonoBehaviour_t3962482529
{
public:
	// System.Single BackgroundScroller::scrollSpeed
	float ___scrollSpeed_2;
	// System.Single BackgroundScroller::tileSizeY
	float ___tileSizeY_3;
	// UnityEngine.RectTransform BackgroundScroller::rect
	RectTransform_t3704657025 * ___rect_4;
	// UnityEngine.Vector3 BackgroundScroller::startPosition
	Vector3_t3722313464  ___startPosition_5;

public:
	inline static int32_t get_offset_of_scrollSpeed_2() { return static_cast<int32_t>(offsetof(BackgroundScroller_t2508866296, ___scrollSpeed_2)); }
	inline float get_scrollSpeed_2() const { return ___scrollSpeed_2; }
	inline float* get_address_of_scrollSpeed_2() { return &___scrollSpeed_2; }
	inline void set_scrollSpeed_2(float value)
	{
		___scrollSpeed_2 = value;
	}

	inline static int32_t get_offset_of_tileSizeY_3() { return static_cast<int32_t>(offsetof(BackgroundScroller_t2508866296, ___tileSizeY_3)); }
	inline float get_tileSizeY_3() const { return ___tileSizeY_3; }
	inline float* get_address_of_tileSizeY_3() { return &___tileSizeY_3; }
	inline void set_tileSizeY_3(float value)
	{
		___tileSizeY_3 = value;
	}

	inline static int32_t get_offset_of_rect_4() { return static_cast<int32_t>(offsetof(BackgroundScroller_t2508866296, ___rect_4)); }
	inline RectTransform_t3704657025 * get_rect_4() const { return ___rect_4; }
	inline RectTransform_t3704657025 ** get_address_of_rect_4() { return &___rect_4; }
	inline void set_rect_4(RectTransform_t3704657025 * value)
	{
		___rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___rect_4), value);
	}

	inline static int32_t get_offset_of_startPosition_5() { return static_cast<int32_t>(offsetof(BackgroundScroller_t2508866296, ___startPosition_5)); }
	inline Vector3_t3722313464  get_startPosition_5() const { return ___startPosition_5; }
	inline Vector3_t3722313464 * get_address_of_startPosition_5() { return &___startPosition_5; }
	inline void set_startPosition_5(Vector3_t3722313464  value)
	{
		___startPosition_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDSCROLLER_T2508866296_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef GAMECAMERA_T2564829307_H
#define GAMECAMERA_T2564829307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameCamera
struct  GameCamera_t2564829307  : public MonoBehaviour_t3962482529
{
public:
	// System.Single GameCamera::xModifier
	float ___xModifier_2;
	// System.Single GameCamera::yModifier
	float ___yModifier_3;
	// System.Single GameCamera::zModifier
	float ___zModifier_4;
	// UnityEngine.Vector3 GameCamera::cameraTarget
	Vector3_t3722313464  ___cameraTarget_5;
	// UnityEngine.Transform GameCamera::target
	Transform_t3600365921 * ___target_6;

public:
	inline static int32_t get_offset_of_xModifier_2() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307, ___xModifier_2)); }
	inline float get_xModifier_2() const { return ___xModifier_2; }
	inline float* get_address_of_xModifier_2() { return &___xModifier_2; }
	inline void set_xModifier_2(float value)
	{
		___xModifier_2 = value;
	}

	inline static int32_t get_offset_of_yModifier_3() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307, ___yModifier_3)); }
	inline float get_yModifier_3() const { return ___yModifier_3; }
	inline float* get_address_of_yModifier_3() { return &___yModifier_3; }
	inline void set_yModifier_3(float value)
	{
		___yModifier_3 = value;
	}

	inline static int32_t get_offset_of_zModifier_4() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307, ___zModifier_4)); }
	inline float get_zModifier_4() const { return ___zModifier_4; }
	inline float* get_address_of_zModifier_4() { return &___zModifier_4; }
	inline void set_zModifier_4(float value)
	{
		___zModifier_4 = value;
	}

	inline static int32_t get_offset_of_cameraTarget_5() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307, ___cameraTarget_5)); }
	inline Vector3_t3722313464  get_cameraTarget_5() const { return ___cameraTarget_5; }
	inline Vector3_t3722313464 * get_address_of_cameraTarget_5() { return &___cameraTarget_5; }
	inline void set_cameraTarget_5(Vector3_t3722313464  value)
	{
		___cameraTarget_5 = value;
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307, ___target_6)); }
	inline Transform_t3600365921 * get_target_6() const { return ___target_6; }
	inline Transform_t3600365921 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Transform_t3600365921 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECAMERA_T2564829307_H
#ifndef AUDIOSOURCE_T3935305588_H
#define AUDIOSOURCE_T3935305588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSource
struct  AudioSource_t3935305588  : public AudioBehaviour_t2879336574
{
public:
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::spatializerExtension
	AudioSourceExtension_t3064908834 * ___spatializerExtension_2;
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::ambisonicExtension
	AudioSourceExtension_t3064908834 * ___ambisonicExtension_3;

public:
	inline static int32_t get_offset_of_spatializerExtension_2() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___spatializerExtension_2)); }
	inline AudioSourceExtension_t3064908834 * get_spatializerExtension_2() const { return ___spatializerExtension_2; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_spatializerExtension_2() { return &___spatializerExtension_2; }
	inline void set_spatializerExtension_2(AudioSourceExtension_t3064908834 * value)
	{
		___spatializerExtension_2 = value;
		Il2CppCodeGenWriteBarrier((&___spatializerExtension_2), value);
	}

	inline static int32_t get_offset_of_ambisonicExtension_3() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___ambisonicExtension_3)); }
	inline AudioSourceExtension_t3064908834 * get_ambisonicExtension_3() const { return ___ambisonicExtension_3; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_ambisonicExtension_3() { return &___ambisonicExtension_3; }
	inline void set_ambisonicExtension_3(AudioSourceExtension_t3064908834 * value)
	{
		___ambisonicExtension_3 = value;
		Il2CppCodeGenWriteBarrier((&___ambisonicExtension_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCE_T3935305588_H
#ifndef DAMAGEPLAYER_T2033828012_H
#define DAMAGEPLAYER_T2033828012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DamagePlayer
struct  DamagePlayer_t2033828012  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DamagePlayer::explosionSmall
	GameObject_t1113636619 * ___explosionSmall_2;
	// UnityEngine.GameObject DamagePlayer::explosionBig
	GameObject_t1113636619 * ___explosionBig_3;
	// System.Int32 DamagePlayer::damageValue
	int32_t ___damageValue_4;
	// HealthUI DamagePlayer::healthUI
	HealthUI_t1908446248 * ___healthUI_5;

public:
	inline static int32_t get_offset_of_explosionSmall_2() { return static_cast<int32_t>(offsetof(DamagePlayer_t2033828012, ___explosionSmall_2)); }
	inline GameObject_t1113636619 * get_explosionSmall_2() const { return ___explosionSmall_2; }
	inline GameObject_t1113636619 ** get_address_of_explosionSmall_2() { return &___explosionSmall_2; }
	inline void set_explosionSmall_2(GameObject_t1113636619 * value)
	{
		___explosionSmall_2 = value;
		Il2CppCodeGenWriteBarrier((&___explosionSmall_2), value);
	}

	inline static int32_t get_offset_of_explosionBig_3() { return static_cast<int32_t>(offsetof(DamagePlayer_t2033828012, ___explosionBig_3)); }
	inline GameObject_t1113636619 * get_explosionBig_3() const { return ___explosionBig_3; }
	inline GameObject_t1113636619 ** get_address_of_explosionBig_3() { return &___explosionBig_3; }
	inline void set_explosionBig_3(GameObject_t1113636619 * value)
	{
		___explosionBig_3 = value;
		Il2CppCodeGenWriteBarrier((&___explosionBig_3), value);
	}

	inline static int32_t get_offset_of_damageValue_4() { return static_cast<int32_t>(offsetof(DamagePlayer_t2033828012, ___damageValue_4)); }
	inline int32_t get_damageValue_4() const { return ___damageValue_4; }
	inline int32_t* get_address_of_damageValue_4() { return &___damageValue_4; }
	inline void set_damageValue_4(int32_t value)
	{
		___damageValue_4 = value;
	}

	inline static int32_t get_offset_of_healthUI_5() { return static_cast<int32_t>(offsetof(DamagePlayer_t2033828012, ___healthUI_5)); }
	inline HealthUI_t1908446248 * get_healthUI_5() const { return ___healthUI_5; }
	inline HealthUI_t1908446248 ** get_address_of_healthUI_5() { return &___healthUI_5; }
	inline void set_healthUI_5(HealthUI_t1908446248 * value)
	{
		___healthUI_5 = value;
		Il2CppCodeGenWriteBarrier((&___healthUI_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAMAGEPLAYER_T2033828012_H
#ifndef ENABLECAMERADEPTHINFORWARD_T3351256833_H
#define ENABLECAMERADEPTHINFORWARD_T3351256833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnableCameraDepthInForward
struct  EnableCameraDepthInForward_t3351256833  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENABLECAMERADEPTHINFORWARD_T3351256833_H
#ifndef INVENTORYUI_T1983315844_H
#define INVENTORYUI_T1983315844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventoryUI
struct  InventoryUI_t1983315844  : public MonoBehaviour_t3962482529
{
public:
	// Inventory InventoryUI::inventory
	Inventory_t1050226016 * ___inventory_2;
	// InventorySlot InventoryUI::weaponSlot
	InventorySlot_t3299309524 * ___weaponSlot_3;
	// InventorySlot InventoryUI::shieldSlot
	InventorySlot_t3299309524 * ___shieldSlot_4;

public:
	inline static int32_t get_offset_of_inventory_2() { return static_cast<int32_t>(offsetof(InventoryUI_t1983315844, ___inventory_2)); }
	inline Inventory_t1050226016 * get_inventory_2() const { return ___inventory_2; }
	inline Inventory_t1050226016 ** get_address_of_inventory_2() { return &___inventory_2; }
	inline void set_inventory_2(Inventory_t1050226016 * value)
	{
		___inventory_2 = value;
		Il2CppCodeGenWriteBarrier((&___inventory_2), value);
	}

	inline static int32_t get_offset_of_weaponSlot_3() { return static_cast<int32_t>(offsetof(InventoryUI_t1983315844, ___weaponSlot_3)); }
	inline InventorySlot_t3299309524 * get_weaponSlot_3() const { return ___weaponSlot_3; }
	inline InventorySlot_t3299309524 ** get_address_of_weaponSlot_3() { return &___weaponSlot_3; }
	inline void set_weaponSlot_3(InventorySlot_t3299309524 * value)
	{
		___weaponSlot_3 = value;
		Il2CppCodeGenWriteBarrier((&___weaponSlot_3), value);
	}

	inline static int32_t get_offset_of_shieldSlot_4() { return static_cast<int32_t>(offsetof(InventoryUI_t1983315844, ___shieldSlot_4)); }
	inline InventorySlot_t3299309524 * get_shieldSlot_4() const { return ___shieldSlot_4; }
	inline InventorySlot_t3299309524 ** get_address_of_shieldSlot_4() { return &___shieldSlot_4; }
	inline void set_shieldSlot_4(InventorySlot_t3299309524 * value)
	{
		___shieldSlot_4 = value;
		Il2CppCodeGenWriteBarrier((&___shieldSlot_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORYUI_T1983315844_H
#ifndef DAMAGEENEMY_T2198349935_H
#define DAMAGEENEMY_T2198349935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DamageEnemy
struct  DamageEnemy_t2198349935  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DamageEnemy::explosionSmall
	GameObject_t1113636619 * ___explosionSmall_2;
	// UnityEngine.GameObject DamageEnemy::explosionBig
	GameObject_t1113636619 * ___explosionBig_3;
	// System.Int32 DamageEnemy::damageValue
	int32_t ___damageValue_4;
	// HealthUI DamageEnemy::healthUI
	HealthUI_t1908446248 * ___healthUI_5;

public:
	inline static int32_t get_offset_of_explosionSmall_2() { return static_cast<int32_t>(offsetof(DamageEnemy_t2198349935, ___explosionSmall_2)); }
	inline GameObject_t1113636619 * get_explosionSmall_2() const { return ___explosionSmall_2; }
	inline GameObject_t1113636619 ** get_address_of_explosionSmall_2() { return &___explosionSmall_2; }
	inline void set_explosionSmall_2(GameObject_t1113636619 * value)
	{
		___explosionSmall_2 = value;
		Il2CppCodeGenWriteBarrier((&___explosionSmall_2), value);
	}

	inline static int32_t get_offset_of_explosionBig_3() { return static_cast<int32_t>(offsetof(DamageEnemy_t2198349935, ___explosionBig_3)); }
	inline GameObject_t1113636619 * get_explosionBig_3() const { return ___explosionBig_3; }
	inline GameObject_t1113636619 ** get_address_of_explosionBig_3() { return &___explosionBig_3; }
	inline void set_explosionBig_3(GameObject_t1113636619 * value)
	{
		___explosionBig_3 = value;
		Il2CppCodeGenWriteBarrier((&___explosionBig_3), value);
	}

	inline static int32_t get_offset_of_damageValue_4() { return static_cast<int32_t>(offsetof(DamageEnemy_t2198349935, ___damageValue_4)); }
	inline int32_t get_damageValue_4() const { return ___damageValue_4; }
	inline int32_t* get_address_of_damageValue_4() { return &___damageValue_4; }
	inline void set_damageValue_4(int32_t value)
	{
		___damageValue_4 = value;
	}

	inline static int32_t get_offset_of_healthUI_5() { return static_cast<int32_t>(offsetof(DamageEnemy_t2198349935, ___healthUI_5)); }
	inline HealthUI_t1908446248 * get_healthUI_5() const { return ___healthUI_5; }
	inline HealthUI_t1908446248 ** get_address_of_healthUI_5() { return &___healthUI_5; }
	inline void set_healthUI_5(HealthUI_t1908446248 * value)
	{
		___healthUI_5 = value;
		Il2CppCodeGenWriteBarrier((&___healthUI_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAMAGEENEMY_T2198349935_H
#ifndef HEALTHUI_T1908446248_H
#define HEALTHUI_T1908446248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HealthUI
struct  HealthUI_t1908446248  : public MonoBehaviour_t3962482529
{
public:
	// PlayerStats HealthUI::playerStats
	PlayerStats_t2044123780 * ___playerStats_2;
	// UnityEngine.AudioClip HealthUI::destroyBullet
	AudioClip_t3680889665 * ___destroyBullet_3;
	// UnityEngine.UI.Image[] HealthUI::healthHearts
	ImageU5BU5D_t2439009922* ___healthHearts_4;

public:
	inline static int32_t get_offset_of_playerStats_2() { return static_cast<int32_t>(offsetof(HealthUI_t1908446248, ___playerStats_2)); }
	inline PlayerStats_t2044123780 * get_playerStats_2() const { return ___playerStats_2; }
	inline PlayerStats_t2044123780 ** get_address_of_playerStats_2() { return &___playerStats_2; }
	inline void set_playerStats_2(PlayerStats_t2044123780 * value)
	{
		___playerStats_2 = value;
		Il2CppCodeGenWriteBarrier((&___playerStats_2), value);
	}

	inline static int32_t get_offset_of_destroyBullet_3() { return static_cast<int32_t>(offsetof(HealthUI_t1908446248, ___destroyBullet_3)); }
	inline AudioClip_t3680889665 * get_destroyBullet_3() const { return ___destroyBullet_3; }
	inline AudioClip_t3680889665 ** get_address_of_destroyBullet_3() { return &___destroyBullet_3; }
	inline void set_destroyBullet_3(AudioClip_t3680889665 * value)
	{
		___destroyBullet_3 = value;
		Il2CppCodeGenWriteBarrier((&___destroyBullet_3), value);
	}

	inline static int32_t get_offset_of_healthHearts_4() { return static_cast<int32_t>(offsetof(HealthUI_t1908446248, ___healthHearts_4)); }
	inline ImageU5BU5D_t2439009922* get_healthHearts_4() const { return ___healthHearts_4; }
	inline ImageU5BU5D_t2439009922** get_address_of_healthHearts_4() { return &___healthHearts_4; }
	inline void set_healthHearts_4(ImageU5BU5D_t2439009922* value)
	{
		___healthHearts_4 = value;
		Il2CppCodeGenWriteBarrier((&___healthHearts_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEALTHUI_T1908446248_H
#ifndef ENEMYCONTROLLER_T2191660454_H
#define ENEMYCONTROLLER_T2191660454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyController
struct  EnemyController_t2191660454  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AI.NavMeshAgent EnemyController::agent
	NavMeshAgent_t1276799816 * ___agent_2;
	// Gun EnemyController::gun
	Gun_t783153271 * ___gun_3;
	// System.Single EnemyController::lookRadius
	float ___lookRadius_4;
	// System.Single EnemyController::reloadTime
	float ___reloadTime_5;
	// UnityEngine.Transform EnemyController::target
	Transform_t3600365921 * ___target_6;
	// UnityEngine.BoxCollider EnemyController::fieldOfViewBox
	BoxCollider_t1640800422 * ___fieldOfViewBox_7;
	// System.Boolean EnemyController::spottedPlayer
	bool ___spottedPlayer_8;

public:
	inline static int32_t get_offset_of_agent_2() { return static_cast<int32_t>(offsetof(EnemyController_t2191660454, ___agent_2)); }
	inline NavMeshAgent_t1276799816 * get_agent_2() const { return ___agent_2; }
	inline NavMeshAgent_t1276799816 ** get_address_of_agent_2() { return &___agent_2; }
	inline void set_agent_2(NavMeshAgent_t1276799816 * value)
	{
		___agent_2 = value;
		Il2CppCodeGenWriteBarrier((&___agent_2), value);
	}

	inline static int32_t get_offset_of_gun_3() { return static_cast<int32_t>(offsetof(EnemyController_t2191660454, ___gun_3)); }
	inline Gun_t783153271 * get_gun_3() const { return ___gun_3; }
	inline Gun_t783153271 ** get_address_of_gun_3() { return &___gun_3; }
	inline void set_gun_3(Gun_t783153271 * value)
	{
		___gun_3 = value;
		Il2CppCodeGenWriteBarrier((&___gun_3), value);
	}

	inline static int32_t get_offset_of_lookRadius_4() { return static_cast<int32_t>(offsetof(EnemyController_t2191660454, ___lookRadius_4)); }
	inline float get_lookRadius_4() const { return ___lookRadius_4; }
	inline float* get_address_of_lookRadius_4() { return &___lookRadius_4; }
	inline void set_lookRadius_4(float value)
	{
		___lookRadius_4 = value;
	}

	inline static int32_t get_offset_of_reloadTime_5() { return static_cast<int32_t>(offsetof(EnemyController_t2191660454, ___reloadTime_5)); }
	inline float get_reloadTime_5() const { return ___reloadTime_5; }
	inline float* get_address_of_reloadTime_5() { return &___reloadTime_5; }
	inline void set_reloadTime_5(float value)
	{
		___reloadTime_5 = value;
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(EnemyController_t2191660454, ___target_6)); }
	inline Transform_t3600365921 * get_target_6() const { return ___target_6; }
	inline Transform_t3600365921 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Transform_t3600365921 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_fieldOfViewBox_7() { return static_cast<int32_t>(offsetof(EnemyController_t2191660454, ___fieldOfViewBox_7)); }
	inline BoxCollider_t1640800422 * get_fieldOfViewBox_7() const { return ___fieldOfViewBox_7; }
	inline BoxCollider_t1640800422 ** get_address_of_fieldOfViewBox_7() { return &___fieldOfViewBox_7; }
	inline void set_fieldOfViewBox_7(BoxCollider_t1640800422 * value)
	{
		___fieldOfViewBox_7 = value;
		Il2CppCodeGenWriteBarrier((&___fieldOfViewBox_7), value);
	}

	inline static int32_t get_offset_of_spottedPlayer_8() { return static_cast<int32_t>(offsetof(EnemyController_t2191660454, ___spottedPlayer_8)); }
	inline bool get_spottedPlayer_8() const { return ___spottedPlayer_8; }
	inline bool* get_address_of_spottedPlayer_8() { return &___spottedPlayer_8; }
	inline void set_spottedPlayer_8(bool value)
	{
		___spottedPlayer_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYCONTROLLER_T2191660454_H
#ifndef PLAYERMANAGER_T1349889689_H
#define PLAYERMANAGER_T1349889689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerManager
struct  PlayerManager_t1349889689  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PlayerManager::player
	GameObject_t1113636619 * ___player_3;

public:
	inline static int32_t get_offset_of_player_3() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___player_3)); }
	inline GameObject_t1113636619 * get_player_3() const { return ___player_3; }
	inline GameObject_t1113636619 ** get_address_of_player_3() { return &___player_3; }
	inline void set_player_3(GameObject_t1113636619 * value)
	{
		___player_3 = value;
		Il2CppCodeGenWriteBarrier((&___player_3), value);
	}
};

struct PlayerManager_t1349889689_StaticFields
{
public:
	// PlayerManager PlayerManager::instance
	PlayerManager_t1349889689 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689_StaticFields, ___instance_2)); }
	inline PlayerManager_t1349889689 * get_instance_2() const { return ___instance_2; }
	inline PlayerManager_t1349889689 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(PlayerManager_t1349889689 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMANAGER_T1349889689_H
#ifndef LEVELSELECTPANEL_T2626311185_H
#define LEVELSELECTPANEL_T2626311185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelSelectPanel
struct  LevelSelectPanel_t2626311185  : public MonoBehaviour_t3962482529
{
public:
	// System.String LevelSelectPanel::levelName
	String_t* ___levelName_2;
	// System.Boolean LevelSelectPanel::levelUnlocked
	bool ___levelUnlocked_3;
	// System.Boolean LevelSelectPanel::levelCompleted
	bool ___levelCompleted_4;
	// System.Boolean LevelSelectPanel::selected
	bool ___selected_5;
	// UnityEngine.UI.Image LevelSelectPanel::panelImage
	Image_t2670269651 * ___panelImage_6;
	// UnityEngine.UI.Image LevelSelectPanel::levelStatus
	Image_t2670269651 * ___levelStatus_7;
	// UnityEngine.Sprite[] LevelSelectPanel::panelSprites
	SpriteU5BU5D_t2581906349* ___panelSprites_8;

public:
	inline static int32_t get_offset_of_levelName_2() { return static_cast<int32_t>(offsetof(LevelSelectPanel_t2626311185, ___levelName_2)); }
	inline String_t* get_levelName_2() const { return ___levelName_2; }
	inline String_t** get_address_of_levelName_2() { return &___levelName_2; }
	inline void set_levelName_2(String_t* value)
	{
		___levelName_2 = value;
		Il2CppCodeGenWriteBarrier((&___levelName_2), value);
	}

	inline static int32_t get_offset_of_levelUnlocked_3() { return static_cast<int32_t>(offsetof(LevelSelectPanel_t2626311185, ___levelUnlocked_3)); }
	inline bool get_levelUnlocked_3() const { return ___levelUnlocked_3; }
	inline bool* get_address_of_levelUnlocked_3() { return &___levelUnlocked_3; }
	inline void set_levelUnlocked_3(bool value)
	{
		___levelUnlocked_3 = value;
	}

	inline static int32_t get_offset_of_levelCompleted_4() { return static_cast<int32_t>(offsetof(LevelSelectPanel_t2626311185, ___levelCompleted_4)); }
	inline bool get_levelCompleted_4() const { return ___levelCompleted_4; }
	inline bool* get_address_of_levelCompleted_4() { return &___levelCompleted_4; }
	inline void set_levelCompleted_4(bool value)
	{
		___levelCompleted_4 = value;
	}

	inline static int32_t get_offset_of_selected_5() { return static_cast<int32_t>(offsetof(LevelSelectPanel_t2626311185, ___selected_5)); }
	inline bool get_selected_5() const { return ___selected_5; }
	inline bool* get_address_of_selected_5() { return &___selected_5; }
	inline void set_selected_5(bool value)
	{
		___selected_5 = value;
	}

	inline static int32_t get_offset_of_panelImage_6() { return static_cast<int32_t>(offsetof(LevelSelectPanel_t2626311185, ___panelImage_6)); }
	inline Image_t2670269651 * get_panelImage_6() const { return ___panelImage_6; }
	inline Image_t2670269651 ** get_address_of_panelImage_6() { return &___panelImage_6; }
	inline void set_panelImage_6(Image_t2670269651 * value)
	{
		___panelImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___panelImage_6), value);
	}

	inline static int32_t get_offset_of_levelStatus_7() { return static_cast<int32_t>(offsetof(LevelSelectPanel_t2626311185, ___levelStatus_7)); }
	inline Image_t2670269651 * get_levelStatus_7() const { return ___levelStatus_7; }
	inline Image_t2670269651 ** get_address_of_levelStatus_7() { return &___levelStatus_7; }
	inline void set_levelStatus_7(Image_t2670269651 * value)
	{
		___levelStatus_7 = value;
		Il2CppCodeGenWriteBarrier((&___levelStatus_7), value);
	}

	inline static int32_t get_offset_of_panelSprites_8() { return static_cast<int32_t>(offsetof(LevelSelectPanel_t2626311185, ___panelSprites_8)); }
	inline SpriteU5BU5D_t2581906349* get_panelSprites_8() const { return ___panelSprites_8; }
	inline SpriteU5BU5D_t2581906349** get_address_of_panelSprites_8() { return &___panelSprites_8; }
	inline void set_panelSprites_8(SpriteU5BU5D_t2581906349* value)
	{
		___panelSprites_8 = value;
		Il2CppCodeGenWriteBarrier((&___panelSprites_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSELECTPANEL_T2626311185_H
#ifndef PAUSEMENU_T3916167947_H
#define PAUSEMENU_T3916167947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PauseMenu
struct  PauseMenu_t3916167947  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PauseMenu::pauseMenuUI
	GameObject_t1113636619 * ___pauseMenuUI_4;
	// System.Int32 PauseMenu::index
	int32_t ___index_5;
	// UnityEngine.AudioClip PauseMenu::selectSound
	AudioClip_t3680889665 * ___selectSound_6;
	// UnityEngine.AudioClip PauseMenu::confirmSound
	AudioClip_t3680889665 * ___confirmSound_7;
	// UnityEngine.GameObject[] PauseMenu::options
	GameObjectU5BU5D_t3328599146* ___options_8;
	// UnityEngine.Sprite[] PauseMenu::optionBox
	SpriteU5BU5D_t2581906349* ___optionBox_9;
	// System.Boolean PauseMenu::upAxesInUse
	bool ___upAxesInUse_10;
	// System.Boolean PauseMenu::downAxesInUse
	bool ___downAxesInUse_11;
	// System.Boolean PauseMenu::cancelAxesInUse
	bool ___cancelAxesInUse_12;

public:
	inline static int32_t get_offset_of_pauseMenuUI_4() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___pauseMenuUI_4)); }
	inline GameObject_t1113636619 * get_pauseMenuUI_4() const { return ___pauseMenuUI_4; }
	inline GameObject_t1113636619 ** get_address_of_pauseMenuUI_4() { return &___pauseMenuUI_4; }
	inline void set_pauseMenuUI_4(GameObject_t1113636619 * value)
	{
		___pauseMenuUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___pauseMenuUI_4), value);
	}

	inline static int32_t get_offset_of_index_5() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___index_5)); }
	inline int32_t get_index_5() const { return ___index_5; }
	inline int32_t* get_address_of_index_5() { return &___index_5; }
	inline void set_index_5(int32_t value)
	{
		___index_5 = value;
	}

	inline static int32_t get_offset_of_selectSound_6() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___selectSound_6)); }
	inline AudioClip_t3680889665 * get_selectSound_6() const { return ___selectSound_6; }
	inline AudioClip_t3680889665 ** get_address_of_selectSound_6() { return &___selectSound_6; }
	inline void set_selectSound_6(AudioClip_t3680889665 * value)
	{
		___selectSound_6 = value;
		Il2CppCodeGenWriteBarrier((&___selectSound_6), value);
	}

	inline static int32_t get_offset_of_confirmSound_7() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___confirmSound_7)); }
	inline AudioClip_t3680889665 * get_confirmSound_7() const { return ___confirmSound_7; }
	inline AudioClip_t3680889665 ** get_address_of_confirmSound_7() { return &___confirmSound_7; }
	inline void set_confirmSound_7(AudioClip_t3680889665 * value)
	{
		___confirmSound_7 = value;
		Il2CppCodeGenWriteBarrier((&___confirmSound_7), value);
	}

	inline static int32_t get_offset_of_options_8() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___options_8)); }
	inline GameObjectU5BU5D_t3328599146* get_options_8() const { return ___options_8; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_options_8() { return &___options_8; }
	inline void set_options_8(GameObjectU5BU5D_t3328599146* value)
	{
		___options_8 = value;
		Il2CppCodeGenWriteBarrier((&___options_8), value);
	}

	inline static int32_t get_offset_of_optionBox_9() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___optionBox_9)); }
	inline SpriteU5BU5D_t2581906349* get_optionBox_9() const { return ___optionBox_9; }
	inline SpriteU5BU5D_t2581906349** get_address_of_optionBox_9() { return &___optionBox_9; }
	inline void set_optionBox_9(SpriteU5BU5D_t2581906349* value)
	{
		___optionBox_9 = value;
		Il2CppCodeGenWriteBarrier((&___optionBox_9), value);
	}

	inline static int32_t get_offset_of_upAxesInUse_10() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___upAxesInUse_10)); }
	inline bool get_upAxesInUse_10() const { return ___upAxesInUse_10; }
	inline bool* get_address_of_upAxesInUse_10() { return &___upAxesInUse_10; }
	inline void set_upAxesInUse_10(bool value)
	{
		___upAxesInUse_10 = value;
	}

	inline static int32_t get_offset_of_downAxesInUse_11() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___downAxesInUse_11)); }
	inline bool get_downAxesInUse_11() const { return ___downAxesInUse_11; }
	inline bool* get_address_of_downAxesInUse_11() { return &___downAxesInUse_11; }
	inline void set_downAxesInUse_11(bool value)
	{
		___downAxesInUse_11 = value;
	}

	inline static int32_t get_offset_of_cancelAxesInUse_12() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___cancelAxesInUse_12)); }
	inline bool get_cancelAxesInUse_12() const { return ___cancelAxesInUse_12; }
	inline bool* get_address_of_cancelAxesInUse_12() { return &___cancelAxesInUse_12; }
	inline void set_cancelAxesInUse_12(bool value)
	{
		___cancelAxesInUse_12 = value;
	}
};

struct PauseMenu_t3916167947_StaticFields
{
public:
	// System.Boolean PauseMenu::gameIsPaused
	bool ___gameIsPaused_2;
	// System.Boolean PauseMenu::pausingEnabled
	bool ___pausingEnabled_3;

public:
	inline static int32_t get_offset_of_gameIsPaused_2() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947_StaticFields, ___gameIsPaused_2)); }
	inline bool get_gameIsPaused_2() const { return ___gameIsPaused_2; }
	inline bool* get_address_of_gameIsPaused_2() { return &___gameIsPaused_2; }
	inline void set_gameIsPaused_2(bool value)
	{
		___gameIsPaused_2 = value;
	}

	inline static int32_t get_offset_of_pausingEnabled_3() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947_StaticFields, ___pausingEnabled_3)); }
	inline bool get_pausingEnabled_3() const { return ___pausingEnabled_3; }
	inline bool* get_address_of_pausingEnabled_3() { return &___pausingEnabled_3; }
	inline void set_pausingEnabled_3(bool value)
	{
		___pausingEnabled_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSEMENU_T3916167947_H
#ifndef LEVELSELECT_T2669120976_H
#define LEVELSELECT_T2669120976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelSelect
struct  LevelSelect_t2669120976  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 LevelSelect::index
	int32_t ___index_2;
	// UnityEngine.AudioClip LevelSelect::selectSound
	AudioClip_t3680889665 * ___selectSound_3;
	// UnityEngine.AudioClip LevelSelect::confirmSound
	AudioClip_t3680889665 * ___confirmSound_4;
	// UnityEngine.AudioClip LevelSelect::deniedSound
	AudioClip_t3680889665 * ___deniedSound_5;
	// LevelSelectPanel[] LevelSelect::levelSelectPanels
	LevelSelectPanelU5BU5D_t1747102860* ___levelSelectPanels_6;
	// LevelChanger LevelSelect::levelChanger
	LevelChanger_t225386971 * ___levelChanger_7;
	// System.Boolean LevelSelect::leftAxesInUse
	bool ___leftAxesInUse_8;
	// System.Boolean LevelSelect::rightAxesInUse
	bool ___rightAxesInUse_9;
	// System.Boolean LevelSelect::confirmAxesInUse
	bool ___confirmAxesInUse_10;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_selectSound_3() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___selectSound_3)); }
	inline AudioClip_t3680889665 * get_selectSound_3() const { return ___selectSound_3; }
	inline AudioClip_t3680889665 ** get_address_of_selectSound_3() { return &___selectSound_3; }
	inline void set_selectSound_3(AudioClip_t3680889665 * value)
	{
		___selectSound_3 = value;
		Il2CppCodeGenWriteBarrier((&___selectSound_3), value);
	}

	inline static int32_t get_offset_of_confirmSound_4() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___confirmSound_4)); }
	inline AudioClip_t3680889665 * get_confirmSound_4() const { return ___confirmSound_4; }
	inline AudioClip_t3680889665 ** get_address_of_confirmSound_4() { return &___confirmSound_4; }
	inline void set_confirmSound_4(AudioClip_t3680889665 * value)
	{
		___confirmSound_4 = value;
		Il2CppCodeGenWriteBarrier((&___confirmSound_4), value);
	}

	inline static int32_t get_offset_of_deniedSound_5() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___deniedSound_5)); }
	inline AudioClip_t3680889665 * get_deniedSound_5() const { return ___deniedSound_5; }
	inline AudioClip_t3680889665 ** get_address_of_deniedSound_5() { return &___deniedSound_5; }
	inline void set_deniedSound_5(AudioClip_t3680889665 * value)
	{
		___deniedSound_5 = value;
		Il2CppCodeGenWriteBarrier((&___deniedSound_5), value);
	}

	inline static int32_t get_offset_of_levelSelectPanels_6() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___levelSelectPanels_6)); }
	inline LevelSelectPanelU5BU5D_t1747102860* get_levelSelectPanels_6() const { return ___levelSelectPanels_6; }
	inline LevelSelectPanelU5BU5D_t1747102860** get_address_of_levelSelectPanels_6() { return &___levelSelectPanels_6; }
	inline void set_levelSelectPanels_6(LevelSelectPanelU5BU5D_t1747102860* value)
	{
		___levelSelectPanels_6 = value;
		Il2CppCodeGenWriteBarrier((&___levelSelectPanels_6), value);
	}

	inline static int32_t get_offset_of_levelChanger_7() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___levelChanger_7)); }
	inline LevelChanger_t225386971 * get_levelChanger_7() const { return ___levelChanger_7; }
	inline LevelChanger_t225386971 ** get_address_of_levelChanger_7() { return &___levelChanger_7; }
	inline void set_levelChanger_7(LevelChanger_t225386971 * value)
	{
		___levelChanger_7 = value;
		Il2CppCodeGenWriteBarrier((&___levelChanger_7), value);
	}

	inline static int32_t get_offset_of_leftAxesInUse_8() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___leftAxesInUse_8)); }
	inline bool get_leftAxesInUse_8() const { return ___leftAxesInUse_8; }
	inline bool* get_address_of_leftAxesInUse_8() { return &___leftAxesInUse_8; }
	inline void set_leftAxesInUse_8(bool value)
	{
		___leftAxesInUse_8 = value;
	}

	inline static int32_t get_offset_of_rightAxesInUse_9() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___rightAxesInUse_9)); }
	inline bool get_rightAxesInUse_9() const { return ___rightAxesInUse_9; }
	inline bool* get_address_of_rightAxesInUse_9() { return &___rightAxesInUse_9; }
	inline void set_rightAxesInUse_9(bool value)
	{
		___rightAxesInUse_9 = value;
	}

	inline static int32_t get_offset_of_confirmAxesInUse_10() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___confirmAxesInUse_10)); }
	inline bool get_confirmAxesInUse_10() const { return ___confirmAxesInUse_10; }
	inline bool* get_address_of_confirmAxesInUse_10() { return &___confirmAxesInUse_10; }
	inline void set_confirmAxesInUse_10(bool value)
	{
		___confirmAxesInUse_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSELECT_T2669120976_H
#ifndef PATHFINDING_T1696161914_H
#define PATHFINDING_T1696161914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding
struct  Pathfinding_t1696161914  : public MonoBehaviour_t3962482529
{
public:
	// Grid Pathfinding::grid
	Grid_t1081586032 * ___grid_2;

public:
	inline static int32_t get_offset_of_grid_2() { return static_cast<int32_t>(offsetof(Pathfinding_t1696161914, ___grid_2)); }
	inline Grid_t1081586032 * get_grid_2() const { return ___grid_2; }
	inline Grid_t1081586032 ** get_address_of_grid_2() { return &___grid_2; }
	inline void set_grid_2(Grid_t1081586032 * value)
	{
		___grid_2 = value;
		Il2CppCodeGenWriteBarrier((&___grid_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHFINDING_T1696161914_H
#ifndef INVENTORYSLOT_T3299309524_H
#define INVENTORYSLOT_T3299309524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventorySlot
struct  InventorySlot_t3299309524  : public MonoBehaviour_t3962482529
{
public:
	// Item InventorySlot::item
	Item_t2953980098 * ___item_2;
	// InventorySlot/SlotType InventorySlot::slotType
	int32_t ___slotType_3;
	// UnityEngine.UI.Image InventorySlot::icon
	Image_t2670269651 * ___icon_4;
	// UnityEngine.GameObject InventorySlot::button
	GameObject_t1113636619 * ___button_5;
	// UnityEngine.UI.Text InventorySlot::itemName
	Text_t1901882714 * ___itemName_6;

public:
	inline static int32_t get_offset_of_item_2() { return static_cast<int32_t>(offsetof(InventorySlot_t3299309524, ___item_2)); }
	inline Item_t2953980098 * get_item_2() const { return ___item_2; }
	inline Item_t2953980098 ** get_address_of_item_2() { return &___item_2; }
	inline void set_item_2(Item_t2953980098 * value)
	{
		___item_2 = value;
		Il2CppCodeGenWriteBarrier((&___item_2), value);
	}

	inline static int32_t get_offset_of_slotType_3() { return static_cast<int32_t>(offsetof(InventorySlot_t3299309524, ___slotType_3)); }
	inline int32_t get_slotType_3() const { return ___slotType_3; }
	inline int32_t* get_address_of_slotType_3() { return &___slotType_3; }
	inline void set_slotType_3(int32_t value)
	{
		___slotType_3 = value;
	}

	inline static int32_t get_offset_of_icon_4() { return static_cast<int32_t>(offsetof(InventorySlot_t3299309524, ___icon_4)); }
	inline Image_t2670269651 * get_icon_4() const { return ___icon_4; }
	inline Image_t2670269651 ** get_address_of_icon_4() { return &___icon_4; }
	inline void set_icon_4(Image_t2670269651 * value)
	{
		___icon_4 = value;
		Il2CppCodeGenWriteBarrier((&___icon_4), value);
	}

	inline static int32_t get_offset_of_button_5() { return static_cast<int32_t>(offsetof(InventorySlot_t3299309524, ___button_5)); }
	inline GameObject_t1113636619 * get_button_5() const { return ___button_5; }
	inline GameObject_t1113636619 ** get_address_of_button_5() { return &___button_5; }
	inline void set_button_5(GameObject_t1113636619 * value)
	{
		___button_5 = value;
		Il2CppCodeGenWriteBarrier((&___button_5), value);
	}

	inline static int32_t get_offset_of_itemName_6() { return static_cast<int32_t>(offsetof(InventorySlot_t3299309524, ___itemName_6)); }
	inline Text_t1901882714 * get_itemName_6() const { return ___itemName_6; }
	inline Text_t1901882714 ** get_address_of_itemName_6() { return &___itemName_6; }
	inline void set_itemName_6(Text_t1901882714 * value)
	{
		___itemName_6 = value;
		Il2CppCodeGenWriteBarrier((&___itemName_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORYSLOT_T3299309524_H
#ifndef INVENTORY_T1050226016_H
#define INVENTORY_T1050226016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Inventory
struct  Inventory_t1050226016  : public MonoBehaviour_t3962482529
{
public:
	// Inventory/OnItemChanged Inventory::onItemChangedCallback
	OnItemChanged_t22848112 * ___onItemChangedCallback_3;
	// System.Int32 Inventory::weaponSpace
	int32_t ___weaponSpace_4;
	// System.Int32 Inventory::shieldSpace
	int32_t ___shieldSpace_5;
	// Item Inventory::weapon
	Item_t2953980098 * ___weapon_6;
	// Item Inventory::shield
	Item_t2953980098 * ___shield_7;

public:
	inline static int32_t get_offset_of_onItemChangedCallback_3() { return static_cast<int32_t>(offsetof(Inventory_t1050226016, ___onItemChangedCallback_3)); }
	inline OnItemChanged_t22848112 * get_onItemChangedCallback_3() const { return ___onItemChangedCallback_3; }
	inline OnItemChanged_t22848112 ** get_address_of_onItemChangedCallback_3() { return &___onItemChangedCallback_3; }
	inline void set_onItemChangedCallback_3(OnItemChanged_t22848112 * value)
	{
		___onItemChangedCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___onItemChangedCallback_3), value);
	}

	inline static int32_t get_offset_of_weaponSpace_4() { return static_cast<int32_t>(offsetof(Inventory_t1050226016, ___weaponSpace_4)); }
	inline int32_t get_weaponSpace_4() const { return ___weaponSpace_4; }
	inline int32_t* get_address_of_weaponSpace_4() { return &___weaponSpace_4; }
	inline void set_weaponSpace_4(int32_t value)
	{
		___weaponSpace_4 = value;
	}

	inline static int32_t get_offset_of_shieldSpace_5() { return static_cast<int32_t>(offsetof(Inventory_t1050226016, ___shieldSpace_5)); }
	inline int32_t get_shieldSpace_5() const { return ___shieldSpace_5; }
	inline int32_t* get_address_of_shieldSpace_5() { return &___shieldSpace_5; }
	inline void set_shieldSpace_5(int32_t value)
	{
		___shieldSpace_5 = value;
	}

	inline static int32_t get_offset_of_weapon_6() { return static_cast<int32_t>(offsetof(Inventory_t1050226016, ___weapon_6)); }
	inline Item_t2953980098 * get_weapon_6() const { return ___weapon_6; }
	inline Item_t2953980098 ** get_address_of_weapon_6() { return &___weapon_6; }
	inline void set_weapon_6(Item_t2953980098 * value)
	{
		___weapon_6 = value;
		Il2CppCodeGenWriteBarrier((&___weapon_6), value);
	}

	inline static int32_t get_offset_of_shield_7() { return static_cast<int32_t>(offsetof(Inventory_t1050226016, ___shield_7)); }
	inline Item_t2953980098 * get_shield_7() const { return ___shield_7; }
	inline Item_t2953980098 ** get_address_of_shield_7() { return &___shield_7; }
	inline void set_shield_7(Item_t2953980098 * value)
	{
		___shield_7 = value;
		Il2CppCodeGenWriteBarrier((&___shield_7), value);
	}
};

struct Inventory_t1050226016_StaticFields
{
public:
	// Inventory Inventory::instance
	Inventory_t1050226016 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Inventory_t1050226016_StaticFields, ___instance_2)); }
	inline Inventory_t1050226016 * get_instance_2() const { return ___instance_2; }
	inline Inventory_t1050226016 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(Inventory_t1050226016 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORY_T1050226016_H
#ifndef ROTATINGLIGHTS_T2402942693_H
#define ROTATINGLIGHTS_T2402942693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotatingLights
struct  RotatingLights_t2402942693  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject RotatingLights::lights1
	GameObject_t1113636619 * ___lights1_2;
	// UnityEngine.GameObject RotatingLights::lights2
	GameObject_t1113636619 * ___lights2_3;
	// System.Single RotatingLights::rotationSpeed
	float ___rotationSpeed_4;

public:
	inline static int32_t get_offset_of_lights1_2() { return static_cast<int32_t>(offsetof(RotatingLights_t2402942693, ___lights1_2)); }
	inline GameObject_t1113636619 * get_lights1_2() const { return ___lights1_2; }
	inline GameObject_t1113636619 ** get_address_of_lights1_2() { return &___lights1_2; }
	inline void set_lights1_2(GameObject_t1113636619 * value)
	{
		___lights1_2 = value;
		Il2CppCodeGenWriteBarrier((&___lights1_2), value);
	}

	inline static int32_t get_offset_of_lights2_3() { return static_cast<int32_t>(offsetof(RotatingLights_t2402942693, ___lights2_3)); }
	inline GameObject_t1113636619 * get_lights2_3() const { return ___lights2_3; }
	inline GameObject_t1113636619 ** get_address_of_lights2_3() { return &___lights2_3; }
	inline void set_lights2_3(GameObject_t1113636619 * value)
	{
		___lights2_3 = value;
		Il2CppCodeGenWriteBarrier((&___lights2_3), value);
	}

	inline static int32_t get_offset_of_rotationSpeed_4() { return static_cast<int32_t>(offsetof(RotatingLights_t2402942693, ___rotationSpeed_4)); }
	inline float get_rotationSpeed_4() const { return ___rotationSpeed_4; }
	inline float* get_address_of_rotationSpeed_4() { return &___rotationSpeed_4; }
	inline void set_rotationSpeed_4(float value)
	{
		___rotationSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATINGLIGHTS_T2402942693_H
#ifndef SOFTNORMALSTOVERTEXCOLOR_T279782399_H
#define SOFTNORMALSTOVERTEXCOLOR_T279782399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoftNormalsToVertexColor
struct  SoftNormalsToVertexColor_t279782399  : public MonoBehaviour_t3962482529
{
public:
	// SoftNormalsToVertexColor/Method SoftNormalsToVertexColor::method
	int32_t ___method_2;
	// System.Boolean SoftNormalsToVertexColor::generateOnAwake
	bool ___generateOnAwake_3;
	// System.Boolean SoftNormalsToVertexColor::generateNow
	bool ___generateNow_4;

public:
	inline static int32_t get_offset_of_method_2() { return static_cast<int32_t>(offsetof(SoftNormalsToVertexColor_t279782399, ___method_2)); }
	inline int32_t get_method_2() const { return ___method_2; }
	inline int32_t* get_address_of_method_2() { return &___method_2; }
	inline void set_method_2(int32_t value)
	{
		___method_2 = value;
	}

	inline static int32_t get_offset_of_generateOnAwake_3() { return static_cast<int32_t>(offsetof(SoftNormalsToVertexColor_t279782399, ___generateOnAwake_3)); }
	inline bool get_generateOnAwake_3() const { return ___generateOnAwake_3; }
	inline bool* get_address_of_generateOnAwake_3() { return &___generateOnAwake_3; }
	inline void set_generateOnAwake_3(bool value)
	{
		___generateOnAwake_3 = value;
	}

	inline static int32_t get_offset_of_generateNow_4() { return static_cast<int32_t>(offsetof(SoftNormalsToVertexColor_t279782399, ___generateNow_4)); }
	inline bool get_generateNow_4() const { return ___generateNow_4; }
	inline bool* get_address_of_generateNow_4() { return &___generateNow_4; }
	inline void set_generateNow_4(bool value)
	{
		___generateNow_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOFTNORMALSTOVERTEXCOLOR_T279782399_H
#ifndef SELECTTITLEOPTIONS_T2341392585_H
#define SELECTTITLEOPTIONS_T2341392585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectTitleOptions
struct  SelectTitleOptions_t2341392585  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 SelectTitleOptions::index
	int32_t ___index_2;
	// UnityEngine.AudioClip SelectTitleOptions::selectSound
	AudioClip_t3680889665 * ___selectSound_3;
	// UnityEngine.AudioClip SelectTitleOptions::confirmSound
	AudioClip_t3680889665 * ___confirmSound_4;
	// System.String SelectTitleOptions::newGameSceneName
	String_t* ___newGameSceneName_5;
	// UnityEngine.GameObject[] SelectTitleOptions::options
	GameObjectU5BU5D_t3328599146* ___options_6;
	// UnityEngine.Sprite[] SelectTitleOptions::titleOptionBox
	SpriteU5BU5D_t2581906349* ___titleOptionBox_7;
	// LevelChanger SelectTitleOptions::levelChanger
	LevelChanger_t225386971 * ___levelChanger_8;
	// System.Boolean SelectTitleOptions::leftAxesInUse
	bool ___leftAxesInUse_9;
	// System.Boolean SelectTitleOptions::rightAxesInUse
	bool ___rightAxesInUse_10;
	// System.Boolean SelectTitleOptions::confirmAxesInUse
	bool ___confirmAxesInUse_11;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_selectSound_3() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___selectSound_3)); }
	inline AudioClip_t3680889665 * get_selectSound_3() const { return ___selectSound_3; }
	inline AudioClip_t3680889665 ** get_address_of_selectSound_3() { return &___selectSound_3; }
	inline void set_selectSound_3(AudioClip_t3680889665 * value)
	{
		___selectSound_3 = value;
		Il2CppCodeGenWriteBarrier((&___selectSound_3), value);
	}

	inline static int32_t get_offset_of_confirmSound_4() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___confirmSound_4)); }
	inline AudioClip_t3680889665 * get_confirmSound_4() const { return ___confirmSound_4; }
	inline AudioClip_t3680889665 ** get_address_of_confirmSound_4() { return &___confirmSound_4; }
	inline void set_confirmSound_4(AudioClip_t3680889665 * value)
	{
		___confirmSound_4 = value;
		Il2CppCodeGenWriteBarrier((&___confirmSound_4), value);
	}

	inline static int32_t get_offset_of_newGameSceneName_5() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___newGameSceneName_5)); }
	inline String_t* get_newGameSceneName_5() const { return ___newGameSceneName_5; }
	inline String_t** get_address_of_newGameSceneName_5() { return &___newGameSceneName_5; }
	inline void set_newGameSceneName_5(String_t* value)
	{
		___newGameSceneName_5 = value;
		Il2CppCodeGenWriteBarrier((&___newGameSceneName_5), value);
	}

	inline static int32_t get_offset_of_options_6() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___options_6)); }
	inline GameObjectU5BU5D_t3328599146* get_options_6() const { return ___options_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_options_6() { return &___options_6; }
	inline void set_options_6(GameObjectU5BU5D_t3328599146* value)
	{
		___options_6 = value;
		Il2CppCodeGenWriteBarrier((&___options_6), value);
	}

	inline static int32_t get_offset_of_titleOptionBox_7() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___titleOptionBox_7)); }
	inline SpriteU5BU5D_t2581906349* get_titleOptionBox_7() const { return ___titleOptionBox_7; }
	inline SpriteU5BU5D_t2581906349** get_address_of_titleOptionBox_7() { return &___titleOptionBox_7; }
	inline void set_titleOptionBox_7(SpriteU5BU5D_t2581906349* value)
	{
		___titleOptionBox_7 = value;
		Il2CppCodeGenWriteBarrier((&___titleOptionBox_7), value);
	}

	inline static int32_t get_offset_of_levelChanger_8() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___levelChanger_8)); }
	inline LevelChanger_t225386971 * get_levelChanger_8() const { return ___levelChanger_8; }
	inline LevelChanger_t225386971 ** get_address_of_levelChanger_8() { return &___levelChanger_8; }
	inline void set_levelChanger_8(LevelChanger_t225386971 * value)
	{
		___levelChanger_8 = value;
		Il2CppCodeGenWriteBarrier((&___levelChanger_8), value);
	}

	inline static int32_t get_offset_of_leftAxesInUse_9() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___leftAxesInUse_9)); }
	inline bool get_leftAxesInUse_9() const { return ___leftAxesInUse_9; }
	inline bool* get_address_of_leftAxesInUse_9() { return &___leftAxesInUse_9; }
	inline void set_leftAxesInUse_9(bool value)
	{
		___leftAxesInUse_9 = value;
	}

	inline static int32_t get_offset_of_rightAxesInUse_10() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___rightAxesInUse_10)); }
	inline bool get_rightAxesInUse_10() const { return ___rightAxesInUse_10; }
	inline bool* get_address_of_rightAxesInUse_10() { return &___rightAxesInUse_10; }
	inline void set_rightAxesInUse_10(bool value)
	{
		___rightAxesInUse_10 = value;
	}

	inline static int32_t get_offset_of_confirmAxesInUse_11() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___confirmAxesInUse_11)); }
	inline bool get_confirmAxesInUse_11() const { return ___confirmAxesInUse_11; }
	inline bool* get_address_of_confirmAxesInUse_11() { return &___confirmAxesInUse_11; }
	inline void set_confirmAxesInUse_11(bool value)
	{
		___confirmAxesInUse_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTTITLEOPTIONS_T2341392585_H
#ifndef SHIELD_T1860136854_H
#define SHIELD_T1860136854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shield
struct  Shield_t1860136854  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 Shield::uses
	int32_t ___uses_2;

public:
	inline static int32_t get_offset_of_uses_2() { return static_cast<int32_t>(offsetof(Shield_t1860136854, ___uses_2)); }
	inline int32_t get_uses_2() const { return ___uses_2; }
	inline int32_t* get_address_of_uses_2() { return &___uses_2; }
	inline void set_uses_2(int32_t value)
	{
		___uses_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIELD_T1860136854_H
#ifndef INTERACTIBLE_T1060880155_H
#define INTERACTIBLE_T1060880155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Interactible
struct  Interactible_t1060880155  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Interactible::radius
	float ___radius_2;
	// UnityEngine.Transform Interactible::interactionTransform
	Transform_t3600365921 * ___interactionTransform_3;

public:
	inline static int32_t get_offset_of_radius_2() { return static_cast<int32_t>(offsetof(Interactible_t1060880155, ___radius_2)); }
	inline float get_radius_2() const { return ___radius_2; }
	inline float* get_address_of_radius_2() { return &___radius_2; }
	inline void set_radius_2(float value)
	{
		___radius_2 = value;
	}

	inline static int32_t get_offset_of_interactionTransform_3() { return static_cast<int32_t>(offsetof(Interactible_t1060880155, ___interactionTransform_3)); }
	inline Transform_t3600365921 * get_interactionTransform_3() const { return ___interactionTransform_3; }
	inline Transform_t3600365921 ** get_address_of_interactionTransform_3() { return &___interactionTransform_3; }
	inline void set_interactionTransform_3(Transform_t3600365921 * value)
	{
		___interactionTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___interactionTransform_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIBLE_T1060880155_H
#ifndef GUN_T783153271_H
#define GUN_T783153271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gun
struct  Gun_t783153271  : public MonoBehaviour_t3962482529
{
public:
	// Gun/GunType Gun::gunType
	int32_t ___gunType_2;
	// System.Single Gun::rpm
	float ___rpm_3;
	// UnityEngine.AudioClip Gun::gunShot
	AudioClip_t3680889665 * ___gunShot_4;
	// UnityEngine.AudioClip Gun::laserShot
	AudioClip_t3680889665 * ___laserShot_5;
	// UnityEngine.Transform Gun::spawn
	Transform_t3600365921 * ___spawn_6;
	// UnityEngine.GameObject Gun::shell
	GameObject_t1113636619 * ___shell_7;
	// UnityEngine.GameObject Gun::laser
	GameObject_t1113636619 * ___laser_8;
	// System.Int32 Gun::rounds
	int32_t ___rounds_9;
	// System.Int32 Gun::uses
	int32_t ___uses_10;
	// System.Single Gun::secondsBetweenShots
	float ___secondsBetweenShots_11;
	// System.Single Gun::nextPossibleShootTime
	float ___nextPossibleShootTime_12;

public:
	inline static int32_t get_offset_of_gunType_2() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___gunType_2)); }
	inline int32_t get_gunType_2() const { return ___gunType_2; }
	inline int32_t* get_address_of_gunType_2() { return &___gunType_2; }
	inline void set_gunType_2(int32_t value)
	{
		___gunType_2 = value;
	}

	inline static int32_t get_offset_of_rpm_3() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___rpm_3)); }
	inline float get_rpm_3() const { return ___rpm_3; }
	inline float* get_address_of_rpm_3() { return &___rpm_3; }
	inline void set_rpm_3(float value)
	{
		___rpm_3 = value;
	}

	inline static int32_t get_offset_of_gunShot_4() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___gunShot_4)); }
	inline AudioClip_t3680889665 * get_gunShot_4() const { return ___gunShot_4; }
	inline AudioClip_t3680889665 ** get_address_of_gunShot_4() { return &___gunShot_4; }
	inline void set_gunShot_4(AudioClip_t3680889665 * value)
	{
		___gunShot_4 = value;
		Il2CppCodeGenWriteBarrier((&___gunShot_4), value);
	}

	inline static int32_t get_offset_of_laserShot_5() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___laserShot_5)); }
	inline AudioClip_t3680889665 * get_laserShot_5() const { return ___laserShot_5; }
	inline AudioClip_t3680889665 ** get_address_of_laserShot_5() { return &___laserShot_5; }
	inline void set_laserShot_5(AudioClip_t3680889665 * value)
	{
		___laserShot_5 = value;
		Il2CppCodeGenWriteBarrier((&___laserShot_5), value);
	}

	inline static int32_t get_offset_of_spawn_6() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___spawn_6)); }
	inline Transform_t3600365921 * get_spawn_6() const { return ___spawn_6; }
	inline Transform_t3600365921 ** get_address_of_spawn_6() { return &___spawn_6; }
	inline void set_spawn_6(Transform_t3600365921 * value)
	{
		___spawn_6 = value;
		Il2CppCodeGenWriteBarrier((&___spawn_6), value);
	}

	inline static int32_t get_offset_of_shell_7() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___shell_7)); }
	inline GameObject_t1113636619 * get_shell_7() const { return ___shell_7; }
	inline GameObject_t1113636619 ** get_address_of_shell_7() { return &___shell_7; }
	inline void set_shell_7(GameObject_t1113636619 * value)
	{
		___shell_7 = value;
		Il2CppCodeGenWriteBarrier((&___shell_7), value);
	}

	inline static int32_t get_offset_of_laser_8() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___laser_8)); }
	inline GameObject_t1113636619 * get_laser_8() const { return ___laser_8; }
	inline GameObject_t1113636619 ** get_address_of_laser_8() { return &___laser_8; }
	inline void set_laser_8(GameObject_t1113636619 * value)
	{
		___laser_8 = value;
		Il2CppCodeGenWriteBarrier((&___laser_8), value);
	}

	inline static int32_t get_offset_of_rounds_9() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___rounds_9)); }
	inline int32_t get_rounds_9() const { return ___rounds_9; }
	inline int32_t* get_address_of_rounds_9() { return &___rounds_9; }
	inline void set_rounds_9(int32_t value)
	{
		___rounds_9 = value;
	}

	inline static int32_t get_offset_of_uses_10() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___uses_10)); }
	inline int32_t get_uses_10() const { return ___uses_10; }
	inline int32_t* get_address_of_uses_10() { return &___uses_10; }
	inline void set_uses_10(int32_t value)
	{
		___uses_10 = value;
	}

	inline static int32_t get_offset_of_secondsBetweenShots_11() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___secondsBetweenShots_11)); }
	inline float get_secondsBetweenShots_11() const { return ___secondsBetweenShots_11; }
	inline float* get_address_of_secondsBetweenShots_11() { return &___secondsBetweenShots_11; }
	inline void set_secondsBetweenShots_11(float value)
	{
		___secondsBetweenShots_11 = value;
	}

	inline static int32_t get_offset_of_nextPossibleShootTime_12() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___nextPossibleShootTime_12)); }
	inline float get_nextPossibleShootTime_12() const { return ___nextPossibleShootTime_12; }
	inline float* get_address_of_nextPossibleShootTime_12() { return &___nextPossibleShootTime_12; }
	inline void set_nextPossibleShootTime_12(float value)
	{
		___nextPossibleShootTime_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUN_T783153271_H
#ifndef PLAYERHEALTHUI_T636700963_H
#define PLAYERHEALTHUI_T636700963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerHealthUI
struct  PlayerHealthUI_t636700963  : public MonoBehaviour_t3962482529
{
public:
	// PlayerStats PlayerHealthUI::pStats
	PlayerStats_t2044123780 * ___pStats_2;
	// UnityEngine.UI.Text PlayerHealthUI::pHealthValue
	Text_t1901882714 * ___pHealthValue_3;

public:
	inline static int32_t get_offset_of_pStats_2() { return static_cast<int32_t>(offsetof(PlayerHealthUI_t636700963, ___pStats_2)); }
	inline PlayerStats_t2044123780 * get_pStats_2() const { return ___pStats_2; }
	inline PlayerStats_t2044123780 ** get_address_of_pStats_2() { return &___pStats_2; }
	inline void set_pStats_2(PlayerStats_t2044123780 * value)
	{
		___pStats_2 = value;
		Il2CppCodeGenWriteBarrier((&___pStats_2), value);
	}

	inline static int32_t get_offset_of_pHealthValue_3() { return static_cast<int32_t>(offsetof(PlayerHealthUI_t636700963, ___pHealthValue_3)); }
	inline Text_t1901882714 * get_pHealthValue_3() const { return ___pHealthValue_3; }
	inline Text_t1901882714 ** get_address_of_pHealthValue_3() { return &___pHealthValue_3; }
	inline void set_pHealthValue_3(Text_t1901882714 * value)
	{
		___pHealthValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___pHealthValue_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERHEALTHUI_T636700963_H
#ifndef PLAYERCONTROLLER_T2064355688_H
#define PLAYERCONTROLLER_T2064355688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerController
struct  PlayerController_t2064355688  : public MonoBehaviour_t3962482529
{
public:
	// Inventory PlayerController::inventory
	Inventory_t1050226016 * ___inventory_2;
	// System.Boolean PlayerController::laserGunEnabled
	bool ___laserGunEnabled_3;
	// System.Boolean PlayerController::laserShieldEnabled
	bool ___laserShieldEnabled_4;
	// Gun PlayerController::gun
	Gun_t783153271 * ___gun_5;
	// System.Single PlayerController::moveSpeed
	float ___moveSpeed_6;
	// System.Single PlayerController::turnSpeed
	float ___turnSpeed_7;
	// UnityEngine.Camera PlayerController::ourCamera
	Camera_t4157153871 * ___ourCamera_8;
	// System.Single PlayerController::reloadTime
	float ___reloadTime_9;

public:
	inline static int32_t get_offset_of_inventory_2() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___inventory_2)); }
	inline Inventory_t1050226016 * get_inventory_2() const { return ___inventory_2; }
	inline Inventory_t1050226016 ** get_address_of_inventory_2() { return &___inventory_2; }
	inline void set_inventory_2(Inventory_t1050226016 * value)
	{
		___inventory_2 = value;
		Il2CppCodeGenWriteBarrier((&___inventory_2), value);
	}

	inline static int32_t get_offset_of_laserGunEnabled_3() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___laserGunEnabled_3)); }
	inline bool get_laserGunEnabled_3() const { return ___laserGunEnabled_3; }
	inline bool* get_address_of_laserGunEnabled_3() { return &___laserGunEnabled_3; }
	inline void set_laserGunEnabled_3(bool value)
	{
		___laserGunEnabled_3 = value;
	}

	inline static int32_t get_offset_of_laserShieldEnabled_4() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___laserShieldEnabled_4)); }
	inline bool get_laserShieldEnabled_4() const { return ___laserShieldEnabled_4; }
	inline bool* get_address_of_laserShieldEnabled_4() { return &___laserShieldEnabled_4; }
	inline void set_laserShieldEnabled_4(bool value)
	{
		___laserShieldEnabled_4 = value;
	}

	inline static int32_t get_offset_of_gun_5() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___gun_5)); }
	inline Gun_t783153271 * get_gun_5() const { return ___gun_5; }
	inline Gun_t783153271 ** get_address_of_gun_5() { return &___gun_5; }
	inline void set_gun_5(Gun_t783153271 * value)
	{
		___gun_5 = value;
		Il2CppCodeGenWriteBarrier((&___gun_5), value);
	}

	inline static int32_t get_offset_of_moveSpeed_6() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___moveSpeed_6)); }
	inline float get_moveSpeed_6() const { return ___moveSpeed_6; }
	inline float* get_address_of_moveSpeed_6() { return &___moveSpeed_6; }
	inline void set_moveSpeed_6(float value)
	{
		___moveSpeed_6 = value;
	}

	inline static int32_t get_offset_of_turnSpeed_7() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___turnSpeed_7)); }
	inline float get_turnSpeed_7() const { return ___turnSpeed_7; }
	inline float* get_address_of_turnSpeed_7() { return &___turnSpeed_7; }
	inline void set_turnSpeed_7(float value)
	{
		___turnSpeed_7 = value;
	}

	inline static int32_t get_offset_of_ourCamera_8() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___ourCamera_8)); }
	inline Camera_t4157153871 * get_ourCamera_8() const { return ___ourCamera_8; }
	inline Camera_t4157153871 ** get_address_of_ourCamera_8() { return &___ourCamera_8; }
	inline void set_ourCamera_8(Camera_t4157153871 * value)
	{
		___ourCamera_8 = value;
		Il2CppCodeGenWriteBarrier((&___ourCamera_8), value);
	}

	inline static int32_t get_offset_of_reloadTime_9() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___reloadTime_9)); }
	inline float get_reloadTime_9() const { return ___reloadTime_9; }
	inline float* get_address_of_reloadTime_9() { return &___reloadTime_9; }
	inline void set_reloadTime_9(float value)
	{
		___reloadTime_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLLER_T2064355688_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_3)); }
	inline Navigation_t3049316579  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t3049316579  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_5)); }
	inline ColorBlock_t2139031574  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2139031574  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_6)); }
	inline SpriteState_t1362986479  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1362986479  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_9)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_15)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_2)); }
	inline List_1_t427135887 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t427135887 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t427135887 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_4)); }
	inline Material_t340375123 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t340375123 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t340375123 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_7)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_8)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_8() const { return ___m_CanvasRenderer_8; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_8() { return &___m_CanvasRenderer_8; }
	inline void set_m_CanvasRenderer_8(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_9)); }
	inline Canvas_t3310196443 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t3310196443 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t340375123 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t340375123 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3648964284 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3648964284 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef ITEMPICKUP_T4277059944_H
#define ITEMPICKUP_T4277059944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemPickup
struct  ItemPickup_t4277059944  : public Interactible_t1060880155
{
public:
	// Item ItemPickup::item
	Item_t2953980098 * ___item_4;
	// UnityEngine.AudioClip ItemPickup::pickupSound
	AudioClip_t3680889665 * ___pickupSound_5;

public:
	inline static int32_t get_offset_of_item_4() { return static_cast<int32_t>(offsetof(ItemPickup_t4277059944, ___item_4)); }
	inline Item_t2953980098 * get_item_4() const { return ___item_4; }
	inline Item_t2953980098 ** get_address_of_item_4() { return &___item_4; }
	inline void set_item_4(Item_t2953980098 * value)
	{
		___item_4 = value;
		Il2CppCodeGenWriteBarrier((&___item_4), value);
	}

	inline static int32_t get_offset_of_pickupSound_5() { return static_cast<int32_t>(offsetof(ItemPickup_t4277059944, ___pickupSound_5)); }
	inline AudioClip_t3680889665 * get_pickupSound_5() const { return ___pickupSound_5; }
	inline AudioClip_t3680889665 ** get_address_of_pickupSound_5() { return &___pickupSound_5; }
	inline void set_pickupSound_5(AudioClip_t3680889665 * value)
	{
		___pickupSound_5 = value;
		Il2CppCodeGenWriteBarrier((&___pickupSound_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMPICKUP_T4277059944_H
#ifndef INPUTFIELD_T3762917431_H
#define INPUTFIELD_T3762917431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.InputField
struct  InputField_t3762917431  : public Selectable_t3250028441
{
public:
	// UnityEngine.TouchScreenKeyboard UnityEngine.UI.InputField::m_Keyboard
	TouchScreenKeyboard_t731888065 * ___m_Keyboard_16;
	// UnityEngine.UI.Text UnityEngine.UI.InputField::m_TextComponent
	Text_t1901882714 * ___m_TextComponent_18;
	// UnityEngine.UI.Graphic UnityEngine.UI.InputField::m_Placeholder
	Graphic_t1660335611 * ___m_Placeholder_19;
	// UnityEngine.UI.InputField/ContentType UnityEngine.UI.InputField::m_ContentType
	int32_t ___m_ContentType_20;
	// UnityEngine.UI.InputField/InputType UnityEngine.UI.InputField::m_InputType
	int32_t ___m_InputType_21;
	// System.Char UnityEngine.UI.InputField::m_AsteriskChar
	Il2CppChar ___m_AsteriskChar_22;
	// UnityEngine.TouchScreenKeyboardType UnityEngine.UI.InputField::m_KeyboardType
	int32_t ___m_KeyboardType_23;
	// UnityEngine.UI.InputField/LineType UnityEngine.UI.InputField::m_LineType
	int32_t ___m_LineType_24;
	// System.Boolean UnityEngine.UI.InputField::m_HideMobileInput
	bool ___m_HideMobileInput_25;
	// UnityEngine.UI.InputField/CharacterValidation UnityEngine.UI.InputField::m_CharacterValidation
	int32_t ___m_CharacterValidation_26;
	// System.Int32 UnityEngine.UI.InputField::m_CharacterLimit
	int32_t ___m_CharacterLimit_27;
	// UnityEngine.UI.InputField/SubmitEvent UnityEngine.UI.InputField::m_OnEndEdit
	SubmitEvent_t648412432 * ___m_OnEndEdit_28;
	// UnityEngine.UI.InputField/OnChangeEvent UnityEngine.UI.InputField::m_OnValueChanged
	OnChangeEvent_t467195904 * ___m_OnValueChanged_29;
	// UnityEngine.UI.InputField/OnValidateInput UnityEngine.UI.InputField::m_OnValidateInput
	OnValidateInput_t2355412304 * ___m_OnValidateInput_30;
	// UnityEngine.Color UnityEngine.UI.InputField::m_CaretColor
	Color_t2555686324  ___m_CaretColor_31;
	// System.Boolean UnityEngine.UI.InputField::m_CustomCaretColor
	bool ___m_CustomCaretColor_32;
	// UnityEngine.Color UnityEngine.UI.InputField::m_SelectionColor
	Color_t2555686324  ___m_SelectionColor_33;
	// System.String UnityEngine.UI.InputField::m_Text
	String_t* ___m_Text_34;
	// System.Single UnityEngine.UI.InputField::m_CaretBlinkRate
	float ___m_CaretBlinkRate_35;
	// System.Int32 UnityEngine.UI.InputField::m_CaretWidth
	int32_t ___m_CaretWidth_36;
	// System.Boolean UnityEngine.UI.InputField::m_ReadOnly
	bool ___m_ReadOnly_37;
	// System.Int32 UnityEngine.UI.InputField::m_CaretPosition
	int32_t ___m_CaretPosition_38;
	// System.Int32 UnityEngine.UI.InputField::m_CaretSelectPosition
	int32_t ___m_CaretSelectPosition_39;
	// UnityEngine.RectTransform UnityEngine.UI.InputField::caretRectTrans
	RectTransform_t3704657025 * ___caretRectTrans_40;
	// UnityEngine.UIVertex[] UnityEngine.UI.InputField::m_CursorVerts
	UIVertexU5BU5D_t1981460040* ___m_CursorVerts_41;
	// UnityEngine.TextGenerator UnityEngine.UI.InputField::m_InputTextCache
	TextGenerator_t3211863866 * ___m_InputTextCache_42;
	// UnityEngine.CanvasRenderer UnityEngine.UI.InputField::m_CachedInputRenderer
	CanvasRenderer_t2598313366 * ___m_CachedInputRenderer_43;
	// System.Boolean UnityEngine.UI.InputField::m_PreventFontCallback
	bool ___m_PreventFontCallback_44;
	// UnityEngine.Mesh UnityEngine.UI.InputField::m_Mesh
	Mesh_t3648964284 * ___m_Mesh_45;
	// System.Boolean UnityEngine.UI.InputField::m_AllowInput
	bool ___m_AllowInput_46;
	// System.Boolean UnityEngine.UI.InputField::m_ShouldActivateNextUpdate
	bool ___m_ShouldActivateNextUpdate_47;
	// System.Boolean UnityEngine.UI.InputField::m_UpdateDrag
	bool ___m_UpdateDrag_48;
	// System.Boolean UnityEngine.UI.InputField::m_DragPositionOutOfBounds
	bool ___m_DragPositionOutOfBounds_49;
	// System.Boolean UnityEngine.UI.InputField::m_CaretVisible
	bool ___m_CaretVisible_52;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_BlinkCoroutine
	Coroutine_t3829159415 * ___m_BlinkCoroutine_53;
	// System.Single UnityEngine.UI.InputField::m_BlinkStartTime
	float ___m_BlinkStartTime_54;
	// System.Int32 UnityEngine.UI.InputField::m_DrawStart
	int32_t ___m_DrawStart_55;
	// System.Int32 UnityEngine.UI.InputField::m_DrawEnd
	int32_t ___m_DrawEnd_56;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_DragCoroutine
	Coroutine_t3829159415 * ___m_DragCoroutine_57;
	// System.String UnityEngine.UI.InputField::m_OriginalText
	String_t* ___m_OriginalText_58;
	// System.Boolean UnityEngine.UI.InputField::m_WasCanceled
	bool ___m_WasCanceled_59;
	// System.Boolean UnityEngine.UI.InputField::m_HasDoneFocusTransition
	bool ___m_HasDoneFocusTransition_60;
	// UnityEngine.Event UnityEngine.UI.InputField::m_ProcessingEvent
	Event_t2956885303 * ___m_ProcessingEvent_62;

public:
	inline static int32_t get_offset_of_m_Keyboard_16() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Keyboard_16)); }
	inline TouchScreenKeyboard_t731888065 * get_m_Keyboard_16() const { return ___m_Keyboard_16; }
	inline TouchScreenKeyboard_t731888065 ** get_address_of_m_Keyboard_16() { return &___m_Keyboard_16; }
	inline void set_m_Keyboard_16(TouchScreenKeyboard_t731888065 * value)
	{
		___m_Keyboard_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Keyboard_16), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_18() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_TextComponent_18)); }
	inline Text_t1901882714 * get_m_TextComponent_18() const { return ___m_TextComponent_18; }
	inline Text_t1901882714 ** get_address_of_m_TextComponent_18() { return &___m_TextComponent_18; }
	inline void set_m_TextComponent_18(Text_t1901882714 * value)
	{
		___m_TextComponent_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_18), value);
	}

	inline static int32_t get_offset_of_m_Placeholder_19() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Placeholder_19)); }
	inline Graphic_t1660335611 * get_m_Placeholder_19() const { return ___m_Placeholder_19; }
	inline Graphic_t1660335611 ** get_address_of_m_Placeholder_19() { return &___m_Placeholder_19; }
	inline void set_m_Placeholder_19(Graphic_t1660335611 * value)
	{
		___m_Placeholder_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_Placeholder_19), value);
	}

	inline static int32_t get_offset_of_m_ContentType_20() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ContentType_20)); }
	inline int32_t get_m_ContentType_20() const { return ___m_ContentType_20; }
	inline int32_t* get_address_of_m_ContentType_20() { return &___m_ContentType_20; }
	inline void set_m_ContentType_20(int32_t value)
	{
		___m_ContentType_20 = value;
	}

	inline static int32_t get_offset_of_m_InputType_21() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_InputType_21)); }
	inline int32_t get_m_InputType_21() const { return ___m_InputType_21; }
	inline int32_t* get_address_of_m_InputType_21() { return &___m_InputType_21; }
	inline void set_m_InputType_21(int32_t value)
	{
		___m_InputType_21 = value;
	}

	inline static int32_t get_offset_of_m_AsteriskChar_22() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_AsteriskChar_22)); }
	inline Il2CppChar get_m_AsteriskChar_22() const { return ___m_AsteriskChar_22; }
	inline Il2CppChar* get_address_of_m_AsteriskChar_22() { return &___m_AsteriskChar_22; }
	inline void set_m_AsteriskChar_22(Il2CppChar value)
	{
		___m_AsteriskChar_22 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardType_23() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_KeyboardType_23)); }
	inline int32_t get_m_KeyboardType_23() const { return ___m_KeyboardType_23; }
	inline int32_t* get_address_of_m_KeyboardType_23() { return &___m_KeyboardType_23; }
	inline void set_m_KeyboardType_23(int32_t value)
	{
		___m_KeyboardType_23 = value;
	}

	inline static int32_t get_offset_of_m_LineType_24() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_LineType_24)); }
	inline int32_t get_m_LineType_24() const { return ___m_LineType_24; }
	inline int32_t* get_address_of_m_LineType_24() { return &___m_LineType_24; }
	inline void set_m_LineType_24(int32_t value)
	{
		___m_LineType_24 = value;
	}

	inline static int32_t get_offset_of_m_HideMobileInput_25() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_HideMobileInput_25)); }
	inline bool get_m_HideMobileInput_25() const { return ___m_HideMobileInput_25; }
	inline bool* get_address_of_m_HideMobileInput_25() { return &___m_HideMobileInput_25; }
	inline void set_m_HideMobileInput_25(bool value)
	{
		___m_HideMobileInput_25 = value;
	}

	inline static int32_t get_offset_of_m_CharacterValidation_26() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CharacterValidation_26)); }
	inline int32_t get_m_CharacterValidation_26() const { return ___m_CharacterValidation_26; }
	inline int32_t* get_address_of_m_CharacterValidation_26() { return &___m_CharacterValidation_26; }
	inline void set_m_CharacterValidation_26(int32_t value)
	{
		___m_CharacterValidation_26 = value;
	}

	inline static int32_t get_offset_of_m_CharacterLimit_27() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CharacterLimit_27)); }
	inline int32_t get_m_CharacterLimit_27() const { return ___m_CharacterLimit_27; }
	inline int32_t* get_address_of_m_CharacterLimit_27() { return &___m_CharacterLimit_27; }
	inline void set_m_CharacterLimit_27(int32_t value)
	{
		___m_CharacterLimit_27 = value;
	}

	inline static int32_t get_offset_of_m_OnEndEdit_28() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OnEndEdit_28)); }
	inline SubmitEvent_t648412432 * get_m_OnEndEdit_28() const { return ___m_OnEndEdit_28; }
	inline SubmitEvent_t648412432 ** get_address_of_m_OnEndEdit_28() { return &___m_OnEndEdit_28; }
	inline void set_m_OnEndEdit_28(SubmitEvent_t648412432 * value)
	{
		___m_OnEndEdit_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnEndEdit_28), value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_29() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OnValueChanged_29)); }
	inline OnChangeEvent_t467195904 * get_m_OnValueChanged_29() const { return ___m_OnValueChanged_29; }
	inline OnChangeEvent_t467195904 ** get_address_of_m_OnValueChanged_29() { return &___m_OnValueChanged_29; }
	inline void set_m_OnValueChanged_29(OnChangeEvent_t467195904 * value)
	{
		___m_OnValueChanged_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_29), value);
	}

	inline static int32_t get_offset_of_m_OnValidateInput_30() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OnValidateInput_30)); }
	inline OnValidateInput_t2355412304 * get_m_OnValidateInput_30() const { return ___m_OnValidateInput_30; }
	inline OnValidateInput_t2355412304 ** get_address_of_m_OnValidateInput_30() { return &___m_OnValidateInput_30; }
	inline void set_m_OnValidateInput_30(OnValidateInput_t2355412304 * value)
	{
		___m_OnValidateInput_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValidateInput_30), value);
	}

	inline static int32_t get_offset_of_m_CaretColor_31() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretColor_31)); }
	inline Color_t2555686324  get_m_CaretColor_31() const { return ___m_CaretColor_31; }
	inline Color_t2555686324 * get_address_of_m_CaretColor_31() { return &___m_CaretColor_31; }
	inline void set_m_CaretColor_31(Color_t2555686324  value)
	{
		___m_CaretColor_31 = value;
	}

	inline static int32_t get_offset_of_m_CustomCaretColor_32() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CustomCaretColor_32)); }
	inline bool get_m_CustomCaretColor_32() const { return ___m_CustomCaretColor_32; }
	inline bool* get_address_of_m_CustomCaretColor_32() { return &___m_CustomCaretColor_32; }
	inline void set_m_CustomCaretColor_32(bool value)
	{
		___m_CustomCaretColor_32 = value;
	}

	inline static int32_t get_offset_of_m_SelectionColor_33() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_SelectionColor_33)); }
	inline Color_t2555686324  get_m_SelectionColor_33() const { return ___m_SelectionColor_33; }
	inline Color_t2555686324 * get_address_of_m_SelectionColor_33() { return &___m_SelectionColor_33; }
	inline void set_m_SelectionColor_33(Color_t2555686324  value)
	{
		___m_SelectionColor_33 = value;
	}

	inline static int32_t get_offset_of_m_Text_34() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Text_34)); }
	inline String_t* get_m_Text_34() const { return ___m_Text_34; }
	inline String_t** get_address_of_m_Text_34() { return &___m_Text_34; }
	inline void set_m_Text_34(String_t* value)
	{
		___m_Text_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_34), value);
	}

	inline static int32_t get_offset_of_m_CaretBlinkRate_35() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretBlinkRate_35)); }
	inline float get_m_CaretBlinkRate_35() const { return ___m_CaretBlinkRate_35; }
	inline float* get_address_of_m_CaretBlinkRate_35() { return &___m_CaretBlinkRate_35; }
	inline void set_m_CaretBlinkRate_35(float value)
	{
		___m_CaretBlinkRate_35 = value;
	}

	inline static int32_t get_offset_of_m_CaretWidth_36() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretWidth_36)); }
	inline int32_t get_m_CaretWidth_36() const { return ___m_CaretWidth_36; }
	inline int32_t* get_address_of_m_CaretWidth_36() { return &___m_CaretWidth_36; }
	inline void set_m_CaretWidth_36(int32_t value)
	{
		___m_CaretWidth_36 = value;
	}

	inline static int32_t get_offset_of_m_ReadOnly_37() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ReadOnly_37)); }
	inline bool get_m_ReadOnly_37() const { return ___m_ReadOnly_37; }
	inline bool* get_address_of_m_ReadOnly_37() { return &___m_ReadOnly_37; }
	inline void set_m_ReadOnly_37(bool value)
	{
		___m_ReadOnly_37 = value;
	}

	inline static int32_t get_offset_of_m_CaretPosition_38() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretPosition_38)); }
	inline int32_t get_m_CaretPosition_38() const { return ___m_CaretPosition_38; }
	inline int32_t* get_address_of_m_CaretPosition_38() { return &___m_CaretPosition_38; }
	inline void set_m_CaretPosition_38(int32_t value)
	{
		___m_CaretPosition_38 = value;
	}

	inline static int32_t get_offset_of_m_CaretSelectPosition_39() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretSelectPosition_39)); }
	inline int32_t get_m_CaretSelectPosition_39() const { return ___m_CaretSelectPosition_39; }
	inline int32_t* get_address_of_m_CaretSelectPosition_39() { return &___m_CaretSelectPosition_39; }
	inline void set_m_CaretSelectPosition_39(int32_t value)
	{
		___m_CaretSelectPosition_39 = value;
	}

	inline static int32_t get_offset_of_caretRectTrans_40() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___caretRectTrans_40)); }
	inline RectTransform_t3704657025 * get_caretRectTrans_40() const { return ___caretRectTrans_40; }
	inline RectTransform_t3704657025 ** get_address_of_caretRectTrans_40() { return &___caretRectTrans_40; }
	inline void set_caretRectTrans_40(RectTransform_t3704657025 * value)
	{
		___caretRectTrans_40 = value;
		Il2CppCodeGenWriteBarrier((&___caretRectTrans_40), value);
	}

	inline static int32_t get_offset_of_m_CursorVerts_41() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CursorVerts_41)); }
	inline UIVertexU5BU5D_t1981460040* get_m_CursorVerts_41() const { return ___m_CursorVerts_41; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_CursorVerts_41() { return &___m_CursorVerts_41; }
	inline void set_m_CursorVerts_41(UIVertexU5BU5D_t1981460040* value)
	{
		___m_CursorVerts_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_CursorVerts_41), value);
	}

	inline static int32_t get_offset_of_m_InputTextCache_42() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_InputTextCache_42)); }
	inline TextGenerator_t3211863866 * get_m_InputTextCache_42() const { return ___m_InputTextCache_42; }
	inline TextGenerator_t3211863866 ** get_address_of_m_InputTextCache_42() { return &___m_InputTextCache_42; }
	inline void set_m_InputTextCache_42(TextGenerator_t3211863866 * value)
	{
		___m_InputTextCache_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputTextCache_42), value);
	}

	inline static int32_t get_offset_of_m_CachedInputRenderer_43() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CachedInputRenderer_43)); }
	inline CanvasRenderer_t2598313366 * get_m_CachedInputRenderer_43() const { return ___m_CachedInputRenderer_43; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CachedInputRenderer_43() { return &___m_CachedInputRenderer_43; }
	inline void set_m_CachedInputRenderer_43(CanvasRenderer_t2598313366 * value)
	{
		___m_CachedInputRenderer_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedInputRenderer_43), value);
	}

	inline static int32_t get_offset_of_m_PreventFontCallback_44() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_PreventFontCallback_44)); }
	inline bool get_m_PreventFontCallback_44() const { return ___m_PreventFontCallback_44; }
	inline bool* get_address_of_m_PreventFontCallback_44() { return &___m_PreventFontCallback_44; }
	inline void set_m_PreventFontCallback_44(bool value)
	{
		___m_PreventFontCallback_44 = value;
	}

	inline static int32_t get_offset_of_m_Mesh_45() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_Mesh_45)); }
	inline Mesh_t3648964284 * get_m_Mesh_45() const { return ___m_Mesh_45; }
	inline Mesh_t3648964284 ** get_address_of_m_Mesh_45() { return &___m_Mesh_45; }
	inline void set_m_Mesh_45(Mesh_t3648964284 * value)
	{
		___m_Mesh_45 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mesh_45), value);
	}

	inline static int32_t get_offset_of_m_AllowInput_46() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_AllowInput_46)); }
	inline bool get_m_AllowInput_46() const { return ___m_AllowInput_46; }
	inline bool* get_address_of_m_AllowInput_46() { return &___m_AllowInput_46; }
	inline void set_m_AllowInput_46(bool value)
	{
		___m_AllowInput_46 = value;
	}

	inline static int32_t get_offset_of_m_ShouldActivateNextUpdate_47() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ShouldActivateNextUpdate_47)); }
	inline bool get_m_ShouldActivateNextUpdate_47() const { return ___m_ShouldActivateNextUpdate_47; }
	inline bool* get_address_of_m_ShouldActivateNextUpdate_47() { return &___m_ShouldActivateNextUpdate_47; }
	inline void set_m_ShouldActivateNextUpdate_47(bool value)
	{
		___m_ShouldActivateNextUpdate_47 = value;
	}

	inline static int32_t get_offset_of_m_UpdateDrag_48() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_UpdateDrag_48)); }
	inline bool get_m_UpdateDrag_48() const { return ___m_UpdateDrag_48; }
	inline bool* get_address_of_m_UpdateDrag_48() { return &___m_UpdateDrag_48; }
	inline void set_m_UpdateDrag_48(bool value)
	{
		___m_UpdateDrag_48 = value;
	}

	inline static int32_t get_offset_of_m_DragPositionOutOfBounds_49() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DragPositionOutOfBounds_49)); }
	inline bool get_m_DragPositionOutOfBounds_49() const { return ___m_DragPositionOutOfBounds_49; }
	inline bool* get_address_of_m_DragPositionOutOfBounds_49() { return &___m_DragPositionOutOfBounds_49; }
	inline void set_m_DragPositionOutOfBounds_49(bool value)
	{
		___m_DragPositionOutOfBounds_49 = value;
	}

	inline static int32_t get_offset_of_m_CaretVisible_52() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_CaretVisible_52)); }
	inline bool get_m_CaretVisible_52() const { return ___m_CaretVisible_52; }
	inline bool* get_address_of_m_CaretVisible_52() { return &___m_CaretVisible_52; }
	inline void set_m_CaretVisible_52(bool value)
	{
		___m_CaretVisible_52 = value;
	}

	inline static int32_t get_offset_of_m_BlinkCoroutine_53() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_BlinkCoroutine_53)); }
	inline Coroutine_t3829159415 * get_m_BlinkCoroutine_53() const { return ___m_BlinkCoroutine_53; }
	inline Coroutine_t3829159415 ** get_address_of_m_BlinkCoroutine_53() { return &___m_BlinkCoroutine_53; }
	inline void set_m_BlinkCoroutine_53(Coroutine_t3829159415 * value)
	{
		___m_BlinkCoroutine_53 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlinkCoroutine_53), value);
	}

	inline static int32_t get_offset_of_m_BlinkStartTime_54() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_BlinkStartTime_54)); }
	inline float get_m_BlinkStartTime_54() const { return ___m_BlinkStartTime_54; }
	inline float* get_address_of_m_BlinkStartTime_54() { return &___m_BlinkStartTime_54; }
	inline void set_m_BlinkStartTime_54(float value)
	{
		___m_BlinkStartTime_54 = value;
	}

	inline static int32_t get_offset_of_m_DrawStart_55() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DrawStart_55)); }
	inline int32_t get_m_DrawStart_55() const { return ___m_DrawStart_55; }
	inline int32_t* get_address_of_m_DrawStart_55() { return &___m_DrawStart_55; }
	inline void set_m_DrawStart_55(int32_t value)
	{
		___m_DrawStart_55 = value;
	}

	inline static int32_t get_offset_of_m_DrawEnd_56() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DrawEnd_56)); }
	inline int32_t get_m_DrawEnd_56() const { return ___m_DrawEnd_56; }
	inline int32_t* get_address_of_m_DrawEnd_56() { return &___m_DrawEnd_56; }
	inline void set_m_DrawEnd_56(int32_t value)
	{
		___m_DrawEnd_56 = value;
	}

	inline static int32_t get_offset_of_m_DragCoroutine_57() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_DragCoroutine_57)); }
	inline Coroutine_t3829159415 * get_m_DragCoroutine_57() const { return ___m_DragCoroutine_57; }
	inline Coroutine_t3829159415 ** get_address_of_m_DragCoroutine_57() { return &___m_DragCoroutine_57; }
	inline void set_m_DragCoroutine_57(Coroutine_t3829159415 * value)
	{
		___m_DragCoroutine_57 = value;
		Il2CppCodeGenWriteBarrier((&___m_DragCoroutine_57), value);
	}

	inline static int32_t get_offset_of_m_OriginalText_58() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_OriginalText_58)); }
	inline String_t* get_m_OriginalText_58() const { return ___m_OriginalText_58; }
	inline String_t** get_address_of_m_OriginalText_58() { return &___m_OriginalText_58; }
	inline void set_m_OriginalText_58(String_t* value)
	{
		___m_OriginalText_58 = value;
		Il2CppCodeGenWriteBarrier((&___m_OriginalText_58), value);
	}

	inline static int32_t get_offset_of_m_WasCanceled_59() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_WasCanceled_59)); }
	inline bool get_m_WasCanceled_59() const { return ___m_WasCanceled_59; }
	inline bool* get_address_of_m_WasCanceled_59() { return &___m_WasCanceled_59; }
	inline void set_m_WasCanceled_59(bool value)
	{
		___m_WasCanceled_59 = value;
	}

	inline static int32_t get_offset_of_m_HasDoneFocusTransition_60() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_HasDoneFocusTransition_60)); }
	inline bool get_m_HasDoneFocusTransition_60() const { return ___m_HasDoneFocusTransition_60; }
	inline bool* get_address_of_m_HasDoneFocusTransition_60() { return &___m_HasDoneFocusTransition_60; }
	inline void set_m_HasDoneFocusTransition_60(bool value)
	{
		___m_HasDoneFocusTransition_60 = value;
	}

	inline static int32_t get_offset_of_m_ProcessingEvent_62() { return static_cast<int32_t>(offsetof(InputField_t3762917431, ___m_ProcessingEvent_62)); }
	inline Event_t2956885303 * get_m_ProcessingEvent_62() const { return ___m_ProcessingEvent_62; }
	inline Event_t2956885303 ** get_address_of_m_ProcessingEvent_62() { return &___m_ProcessingEvent_62; }
	inline void set_m_ProcessingEvent_62(Event_t2956885303 * value)
	{
		___m_ProcessingEvent_62 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProcessingEvent_62), value);
	}
};

struct InputField_t3762917431_StaticFields
{
public:
	// System.Char[] UnityEngine.UI.InputField::kSeparators
	CharU5BU5D_t3528271667* ___kSeparators_17;

public:
	inline static int32_t get_offset_of_kSeparators_17() { return static_cast<int32_t>(offsetof(InputField_t3762917431_StaticFields, ___kSeparators_17)); }
	inline CharU5BU5D_t3528271667* get_kSeparators_17() const { return ___kSeparators_17; }
	inline CharU5BU5D_t3528271667** get_address_of_kSeparators_17() { return &___kSeparators_17; }
	inline void set_kSeparators_17(CharU5BU5D_t3528271667* value)
	{
		___kSeparators_17 = value;
		Il2CppCodeGenWriteBarrier((&___kSeparators_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTFIELD_T3762917431_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_20)); }
	inline Material_t340375123 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t340375123 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_21)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef IMAGE_T2670269651_H
#define IMAGE_T2670269651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2670269651  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t280657092 * ___m_Sprite_29;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t280657092 * ___m_OverrideSprite_30;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_31;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_32;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_33;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_34;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_35;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_36;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_37;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_38;

public:
	inline static int32_t get_offset_of_m_Sprite_29() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Sprite_29)); }
	inline Sprite_t280657092 * get_m_Sprite_29() const { return ___m_Sprite_29; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_29() { return &___m_Sprite_29; }
	inline void set_m_Sprite_29(Sprite_t280657092 * value)
	{
		___m_Sprite_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_29), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_30() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_OverrideSprite_30)); }
	inline Sprite_t280657092 * get_m_OverrideSprite_30() const { return ___m_OverrideSprite_30; }
	inline Sprite_t280657092 ** get_address_of_m_OverrideSprite_30() { return &___m_OverrideSprite_30; }
	inline void set_m_OverrideSprite_30(Sprite_t280657092 * value)
	{
		___m_OverrideSprite_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_30), value);
	}

	inline static int32_t get_offset_of_m_Type_31() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_Type_31)); }
	inline int32_t get_m_Type_31() const { return ___m_Type_31; }
	inline int32_t* get_address_of_m_Type_31() { return &___m_Type_31; }
	inline void set_m_Type_31(int32_t value)
	{
		___m_Type_31 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_32() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_PreserveAspect_32)); }
	inline bool get_m_PreserveAspect_32() const { return ___m_PreserveAspect_32; }
	inline bool* get_address_of_m_PreserveAspect_32() { return &___m_PreserveAspect_32; }
	inline void set_m_PreserveAspect_32(bool value)
	{
		___m_PreserveAspect_32 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_33() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillCenter_33)); }
	inline bool get_m_FillCenter_33() const { return ___m_FillCenter_33; }
	inline bool* get_address_of_m_FillCenter_33() { return &___m_FillCenter_33; }
	inline void set_m_FillCenter_33(bool value)
	{
		___m_FillCenter_33 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_34() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillMethod_34)); }
	inline int32_t get_m_FillMethod_34() const { return ___m_FillMethod_34; }
	inline int32_t* get_address_of_m_FillMethod_34() { return &___m_FillMethod_34; }
	inline void set_m_FillMethod_34(int32_t value)
	{
		___m_FillMethod_34 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_35() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillAmount_35)); }
	inline float get_m_FillAmount_35() const { return ___m_FillAmount_35; }
	inline float* get_address_of_m_FillAmount_35() { return &___m_FillAmount_35; }
	inline void set_m_FillAmount_35(float value)
	{
		___m_FillAmount_35 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_36() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillClockwise_36)); }
	inline bool get_m_FillClockwise_36() const { return ___m_FillClockwise_36; }
	inline bool* get_address_of_m_FillClockwise_36() { return &___m_FillClockwise_36; }
	inline void set_m_FillClockwise_36(bool value)
	{
		___m_FillClockwise_36 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_37() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_FillOrigin_37)); }
	inline int32_t get_m_FillOrigin_37() const { return ___m_FillOrigin_37; }
	inline int32_t* get_address_of_m_FillOrigin_37() { return &___m_FillOrigin_37; }
	inline void set_m_FillOrigin_37(int32_t value)
	{
		___m_FillOrigin_37 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_38() { return static_cast<int32_t>(offsetof(Image_t2670269651, ___m_AlphaHitTestMinimumThreshold_38)); }
	inline float get_m_AlphaHitTestMinimumThreshold_38() const { return ___m_AlphaHitTestMinimumThreshold_38; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_38() { return &___m_AlphaHitTestMinimumThreshold_38; }
	inline void set_m_AlphaHitTestMinimumThreshold_38(float value)
	{
		___m_AlphaHitTestMinimumThreshold_38 = value;
	}
};

struct Image_t2670269651_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t340375123 * ___s_ETC1DefaultUI_28;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t1457185986* ___s_VertScratch_39;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t1457185986* ___s_UVScratch_40;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1718750761* ___s_Xy_41;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1718750761* ___s_Uv_42;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_28() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_ETC1DefaultUI_28)); }
	inline Material_t340375123 * get_s_ETC1DefaultUI_28() const { return ___s_ETC1DefaultUI_28; }
	inline Material_t340375123 ** get_address_of_s_ETC1DefaultUI_28() { return &___s_ETC1DefaultUI_28; }
	inline void set_s_ETC1DefaultUI_28(Material_t340375123 * value)
	{
		___s_ETC1DefaultUI_28 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_28), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_39() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_VertScratch_39)); }
	inline Vector2U5BU5D_t1457185986* get_s_VertScratch_39() const { return ___s_VertScratch_39; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_VertScratch_39() { return &___s_VertScratch_39; }
	inline void set_s_VertScratch_39(Vector2U5BU5D_t1457185986* value)
	{
		___s_VertScratch_39 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_39), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_40() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_UVScratch_40)); }
	inline Vector2U5BU5D_t1457185986* get_s_UVScratch_40() const { return ___s_UVScratch_40; }
	inline Vector2U5BU5D_t1457185986** get_address_of_s_UVScratch_40() { return &___s_UVScratch_40; }
	inline void set_s_UVScratch_40(Vector2U5BU5D_t1457185986* value)
	{
		___s_UVScratch_40 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_40), value);
	}

	inline static int32_t get_offset_of_s_Xy_41() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Xy_41)); }
	inline Vector3U5BU5D_t1718750761* get_s_Xy_41() const { return ___s_Xy_41; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Xy_41() { return &___s_Xy_41; }
	inline void set_s_Xy_41(Vector3U5BU5D_t1718750761* value)
	{
		___s_Xy_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_41), value);
	}

	inline static int32_t get_offset_of_s_Uv_42() { return static_cast<int32_t>(offsetof(Image_t2670269651_StaticFields, ___s_Uv_42)); }
	inline Vector3U5BU5D_t1718750761* get_s_Uv_42() const { return ___s_Uv_42; }
	inline Vector3U5BU5D_t1718750761** get_address_of_s_Uv_42() { return &___s_Uv_42; }
	inline void set_s_Uv_42(Vector3U5BU5D_t1718750761* value)
	{
		___s_Uv_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2670269651_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_28)); }
	inline FontData_t746620069 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t746620069 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t746620069 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_30)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_32)); }
	inline Material_t340375123 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t340375123 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_t1113636619 * m_Items[1];

public:
	inline GameObject_t1113636619 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1113636619 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1113636619 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1113636619 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Sprite_t280657092 * m_Items[1];

public:
	inline Sprite_t280657092 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Sprite_t280657092 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Sprite_t280657092 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Sprite_t280657092 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Sprite_t280657092 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Sprite_t280657092 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Grid/TerrainType[]
struct TerrainTypeU5BU5D_t1748095520  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) TerrainType_t3627529997 * m_Items[1];

public:
	inline TerrainType_t3627529997 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline TerrainType_t3627529997 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, TerrainType_t3627529997 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline TerrainType_t3627529997 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline TerrainType_t3627529997 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, TerrainType_t3627529997 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Node[0...,0...]
struct NodeU5B0___U2C0___U5D_t1457036568  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Node_t2989995042 * m_Items[1];

public:
	inline Node_t2989995042 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Node_t2989995042 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Node_t2989995042 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Node_t2989995042 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Node_t2989995042 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Node_t2989995042 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Node_t2989995042 * GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline Node_t2989995042 ** GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, Node_t2989995042 * value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Node_t2989995042 * GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline Node_t2989995042 ** GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, Node_t2989995042 * value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[0...,0...]
struct Int32U5B0___U2C0___U5D_t385246373  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
	inline int32_t GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, int32_t value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, int32_t value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t3722313464  m_Items[1];

public:
	inline Vector3_t3722313464  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t3722313464  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t2439009922  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Image_t2670269651 * m_Items[1];

public:
	inline Image_t2670269651 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Image_t2670269651 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Image_t2670269651 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Image_t2670269651 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Image_t2670269651 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Image_t2670269651 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t1703627840  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t1188392813 * m_Items[1];

public:
	inline Delegate_t1188392813 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t1188392813 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t1188392813 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t1188392813 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t1188392813 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t1188392813 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LevelSelectPanel[]
struct LevelSelectPanelU5BU5D_t1747102860  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) LevelSelectPanel_t2626311185 * m_Items[1];

public:
	inline LevelSelectPanel_t2626311185 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline LevelSelectPanel_t2626311185 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, LevelSelectPanel_t2626311185 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline LevelSelectPanel_t2626311185 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline LevelSelectPanel_t2626311185 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, LevelSelectPanel_t2626311185 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Line[]
struct LineU5BU5D_t3212499767  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Line_t3796957314  m_Items[1];

public:
	inline Line_t3796957314  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Line_t3796957314 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Line_t3796957314  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Line_t3796957314  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Line_t3796957314 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Line_t3796957314  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_t2555686324  m_Items[1];

public:
	inline Color_t2555686324  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t2555686324  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m1135049463_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Vector3_t3722313464  p1, Quaternion_t2301928331  p2, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m1535364901_gshared (Dictionary_2_t1839659084 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::TryGetValue(!0,!1&)
extern "C"  bool Dictionary_2_TryGetValue_m1682688660_gshared (Dictionary_2_t1839659084 * __this, int32_t p0, int32_t* p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor()
extern "C"  void Dictionary_2__ctor_m1287366611_gshared (Dictionary_2_t1839659084 * __this, const RuntimeMethod* method);
// System.Void Heap`1<System.Object>::.ctor(System.Int32)
extern "C"  void Heap_1__ctor_m1149134057_gshared (Heap_1_t1002052798 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
extern "C"  void HashSet_1__ctor_m4231804131_gshared (HashSet_1_t1645055638 * __this, const RuntimeMethod* method);
// System.Void Heap`1<System.Object>::Add(T)
extern "C"  void Heap_1_Add_m932648688_gshared (Heap_1_t1002052798 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// T Heap`1<System.Object>::RemoveFirst()
extern "C"  RuntimeObject * Heap_1_RemoveFirst_m3504776775_gshared (Heap_1_t1002052798 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(!0)
extern "C"  bool HashSet_1_Add_m1971460364_gshared (HashSet_1_t1645055638 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t2146457487  List_1_GetEnumerator_m816315209_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m337713592_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Contains(!0)
extern "C"  bool HashSet_1_Contains_m3173358704_gshared (HashSet_1_t1645055638 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean Heap`1<System.Object>::Contains(T)
extern "C"  bool Heap_1_Contains_m2784630065_gshared (Heap_1_t1002052798 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void Heap`1<System.Object>::UpdateItem(T)
extern "C"  void Heap_1_UpdateItem_m1138389099_gshared (Heap_1_t1002052798 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Int32 Heap`1<System.Object>::get_Count()
extern "C"  int32_t Heap_1_get_Count_m2611955549_gshared (Heap_1_t1002052798 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<PathResult>::Invoke(!0)
extern "C"  void Action_1_Invoke_m2478306080_gshared (Action_1_t1700480823 * __this, PathResult_t1528013228  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C"  void List_1__ctor_m1536473967_gshared (List_1_t899420910 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m1328026504_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
extern "C"  void List_1_Add_m1524640104_gshared (List_1_t899420910 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
extern "C"  Vector3U5BU5D_t1718750761* List_1_ToArray_m1691996104_gshared (List_1_t899420910 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Queue`1<PathResult>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m3067217344_gshared (Queue_1_t1374272722 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1<PathResult>::Dequeue()
extern "C"  PathResult_t1528013228  Queue_1_Dequeue_m920511108_gshared (Queue_1_t1374272722 * __this, const RuntimeMethod* method);
// System.Void System.Action`2<System.Object,System.Boolean>::Invoke(!0,!1)
extern "C"  void Action_2_Invoke_m2879433563_gshared (Action_2_t3782157935 * __this, RuntimeObject * p0, bool p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<PathResult>::Enqueue(!0)
extern "C"  void Queue_1_Enqueue_m1828253350_gshared (Queue_1_t1374272722 * __this, PathResult_t1528013228  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<PathResult>::.ctor()
extern "C"  void Queue_1__ctor_m534127087_gshared (Queue_1_t1374272722 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<PathResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1884953144_gshared (Action_1_t1700480823 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m888956288_gshared (List_1_t128053199 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
extern "C"  void List_1_Add_m2080863212_gshared (List_1_t128053199 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C"  void List_1__ctor_m1204004817_gshared (List_1_t128053199 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C"  Enumerator_t2017297076  List_1_GetEnumerator_m2838255531_gshared (List_1_t128053199 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2612064142_gshared (Enumerator_t2017297076 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1050804954_gshared (Enumerator_t2017297076 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m222348240_gshared (Enumerator_t2017297076 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C"  int32_t List_1_get_Count_m361000296_gshared (List_1_t128053199 * __this, const RuntimeMethod* method);
// System.Void System.Action`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m1824561290_gshared (Action_2_t3782157935 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);

// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<BossTankController>()
#define GameObject_GetComponent_TisBossTankController_t1902331492_m3861119340(__this, method) ((  BossTankController_t1902331492 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void BossTankController::FindPlayer()
extern "C"  void BossTankController_FindPlayer_m4054288205 (BossTankController_t1902331492 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void BossTankController::PhaseTwo()
extern "C"  void BossTankController_PhaseTwo_m3848855080 (BossTankController_t1902331492 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void BossTankController::PhaseThree()
extern "C"  void BossTankController_PhaseThree_m1686685524 (BossTankController_t1902331492 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void BossTankController::FindHealingSpot()
extern "C"  void BossTankController_FindHealingSpot_m1715802366 (BossTankController_t1902331492 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t3704657025_m3396022872(__this, method) ((  RectTransform_t3704657025 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m2907476221 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
extern "C"  float Mathf_Repeat_m1502810009 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t3722313464  Vector3_get_up_m3584168373 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m3376773913 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Addition_m779775034 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void BossTankController::CheckBossStats()
extern "C"  void BossTankController_CheckBossStats_m1300902599 (BossTankController_t1902331492 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyShoot::Shoot()
extern "C"  void EnemyShoot_Shoot_m569725038 (EnemyShoot_t243830779 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void BossTankController::FindCenterPoint()
extern "C"  void BossTankController_FindCenterPoint_m3328217412 (BossTankController_t1902331492 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C"  void Vector3__ctor_m1719387948 (Vector3_t3722313464 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
extern "C"  void Transform_Rotate_m720511863 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyShoot::ShootLaser()
extern "C"  void EnemyShoot_ShootLaser_m1234821334 (EnemyShoot_t243830779 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Image::set_fillAmount(System.Single)
extern "C"  void Image_set_fillAmount_m3737925590 (Image_t2670269651 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m141394615 (int32_t* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, method) ((  Renderer_t2627027031 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t340375123 * Renderer_get_material_m4171603682 (Renderer_t2627027031 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Material::get_color()
extern "C"  Color_t2555686324  Material_get_color_m3827673574 (Material_t340375123 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
extern "C"  Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m2618285814 (MonoBehaviour_t3962482529 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Bullet/<Fade>d__7::.ctor(System.Int32)
extern "C"  void U3CFadeU3Ed__7__ctor_m1264547669 (U3CFadeU3Ed__7_t626104978 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Component::get_tag()
extern "C"  String_t* Component_get_tag_m2716693327 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t3916780224_m279685075(__this, method) ((  Rigidbody_t3916780224 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void UnityEngine.Rigidbody::Sleep()
extern "C"  void Rigidbody_Sleep_m1823108224 (Rigidbody_t3916780224 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m2199082655 (WaitForSeconds_t1699091251 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_clear()
extern "C"  Color_t2555686324  Color_get_clear_m1016382534 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  Color_t2555686324  Color_Lerp_m973389909 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, Color_t2555686324  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern "C"  void Material_set_color_m1794818007 (Material_t340375123 * __this, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::FindObjectOfType<HealthUI>()
#define Object_FindObjectOfType_TisHealthUI_t1908446248_m1692366917(__this /* static, unused */, method) ((  HealthUI_t1908446248 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared)(__this /* static, unused */, method)
// System.String UnityEngine.GameObject::get_tag()
extern "C"  String_t* GameObject_get_tag_m3951609671 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, method) ((  AudioSource_t3935305588 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)
extern "C"  void AudioSource_PlayOneShot_m2678069419 (AudioSource_t3935305588 * __this, AudioClip_t3680889665 * p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t2301928331  Transform_get_rotation_m3502953881 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1113636619_m3006960551(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1113636619 * (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, Vector3_t3722313464 , Quaternion_t2301928331 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m1135049463_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 UnityEngine.GameObject::GetComponent<EnemyStats>()
#define GameObject_GetComponent_TisEnemyStats_t4187841152_m2036182613(__this, method) ((  EnemyStats_t4187841152 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PlayerStats>()
#define GameObject_GetComponent_TisPlayerStats_t2044123780_m4080514128(__this, method) ((  PlayerStats_t2044123780 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void EnableCameraDepthInForward::Set()
extern "C"  void EnableCameraDepthInForward_Set_m3860174829 (EnableCameraDepthInForward_t3351256833 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, method) ((  Camera_t4157153871 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.DepthTextureMode UnityEngine.Camera::get_depthTextureMode()
extern "C"  int32_t Camera_get_depthTextureMode_m871144641 (Camera_t4157153871 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_depthTextureMode(UnityEngine.DepthTextureMode)
extern "C"  void Camera_set_depthTextureMode_m754977860 (Camera_t4157153871 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AI.NavMeshAgent>()
#define Component_GetComponent_TisNavMeshAgent_t1276799816_m597731532(__this, method) ((  NavMeshAgent_t1276799816 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void EnemyController::MoveTowardsTarget()
extern "C"  void EnemyController_MoveTowardsTarget_m1657189778 (EnemyController_t2191660454 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyController::UpdateReloadTime()
extern "C"  void EnemyController_UpdateReloadTime_m1142540528 (EnemyController_t2191660454 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Distance_m886789632 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AI.NavMeshAgent::SetDestination(UnityEngine.Vector3)
extern "C"  bool NavMeshAgent_SetDestination_m2826390109 (NavMeshAgent_t1276799816 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Gun::Shoot()
extern "C"  void Gun_Shoot_m3160136974 (Gun_t783153271 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AI.NavMeshAgent::get_stoppingDistance()
extern "C"  float NavMeshAgent_get_stoppingDistance_m1834551109 (NavMeshAgent_t1276799816 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyController::FaceTarget()
extern "C"  void EnemyController_FaceTarget_m3754874152 (EnemyController_t2191660454 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Subtraction_m3073674971 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t3722313464  Vector3_get_normalized_m2454957984 (Vector3_t3722313464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Quaternion_LookRotation_m4040767668 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t2301928331  Quaternion_Slerp_m1234055455 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Quaternion_t2301928331  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m3524318132 (Transform_t3600365921 * __this, Quaternion_t2301928331  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.BoxCollider>()
#define GameObject_AddComponent_TisBoxCollider_t1640800422_m1661675543(__this, method) ((  BoxCollider_t1640800422 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method)
// System.Void UnityEngine.BoxCollider::set_center(UnityEngine.Vector3)
extern "C"  void BoxCollider_set_center_m1086194996 (BoxCollider_t1640800422 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoxCollider::set_size(UnityEngine.Vector3)
extern "C"  void BoxCollider_set_size_m3618428148 (BoxCollider_t1640800422 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider::set_isTrigger(System.Boolean)
extern "C"  void Collider_set_isTrigger_m1770557358 (Collider_t1773347010 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_red()
extern "C"  Color_t2555686324  Color_get_red_m3227813939 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern "C"  void Gizmos_set_color_m3399737545 (RuntimeObject * __this /* static, unused */, Color_t2555686324  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawWireSphere(UnityEngine.Vector3,System.Single)
extern "C"  void Gizmos_DrawWireSphere_m132265467 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::FindObjectOfType<LevelClear>()
#define Object_FindObjectOfType_TisLevelClear_t1949285084_m2992299238(__this /* static, unused */, method) ((  LevelClear_t1949285084 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared)(__this /* static, unused */, method)
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern "C"  GameObjectU5BU5D_t3328599146* GameObject_FindGameObjectsWithTag_m2585173894 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LevelClear::FadeToLevel(System.String)
extern "C"  void LevelClear_FadeToLevel_m1500572359 (LevelClear_t1949285084 * __this, String_t* ___levelName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyFuzzyState::SetLabels()
extern "C"  void EnemyFuzzyState_SetLabels_m2897062058 (EnemyFuzzyState_t3026123389 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void EnemyFuzzyState::EvaluateStatements()
extern "C"  void EnemyFuzzyState_EvaluateStatements_m2484608551 (EnemyFuzzyState_t3026123389 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
extern "C"  float AnimationCurve_Evaluate_m2125563588 (AnimationCurve_t3046754366 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody>()
#define GameObject_GetComponent_TisRigidbody_t3916780224_m564316479(__this, method) ((  Rigidbody_t3916780224 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t3722313464  Transform_get_forward_m747522392 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_velocity_m2899403247 (Rigidbody_t3916780224 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m168149494 (Ray_t3785851493 * __this, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_Raycast_m1743768310 (RuntimeObject * __this /* static, unused */, Ray_t3785851493  p0, RaycastHit_t1056001966 * p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C"  float RaycastHit_get_distance_m3727327466 (RaycastHit_t1056001966 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::FindObjectOfType<EnemyCount>()
#define Object_FindObjectOfType_TisEnemyCount_t3730988989_m432317606(__this /* static, unused */, method) ((  EnemyCount_t3730988989 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void FuzzySample::SetLabels()
extern "C"  void FuzzySample_SetLabels_m3256301443 (FuzzySample_t2008138351 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.InputField::get_text()
extern "C"  String_t* InputField_get_text_m3534748202 (InputField_t3762917431 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2969720369 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single System.Single::Parse(System.String)
extern "C"  float Single_Parse_m364357836 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2844511972 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
extern "C"  GameObject_t1113636619 * GameObject_FindGameObjectWithTag_m2129039296 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_Lerp_m407887542 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::FindObjectOfType<LevelChanger>()
#define Object_FindObjectOfType_TisLevelChanger_t225386971_m2918287313(__this /* static, unused */, method) ((  LevelChanger_t225386971 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m2612646359_gshared)(__this /* static, unused */, method)
// System.Void GameOverScreen::DeselectAll()
extern "C"  void GameOverScreen_DeselectAll_m311764793 (GameOverScreen_t2285777029 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t2670269651_m2486712510(__this, method) ((  Image_t2670269651 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method)
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
extern "C"  void Image_set_sprite_m2369174689 (Image_t2670269651 * __this, Sprite_t280657092 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void GameOverScreen::UpdateOptions()
extern "C"  void GameOverScreen_UpdateOptions_m2082488164 (GameOverScreen_t2285777029 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void GameOverScreen::SelectOptions()
extern "C"  void GameOverScreen_SelectOptions_m3194635162 (GameOverScreen_t2285777029 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern "C"  bool Input_GetKeyDown_m17791917 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern "C"  float Input_GetAxisRaw_m2316819917 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LevelChanger::FadeToLevel(System.String)
extern "C"  void LevelChanger_FadeToLevel_m2444280689 (LevelChanger_t225386971 * __this, String_t* ___levelName0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern "C"  int32_t Mathf_RoundToInt_m1874334613 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::get_value()
extern "C"  int32_t LayerMask_get_value_m1881709263 (LayerMask_t3493934918 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LayerMask::set_value(System.Int32)
extern "C"  void LayerMask_set_value_m1631067668 (LayerMask_t3493934918 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Log(System.Single,System.Single)
extern "C"  float Mathf_Log_m2177375338 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Add(!0,!1)
#define Dictionary_2_Add_m1535364901(__this, p0, p1, method) ((  void (*) (Dictionary_2_t1839659084 *, int32_t, int32_t, const RuntimeMethod*))Dictionary_2_Add_m1535364901_gshared)(__this, p0, p1, method)
// System.Void Grid::CreateGrid()
extern "C"  void Grid_CreateGrid_m256214910 (Grid_t1081586032 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C"  Vector3_t3722313464  Vector3_get_right_m1913784872 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Division_m510815599 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t3722313464  Vector3_get_forward_m3100859705 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
extern "C"  int32_t LayerMask_op_Implicit_m3296792737 (RuntimeObject * __this /* static, unused */, LayerMask_t3493934918  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::CheckSphere(UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  bool Physics_CheckSphere_m3218818399 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, float p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern "C"  Vector3_t3722313464  Vector3_get_down_m3781355428 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m1893809531 (RuntimeObject * __this /* static, unused */, Ray_t3785851493  p0, RaycastHit_t1056001966 * p1, float p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C"  Collider_t1773347010 * RaycastHit_get_collider_m1464180279 (RaycastHit_t1056001966 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.GameObject::get_layer()
extern "C"  int32_t GameObject_get_layer_m4158800245 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::TryGetValue(!0,!1&)
#define Dictionary_2_TryGetValue_m1682688660(__this, p0, p1, method) ((  bool (*) (Dictionary_2_t1839659084 *, int32_t, int32_t*, const RuntimeMethod*))Dictionary_2_TryGetValue_m1682688660_gshared)(__this, p0, p1, method)
// System.Void Node::.ctor(System.Boolean,UnityEngine.Vector3,System.Int32,System.Int32,System.Int32)
extern "C"  void Node__ctor_m304489418 (Node_t2989995042 * __this, bool ____walkable0, Vector3_t3722313464  ____worldPosition1, int32_t ____gridX2, int32_t ____gridY3, int32_t ____penalty4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Grid::BlurPenaltyMap(System.Int32)
extern "C"  void Grid_BlurPenaltyMap_m2132322175 (Grid_t1081586032 * __this, int32_t ___blurSize0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Mathf_Clamp_m2756574208 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Node>::.ctor()
#define List_1__ctor_m3067334415(__this, method) ((  void (*) (List_1_t167102488 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Node>::Add(!0)
#define List_1_Add_m4134525979(__this, p0, method) ((  void (*) (List_1_t167102488 *, Node_t2989995042 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m56433566 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawWireCube(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawWireCube_m2631700312 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::GetUpperBound(System.Int32)
extern "C"  int32_t Array_GetUpperBound_m4018715963 (RuntimeArray * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::GetLowerBound(System.Int32)
extern "C"  int32_t Array_GetLowerBound_m2045984623 (RuntimeArray * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2555686324  Color_get_white_m332174077 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2555686324  Color_get_black_m719512684 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_InverseLerp_m4155825980 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Gizmos::get_color()
extern "C"  Color_t2555686324  Gizmos_get_color_m712612415 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C"  Vector3_t3722313464  Vector3_get_one_m1629952498 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawCube(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawCube_m530322281 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor()
#define Dictionary_2__ctor_m1287366611(__this, method) ((  void (*) (Dictionary_2_t1839659084 *, const RuntimeMethod*))Dictionary_2__ctor_m1287366611_gshared)(__this, method)
// System.Boolean Gun::CanShoot()
extern "C"  bool Gun_CanShoot_m1361174918 (Gun_t783153271 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m4054026115 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<EnemyStats>()
#define Component_GetComponent_TisEnemyStats_t4187841152_m1077339419(__this, method) ((  EnemyStats_t4187841152 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<PlayerStats>()
#define Component_GetComponent_TisPlayerStats_t2044123780_m1979177729(__this, method) ((  PlayerStats_t2044123780 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m2971454694 (RuntimeObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m20417929 (Behaviour_t1437897464 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m4211327027 (Object_t631007953 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_yellow()
extern "C"  Color_t2555686324  Color_get_yellow_m1287957903 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m3752629331 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Inventory/OnItemChanged::Invoke()
extern "C"  void OnItemChanged_Invoke_m404998984 (OnItemChanged_t22848112 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Inventory/OnItemChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void OnItemChanged__ctor_m2205075595 (OnItemChanged_t22848112 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Combine_m1859655160 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void InventorySlot::AddItem(Item)
extern "C"  void InventorySlot_AddItem_m1624661967 (InventorySlot_t3299309524 * __this, Item_t2953980098 * ___newItem0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void InventorySlot::ClearSlot()
extern "C"  void InventorySlot_ClearSlot_m1469806239 (InventorySlot_t3299309524 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m1310743131 (ScriptableObject_t2528358522 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Interactible::Interact()
extern "C"  void Interactible_Interact_m2171514313 (Interactible_t1060880155 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void ItemPickup::PickUp()
extern "C"  void ItemPickup_PickUp_m3865268335 (ItemPickup_t4277059944 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Inventory::AddItem(Item)
extern "C"  bool Inventory_AddItem_m2364679108 (Inventory_t1050226016 * __this, Item_t2953980098 * ___item0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Interactible::.ctor()
extern "C"  void Interactible__ctor_m757280019 (Interactible_t1060880155 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C"  void Animator_SetTrigger_m2134052629 (Animator_t434523843 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m1758133949 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
extern "C"  void SceneManager_LoadScene_m3463216446 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LevelChanger::FadeToLevel(System.Int32)
extern "C"  void LevelChanger_FadeToLevel_m359947979 (LevelChanger_t225386971 * __this, int32_t ___levelIndex0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LevelSelect::DeselectAll()
extern "C"  void LevelSelect_DeselectAll_m630293501 (LevelSelect_t2669120976 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LevelSelect::UpdateOptions()
extern "C"  void LevelSelect_UpdateOptions_m4096405336 (LevelSelect_t2669120976 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LevelSelect::SelectOptions()
extern "C"  void LevelSelect_SelectOptions_m312907934 (LevelSelect_t2669120976 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3755062657 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2670269651_m980647750(__this, method) ((  Image_t2670269651 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3600365921 * Transform_GetChild_m1092972975 (Transform_t3600365921 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  Vector2_op_Addition_m800700293 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Line::GetSide(UnityEngine.Vector2)
extern "C"  bool Line_GetSide_m3671066560 (Line_t3796957314 * __this, Vector2_t2156229523  ___p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Line::.ctor(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Line__ctor_m1943978764 (Line_t3796957314 * __this, Vector2_t2156229523  ___pointOnLine0, Vector2_t2156229523  ___pointPerpendicularToLine1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Line::HasCrossedLine(UnityEngine.Vector2)
extern "C"  bool Line_HasCrossedLine_m2779751333 (Line_t3796957314 * __this, Vector2_t2156229523  ___p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float Vector2_Distance_m3048868881 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Line::DistanceFromPoint(UnityEngine.Vector2)
extern "C"  float Line_DistanceFromPoint_m4294230093 (Line_t3796957314 * __this, Vector2_t2156229523  ___p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Gizmos_DrawLine_m3273476787 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Line::DrawWithGizmos(System.Single)
extern "C"  void Line_DrawWithGizmos_m1752959317 (Line_t3796957314 * __this, float ___length0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Node::get_fCost()
extern "C"  int32_t Node_get_fCost_m1210297693 (Node_t2989995042 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::CompareTo(System.Int32)
extern "C"  int32_t Int32_CompareTo_m4284770383 (int32_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Path::V3ToV2(UnityEngine.Vector3)
extern "C"  Vector2_t2156229523  Path_V3ToV2_m3527951486 (Path_t2615110272 * __this, Vector3_t3722313464  ___v30, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  Vector2_op_Subtraction_m73004381 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
extern "C"  Vector2_t2156229523  Vector2_get_normalized_m2683665860 (Vector2_t2156229523 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2156229523  Vector2_op_Multiply_m2347887432 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<Grid>()
#define Component_GetComponent_TisGrid_t1081586032_m3342251133(__this, method) ((  Grid_t1081586032 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void System.Diagnostics.Stopwatch::.ctor()
extern "C"  void Stopwatch__ctor_m2673586837 (Stopwatch_t305734070 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.Stopwatch::Start()
extern "C"  void Stopwatch_Start_m1142799187 (Stopwatch_t305734070 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Node Grid::NodeFromWorldPoint(UnityEngine.Vector3)
extern "C"  Node_t2989995042 * Grid_NodeFromWorldPoint_m3738070334 (Grid_t1081586032 * __this, Vector3_t3722313464  ___worldPosition0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Grid::get_MaxSize()
extern "C"  int32_t Grid_get_MaxSize_m3127825365 (Grid_t1081586032 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Heap`1<Node>::.ctor(System.Int32)
#define Heap_1__ctor_m297432033(__this, p0, method) ((  void (*) (Heap_1_t911941676 *, int32_t, const RuntimeMethod*))Heap_1__ctor_m1149134057_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.HashSet`1<Node>::.ctor()
#define HashSet_1__ctor_m3938937520(__this, method) ((  void (*) (HashSet_1_t1554944516 *, const RuntimeMethod*))HashSet_1__ctor_m4231804131_gshared)(__this, method)
// System.Void Heap`1<Node>::Add(T)
#define Heap_1_Add_m2689756234(__this, p0, method) ((  void (*) (Heap_1_t911941676 *, Node_t2989995042 *, const RuntimeMethod*))Heap_1_Add_m932648688_gshared)(__this, p0, method)
// T Heap`1<Node>::RemoveFirst()
#define Heap_1_RemoveFirst_m1685389745(__this, method) ((  Node_t2989995042 * (*) (Heap_1_t911941676 *, const RuntimeMethod*))Heap_1_RemoveFirst_m3504776775_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<Node>::Add(!0)
#define HashSet_1_Add_m87732039(__this, p0, method) ((  bool (*) (HashSet_1_t1554944516 *, Node_t2989995042 *, const RuntimeMethod*))HashSet_1_Add_m1971460364_gshared)(__this, p0, method)
// System.Void System.Diagnostics.Stopwatch::Stop()
extern "C"  void Stopwatch_Stop_m1583564474 (Stopwatch_t305734070 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Diagnostics.Stopwatch::get_ElapsedMilliseconds()
extern "C"  int64_t Stopwatch_get_ElapsedMilliseconds_m1101465039 (Stopwatch_t305734070 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C"  String_t* String_Concat_m1715369213 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern "C"  void MonoBehaviour_print_m330341231 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Node> Grid::GetNeighbours(Node)
extern "C"  List_1_t167102488 * Grid_GetNeighbours_m4128505182 (Grid_t1081586032 * __this, Node_t2989995042 * ___node0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Node>::GetEnumerator()
#define List_1_GetEnumerator_m3741205568(__this, method) ((  Enumerator_t2056346365  (*) (List_1_t167102488 *, const RuntimeMethod*))List_1_GetEnumerator_m816315209_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<Node>::get_Current()
#define Enumerator_get_Current_m3563945012(__this, method) ((  Node_t2989995042 * (*) (Enumerator_t2056346365 *, const RuntimeMethod*))Enumerator_get_Current_m337713592_gshared)(__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<Node>::Contains(!0)
#define HashSet_1_Contains_m1749837864(__this, p0, method) ((  bool (*) (HashSet_1_t1554944516 *, Node_t2989995042 *, const RuntimeMethod*))HashSet_1_Contains_m3173358704_gshared)(__this, p0, method)
// System.Int32 Pathfinding::GetDistance(Node,Node)
extern "C"  int32_t Pathfinding_GetDistance_m3683541895 (Pathfinding_t1696161914 * __this, Node_t2989995042 * ___nodeA0, Node_t2989995042 * ___nodeB1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Heap`1<Node>::Contains(T)
#define Heap_1_Contains_m3137522954(__this, p0, method) ((  bool (*) (Heap_1_t911941676 *, Node_t2989995042 *, const RuntimeMethod*))Heap_1_Contains_m2784630065_gshared)(__this, p0, method)
// System.Void Heap`1<Node>::UpdateItem(T)
#define Heap_1_UpdateItem_m3101882114(__this, p0, method) ((  void (*) (Heap_1_t911941676 *, Node_t2989995042 *, const RuntimeMethod*))Heap_1_UpdateItem_m1138389099_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Node>::MoveNext()
#define Enumerator_MoveNext_m1693710810(__this, method) ((  bool (*) (Enumerator_t2056346365 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Node>::Dispose()
#define Enumerator_Dispose_m205031767(__this, method) ((  void (*) (Enumerator_t2056346365 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method)
// System.Int32 Heap`1<Node>::get_Count()
#define Heap_1_get_Count_m149300838(__this, method) ((  int32_t (*) (Heap_1_t911941676 *, const RuntimeMethod*))Heap_1_get_Count_m2611955549_gshared)(__this, method)
// UnityEngine.Vector3[] Pathfinding::RetracePath(Node,Node)
extern "C"  Vector3U5BU5D_t1718750761* Pathfinding_RetracePath_m276304888 (Pathfinding_t1696161914 * __this, Node_t2989995042 * ___startNode0, Node_t2989995042 * ___endNode1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PathResult::.ctor(UnityEngine.Vector3[],System.Boolean,System.Action`2<UnityEngine.Vector3[],System.Boolean>)
extern "C"  void PathResult__ctor_m1065815828 (PathResult_t1528013228 * __this, Vector3U5BU5D_t1718750761* ___path0, bool ___success1, Action_2_t1484661638 * ___callback2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<PathResult>::Invoke(!0)
#define Action_1_Invoke_m2478306080(__this, p0, method) ((  void (*) (Action_1_t1700480823 *, PathResult_t1528013228 , const RuntimeMethod*))Action_1_Invoke_m2478306080_gshared)(__this, p0, method)
// UnityEngine.Vector3[] Pathfinding::SimplifyPath(System.Collections.Generic.List`1<Node>)
extern "C"  Vector3U5BU5D_t1718750761* Pathfinding_SimplifyPath_m3637710884 (Pathfinding_t1696161914 * __this, List_1_t167102488 * ___path0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Reverse(System.Array)
extern "C"  void Array_Reverse_m3714848183 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
#define List_1__ctor_m1536473967(__this, method) ((  void (*) (List_1_t899420910 *, const RuntimeMethod*))List_1__ctor_m1536473967_gshared)(__this, method)
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2156229523  Vector2_get_zero_m540426400 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<Node>::get_Item(System.Int32)
#define List_1_get_Item_m1948408520(__this, p0, method) ((  Node_t2989995042 * (*) (List_1_t167102488 *, int32_t, const RuntimeMethod*))List_1_get_Item_m1328026504_gshared)(__this, p0, method)
// System.Boolean UnityEngine.Vector2::op_Inequality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Inequality_m3858779880 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
#define List_1_Add_m1524640104(__this, p0, method) ((  void (*) (List_1_t899420910 *, Vector3_t3722313464 , const RuntimeMethod*))List_1_Add_m1524640104_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<Node>::get_Count()
#define List_1_get_Count_m2928376997(__this, method) ((  int32_t (*) (List_1_t167102488 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method)
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
#define List_1_ToArray_m1691996104(__this, method) ((  Vector3U5BU5D_t1718750761* (*) (List_1_t899420910 *, const RuntimeMethod*))List_1_ToArray_m1691996104_gshared)(__this, method)
// System.Int32 UnityEngine.Mathf::Abs(System.Int32)
extern "C"  int32_t Mathf_Abs_m2460432655 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PathRequest::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,System.Action`2<UnityEngine.Vector3[],System.Boolean>)
extern "C"  void PathRequest__ctor_m1589651504 (PathRequest_t2117613800 * __this, Vector3_t3722313464  ____start0, Vector3_t3722313464  ____end1, Action_2_t1484661638 * ____callback2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<Pathfinding>()
#define Component_GetComponent_TisPathfinding_t1696161914_m4281442259(__this, method) ((  Pathfinding_t1696161914 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Queue`1<PathResult>::get_Count()
#define Queue_1_get_Count_m3067217344(__this, method) ((  int32_t (*) (Queue_1_t1374272722 *, const RuntimeMethod*))Queue_1_get_Count_m3067217344_gshared)(__this, method)
// System.Void System.Threading.Monitor::Enter(System.Object,System.Boolean&)
extern "C"  void Monitor_Enter_m984175629 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, bool* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.Queue`1<PathResult>::Dequeue()
#define Queue_1_Dequeue_m920511108(__this, method) ((  PathResult_t1528013228  (*) (Queue_1_t1374272722 *, const RuntimeMethod*))Queue_1_Dequeue_m920511108_gshared)(__this, method)
// System.Void System.Action`2<UnityEngine.Vector3[],System.Boolean>::Invoke(!0,!1)
#define Action_2_Invoke_m2062908132(__this, p0, p1, method) ((  void (*) (Action_2_t1484661638 *, Vector3U5BU5D_t1718750761*, bool, const RuntimeMethod*))Action_2_Invoke_m2879433563_gshared)(__this, p0, p1, method)
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m3585316909 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PathRequestManager/<>c__DisplayClass5_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass5_0__ctor_m3126442359 (U3CU3Ec__DisplayClass5_0_t786576806 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ThreadStart::.ctor(System.Object,System.IntPtr)
extern "C"  void ThreadStart__ctor_m3250019360 (ThreadStart_t1006689297 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ThreadStart::Invoke()
extern "C"  void ThreadStart_Invoke_m1483406622 (ThreadStart_t1006689297 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Queue`1<PathResult>::Enqueue(!0)
#define Queue_1_Enqueue_m1828253350(__this, p0, method) ((  void (*) (Queue_1_t1374272722 *, PathResult_t1528013228 , const RuntimeMethod*))Queue_1_Enqueue_m1828253350_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Queue`1<PathResult>::.ctor()
#define Queue_1__ctor_m534127087(__this, method) ((  void (*) (Queue_1_t1374272722 *, const RuntimeMethod*))Queue_1__ctor_m534127087_gshared)(__this, method)
// System.Void System.Action`1<PathResult>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m1884953144(__this, p0, p1, method) ((  void (*) (Action_1_t1700480823 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m1884953144_gshared)(__this, p0, p1, method)
// System.Void Pathfinding::FindPath(PathRequest,System.Action`1<PathResult>)
extern "C"  void Pathfinding_FindPath_m1666106048 (Pathfinding_t1696161914 * __this, PathRequest_t2117613800  ___request0, Action_1_t1700480823 * ___callback1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PauseMenu::DeselectAll()
extern "C"  void PauseMenu_DeselectAll_m424026074 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PauseMenu::DetectInput()
extern "C"  void PauseMenu_DetectInput_m2353324690 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PauseMenu::WhenPaused()
extern "C"  void PauseMenu_WhenPaused_m3668710605 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PauseMenu::PauseGame()
extern "C"  void PauseMenu_PauseGame_m3294073383 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PauseMenu::ResumeGame()
extern "C"  void PauseMenu_ResumeGame_m1593775081 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t4157153871 * Camera_get_main_m3643453163 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::MovePlayer()
extern "C"  void PlayerController_MovePlayer_m2560970386 (PlayerController_t2064355688 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::FireGun()
extern "C"  void PlayerController_FireGun_m2156033678 (PlayerController_t2064355688 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::UpdateReloadTime()
extern "C"  void PlayerController_UpdateReloadTime_m2370383035 (PlayerController_t2064355688 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::UseWeapon()
extern "C"  void PlayerController_UseWeapon_m3970795754 (PlayerController_t2064355688 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerController::UseShield()
extern "C"  void PlayerController_UseShield_m304736563 (PlayerController_t2064355688 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetButton(System.String)
extern "C"  bool Input_GetButton_m2064261504 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern "C"  bool Input_GetButtonDown_m3074252807 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Gun::ShootLaser()
extern "C"  void Gun_ShootLaser_m1916414356 (Gun_t783153271 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Inventory::RemoveItem(Item)
extern "C"  void Inventory_RemoveItem_m325135610 (Inventory_t1050226016 * __this, Item_t2953980098 * ___item0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
extern "C"  void Transform_Translate_m1810197270 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
extern "C"  void Transform_Rotate_m3172098886 (Transform_t3600365921 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SelectTitleOptions::DeselectAll()
extern "C"  void SelectTitleOptions_DeselectAll_m2671487348 (SelectTitleOptions_t2341392585 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SelectTitleOptions::UpdateOptions()
extern "C"  void SelectTitleOptions_UpdateOptions_m3436397335 (SelectTitleOptions_t2341392585 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SelectTitleOptions::SelectOptions()
extern "C"  void SelectTitleOptions_SelectOptions_m1091832534 (SelectTitleOptions_t2341392585 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SoftNormalsToVertexColor::TryGenerate()
extern "C"  void SoftNormalsToVertexColor_TryGenerate_m1356071037 (SoftNormalsToVertexColor_t279782399 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t3523625662_m1718783796(__this, method) ((  MeshFilter_t3523625662 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogError_m1665621915 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
extern "C"  Mesh_t3648964284 * MeshFilter_get_sharedMesh_m1726897210 (MeshFilter_t3523625662 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void SoftNormalsToVertexColor::Generate(UnityEngine.Mesh)
extern "C"  void SoftNormalsToVertexColor_Generate_m3390156695 (SoftNormalsToVertexColor_t279782399 * __this, Mesh_t3648964284 * ___m0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object,UnityEngine.Object)
extern "C"  void Debug_Log_m2288605041 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Object_t631007953 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
extern "C"  Vector3U5BU5D_t1718750761* Mesh_get_normals_m4099615978 (Mesh_t3648964284 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern "C"  Vector3U5BU5D_t1718750761* Mesh_get_vertices_m3585684815 (Mesh_t3648964284 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Int32>>::.ctor()
#define List_1__ctor_m612749431(__this, method) ((  void (*) (List_1_t1600127941 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Int32>>::GetEnumerator()
#define List_1_GetEnumerator_m784752458(__this, method) ((  Enumerator_t3489371818  (*) (List_1_t1600127941 *, const RuntimeMethod*))List_1_GetEnumerator_m816315209_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<System.Int32>>::get_Current()
#define Enumerator_get_Current_m2870147329(__this, method) ((  List_1_t128053199 * (*) (Enumerator_t3489371818 *, const RuntimeMethod*))Enumerator_get_Current_m337713592_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
#define List_1_get_Item_m888956288(__this, p0, method) ((  int32_t (*) (List_1_t128053199 *, int32_t, const RuntimeMethod*))List_1_get_Item_m888956288_gshared)(__this, p0, method)
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Equality_m4231250055 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
#define List_1_Add_m2080863212(__this, p0, method) ((  void (*) (List_1_t128053199 *, int32_t, const RuntimeMethod*))List_1_Add_m2080863212_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<System.Int32>>::MoveNext()
#define Enumerator_MoveNext_m1436172384(__this, method) ((  bool (*) (Enumerator_t3489371818 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<System.Int32>>::Dispose()
#define Enumerator_Dispose_m28313383(__this, method) ((  void (*) (Enumerator_t3489371818 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
#define List_1__ctor_m1204004817(__this, method) ((  void (*) (List_1_t128053199 *, const RuntimeMethod*))List_1__ctor_m1204004817_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.Int32>>::Add(!0)
#define List_1_Add_m3830006445(__this, p0, method) ((  void (*) (List_1_t1600127941 *, List_1_t128053199 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t3722313464  Vector3_get_zero_m1409827619 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
#define List_1_GetEnumerator_m2838255531(__this, method) ((  Enumerator_t2017297076  (*) (List_1_t128053199 *, const RuntimeMethod*))List_1_GetEnumerator_m2838255531_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
#define Enumerator_get_Current_m2612064142(__this, method) ((  int32_t (*) (Enumerator_t2017297076 *, const RuntimeMethod*))Enumerator_get_Current_m2612064142_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
#define Enumerator_MoveNext_m1050804954(__this, method) ((  bool (*) (Enumerator_t2017297076 *, const RuntimeMethod*))Enumerator_MoveNext_m1050804954_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
#define Enumerator_Dispose_m222348240(__this, method) ((  void (*) (Enumerator_t2017297076 *, const RuntimeMethod*))Enumerator_Dispose_m222348240_gshared)(__this, method)
// System.Void UnityEngine.Vector3::Normalize()
extern "C"  void Vector3_Normalize_m914904454 (Vector3_t3722313464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m606404487 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
#define List_1_get_Count_m361000296(__this, method) ((  int32_t (*) (List_1_t128053199 *, const RuntimeMethod*))List_1_get_Count_m361000296_gshared)(__this, method)
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m286683560 (Color_t2555686324 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_colors(UnityEngine.Color[])
extern "C"  void Mesh_set_colors_m2218481242 (Mesh_t3648964284 * __this, ColorU5BU5D_t941916413* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Unit::UpdatePath()
extern "C"  RuntimeObject* Unit_UpdatePath_m2481450807 (Unit_t4139495810 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Path::.ctor(UnityEngine.Vector3[],UnityEngine.Vector3,System.Single,System.Single)
extern "C"  void Path__ctor_m3753908269 (Path_t2615110272 * __this, Vector3U5BU5D_t1718750761* ___wayPoints0, Vector3_t3722313464  ___startPos1, float ___turnDst2, float ___stoppingDst3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
extern "C"  void MonoBehaviour_StopCoroutine_m1962070247 (MonoBehaviour_t3962482529 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Unit/<UpdatePath>d__10::.ctor(System.Int32)
extern "C"  void U3CUpdatePathU3Ed__10__ctor_m794121603 (U3CUpdatePathU3Ed__10_t1880251700 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Unit/<FollowPath>d__11::.ctor(System.Int32)
extern "C"  void U3CFollowPathU3Ed__11__ctor_m2685063142 (U3CFollowPathU3Ed__11_t3675468882 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Path::DrawWithGizmos()
extern "C"  void Path_DrawWithGizmos_m2056486393 (Path_t2615110272 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3)
extern "C"  void Transform_LookAt_m3649447396 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t2301928331  Quaternion_Lerp_m1238806789 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Quaternion_t2301928331  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Space)
extern "C"  void Transform_Translate_m1990195114 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_timeSinceLevelLoad()
extern "C"  float Time_get_timeSinceLevelLoad_m2224611026 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`2<UnityEngine.Vector3[],System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m2824747351(__this, p0, p1, method) ((  void (*) (Action_2_t1484661638 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_2__ctor_m1824561290_gshared)(__this, p0, p1, method)
// System.Void PathRequestManager::RequestPath(PathRequest)
extern "C"  void PathRequestManager_RequestPath_m3072144720 (RuntimeObject * __this /* static, unused */, PathRequest_t2117613800  ___request0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C"  float Vector3_get_sqrMagnitude_m1474274574 (Vector3_t3722313464 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AIStateMachine::Start()
extern "C"  void AIStateMachine_Start_m2140641173 (AIStateMachine_t2425577084 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AIStateMachine_Start_m2140641173_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// controller = gameObject.GetComponent<BossTankController>();
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		BossTankController_t1902331492 * L_1 = GameObject_GetComponent_TisBossTankController_t1902331492_m3861119340(L_0, /*hidden argument*/GameObject_GetComponent_TisBossTankController_t1902331492_m3861119340_RuntimeMethod_var);
		__this->set_controller_6(L_1);
		// phaseOne = true;
		((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->set_phaseOne_3((bool)1);
		// currentState = AIStates.PHASEONE;
		((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->set_currentState_2(0);
		// }
		return;
	}
}
// System.Void AIStateMachine::Update()
extern "C"  void AIStateMachine_Update_m3439513021 (AIStateMachine_t2425577084 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AIStateMachine_Update_m3439513021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// switch (currentState)
		int32_t L_0 = ((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->get_currentState_2();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_0031;
			}
			case 2:
			{
				goto IL_003d;
			}
			case 3:
			{
				goto IL_0049;
			}
			case 4:
			{
				goto IL_0054;
			}
			case 5:
			{
				goto IL_0054;
			}
		}
	}
	{
		return;
	}

IL_0025:
	{
		// controller.FindPlayer();
		BossTankController_t1902331492 * L_2 = __this->get_controller_6();
		NullCheck(L_2);
		BossTankController_FindPlayer_m4054288205(L_2, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_0031:
	{
		// controller.PhaseTwo();
		BossTankController_t1902331492 * L_3 = __this->get_controller_6();
		NullCheck(L_3);
		BossTankController_PhaseTwo_m3848855080(L_3, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_003d:
	{
		// controller.PhaseThree();
		BossTankController_t1902331492 * L_4 = __this->get_controller_6();
		NullCheck(L_4);
		BossTankController_PhaseThree_m1686685524(L_4, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_0049:
	{
		// controller.FindHealingSpot();
		BossTankController_t1902331492 * L_5 = __this->get_controller_6();
		NullCheck(L_5);
		BossTankController_FindHealingSpot_m1715802366(L_5, /*hidden argument*/NULL);
	}

IL_0054:
	{
		// }
		return;
	}
}
// System.Void AIStateMachine::.ctor()
extern "C"  void AIStateMachine__ctor_m1102640063 (AIStateMachine_t2425577084 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BackgroundScroller::Start()
extern "C"  void BackgroundScroller_Start_m2345096096 (BackgroundScroller_t2508866296 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BackgroundScroller_Start_m2345096096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// rect = GetComponent<RectTransform>();
		RectTransform_t3704657025 * L_0 = Component_GetComponent_TisRectTransform_t3704657025_m3396022872(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t3704657025_m3396022872_RuntimeMethod_var);
		__this->set_rect_4(L_0);
		// startPosition = rect.position; //transform.position;
		RectTransform_t3704657025 * L_1 = __this->get_rect_4();
		NullCheck(L_1);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		__this->set_startPosition_5(L_2);
		// }
		return;
	}
}
// System.Void BackgroundScroller::Update()
extern "C"  void BackgroundScroller_Update_m1770000666 (BackgroundScroller_t2508866296 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BackgroundScroller_Update_m1770000666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// float newPosition = Mathf.Repeat(Time.time * scrollSpeed, tileSizeY);
		float L_0 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_scrollSpeed_2();
		float L_2 = __this->get_tileSizeY_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Repeat_m1502810009(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)L_0, (float)L_1)), L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// rect.position = startPosition + Vector3.up * newPosition;
		RectTransform_t3704657025 * L_4 = __this->get_rect_4();
		Vector3_t3722313464  L_5 = __this->get_startPosition_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_6 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = V_0;
		Vector3_t3722313464  L_8 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_5, L_8, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_position_m3387557959(L_4, L_9, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BackgroundScroller::.ctor()
extern "C"  void BackgroundScroller__ctor_m1874393550 (BackgroundScroller_t2508866296 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BossTankController::Start()
extern "C"  void BossTankController_Start_m2778098626 (BossTankController_t1902331492 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void BossTankController::Update()
extern "C"  void BossTankController_Update_m1715711242 (BossTankController_t1902331492 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BossTankController_Update_m1715711242_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// CheckBossStats();
		BossTankController_CheckBossStats_m1300902599(__this, /*hidden argument*/NULL);
		// Debug.Log(AIStateMachine.currentState.ToString());
		RuntimeObject * L_0 = Box(AIStates_t2852279327_il2cpp_TypeInfo_var, (((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->get_address_of_currentState_2()));
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		*(((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->get_address_of_currentState_2()) = *(int32_t*)UnBox(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// Debug.Log(EnemyFuzzyState.currentFuzzyState.ToString() + " health");
		RuntimeObject * L_2 = Box(FuzzyStates_t3314355612_il2cpp_TypeInfo_var, (((EnemyFuzzyState_t3026123389_StaticFields*)il2cpp_codegen_static_fields_for(EnemyFuzzyState_t3026123389_il2cpp_TypeInfo_var))->get_address_of_currentFuzzyState_8()));
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		*(((EnemyFuzzyState_t3026123389_StaticFields*)il2cpp_codegen_static_fields_for(EnemyFuzzyState_t3026123389_il2cpp_TypeInfo_var))->get_address_of_currentFuzzyState_8()) = *(int32_t*)UnBox(L_2);
		String_t* L_4 = String_Concat_m3937257545(NULL /*static, unused*/, L_3, _stringLiteral2278281217, /*hidden argument*/NULL);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BossTankController::CheckBossStats()
extern "C"  void BossTankController_CheckBossStats_m1300902599 (BossTankController_t1902331492 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BossTankController_CheckBossStats_m1300902599_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// switch (EnemyFuzzyState.currentFuzzyState)
		int32_t L_0 = ((EnemyFuzzyState_t3026123389_StaticFields*)il2cpp_codegen_static_fields_for(EnemyFuzzyState_t3026123389_il2cpp_TypeInfo_var))->get_currentFuzzyState_8();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001a;
			}
			case 1:
			{
				goto IL_0043;
			}
			case 2:
			{
				goto IL_006c;
			}
		}
	}
	{
		goto IL_0072;
	}

IL_001a:
	{
		// if (AIStateMachine.phaseOne)
		bool L_2 = ((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->get_phaseOne_3();
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		// AIStateMachine.currentState = AIStateMachine.AIStates.PHASEONE;
		((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->set_currentState_2(0);
	}

IL_0027:
	{
		// if (AIStateMachine.phaseTwo)
		bool L_3 = ((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->get_phaseTwo_4();
		if (!L_3)
		{
			goto IL_0034;
		}
	}
	{
		// AIStateMachine.currentState = AIStateMachine.AIStates.PHASETWO;
		((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->set_currentState_2(1);
	}

IL_0034:
	{
		// if (AIStateMachine.phaseThree)
		bool L_4 = ((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->get_phaseThree_5();
		if (!L_4)
		{
			goto IL_0072;
		}
	}
	{
		// AIStateMachine.currentState = AIStateMachine.AIStates.PHASETHREE;
		((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->set_currentState_2(2);
		// break;
		goto IL_0072;
	}

IL_0043:
	{
		// if (AIStateMachine.phaseOne)
		bool L_5 = ((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->get_phaseOne_3();
		if (!L_5)
		{
			goto IL_0050;
		}
	}
	{
		// AIStateMachine.currentState = AIStateMachine.AIStates.PHASEONE;
		((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->set_currentState_2(0);
	}

IL_0050:
	{
		// if (AIStateMachine.phaseTwo)
		bool L_6 = ((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->get_phaseTwo_4();
		if (!L_6)
		{
			goto IL_005d;
		}
	}
	{
		// AIStateMachine.currentState = AIStateMachine.AIStates.PHASETWO;
		((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->set_currentState_2(1);
	}

IL_005d:
	{
		// if (AIStateMachine.phaseThree)
		bool L_7 = ((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->get_phaseThree_5();
		if (!L_7)
		{
			goto IL_0072;
		}
	}
	{
		// AIStateMachine.currentState = AIStateMachine.AIStates.PHASETHREE;
		((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->set_currentState_2(2);
		// break;
		goto IL_0072;
	}

IL_006c:
	{
		// AIStateMachine.currentState = AIStateMachine.AIStates.HEAL;
		((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->set_currentState_2(3);
	}

IL_0072:
	{
		// if (eStats.eCurrentHealth <= 0)
		EnemyStats_t4187841152 * L_8 = __this->get_eStats_2();
		NullCheck(L_8);
		int32_t L_9 = L_8->get_eCurrentHealth_2();
		if ((((int32_t)L_9) > ((int32_t)0)))
		{
			goto IL_0086;
		}
	}
	{
		// AIStateMachine.currentState = AIStateMachine.AIStates.DEFEATED;
		((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->set_currentState_2(5);
	}

IL_0086:
	{
		// }
		return;
	}
}
// System.Void BossTankController::FindPlayer()
extern "C"  void BossTankController_FindPlayer_m4054288205 (BossTankController_t1902331492 * __this, const RuntimeMethod* method)
{
	{
		// unit.target = player;
		Unit_t4139495810 * L_0 = __this->get_unit_4();
		Transform_t3600365921 * L_1 = __this->get_player_5();
		NullCheck(L_0);
		L_0->set_target_4(L_1);
		// eShoot.Shoot();
		EnemyShoot_t243830779 * L_2 = __this->get_eShoot_3();
		NullCheck(L_2);
		EnemyShoot_Shoot_m569725038(L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BossTankController::FindHealingSpot()
extern "C"  void BossTankController_FindHealingSpot_m1715802366 (BossTankController_t1902331492 * __this, const RuntimeMethod* method)
{
	{
		// unit.target = healingSpot;
		Unit_t4139495810 * L_0 = __this->get_unit_4();
		Transform_t3600365921 * L_1 = __this->get_healingSpot_6();
		NullCheck(L_0);
		L_0->set_target_4(L_1);
		// unit.speed = 10;
		Unit_t4139495810 * L_2 = __this->get_unit_4();
		NullCheck(L_2);
		L_2->set_speed_5((10.0f));
		// }
		return;
	}
}
// System.Void BossTankController::FindCenterPoint()
extern "C"  void BossTankController_FindCenterPoint_m3328217412 (BossTankController_t1902331492 * __this, const RuntimeMethod* method)
{
	{
		// unit.turnDst = 0;
		Unit_t4139495810 * L_0 = __this->get_unit_4();
		NullCheck(L_0);
		L_0->set_turnDst_7((0.0f));
		// unit.stoppingDst = 0;
		Unit_t4139495810 * L_1 = __this->get_unit_4();
		NullCheck(L_1);
		L_1->set_stoppingDst_8((0.0f));
		// unit.turnSpeed = 10;
		Unit_t4139495810 * L_2 = __this->get_unit_4();
		NullCheck(L_2);
		L_2->set_turnSpeed_6((10.0f));
		// unit.target = centerPoint.transform;
		Unit_t4139495810 * L_3 = __this->get_unit_4();
		GameObject_t1113636619 * L_4 = __this->get_centerPoint_9();
		NullCheck(L_4);
		Transform_t3600365921 * L_5 = GameObject_get_transform_m1369836730(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_target_4(L_5);
		// }
		return;
	}
}
// System.Void BossTankController::PhaseTwo()
extern "C"  void BossTankController_PhaseTwo_m3848855080 (BossTankController_t1902331492 * __this, const RuntimeMethod* method)
{
	{
		// unit.speed = 10;
		Unit_t4139495810 * L_0 = __this->get_unit_4();
		NullCheck(L_0);
		L_0->set_speed_5((10.0f));
		// eShoot.bulletSpeed = 12;
		EnemyShoot_t243830779 * L_1 = __this->get_eShoot_3();
		NullCheck(L_1);
		L_1->set_bulletSpeed_7((12.0f));
		// FindPlayer();
		BossTankController_FindPlayer_m4054288205(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BossTankController::PhaseThree()
extern "C"  void BossTankController_PhaseThree_m1686685524 (BossTankController_t1902331492 * __this, const RuntimeMethod* method)
{
	{
		// FindCenterPoint();
		BossTankController_FindCenterPoint_m3328217412(__this, /*hidden argument*/NULL);
		// transform.Rotate(new Vector3(0, 3));
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m1719387948((&L_1), (0.0f), (3.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_Rotate_m720511863(L_0, L_1, /*hidden argument*/NULL);
		// eShoot.cooldownTimeMax = 0.085f;
		EnemyShoot_t243830779 * L_2 = __this->get_eShoot_3();
		NullCheck(L_2);
		L_2->set_cooldownTimeMax_10((0.085f));
		// eShoot.bulletSpeed = 6;
		EnemyShoot_t243830779 * L_3 = __this->get_eShoot_3();
		NullCheck(L_3);
		L_3->set_bulletSpeed_7((6.0f));
		// eShoot.ShootLaser();
		EnemyShoot_t243830779 * L_4 = __this->get_eShoot_3();
		NullCheck(L_4);
		EnemyShoot_ShootLaser_m1234821334(L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void BossTankController::.ctor()
extern "C"  void BossTankController__ctor_m402375086 (BossTankController_t1902331492 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BossUI::Start()
extern "C"  void BossUI_Start_m2180358406 (BossUI_t1795207708 * __this, const RuntimeMethod* method)
{
	{
		// enemyHealthBarValue = enemyTank.eCurrentHealth;
		EnemyStats_t4187841152 * L_0 = __this->get_enemyTank_2();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_eCurrentHealth_2();
		__this->set_enemyHealthBarValue_4((((float)((float)L_1))));
		// }
		return;
	}
}
// System.Void BossUI::Update()
extern "C"  void BossUI_Update_m1511382654 (BossUI_t1795207708 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BossUI_Update_m1511382654_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// enemyHealthBarValue = (float)enemyTank.eCurrentHealth / enemyTank.eMaximumHealth;
		EnemyStats_t4187841152 * L_0 = __this->get_enemyTank_2();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_eCurrentHealth_2();
		EnemyStats_t4187841152 * L_2 = __this->get_enemyTank_2();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_eMaximumHealth_3();
		__this->set_enemyHealthBarValue_4(((float)((float)(((float)((float)L_1)))/(float)(((float)((float)L_3))))));
		// enemyHealthBar.fillAmount = enemyHealthBarValue;
		Image_t2670269651 * L_4 = __this->get_enemyHealthBar_3();
		float L_5 = __this->get_enemyHealthBarValue_4();
		NullCheck(L_4);
		Image_set_fillAmount_m3737925590(L_4, L_5, /*hidden argument*/NULL);
		// playerHealthText.text = "Player Health: " + playerTank.pCurrentHealth.ToString();
		Text_t1901882714 * L_6 = __this->get_playerHealthText_6();
		PlayerStats_t2044123780 * L_7 = __this->get_playerTank_5();
		NullCheck(L_7);
		int32_t* L_8 = L_7->get_address_of_pCurrentHealth_2();
		String_t* L_9 = Int32_ToString_m141394615((int32_t*)L_8, /*hidden argument*/NULL);
		String_t* L_10 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral833246398, L_9, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_10);
		// }
		return;
	}
}
// System.Void BossUI::.ctor()
extern "C"  void BossUI__ctor_m1470505008 (BossUI_t1795207708 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Bullet::Start()
extern "C"  void Bullet_Start_m1480719556 (Bullet_t1042140031 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bullet_Start_m1480719556_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// material = GetComponent<Renderer>().material;
		Renderer_t2627027031 * L_0 = Component_GetComponent_TisRenderer_t2627027031_m2651633905(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t2627027031_m2651633905_RuntimeMethod_var);
		NullCheck(L_0);
		Material_t340375123 * L_1 = Renderer_get_material_m4171603682(L_0, /*hidden argument*/NULL);
		__this->set_material_3(L_1);
		// originalCol = material.color;
		Material_t340375123 * L_2 = __this->get_material_3();
		NullCheck(L_2);
		Color_t2555686324  L_3 = Material_get_color_m3827673574(L_2, /*hidden argument*/NULL);
		__this->set_originalCol_4(L_3);
		// disposeTime = Time.time + lifeTime;
		float L_4 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_lifeTime_2();
		__this->set_disposeTime_6(((float)il2cpp_codegen_add((float)L_4, (float)L_5)));
		// StartCoroutine("Fade");
		MonoBehaviour_StartCoroutine_m2618285814(__this, _stringLiteral1985039568, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator Bullet::Fade()
extern "C"  RuntimeObject* Bullet_Fade_m3288204635 (Bullet_t1042140031 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bullet_Fade_m3288204635_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CFadeU3Ed__7_t626104978 * L_0 = (U3CFadeU3Ed__7_t626104978 *)il2cpp_codegen_object_new(U3CFadeU3Ed__7_t626104978_il2cpp_TypeInfo_var);
		U3CFadeU3Ed__7__ctor_m1264547669(L_0, 0, /*hidden argument*/NULL);
		U3CFadeU3Ed__7_t626104978 * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void Bullet::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void Bullet_OnTriggerEnter_m3806200962 (Bullet_t1042140031 * __this, Collider_t1773347010 * ___c0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bullet_OnTriggerEnter_m3806200962_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (c.tag == "Ground")
		Collider_t1773347010 * L_0 = ___c0;
		NullCheck(L_0);
		String_t* L_1 = Component_get_tag_m2716693327(L_0, /*hidden argument*/NULL);
		bool L_2 = String_op_Equality_m920492651(NULL /*static, unused*/, L_1, _stringLiteral3128803744, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		// GetComponent<Rigidbody>().Sleep();
		Rigidbody_t3916780224 * L_3 = Component_GetComponent_TisRigidbody_t3916780224_m279685075(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t3916780224_m279685075_RuntimeMethod_var);
		NullCheck(L_3);
		Rigidbody_Sleep_m1823108224(L_3, /*hidden argument*/NULL);
	}

IL_001d:
	{
		// }
		return;
	}
}
// System.Void Bullet::.ctor()
extern "C"  void Bullet__ctor_m2787309514 (Bullet_t1042140031 * __this, const RuntimeMethod* method)
{
	{
		// private float lifeTime = 0.5f;
		__this->set_lifeTime_2((0.5f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Bullet/<Fade>d__7::.ctor(System.Int32)
extern "C"  void U3CFadeU3Ed__7__ctor_m1264547669 (U3CFadeU3Ed__7_t626104978 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Bullet/<Fade>d__7::System.IDisposable.Dispose()
extern "C"  void U3CFadeU3Ed__7_System_IDisposable_Dispose_m1820615146 (U3CFadeU3Ed__7_t626104978 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Bullet/<Fade>d__7::MoveNext()
extern "C"  bool U3CFadeU3Ed__7_MoveNext_m1628721258 (U3CFadeU3Ed__7_t626104978 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFadeU3Ed__7_MoveNext_m1628721258_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0030;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0017:
	{
		// yield return new WaitForSeconds(0.2f);
		WaitForSeconds_t1699091251 * L_3 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_3, (0.2f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_3);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0030:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (fading)
		Bullet_t1042140031 * L_4 = __this->get_U3CU3E4__this_2();
		NullCheck(L_4);
		bool L_5 = L_4->get_fading_7();
		if (!L_5)
		{
			goto IL_00ba;
		}
	}
	{
		// fadePercent += Time.deltaTime;
		Bullet_t1042140031 * L_6 = __this->get_U3CU3E4__this_2();
		Bullet_t1042140031 * L_7 = __this->get_U3CU3E4__this_2();
		NullCheck(L_7);
		float L_8 = L_7->get_fadePercent_5();
		float L_9 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		L_6->set_fadePercent_5(((float)il2cpp_codegen_add((float)L_8, (float)L_9)));
		// material.color = Color.Lerp(originalCol, Color.clear, fadePercent);
		Bullet_t1042140031 * L_10 = __this->get_U3CU3E4__this_2();
		NullCheck(L_10);
		Material_t340375123 * L_11 = L_10->get_material_3();
		Bullet_t1042140031 * L_12 = __this->get_U3CU3E4__this_2();
		NullCheck(L_12);
		Color_t2555686324  L_13 = L_12->get_originalCol_4();
		Color_t2555686324  L_14 = Color_get_clear_m1016382534(NULL /*static, unused*/, /*hidden argument*/NULL);
		Bullet_t1042140031 * L_15 = __this->get_U3CU3E4__this_2();
		NullCheck(L_15);
		float L_16 = L_15->get_fadePercent_5();
		Color_t2555686324  L_17 = Color_Lerp_m973389909(NULL /*static, unused*/, L_13, L_14, L_16, /*hidden argument*/NULL);
		NullCheck(L_11);
		Material_set_color_m1794818007(L_11, L_17, /*hidden argument*/NULL);
		// if (fadePercent >= 1)
		Bullet_t1042140031 * L_18 = __this->get_U3CU3E4__this_2();
		NullCheck(L_18);
		float L_19 = L_18->get_fadePercent_5();
		if ((!(((float)L_19) >= ((float)(1.0f)))))
		{
			goto IL_0017;
		}
	}
	{
		// Destroy(gameObject);
		Bullet_t1042140031 * L_20 = __this->get_U3CU3E4__this_2();
		NullCheck(L_20);
		GameObject_t1113636619 * L_21 = Component_get_gameObject_m442555142(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		// }
		goto IL_0017;
	}

IL_00ba:
	{
		// if (Time.time > disposeTime)
		float L_22 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		Bullet_t1042140031 * L_23 = __this->get_U3CU3E4__this_2();
		NullCheck(L_23);
		float L_24 = L_23->get_disposeTime_6();
		if ((!(((float)L_22) > ((float)L_24))))
		{
			goto IL_0017;
		}
	}
	{
		// fading = true;
		Bullet_t1042140031 * L_25 = __this->get_U3CU3E4__this_2();
		NullCheck(L_25);
		L_25->set_fading_7((bool)1);
		// while (true)
		goto IL_0017;
	}
}
// System.Object Bullet/<Fade>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C"  RuntimeObject * U3CFadeU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m830265175 (U3CFadeU3Ed__7_t626104978 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Bullet/<Fade>d__7::System.Collections.IEnumerator.Reset()
extern "C"  void U3CFadeU3Ed__7_System_Collections_IEnumerator_Reset_m3612806028 (U3CFadeU3Ed__7_t626104978 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFadeU3Ed__7_System_Collections_IEnumerator_Reset_m3612806028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CFadeU3Ed__7_System_Collections_IEnumerator_Reset_m3612806028_RuntimeMethod_var);
	}
}
// System.Object Bullet/<Fade>d__7::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CFadeU3Ed__7_System_Collections_IEnumerator_get_Current_m199706421 (U3CFadeU3Ed__7_t626104978 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DamageEnemy::Start()
extern "C"  void DamageEnemy_Start_m4217926186 (DamageEnemy_t2198349935 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DamageEnemy_Start_m4217926186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// healthUI = FindObjectOfType<HealthUI>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		HealthUI_t1908446248 * L_0 = Object_FindObjectOfType_TisHealthUI_t1908446248_m1692366917(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisHealthUI_t1908446248_m1692366917_RuntimeMethod_var);
		__this->set_healthUI_5(L_0);
		// }
		return;
	}
}
// System.Void DamageEnemy::Update()
extern "C"  void DamageEnemy_Update_m507921797 (DamageEnemy_t2198349935 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void DamageEnemy::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void DamageEnemy_OnTriggerEnter_m228673893 (DamageEnemy_t2198349935 * __this, Collider_t1773347010 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DamageEnemy_OnTriggerEnter_m228673893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (collider.gameObject.tag == "Enemy")
		Collider_t1773347010 * L_0 = ___collider0;
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m3951609671(L_1, /*hidden argument*/NULL);
		bool L_3 = String_op_Equality_m920492651(NULL /*static, unused*/, L_2, _stringLiteral760905195, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0082;
		}
	}
	{
		// healthUI.GetComponent<AudioSource>().PlayOneShot(healthUI.destroyBullet, 0.2f);
		HealthUI_t1908446248 * L_4 = __this->get_healthUI_5();
		NullCheck(L_4);
		AudioSource_t3935305588 * L_5 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(L_4, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		HealthUI_t1908446248 * L_6 = __this->get_healthUI_5();
		NullCheck(L_6);
		AudioClip_t3680889665 * L_7 = L_6->get_destroyBullet_3();
		NullCheck(L_5);
		AudioSource_PlayOneShot_m2678069419(L_5, L_7, (0.2f), /*hidden argument*/NULL);
		// Destroy(gameObject);
		GameObject_t1113636619 * L_8 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		// Instantiate(explosionBig, transform.position, transform.rotation);
		GameObject_t1113636619 * L_9 = __this->get_explosionBig_3();
		Transform_t3600365921 * L_10 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t3722313464  L_11 = Transform_get_position_m36019626(L_10, /*hidden argument*/NULL);
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Quaternion_t2301928331  L_13 = Transform_get_rotation_m3502953881(L_12, /*hidden argument*/NULL);
		Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_9, L_11, L_13, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		// collider.gameObject.GetComponent<EnemyStats>().eCurrentHealth -= damageValue;
		Collider_t1773347010 * L_14 = ___collider0;
		NullCheck(L_14);
		GameObject_t1113636619 * L_15 = Component_get_gameObject_m442555142(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		EnemyStats_t4187841152 * L_16 = GameObject_GetComponent_TisEnemyStats_t4187841152_m2036182613(L_15, /*hidden argument*/GameObject_GetComponent_TisEnemyStats_t4187841152_m2036182613_RuntimeMethod_var);
		EnemyStats_t4187841152 * L_17 = L_16;
		NullCheck(L_17);
		int32_t L_18 = L_17->get_eCurrentHealth_2();
		int32_t L_19 = __this->get_damageValue_4();
		NullCheck(L_17);
		L_17->set_eCurrentHealth_2(((int32_t)il2cpp_codegen_subtract((int32_t)L_18, (int32_t)L_19)));
		// }
		return;
	}

IL_0082:
	{
		// healthUI.GetComponent<AudioSource>().PlayOneShot(healthUI.destroyBullet, 0.2f);
		HealthUI_t1908446248 * L_20 = __this->get_healthUI_5();
		NullCheck(L_20);
		AudioSource_t3935305588 * L_21 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(L_20, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		HealthUI_t1908446248 * L_22 = __this->get_healthUI_5();
		NullCheck(L_22);
		AudioClip_t3680889665 * L_23 = L_22->get_destroyBullet_3();
		NullCheck(L_21);
		AudioSource_PlayOneShot_m2678069419(L_21, L_23, (0.2f), /*hidden argument*/NULL);
		// Instantiate(explosionSmall, transform.position, transform.rotation);
		GameObject_t1113636619 * L_24 = __this->get_explosionSmall_2();
		Transform_t3600365921 * L_25 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t3722313464  L_26 = Transform_get_position_m36019626(L_25, /*hidden argument*/NULL);
		Transform_t3600365921 * L_27 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Quaternion_t2301928331  L_28 = Transform_get_rotation_m3502953881(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_24, L_26, L_28, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		// Destroy(gameObject);
		GameObject_t1113636619 * L_29 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DamageEnemy::.ctor()
extern "C"  void DamageEnemy__ctor_m157066984 (DamageEnemy_t2198349935 * __this, const RuntimeMethod* method)
{
	{
		// public int damageValue = 1;         // How much damage each bullet will do
		__this->set_damageValue_4(1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DamagePlayer::Start()
extern "C"  void DamagePlayer_Start_m900557365 (DamagePlayer_t2033828012 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DamagePlayer_Start_m900557365_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// healthUI = FindObjectOfType<HealthUI>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		HealthUI_t1908446248 * L_0 = Object_FindObjectOfType_TisHealthUI_t1908446248_m1692366917(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisHealthUI_t1908446248_m1692366917_RuntimeMethod_var);
		__this->set_healthUI_5(L_0);
		// }
		return;
	}
}
// System.Void DamagePlayer::Update()
extern "C"  void DamagePlayer_Update_m3652686413 (DamagePlayer_t2033828012 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void DamagePlayer::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void DamagePlayer_OnTriggerEnter_m878711311 (DamagePlayer_t2033828012 * __this, Collider_t1773347010 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DamagePlayer_OnTriggerEnter_m878711311_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// healthUI.GetComponent<AudioSource>().PlayOneShot(healthUI.destroyBullet, 0.2f);
		HealthUI_t1908446248 * L_0 = __this->get_healthUI_5();
		NullCheck(L_0);
		AudioSource_t3935305588 * L_1 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(L_0, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		HealthUI_t1908446248 * L_2 = __this->get_healthUI_5();
		NullCheck(L_2);
		AudioClip_t3680889665 * L_3 = L_2->get_destroyBullet_3();
		NullCheck(L_1);
		AudioSource_PlayOneShot_m2678069419(L_1, L_3, (0.2f), /*hidden argument*/NULL);
		// if (collider.gameObject.tag == "Player")
		Collider_t1773347010 * L_4 = ___collider0;
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = GameObject_get_tag_m3951609671(L_5, /*hidden argument*/NULL);
		bool L_7 = String_op_Equality_m920492651(NULL /*static, unused*/, L_6, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0082;
		}
	}
	{
		// Instantiate(explosionBig, transform.position, transform.rotation);
		GameObject_t1113636619 * L_8 = __this->get_explosionBig_3();
		Transform_t3600365921 * L_9 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t3722313464  L_10 = Transform_get_position_m36019626(L_9, /*hidden argument*/NULL);
		Transform_t3600365921 * L_11 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		Quaternion_t2301928331  L_12 = Transform_get_rotation_m3502953881(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_8, L_10, L_12, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		// collider.gameObject.GetComponent<PlayerStats>().pCurrentHealth -= damageValue;
		Collider_t1773347010 * L_13 = ___collider0;
		NullCheck(L_13);
		GameObject_t1113636619 * L_14 = Component_get_gameObject_m442555142(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		PlayerStats_t2044123780 * L_15 = GameObject_GetComponent_TisPlayerStats_t2044123780_m4080514128(L_14, /*hidden argument*/GameObject_GetComponent_TisPlayerStats_t2044123780_m4080514128_RuntimeMethod_var);
		PlayerStats_t2044123780 * L_16 = L_15;
		NullCheck(L_16);
		int32_t L_17 = L_16->get_pCurrentHealth_2();
		int32_t L_18 = __this->get_damageValue_4();
		NullCheck(L_16);
		L_16->set_pCurrentHealth_2(((int32_t)il2cpp_codegen_subtract((int32_t)L_17, (int32_t)L_18)));
		// Destroy(gameObject);
		GameObject_t1113636619 * L_19 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0082:
	{
		// else if (collider.gameObject.tag == "Enemy")
		Collider_t1773347010 * L_20 = ___collider0;
		NullCheck(L_20);
		GameObject_t1113636619 * L_21 = Component_get_gameObject_m442555142(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_22 = GameObject_get_tag_m3951609671(L_21, /*hidden argument*/NULL);
		bool L_23 = String_op_Equality_m920492651(NULL /*static, unused*/, L_22, _stringLiteral760905195, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00e4;
		}
	}
	{
		// Instantiate(explosionBig, transform.position, transform.rotation);
		GameObject_t1113636619 * L_24 = __this->get_explosionBig_3();
		Transform_t3600365921 * L_25 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_25);
		Vector3_t3722313464  L_26 = Transform_get_position_m36019626(L_25, /*hidden argument*/NULL);
		Transform_t3600365921 * L_27 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Quaternion_t2301928331  L_28 = Transform_get_rotation_m3502953881(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_24, L_26, L_28, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		// collider.gameObject.GetComponent<EnemyStats>().eCurrentHealth -= damageValue;
		Collider_t1773347010 * L_29 = ___collider0;
		NullCheck(L_29);
		GameObject_t1113636619 * L_30 = Component_get_gameObject_m442555142(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		EnemyStats_t4187841152 * L_31 = GameObject_GetComponent_TisEnemyStats_t4187841152_m2036182613(L_30, /*hidden argument*/GameObject_GetComponent_TisEnemyStats_t4187841152_m2036182613_RuntimeMethod_var);
		EnemyStats_t4187841152 * L_32 = L_31;
		NullCheck(L_32);
		int32_t L_33 = L_32->get_eCurrentHealth_2();
		int32_t L_34 = __this->get_damageValue_4();
		NullCheck(L_32);
		L_32->set_eCurrentHealth_2(((int32_t)il2cpp_codegen_subtract((int32_t)L_33, (int32_t)L_34)));
		// Destroy(gameObject);
		GameObject_t1113636619 * L_35 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00e4:
	{
		// Instantiate(explosionSmall, transform.position, transform.rotation);
		GameObject_t1113636619 * L_36 = __this->get_explosionSmall_2();
		Transform_t3600365921 * L_37 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		Vector3_t3722313464  L_38 = Transform_get_position_m36019626(L_37, /*hidden argument*/NULL);
		Transform_t3600365921 * L_39 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_39);
		Quaternion_t2301928331  L_40 = Transform_get_rotation_m3502953881(L_39, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_36, L_38, L_40, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		// Destroy(gameObject);
		GameObject_t1113636619 * L_41 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_41, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void DamagePlayer::.ctor()
extern "C"  void DamagePlayer__ctor_m161439903 (DamagePlayer_t2033828012 * __this, const RuntimeMethod* method)
{
	{
		// public int damageValue = 1;         // How much damage each bullet will do
		__this->set_damageValue_4(1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnableCameraDepthInForward::Start()
extern "C"  void EnableCameraDepthInForward_Start_m3387668119 (EnableCameraDepthInForward_t3351256833 * __this, const RuntimeMethod* method)
{
	{
		// Set();
		EnableCameraDepthInForward_Set_m3860174829(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnableCameraDepthInForward::Set()
extern "C"  void EnableCameraDepthInForward_Set_m3860174829 (EnableCameraDepthInForward_t3351256833 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnableCameraDepthInForward_Set_m3860174829_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(GetComponent<Camera>().depthTextureMode == DepthTextureMode.None)
		Camera_t4157153871 * L_0 = Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		NullCheck(L_0);
		int32_t L_1 = Camera_get_depthTextureMode_m871144641(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		// GetComponent<Camera>().depthTextureMode = DepthTextureMode.Depth;
		Camera_t4157153871 * L_2 = Component_GetComponent_TisCamera_t4157153871_m1557787507(__this, /*hidden argument*/Component_GetComponent_TisCamera_t4157153871_m1557787507_RuntimeMethod_var);
		NullCheck(L_2);
		Camera_set_depthTextureMode_m754977860(L_2, 1, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// }
		return;
	}
}
// System.Void EnableCameraDepthInForward::.ctor()
extern "C"  void EnableCameraDepthInForward__ctor_m2771181567 (EnableCameraDepthInForward_t3351256833 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemyController::Start()
extern "C"  void EnemyController_Start_m1137308130 (EnemyController_t2191660454 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyController_Start_m1137308130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// target = PlayerManager.instance.player.transform;
		PlayerManager_t1349889689 * L_0 = ((PlayerManager_t1349889689_StaticFields*)il2cpp_codegen_static_fields_for(PlayerManager_t1349889689_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = L_0->get_player_3();
		NullCheck(L_1);
		Transform_t3600365921 * L_2 = GameObject_get_transform_m1369836730(L_1, /*hidden argument*/NULL);
		__this->set_target_6(L_2);
		// agent = GetComponent<NavMeshAgent>();
		NavMeshAgent_t1276799816 * L_3 = Component_GetComponent_TisNavMeshAgent_t1276799816_m597731532(__this, /*hidden argument*/Component_GetComponent_TisNavMeshAgent_t1276799816_m597731532_RuntimeMethod_var);
		__this->set_agent_2(L_3);
		// }
		return;
	}
}
// System.Void EnemyController::Update()
extern "C"  void EnemyController_Update_m2232054175 (EnemyController_t2191660454 * __this, const RuntimeMethod* method)
{
	{
		// MoveTowardsTarget();
		EnemyController_MoveTowardsTarget_m1657189778(__this, /*hidden argument*/NULL);
		// UpdateReloadTime();
		EnemyController_UpdateReloadTime_m1142540528(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyController::MoveTowardsTarget()
extern "C"  void EnemyController_MoveTowardsTarget_m1657189778 (EnemyController_t2191660454 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyController_MoveTowardsTarget_m1657189778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// float distance = Vector3.Distance(target.position, transform.position);
		Transform_t3600365921 * L_0 = __this->get_target_6();
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_4 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// if (distance <= lookRadius)
		float L_5 = V_0;
		float L_6 = __this->get_lookRadius_4();
		if ((!(((float)L_5) <= ((float)L_6))))
		{
			goto IL_005b;
		}
	}
	{
		// agent.SetDestination(target.position);
		NavMeshAgent_t1276799816 * L_7 = __this->get_agent_2();
		Transform_t3600365921 * L_8 = __this->get_target_6();
		NullCheck(L_8);
		Vector3_t3722313464  L_9 = Transform_get_position_m36019626(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		NavMeshAgent_SetDestination_m2826390109(L_7, L_9, /*hidden argument*/NULL);
		// gun.Shoot();
		Gun_t783153271 * L_10 = __this->get_gun_3();
		NullCheck(L_10);
		Gun_Shoot_m3160136974(L_10, /*hidden argument*/NULL);
		// if (distance <= agent.stoppingDistance)
		float L_11 = V_0;
		NavMeshAgent_t1276799816 * L_12 = __this->get_agent_2();
		NullCheck(L_12);
		float L_13 = NavMeshAgent_get_stoppingDistance_m1834551109(L_12, /*hidden argument*/NULL);
		if ((!(((float)L_11) <= ((float)L_13))))
		{
			goto IL_005b;
		}
	}
	{
		// FaceTarget();
		EnemyController_FaceTarget_m3754874152(__this, /*hidden argument*/NULL);
	}

IL_005b:
	{
		// }
		return;
	}
}
// System.Void EnemyController::FaceTarget()
extern "C"  void EnemyController_FaceTarget_m3754874152 (EnemyController_t2191660454 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyController_FaceTarget_m3754874152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		// Vector3 direction = (target.position - transform.position).normalized;
		Transform_t3600365921 * L_0 = __this->get_target_6();
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		Vector3_t3722313464  L_5 = Vector3_get_normalized_m2454957984((Vector3_t3722313464 *)(&V_2), /*hidden argument*/NULL);
		V_0 = L_5;
		// Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
		Vector3_t3722313464  L_6 = V_0;
		float L_7 = L_6.get_x_1();
		Vector3_t3722313464  L_8 = V_0;
		float L_9 = L_8.get_z_3();
		Vector3_t3722313464  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m3353183577((&L_10), L_7, (0.0f), L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_11 = Quaternion_LookRotation_m4040767668(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		// transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5.0f);
		Transform_t3600365921 * L_12 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_13 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Quaternion_t2301928331  L_14 = Transform_get_rotation_m3502953881(L_13, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_15 = V_1;
		float L_16 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_17 = Quaternion_Slerp_m1234055455(NULL /*static, unused*/, L_14, L_15, ((float)il2cpp_codegen_multiply((float)L_16, (float)(5.0f))), /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_rotation_m3524318132(L_12, L_17, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyController::UpdateReloadTime()
extern "C"  void EnemyController_UpdateReloadTime_m1142540528 (EnemyController_t2191660454 * __this, const RuntimeMethod* method)
{
	{
		// if (reloadTime >= 2.0f)
		float L_0 = __this->get_reloadTime_5();
		if ((!(((float)L_0) >= ((float)(2.0f)))))
		{
			goto IL_0024;
		}
	}
	{
		// gun.rounds = 1;
		Gun_t783153271 * L_1 = __this->get_gun_3();
		NullCheck(L_1);
		L_1->set_rounds_9(1);
		// reloadTime = 0.0f;
		__this->set_reloadTime_5((0.0f));
	}

IL_0024:
	{
		// reloadTime += Time.deltaTime;
		float L_2 = __this->get_reloadTime_5();
		float L_3 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_reloadTime_5(((float)il2cpp_codegen_add((float)L_2, (float)L_3)));
		// }
		return;
	}
}
// System.Void EnemyController::CreateFOV()
extern "C"  void EnemyController_CreateFOV_m1491746096 (EnemyController_t2191660454 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyController_CreateFOV_m1491746096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// fieldOfViewBox = gameObject.AddComponent<BoxCollider>();
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		BoxCollider_t1640800422 * L_1 = GameObject_AddComponent_TisBoxCollider_t1640800422_m1661675543(L_0, /*hidden argument*/GameObject_AddComponent_TisBoxCollider_t1640800422_m1661675543_RuntimeMethod_var);
		__this->set_fieldOfViewBox_7(L_1);
		// fieldOfViewBox.center = new Vector3(0, 0, lookRadius / 2);
		BoxCollider_t1640800422 * L_2 = __this->get_fieldOfViewBox_7();
		float L_3 = __this->get_lookRadius_4();
		Vector3_t3722313464  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m3353183577((&L_4), (0.0f), (0.0f), ((float)((float)L_3/(float)(2.0f))), /*hidden argument*/NULL);
		NullCheck(L_2);
		BoxCollider_set_center_m1086194996(L_2, L_4, /*hidden argument*/NULL);
		// fieldOfViewBox.size = new Vector3(2, 1, lookRadius);
		BoxCollider_t1640800422 * L_5 = __this->get_fieldOfViewBox_7();
		float L_6 = __this->get_lookRadius_4();
		Vector3_t3722313464  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m3353183577((&L_7), (2.0f), (1.0f), L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		BoxCollider_set_size_m3618428148(L_5, L_7, /*hidden argument*/NULL);
		// fieldOfViewBox.isTrigger = true;
		BoxCollider_t1640800422 * L_8 = __this->get_fieldOfViewBox_7();
		NullCheck(L_8);
		Collider_set_isTrigger_m1770557358(L_8, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyController::OnDrawGizmosSelected()
extern "C"  void EnemyController_OnDrawGizmosSelected_m3456646563 (EnemyController_t2191660454 * __this, const RuntimeMethod* method)
{
	{
		// Gizmos.color = Color.red;
		Color_t2555686324  L_0 = Color_get_red_m3227813939(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m3399737545(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		// Gizmos.DrawWireSphere(transform.position, lookRadius);
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		float L_3 = __this->get_lookRadius_4();
		Gizmos_DrawWireSphere_m132265467(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyController::.ctor()
extern "C"  void EnemyController__ctor_m162774262 (EnemyController_t2191660454 * __this, const RuntimeMethod* method)
{
	{
		// public float lookRadius = 10.0f;                // The FOV of the Enemy
		__this->set_lookRadius_4((10.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemyCount::Start()
extern "C"  void EnemyCount_Start_m3648910747 (EnemyCount_t3730988989 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyCount_Start_m3648910747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// levelClear = FindObjectOfType<LevelClear>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		LevelClear_t1949285084 * L_0 = Object_FindObjectOfType_TisLevelClear_t1949285084_m2992299238(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisLevelClear_t1949285084_m2992299238_RuntimeMethod_var);
		__this->set_levelClear_4(L_0);
		// enemy = GameObject.FindGameObjectsWithTag("Enemy");
		GameObjectU5BU5D_t3328599146* L_1 = GameObject_FindGameObjectsWithTag_m2585173894(NULL /*static, unused*/, _stringLiteral760905195, /*hidden argument*/NULL);
		__this->set_enemy_2(L_1);
		// enemyNumber = enemy.Length;
		GameObjectU5BU5D_t3328599146* L_2 = __this->get_enemy_2();
		NullCheck(L_2);
		__this->set_enemyNumber_3((((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length)))));
		// }
		return;
	}
}
// System.Void EnemyCount::Update()
extern "C"  void EnemyCount_Update_m2074001267 (EnemyCount_t3730988989 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyCount_Update_m2074001267_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (enemyNumber <= 0)
		int32_t L_0 = __this->get_enemyNumber_3();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		// levelClear.FadeToLevel("LevelSelection");
		LevelClear_t1949285084 * L_1 = __this->get_levelClear_4();
		NullCheck(L_1);
		LevelClear_FadeToLevel_m1500572359(L_1, _stringLiteral2662547089, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// }
		return;
	}
}
// System.Void EnemyCount::.ctor()
extern "C"  void EnemyCount__ctor_m2648814069 (EnemyCount_t3730988989 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemyFuzzyState::Start()
extern "C"  void EnemyFuzzyState_Start_m1284165874 (EnemyFuzzyState_t3026123389 * __this, const RuntimeMethod* method)
{
	{
		// SetLabels();
		EnemyFuzzyState_SetLabels_m2897062058(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyFuzzyState::Update()
extern "C"  void EnemyFuzzyState_Update_m3014890715 (EnemyFuzzyState_t3026123389 * __this, const RuntimeMethod* method)
{
	{
		// EvaluateStatements();
		EnemyFuzzyState_EvaluateStatements_m2484608551(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyFuzzyState::EvaluateStatements()
extern "C"  void EnemyFuzzyState_EvaluateStatements_m2484608551 (EnemyFuzzyState_t3026123389 * __this, const RuntimeMethod* method)
{
	{
		// if (enemyTank.eCurrentHealth <= 0)
		EnemyStats_t4187841152 * L_0 = __this->get_enemyTank_5();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_eCurrentHealth_2();
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		// return;
		return;
	}

IL_000f:
	{
		// enemyHealth = ((float)enemyTank.eCurrentHealth / enemyTank.eMaximumHealth) * 100;
		EnemyStats_t4187841152 * L_2 = __this->get_enemyTank_5();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_eCurrentHealth_2();
		EnemyStats_t4187841152 * L_4 = __this->get_enemyTank_5();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_eMaximumHealth_3();
		__this->set_enemyHealth_7(((float)il2cpp_codegen_multiply((float)((float)((float)(((float)((float)L_3)))/(float)(((float)((float)L_5))))), (float)(100.0f))));
		// valueHealthy = healthy.Evaluate(enemyHealth);
		AnimationCurve_t3046754366 * L_6 = __this->get_healthy_4();
		float L_7 = __this->get_enemyHealth_7();
		NullCheck(L_6);
		float L_8 = AnimationCurve_Evaluate_m2125563588(L_6, L_7, /*hidden argument*/NULL);
		__this->set_valueHealthy_9(L_8);
		// valueHurt = hurt.Evaluate(enemyHealth);
		AnimationCurve_t3046754366 * L_9 = __this->get_hurt_3();
		float L_10 = __this->get_enemyHealth_7();
		NullCheck(L_9);
		float L_11 = AnimationCurve_Evaluate_m2125563588(L_9, L_10, /*hidden argument*/NULL);
		__this->set_valueHurt_10(L_11);
		// valueCritical = critical.Evaluate(enemyHealth);
		AnimationCurve_t3046754366 * L_12 = __this->get_critical_2();
		float L_13 = __this->get_enemyHealth_7();
		NullCheck(L_12);
		float L_14 = AnimationCurve_Evaluate_m2125563588(L_12, L_13, /*hidden argument*/NULL);
		__this->set_valueCritical_11(L_14);
		// SetLabels();
		EnemyFuzzyState_SetLabels_m2897062058(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyFuzzyState::SetLabels()
extern "C"  void EnemyFuzzyState_SetLabels_m2897062058 (EnemyFuzzyState_t3026123389 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyFuzzyState_SetLabels_m2897062058_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (valueHealthy > valueHurt && valueHealthy > valueCritical)
		float L_0 = __this->get_valueHealthy_9();
		float L_1 = __this->get_valueHurt_10();
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0033;
		}
	}
	{
		float L_2 = __this->get_valueHealthy_9();
		float L_3 = __this->get_valueCritical_11();
		if ((!(((float)L_2) > ((float)L_3))))
		{
			goto IL_0033;
		}
	}
	{
		// currentFuzzyState = FuzzyStates.HIGH;
		((EnemyFuzzyState_t3026123389_StaticFields*)il2cpp_codegen_static_fields_for(EnemyFuzzyState_t3026123389_il2cpp_TypeInfo_var))->set_currentFuzzyState_8(0);
		// fuzzyState.text = "Health: High";
		Text_t1901882714 * L_4 = __this->get_fuzzyState_6();
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, _stringLiteral323012186);
		// }
		return;
	}

IL_0033:
	{
		// else if (valueHurt > valueHealthy && valueHurt > valueCritical)
		float L_5 = __this->get_valueHurt_10();
		float L_6 = __this->get_valueHealthy_9();
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0066;
		}
	}
	{
		float L_7 = __this->get_valueHurt_10();
		float L_8 = __this->get_valueCritical_11();
		if ((!(((float)L_7) > ((float)L_8))))
		{
			goto IL_0066;
		}
	}
	{
		// currentFuzzyState = FuzzyStates.MEDIUM;
		((EnemyFuzzyState_t3026123389_StaticFields*)il2cpp_codegen_static_fields_for(EnemyFuzzyState_t3026123389_il2cpp_TypeInfo_var))->set_currentFuzzyState_8(1);
		// fuzzyState.text = "Health: Medium";
		Text_t1901882714 * L_9 = __this->get_fuzzyState_6();
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_9, _stringLiteral1441634946);
		// }
		return;
	}

IL_0066:
	{
		// else if (valueCritical > valueHealthy && valueCritical > valueHurt)
		float L_10 = __this->get_valueCritical_11();
		float L_11 = __this->get_valueHealthy_9();
		if ((!(((float)L_10) > ((float)L_11))))
		{
			goto IL_0098;
		}
	}
	{
		float L_12 = __this->get_valueCritical_11();
		float L_13 = __this->get_valueHurt_10();
		if ((!(((float)L_12) > ((float)L_13))))
		{
			goto IL_0098;
		}
	}
	{
		// currentFuzzyState = FuzzyStates.LOW;
		((EnemyFuzzyState_t3026123389_StaticFields*)il2cpp_codegen_static_fields_for(EnemyFuzzyState_t3026123389_il2cpp_TypeInfo_var))->set_currentFuzzyState_8(2);
		// fuzzyState.text = "Health: Low";
		Text_t1901882714 * L_14 = __this->get_fuzzyState_6();
		NullCheck(L_14);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_14, _stringLiteral1978273798);
	}

IL_0098:
	{
		// }
		return;
	}
}
// System.Void EnemyFuzzyState::.ctor()
extern "C"  void EnemyFuzzyState__ctor_m3801790542 (EnemyFuzzyState_t3026123389 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemyShoot::Start()
extern "C"  void EnemyShoot_Start_m1162270428 (EnemyShoot_t243830779 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void EnemyShoot::Update()
extern "C"  void EnemyShoot_Update_m2710617102 (EnemyShoot_t243830779 * __this, const RuntimeMethod* method)
{
	{
		// Shoot();
		EnemyShoot_Shoot_m569725038(__this, /*hidden argument*/NULL);
		// if (firedGun)
		bool L_0 = __this->get_firedGun_9();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		// cooldownTime -= Time.deltaTime;
		float L_1 = __this->get_cooldownTime_8();
		float L_2 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_cooldownTime_8(((float)il2cpp_codegen_subtract((float)L_1, (float)L_2)));
	}

IL_0020:
	{
		// if (cooldownTime <= 0)
		float L_3 = __this->get_cooldownTime_8();
		if ((!(((float)L_3) <= ((float)(0.0f)))))
		{
			goto IL_0040;
		}
	}
	{
		// firedGun = false;
		__this->set_firedGun_9((bool)0);
		// cooldownTime = cooldownTimeMax;
		float L_4 = __this->get_cooldownTimeMax_10();
		__this->set_cooldownTime_8(L_4);
	}

IL_0040:
	{
		// }
		return;
	}
}
// System.Void EnemyShoot::Shoot()
extern "C"  void EnemyShoot_Shoot_m569725038 (EnemyShoot_t243830779 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyShoot_Shoot_m569725038_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!firedGun)// && EnemyFuzzyState.currentFuzzyState == EnemyFuzzyState.FuzzyStates.LOW)
		bool L_0 = __this->get_firedGun_9();
		if (L_0)
		{
			goto IL_0070;
		}
	}
	{
		// GameObject bulletFire = Instantiate(bullet, spawn.transform.position, spawn.transform.rotation);
		GameObject_t1113636619 * L_1 = __this->get_bullet_4();
		Transform_t3600365921 * L_2 = __this->get_spawn_6();
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		Transform_t3600365921 * L_5 = __this->get_spawn_6();
		NullCheck(L_5);
		Transform_t3600365921 * L_6 = Component_get_transform_m3162698980(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Quaternion_t2301928331  L_7 = Transform_get_rotation_m3502953881(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_8 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_1, L_4, L_7, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		// bulletFire.GetComponent<Rigidbody>().velocity = transform.forward * bulletSpeed;
		NullCheck(L_8);
		Rigidbody_t3916780224 * L_9 = GameObject_GetComponent_TisRigidbody_t3916780224_m564316479(L_8, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t3916780224_m564316479_RuntimeMethod_var);
		Transform_t3600365921 * L_10 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t3722313464  L_11 = Transform_get_forward_m747522392(L_10, /*hidden argument*/NULL);
		float L_12 = __this->get_bulletSpeed_7();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_13 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		Rigidbody_set_velocity_m2899403247(L_9, L_13, /*hidden argument*/NULL);
		// GetComponent<AudioSource>().PlayOneShot(gunShot, 1.0f);
		AudioSource_t3935305588 * L_14 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_15 = __this->get_gunShot_2();
		NullCheck(L_14);
		AudioSource_PlayOneShot_m2678069419(L_14, L_15, (1.0f), /*hidden argument*/NULL);
		// firedGun = true;
		__this->set_firedGun_9((bool)1);
	}

IL_0070:
	{
		// }
		return;
	}
}
// System.Void EnemyShoot::ShootLaser()
extern "C"  void EnemyShoot_ShootLaser_m1234821334 (EnemyShoot_t243830779 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyShoot_ShootLaser_m1234821334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!firedGun)
		bool L_0 = __this->get_firedGun_9();
		if (L_0)
		{
			goto IL_006a;
		}
	}
	{
		// GameObject bulletFire = Instantiate(laser, spawn.transform.position, transform.rotation);
		GameObject_t1113636619 * L_1 = __this->get_laser_5();
		Transform_t3600365921 * L_2 = __this->get_spawn_6();
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Quaternion_t2301928331  L_6 = Transform_get_rotation_m3502953881(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_7 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_1, L_4, L_6, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		// bulletFire.GetComponent<Rigidbody>().velocity = transform.forward * 32.0f;
		NullCheck(L_7);
		Rigidbody_t3916780224 * L_8 = GameObject_GetComponent_TisRigidbody_t3916780224_m564316479(L_7, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t3916780224_m564316479_RuntimeMethod_var);
		Transform_t3600365921 * L_9 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t3722313464  L_10 = Transform_get_forward_m747522392(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_11 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_10, (32.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		Rigidbody_set_velocity_m2899403247(L_8, L_11, /*hidden argument*/NULL);
		// GetComponent<AudioSource>().PlayOneShot(laserShot, 1.0f);
		AudioSource_t3935305588 * L_12 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_13 = __this->get_laserShot_3();
		NullCheck(L_12);
		AudioSource_PlayOneShot_m2678069419(L_12, L_13, (1.0f), /*hidden argument*/NULL);
		// firedGun = true;
		__this->set_firedGun_9((bool)1);
	}

IL_006a:
	{
		// }
		return;
	}
}
// System.Void EnemyShoot::ShootContinuous()
extern "C"  void EnemyShoot_ShootContinuous_m784199287 (EnemyShoot_t243830779 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyShoot_ShootContinuous_m784199287_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit_t1056001966  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		// Ray ray = new Ray(spawn.position, spawn.forward);
		Transform_t3600365921 * L_0 = __this->get_spawn_6();
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_2 = __this->get_spawn_6();
		NullCheck(L_2);
		Vector3_t3722313464  L_3 = Transform_get_forward_m747522392(L_2, /*hidden argument*/NULL);
		Ray_t3785851493  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Ray__ctor_m168149494((&L_4), L_1, L_3, /*hidden argument*/NULL);
		// float shotDistance = 20.0f;
		V_1 = (20.0f);
		// if (Physics.Raycast(ray, out hit, shotDistance))
		float L_5 = V_1;
		bool L_6 = Physics_Raycast_m1743768310(NULL /*static, unused*/, L_4, (RaycastHit_t1056001966 *)(&V_0), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0033;
		}
	}
	{
		// shotDistance = hit.distance;
		float L_7 = RaycastHit_get_distance_m3727327466((RaycastHit_t1056001966 *)(&V_0), /*hidden argument*/NULL);
		V_1 = L_7;
	}

IL_0033:
	{
		// GameObject bulletFire = Instantiate(bullet, spawn.transform.position, spawn.transform.rotation);
		GameObject_t1113636619 * L_8 = __this->get_bullet_4();
		Transform_t3600365921 * L_9 = __this->get_spawn_6();
		NullCheck(L_9);
		Transform_t3600365921 * L_10 = Component_get_transform_m3162698980(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t3722313464  L_11 = Transform_get_position_m36019626(L_10, /*hidden argument*/NULL);
		Transform_t3600365921 * L_12 = __this->get_spawn_6();
		NullCheck(L_12);
		Transform_t3600365921 * L_13 = Component_get_transform_m3162698980(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Quaternion_t2301928331  L_14 = Transform_get_rotation_m3502953881(L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_15 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_8, L_11, L_14, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		// bulletFire.GetComponent<Rigidbody>().velocity = transform.forward * bulletSpeed;
		NullCheck(L_15);
		Rigidbody_t3916780224 * L_16 = GameObject_GetComponent_TisRigidbody_t3916780224_m564316479(L_15, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t3916780224_m564316479_RuntimeMethod_var);
		Transform_t3600365921 * L_17 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t3722313464  L_18 = Transform_get_forward_m747522392(L_17, /*hidden argument*/NULL);
		float L_19 = __this->get_bulletSpeed_7();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_20 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		Rigidbody_set_velocity_m2899403247(L_16, L_20, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void EnemyShoot::.ctor()
extern "C"  void EnemyShoot__ctor_m1274202978 (EnemyShoot_t243830779 * __this, const RuntimeMethod* method)
{
	{
		// public float bulletSpeed = 6;                   // The speed of the bullet
		__this->set_bulletSpeed_7((6.0f));
		// public float cooldownTimeMax = 0.5f;            // Maximum cooldown time
		__this->set_cooldownTimeMax_10((0.5f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void EnemyStats::Start()
extern "C"  void EnemyStats_Start_m165439304 (EnemyStats_t4187841152 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyStats_Start_m165439304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// eCurrentHealth = eMaximumHealth;
		int32_t L_0 = __this->get_eMaximumHealth_3();
		__this->set_eCurrentHealth_2(L_0);
		// eCount = FindObjectOfType<EnemyCount>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		EnemyCount_t3730988989 * L_1 = Object_FindObjectOfType_TisEnemyCount_t3730988989_m432317606(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisEnemyCount_t3730988989_m432317606_RuntimeMethod_var);
		__this->set_eCount_4(L_1);
		// }
		return;
	}
}
// System.Void EnemyStats::Update()
extern "C"  void EnemyStats_Update_m2668008344 (EnemyStats_t4187841152 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EnemyStats_Update_m2668008344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (eCurrentHealth >= eMaximumHealth)
		int32_t L_0 = __this->get_eCurrentHealth_2();
		int32_t L_1 = __this->get_eMaximumHealth_3();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_001a;
		}
	}
	{
		// eCurrentHealth = eMaximumHealth;
		int32_t L_2 = __this->get_eMaximumHealth_3();
		__this->set_eCurrentHealth_2(L_2);
	}

IL_001a:
	{
		// if (eCurrentHealth <= 0)
		int32_t L_3 = __this->get_eCurrentHealth_2();
		if ((((int32_t)L_3) > ((int32_t)0)))
		{
			goto IL_004d;
		}
	}
	{
		// gameObject.SetActive(false);
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)0, /*hidden argument*/NULL);
		// eCount.enemyNumber -= 1;
		EnemyCount_t3730988989 * L_5 = __this->get_eCount_4();
		EnemyCount_t3730988989 * L_6 = L_5;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_enemyNumber_3();
		NullCheck(L_6);
		L_6->set_enemyNumber_3(((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)1)));
		// Destroy(gameObject);
		GameObject_t1113636619 * L_8 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_004d:
	{
		// }
		return;
	}
}
// System.Void EnemyStats::.ctor()
extern "C"  void EnemyStats__ctor_m3164766448 (EnemyStats_t4187841152 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FuzzySample::Start()
extern "C"  void FuzzySample_Start_m1487148842 (FuzzySample_t2008138351 * __this, const RuntimeMethod* method)
{
	{
		// SetLabels();
		FuzzySample_SetLabels_m3256301443(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void FuzzySample::EvaluateStatements()
extern "C"  void FuzzySample_EvaluateStatements_m197160706 (FuzzySample_t2008138351 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// if (string.IsNullOrEmpty(healthInput.text))
		InputField_t3762917431 * L_0 = __this->get_healthInput_5();
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m3534748202(L_0, /*hidden argument*/NULL);
		bool L_2 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		// return;
		return;
	}

IL_0013:
	{
		// float valueInput = float.Parse(healthInput.text);
		InputField_t3762917431 * L_3 = __this->get_healthInput_5();
		NullCheck(L_3);
		String_t* L_4 = InputField_get_text_m3534748202(L_3, /*hidden argument*/NULL);
		float L_5 = Single_Parse_m364357836(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		// valueHealthy = healthy.Evaluate(valueInput);
		AnimationCurve_t3046754366 * L_6 = __this->get_healthy_4();
		float L_7 = V_0;
		NullCheck(L_6);
		float L_8 = AnimationCurve_Evaluate_m2125563588(L_6, L_7, /*hidden argument*/NULL);
		__this->set_valueHealthy_11(L_8);
		// valueHurt = hurt.Evaluate(valueInput);
		AnimationCurve_t3046754366 * L_9 = __this->get_hurt_3();
		float L_10 = V_0;
		NullCheck(L_9);
		float L_11 = AnimationCurve_Evaluate_m2125563588(L_9, L_10, /*hidden argument*/NULL);
		__this->set_valueHurt_12(L_11);
		// valueCritical = critical.Evaluate(valueInput);
		AnimationCurve_t3046754366 * L_12 = __this->get_critical_2();
		float L_13 = V_0;
		NullCheck(L_12);
		float L_14 = AnimationCurve_Evaluate_m2125563588(L_12, L_13, /*hidden argument*/NULL);
		__this->set_valueCritical_13(L_14);
		// SetLabels();
		FuzzySample_SetLabels_m3256301443(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void FuzzySample::SetLabels()
extern "C"  void FuzzySample_SetLabels_m3256301443 (FuzzySample_t2008138351 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FuzzySample_SetLabels_m3256301443_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// labelHealthy.text = string.Format(labelText, valueHealthy);
		Text_t1901882714 * L_0 = __this->get_labelHealthy_6();
		float L_1 = __this->get_valueHealthy_11();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_2);
		String_t* L_4 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral1142555903, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		// labelHurt.text = string.Format(labelText, valueHurt);
		Text_t1901882714 * L_5 = __this->get_labelHurt_7();
		float L_6 = __this->get_valueHurt_12();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral1142555903, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_9);
		// labelCritical.text = string.Format(labelText, valueCritical);
		Text_t1901882714 * L_10 = __this->get_labelCritical_8();
		float L_11 = __this->get_valueCritical_13();
		float L_12 = L_11;
		RuntimeObject * L_13 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_12);
		String_t* L_14 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral1142555903, L_13, /*hidden argument*/NULL);
		NullCheck(L_10);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_10, L_14);
		// if (valueHealthy > valueHurt && valueHealthy > valueCritical)
		float L_15 = __this->get_valueHealthy_11();
		float L_16 = __this->get_valueHurt_12();
		if ((!(((float)L_15) > ((float)L_16))))
		{
			goto IL_008d;
		}
	}
	{
		float L_17 = __this->get_valueHealthy_11();
		float L_18 = __this->get_valueCritical_13();
		if ((!(((float)L_17) > ((float)L_18))))
		{
			goto IL_008d;
		}
	}
	{
		// fuzzyState.text = "Healthy";
		Text_t1901882714 * L_19 = __this->get_fuzzyState_9();
		NullCheck(L_19);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_19, _stringLiteral136024746);
		// }
		return;
	}

IL_008d:
	{
		// else if (valueHurt > valueHealthy && valueHurt > valueCritical)
		float L_20 = __this->get_valueHurt_12();
		float L_21 = __this->get_valueHealthy_11();
		if ((!(((float)L_20) > ((float)L_21))))
		{
			goto IL_00ba;
		}
	}
	{
		float L_22 = __this->get_valueHurt_12();
		float L_23 = __this->get_valueCritical_13();
		if ((!(((float)L_22) > ((float)L_23))))
		{
			goto IL_00ba;
		}
	}
	{
		// fuzzyState.text = "Hurt";
		Text_t1901882714 * L_24 = __this->get_fuzzyState_9();
		NullCheck(L_24);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_24, _stringLiteral1212946936);
		// }
		return;
	}

IL_00ba:
	{
		// else if (valueCritical > valueHealthy && valueCritical > valueHurt)
		float L_25 = __this->get_valueCritical_13();
		float L_26 = __this->get_valueHealthy_11();
		if ((!(((float)L_25) > ((float)L_26))))
		{
			goto IL_00e6;
		}
	}
	{
		float L_27 = __this->get_valueCritical_13();
		float L_28 = __this->get_valueHurt_12();
		if ((!(((float)L_27) > ((float)L_28))))
		{
			goto IL_00e6;
		}
	}
	{
		// fuzzyState.text = "Critical";
		Text_t1901882714 * L_29 = __this->get_fuzzyState_9();
		NullCheck(L_29);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_29, _stringLiteral915598988);
	}

IL_00e6:
	{
		// }
		return;
	}
}
// System.Void FuzzySample::.ctor()
extern "C"  void FuzzySample__ctor_m1025906146 (FuzzySample_t2008138351 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameCamera::Start()
extern "C"  void GameCamera_Start_m2841233292 (GameCamera_t2564829307 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCamera_Start_m2841233292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// target = GameObject.FindGameObjectWithTag("Player").transform;
		GameObject_t1113636619 * L_0 = GameObject_FindGameObjectWithTag_m2129039296(NULL /*static, unused*/, _stringLiteral2261822918, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = GameObject_get_transform_m1369836730(L_0, /*hidden argument*/NULL);
		__this->set_target_6(L_1);
		// }
		return;
	}
}
// System.Void GameCamera::Update()
extern "C"  void GameCamera_Update_m3438868780 (GameCamera_t2564829307 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameCamera_Update_m3438868780_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// cameraTarget = new Vector3(target.position.x + xModifier, target.position.y + yModifier, target.position.z + zModifier);
		Transform_t3600365921 * L_0 = __this->get_target_6();
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		float L_2 = L_1.get_x_1();
		float L_3 = __this->get_xModifier_2();
		Transform_t3600365921 * L_4 = __this->get_target_6();
		NullCheck(L_4);
		Vector3_t3722313464  L_5 = Transform_get_position_m36019626(L_4, /*hidden argument*/NULL);
		float L_6 = L_5.get_y_2();
		float L_7 = __this->get_yModifier_3();
		Transform_t3600365921 * L_8 = __this->get_target_6();
		NullCheck(L_8);
		Vector3_t3722313464  L_9 = Transform_get_position_m36019626(L_8, /*hidden argument*/NULL);
		float L_10 = L_9.get_z_3();
		float L_11 = __this->get_zModifier_4();
		Vector3_t3722313464  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m3353183577((&L_12), ((float)il2cpp_codegen_add((float)L_2, (float)L_3)), ((float)il2cpp_codegen_add((float)L_6, (float)L_7)), ((float)il2cpp_codegen_add((float)L_10, (float)L_11)), /*hidden argument*/NULL);
		__this->set_cameraTarget_5(L_12);
		// transform.position = Vector3.Lerp(transform.position, cameraTarget, Time.deltaTime * 8);
		Transform_t3600365921 * L_13 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_14 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t3722313464  L_15 = Transform_get_position_m36019626(L_14, /*hidden argument*/NULL);
		Vector3_t3722313464  L_16 = __this->get_cameraTarget_5();
		float L_17 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_18 = Vector3_Lerp_m407887542(NULL /*static, unused*/, L_15, L_16, ((float)il2cpp_codegen_multiply((float)L_17, (float)(8.0f))), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_position_m3387557959(L_13, L_18, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameCamera::.ctor()
extern "C"  void GameCamera__ctor_m57809962 (GameCamera_t2564829307 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameOverScreen::Start()
extern "C"  void GameOverScreen_Start_m2140908633 (GameOverScreen_t2285777029 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameOverScreen_Start_m2140908633_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// levelChanger = FindObjectOfType<LevelChanger>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		LevelChanger_t225386971 * L_0 = Object_FindObjectOfType_TisLevelChanger_t225386971_m2918287313(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisLevelChanger_t225386971_m2918287313_RuntimeMethod_var);
		__this->set_levelChanger_7(L_0);
		// index = 0;
		__this->set_index_2(0);
		// DeselectAll();
		GameOverScreen_DeselectAll_m311764793(__this, /*hidden argument*/NULL);
		// options[index].GetComponent<Image>().sprite = optionBox[1];
		GameObjectU5BU5D_t3328599146* L_1 = __this->get_options_5();
		int32_t L_2 = __this->get_index_2();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		GameObject_t1113636619 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		Image_t2670269651 * L_5 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_4, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_6 = __this->get_optionBox_6();
		NullCheck(L_6);
		int32_t L_7 = 1;
		Sprite_t280657092 * L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_5);
		Image_set_sprite_m2369174689(L_5, L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameOverScreen::Update()
extern "C"  void GameOverScreen_Update_m3091737881 (GameOverScreen_t2285777029 * __this, const RuntimeMethod* method)
{
	{
		// UpdateOptions();
		GameOverScreen_UpdateOptions_m2082488164(__this, /*hidden argument*/NULL);
		// SelectOptions();
		GameOverScreen_SelectOptions_m3194635162(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void GameOverScreen::UpdateOptions()
extern "C"  void GameOverScreen_UpdateOptions_m2082488164 (GameOverScreen_t2285777029 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameOverScreen_UpdateOptions_m2082488164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (index >= options.Length)
		int32_t L_0 = __this->get_index_2();
		GameObjectU5BU5D_t3328599146* L_1 = __this->get_options_5();
		NullCheck(L_1);
		if ((((int32_t)L_0) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))))))
		{
			goto IL_0052;
		}
	}
	{
		// GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);
		AudioSource_t3935305588 * L_2 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_3 = __this->get_selectSound_3();
		NullCheck(L_2);
		AudioSource_PlayOneShot_m2678069419(L_2, L_3, (0.6f), /*hidden argument*/NULL);
		// index = 0;
		__this->set_index_2(0);
		// DeselectAll();
		GameOverScreen_DeselectAll_m311764793(__this, /*hidden argument*/NULL);
		// options[index].GetComponent<Image>().sprite = optionBox[1];
		GameObjectU5BU5D_t3328599146* L_4 = __this->get_options_5();
		int32_t L_5 = __this->get_index_2();
		NullCheck(L_4);
		int32_t L_6 = L_5;
		GameObject_t1113636619 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		Image_t2670269651 * L_8 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_7, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_9 = __this->get_optionBox_6();
		NullCheck(L_9);
		int32_t L_10 = 1;
		Sprite_t280657092 * L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_8);
		Image_set_sprite_m2369174689(L_8, L_11, /*hidden argument*/NULL);
	}

IL_0052:
	{
		// if (index < 0)
		int32_t L_12 = __this->get_index_2();
		if ((((int32_t)L_12) >= ((int32_t)0)))
		{
			goto IL_00a6;
		}
	}
	{
		// GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);
		AudioSource_t3935305588 * L_13 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_14 = __this->get_selectSound_3();
		NullCheck(L_13);
		AudioSource_PlayOneShot_m2678069419(L_13, L_14, (0.6f), /*hidden argument*/NULL);
		// index = options.Length - 1;
		GameObjectU5BU5D_t3328599146* L_15 = __this->get_options_5();
		NullCheck(L_15);
		__this->set_index_2(((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_15)->max_length)))), (int32_t)1)));
		// DeselectAll();
		GameOverScreen_DeselectAll_m311764793(__this, /*hidden argument*/NULL);
		// options[index].GetComponent<Image>().sprite = optionBox[1];
		GameObjectU5BU5D_t3328599146* L_16 = __this->get_options_5();
		int32_t L_17 = __this->get_index_2();
		NullCheck(L_16);
		int32_t L_18 = L_17;
		GameObject_t1113636619 * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_19);
		Image_t2670269651 * L_20 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_19, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_21 = __this->get_optionBox_6();
		NullCheck(L_21);
		int32_t L_22 = 1;
		Sprite_t280657092 * L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_20);
		Image_set_sprite_m2369174689(L_20, L_23, /*hidden argument*/NULL);
	}

IL_00a6:
	{
		// if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetAxisRaw("DPadVertical") > 0 || Input.GetAxisRaw("Vertical") > 0)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_24 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)274), /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_00d4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_25 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral767005334, /*hidden argument*/NULL);
		if ((((float)L_25) > ((float)(0.0f))))
		{
			goto IL_00d4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_26 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral2984908384, /*hidden argument*/NULL);
		if ((!(((float)L_26) > ((float)(0.0f)))))
		{
			goto IL_012e;
		}
	}

IL_00d4:
	{
		// if (!rightAxesInUse)
		bool L_27 = __this->get_rightAxesInUse_9();
		if (L_27)
		{
			goto IL_0135;
		}
	}
	{
		// rightAxesInUse = true;
		__this->set_rightAxesInUse_9((bool)1);
		// GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);
		AudioSource_t3935305588 * L_28 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_29 = __this->get_selectSound_3();
		NullCheck(L_28);
		AudioSource_PlayOneShot_m2678069419(L_28, L_29, (0.6f), /*hidden argument*/NULL);
		// index += 1;
		int32_t L_30 = __this->get_index_2();
		__this->set_index_2(((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1)));
		// DeselectAll();
		GameOverScreen_DeselectAll_m311764793(__this, /*hidden argument*/NULL);
		// options[index].GetComponent<Image>().sprite = optionBox[1];
		GameObjectU5BU5D_t3328599146* L_31 = __this->get_options_5();
		int32_t L_32 = __this->get_index_2();
		NullCheck(L_31);
		int32_t L_33 = L_32;
		GameObject_t1113636619 * L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NullCheck(L_34);
		Image_t2670269651 * L_35 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_34, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_36 = __this->get_optionBox_6();
		NullCheck(L_36);
		int32_t L_37 = 1;
		Sprite_t280657092 * L_38 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_35);
		Image_set_sprite_m2369174689(L_35, L_38, /*hidden argument*/NULL);
		// }
		goto IL_0135;
	}

IL_012e:
	{
		// rightAxesInUse = false;
		__this->set_rightAxesInUse_9((bool)0);
	}

IL_0135:
	{
		// if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetAxisRaw("DPadVertical") < 0 || Input.GetAxisRaw("Vertical") < 0)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_39 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)273), /*hidden argument*/NULL);
		if (L_39)
		{
			goto IL_0163;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_40 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral767005334, /*hidden argument*/NULL);
		if ((((float)L_40) < ((float)(0.0f))))
		{
			goto IL_0163;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_41 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral2984908384, /*hidden argument*/NULL);
		if ((!(((float)L_41) < ((float)(0.0f)))))
		{
			goto IL_01bc;
		}
	}

IL_0163:
	{
		// if (!leftAxesInUse)
		bool L_42 = __this->get_leftAxesInUse_8();
		if (L_42)
		{
			goto IL_01c3;
		}
	}
	{
		// leftAxesInUse = true;
		__this->set_leftAxesInUse_8((bool)1);
		// GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);
		AudioSource_t3935305588 * L_43 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_44 = __this->get_selectSound_3();
		NullCheck(L_43);
		AudioSource_PlayOneShot_m2678069419(L_43, L_44, (0.6f), /*hidden argument*/NULL);
		// index -= 1;
		int32_t L_45 = __this->get_index_2();
		__this->set_index_2(((int32_t)il2cpp_codegen_subtract((int32_t)L_45, (int32_t)1)));
		// DeselectAll();
		GameOverScreen_DeselectAll_m311764793(__this, /*hidden argument*/NULL);
		// options[index].GetComponent<Image>().sprite = optionBox[1];
		GameObjectU5BU5D_t3328599146* L_46 = __this->get_options_5();
		int32_t L_47 = __this->get_index_2();
		NullCheck(L_46);
		int32_t L_48 = L_47;
		GameObject_t1113636619 * L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		NullCheck(L_49);
		Image_t2670269651 * L_50 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_49, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_51 = __this->get_optionBox_6();
		NullCheck(L_51);
		int32_t L_52 = 1;
		Sprite_t280657092 * L_53 = (L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		NullCheck(L_50);
		Image_set_sprite_m2369174689(L_50, L_53, /*hidden argument*/NULL);
		// }
		return;
	}

IL_01bc:
	{
		// leftAxesInUse = false;
		__this->set_leftAxesInUse_8((bool)0);
	}

IL_01c3:
	{
		// }
		return;
	}
}
// System.Void GameOverScreen::SelectOptions()
extern "C"  void GameOverScreen_SelectOptions_m3194635162 (GameOverScreen_t2285777029 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameOverScreen_SelectOptions_m3194635162_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (Input.GetKeyDown(KeyCode.Space) || Input.GetAxisRaw("Submit") > 0)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)32), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_1 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral1187062204, /*hidden argument*/NULL);
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_00f4;
		}
	}

IL_001d:
	{
		// switch (index)
		int32_t L_2 = __this->get_index_2();
		V_0 = L_2;
		int32_t L_3 = V_0;
		switch (L_3)
		{
			case 0:
			{
				goto IL_0037;
			}
			case 1:
			{
				goto IL_0084;
			}
			case 2:
			{
				goto IL_00c4;
			}
		}
	}
	{
		return;
	}

IL_0037:
	{
		// if (!confirmAxesInUse)
		bool L_4 = __this->get_confirmAxesInUse_10();
		if (L_4)
		{
			goto IL_00fb;
		}
	}
	{
		// confirmAxesInUse = true;
		__this->set_confirmAxesInUse_10((bool)1);
		// GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);
		AudioSource_t3935305588 * L_5 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_6 = __this->get_confirmSound_4();
		NullCheck(L_5);
		AudioSource_PlayOneShot_m2678069419(L_5, L_6, (0.6f), /*hidden argument*/NULL);
		// levelChanger.FadeToLevel("LevelSelection");
		LevelChanger_t225386971 * L_7 = __this->get_levelChanger_7();
		NullCheck(L_7);
		LevelChanger_FadeToLevel_m2444280689(L_7, _stringLiteral2662547089, /*hidden argument*/NULL);
		// Debug.Log("To Level Selection");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1842748674, /*hidden argument*/NULL);
		// Debug.Log("Continue");
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1055811909, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_0084:
	{
		// if (!confirmAxesInUse)
		bool L_8 = __this->get_confirmAxesInUse_10();
		if (L_8)
		{
			goto IL_00fb;
		}
	}
	{
		// confirmAxesInUse = true;
		__this->set_confirmAxesInUse_10((bool)1);
		// GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);
		AudioSource_t3935305588 * L_9 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_10 = __this->get_confirmSound_4();
		NullCheck(L_9);
		AudioSource_PlayOneShot_m2678069419(L_9, L_10, (0.6f), /*hidden argument*/NULL);
		// levelChanger.FadeToLevel("Title");
		LevelChanger_t225386971 * L_11 = __this->get_levelChanger_7();
		NullCheck(L_11);
		LevelChanger_FadeToLevel_m2444280689(L_11, _stringLiteral3963994475, /*hidden argument*/NULL);
		// Debug.Log("To title");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1538482293, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_00c4:
	{
		// if (!confirmAxesInUse)
		bool L_12 = __this->get_confirmAxesInUse_10();
		if (L_12)
		{
			goto IL_00fb;
		}
	}
	{
		// confirmAxesInUse = true;
		__this->set_confirmAxesInUse_10((bool)1);
		// GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);
		AudioSource_t3935305588 * L_13 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_14 = __this->get_confirmSound_4();
		NullCheck(L_13);
		AudioSource_PlayOneShot_m2678069419(L_13, L_14, (0.6f), /*hidden argument*/NULL);
		// Debug.Log("Settings");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3588955337, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_00f4:
	{
		// confirmAxesInUse = false;
		__this->set_confirmAxesInUse_10((bool)0);
	}

IL_00fb:
	{
		// }
		return;
	}
}
// System.Void GameOverScreen::DeselectAll()
extern "C"  void GameOverScreen_DeselectAll_m311764793 (GameOverScreen_t2285777029 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameOverScreen_DeselectAll_m311764793_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < options.Length; i++)
		V_0 = 0;
		goto IL_0022;
	}

IL_0004:
	{
		// options[i].GetComponent<Image>().sprite = optionBox[0];
		GameObjectU5BU5D_t3328599146* L_0 = __this->get_options_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_t1113636619 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Image_t2670269651 * L_4 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_3, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_5 = __this->get_optionBox_6();
		NullCheck(L_5);
		int32_t L_6 = 0;
		Sprite_t280657092 * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_4);
		Image_set_sprite_m2369174689(L_4, L_7, /*hidden argument*/NULL);
		// for (int i = 0; i < options.Length; i++)
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0022:
	{
		// for (int i = 0; i < options.Length; i++)
		int32_t L_9 = V_0;
		GameObjectU5BU5D_t3328599146* L_10 = __this->get_options_5();
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void GameOverScreen::.ctor()
extern "C"  void GameOverScreen__ctor_m3128506725 (GameOverScreen_t2285777029 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 Grid::get_MaxSize()
extern "C"  int32_t Grid_get_MaxSize_m3127825365 (Grid_t1081586032 * __this, const RuntimeMethod* method)
{
	{
		// get { return gridSizeX * gridSizeY; }
		int32_t L_0 = __this->get_gridSizeX_12();
		int32_t L_1 = __this->get_gridSizeY_13();
		return ((int32_t)il2cpp_codegen_multiply((int32_t)L_0, (int32_t)L_1));
	}
}
// System.Void Grid::Awake()
extern "C"  void Grid_Awake_m2133102238 (Grid_t1081586032 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Grid_Awake_m2133102238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TerrainTypeU5BU5D_t1748095520* V_0 = NULL;
	int32_t V_1 = 0;
	TerrainType_t3627529997 * V_2 = NULL;
	{
		// nodeDiameter = nodeRadius * 2;
		float L_0 = __this->get_nodeRadius_5();
		__this->set_nodeDiameter_11(((float)il2cpp_codegen_multiply((float)L_0, (float)(2.0f))));
		// gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
		Vector2_t2156229523 * L_1 = __this->get_address_of_gridWorldSize_4();
		float L_2 = L_1->get_x_0();
		float L_3 = __this->get_nodeDiameter_11();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)((float)L_2/(float)L_3)), /*hidden argument*/NULL);
		__this->set_gridSizeX_12(L_4);
		// gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
		Vector2_t2156229523 * L_5 = __this->get_address_of_gridWorldSize_4();
		float L_6 = L_5->get_y_1();
		float L_7 = __this->get_nodeDiameter_11();
		int32_t L_8 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)((float)L_6/(float)L_7)), /*hidden argument*/NULL);
		__this->set_gridSizeY_13(L_8);
		// foreach (TerrainType region in walkableRegions)
		TerrainTypeU5BU5D_t1748095520* L_9 = __this->get_walkableRegions_6();
		V_0 = L_9;
		V_1 = 0;
		goto IL_00a4;
	}

IL_0057:
	{
		// foreach (TerrainType region in walkableRegions)
		TerrainTypeU5BU5D_t1748095520* L_10 = V_0;
		int32_t L_11 = V_1;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		TerrainType_t3627529997 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_2 = L_13;
		// walkableMask.value |= region.terrainMask.value;
		LayerMask_t3493934918 * L_14 = __this->get_address_of_walkableMask_9();
		LayerMask_t3493934918 * L_15 = L_14;
		int32_t L_16 = LayerMask_get_value_m1881709263((LayerMask_t3493934918 *)L_15, /*hidden argument*/NULL);
		TerrainType_t3627529997 * L_17 = V_2;
		NullCheck(L_17);
		LayerMask_t3493934918 * L_18 = L_17->get_address_of_terrainMask_0();
		int32_t L_19 = LayerMask_get_value_m1881709263((LayerMask_t3493934918 *)L_18, /*hidden argument*/NULL);
		LayerMask_set_value_m1631067668((LayerMask_t3493934918 *)L_15, ((int32_t)((int32_t)L_16|(int32_t)L_19)), /*hidden argument*/NULL);
		// walkableRegionsDict.Add((int)Mathf.Log(region.terrainMask.value, 2), region.terrainPenalty);
		Dictionary_2_t1839659084 * L_20 = __this->get_walkableRegionsDict_8();
		TerrainType_t3627529997 * L_21 = V_2;
		NullCheck(L_21);
		LayerMask_t3493934918 * L_22 = L_21->get_address_of_terrainMask_0();
		int32_t L_23 = LayerMask_get_value_m1881709263((LayerMask_t3493934918 *)L_22, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_24 = Mathf_Log_m2177375338(NULL /*static, unused*/, (((float)((float)L_23))), (2.0f), /*hidden argument*/NULL);
		TerrainType_t3627529997 * L_25 = V_2;
		NullCheck(L_25);
		int32_t L_26 = L_25->get_terrainPenalty_1();
		NullCheck(L_20);
		Dictionary_2_Add_m1535364901(L_20, (((int32_t)((int32_t)L_24))), L_26, /*hidden argument*/Dictionary_2_Add_m1535364901_RuntimeMethod_var);
		int32_t L_27 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)1));
	}

IL_00a4:
	{
		// foreach (TerrainType region in walkableRegions)
		int32_t L_28 = V_1;
		TerrainTypeU5BU5D_t1748095520* L_29 = V_0;
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_29)->max_length)))))))
		{
			goto IL_0057;
		}
	}
	{
		// CreateGrid();
		Grid_CreateGrid_m256214910(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Grid::CreateGrid()
extern "C"  void Grid_CreateGrid_m256214910 (Grid_t1081586032 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Grid_CreateGrid_m256214910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	bool V_4 = false;
	int32_t V_5 = 0;
	RaycastHit_t1056001966  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		// grid = new Node[gridSizeX, gridSizeY];
		int32_t L_0 = __this->get_gridSizeX_12();
		int32_t L_1 = __this->get_gridSizeY_13();
		il2cpp_array_size_t L_3[] = { (il2cpp_array_size_t)L_0, (il2cpp_array_size_t)L_1 };
		NodeU5B0___U2C0___U5D_t1457036568* L_2 = (NodeU5B0___U2C0___U5D_t1457036568*)GenArrayNew(NodeU5B0___U2C0___U5D_t1457036568_il2cpp_TypeInfo_var, L_3);
		__this->set_grid_10((NodeU5B0___U2C0___U5D_t1457036568*)L_2);
		// Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3722313464  L_5 = Transform_get_position_m36019626(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_6 = Vector3_get_right_m1913784872(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2156229523 * L_7 = __this->get_address_of_gridWorldSize_4();
		float L_8 = L_7->get_x_0();
		Vector3_t3722313464  L_9 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		Vector3_t3722313464  L_10 = Vector3_op_Division_m510815599(NULL /*static, unused*/, L_9, (2.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_5, L_10, /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2156229523 * L_13 = __this->get_address_of_gridWorldSize_4();
		float L_14 = L_13->get_y_1();
		Vector3_t3722313464  L_15 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		Vector3_t3722313464  L_16 = Vector3_op_Division_m510815599(NULL /*static, unused*/, L_15, (2.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_17 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_11, L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		// for (int x = 0; x < gridSizeX; x++)
		V_1 = 0;
		goto IL_016b;
	}

IL_0072:
	{
		// for (int y = 0; y < gridSizeY; y++)
		V_2 = 0;
		goto IL_015b;
	}

IL_0079:
	{
		// Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
		Vector3_t3722313464  L_18 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_19 = Vector3_get_right_m1913784872(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_20 = V_1;
		float L_21 = __this->get_nodeDiameter_11();
		float L_22 = __this->get_nodeRadius_5();
		Vector3_t3722313464  L_23 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_19, ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_20))), (float)L_21)), (float)L_22)), /*hidden argument*/NULL);
		Vector3_t3722313464  L_24 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_18, L_23, /*hidden argument*/NULL);
		Vector3_t3722313464  L_25 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_26 = V_2;
		float L_27 = __this->get_nodeDiameter_11();
		float L_28 = __this->get_nodeRadius_5();
		Vector3_t3722313464  L_29 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_25, ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)(((float)((float)L_26))), (float)L_27)), (float)L_28)), /*hidden argument*/NULL);
		Vector3_t3722313464  L_30 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_24, L_29, /*hidden argument*/NULL);
		V_3 = L_30;
		// bool walkable = !(Physics.CheckSphere(worldPoint, nodeRadius, unwalkableMask));
		Vector3_t3722313464  L_31 = V_3;
		float L_32 = __this->get_nodeRadius_5();
		LayerMask_t3493934918  L_33 = __this->get_unwalkableMask_3();
		int32_t L_34 = LayerMask_op_Implicit_m3296792737(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		bool L_35 = Physics_CheckSphere_m3218818399(NULL /*static, unused*/, L_31, L_32, L_34, /*hidden argument*/NULL);
		V_4 = (bool)((((int32_t)L_35) == ((int32_t)0))? 1 : 0);
		// int movementPenalty = 0;
		V_5 = 0;
		// Ray ray = new Ray(worldPoint + Vector3.up * 50, Vector3.down);
		Vector3_t3722313464  L_36 = V_3;
		Vector3_t3722313464  L_37 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_38 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_37, (50.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_39 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_36, L_38, /*hidden argument*/NULL);
		Vector3_t3722313464  L_40 = Vector3_get_down_m3781355428(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t3785851493  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Ray__ctor_m168149494((&L_41), L_39, L_40, /*hidden argument*/NULL);
		// if (Physics.Raycast(ray, out hit, 100, walkableMask))
		LayerMask_t3493934918  L_42 = __this->get_walkableMask_9();
		int32_t L_43 = LayerMask_op_Implicit_m3296792737(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		bool L_44 = Physics_Raycast_m1893809531(NULL /*static, unused*/, L_41, (RaycastHit_t1056001966 *)(&V_6), (100.0f), L_43, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_012f;
		}
	}
	{
		// walkableRegionsDict.TryGetValue(hit.collider.gameObject.layer, out movementPenalty);
		Dictionary_2_t1839659084 * L_45 = __this->get_walkableRegionsDict_8();
		Collider_t1773347010 * L_46 = RaycastHit_get_collider_m1464180279((RaycastHit_t1056001966 *)(&V_6), /*hidden argument*/NULL);
		NullCheck(L_46);
		GameObject_t1113636619 * L_47 = Component_get_gameObject_m442555142(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		int32_t L_48 = GameObject_get_layer_m4158800245(L_47, /*hidden argument*/NULL);
		NullCheck(L_45);
		Dictionary_2_TryGetValue_m1682688660(L_45, L_48, (int32_t*)(&V_5), /*hidden argument*/Dictionary_2_TryGetValue_m1682688660_RuntimeMethod_var);
	}

IL_012f:
	{
		// if (!walkable)
		bool L_49 = V_4;
		if (L_49)
		{
			goto IL_013e;
		}
	}
	{
		// movementPenalty += obstacleProximityPenalty;
		int32_t L_50 = V_5;
		int32_t L_51 = __this->get_obstacleProximityPenalty_7();
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_50, (int32_t)L_51));
	}

IL_013e:
	{
		// grid[x, y] = new Node(walkable, worldPoint, x, y, movementPenalty);
		NodeU5B0___U2C0___U5D_t1457036568* L_52 = __this->get_grid_10();
		int32_t L_53 = V_1;
		int32_t L_54 = V_2;
		bool L_55 = V_4;
		Vector3_t3722313464  L_56 = V_3;
		int32_t L_57 = V_1;
		int32_t L_58 = V_2;
		int32_t L_59 = V_5;
		Node_t2989995042 * L_60 = (Node_t2989995042 *)il2cpp_codegen_object_new(Node_t2989995042_il2cpp_TypeInfo_var);
		Node__ctor_m304489418(L_60, L_55, L_56, L_57, L_58, L_59, /*hidden argument*/NULL);
		NullCheck((NodeU5B0___U2C0___U5D_t1457036568*)(NodeU5B0___U2C0___U5D_t1457036568*)L_52);
		((NodeU5B0___U2C0___U5D_t1457036568*)(NodeU5B0___U2C0___U5D_t1457036568*)L_52)->SetAt(L_53, L_54, L_60);
		// for (int y = 0; y < gridSizeY; y++)
		int32_t L_61 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_61, (int32_t)1));
	}

IL_015b:
	{
		// for (int y = 0; y < gridSizeY; y++)
		int32_t L_62 = V_2;
		int32_t L_63 = __this->get_gridSizeY_13();
		if ((((int32_t)L_62) < ((int32_t)L_63)))
		{
			goto IL_0079;
		}
	}
	{
		// for (int x = 0; x < gridSizeX; x++)
		int32_t L_64 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_64, (int32_t)1));
	}

IL_016b:
	{
		// for (int x = 0; x < gridSizeX; x++)
		int32_t L_65 = V_1;
		int32_t L_66 = __this->get_gridSizeX_12();
		if ((((int32_t)L_65) < ((int32_t)L_66)))
		{
			goto IL_0072;
		}
	}
	{
		// BlurPenaltyMap(3);
		Grid_BlurPenaltyMap_m2132322175(__this, 3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Grid::BlurPenaltyMap(System.Int32)
extern "C"  void Grid_BlurPenaltyMap_m2132322175 (Grid_t1081586032 * __this, int32_t ___blurSize0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Grid_BlurPenaltyMap_m2132322175_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Int32U5B0___U2C0___U5D_t385246373* V_2 = NULL;
	Int32U5B0___U2C0___U5D_t385246373* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	{
		// int kernelSize = blurSize * 2 + 1;          // (blurSize * 2) + 1
		int32_t L_0 = ___blurSize0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_0, (int32_t)2)), (int32_t)1));
		// int kernelExtents = (kernelSize - 1) / 2;   // (kernalSize - 1) / 2
		int32_t L_1 = V_0;
		V_1 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1))/(int32_t)2));
		// int[,] penaltiesHorizontalPass = new int[gridSizeX, gridSizeY];
		int32_t L_2 = __this->get_gridSizeX_12();
		int32_t L_3 = __this->get_gridSizeY_13();
		il2cpp_array_size_t L_5[] = { (il2cpp_array_size_t)L_2, (il2cpp_array_size_t)L_3 };
		Int32U5B0___U2C0___U5D_t385246373* L_4 = (Int32U5B0___U2C0___U5D_t385246373*)GenArrayNew(Int32U5B0___U2C0___U5D_t385246373_il2cpp_TypeInfo_var, L_5);
		V_2 = L_4;
		// int[,] penaltiesVerticalPass = new int[gridSizeX, gridSizeY];
		int32_t L_6 = __this->get_gridSizeX_12();
		int32_t L_7 = __this->get_gridSizeY_13();
		il2cpp_array_size_t L_9[] = { (il2cpp_array_size_t)L_6, (il2cpp_array_size_t)L_7 };
		Int32U5B0___U2C0___U5D_t385246373* L_8 = (Int32U5B0___U2C0___U5D_t385246373*)GenArrayNew(Int32U5B0___U2C0___U5D_t385246373_il2cpp_TypeInfo_var, L_9);
		V_3 = L_8;
		// for (int y = 0; y < gridSizeY; y++)
		V_4 = 0;
		goto IL_00f8;
	}

IL_0038:
	{
		// for (int x = -kernelExtents; x < kernelExtents; x++)
		int32_t L_10 = V_1;
		V_5 = ((-L_10));
		goto IL_0070;
	}

IL_003e:
	{
		// int sampleX = Mathf.Clamp(x, 0, kernelExtents);
		int32_t L_11 = V_5;
		int32_t L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_13 = Mathf_Clamp_m2756574208(NULL /*static, unused*/, L_11, 0, L_12, /*hidden argument*/NULL);
		V_6 = L_13;
		// penaltiesHorizontalPass[0, y] += grid[sampleX, y].movementPenalty;
		Int32U5B0___U2C0___U5D_t385246373* L_14 = V_2;
		int32_t L_15 = V_4;
		NullCheck(L_14);
		int32_t* L_16 = (L_14)->GetAddressAt(0, L_15);
		int32_t* L_17 = L_16;
		NodeU5B0___U2C0___U5D_t1457036568* L_18 = __this->get_grid_10();
		int32_t L_19 = V_6;
		int32_t L_20 = V_4;
		NullCheck((NodeU5B0___U2C0___U5D_t1457036568*)(NodeU5B0___U2C0___U5D_t1457036568*)L_18);
		Node_t2989995042 * L_21 = ((NodeU5B0___U2C0___U5D_t1457036568*)(NodeU5B0___U2C0___U5D_t1457036568*)L_18)->GetAt(L_19, L_20);
		NullCheck(L_21);
		int32_t L_22 = L_21->get_movementPenalty_4();
		*((int32_t*)(L_17)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_17)), (int32_t)L_22));
		// for (int x = -kernelExtents; x < kernelExtents; x++)
		int32_t L_23 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_0070:
	{
		// for (int x = -kernelExtents; x < kernelExtents; x++)
		int32_t L_24 = V_5;
		int32_t L_25 = V_1;
		if ((((int32_t)L_24) < ((int32_t)L_25)))
		{
			goto IL_003e;
		}
	}
	{
		// for (int x = 1; x < gridSizeX; x++)
		V_7 = 1;
		goto IL_00e8;
	}

IL_007a:
	{
		// int removeIndex = Mathf.Clamp(x - kernelExtents - 1, 0, gridSizeX);
		int32_t L_26 = V_7;
		int32_t L_27 = V_1;
		int32_t L_28 = __this->get_gridSizeX_12();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_29 = Mathf_Clamp_m2756574208(NULL /*static, unused*/, ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_26, (int32_t)L_27)), (int32_t)1)), 0, L_28, /*hidden argument*/NULL);
		V_8 = L_29;
		// int addIndex = Mathf.Clamp(x + kernelExtents, 0, gridSizeX - 1);
		int32_t L_30 = V_7;
		int32_t L_31 = V_1;
		int32_t L_32 = __this->get_gridSizeX_12();
		int32_t L_33 = Mathf_Clamp_m2756574208(NULL /*static, unused*/, ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)L_31)), 0, ((int32_t)il2cpp_codegen_subtract((int32_t)L_32, (int32_t)1)), /*hidden argument*/NULL);
		V_9 = L_33;
		// penaltiesHorizontalPass[x, y] = penaltiesHorizontalPass[x - 1, y] - grid[removeIndex, y].movementPenalty
		//                                                                   + grid[addIndex, y].movementPenalty;
		Int32U5B0___U2C0___U5D_t385246373* L_34 = V_2;
		int32_t L_35 = V_7;
		int32_t L_36 = V_4;
		Int32U5B0___U2C0___U5D_t385246373* L_37 = V_2;
		int32_t L_38 = V_7;
		int32_t L_39 = V_4;
		NullCheck(L_37);
		int32_t L_40 = (L_37)->GetAt(((int32_t)il2cpp_codegen_subtract((int32_t)L_38, (int32_t)1)), L_39);
		NodeU5B0___U2C0___U5D_t1457036568* L_41 = __this->get_grid_10();
		int32_t L_42 = V_8;
		int32_t L_43 = V_4;
		NullCheck((NodeU5B0___U2C0___U5D_t1457036568*)(NodeU5B0___U2C0___U5D_t1457036568*)L_41);
		Node_t2989995042 * L_44 = ((NodeU5B0___U2C0___U5D_t1457036568*)(NodeU5B0___U2C0___U5D_t1457036568*)L_41)->GetAt(L_42, L_43);
		NullCheck(L_44);
		int32_t L_45 = L_44->get_movementPenalty_4();
		NodeU5B0___U2C0___U5D_t1457036568* L_46 = __this->get_grid_10();
		int32_t L_47 = V_9;
		int32_t L_48 = V_4;
		NullCheck((NodeU5B0___U2C0___U5D_t1457036568*)(NodeU5B0___U2C0___U5D_t1457036568*)L_46);
		Node_t2989995042 * L_49 = ((NodeU5B0___U2C0___U5D_t1457036568*)(NodeU5B0___U2C0___U5D_t1457036568*)L_46)->GetAt(L_47, L_48);
		NullCheck(L_49);
		int32_t L_50 = L_49->get_movementPenalty_4();
		NullCheck(L_34);
		(L_34)->SetAt(L_35, L_36, ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_40, (int32_t)L_45)), (int32_t)L_50)));
		// for (int x = 1; x < gridSizeX; x++)
		int32_t L_51 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_51, (int32_t)1));
	}

IL_00e8:
	{
		// for (int x = 1; x < gridSizeX; x++)
		int32_t L_52 = V_7;
		int32_t L_53 = __this->get_gridSizeX_12();
		if ((((int32_t)L_52) < ((int32_t)L_53)))
		{
			goto IL_007a;
		}
	}
	{
		// for (int y = 0; y < gridSizeY; y++)
		int32_t L_54 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_54, (int32_t)1));
	}

IL_00f8:
	{
		// for (int y = 0; y < gridSizeY; y++)
		int32_t L_55 = V_4;
		int32_t L_56 = __this->get_gridSizeY_13();
		if ((((int32_t)L_55) < ((int32_t)L_56)))
		{
			goto IL_0038;
		}
	}
	{
		// for (int x = 0; x < gridSizeX; x++)
		V_10 = 0;
		goto IL_0231;
	}

IL_010d:
	{
		// for (int y = -kernelExtents; y < kernelExtents; y++)
		int32_t L_57 = V_1;
		V_12 = ((-L_57));
		goto IL_013b;
	}

IL_0113:
	{
		// int sampleY = Mathf.Clamp(y, 0, kernelExtents);
		int32_t L_58 = V_12;
		int32_t L_59 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_60 = Mathf_Clamp_m2756574208(NULL /*static, unused*/, L_58, 0, L_59, /*hidden argument*/NULL);
		V_13 = L_60;
		// penaltiesVerticalPass[x, 0] += penaltiesHorizontalPass[x, sampleY];
		Int32U5B0___U2C0___U5D_t385246373* L_61 = V_3;
		int32_t L_62 = V_10;
		NullCheck(L_61);
		int32_t* L_63 = (L_61)->GetAddressAt(L_62, 0);
		int32_t* L_64 = L_63;
		Int32U5B0___U2C0___U5D_t385246373* L_65 = V_2;
		int32_t L_66 = V_10;
		int32_t L_67 = V_13;
		NullCheck(L_65);
		int32_t L_68 = (L_65)->GetAt(L_66, L_67);
		*((int32_t*)(L_64)) = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)(*((int32_t*)L_64)), (int32_t)L_68));
		// for (int y = -kernelExtents; y < kernelExtents; y++)
		int32_t L_69 = V_12;
		V_12 = ((int32_t)il2cpp_codegen_add((int32_t)L_69, (int32_t)1));
	}

IL_013b:
	{
		// for (int y = -kernelExtents; y < kernelExtents; y++)
		int32_t L_70 = V_12;
		int32_t L_71 = V_1;
		if ((((int32_t)L_70) < ((int32_t)L_71)))
		{
			goto IL_0113;
		}
	}
	{
		// int blurredPenalty = Mathf.RoundToInt((float)penaltiesVerticalPass[x, 0] / (kernelSize * kernelSize));
		Int32U5B0___U2C0___U5D_t385246373* L_72 = V_3;
		int32_t L_73 = V_10;
		NullCheck(L_72);
		int32_t L_74 = (L_72)->GetAt(L_73, 0);
		int32_t L_75 = V_0;
		int32_t L_76 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_77 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)((float)(((float)((float)L_74)))/(float)(((float)((float)((int32_t)il2cpp_codegen_multiply((int32_t)L_75, (int32_t)L_76))))))), /*hidden argument*/NULL);
		V_11 = L_77;
		// grid[x, 0].movementPenalty = blurredPenalty;
		NodeU5B0___U2C0___U5D_t1457036568* L_78 = __this->get_grid_10();
		int32_t L_79 = V_10;
		NullCheck((NodeU5B0___U2C0___U5D_t1457036568*)(NodeU5B0___U2C0___U5D_t1457036568*)L_78);
		Node_t2989995042 * L_80 = ((NodeU5B0___U2C0___U5D_t1457036568*)(NodeU5B0___U2C0___U5D_t1457036568*)L_78)->GetAt(L_79, 0);
		int32_t L_81 = V_11;
		NullCheck(L_80);
		L_80->set_movementPenalty_4(L_81);
		// for (int y = 1; y < gridSizeY; y++)
		V_14 = 1;
		goto IL_021e;
	}

IL_0173:
	{
		// int removeIndex = Mathf.Clamp(y - kernelExtents - 1, 0, gridSizeY);
		int32_t L_82 = V_14;
		int32_t L_83 = V_1;
		int32_t L_84 = __this->get_gridSizeY_13();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_85 = Mathf_Clamp_m2756574208(NULL /*static, unused*/, ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_82, (int32_t)L_83)), (int32_t)1)), 0, L_84, /*hidden argument*/NULL);
		V_15 = L_85;
		// int addIndex = Mathf.Clamp(y + kernelExtents, 0, gridSizeY - 1);
		int32_t L_86 = V_14;
		int32_t L_87 = V_1;
		int32_t L_88 = __this->get_gridSizeY_13();
		int32_t L_89 = Mathf_Clamp_m2756574208(NULL /*static, unused*/, ((int32_t)il2cpp_codegen_add((int32_t)L_86, (int32_t)L_87)), 0, ((int32_t)il2cpp_codegen_subtract((int32_t)L_88, (int32_t)1)), /*hidden argument*/NULL);
		V_16 = L_89;
		// penaltiesVerticalPass[x, y] = penaltiesVerticalPass[x, y - 1] - penaltiesHorizontalPass[x, removeIndex]
		//                                                                   + penaltiesHorizontalPass[x, addIndex];
		Int32U5B0___U2C0___U5D_t385246373* L_90 = V_3;
		int32_t L_91 = V_10;
		int32_t L_92 = V_14;
		Int32U5B0___U2C0___U5D_t385246373* L_93 = V_3;
		int32_t L_94 = V_10;
		int32_t L_95 = V_14;
		NullCheck(L_93);
		int32_t L_96 = (L_93)->GetAt(L_94, ((int32_t)il2cpp_codegen_subtract((int32_t)L_95, (int32_t)1)));
		Int32U5B0___U2C0___U5D_t385246373* L_97 = V_2;
		int32_t L_98 = V_10;
		int32_t L_99 = V_15;
		NullCheck(L_97);
		int32_t L_100 = (L_97)->GetAt(L_98, L_99);
		Int32U5B0___U2C0___U5D_t385246373* L_101 = V_2;
		int32_t L_102 = V_10;
		int32_t L_103 = V_16;
		NullCheck(L_101);
		int32_t L_104 = (L_101)->GetAt(L_102, L_103);
		NullCheck(L_90);
		(L_90)->SetAt(L_91, L_92, ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_96, (int32_t)L_100)), (int32_t)L_104)));
		// blurredPenalty = Mathf.RoundToInt((float)penaltiesVerticalPass[x, y] / (kernelSize * kernelSize));
		Int32U5B0___U2C0___U5D_t385246373* L_105 = V_3;
		int32_t L_106 = V_10;
		int32_t L_107 = V_14;
		NullCheck(L_105);
		int32_t L_108 = (L_105)->GetAt(L_106, L_107);
		int32_t L_109 = V_0;
		int32_t L_110 = V_0;
		int32_t L_111 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)((float)(((float)((float)L_108)))/(float)(((float)((float)((int32_t)il2cpp_codegen_multiply((int32_t)L_109, (int32_t)L_110))))))), /*hidden argument*/NULL);
		V_11 = L_111;
		// grid[x, y].movementPenalty = blurredPenalty;
		NodeU5B0___U2C0___U5D_t1457036568* L_112 = __this->get_grid_10();
		int32_t L_113 = V_10;
		int32_t L_114 = V_14;
		NullCheck((NodeU5B0___U2C0___U5D_t1457036568*)(NodeU5B0___U2C0___U5D_t1457036568*)L_112);
		Node_t2989995042 * L_115 = ((NodeU5B0___U2C0___U5D_t1457036568*)(NodeU5B0___U2C0___U5D_t1457036568*)L_112)->GetAt(L_113, L_114);
		int32_t L_116 = V_11;
		NullCheck(L_115);
		L_115->set_movementPenalty_4(L_116);
		// if (blurredPenalty > penaltyMax)
		int32_t L_117 = V_11;
		int32_t L_118 = __this->get_penaltyMax_15();
		if ((((int32_t)L_117) <= ((int32_t)L_118)))
		{
			goto IL_0206;
		}
	}
	{
		// penaltyMax = blurredPenalty;
		int32_t L_119 = V_11;
		__this->set_penaltyMax_15(L_119);
	}

IL_0206:
	{
		// if (blurredPenalty < penaltyMin)
		int32_t L_120 = V_11;
		int32_t L_121 = __this->get_penaltyMin_14();
		if ((((int32_t)L_120) >= ((int32_t)L_121)))
		{
			goto IL_0218;
		}
	}
	{
		// penaltyMin = blurredPenalty;
		int32_t L_122 = V_11;
		__this->set_penaltyMin_14(L_122);
	}

IL_0218:
	{
		// for (int y = 1; y < gridSizeY; y++)
		int32_t L_123 = V_14;
		V_14 = ((int32_t)il2cpp_codegen_add((int32_t)L_123, (int32_t)1));
	}

IL_021e:
	{
		// for (int y = 1; y < gridSizeY; y++)
		int32_t L_124 = V_14;
		int32_t L_125 = __this->get_gridSizeY_13();
		if ((((int32_t)L_124) < ((int32_t)L_125)))
		{
			goto IL_0173;
		}
	}
	{
		// for (int x = 0; x < gridSizeX; x++)
		int32_t L_126 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add((int32_t)L_126, (int32_t)1));
	}

IL_0231:
	{
		// for (int x = 0; x < gridSizeX; x++)
		int32_t L_127 = V_10;
		int32_t L_128 = __this->get_gridSizeX_12();
		if ((((int32_t)L_127) < ((int32_t)L_128)))
		{
			goto IL_010d;
		}
	}
	{
		// }
		return;
	}
}
// System.Collections.Generic.List`1<Node> Grid::GetNeighbours(Node)
extern "C"  List_1_t167102488 * Grid_GetNeighbours_m4128505182 (Grid_t1081586032 * __this, Node_t2989995042 * ___node0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Grid_GetNeighbours_m4128505182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t167102488 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		// List<Node> neighbours = new List<Node>();
		List_1_t167102488 * L_0 = (List_1_t167102488 *)il2cpp_codegen_object_new(List_1_t167102488_il2cpp_TypeInfo_var);
		List_1__ctor_m3067334415(L_0, /*hidden argument*/List_1__ctor_m3067334415_RuntimeMethod_var);
		V_0 = L_0;
		// for (int x = -1; x <= 1; x++)
		V_1 = (-1);
		goto IL_0063;
	}

IL_000a:
	{
		// for (int y = -1; y <= 1; y++)
		V_2 = (-1);
		goto IL_005b;
	}

IL_000e:
	{
		// if (x == 0 && y == 0)
		int32_t L_1 = V_1;
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_2;
		if (!L_2)
		{
			goto IL_0057;
		}
	}

IL_0014:
	{
		// int checkX = node.gridX + x;
		Node_t2989995042 * L_3 = ___node0;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_gridX_2();
		int32_t L_5 = V_1;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5));
		// int checkY = node.gridY + y;
		Node_t2989995042 * L_6 = ___node0;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_gridY_3();
		int32_t L_8 = V_2;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8));
		// if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
		int32_t L_9 = V_3;
		if ((((int32_t)L_9) < ((int32_t)0)))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_10 = V_3;
		int32_t L_11 = __this->get_gridSizeX_12();
		if ((((int32_t)L_10) >= ((int32_t)L_11)))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_12 = V_4;
		if ((((int32_t)L_12) < ((int32_t)0)))
		{
			goto IL_0057;
		}
	}
	{
		int32_t L_13 = V_4;
		int32_t L_14 = __this->get_gridSizeY_13();
		if ((((int32_t)L_13) >= ((int32_t)L_14)))
		{
			goto IL_0057;
		}
	}
	{
		// neighbours.Add(grid[checkX, checkY]);
		List_1_t167102488 * L_15 = V_0;
		NodeU5B0___U2C0___U5D_t1457036568* L_16 = __this->get_grid_10();
		int32_t L_17 = V_3;
		int32_t L_18 = V_4;
		NullCheck((NodeU5B0___U2C0___U5D_t1457036568*)(NodeU5B0___U2C0___U5D_t1457036568*)L_16);
		Node_t2989995042 * L_19 = ((NodeU5B0___U2C0___U5D_t1457036568*)(NodeU5B0___U2C0___U5D_t1457036568*)L_16)->GetAt(L_17, L_18);
		NullCheck(L_15);
		List_1_Add_m4134525979(L_15, L_19, /*hidden argument*/List_1_Add_m4134525979_RuntimeMethod_var);
	}

IL_0057:
	{
		// for (int y = -1; y <= 1; y++)
		int32_t L_20 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
	}

IL_005b:
	{
		// for (int y = -1; y <= 1; y++)
		int32_t L_21 = V_2;
		if ((((int32_t)L_21) <= ((int32_t)1)))
		{
			goto IL_000e;
		}
	}
	{
		// for (int x = -1; x <= 1; x++)
		int32_t L_22 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0063:
	{
		// for (int x = -1; x <= 1; x++)
		int32_t L_23 = V_1;
		if ((((int32_t)L_23) <= ((int32_t)1)))
		{
			goto IL_000a;
		}
	}
	{
		// return neighbours;
		List_1_t167102488 * L_24 = V_0;
		return L_24;
	}
}
// Node Grid::NodeFromWorldPoint(UnityEngine.Vector3)
extern "C"  Node_t2989995042 * Grid_NodeFromWorldPoint_m3738070334 (Grid_t1081586032 * __this, Vector3_t3722313464  ___worldPosition0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Grid_NodeFromWorldPoint_m3738070334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		// float percentX = (worldPosition.x + gridWorldSize.x / 2) / gridWorldSize.x;
		Vector3_t3722313464  L_0 = ___worldPosition0;
		float L_1 = L_0.get_x_1();
		Vector2_t2156229523 * L_2 = __this->get_address_of_gridWorldSize_4();
		float L_3 = L_2->get_x_0();
		Vector2_t2156229523 * L_4 = __this->get_address_of_gridWorldSize_4();
		float L_5 = L_4->get_x_0();
		V_0 = ((float)((float)((float)il2cpp_codegen_add((float)L_1, (float)((float)((float)L_3/(float)(2.0f)))))/(float)L_5));
		// float percentY = (worldPosition.z + gridWorldSize.y / 2) / gridWorldSize.y;
		Vector3_t3722313464  L_6 = ___worldPosition0;
		float L_7 = L_6.get_z_3();
		Vector2_t2156229523 * L_8 = __this->get_address_of_gridWorldSize_4();
		float L_9 = L_8->get_y_1();
		Vector2_t2156229523 * L_10 = __this->get_address_of_gridWorldSize_4();
		float L_11 = L_10->get_y_1();
		V_1 = ((float)((float)((float)il2cpp_codegen_add((float)L_7, (float)((float)((float)L_9/(float)(2.0f)))))/(float)L_11));
		// percentX = Mathf.Clamp01(percentX);
		float L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_13 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		// percentY = Mathf.Clamp01(percentY);
		float L_14 = V_1;
		float L_15 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		// int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);
		int32_t L_16 = __this->get_gridSizeX_12();
		float L_17 = V_0;
		int32_t L_18 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_16, (int32_t)1))))), (float)L_17)), /*hidden argument*/NULL);
		V_2 = L_18;
		// int y = Mathf.RoundToInt((gridSizeY - 1) * percentY);
		int32_t L_19 = __this->get_gridSizeY_13();
		float L_20 = V_1;
		int32_t L_21 = Mathf_RoundToInt_m1874334613(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_19, (int32_t)1))))), (float)L_20)), /*hidden argument*/NULL);
		V_3 = L_21;
		// return grid[x, y];
		NodeU5B0___U2C0___U5D_t1457036568* L_22 = __this->get_grid_10();
		int32_t L_23 = V_2;
		int32_t L_24 = V_3;
		NullCheck((NodeU5B0___U2C0___U5D_t1457036568*)(NodeU5B0___U2C0___U5D_t1457036568*)L_22);
		Node_t2989995042 * L_25 = ((NodeU5B0___U2C0___U5D_t1457036568*)(NodeU5B0___U2C0___U5D_t1457036568*)L_22)->GetAt(L_23, L_24);
		return L_25;
	}
}
// System.Void Grid::OnDrawGizmos()
extern "C"  void Grid_OnDrawGizmos_m569343316 (Grid_t1081586032 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Grid_OnDrawGizmos_m569343316_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	NodeU5B0___U2C0___U5D_t1457036568* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Node_t2989995042 * V_5 = NULL;
	Color_t2555686324  G_B7_0;
	memset(&G_B7_0, 0, sizeof(G_B7_0));
	{
		// Gizmos.DrawWireCube (transform.position, new Vector3 (gridWorldSize.x, 1, gridWorldSize.y));
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3722313464  L_1 = Transform_get_position_m36019626(L_0, /*hidden argument*/NULL);
		Vector2_t2156229523 * L_2 = __this->get_address_of_gridWorldSize_4();
		float L_3 = L_2->get_x_0();
		Vector2_t2156229523 * L_4 = __this->get_address_of_gridWorldSize_4();
		float L_5 = L_4->get_y_1();
		Vector3_t3722313464  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m3353183577((&L_6), L_3, (1.0f), L_5, /*hidden argument*/NULL);
		Gizmos_DrawWireCube_m2631700312(NULL /*static, unused*/, L_1, L_6, /*hidden argument*/NULL);
		// if (grid != null && displayGridGizmos)
		NodeU5B0___U2C0___U5D_t1457036568* L_7 = __this->get_grid_10();
		if (!L_7)
		{
			goto IL_00fb;
		}
	}
	{
		bool L_8 = __this->get_displayGridGizmos_2();
		if (!L_8)
		{
			goto IL_00fb;
		}
	}
	{
		// foreach (Node n in grid)
		NodeU5B0___U2C0___U5D_t1457036568* L_9 = __this->get_grid_10();
		V_0 = (NodeU5B0___U2C0___U5D_t1457036568*)L_9;
		NodeU5B0___U2C0___U5D_t1457036568* L_10 = V_0;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_10);
		int32_t L_11 = Array_GetUpperBound_m4018715963((RuntimeArray *)(RuntimeArray *)L_10, 0, /*hidden argument*/NULL);
		V_1 = L_11;
		NodeU5B0___U2C0___U5D_t1457036568* L_12 = V_0;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_12);
		int32_t L_13 = Array_GetUpperBound_m4018715963((RuntimeArray *)(RuntimeArray *)L_12, 1, /*hidden argument*/NULL);
		V_2 = L_13;
		NodeU5B0___U2C0___U5D_t1457036568* L_14 = V_0;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_14);
		int32_t L_15 = Array_GetLowerBound_m2045984623((RuntimeArray *)(RuntimeArray *)L_14, 0, /*hidden argument*/NULL);
		V_3 = L_15;
		goto IL_00f4;
	}

IL_006a:
	{
		NodeU5B0___U2C0___U5D_t1457036568* L_16 = V_0;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_16);
		int32_t L_17 = Array_GetLowerBound_m2045984623((RuntimeArray *)(RuntimeArray *)L_16, 1, /*hidden argument*/NULL);
		V_4 = L_17;
		goto IL_00eb;
	}

IL_0075:
	{
		// foreach (Node n in grid)
		NodeU5B0___U2C0___U5D_t1457036568* L_18 = V_0;
		int32_t L_19 = V_3;
		int32_t L_20 = V_4;
		NullCheck(L_18);
		Node_t2989995042 * L_21 = (L_18)->GetAt(L_19, L_20);
		V_5 = L_21;
		// Gizmos.color = Color.Lerp(Color.white, Color.black, Mathf.InverseLerp(penaltyMin, penaltyMax, n.movementPenalty));
		Color_t2555686324  L_22 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t2555686324  L_23 = Color_get_black_m719512684(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_24 = __this->get_penaltyMin_14();
		int32_t L_25 = __this->get_penaltyMax_15();
		Node_t2989995042 * L_26 = V_5;
		NullCheck(L_26);
		int32_t L_27 = L_26->get_movementPenalty_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_28 = Mathf_InverseLerp_m4155825980(NULL /*static, unused*/, (((float)((float)L_24))), (((float)((float)L_25))), (((float)((float)L_27))), /*hidden argument*/NULL);
		Color_t2555686324  L_29 = Color_Lerp_m973389909(NULL /*static, unused*/, L_22, L_23, L_28, /*hidden argument*/NULL);
		Gizmos_set_color_m3399737545(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		// Gizmos.color = (n.walkable) ? Gizmos.color : Color.red;
		Node_t2989995042 * L_30 = V_5;
		NullCheck(L_30);
		bool L_31 = L_30->get_walkable_0();
		if (L_31)
		{
			goto IL_00bf;
		}
	}
	{
		Color_t2555686324  L_32 = Color_get_red_m3227813939(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B7_0 = L_32;
		goto IL_00c4;
	}

IL_00bf:
	{
		Color_t2555686324  L_33 = Gizmos_get_color_m712612415(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B7_0 = L_33;
	}

IL_00c4:
	{
		Gizmos_set_color_m3399737545(NULL /*static, unused*/, G_B7_0, /*hidden argument*/NULL);
		// Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter));
		Node_t2989995042 * L_34 = V_5;
		NullCheck(L_34);
		Vector3_t3722313464  L_35 = L_34->get_worldPosition_1();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_36 = Vector3_get_one_m1629952498(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_37 = __this->get_nodeDiameter_11();
		Vector3_t3722313464  L_38 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		Gizmos_DrawCube_m530322281(NULL /*static, unused*/, L_35, L_38, /*hidden argument*/NULL);
		int32_t L_39 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_39, (int32_t)1));
	}

IL_00eb:
	{
		// foreach (Node n in grid)
		int32_t L_40 = V_4;
		int32_t L_41 = V_2;
		if ((((int32_t)L_40) <= ((int32_t)L_41)))
		{
			goto IL_0075;
		}
	}
	{
		int32_t L_42 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_42, (int32_t)1));
	}

IL_00f4:
	{
		// foreach (Node n in grid)
		int32_t L_43 = V_3;
		int32_t L_44 = V_1;
		if ((((int32_t)L_43) <= ((int32_t)L_44)))
		{
			goto IL_006a;
		}
	}

IL_00fb:
	{
		// }
		return;
	}
}
// System.Void Grid::.ctor()
extern "C"  void Grid__ctor_m209885941 (Grid_t1081586032 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Grid__ctor_m209885941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public int obstacleProximityPenalty = 10;               // The terrain penalty of an obstacle
		__this->set_obstacleProximityPenalty_7(((int32_t)10));
		// private Dictionary<int, int> walkableRegionsDict =      // A dictionary of walkable regions
		//     new Dictionary<int, int>();
		Dictionary_2_t1839659084 * L_0 = (Dictionary_2_t1839659084 *)il2cpp_codegen_object_new(Dictionary_2_t1839659084_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1287366611(L_0, /*hidden argument*/Dictionary_2__ctor_m1287366611_RuntimeMethod_var);
		__this->set_walkableRegionsDict_8(L_0);
		// private int penaltyMin = int.MaxValue;                  // The minimum terrain penalty
		__this->set_penaltyMin_14(((int32_t)2147483647LL));
		// private int penaltyMax = int.MinValue;                  // The maximum terrain penalty
		__this->set_penaltyMax_15(((int32_t)-2147483648LL));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Grid/TerrainType::.ctor()
extern "C"  void TerrainType__ctor_m1989528255 (TerrainType_t3627529997 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Gun::Start()
extern "C"  void Gun_Start_m3692087021 (Gun_t783153271 * __this, const RuntimeMethod* method)
{
	{
		// secondsBetweenShots = 60 / rpm;
		float L_0 = __this->get_rpm_3();
		__this->set_secondsBetweenShots_11(((float)((float)(60.0f)/(float)L_0)));
		// }
		return;
	}
}
// System.Void Gun::Shoot()
extern "C"  void Gun_Shoot_m3160136974 (Gun_t783153271 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Gun_Shoot_m3160136974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (CanShoot())
		bool L_0 = Gun_CanShoot_m1361174918(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0083;
		}
	}
	{
		// GameObject bulletFire = Instantiate(shell, spawn.transform.position, transform.rotation);
		GameObject_t1113636619 * L_1 = __this->get_shell_7();
		Transform_t3600365921 * L_2 = __this->get_spawn_6();
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Quaternion_t2301928331  L_6 = Transform_get_rotation_m3502953881(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_7 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_1, L_4, L_6, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		// bulletFire.GetComponent<Rigidbody>().velocity = transform.forward * 32.0f;
		NullCheck(L_7);
		Rigidbody_t3916780224 * L_8 = GameObject_GetComponent_TisRigidbody_t3916780224_m564316479(L_7, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t3916780224_m564316479_RuntimeMethod_var);
		Transform_t3600365921 * L_9 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t3722313464  L_10 = Transform_get_forward_m747522392(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_11 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_10, (32.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		Rigidbody_set_velocity_m2899403247(L_8, L_11, /*hidden argument*/NULL);
		// nextPossibleShootTime = Time.time + secondsBetweenShots;
		float L_12 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_13 = __this->get_secondsBetweenShots_11();
		__this->set_nextPossibleShootTime_12(((float)il2cpp_codegen_add((float)L_12, (float)L_13)));
		// GetComponent<AudioSource>().PlayOneShot(gunShot, 1.0f);
		AudioSource_t3935305588 * L_14 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_15 = __this->get_gunShot_4();
		NullCheck(L_14);
		AudioSource_PlayOneShot_m2678069419(L_14, L_15, (1.0f), /*hidden argument*/NULL);
		// rounds -= 1;
		int32_t L_16 = __this->get_rounds_9();
		__this->set_rounds_9(((int32_t)il2cpp_codegen_subtract((int32_t)L_16, (int32_t)1)));
	}

IL_0083:
	{
		// }
		return;
	}
}
// System.Void Gun::ShootLaser()
extern "C"  void Gun_ShootLaser_m1916414356 (Gun_t783153271 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Gun_ShootLaser_m1916414356_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (CanShoot())
		bool L_0 = Gun_CanShoot_m1361174918(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0083;
		}
	}
	{
		// GameObject bulletFire = Instantiate(laser, spawn.transform.position, transform.rotation);
		GameObject_t1113636619 * L_1 = __this->get_laser_8();
		Transform_t3600365921 * L_2 = __this->get_spawn_6();
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Quaternion_t2301928331  L_6 = Transform_get_rotation_m3502953881(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_7 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_1, L_4, L_6, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		// bulletFire.GetComponent<Rigidbody>().velocity = transform.forward * 32.0f;
		NullCheck(L_7);
		Rigidbody_t3916780224 * L_8 = GameObject_GetComponent_TisRigidbody_t3916780224_m564316479(L_7, /*hidden argument*/GameObject_GetComponent_TisRigidbody_t3916780224_m564316479_RuntimeMethod_var);
		Transform_t3600365921 * L_9 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t3722313464  L_10 = Transform_get_forward_m747522392(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_11 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_10, (32.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		Rigidbody_set_velocity_m2899403247(L_8, L_11, /*hidden argument*/NULL);
		// nextPossibleShootTime = Time.time + secondsBetweenShots;
		float L_12 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_13 = __this->get_secondsBetweenShots_11();
		__this->set_nextPossibleShootTime_12(((float)il2cpp_codegen_add((float)L_12, (float)L_13)));
		// GetComponent<AudioSource>().PlayOneShot(laserShot, 0.2f);
		AudioSource_t3935305588 * L_14 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_15 = __this->get_laserShot_5();
		NullCheck(L_14);
		AudioSource_PlayOneShot_m2678069419(L_14, L_15, (0.2f), /*hidden argument*/NULL);
		// uses += 1;
		int32_t L_16 = __this->get_uses_10();
		__this->set_uses_10(((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1)));
	}

IL_0083:
	{
		// }
		return;
	}
}
// System.Void Gun::ShootContinuous()
extern "C"  void Gun_ShootContinuous_m1701998370 (Gun_t783153271 * __this, const RuntimeMethod* method)
{
	{
		// if (gunType == GunType.Auto)
		int32_t L_0 = __this->get_gunType_2();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_000f;
		}
	}
	{
		// Shoot();
		Gun_Shoot_m3160136974(__this, /*hidden argument*/NULL);
	}

IL_000f:
	{
		// }
		return;
	}
}
// System.Boolean Gun::CanShoot()
extern "C"  bool Gun_CanShoot_m1361174918 (Gun_t783153271 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// bool canShoot = true;
		V_0 = (bool)1;
		// if ((Time.time < nextPossibleShootTime) || (rounds < 1))
		float L_0 = Time_get_time_m2907476221(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_nextPossibleShootTime_12();
		if ((((float)L_0) < ((float)L_1)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_2 = __this->get_rounds_9();
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_001a;
		}
	}

IL_0018:
	{
		// canShoot = false;
		V_0 = (bool)0;
	}

IL_001a:
	{
		// return canShoot;
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void Gun::.ctor()
extern "C"  void Gun__ctor_m2866249351 (Gun_t783153271 * __this, const RuntimeMethod* method)
{
	{
		// public int rounds = 5;                          // Max number of rounds
		__this->set_rounds_9(5);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HealingSpot::Start()
extern "C"  void HealingSpot_Start_m1942109577 (HealingSpot_t2443304191 * __this, const RuntimeMethod* method)
{
	{
		// positionsIndex = Random.Range(0, positions.Length);
		Vector3U5BU5D_t1718750761* L_0 = __this->get_positions_3();
		NullCheck(L_0);
		int32_t L_1 = Random_Range_m4054026115(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_0)->max_length)))), /*hidden argument*/NULL);
		__this->set_positionsIndex_2(L_1);
		// }
		return;
	}
}
// System.Void HealingSpot::Update()
extern "C"  void HealingSpot_Update_m1717771304 (HealingSpot_t2443304191 * __this, const RuntimeMethod* method)
{
	{
		// transform.position = positions[positionsIndex];
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3U5BU5D_t1718750761* L_1 = __this->get_positions_3();
		int32_t L_2 = __this->get_positionsIndex_2();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		Vector3_t3722313464  L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_0);
		Transform_set_position_m3387557959(L_0, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void HealingSpot::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void HealingSpot_OnTriggerEnter_m4057859058 (HealingSpot_t2443304191 * __this, Collider_t1773347010 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealingSpot_OnTriggerEnter_m4057859058_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (collider.gameObject.tag == "Enemy")
		Collider_t1773347010 * L_0 = ___collider0;
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m3951609671(L_1, /*hidden argument*/NULL);
		bool L_3 = String_op_Equality_m920492651(NULL /*static, unused*/, L_2, _stringLiteral760905195, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0095;
		}
	}
	{
		// collider.GetComponent<EnemyStats>().eCurrentHealth += 30;
		Collider_t1773347010 * L_4 = ___collider0;
		NullCheck(L_4);
		EnemyStats_t4187841152 * L_5 = Component_GetComponent_TisEnemyStats_t4187841152_m1077339419(L_4, /*hidden argument*/Component_GetComponent_TisEnemyStats_t4187841152_m1077339419_RuntimeMethod_var);
		EnemyStats_t4187841152 * L_6 = L_5;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_eCurrentHealth_2();
		NullCheck(L_6);
		L_6->set_eCurrentHealth_2(((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)((int32_t)30))));
		// if (collider.GetComponent<EnemyStats>().eCurrentHealth < collider.GetComponent<EnemyStats>().eMaximumHealth)
		Collider_t1773347010 * L_8 = ___collider0;
		NullCheck(L_8);
		EnemyStats_t4187841152 * L_9 = Component_GetComponent_TisEnemyStats_t4187841152_m1077339419(L_8, /*hidden argument*/Component_GetComponent_TisEnemyStats_t4187841152_m1077339419_RuntimeMethod_var);
		NullCheck(L_9);
		int32_t L_10 = L_9->get_eCurrentHealth_2();
		Collider_t1773347010 * L_11 = ___collider0;
		NullCheck(L_11);
		EnemyStats_t4187841152 * L_12 = Component_GetComponent_TisEnemyStats_t4187841152_m1077339419(L_11, /*hidden argument*/Component_GetComponent_TisEnemyStats_t4187841152_m1077339419_RuntimeMethod_var);
		NullCheck(L_12);
		int32_t L_13 = L_12->get_eMaximumHealth_3();
		if ((((int32_t)L_10) >= ((int32_t)L_13)))
		{
			goto IL_0059;
		}
	}
	{
		// collider.GetComponent<EnemyStats>().eCurrentHealth = collider.GetComponent<EnemyStats>().eMaximumHealth;
		Collider_t1773347010 * L_14 = ___collider0;
		NullCheck(L_14);
		EnemyStats_t4187841152 * L_15 = Component_GetComponent_TisEnemyStats_t4187841152_m1077339419(L_14, /*hidden argument*/Component_GetComponent_TisEnemyStats_t4187841152_m1077339419_RuntimeMethod_var);
		Collider_t1773347010 * L_16 = ___collider0;
		NullCheck(L_16);
		EnemyStats_t4187841152 * L_17 = Component_GetComponent_TisEnemyStats_t4187841152_m1077339419(L_16, /*hidden argument*/Component_GetComponent_TisEnemyStats_t4187841152_m1077339419_RuntimeMethod_var);
		NullCheck(L_17);
		int32_t L_18 = L_17->get_eMaximumHealth_3();
		NullCheck(L_15);
		L_15->set_eCurrentHealth_2(L_18);
	}

IL_0059:
	{
		// positionsIndex = Random.Range(0, positions.Length);
		Vector3U5BU5D_t1718750761* L_19 = __this->get_positions_3();
		NullCheck(L_19);
		int32_t L_20 = Random_Range_m4054026115(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_19)->max_length)))), /*hidden argument*/NULL);
		__this->set_positionsIndex_2(L_20);
		// if (AIStateMachine.phaseOne)
		bool L_21 = ((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->get_phaseOne_3();
		if (!L_21)
		{
			goto IL_0082;
		}
	}
	{
		// AIStateMachine.phaseOne = false;
		((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->set_phaseOne_3((bool)0);
		// AIStateMachine.phaseTwo = true;
		((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->set_phaseTwo_4((bool)1);
		// }
		goto IL_0095;
	}

IL_0082:
	{
		// else if (AIStateMachine.phaseTwo)
		bool L_22 = ((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->get_phaseTwo_4();
		if (!L_22)
		{
			goto IL_0095;
		}
	}
	{
		// AIStateMachine.phaseTwo = false;
		((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->set_phaseTwo_4((bool)0);
		// AIStateMachine.phaseThree = true;
		((AIStateMachine_t2425577084_StaticFields*)il2cpp_codegen_static_fields_for(AIStateMachine_t2425577084_il2cpp_TypeInfo_var))->set_phaseThree_5((bool)1);
	}

IL_0095:
	{
		// if (collider.gameObject.tag == "Player")
		Collider_t1773347010 * L_23 = ___collider0;
		NullCheck(L_23);
		GameObject_t1113636619 * L_24 = Component_get_gameObject_m442555142(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		String_t* L_25 = GameObject_get_tag_m3951609671(L_24, /*hidden argument*/NULL);
		bool L_26 = String_op_Equality_m920492651(NULL /*static, unused*/, L_25, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0102;
		}
	}
	{
		// collider.GetComponent<PlayerStats>().pCurrentHealth += 10;
		Collider_t1773347010 * L_27 = ___collider0;
		NullCheck(L_27);
		PlayerStats_t2044123780 * L_28 = Component_GetComponent_TisPlayerStats_t2044123780_m1979177729(L_27, /*hidden argument*/Component_GetComponent_TisPlayerStats_t2044123780_m1979177729_RuntimeMethod_var);
		PlayerStats_t2044123780 * L_29 = L_28;
		NullCheck(L_29);
		int32_t L_30 = L_29->get_pCurrentHealth_2();
		NullCheck(L_29);
		L_29->set_pCurrentHealth_2(((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)((int32_t)10))));
		// if (collider.GetComponent<PlayerStats>().pCurrentHealth < collider.GetComponent<PlayerStats>().pMaximumHealth)
		Collider_t1773347010 * L_31 = ___collider0;
		NullCheck(L_31);
		PlayerStats_t2044123780 * L_32 = Component_GetComponent_TisPlayerStats_t2044123780_m1979177729(L_31, /*hidden argument*/Component_GetComponent_TisPlayerStats_t2044123780_m1979177729_RuntimeMethod_var);
		NullCheck(L_32);
		int32_t L_33 = L_32->get_pCurrentHealth_2();
		Collider_t1773347010 * L_34 = ___collider0;
		NullCheck(L_34);
		PlayerStats_t2044123780 * L_35 = Component_GetComponent_TisPlayerStats_t2044123780_m1979177729(L_34, /*hidden argument*/Component_GetComponent_TisPlayerStats_t2044123780_m1979177729_RuntimeMethod_var);
		NullCheck(L_35);
		int32_t L_36 = L_35->get_pMaximumHealth_3();
		if ((((int32_t)L_33) >= ((int32_t)L_36)))
		{
			goto IL_00ee;
		}
	}
	{
		// collider.GetComponent<PlayerStats>().pCurrentHealth = collider.GetComponent<PlayerStats>().pMaximumHealth;
		Collider_t1773347010 * L_37 = ___collider0;
		NullCheck(L_37);
		PlayerStats_t2044123780 * L_38 = Component_GetComponent_TisPlayerStats_t2044123780_m1979177729(L_37, /*hidden argument*/Component_GetComponent_TisPlayerStats_t2044123780_m1979177729_RuntimeMethod_var);
		Collider_t1773347010 * L_39 = ___collider0;
		NullCheck(L_39);
		PlayerStats_t2044123780 * L_40 = Component_GetComponent_TisPlayerStats_t2044123780_m1979177729(L_39, /*hidden argument*/Component_GetComponent_TisPlayerStats_t2044123780_m1979177729_RuntimeMethod_var);
		NullCheck(L_40);
		int32_t L_41 = L_40->get_pMaximumHealth_3();
		NullCheck(L_38);
		L_38->set_pCurrentHealth_2(L_41);
	}

IL_00ee:
	{
		// positionsIndex = Random.Range(0, positions.Length);
		Vector3U5BU5D_t1718750761* L_42 = __this->get_positions_3();
		NullCheck(L_42);
		int32_t L_43 = Random_Range_m4054026115(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_42)->max_length)))), /*hidden argument*/NULL);
		__this->set_positionsIndex_2(L_43);
	}

IL_0102:
	{
		// Debug.Log(transform.position.x + ", "
		//         + transform.position.y + ", "
		//         + transform.position.z);
		ObjectU5BU5D_t2843939325* L_44 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)5));
		Transform_t3600365921 * L_45 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_45);
		Vector3_t3722313464  L_46 = Transform_get_position_m36019626(L_45, /*hidden argument*/NULL);
		float L_47 = L_46.get_x_1();
		float L_48 = L_47;
		RuntimeObject * L_49 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_48);
		NullCheck(L_44);
		ArrayElementTypeCheck (L_44, L_49);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_49);
		ObjectU5BU5D_t2843939325* L_50 = L_44;
		NullCheck(L_50);
		ArrayElementTypeCheck (L_50, _stringLiteral3450517380);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)_stringLiteral3450517380);
		ObjectU5BU5D_t2843939325* L_51 = L_50;
		Transform_t3600365921 * L_52 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_52);
		Vector3_t3722313464  L_53 = Transform_get_position_m36019626(L_52, /*hidden argument*/NULL);
		float L_54 = L_53.get_y_2();
		float L_55 = L_54;
		RuntimeObject * L_56 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_55);
		NullCheck(L_51);
		ArrayElementTypeCheck (L_51, L_56);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_56);
		ObjectU5BU5D_t2843939325* L_57 = L_51;
		NullCheck(L_57);
		ArrayElementTypeCheck (L_57, _stringLiteral3450517380);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)_stringLiteral3450517380);
		ObjectU5BU5D_t2843939325* L_58 = L_57;
		Transform_t3600365921 * L_59 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_59);
		Vector3_t3722313464  L_60 = Transform_get_position_m36019626(L_59, /*hidden argument*/NULL);
		float L_61 = L_60.get_z_3();
		float L_62 = L_61;
		RuntimeObject * L_63 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_62);
		NullCheck(L_58);
		ArrayElementTypeCheck (L_58, L_63);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_63);
		String_t* L_64 = String_Concat_m2971454694(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_64, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void HealingSpot::.ctor()
extern "C"  void HealingSpot__ctor_m656588571 (HealingSpot_t2443304191 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealingSpot__ctor_m656588571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private Vector3[] positions = new Vector3[4]    // Array of Vector3 positions for the healing spot to spawn
		// {
		//     new Vector3(-17.0f, 0.5f, 17.0f),           // Top Left
		//     new Vector3(17.0f, 0.5f, 17.0f),            // Top Right
		//     new Vector3(-17.0f, 0.5f, -17.0f),          // Bottom Left
		//     new Vector3(17.0f, 0.5f, -17.0f)            // Bottom Right
		// };
		Vector3U5BU5D_t1718750761* L_0 = ((Vector3U5BU5D_t1718750761*)SZArrayNew(Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var, (uint32_t)4));
		Vector3_t3722313464  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m3353183577((&L_1), (-17.0f), (0.5f), (17.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector3_t3722313464 )L_1);
		Vector3U5BU5D_t1718750761* L_2 = L_0;
		Vector3_t3722313464  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m3353183577((&L_3), (17.0f), (0.5f), (17.0f), /*hidden argument*/NULL);
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (Vector3_t3722313464 )L_3);
		Vector3U5BU5D_t1718750761* L_4 = L_2;
		Vector3_t3722313464  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m3353183577((&L_5), (-17.0f), (0.5f), (-17.0f), /*hidden argument*/NULL);
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (Vector3_t3722313464 )L_5);
		Vector3U5BU5D_t1718750761* L_6 = L_4;
		Vector3_t3722313464  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m3353183577((&L_7), (17.0f), (0.5f), (-17.0f), /*hidden argument*/NULL);
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Vector3_t3722313464 )L_7);
		__this->set_positions_3(L_6);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HealthUI::Start()
extern "C"  void HealthUI_Start_m1058072216 (HealthUI_t1908446248 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void HealthUI::Update()
extern "C"  void HealthUI_Update_m287428648 (HealthUI_t1908446248 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < playerStats.pCurrentHealth; i++)
		V_0 = 0;
		goto IL_0045;
	}

IL_0004:
	{
		// if (playerStats.pCurrentHealth < healthHearts.Length)
		PlayerStats_t2044123780 * L_0 = __this->get_playerStats_2();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_pCurrentHealth_2();
		ImageU5BU5D_t2439009922* L_2 = __this->get_healthHearts_4();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length)))))))
		{
			goto IL_0033;
		}
	}
	{
		// healthHearts[playerStats.pCurrentHealth].enabled = false;
		ImageU5BU5D_t2439009922* L_3 = __this->get_healthHearts_4();
		PlayerStats_t2044123780 * L_4 = __this->get_playerStats_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_pCurrentHealth_2();
		NullCheck(L_3);
		int32_t L_6 = L_5;
		Image_t2670269651 * L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		Behaviour_set_enabled_m20417929(L_7, (bool)0, /*hidden argument*/NULL);
		// }
		goto IL_0041;
	}

IL_0033:
	{
		// healthHearts[i].enabled = true;
		ImageU5BU5D_t2439009922* L_8 = __this->get_healthHearts_4();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Image_t2670269651 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		Behaviour_set_enabled_m20417929(L_11, (bool)1, /*hidden argument*/NULL);
	}

IL_0041:
	{
		// for (int i = 0; i < playerStats.pCurrentHealth; i++)
		int32_t L_12 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_0045:
	{
		// for (int i = 0; i < playerStats.pCurrentHealth; i++)
		int32_t L_13 = V_0;
		PlayerStats_t2044123780 * L_14 = __this->get_playerStats_2();
		NullCheck(L_14);
		int32_t L_15 = L_14->get_pCurrentHealth_2();
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void HealthUI::.ctor()
extern "C"  void HealthUI__ctor_m3130073728 (HealthUI_t1908446248 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Interactible::Interact()
extern "C"  void Interactible_Interact_m2171514313 (Interactible_t1060880155 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Interactible_Interact_m2171514313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("Interacting with " + transform.name);
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m4211327027(L_0, /*hidden argument*/NULL);
		String_t* L_2 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2295225452, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Interactible::OnDrawGizmos()
extern "C"  void Interactible_OnDrawGizmos_m2396099765 (Interactible_t1060880155 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Interactible_OnDrawGizmos_m2396099765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (interactionTransform == null)
		Transform_t3600365921 * L_0 = __this->get_interactionTransform_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		// interactionTransform = transform;
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		__this->set_interactionTransform_3(L_2);
	}

IL_001a:
	{
		// Gizmos.color = Color.yellow;
		Color_t2555686324  L_3 = Color_get_yellow_m1287957903(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m3399737545(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		// Gizmos.DrawWireSphere(interactionTransform.position, radius);
		Transform_t3600365921 * L_4 = __this->get_interactionTransform_3();
		NullCheck(L_4);
		Vector3_t3722313464  L_5 = Transform_get_position_m36019626(L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_radius_2();
		Gizmos_DrawWireSphere_m132265467(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Interactible::.ctor()
extern "C"  void Interactible__ctor_m757280019 (Interactible_t1060880155 * __this, const RuntimeMethod* method)
{
	{
		// public float radius = 3.0f;             // How close the player needs to be to interact with the GameObject
		__this->set_radius_2((3.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Inventory::Awake()
extern "C"  void Inventory_Awake_m1952883581 (Inventory_t1050226016 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Inventory_Awake_m1952883581_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (instance != null)
		Inventory_t1050226016 * L_0 = ((Inventory_t1050226016_StaticFields*)il2cpp_codegen_static_fields_for(Inventory_t1050226016_il2cpp_TypeInfo_var))->get_instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		// Debug.LogWarning("More than 1 instance of Inventory found!");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3752629331(NULL /*static, unused*/, _stringLiteral1456734628, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_0018:
	{
		// instance = this;
		((Inventory_t1050226016_StaticFields*)il2cpp_codegen_static_fields_for(Inventory_t1050226016_il2cpp_TypeInfo_var))->set_instance_2(__this);
		// }
		return;
	}
}
// System.Boolean Inventory::AddItem(Item)
extern "C"  bool Inventory_AddItem_m2364679108 (Inventory_t1050226016 * __this, Item_t2953980098 * ___item0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Inventory_AddItem_m2364679108_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (item.isWeapon && weapon == null)
		Item_t2953980098 * L_0 = ___item0;
		NullCheck(L_0);
		bool L_1 = L_0->get_isWeapon_4();
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		Item_t2953980098 * L_2 = __this->get_weapon_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		// weapon = item;
		Item_t2953980098 * L_4 = ___item0;
		__this->set_weapon_6(L_4);
		// if (onItemChangedCallback != null)
		OnItemChanged_t22848112 * L_5 = __this->get_onItemChangedCallback_3();
		if (!L_5)
		{
			goto IL_0030;
		}
	}
	{
		// onItemChangedCallback.Invoke();
		OnItemChanged_t22848112 * L_6 = __this->get_onItemChangedCallback_3();
		NullCheck(L_6);
		OnItemChanged_Invoke_m404998984(L_6, /*hidden argument*/NULL);
	}

IL_0030:
	{
		// return true;
		return (bool)1;
	}

IL_0032:
	{
		// else if (!item.isWeapon && shield == null)
		Item_t2953980098 * L_7 = ___item0;
		NullCheck(L_7);
		bool L_8 = L_7->get_isWeapon_4();
		if (L_8)
		{
			goto IL_0064;
		}
	}
	{
		Item_t2953980098 * L_9 = __this->get_shield_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_9, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0064;
		}
	}
	{
		// shield = item;
		Item_t2953980098 * L_11 = ___item0;
		__this->set_shield_7(L_11);
		// if (onItemChangedCallback != null)
		OnItemChanged_t22848112 * L_12 = __this->get_onItemChangedCallback_3();
		if (!L_12)
		{
			goto IL_0062;
		}
	}
	{
		// onItemChangedCallback.Invoke();
		OnItemChanged_t22848112 * L_13 = __this->get_onItemChangedCallback_3();
		NullCheck(L_13);
		OnItemChanged_Invoke_m404998984(L_13, /*hidden argument*/NULL);
	}

IL_0062:
	{
		// return true;
		return (bool)1;
	}

IL_0064:
	{
		// Debug.Log("No room at the inn");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2285872367, /*hidden argument*/NULL);
		// return false;
		return (bool)0;
	}
}
// System.Void Inventory::RemoveItem(Item)
extern "C"  void Inventory_RemoveItem_m325135610 (Inventory_t1050226016 * __this, Item_t2953980098 * ___item0, const RuntimeMethod* method)
{
	{
		// if (item.isWeapon)
		Item_t2953980098 * L_0 = ___item0;
		NullCheck(L_0);
		bool L_1 = L_0->get_isWeapon_4();
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		// weapon = null;
		__this->set_weapon_6((Item_t2953980098 *)NULL);
		// }
		goto IL_0018;
	}

IL_0011:
	{
		// shield = null;
		__this->set_shield_7((Item_t2953980098 *)NULL);
	}

IL_0018:
	{
		// if (onItemChangedCallback != null)
		OnItemChanged_t22848112 * L_2 = __this->get_onItemChangedCallback_3();
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		// onItemChangedCallback.Invoke();
		OnItemChanged_t22848112 * L_3 = __this->get_onItemChangedCallback_3();
		NullCheck(L_3);
		OnItemChanged_Invoke_m404998984(L_3, /*hidden argument*/NULL);
	}

IL_002b:
	{
		// }
		return;
	}
}
// System.Void Inventory::.ctor()
extern "C"  void Inventory__ctor_m912618405 (Inventory_t1050226016 * __this, const RuntimeMethod* method)
{
	{
		// public int weaponSpace = 1;
		__this->set_weaponSpace_4(1);
		// public int shieldSpace = 1;
		__this->set_shieldSpace_5(1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_OnItemChanged_t22848112 (OnItemChanged_t22848112 * __this, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void Inventory/OnItemChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void OnItemChanged__ctor_m2205075595 (OnItemChanged_t22848112 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Inventory/OnItemChanged::Invoke()
extern "C"  void OnItemChanged_Invoke_m404998984 (OnItemChanged_t22848112 * __this, const RuntimeMethod* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	DelegateU5BU5D_t1703627840* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t1188392813* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			if (___methodIsStatic)
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 0)
				{
					// open
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, targetMethod);
					}
				}
				else
				{
					// closed
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, void*, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, targetThis, targetMethod);
					}
				}
			}
			else
			{
				{
					// closed
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
							else
								GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
							else
								VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
					}
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		if (___methodIsStatic)
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 0)
			{
				// open
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, targetMethod);
				}
			}
			else
			{
				// closed
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, void*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, targetThis, targetMethod);
				}
			}
		}
		else
		{
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
						else
							GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
						else
							VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult Inventory/OnItemChanged::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* OnItemChanged_BeginInvoke_m1036576206 (OnItemChanged_t22848112 * __this, AsyncCallback_t3962456242 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void Inventory/OnItemChanged::EndInvoke(System.IAsyncResult)
extern "C"  void OnItemChanged_EndInvoke_m1829018350 (OnItemChanged_t22848112 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InventorySlot::AddItem(Item)
extern "C"  void InventorySlot_AddItem_m1624661967 (InventorySlot_t3299309524 * __this, Item_t2953980098 * ___newItem0, const RuntimeMethod* method)
{
	{
		// item = newItem;
		Item_t2953980098 * L_0 = ___newItem0;
		__this->set_item_2(L_0);
		// icon.sprite = item.icon;
		Image_t2670269651 * L_1 = __this->get_icon_4();
		Item_t2953980098 * L_2 = __this->get_item_2();
		NullCheck(L_2);
		Sprite_t280657092 * L_3 = L_2->get_icon_3();
		NullCheck(L_1);
		Image_set_sprite_m2369174689(L_1, L_3, /*hidden argument*/NULL);
		// icon.enabled = true;
		Image_t2670269651 * L_4 = __this->get_icon_4();
		NullCheck(L_4);
		Behaviour_set_enabled_m20417929(L_4, (bool)1, /*hidden argument*/NULL);
		// button.SetActive(true);
		GameObject_t1113636619 * L_5 = __this->get_button_5();
		NullCheck(L_5);
		GameObject_SetActive_m796801857(L_5, (bool)1, /*hidden argument*/NULL);
		// itemName.text = item.name;
		Text_t1901882714 * L_6 = __this->get_itemName_6();
		Item_t2953980098 * L_7 = __this->get_item_2();
		NullCheck(L_7);
		String_t* L_8 = L_7->get_name_2();
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_8);
		// itemName.gameObject.SetActive(true);
		Text_t1901882714 * L_9 = __this->get_itemName_6();
		NullCheck(L_9);
		GameObject_t1113636619 * L_10 = Component_get_gameObject_m442555142(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		GameObject_SetActive_m796801857(L_10, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void InventorySlot::ClearSlot()
extern "C"  void InventorySlot_ClearSlot_m1469806239 (InventorySlot_t3299309524 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InventorySlot_ClearSlot_m1469806239_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// item = null;
		__this->set_item_2((Item_t2953980098 *)NULL);
		// icon.sprite = null;
		Image_t2670269651 * L_0 = __this->get_icon_4();
		NullCheck(L_0);
		Image_set_sprite_m2369174689(L_0, (Sprite_t280657092 *)NULL, /*hidden argument*/NULL);
		// icon.enabled = false;
		Image_t2670269651 * L_1 = __this->get_icon_4();
		NullCheck(L_1);
		Behaviour_set_enabled_m20417929(L_1, (bool)0, /*hidden argument*/NULL);
		// button.SetActive(false);
		GameObject_t1113636619 * L_2 = __this->get_button_5();
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		// itemName.gameObject.SetActive(false);
		Text_t1901882714 * L_3 = __this->get_itemName_6();
		NullCheck(L_3);
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)0, /*hidden argument*/NULL);
		// itemName.text = "";
		Text_t1901882714 * L_5 = __this->get_itemName_6();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteral757602046);
		// }
		return;
	}
}
// System.Void InventorySlot::.ctor()
extern "C"  void InventorySlot__ctor_m3565299669 (InventorySlot_t3299309524 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InventoryUI::Start()
extern "C"  void InventoryUI_Start_m1998966654 (InventoryUI_t1983315844 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InventoryUI_Start_m1998966654_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// inventory = Inventory.instance;
		Inventory_t1050226016 * L_0 = ((Inventory_t1050226016_StaticFields*)il2cpp_codegen_static_fields_for(Inventory_t1050226016_il2cpp_TypeInfo_var))->get_instance_2();
		__this->set_inventory_2(L_0);
		// inventory.onItemChangedCallback += UpdateUI;
		Inventory_t1050226016 * L_1 = __this->get_inventory_2();
		Inventory_t1050226016 * L_2 = L_1;
		NullCheck(L_2);
		OnItemChanged_t22848112 * L_3 = L_2->get_onItemChangedCallback_3();
		intptr_t L_4 = (intptr_t)InventoryUI_UpdateUI_m4132935956_RuntimeMethod_var;
		OnItemChanged_t22848112 * L_5 = (OnItemChanged_t22848112 *)il2cpp_codegen_object_new(OnItemChanged_t22848112_il2cpp_TypeInfo_var);
		OnItemChanged__ctor_m2205075595(L_5, __this, L_4, /*hidden argument*/NULL);
		Delegate_t1188392813 * L_6 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		L_2->set_onItemChangedCallback_3(((OnItemChanged_t22848112 *)CastclassSealed((RuntimeObject*)L_6, OnItemChanged_t22848112_il2cpp_TypeInfo_var)));
		// }
		return;
	}
}
// System.Void InventoryUI::Update()
extern "C"  void InventoryUI_Update_m2843462320 (InventoryUI_t1983315844 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void InventoryUI::UpdateUI()
extern "C"  void InventoryUI_UpdateUI_m4132935956 (InventoryUI_t1983315844 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InventoryUI_UpdateUI_m4132935956_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("Updating UI");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral32543115, /*hidden argument*/NULL);
		// if (inventory.weapon != null)
		Inventory_t1050226016 * L_0 = __this->get_inventory_2();
		NullCheck(L_0);
		Item_t2953980098 * L_1 = L_0->get_weapon_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0035;
		}
	}
	{
		// weaponSlot.AddItem(inventory.weapon);
		InventorySlot_t3299309524 * L_3 = __this->get_weaponSlot_3();
		Inventory_t1050226016 * L_4 = __this->get_inventory_2();
		NullCheck(L_4);
		Item_t2953980098 * L_5 = L_4->get_weapon_6();
		NullCheck(L_3);
		InventorySlot_AddItem_m1624661967(L_3, L_5, /*hidden argument*/NULL);
		// }
		goto IL_0040;
	}

IL_0035:
	{
		// weaponSlot.ClearSlot();
		InventorySlot_t3299309524 * L_6 = __this->get_weaponSlot_3();
		NullCheck(L_6);
		InventorySlot_ClearSlot_m1469806239(L_6, /*hidden argument*/NULL);
	}

IL_0040:
	{
		// if (inventory.shield != null)
		Inventory_t1050226016 * L_7 = __this->get_inventory_2();
		NullCheck(L_7);
		Item_t2953980098 * L_8 = L_7->get_shield_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_8, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_006a;
		}
	}
	{
		// shieldSlot.AddItem(inventory.shield);
		InventorySlot_t3299309524 * L_10 = __this->get_shieldSlot_4();
		Inventory_t1050226016 * L_11 = __this->get_inventory_2();
		NullCheck(L_11);
		Item_t2953980098 * L_12 = L_11->get_shield_7();
		NullCheck(L_10);
		InventorySlot_AddItem_m1624661967(L_10, L_12, /*hidden argument*/NULL);
		// }
		return;
	}

IL_006a:
	{
		// shieldSlot.ClearSlot();
		InventorySlot_t3299309524 * L_13 = __this->get_shieldSlot_4();
		NullCheck(L_13);
		InventorySlot_ClearSlot_m1469806239(L_13, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void InventoryUI::.ctor()
extern "C"  void InventoryUI__ctor_m3917235252 (InventoryUI_t1983315844 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Item::.ctor()
extern "C"  void Item__ctor_m64206515 (Item_t2953980098 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Item__ctor_m64206515_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// new public string name = "New Item";
		__this->set_name_2(_stringLiteral3636330245);
		ScriptableObject__ctor_m1310743131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ItemPickup::Interact()
extern "C"  void ItemPickup_Interact_m1189612395 (ItemPickup_t4277059944 * __this, const RuntimeMethod* method)
{
	{
		// base.Interact();
		Interactible_Interact_m2171514313(__this, /*hidden argument*/NULL);
		// PickUp();
		ItemPickup_PickUp_m3865268335(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ItemPickup::PickUp()
extern "C"  void ItemPickup_PickUp_m3865268335 (ItemPickup_t4277059944 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemPickup_PickUp_m3865268335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Debug.Log("Picking up " + item.name);
		Item_t2953980098 * L_0 = __this->get_item_4();
		NullCheck(L_0);
		String_t* L_1 = L_0->get_name_2();
		String_t* L_2 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2101004758, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// bool pickedUpItem = Inventory.instance.AddItem(item);
		Inventory_t1050226016 * L_3 = ((Inventory_t1050226016_StaticFields*)il2cpp_codegen_static_fields_for(Inventory_t1050226016_il2cpp_TypeInfo_var))->get_instance_2();
		Item_t2953980098 * L_4 = __this->get_item_4();
		NullCheck(L_3);
		bool L_5 = Inventory_AddItem_m2364679108(L_3, L_4, /*hidden argument*/NULL);
		// if (pickedUpItem)
		if (!L_5)
		{
			goto IL_0051;
		}
	}
	{
		// Inventory.instance.GetComponent<AudioSource>().PlayOneShot(pickupSound, 0.5f);
		Inventory_t1050226016 * L_6 = ((Inventory_t1050226016_StaticFields*)il2cpp_codegen_static_fields_for(Inventory_t1050226016_il2cpp_TypeInfo_var))->get_instance_2();
		NullCheck(L_6);
		AudioSource_t3935305588 * L_7 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(L_6, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_8 = __this->get_pickupSound_5();
		NullCheck(L_7);
		AudioSource_PlayOneShot_m2678069419(L_7, L_8, (0.5f), /*hidden argument*/NULL);
		// Destroy(gameObject);
		GameObject_t1113636619 * L_9 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_0051:
	{
		// }
		return;
	}
}
// System.Void ItemPickup::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void ItemPickup_OnTriggerEnter_m2453855028 (ItemPickup_t4277059944 * __this, Collider_t1773347010 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ItemPickup_OnTriggerEnter_m2453855028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (collider.gameObject.tag == "Player")
		Collider_t1773347010 * L_0 = ___collider0;
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m3951609671(L_1, /*hidden argument*/NULL);
		bool L_3 = String_op_Equality_m920492651(NULL /*static, unused*/, L_2, _stringLiteral2261822918, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001d;
		}
	}
	{
		// Interact();
		VirtActionInvoker0::Invoke(4 /* System.Void Interactible::Interact() */, __this);
	}

IL_001d:
	{
		// }
		return;
	}
}
// System.Void ItemPickup::.ctor()
extern "C"  void ItemPickup__ctor_m3317152332 (ItemPickup_t4277059944 * __this, const RuntimeMethod* method)
{
	{
		Interactible__ctor_m757280019(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LevelChanger::Update()
extern "C"  void LevelChanger_Update_m888405421 (LevelChanger_t225386971 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void LevelChanger::FadeToLevel(System.String)
extern "C"  void LevelChanger_FadeToLevel_m2444280689 (LevelChanger_t225386971 * __this, String_t* ___levelName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelChanger_FadeToLevel_m2444280689_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// levelNameToLoad = levelName;
		String_t* L_0 = ___levelName0;
		__this->set_levelNameToLoad_3(L_0);
		// loadByName = true;
		__this->set_loadByName_5((bool)1);
		// animator.SetTrigger("FadeOut");
		Animator_t434523843 * L_1 = __this->get_animator_2();
		NullCheck(L_1);
		Animator_SetTrigger_m2134052629(L_1, _stringLiteral1012806073, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelChanger::FadeToLevel(System.Int32)
extern "C"  void LevelChanger_FadeToLevel_m359947979 (LevelChanger_t225386971 * __this, int32_t ___levelIndex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelChanger_FadeToLevel_m359947979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// levelIndexToLoad = levelIndex;
		int32_t L_0 = ___levelIndex0;
		__this->set_levelIndexToLoad_4(L_0);
		// loadByName = false;
		__this->set_loadByName_5((bool)0);
		// animator.SetTrigger("FadeOut");
		Animator_t434523843 * L_1 = __this->get_animator_2();
		NullCheck(L_1);
		Animator_SetTrigger_m2134052629(L_1, _stringLiteral1012806073, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelChanger::OnFadeComplete()
extern "C"  void LevelChanger_OnFadeComplete_m3579193737 (LevelChanger_t225386971 * __this, const RuntimeMethod* method)
{
	{
		// if (loadByName)
		bool L_0 = __this->get_loadByName_5();
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		// SceneManager.LoadScene(levelNameToLoad);
		String_t* L_1 = __this->get_levelNameToLoad_3();
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0014:
	{
		// SceneManager.LoadScene(levelIndexToLoad);
		int32_t L_2 = __this->get_levelIndexToLoad_4();
		SceneManager_LoadScene_m3463216446(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelChanger::.ctor()
extern "C"  void LevelChanger__ctor_m1583998437 (LevelChanger_t225386971 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LevelClear::Start()
extern "C"  void LevelClear_Start_m1882581272 (LevelClear_t1949285084 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelClear_Start_m1882581272_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// levelChanger = FindObjectOfType<LevelChanger>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		LevelChanger_t225386971 * L_0 = Object_FindObjectOfType_TisLevelChanger_t225386971_m2918287313(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisLevelChanger_t225386971_m2918287313_RuntimeMethod_var);
		__this->set_levelChanger_3(L_0);
		// }
		return;
	}
}
// System.Void LevelClear::Update()
extern "C"  void LevelClear_Update_m1982332088 (LevelClear_t1949285084 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void LevelClear::FadeToLevel(System.String)
extern "C"  void LevelClear_FadeToLevel_m1500572359 (LevelClear_t1949285084 * __this, String_t* ___levelName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelClear_FadeToLevel_m1500572359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// levelNameToLoad = levelName;
		String_t* L_0 = ___levelName0;
		__this->set_levelNameToLoad_4(L_0);
		// loadByName = true;
		__this->set_loadByName_6((bool)1);
		// animator.enabled = true;
		Animator_t434523843 * L_1 = __this->get_animator_2();
		NullCheck(L_1);
		Behaviour_set_enabled_m20417929(L_1, (bool)1, /*hidden argument*/NULL);
		// PauseMenu.pausingEnabled = false;
		IL2CPP_RUNTIME_CLASS_INIT(PauseMenu_t3916167947_il2cpp_TypeInfo_var);
		((PauseMenu_t3916167947_StaticFields*)il2cpp_codegen_static_fields_for(PauseMenu_t3916167947_il2cpp_TypeInfo_var))->set_pausingEnabled_3((bool)0);
		// animator.SetTrigger("ClearedLevel");
		Animator_t434523843 * L_2 = __this->get_animator_2();
		NullCheck(L_2);
		Animator_SetTrigger_m2134052629(L_2, _stringLiteral3886292556, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelClear::FadeToLevel(System.Int32)
extern "C"  void LevelClear_FadeToLevel_m1424934385 (LevelClear_t1949285084 * __this, int32_t ___levelIndex0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelClear_FadeToLevel_m1424934385_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// levelIndexToLoad = levelIndex;
		int32_t L_0 = ___levelIndex0;
		__this->set_levelIndexToLoad_5(L_0);
		// loadByName = false;
		__this->set_loadByName_6((bool)0);
		// animator.enabled = true;
		Animator_t434523843 * L_1 = __this->get_animator_2();
		NullCheck(L_1);
		Behaviour_set_enabled_m20417929(L_1, (bool)1, /*hidden argument*/NULL);
		// animator.SetTrigger("ClearedLevel");
		Animator_t434523843 * L_2 = __this->get_animator_2();
		NullCheck(L_2);
		Animator_SetTrigger_m2134052629(L_2, _stringLiteral3886292556, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelClear::OnFadeComplete()
extern "C"  void LevelClear_OnFadeComplete_m980408912 (LevelClear_t1949285084 * __this, const RuntimeMethod* method)
{
	{
		// if (loadByName)
		bool L_0 = __this->get_loadByName_6();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		// levelChanger.FadeToLevel(levelNameToLoad);
		LevelChanger_t225386971 * L_1 = __this->get_levelChanger_3();
		String_t* L_2 = __this->get_levelNameToLoad_4();
		NullCheck(L_1);
		LevelChanger_FadeToLevel_m2444280689(L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}

IL_001a:
	{
		// levelChanger.FadeToLevel(levelIndexToLoad);
		LevelChanger_t225386971 * L_3 = __this->get_levelChanger_3();
		int32_t L_4 = __this->get_levelIndexToLoad_5();
		NullCheck(L_3);
		LevelChanger_FadeToLevel_m359947979(L_3, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelClear::.ctor()
extern "C"  void LevelClear__ctor_m106764232 (LevelClear_t1949285084 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LevelSelect::Start()
extern "C"  void LevelSelect_Start_m703985854 (LevelSelect_t2669120976 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelSelect_Start_m703985854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// levelChanger = FindObjectOfType<LevelChanger>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		LevelChanger_t225386971 * L_0 = Object_FindObjectOfType_TisLevelChanger_t225386971_m2918287313(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisLevelChanger_t225386971_m2918287313_RuntimeMethod_var);
		__this->set_levelChanger_7(L_0);
		// index = 0;
		__this->set_index_2(0);
		// DeselectAll();
		LevelSelect_DeselectAll_m630293501(__this, /*hidden argument*/NULL);
		// levelSelectPanels[index].selected = true;
		LevelSelectPanelU5BU5D_t1747102860* L_1 = __this->get_levelSelectPanels_6();
		int32_t L_2 = __this->get_index_2();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		LevelSelectPanel_t2626311185 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		L_4->set_selected_5((bool)1);
		// }
		return;
	}
}
// System.Void LevelSelect::Update()
extern "C"  void LevelSelect_Update_m1046745160 (LevelSelect_t2669120976 * __this, const RuntimeMethod* method)
{
	{
		// UpdateOptions();
		LevelSelect_UpdateOptions_m4096405336(__this, /*hidden argument*/NULL);
		// SelectOptions();
		LevelSelect_SelectOptions_m312907934(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelSelect::UpdateOptions()
extern "C"  void LevelSelect_UpdateOptions_m4096405336 (LevelSelect_t2669120976 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelSelect_UpdateOptions_m4096405336_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (index >= levelSelectPanels.Length)
		int32_t L_0 = __this->get_index_2();
		LevelSelectPanelU5BU5D_t1747102860* L_1 = __this->get_levelSelectPanels_6();
		NullCheck(L_1);
		if ((((int32_t)L_0) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))))))
		{
			goto IL_0030;
		}
	}
	{
		// index = 0;
		__this->set_index_2(0);
		// DeselectAll();
		LevelSelect_DeselectAll_m630293501(__this, /*hidden argument*/NULL);
		// levelSelectPanels[index].selected = true;
		LevelSelectPanelU5BU5D_t1747102860* L_2 = __this->get_levelSelectPanels_6();
		int32_t L_3 = __this->get_index_2();
		NullCheck(L_2);
		int32_t L_4 = L_3;
		LevelSelectPanel_t2626311185 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		L_5->set_selected_5((bool)1);
	}

IL_0030:
	{
		// if (index < 0)
		int32_t L_6 = __this->get_index_2();
		if ((((int32_t)L_6) >= ((int32_t)0)))
		{
			goto IL_0062;
		}
	}
	{
		// index = levelSelectPanels.Length - 1;
		LevelSelectPanelU5BU5D_t1747102860* L_7 = __this->get_levelSelectPanels_6();
		NullCheck(L_7);
		__this->set_index_2(((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_7)->max_length)))), (int32_t)1)));
		// DeselectAll();
		LevelSelect_DeselectAll_m630293501(__this, /*hidden argument*/NULL);
		// levelSelectPanels[index].selected = true;
		LevelSelectPanelU5BU5D_t1747102860* L_8 = __this->get_levelSelectPanels_6();
		int32_t L_9 = __this->get_index_2();
		NullCheck(L_8);
		int32_t L_10 = L_9;
		LevelSelectPanel_t2626311185 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		L_11->set_selected_5((bool)1);
	}

IL_0062:
	{
		// if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetAxisRaw("DPadHorizontal") > 0 || Input.GetAxisRaw("Horizontal") > 0)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_12 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)275), /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0090;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_13 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral2219833165, /*hidden argument*/NULL);
		if ((((float)L_13) > ((float)(0.0f))))
		{
			goto IL_0090;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_14 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral1828639942, /*hidden argument*/NULL);
		if ((!(((float)L_14) > ((float)(0.0f)))))
		{
			goto IL_00de;
		}
	}

IL_0090:
	{
		// if (!rightAxesInUse)
		bool L_15 = __this->get_rightAxesInUse_9();
		if (L_15)
		{
			goto IL_00e5;
		}
	}
	{
		// rightAxesInUse = true;
		__this->set_rightAxesInUse_9((bool)1);
		// GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);
		AudioSource_t3935305588 * L_16 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_17 = __this->get_selectSound_3();
		NullCheck(L_16);
		AudioSource_PlayOneShot_m2678069419(L_16, L_17, (0.6f), /*hidden argument*/NULL);
		// index += 1;
		int32_t L_18 = __this->get_index_2();
		__this->set_index_2(((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1)));
		// DeselectAll();
		LevelSelect_DeselectAll_m630293501(__this, /*hidden argument*/NULL);
		// levelSelectPanels[index].selected = true;
		LevelSelectPanelU5BU5D_t1747102860* L_19 = __this->get_levelSelectPanels_6();
		int32_t L_20 = __this->get_index_2();
		NullCheck(L_19);
		int32_t L_21 = L_20;
		LevelSelectPanel_t2626311185 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		L_22->set_selected_5((bool)1);
		// }
		goto IL_00e5;
	}

IL_00de:
	{
		// rightAxesInUse = false;
		__this->set_rightAxesInUse_9((bool)0);
	}

IL_00e5:
	{
		// if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetAxisRaw("DPadHorizontal") < 0 || Input.GetAxisRaw("Horizontal") < 0)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_23 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)276), /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_0113;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_24 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral2219833165, /*hidden argument*/NULL);
		if ((((float)L_24) < ((float)(0.0f))))
		{
			goto IL_0113;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_25 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral1828639942, /*hidden argument*/NULL);
		if ((!(((float)L_25) < ((float)(0.0f)))))
		{
			goto IL_0160;
		}
	}

IL_0113:
	{
		// if (!leftAxesInUse)
		bool L_26 = __this->get_leftAxesInUse_8();
		if (L_26)
		{
			goto IL_0167;
		}
	}
	{
		// leftAxesInUse = true;
		__this->set_leftAxesInUse_8((bool)1);
		// GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);
		AudioSource_t3935305588 * L_27 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_28 = __this->get_selectSound_3();
		NullCheck(L_27);
		AudioSource_PlayOneShot_m2678069419(L_27, L_28, (0.6f), /*hidden argument*/NULL);
		// index -= 1;
		int32_t L_29 = __this->get_index_2();
		__this->set_index_2(((int32_t)il2cpp_codegen_subtract((int32_t)L_29, (int32_t)1)));
		// DeselectAll();
		LevelSelect_DeselectAll_m630293501(__this, /*hidden argument*/NULL);
		// levelSelectPanels[index].selected = true;
		LevelSelectPanelU5BU5D_t1747102860* L_30 = __this->get_levelSelectPanels_6();
		int32_t L_31 = __this->get_index_2();
		NullCheck(L_30);
		int32_t L_32 = L_31;
		LevelSelectPanel_t2626311185 * L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_33);
		L_33->set_selected_5((bool)1);
		// }
		return;
	}

IL_0160:
	{
		// leftAxesInUse = false;
		__this->set_leftAxesInUse_8((bool)0);
	}

IL_0167:
	{
		// }
		return;
	}
}
// System.Void LevelSelect::SelectOptions()
extern "C"  void LevelSelect_SelectOptions_m312907934 (LevelSelect_t2669120976 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelSelect_SelectOptions_m312907934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Input.GetKeyDown(KeyCode.Space) || Input.GetAxisRaw("Submit") > 0)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)32), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_1 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral1187062204, /*hidden argument*/NULL);
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_00dc;
		}
	}

IL_001d:
	{
		// if (!confirmAxesInUse)
		bool L_2 = __this->get_confirmAxesInUse_10();
		if (L_2)
		{
			goto IL_00e3;
		}
	}
	{
		// if (levelSelectPanels[index].levelUnlocked)
		LevelSelectPanelU5BU5D_t1747102860* L_3 = __this->get_levelSelectPanels_6();
		int32_t L_4 = __this->get_index_2();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		LevelSelectPanel_t2626311185 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		bool L_7 = L_6->get_levelUnlocked_3();
		if (!L_7)
		{
			goto IL_0098;
		}
	}
	{
		// confirmAxesInUse = true;
		__this->set_confirmAxesInUse_10((bool)1);
		// GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);
		AudioSource_t3935305588 * L_8 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_9 = __this->get_confirmSound_4();
		NullCheck(L_8);
		AudioSource_PlayOneShot_m2678069419(L_8, L_9, (0.6f), /*hidden argument*/NULL);
		// levelChanger.FadeToLevel(levelSelectPanels[index].levelName);
		LevelChanger_t225386971 * L_10 = __this->get_levelChanger_7();
		LevelSelectPanelU5BU5D_t1747102860* L_11 = __this->get_levelSelectPanels_6();
		int32_t L_12 = __this->get_index_2();
		NullCheck(L_11);
		int32_t L_13 = L_12;
		LevelSelectPanel_t2626311185 * L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		String_t* L_15 = L_14->get_levelName_2();
		NullCheck(L_10);
		LevelChanger_FadeToLevel_m2444280689(L_10, L_15, /*hidden argument*/NULL);
		// Debug.Log("Loading level: " + levelSelectPanels[index].levelName);
		LevelSelectPanelU5BU5D_t1747102860* L_16 = __this->get_levelSelectPanels_6();
		int32_t L_17 = __this->get_index_2();
		NullCheck(L_16);
		int32_t L_18 = L_17;
		LevelSelectPanel_t2626311185 * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_19);
		String_t* L_20 = L_19->get_levelName_2();
		String_t* L_21 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3862077153, L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0098:
	{
		// confirmAxesInUse = true;
		__this->set_confirmAxesInUse_10((bool)1);
		// GetComponent<AudioSource>().PlayOneShot(deniedSound, 0.6f);
		AudioSource_t3935305588 * L_22 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_23 = __this->get_deniedSound_5();
		NullCheck(L_22);
		AudioSource_PlayOneShot_m2678069419(L_22, L_23, (0.6f), /*hidden argument*/NULL);
		// Debug.Log("Level: " + levelSelectPanels[index].levelName + " is not available yet");
		LevelSelectPanelU5BU5D_t1747102860* L_24 = __this->get_levelSelectPanels_6();
		int32_t L_25 = __this->get_index_2();
		NullCheck(L_24);
		int32_t L_26 = L_25;
		LevelSelectPanel_t2626311185 * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_27);
		String_t* L_28 = L_27->get_levelName_2();
		String_t* L_29 = String_Concat_m3755062657(NULL /*static, unused*/, _stringLiteral1699060144, L_28, _stringLiteral2406229878, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00dc:
	{
		// confirmAxesInUse = false;
		__this->set_confirmAxesInUse_10((bool)0);
	}

IL_00e3:
	{
		// }
		return;
	}
}
// System.Void LevelSelect::DeselectAll()
extern "C"  void LevelSelect_DeselectAll_m630293501 (LevelSelect_t2669120976 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < levelSelectPanels.Length; i++)
		V_0 = 0;
		goto IL_0016;
	}

IL_0004:
	{
		// levelSelectPanels[i].selected = false;
		LevelSelectPanelU5BU5D_t1747102860* L_0 = __this->get_levelSelectPanels_6();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		LevelSelectPanel_t2626311185 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		L_3->set_selected_5((bool)0);
		// for (int i = 0; i < levelSelectPanels.Length; i++)
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0016:
	{
		// for (int i = 0; i < levelSelectPanels.Length; i++)
		int32_t L_5 = V_0;
		LevelSelectPanelU5BU5D_t1747102860* L_6 = __this->get_levelSelectPanels_6();
		NullCheck(L_6);
		if ((((int32_t)L_5) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_6)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void LevelSelect::.ctor()
extern "C"  void LevelSelect__ctor_m4242837570 (LevelSelect_t2669120976 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LevelSelectPanel::Start()
extern "C"  void LevelSelectPanel_Start_m2274833742 (LevelSelectPanel_t2626311185 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LevelSelectPanel_Start_m2274833742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// panelImage = GetComponent<Image>();
		Image_t2670269651 * L_0 = Component_GetComponent_TisImage_t2670269651_m980647750(__this, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		__this->set_panelImage_6(L_0);
		// levelStatus = transform.GetChild(0).GetComponent<Image>();
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3600365921 * L_2 = Transform_GetChild_m1092972975(L_1, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		Image_t2670269651 * L_3 = Component_GetComponent_TisImage_t2670269651_m980647750(L_2, /*hidden argument*/Component_GetComponent_TisImage_t2670269651_m980647750_RuntimeMethod_var);
		__this->set_levelStatus_7(L_3);
		// }
		return;
	}
}
// System.Void LevelSelectPanel::Update()
extern "C"  void LevelSelectPanel_Update_m2877862788 (LevelSelectPanel_t2626311185 * __this, const RuntimeMethod* method)
{
	{
		// if (!selected)
		bool L_0 = __this->get_selected_5();
		if (L_0)
		{
			goto IL_003a;
		}
	}
	{
		// if (!levelCompleted)
		bool L_1 = __this->get_levelCompleted_4();
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		// panelImage.sprite = panelSprites[0];
		Image_t2670269651 * L_2 = __this->get_panelImage_6();
		SpriteU5BU5D_t2581906349* L_3 = __this->get_panelSprites_8();
		NullCheck(L_3);
		int32_t L_4 = 0;
		Sprite_t280657092 * L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_2);
		Image_set_sprite_m2369174689(L_2, L_5, /*hidden argument*/NULL);
		// }
		goto IL_006a;
	}

IL_0025:
	{
		// panelImage.sprite = panelSprites[1];
		Image_t2670269651 * L_6 = __this->get_panelImage_6();
		SpriteU5BU5D_t2581906349* L_7 = __this->get_panelSprites_8();
		NullCheck(L_7);
		int32_t L_8 = 1;
		Sprite_t280657092 * L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_6);
		Image_set_sprite_m2369174689(L_6, L_9, /*hidden argument*/NULL);
		// }
		goto IL_006a;
	}

IL_003a:
	{
		// if (!levelCompleted)
		bool L_10 = __this->get_levelCompleted_4();
		if (L_10)
		{
			goto IL_0057;
		}
	}
	{
		// panelImage.sprite = panelSprites[4];
		Image_t2670269651 * L_11 = __this->get_panelImage_6();
		SpriteU5BU5D_t2581906349* L_12 = __this->get_panelSprites_8();
		NullCheck(L_12);
		int32_t L_13 = 4;
		Sprite_t280657092 * L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_11);
		Image_set_sprite_m2369174689(L_11, L_14, /*hidden argument*/NULL);
		// }
		goto IL_006a;
	}

IL_0057:
	{
		// panelImage.sprite = panelSprites[5];
		Image_t2670269651 * L_15 = __this->get_panelImage_6();
		SpriteU5BU5D_t2581906349* L_16 = __this->get_panelSprites_8();
		NullCheck(L_16);
		int32_t L_17 = 5;
		Sprite_t280657092 * L_18 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		NullCheck(L_15);
		Image_set_sprite_m2369174689(L_15, L_18, /*hidden argument*/NULL);
	}

IL_006a:
	{
		// if (!levelUnlocked)
		bool L_19 = __this->get_levelUnlocked_3();
		if (L_19)
		{
			goto IL_0086;
		}
	}
	{
		// levelStatus.sprite = panelSprites[2];
		Image_t2670269651 * L_20 = __this->get_levelStatus_7();
		SpriteU5BU5D_t2581906349* L_21 = __this->get_panelSprites_8();
		NullCheck(L_21);
		int32_t L_22 = 2;
		Sprite_t280657092 * L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_20);
		Image_set_sprite_m2369174689(L_20, L_23, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0086:
	{
		// levelStatus.sprite = panelSprites[3];
		Image_t2670269651 * L_24 = __this->get_levelStatus_7();
		SpriteU5BU5D_t2581906349* L_25 = __this->get_panelSprites_8();
		NullCheck(L_25);
		int32_t L_26 = 3;
		Sprite_t280657092 * L_27 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_24);
		Image_set_sprite_m2369174689(L_24, L_27, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LevelSelectPanel::.ctor()
extern "C"  void LevelSelectPanel__ctor_m1951999708 (LevelSelectPanel_t2626311185 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: Line
extern "C" void Line_t3796957314_marshal_pinvoke(const Line_t3796957314& unmarshaled, Line_t3796957314_marshaled_pinvoke& marshaled)
{
	marshaled.___gradient_1 = unmarshaled.get_gradient_1();
	marshaled.___yIntercept_2 = unmarshaled.get_yIntercept_2();
	marshaled.___pointOnLine_1_3 = unmarshaled.get_pointOnLine_1_3();
	marshaled.___pointOnLine_2_4 = unmarshaled.get_pointOnLine_2_4();
	marshaled.___gradientPerpendicular_5 = unmarshaled.get_gradientPerpendicular_5();
	marshaled.___approachSide_6 = static_cast<int32_t>(unmarshaled.get_approachSide_6());
}
extern "C" void Line_t3796957314_marshal_pinvoke_back(const Line_t3796957314_marshaled_pinvoke& marshaled, Line_t3796957314& unmarshaled)
{
	float unmarshaled_gradient_temp_0 = 0.0f;
	unmarshaled_gradient_temp_0 = marshaled.___gradient_1;
	unmarshaled.set_gradient_1(unmarshaled_gradient_temp_0);
	float unmarshaled_yIntercept_temp_1 = 0.0f;
	unmarshaled_yIntercept_temp_1 = marshaled.___yIntercept_2;
	unmarshaled.set_yIntercept_2(unmarshaled_yIntercept_temp_1);
	Vector2_t2156229523  unmarshaled_pointOnLine_1_temp_2;
	memset(&unmarshaled_pointOnLine_1_temp_2, 0, sizeof(unmarshaled_pointOnLine_1_temp_2));
	unmarshaled_pointOnLine_1_temp_2 = marshaled.___pointOnLine_1_3;
	unmarshaled.set_pointOnLine_1_3(unmarshaled_pointOnLine_1_temp_2);
	Vector2_t2156229523  unmarshaled_pointOnLine_2_temp_3;
	memset(&unmarshaled_pointOnLine_2_temp_3, 0, sizeof(unmarshaled_pointOnLine_2_temp_3));
	unmarshaled_pointOnLine_2_temp_3 = marshaled.___pointOnLine_2_4;
	unmarshaled.set_pointOnLine_2_4(unmarshaled_pointOnLine_2_temp_3);
	float unmarshaled_gradientPerpendicular_temp_4 = 0.0f;
	unmarshaled_gradientPerpendicular_temp_4 = marshaled.___gradientPerpendicular_5;
	unmarshaled.set_gradientPerpendicular_5(unmarshaled_gradientPerpendicular_temp_4);
	bool unmarshaled_approachSide_temp_5 = false;
	unmarshaled_approachSide_temp_5 = static_cast<bool>(marshaled.___approachSide_6);
	unmarshaled.set_approachSide_6(unmarshaled_approachSide_temp_5);
}
// Conversion method for clean up from marshalling of: Line
extern "C" void Line_t3796957314_marshal_pinvoke_cleanup(Line_t3796957314_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Line
extern "C" void Line_t3796957314_marshal_com(const Line_t3796957314& unmarshaled, Line_t3796957314_marshaled_com& marshaled)
{
	marshaled.___gradient_1 = unmarshaled.get_gradient_1();
	marshaled.___yIntercept_2 = unmarshaled.get_yIntercept_2();
	marshaled.___pointOnLine_1_3 = unmarshaled.get_pointOnLine_1_3();
	marshaled.___pointOnLine_2_4 = unmarshaled.get_pointOnLine_2_4();
	marshaled.___gradientPerpendicular_5 = unmarshaled.get_gradientPerpendicular_5();
	marshaled.___approachSide_6 = static_cast<int32_t>(unmarshaled.get_approachSide_6());
}
extern "C" void Line_t3796957314_marshal_com_back(const Line_t3796957314_marshaled_com& marshaled, Line_t3796957314& unmarshaled)
{
	float unmarshaled_gradient_temp_0 = 0.0f;
	unmarshaled_gradient_temp_0 = marshaled.___gradient_1;
	unmarshaled.set_gradient_1(unmarshaled_gradient_temp_0);
	float unmarshaled_yIntercept_temp_1 = 0.0f;
	unmarshaled_yIntercept_temp_1 = marshaled.___yIntercept_2;
	unmarshaled.set_yIntercept_2(unmarshaled_yIntercept_temp_1);
	Vector2_t2156229523  unmarshaled_pointOnLine_1_temp_2;
	memset(&unmarshaled_pointOnLine_1_temp_2, 0, sizeof(unmarshaled_pointOnLine_1_temp_2));
	unmarshaled_pointOnLine_1_temp_2 = marshaled.___pointOnLine_1_3;
	unmarshaled.set_pointOnLine_1_3(unmarshaled_pointOnLine_1_temp_2);
	Vector2_t2156229523  unmarshaled_pointOnLine_2_temp_3;
	memset(&unmarshaled_pointOnLine_2_temp_3, 0, sizeof(unmarshaled_pointOnLine_2_temp_3));
	unmarshaled_pointOnLine_2_temp_3 = marshaled.___pointOnLine_2_4;
	unmarshaled.set_pointOnLine_2_4(unmarshaled_pointOnLine_2_temp_3);
	float unmarshaled_gradientPerpendicular_temp_4 = 0.0f;
	unmarshaled_gradientPerpendicular_temp_4 = marshaled.___gradientPerpendicular_5;
	unmarshaled.set_gradientPerpendicular_5(unmarshaled_gradientPerpendicular_temp_4);
	bool unmarshaled_approachSide_temp_5 = false;
	unmarshaled_approachSide_temp_5 = static_cast<bool>(marshaled.___approachSide_6);
	unmarshaled.set_approachSide_6(unmarshaled_approachSide_temp_5);
}
// Conversion method for clean up from marshalling of: Line
extern "C" void Line_t3796957314_marshal_com_cleanup(Line_t3796957314_marshaled_com& marshaled)
{
}
// System.Void Line::.ctor(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Line__ctor_m1943978764 (Line_t3796957314 * __this, Vector2_t2156229523  ___pointOnLine0, Vector2_t2156229523  ___pointPerpendicularToLine1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Line__ctor_m1943978764_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// float dx = pointOnLine.x - pointPerpendicularToLine.x;
		Vector2_t2156229523  L_0 = ___pointOnLine0;
		float L_1 = L_0.get_x_0();
		Vector2_t2156229523  L_2 = ___pointPerpendicularToLine1;
		float L_3 = L_2.get_x_0();
		V_0 = ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3));
		// float dy = pointOnLine.y - pointPerpendicularToLine.y;
		Vector2_t2156229523  L_4 = ___pointOnLine0;
		float L_5 = L_4.get_y_1();
		Vector2_t2156229523  L_6 = ___pointPerpendicularToLine1;
		float L_7 = L_6.get_y_1();
		V_1 = ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7));
		// if (dx == 0)
		float L_8 = V_0;
		if ((!(((float)L_8) == ((float)(0.0f)))))
		{
			goto IL_0031;
		}
	}
	{
		// gradientPerpendicular = verticalLineGradient;
		__this->set_gradientPerpendicular_5((100000.0f));
		// }
		goto IL_003a;
	}

IL_0031:
	{
		// gradientPerpendicular = dy / dx;
		float L_9 = V_1;
		float L_10 = V_0;
		__this->set_gradientPerpendicular_5(((float)((float)L_9/(float)L_10)));
	}

IL_003a:
	{
		// if (gradientPerpendicular == 0)
		float L_11 = __this->get_gradientPerpendicular_5();
		if ((!(((float)L_11) == ((float)(0.0f)))))
		{
			goto IL_0054;
		}
	}
	{
		// gradient = verticalLineGradient;
		__this->set_gradient_1((100000.0f));
		// }
		goto IL_0066;
	}

IL_0054:
	{
		// gradient = -1 / gradientPerpendicular;
		float L_12 = __this->get_gradientPerpendicular_5();
		__this->set_gradient_1(((float)((float)(-1.0f)/(float)L_12)));
	}

IL_0066:
	{
		// yIntercept = pointOnLine.y - gradient * pointOnLine.x;
		Vector2_t2156229523  L_13 = ___pointOnLine0;
		float L_14 = L_13.get_y_1();
		float L_15 = __this->get_gradient_1();
		Vector2_t2156229523  L_16 = ___pointOnLine0;
		float L_17 = L_16.get_x_0();
		__this->set_yIntercept_2(((float)il2cpp_codegen_subtract((float)L_14, (float)((float)il2cpp_codegen_multiply((float)L_15, (float)L_17)))));
		// pointOnLine_1 = pointOnLine;
		Vector2_t2156229523  L_18 = ___pointOnLine0;
		__this->set_pointOnLine_1_3(L_18);
		// pointOnLine_2 = pointOnLine + new Vector2(1, gradient);
		Vector2_t2156229523  L_19 = ___pointOnLine0;
		float L_20 = __this->get_gradient_1();
		Vector2_t2156229523  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector2__ctor_m3970636864((&L_21), (1.0f), L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_22 = Vector2_op_Addition_m800700293(NULL /*static, unused*/, L_19, L_21, /*hidden argument*/NULL);
		__this->set_pointOnLine_2_4(L_22);
		// approachSide = false;
		__this->set_approachSide_6((bool)0);
		// approachSide = GetSide(pointPerpendicularToLine);
		Vector2_t2156229523  L_23 = ___pointPerpendicularToLine1;
		bool L_24 = Line_GetSide_m3671066560((Line_t3796957314 *)__this, L_23, /*hidden argument*/NULL);
		__this->set_approachSide_6(L_24);
		// }
		return;
	}
}
extern "C"  void Line__ctor_m1943978764_AdjustorThunk (RuntimeObject * __this, Vector2_t2156229523  ___pointOnLine0, Vector2_t2156229523  ___pointPerpendicularToLine1, const RuntimeMethod* method)
{
	Line_t3796957314 * _thisAdjusted = reinterpret_cast<Line_t3796957314 *>(__this + 1);
	Line__ctor_m1943978764(_thisAdjusted, ___pointOnLine0, ___pointPerpendicularToLine1, method);
}
// System.Boolean Line::GetSide(UnityEngine.Vector2)
extern "C"  bool Line_GetSide_m3671066560 (Line_t3796957314 * __this, Vector2_t2156229523  ___p0, const RuntimeMethod* method)
{
	{
		// return (p.x - pointOnLine_1.x) * (pointOnLine_2.y - pointOnLine_1.y)
		//      > (p.y - pointOnLine_1.y) * (pointOnLine_2.x - pointOnLine_1.x);
		Vector2_t2156229523  L_0 = ___p0;
		float L_1 = L_0.get_x_0();
		Vector2_t2156229523 * L_2 = __this->get_address_of_pointOnLine_1_3();
		float L_3 = L_2->get_x_0();
		Vector2_t2156229523 * L_4 = __this->get_address_of_pointOnLine_2_4();
		float L_5 = L_4->get_y_1();
		Vector2_t2156229523 * L_6 = __this->get_address_of_pointOnLine_1_3();
		float L_7 = L_6->get_y_1();
		Vector2_t2156229523  L_8 = ___p0;
		float L_9 = L_8.get_y_1();
		Vector2_t2156229523 * L_10 = __this->get_address_of_pointOnLine_1_3();
		float L_11 = L_10->get_y_1();
		Vector2_t2156229523 * L_12 = __this->get_address_of_pointOnLine_2_4();
		float L_13 = L_12->get_x_0();
		Vector2_t2156229523 * L_14 = __this->get_address_of_pointOnLine_1_3();
		float L_15 = L_14->get_x_0();
		return (bool)((((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_subtract((float)L_5, (float)L_7))))) > ((float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), (float)((float)il2cpp_codegen_subtract((float)L_13, (float)L_15))))))? 1 : 0);
	}
}
extern "C"  bool Line_GetSide_m3671066560_AdjustorThunk (RuntimeObject * __this, Vector2_t2156229523  ___p0, const RuntimeMethod* method)
{
	Line_t3796957314 * _thisAdjusted = reinterpret_cast<Line_t3796957314 *>(__this + 1);
	return Line_GetSide_m3671066560(_thisAdjusted, ___p0, method);
}
// System.Boolean Line::HasCrossedLine(UnityEngine.Vector2)
extern "C"  bool Line_HasCrossedLine_m2779751333 (Line_t3796957314 * __this, Vector2_t2156229523  ___p0, const RuntimeMethod* method)
{
	{
		// return GetSide(p) != approachSide;
		Vector2_t2156229523  L_0 = ___p0;
		bool L_1 = Line_GetSide_m3671066560((Line_t3796957314 *)__this, L_0, /*hidden argument*/NULL);
		bool L_2 = __this->get_approachSide_6();
		return (bool)((((int32_t)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
extern "C"  bool Line_HasCrossedLine_m2779751333_AdjustorThunk (RuntimeObject * __this, Vector2_t2156229523  ___p0, const RuntimeMethod* method)
{
	Line_t3796957314 * _thisAdjusted = reinterpret_cast<Line_t3796957314 *>(__this + 1);
	return Line_HasCrossedLine_m2779751333(_thisAdjusted, ___p0, method);
}
// System.Single Line::DistanceFromPoint(UnityEngine.Vector2)
extern "C"  float Line_DistanceFromPoint_m4294230093 (Line_t3796957314 * __this, Vector2_t2156229523  ___p0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Line_DistanceFromPoint_m4294230093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// float yInterceptPerpendicular = p.y - gradientPerpendicular * p.x;
		Vector2_t2156229523  L_0 = ___p0;
		float L_1 = L_0.get_y_1();
		float L_2 = __this->get_gradientPerpendicular_5();
		Vector2_t2156229523  L_3 = ___p0;
		float L_4 = L_3.get_x_0();
		// float intersectX = (yInterceptPerpendicular - yIntercept) / (gradient - gradientPerpendicular);
		float L_5 = __this->get_yIntercept_2();
		float L_6 = __this->get_gradient_1();
		float L_7 = __this->get_gradientPerpendicular_5();
		V_0 = ((float)((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)L_1, (float)((float)il2cpp_codegen_multiply((float)L_2, (float)L_4)))), (float)L_5))/(float)((float)il2cpp_codegen_subtract((float)L_6, (float)L_7))));
		// float intersectY = gradient * intersectX + yIntercept;
		float L_8 = __this->get_gradient_1();
		float L_9 = V_0;
		float L_10 = __this->get_yIntercept_2();
		V_1 = ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_8, (float)L_9)), (float)L_10));
		// return Vector2.Distance(p, new Vector2(intersectX, intersectY));
		Vector2_t2156229523  L_11 = ___p0;
		float L_12 = V_0;
		float L_13 = V_1;
		Vector2_t2156229523  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector2__ctor_m3970636864((&L_14), L_12, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		float L_15 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		return L_15;
	}
}
extern "C"  float Line_DistanceFromPoint_m4294230093_AdjustorThunk (RuntimeObject * __this, Vector2_t2156229523  ___p0, const RuntimeMethod* method)
{
	Line_t3796957314 * _thisAdjusted = reinterpret_cast<Line_t3796957314 *>(__this + 1);
	return Line_DistanceFromPoint_m4294230093(_thisAdjusted, ___p0, method);
}
// System.Void Line::DrawWithGizmos(System.Single)
extern "C"  void Line_DrawWithGizmos_m1752959317 (Line_t3796957314 * __this, float ___length0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Line_DrawWithGizmos_m1752959317_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		// Vector3 lineDir = new Vector3(1, 0, gradient).normalized;
		float L_0 = __this->get_gradient_1();
		Vector3_t3722313464  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m3353183577((&L_1), (1.0f), (0.0f), L_0, /*hidden argument*/NULL);
		V_2 = L_1;
		Vector3_t3722313464  L_2 = Vector3_get_normalized_m2454957984((Vector3_t3722313464 *)(&V_2), /*hidden argument*/NULL);
		V_0 = L_2;
		// Vector3 lineCentre = new Vector3(pointOnLine_1.x, 0, pointOnLine_1.y);
		Vector2_t2156229523 * L_3 = __this->get_address_of_pointOnLine_1_3();
		float L_4 = L_3->get_x_0();
		Vector2_t2156229523 * L_5 = __this->get_address_of_pointOnLine_1_3();
		float L_6 = L_5->get_y_1();
		Vector3__ctor_m3353183577((Vector3_t3722313464 *)(&V_1), L_4, (0.0f), L_6, /*hidden argument*/NULL);
		// Gizmos.DrawLine(lineCentre - lineDir * length / 2f, lineCentre + lineDir * length / 2f);
		Vector3_t3722313464  L_7 = V_1;
		Vector3_t3722313464  L_8 = V_0;
		float L_9 = ___length0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_10 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = Vector3_op_Division_m510815599(NULL /*static, unused*/, L_10, (2.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_12 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_7, L_11, /*hidden argument*/NULL);
		Vector3_t3722313464  L_13 = V_1;
		Vector3_t3722313464  L_14 = V_0;
		float L_15 = ___length0;
		Vector3_t3722313464  L_16 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		Vector3_t3722313464  L_17 = Vector3_op_Division_m510815599(NULL /*static, unused*/, L_16, (2.0f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_18 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_13, L_17, /*hidden argument*/NULL);
		Gizmos_DrawLine_m3273476787(NULL /*static, unused*/, L_12, L_18, /*hidden argument*/NULL);
		// }
		return;
	}
}
extern "C"  void Line_DrawWithGizmos_m1752959317_AdjustorThunk (RuntimeObject * __this, float ___length0, const RuntimeMethod* method)
{
	Line_t3796957314 * _thisAdjusted = reinterpret_cast<Line_t3796957314 *>(__this + 1);
	Line_DrawWithGizmos_m1752959317(_thisAdjusted, ___length0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Node::.ctor(System.Boolean,UnityEngine.Vector3,System.Int32,System.Int32,System.Int32)
extern "C"  void Node__ctor_m304489418 (Node_t2989995042 * __this, bool ____walkable0, Vector3_t3722313464  ____worldPosition1, int32_t ____gridX2, int32_t ____gridY3, int32_t ____penalty4, const RuntimeMethod* method)
{
	{
		// public Node(bool _walkable, Vector3 _worldPosition, int _gridX, int _gridY, int _penalty)
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		// walkable         = _walkable;
		bool L_0 = ____walkable0;
		__this->set_walkable_0(L_0);
		// worldPosition     = _worldPosition;
		Vector3_t3722313464  L_1 = ____worldPosition1;
		__this->set_worldPosition_1(L_1);
		// gridX             = _gridX;
		int32_t L_2 = ____gridX2;
		__this->set_gridX_2(L_2);
		// gridY             = _gridY;
		int32_t L_3 = ____gridY3;
		__this->set_gridY_3(L_3);
		// movementPenalty = _penalty;
		int32_t L_4 = ____penalty4;
		__this->set_movementPenalty_4(L_4);
		// }
		return;
	}
}
// System.Int32 Node::get_fCost()
extern "C"  int32_t Node_get_fCost_m1210297693 (Node_t2989995042 * __this, const RuntimeMethod* method)
{
	{
		// return gCost + hCost;
		int32_t L_0 = __this->get_gCost_5();
		int32_t L_1 = __this->get_hCost_6();
		return ((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1));
	}
}
// System.Int32 Node::get_HeapIndex()
extern "C"  int32_t Node_get_HeapIndex_m3352962969 (Node_t2989995042 * __this, const RuntimeMethod* method)
{
	{
		// return heapIndex;
		int32_t L_0 = __this->get_heapIndex_8();
		return L_0;
	}
}
// System.Void Node::set_HeapIndex(System.Int32)
extern "C"  void Node_set_HeapIndex_m2156278633 (Node_t2989995042 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// heapIndex = value;
		int32_t L_0 = ___value0;
		__this->set_heapIndex_8(L_0);
		// }
		return;
	}
}
// System.Int32 Node::CompareTo(Node)
extern "C"  int32_t Node_CompareTo_m339415574 (Node_t2989995042 * __this, Node_t2989995042 * ___nodeToCompare0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// int compare = fCost.CompareTo(nodeToCompare.fCost);
		int32_t L_0 = Node_get_fCost_m1210297693(__this, /*hidden argument*/NULL);
		V_1 = L_0;
		Node_t2989995042 * L_1 = ___nodeToCompare0;
		NullCheck(L_1);
		int32_t L_2 = Node_get_fCost_m1210297693(L_1, /*hidden argument*/NULL);
		int32_t L_3 = Int32_CompareTo_m4284770383((int32_t*)(&V_1), L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// if (compare == 0)
		int32_t L_4 = V_0;
		if (L_4)
		{
			goto IL_002a;
		}
	}
	{
		// compare = hCost.CompareTo(nodeToCompare.hCost);
		int32_t* L_5 = __this->get_address_of_hCost_6();
		Node_t2989995042 * L_6 = ___nodeToCompare0;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_hCost_6();
		int32_t L_8 = Int32_CompareTo_m4284770383((int32_t*)L_5, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_002a:
	{
		// return -compare;
		int32_t L_9 = V_0;
		return ((-L_9));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Path::.ctor(UnityEngine.Vector3[],UnityEngine.Vector3,System.Single,System.Single)
extern "C"  void Path__ctor_m3753908269 (Path_t2615110272 * __this, Vector3U5BU5D_t1718750761* ___wayPoints0, Vector3_t3722313464  ___startPos1, float ___turnDst2, float ___stoppingDst3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Path__ctor_m3753908269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2156229523  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2156229523  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t2156229523  V_6;
	memset(&V_6, 0, sizeof(V_6));
	int32_t V_7 = 0;
	Vector2_t2156229523  G_B4_0;
	memset(&G_B4_0, 0, sizeof(G_B4_0));
	{
		// public Path(Vector3[] wayPoints, Vector3 startPos, float turnDst, float stoppingDst)
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		// lookPoints = wayPoints;
		Vector3U5BU5D_t1718750761* L_0 = ___wayPoints0;
		__this->set_lookPoints_0(L_0);
		// turnBoundaries = new Line[lookPoints.Length];
		Vector3U5BU5D_t1718750761* L_1 = __this->get_lookPoints_0();
		NullCheck(L_1);
		__this->set_turnBoundaries_1(((LineU5BU5D_t3212499767*)SZArrayNew(LineU5BU5D_t3212499767_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))))));
		// finishLineIndex = turnBoundaries.Length - 1;
		LineU5BU5D_t3212499767* L_2 = __this->get_turnBoundaries_1();
		NullCheck(L_2);
		__this->set_finishLineIndex_2(((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length)))), (int32_t)1)));
		// Vector2 previousPoint = V3ToV2(startPos);
		Vector3_t3722313464  L_3 = ___startPos1;
		Vector2_t2156229523  L_4 = Path_V3ToV2_m3527951486(__this, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// for (int i = 0; i < lookPoints.Length; i++)
		V_2 = 0;
		goto IL_00a5;
	}

IL_003c:
	{
		// Vector2 currentPoint = V3ToV2(lookPoints[i]);
		Vector3U5BU5D_t1718750761* L_5 = __this->get_lookPoints_0();
		int32_t L_6 = V_2;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Vector3_t3722313464  L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		Vector2_t2156229523  L_9 = Path_V3ToV2_m3527951486(__this, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		// Vector2 dirToCurrentPoint = (currentPoint - previousPoint).normalized;
		Vector2_t2156229523  L_10 = V_3;
		Vector2_t2156229523  L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_12 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_6 = L_12;
		Vector2_t2156229523  L_13 = Vector2_get_normalized_m2683665860((Vector2_t2156229523 *)(&V_6), /*hidden argument*/NULL);
		V_4 = L_13;
		// Vector2 turnBoundaryPoint = (i == finishLineIndex) ? currentPoint : currentPoint - dirToCurrentPoint * turnDst;
		int32_t L_14 = V_2;
		int32_t L_15 = __this->get_finishLineIndex_2();
		if ((((int32_t)L_14) == ((int32_t)L_15)))
		{
			goto IL_007a;
		}
	}
	{
		Vector2_t2156229523  L_16 = V_3;
		Vector2_t2156229523  L_17 = V_4;
		float L_18 = ___turnDst2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_19 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		Vector2_t2156229523  L_20 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_16, L_19, /*hidden argument*/NULL);
		G_B4_0 = L_20;
		goto IL_007b;
	}

IL_007a:
	{
		Vector2_t2156229523  L_21 = V_3;
		G_B4_0 = L_21;
	}

IL_007b:
	{
		V_5 = G_B4_0;
		// turnBoundaries[i] = new Line(turnBoundaryPoint, previousPoint - dirToCurrentPoint * turnDst);
		LineU5BU5D_t3212499767* L_22 = __this->get_turnBoundaries_1();
		int32_t L_23 = V_2;
		Vector2_t2156229523  L_24 = V_5;
		Vector2_t2156229523  L_25 = V_0;
		Vector2_t2156229523  L_26 = V_4;
		float L_27 = ___turnDst2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_28 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		Vector2_t2156229523  L_29 = Vector2_op_Subtraction_m73004381(NULL /*static, unused*/, L_25, L_28, /*hidden argument*/NULL);
		Line_t3796957314  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Line__ctor_m1943978764((&L_30), L_24, L_29, /*hidden argument*/NULL);
		NullCheck(L_22);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(L_23), (Line_t3796957314 )L_30);
		// previousPoint = turnBoundaryPoint;
		Vector2_t2156229523  L_31 = V_5;
		V_0 = L_31;
		// for (int i = 0; i < lookPoints.Length; i++)
		int32_t L_32 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)1));
	}

IL_00a5:
	{
		// for (int i = 0; i < lookPoints.Length; i++)
		int32_t L_33 = V_2;
		Vector3U5BU5D_t1718750761* L_34 = __this->get_lookPoints_0();
		NullCheck(L_34);
		if ((((int32_t)L_33) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_34)->max_length)))))))
		{
			goto IL_003c;
		}
	}
	{
		// float dstFromEndPoint = 0;
		V_1 = (0.0f);
		// for (int i = lookPoints.Length - 1; i > 0; i--)
		Vector3U5BU5D_t1718750761* L_35 = __this->get_lookPoints_0();
		NullCheck(L_35);
		V_7 = ((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_35)->max_length)))), (int32_t)1));
		goto IL_00fc;
	}

IL_00c4:
	{
		// dstFromEndPoint += Vector3.Distance(lookPoints[i], lookPoints[i - 1]);
		float L_36 = V_1;
		Vector3U5BU5D_t1718750761* L_37 = __this->get_lookPoints_0();
		int32_t L_38 = V_7;
		NullCheck(L_37);
		int32_t L_39 = L_38;
		Vector3_t3722313464  L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		Vector3U5BU5D_t1718750761* L_41 = __this->get_lookPoints_0();
		int32_t L_42 = V_7;
		NullCheck(L_41);
		int32_t L_43 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_42, (int32_t)1));
		Vector3_t3722313464  L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		float L_45 = Vector3_Distance_m886789632(NULL /*static, unused*/, L_40, L_44, /*hidden argument*/NULL);
		V_1 = ((float)il2cpp_codegen_add((float)L_36, (float)L_45));
		// if (dstFromEndPoint > stoppingDst)
		float L_46 = V_1;
		float L_47 = ___stoppingDst3;
		if ((!(((float)L_46) > ((float)L_47))))
		{
			goto IL_00f6;
		}
	}
	{
		// slowDownIndex = i;
		int32_t L_48 = V_7;
		__this->set_slowDownIndex_3(L_48);
		// break;
		return;
	}

IL_00f6:
	{
		// for (int i = lookPoints.Length - 1; i > 0; i--)
		int32_t L_49 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_49, (int32_t)1));
	}

IL_00fc:
	{
		// for (int i = lookPoints.Length - 1; i > 0; i--)
		int32_t L_50 = V_7;
		if ((((int32_t)L_50) > ((int32_t)0)))
		{
			goto IL_00c4;
		}
	}
	{
		// }
		return;
	}
}
// UnityEngine.Vector2 Path::V3ToV2(UnityEngine.Vector3)
extern "C"  Vector2_t2156229523  Path_V3ToV2_m3527951486 (Path_t2615110272 * __this, Vector3_t3722313464  ___v30, const RuntimeMethod* method)
{
	{
		// return new Vector2(v3.x, v3.z);
		Vector3_t3722313464  L_0 = ___v30;
		float L_1 = L_0.get_x_1();
		Vector3_t3722313464  L_2 = ___v30;
		float L_3 = L_2.get_z_3();
		Vector2_t2156229523  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m3970636864((&L_4), L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void Path::DrawWithGizmos()
extern "C"  void Path_DrawWithGizmos_m2056486393 (Path_t2615110272 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Path_DrawWithGizmos_m2056486393_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_t1718750761* V_0 = NULL;
	int32_t V_1 = 0;
	LineU5BU5D_t3212499767* V_2 = NULL;
	Line_t3796957314  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		// Gizmos.color = Color.black;
		Color_t2555686324  L_0 = Color_get_black_m719512684(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m3399737545(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		// foreach (Vector3 p in lookPoints)
		Vector3U5BU5D_t1718750761* L_1 = __this->get_lookPoints_0();
		V_0 = L_1;
		V_1 = 0;
		goto IL_0034;
	}

IL_0015:
	{
		// foreach (Vector3 p in lookPoints)
		Vector3U5BU5D_t1718750761* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Vector3_t3722313464  L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		// Gizmos.DrawCube(p + Vector3.up, Vector3.one);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_6 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_7 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = Vector3_get_one_m1629952498(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_DrawCube_m530322281(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0034:
	{
		// foreach (Vector3 p in lookPoints)
		int32_t L_10 = V_1;
		Vector3U5BU5D_t1718750761* L_11 = V_0;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_11)->max_length)))))))
		{
			goto IL_0015;
		}
	}
	{
		// Gizmos.color = Color.white;
		Color_t2555686324  L_12 = Color_get_white_m332174077(NULL /*static, unused*/, /*hidden argument*/NULL);
		Gizmos_set_color_m3399737545(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		// foreach (Line l in turnBoundaries)
		LineU5BU5D_t3212499767* L_13 = __this->get_turnBoundaries_1();
		V_2 = L_13;
		V_1 = 0;
		goto IL_0067;
	}

IL_004f:
	{
		// foreach (Line l in turnBoundaries)
		LineU5BU5D_t3212499767* L_14 = V_2;
		int32_t L_15 = V_1;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Line_t3796957314  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_3 = L_17;
		// l.DrawWithGizmos(10);
		Line_DrawWithGizmos_m1752959317((Line_t3796957314 *)(&V_3), (10.0f), /*hidden argument*/NULL);
		int32_t L_18 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_0067:
	{
		// foreach (Line l in turnBoundaries)
		int32_t L_19 = V_1;
		LineU5BU5D_t3212499767* L_20 = V_2;
		NullCheck(L_20);
		if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_20)->max_length)))))))
		{
			goto IL_004f;
		}
	}
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Pathfinding::Awake()
extern "C"  void Pathfinding_Awake_m1184086700 (Pathfinding_t1696161914 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pathfinding_Awake_m1184086700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// grid = GetComponent<Grid>();
		Grid_t1081586032 * L_0 = Component_GetComponent_TisGrid_t1081586032_m3342251133(__this, /*hidden argument*/Component_GetComponent_TisGrid_t1081586032_m3342251133_RuntimeMethod_var);
		__this->set_grid_2(L_0);
		// }
		return;
	}
}
// System.Void Pathfinding::FindPath(PathRequest,System.Action`1<PathResult>)
extern "C"  void Pathfinding_FindPath_m1666106048 (Pathfinding_t1696161914 * __this, PathRequest_t2117613800  ___request0, Action_1_t1700480823 * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pathfinding_FindPath_m1666106048_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Stopwatch_t305734070 * V_0 = NULL;
	Vector3U5BU5D_t1718750761* V_1 = NULL;
	bool V_2 = false;
	Node_t2989995042 * V_3 = NULL;
	Node_t2989995042 * V_4 = NULL;
	Heap_1_t911941676 * V_5 = NULL;
	HashSet_1_t1554944516 * V_6 = NULL;
	Node_t2989995042 * V_7 = NULL;
	Enumerator_t2056346365  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Node_t2989995042 * V_9 = NULL;
	int32_t V_10 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// Stopwatch sw = new Stopwatch();
		Stopwatch_t305734070 * L_0 = (Stopwatch_t305734070 *)il2cpp_codegen_object_new(Stopwatch_t305734070_il2cpp_TypeInfo_var);
		Stopwatch__ctor_m2673586837(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		// sw.Start();
		Stopwatch_t305734070 * L_1 = V_0;
		NullCheck(L_1);
		Stopwatch_Start_m1142799187(L_1, /*hidden argument*/NULL);
		// Vector3[] waypoints = new Vector3[0];
		V_1 = ((Vector3U5BU5D_t1718750761*)SZArrayNew(Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var, (uint32_t)0));
		// bool pathSuccess = false;
		V_2 = (bool)0;
		// Node startNode = grid.NodeFromWorldPoint(request.pathStart);
		Grid_t1081586032 * L_2 = __this->get_grid_2();
		PathRequest_t2117613800  L_3 = ___request0;
		Vector3_t3722313464  L_4 = L_3.get_pathStart_0();
		NullCheck(L_2);
		Node_t2989995042 * L_5 = Grid_NodeFromWorldPoint_m3738070334(L_2, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		// Node targetNode = grid.NodeFromWorldPoint(request.pathEnd);
		Grid_t1081586032 * L_6 = __this->get_grid_2();
		PathRequest_t2117613800  L_7 = ___request0;
		Vector3_t3722313464  L_8 = L_7.get_pathEnd_1();
		NullCheck(L_6);
		Node_t2989995042 * L_9 = Grid_NodeFromWorldPoint_m3738070334(L_6, L_8, /*hidden argument*/NULL);
		V_4 = L_9;
		// if (startNode.walkable && targetNode.walkable)
		Node_t2989995042 * L_10 = V_3;
		NullCheck(L_10);
		bool L_11 = L_10->get_walkable_0();
		if (!L_11)
		{
			goto IL_018f;
		}
	}
	{
		Node_t2989995042 * L_12 = V_4;
		NullCheck(L_12);
		bool L_13 = L_12->get_walkable_0();
		if (!L_13)
		{
			goto IL_018f;
		}
	}
	{
		// Heap<Node> openSet = new Heap<Node>(grid.MaxSize);
		Grid_t1081586032 * L_14 = __this->get_grid_2();
		NullCheck(L_14);
		int32_t L_15 = Grid_get_MaxSize_m3127825365(L_14, /*hidden argument*/NULL);
		Heap_1_t911941676 * L_16 = (Heap_1_t911941676 *)il2cpp_codegen_object_new(Heap_1_t911941676_il2cpp_TypeInfo_var);
		Heap_1__ctor_m297432033(L_16, L_15, /*hidden argument*/Heap_1__ctor_m297432033_RuntimeMethod_var);
		V_5 = L_16;
		// HashSet<Node> closedSet = new HashSet<Node>();
		HashSet_1_t1554944516 * L_17 = (HashSet_1_t1554944516 *)il2cpp_codegen_object_new(HashSet_1_t1554944516_il2cpp_TypeInfo_var);
		HashSet_1__ctor_m3938937520(L_17, /*hidden argument*/HashSet_1__ctor_m3938937520_RuntimeMethod_var);
		V_6 = L_17;
		// openSet.Add(startNode);
		Heap_1_t911941676 * L_18 = V_5;
		Node_t2989995042 * L_19 = V_3;
		NullCheck(L_18);
		Heap_1_Add_m2689756234(L_18, L_19, /*hidden argument*/Heap_1_Add_m2689756234_RuntimeMethod_var);
		goto IL_0182;
	}

IL_0077:
	{
		// Node currentNode = openSet.RemoveFirst();
		Heap_1_t911941676 * L_20 = V_5;
		NullCheck(L_20);
		Node_t2989995042 * L_21 = Heap_1_RemoveFirst_m1685389745(L_20, /*hidden argument*/Heap_1_RemoveFirst_m1685389745_RuntimeMethod_var);
		V_7 = L_21;
		// closedSet.Add(currentNode);
		HashSet_1_t1554944516 * L_22 = V_6;
		Node_t2989995042 * L_23 = V_7;
		NullCheck(L_22);
		HashSet_1_Add_m87732039(L_22, L_23, /*hidden argument*/HashSet_1_Add_m87732039_RuntimeMethod_var);
		// if (currentNode == targetNode)
		Node_t2989995042 * L_24 = V_7;
		Node_t2989995042 * L_25 = V_4;
		if ((!(((RuntimeObject*)(Node_t2989995042 *)L_24) == ((RuntimeObject*)(Node_t2989995042 *)L_25))))
		{
			goto IL_00bc;
		}
	}
	{
		// sw.Stop();
		Stopwatch_t305734070 * L_26 = V_0;
		NullCheck(L_26);
		Stopwatch_Stop_m1583564474(L_26, /*hidden argument*/NULL);
		// print("Path found : " + sw.ElapsedMilliseconds + "ms");
		Stopwatch_t305734070 * L_27 = V_0;
		NullCheck(L_27);
		int64_t L_28 = Stopwatch_get_ElapsedMilliseconds_m1101465039(L_27, /*hidden argument*/NULL);
		int64_t L_29 = L_28;
		RuntimeObject * L_30 = Box(Int64_t3736567304_il2cpp_TypeInfo_var, &L_29);
		String_t* L_31 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral1946022780, L_30, _stringLiteral3455563715, /*hidden argument*/NULL);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		// pathSuccess = true;
		V_2 = (bool)1;
		// break;
		goto IL_018f;
	}

IL_00bc:
	{
		// foreach (Node neighbour in grid.GetNeighbours(currentNode))
		Grid_t1081586032 * L_32 = __this->get_grid_2();
		Node_t2989995042 * L_33 = V_7;
		NullCheck(L_32);
		List_1_t167102488 * L_34 = Grid_GetNeighbours_m4128505182(L_32, L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		Enumerator_t2056346365  L_35 = List_1_GetEnumerator_m3741205568(L_34, /*hidden argument*/List_1_GetEnumerator_m3741205568_RuntimeMethod_var);
		V_8 = L_35;
	}

IL_00d0:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0166;
		}

IL_00d5:
		{
			// foreach (Node neighbour in grid.GetNeighbours(currentNode))
			Node_t2989995042 * L_36 = Enumerator_get_Current_m3563945012((Enumerator_t2056346365 *)(&V_8), /*hidden argument*/Enumerator_get_Current_m3563945012_RuntimeMethod_var);
			V_9 = L_36;
			// if (!neighbour.walkable || closedSet.Contains(neighbour))
			Node_t2989995042 * L_37 = V_9;
			NullCheck(L_37);
			bool L_38 = L_37->get_walkable_0();
			if (!L_38)
			{
				goto IL_0166;
			}
		}

IL_00e7:
		{
			HashSet_1_t1554944516 * L_39 = V_6;
			Node_t2989995042 * L_40 = V_9;
			NullCheck(L_39);
			bool L_41 = HashSet_1_Contains_m1749837864(L_39, L_40, /*hidden argument*/HashSet_1_Contains_m1749837864_RuntimeMethod_var);
			if (L_41)
			{
				goto IL_0166;
			}
		}

IL_00f2:
		{
			// int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour) + neighbour.movementPenalty;
			Node_t2989995042 * L_42 = V_7;
			NullCheck(L_42);
			int32_t L_43 = L_42->get_gCost_5();
			Node_t2989995042 * L_44 = V_7;
			Node_t2989995042 * L_45 = V_9;
			int32_t L_46 = Pathfinding_GetDistance_m3683541895(__this, L_44, L_45, /*hidden argument*/NULL);
			Node_t2989995042 * L_47 = V_9;
			NullCheck(L_47);
			int32_t L_48 = L_47->get_movementPenalty_4();
			V_10 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_43, (int32_t)L_46)), (int32_t)L_48));
			// if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
			int32_t L_49 = V_10;
			Node_t2989995042 * L_50 = V_9;
			NullCheck(L_50);
			int32_t L_51 = L_50->get_gCost_5();
			if ((((int32_t)L_49) < ((int32_t)L_51)))
			{
				goto IL_0124;
			}
		}

IL_0119:
		{
			Heap_1_t911941676 * L_52 = V_5;
			Node_t2989995042 * L_53 = V_9;
			NullCheck(L_52);
			bool L_54 = Heap_1_Contains_m3137522954(L_52, L_53, /*hidden argument*/Heap_1_Contains_m3137522954_RuntimeMethod_var);
			if (L_54)
			{
				goto IL_0166;
			}
		}

IL_0124:
		{
			// neighbour.gCost = newMovementCostToNeighbour;
			Node_t2989995042 * L_55 = V_9;
			int32_t L_56 = V_10;
			NullCheck(L_55);
			L_55->set_gCost_5(L_56);
			// neighbour.hCost = GetDistance(neighbour, targetNode);
			Node_t2989995042 * L_57 = V_9;
			Node_t2989995042 * L_58 = V_9;
			Node_t2989995042 * L_59 = V_4;
			int32_t L_60 = Pathfinding_GetDistance_m3683541895(__this, L_58, L_59, /*hidden argument*/NULL);
			NullCheck(L_57);
			L_57->set_hCost_6(L_60);
			// neighbour.parent = currentNode;
			Node_t2989995042 * L_61 = V_9;
			Node_t2989995042 * L_62 = V_7;
			NullCheck(L_61);
			L_61->set_parent_7(L_62);
			// if (!openSet.Contains(neighbour))
			Heap_1_t911941676 * L_63 = V_5;
			Node_t2989995042 * L_64 = V_9;
			NullCheck(L_63);
			bool L_65 = Heap_1_Contains_m3137522954(L_63, L_64, /*hidden argument*/Heap_1_Contains_m3137522954_RuntimeMethod_var);
			if (L_65)
			{
				goto IL_015d;
			}
		}

IL_0152:
		{
			// openSet.Add(neighbour);
			Heap_1_t911941676 * L_66 = V_5;
			Node_t2989995042 * L_67 = V_9;
			NullCheck(L_66);
			Heap_1_Add_m2689756234(L_66, L_67, /*hidden argument*/Heap_1_Add_m2689756234_RuntimeMethod_var);
			// }
			goto IL_0166;
		}

IL_015d:
		{
			// openSet.UpdateItem(neighbour);
			Heap_1_t911941676 * L_68 = V_5;
			Node_t2989995042 * L_69 = V_9;
			NullCheck(L_68);
			Heap_1_UpdateItem_m3101882114(L_68, L_69, /*hidden argument*/Heap_1_UpdateItem_m3101882114_RuntimeMethod_var);
		}

IL_0166:
		{
			// foreach (Node neighbour in grid.GetNeighbours(currentNode))
			bool L_70 = Enumerator_MoveNext_m1693710810((Enumerator_t2056346365 *)(&V_8), /*hidden argument*/Enumerator_MoveNext_m1693710810_RuntimeMethod_var);
			if (L_70)
			{
				goto IL_00d5;
			}
		}

IL_0172:
		{
			IL2CPP_LEAVE(0x182, FINALLY_0174);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0174;
	}

FINALLY_0174:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m205031767((Enumerator_t2056346365 *)(&V_8), /*hidden argument*/Enumerator_Dispose_m205031767_RuntimeMethod_var);
		IL2CPP_END_FINALLY(372)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(372)
	{
		IL2CPP_JUMP_TBL(0x182, IL_0182)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0182:
	{
		// while (openSet.Count > 0)
		Heap_1_t911941676 * L_71 = V_5;
		NullCheck(L_71);
		int32_t L_72 = Heap_1_get_Count_m149300838(L_71, /*hidden argument*/Heap_1_get_Count_m149300838_RuntimeMethod_var);
		if ((((int32_t)L_72) > ((int32_t)0)))
		{
			goto IL_0077;
		}
	}

IL_018f:
	{
		// if (pathSuccess)
		bool L_73 = V_2;
		if (!L_73)
		{
			goto IL_01a2;
		}
	}
	{
		// waypoints = RetracePath(startNode, targetNode);
		Node_t2989995042 * L_74 = V_3;
		Node_t2989995042 * L_75 = V_4;
		Vector3U5BU5D_t1718750761* L_76 = Pathfinding_RetracePath_m276304888(__this, L_74, L_75, /*hidden argument*/NULL);
		V_1 = L_76;
		// pathSuccess = waypoints.Length > 0;
		Vector3U5BU5D_t1718750761* L_77 = V_1;
		NullCheck(L_77);
		V_2 = (bool)((!(((uint32_t)(((RuntimeArray *)L_77)->max_length)) <= ((uint32_t)0)))? 1 : 0);
	}

IL_01a2:
	{
		// callback(new PathResult(waypoints, pathSuccess, request.callback));
		Action_1_t1700480823 * L_78 = ___callback1;
		Vector3U5BU5D_t1718750761* L_79 = V_1;
		bool L_80 = V_2;
		PathRequest_t2117613800  L_81 = ___request0;
		Action_2_t1484661638 * L_82 = L_81.get_callback_2();
		PathResult_t1528013228  L_83;
		memset(&L_83, 0, sizeof(L_83));
		PathResult__ctor_m1065815828((&L_83), L_79, L_80, L_82, /*hidden argument*/NULL);
		NullCheck(L_78);
		Action_1_Invoke_m2478306080(L_78, L_83, /*hidden argument*/Action_1_Invoke_m2478306080_RuntimeMethod_var);
		// }
		return;
	}
}
// UnityEngine.Vector3[] Pathfinding::RetracePath(Node,Node)
extern "C"  Vector3U5BU5D_t1718750761* Pathfinding_RetracePath_m276304888 (Pathfinding_t1696161914 * __this, Node_t2989995042 * ___startNode0, Node_t2989995042 * ___endNode1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pathfinding_RetracePath_m276304888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t167102488 * V_0 = NULL;
	Node_t2989995042 * V_1 = NULL;
	{
		// List<Node> path = new List<Node>();
		List_1_t167102488 * L_0 = (List_1_t167102488 *)il2cpp_codegen_object_new(List_1_t167102488_il2cpp_TypeInfo_var);
		List_1__ctor_m3067334415(L_0, /*hidden argument*/List_1__ctor_m3067334415_RuntimeMethod_var);
		V_0 = L_0;
		// Node currentNode = endNode;
		Node_t2989995042 * L_1 = ___endNode1;
		V_1 = L_1;
		goto IL_0018;
	}

IL_000a:
	{
		// path.Add(currentNode);
		List_1_t167102488 * L_2 = V_0;
		Node_t2989995042 * L_3 = V_1;
		NullCheck(L_2);
		List_1_Add_m4134525979(L_2, L_3, /*hidden argument*/List_1_Add_m4134525979_RuntimeMethod_var);
		// currentNode = currentNode.parent;
		Node_t2989995042 * L_4 = V_1;
		NullCheck(L_4);
		Node_t2989995042 * L_5 = L_4->get_parent_7();
		V_1 = L_5;
	}

IL_0018:
	{
		// while (currentNode != startNode)
		Node_t2989995042 * L_6 = V_1;
		Node_t2989995042 * L_7 = ___startNode0;
		if ((!(((RuntimeObject*)(Node_t2989995042 *)L_6) == ((RuntimeObject*)(Node_t2989995042 *)L_7))))
		{
			goto IL_000a;
		}
	}
	{
		// Vector3[] waypoints = SimplifyPath(path);
		List_1_t167102488 * L_8 = V_0;
		Vector3U5BU5D_t1718750761* L_9 = Pathfinding_SimplifyPath_m3637710884(__this, L_8, /*hidden argument*/NULL);
		// Array.Reverse(waypoints);
		Vector3U5BU5D_t1718750761* L_10 = L_9;
		Array_Reverse_m3714848183(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_10, /*hidden argument*/NULL);
		// return waypoints;
		return L_10;
	}
}
// UnityEngine.Vector3[] Pathfinding::SimplifyPath(System.Collections.Generic.List`1<Node>)
extern "C"  Vector3U5BU5D_t1718750761* Pathfinding_SimplifyPath_m3637710884 (Pathfinding_t1696161914 * __this, List_1_t167102488 * ___path0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pathfinding_SimplifyPath_m3637710884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t899420910 * V_0 = NULL;
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Vector2_t2156229523  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	Vector2_t2156229523  G_B2_0;
	memset(&G_B2_0, 0, sizeof(G_B2_0));
	{
		// List<Vector3> waypoints = new List<Vector3>();
		List_1_t899420910 * L_0 = (List_1_t899420910 *)il2cpp_codegen_object_new(List_1_t899420910_il2cpp_TypeInfo_var);
		List_1__ctor_m1536473967(L_0, /*hidden argument*/List_1__ctor_m1536473967_RuntimeMethod_var);
		V_0 = L_0;
		// Vector2 directionOld = Vector2.zero;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_1 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_1;
		// for (int i = 1; i < path.Count; i++)
		V_2 = 1;
		goto IL_006d;
	}

IL_0010:
	{
		// Vector2 directionNew = new Vector2(path[i - 1].gridX - path[i].gridX, path[i - 1].gridY - path[i].gridY);
		List_1_t167102488 * L_2 = ___path0;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		Node_t2989995042 * L_4 = List_1_get_Item_m1948408520(L_2, ((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)1)), /*hidden argument*/List_1_get_Item_m1948408520_RuntimeMethod_var);
		NullCheck(L_4);
		int32_t L_5 = L_4->get_gridX_2();
		List_1_t167102488 * L_6 = ___path0;
		int32_t L_7 = V_2;
		NullCheck(L_6);
		Node_t2989995042 * L_8 = List_1_get_Item_m1948408520(L_6, L_7, /*hidden argument*/List_1_get_Item_m1948408520_RuntimeMethod_var);
		NullCheck(L_8);
		int32_t L_9 = L_8->get_gridX_2();
		List_1_t167102488 * L_10 = ___path0;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		Node_t2989995042 * L_12 = List_1_get_Item_m1948408520(L_10, ((int32_t)il2cpp_codegen_subtract((int32_t)L_11, (int32_t)1)), /*hidden argument*/List_1_get_Item_m1948408520_RuntimeMethod_var);
		NullCheck(L_12);
		int32_t L_13 = L_12->get_gridY_3();
		List_1_t167102488 * L_14 = ___path0;
		int32_t L_15 = V_2;
		NullCheck(L_14);
		Node_t2989995042 * L_16 = List_1_get_Item_m1948408520(L_14, L_15, /*hidden argument*/List_1_get_Item_m1948408520_RuntimeMethod_var);
		NullCheck(L_16);
		int32_t L_17 = L_16->get_gridY_3();
		Vector2_t2156229523  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector2__ctor_m3970636864((&L_18), (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)L_9))))), (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)L_17))))), /*hidden argument*/NULL);
		// if (directionNew != directionOld)
		Vector2_t2156229523  L_19 = L_18;
		Vector2_t2156229523  L_20 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		bool L_21 = Vector2_op_Inequality_m3858779880(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		G_B2_0 = L_19;
		if (!L_21)
		{
			G_B3_0 = L_19;
			goto IL_0068;
		}
	}
	{
		// waypoints.Add(path[i].worldPosition);
		List_1_t899420910 * L_22 = V_0;
		List_1_t167102488 * L_23 = ___path0;
		int32_t L_24 = V_2;
		NullCheck(L_23);
		Node_t2989995042 * L_25 = List_1_get_Item_m1948408520(L_23, L_24, /*hidden argument*/List_1_get_Item_m1948408520_RuntimeMethod_var);
		NullCheck(L_25);
		Vector3_t3722313464  L_26 = L_25->get_worldPosition_1();
		NullCheck(L_22);
		List_1_Add_m1524640104(L_22, L_26, /*hidden argument*/List_1_Add_m1524640104_RuntimeMethod_var);
		G_B3_0 = G_B2_0;
	}

IL_0068:
	{
		// directionOld = directionNew;
		V_1 = G_B3_0;
		// for (int i = 1; i < path.Count; i++)
		int32_t L_27 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)1));
	}

IL_006d:
	{
		// for (int i = 1; i < path.Count; i++)
		int32_t L_28 = V_2;
		List_1_t167102488 * L_29 = ___path0;
		NullCheck(L_29);
		int32_t L_30 = List_1_get_Count_m2928376997(L_29, /*hidden argument*/List_1_get_Count_m2928376997_RuntimeMethod_var);
		if ((((int32_t)L_28) < ((int32_t)L_30)))
		{
			goto IL_0010;
		}
	}
	{
		// return waypoints.ToArray();
		List_1_t899420910 * L_31 = V_0;
		NullCheck(L_31);
		Vector3U5BU5D_t1718750761* L_32 = List_1_ToArray_m1691996104(L_31, /*hidden argument*/List_1_ToArray_m1691996104_RuntimeMethod_var);
		return L_32;
	}
}
// System.Int32 Pathfinding::GetDistance(Node,Node)
extern "C"  int32_t Pathfinding_GetDistance_m3683541895 (Pathfinding_t1696161914 * __this, Node_t2989995042 * ___nodeA0, Node_t2989995042 * ___nodeB1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pathfinding_GetDistance_m3683541895_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
		Node_t2989995042 * L_0 = ___nodeA0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_gridX_2();
		Node_t2989995042 * L_2 = ___nodeB1;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_gridX_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_Abs_m2460432655(NULL /*static, unused*/, ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		// int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);
		Node_t2989995042 * L_5 = ___nodeA0;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_gridY_3();
		Node_t2989995042 * L_7 = ___nodeB1;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_gridY_3();
		int32_t L_9 = Mathf_Abs_m2460432655(NULL /*static, unused*/, ((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)L_8)), /*hidden argument*/NULL);
		V_1 = L_9;
		// if (dstX > dstY)
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) <= ((int32_t)L_11)))
		{
			goto IL_0036;
		}
	}
	{
		// return 14 * dstY + 10 * (dstX - dstY);
		int32_t L_12 = V_1;
		int32_t L_13 = V_0;
		int32_t L_14 = V_1;
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)14), (int32_t)L_12)), (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)10), (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)L_14))))));
	}

IL_0036:
	{
		// return 14 * dstX + 10 * (dstY - dstX);
		int32_t L_15 = V_0;
		int32_t L_16 = V_1;
		int32_t L_17 = V_0;
		return ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)14), (int32_t)L_15)), (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)10), (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_16, (int32_t)L_17))))));
	}
}
// System.Void Pathfinding::.ctor()
extern "C"  void Pathfinding__ctor_m790725036 (Pathfinding_t1696161914 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: PathRequest
extern "C" void PathRequest_t2117613800_marshal_pinvoke(const PathRequest_t2117613800& unmarshaled, PathRequest_t2117613800_marshaled_pinvoke& marshaled)
{
	marshaled.___pathStart_0 = unmarshaled.get_pathStart_0();
	marshaled.___pathEnd_1 = unmarshaled.get_pathEnd_1();
	marshaled.___callback_2 = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(unmarshaled.get_callback_2()));
}
extern "C" void PathRequest_t2117613800_marshal_pinvoke_back(const PathRequest_t2117613800_marshaled_pinvoke& marshaled, PathRequest_t2117613800& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PathRequest_t2117613800_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  unmarshaled_pathStart_temp_0;
	memset(&unmarshaled_pathStart_temp_0, 0, sizeof(unmarshaled_pathStart_temp_0));
	unmarshaled_pathStart_temp_0 = marshaled.___pathStart_0;
	unmarshaled.set_pathStart_0(unmarshaled_pathStart_temp_0);
	Vector3_t3722313464  unmarshaled_pathEnd_temp_1;
	memset(&unmarshaled_pathEnd_temp_1, 0, sizeof(unmarshaled_pathEnd_temp_1));
	unmarshaled_pathEnd_temp_1 = marshaled.___pathEnd_1;
	unmarshaled.set_pathEnd_1(unmarshaled_pathEnd_temp_1);
	unmarshaled.set_callback_2(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_2_t1484661638>(marshaled.___callback_2, Action_2_t1484661638_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: PathRequest
extern "C" void PathRequest_t2117613800_marshal_pinvoke_cleanup(PathRequest_t2117613800_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: PathRequest
extern "C" void PathRequest_t2117613800_marshal_com(const PathRequest_t2117613800& unmarshaled, PathRequest_t2117613800_marshaled_com& marshaled)
{
	marshaled.___pathStart_0 = unmarshaled.get_pathStart_0();
	marshaled.___pathEnd_1 = unmarshaled.get_pathEnd_1();
	marshaled.___callback_2 = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(unmarshaled.get_callback_2()));
}
extern "C" void PathRequest_t2117613800_marshal_com_back(const PathRequest_t2117613800_marshaled_com& marshaled, PathRequest_t2117613800& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PathRequest_t2117613800_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  unmarshaled_pathStart_temp_0;
	memset(&unmarshaled_pathStart_temp_0, 0, sizeof(unmarshaled_pathStart_temp_0));
	unmarshaled_pathStart_temp_0 = marshaled.___pathStart_0;
	unmarshaled.set_pathStart_0(unmarshaled_pathStart_temp_0);
	Vector3_t3722313464  unmarshaled_pathEnd_temp_1;
	memset(&unmarshaled_pathEnd_temp_1, 0, sizeof(unmarshaled_pathEnd_temp_1));
	unmarshaled_pathEnd_temp_1 = marshaled.___pathEnd_1;
	unmarshaled.set_pathEnd_1(unmarshaled_pathEnd_temp_1);
	unmarshaled.set_callback_2(il2cpp_codegen_marshal_function_ptr_to_delegate<Action_2_t1484661638>(marshaled.___callback_2, Action_2_t1484661638_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: PathRequest
extern "C" void PathRequest_t2117613800_marshal_com_cleanup(PathRequest_t2117613800_marshaled_com& marshaled)
{
}
// System.Void PathRequest::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,System.Action`2<UnityEngine.Vector3[],System.Boolean>)
extern "C"  void PathRequest__ctor_m1589651504 (PathRequest_t2117613800 * __this, Vector3_t3722313464  ____start0, Vector3_t3722313464  ____end1, Action_2_t1484661638 * ____callback2, const RuntimeMethod* method)
{
	{
		// pathStart = _start;
		Vector3_t3722313464  L_0 = ____start0;
		__this->set_pathStart_0(L_0);
		// pathEnd = _end;
		Vector3_t3722313464  L_1 = ____end1;
		__this->set_pathEnd_1(L_1);
		// callback = _callback;
		Action_2_t1484661638 * L_2 = ____callback2;
		__this->set_callback_2(L_2);
		// }
		return;
	}
}
extern "C"  void PathRequest__ctor_m1589651504_AdjustorThunk (RuntimeObject * __this, Vector3_t3722313464  ____start0, Vector3_t3722313464  ____end1, Action_2_t1484661638 * ____callback2, const RuntimeMethod* method)
{
	PathRequest_t2117613800 * _thisAdjusted = reinterpret_cast<PathRequest_t2117613800 *>(__this + 1);
	PathRequest__ctor_m1589651504(_thisAdjusted, ____start0, ____end1, ____callback2, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PathRequestManager::Awake()
extern "C"  void PathRequestManager_Awake_m1065215676 (PathRequestManager_t4163282249 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PathRequestManager_Awake_m1065215676_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// instance = this;
		((PathRequestManager_t4163282249_StaticFields*)il2cpp_codegen_static_fields_for(PathRequestManager_t4163282249_il2cpp_TypeInfo_var))->set_instance_3(__this);
		// pathfinding = GetComponent<Pathfinding>();
		Pathfinding_t1696161914 * L_0 = Component_GetComponent_TisPathfinding_t1696161914_m4281442259(__this, /*hidden argument*/Component_GetComponent_TisPathfinding_t1696161914_m4281442259_RuntimeMethod_var);
		__this->set_pathfinding_4(L_0);
		// }
		return;
	}
}
// System.Void PathRequestManager::Update()
extern "C"  void PathRequestManager_Update_m529797885 (PathRequestManager_t4163282249 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PathRequestManager_Update_m529797885_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Queue_1_t1374272722 * V_1 = NULL;
	bool V_2 = false;
	int32_t V_3 = 0;
	PathResult_t1528013228  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// if (results.Count > 0)
		Queue_1_t1374272722 * L_0 = __this->get_results_2();
		NullCheck(L_0);
		int32_t L_1 = Queue_1_get_Count_m3067217344(L_0, /*hidden argument*/Queue_1_get_Count_m3067217344_RuntimeMethod_var);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_006a;
		}
	}
	{
		// int itemsInQueue = results.Count;
		Queue_1_t1374272722 * L_2 = __this->get_results_2();
		NullCheck(L_2);
		int32_t L_3 = Queue_1_get_Count_m3067217344(L_2, /*hidden argument*/Queue_1_get_Count_m3067217344_RuntimeMethod_var);
		V_0 = L_3;
		// lock(results)
		Queue_1_t1374272722 * L_4 = __this->get_results_2();
		V_1 = L_4;
		V_2 = (bool)0;
	}

IL_0023:
	try
	{ // begin try (depth: 1)
		{
			Queue_1_t1374272722 * L_5 = V_1;
			Monitor_Enter_m984175629(NULL /*static, unused*/, L_5, (bool*)(&V_2), /*hidden argument*/NULL);
			// for (int i = 0; i < itemsInQueue; i++)
			V_3 = 0;
			goto IL_005a;
		}

IL_002f:
		{
			// PathResult result = results.Dequeue();
			Queue_1_t1374272722 * L_6 = __this->get_results_2();
			NullCheck(L_6);
			PathResult_t1528013228  L_7 = Queue_1_Dequeue_m920511108(L_6, /*hidden argument*/Queue_1_Dequeue_m920511108_RuntimeMethod_var);
			V_4 = L_7;
			// result.callback(result.path, result.success);
			PathResult_t1528013228  L_8 = V_4;
			Action_2_t1484661638 * L_9 = L_8.get_callback_2();
			PathResult_t1528013228  L_10 = V_4;
			Vector3U5BU5D_t1718750761* L_11 = L_10.get_path_0();
			PathResult_t1528013228  L_12 = V_4;
			bool L_13 = L_12.get_success_1();
			NullCheck(L_9);
			Action_2_Invoke_m2062908132(L_9, L_11, L_13, /*hidden argument*/Action_2_Invoke_m2062908132_RuntimeMethod_var);
			// for (int i = 0; i < itemsInQueue; i++)
			int32_t L_14 = V_3;
			V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
		}

IL_005a:
		{
			// for (int i = 0; i < itemsInQueue; i++)
			int32_t L_15 = V_3;
			int32_t L_16 = V_0;
			if ((((int32_t)L_15) < ((int32_t)L_16)))
			{
				goto IL_002f;
			}
		}

IL_005e:
		{
			// }
			IL2CPP_LEAVE(0x6A, FINALLY_0060);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		{
			bool L_17 = V_2;
			if (!L_17)
			{
				goto IL_0069;
			}
		}

IL_0063:
		{
			Queue_1_t1374272722 * L_18 = V_1;
			Monitor_Exit_m3585316909(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		}

IL_0069:
		{
			IL2CPP_END_FINALLY(96)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x6A, IL_006a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_006a:
	{
		// }
		return;
	}
}
// System.Void PathRequestManager::RequestPath(PathRequest)
extern "C"  void PathRequestManager_RequestPath_m3072144720 (RuntimeObject * __this /* static, unused */, PathRequest_t2117613800  ___request0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PathRequestManager_RequestPath_m3072144720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec__DisplayClass5_0_t786576806 * L_0 = (U3CU3Ec__DisplayClass5_0_t786576806 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass5_0_t786576806_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass5_0__ctor_m3126442359(L_0, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass5_0_t786576806 * L_1 = L_0;
		PathRequest_t2117613800  L_2 = ___request0;
		NullCheck(L_1);
		L_1->set_request_0(L_2);
		// ThreadStart threadStart = delegate
		// {
		//     instance.pathfinding.FindPath(request, instance.FinishedProcessingPath);
		// };
		intptr_t L_3 = (intptr_t)U3CU3Ec__DisplayClass5_0_U3CRequestPathU3Eb__0_m3190873041_RuntimeMethod_var;
		ThreadStart_t1006689297 * L_4 = (ThreadStart_t1006689297 *)il2cpp_codegen_object_new(ThreadStart_t1006689297_il2cpp_TypeInfo_var);
		ThreadStart__ctor_m3250019360(L_4, L_1, L_3, /*hidden argument*/NULL);
		// threadStart.Invoke();
		NullCheck(L_4);
		ThreadStart_Invoke_m1483406622(L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PathRequestManager::FinishedProcessingPath(PathResult)
extern "C"  void PathRequestManager_FinishedProcessingPath_m1346038950 (PathRequestManager_t4163282249 * __this, PathResult_t1528013228  ___result0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PathRequestManager_FinishedProcessingPath_m1346038950_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Queue_1_t1374272722 * V_0 = NULL;
	bool V_1 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// lock(results)
		Queue_1_t1374272722 * L_0 = __this->get_results_2();
		V_0 = L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		Queue_1_t1374272722 * L_1 = V_0;
		Monitor_Enter_m984175629(NULL /*static, unused*/, L_1, (bool*)(&V_1), /*hidden argument*/NULL);
		// results.Enqueue(result);
		Queue_1_t1374272722 * L_2 = __this->get_results_2();
		PathResult_t1528013228  L_3 = ___result0;
		NullCheck(L_2);
		Queue_1_Enqueue_m1828253350(L_2, L_3, /*hidden argument*/Queue_1_Enqueue_m1828253350_RuntimeMethod_var);
		// }
		IL2CPP_LEAVE(0x29, FINALLY_001f);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_001f;
	}

FINALLY_001f:
	{ // begin finally (depth: 1)
		{
			bool L_4 = V_1;
			if (!L_4)
			{
				goto IL_0028;
			}
		}

IL_0022:
		{
			Queue_1_t1374272722 * L_5 = V_0;
			Monitor_Exit_m3585316909(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		}

IL_0028:
		{
			IL2CPP_END_FINALLY(31)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(31)
	{
		IL2CPP_JUMP_TBL(0x29, IL_0029)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0029:
	{
		// }
		return;
	}
}
// System.Void PathRequestManager::.ctor()
extern "C"  void PathRequestManager__ctor_m2599481441 (PathRequestManager_t4163282249 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PathRequestManager__ctor_m2599481441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Queue<PathResult> results = new Queue<PathResult>();
		Queue_1_t1374272722 * L_0 = (Queue_1_t1374272722 *)il2cpp_codegen_object_new(Queue_1_t1374272722_il2cpp_TypeInfo_var);
		Queue_1__ctor_m534127087(L_0, /*hidden argument*/Queue_1__ctor_m534127087_RuntimeMethod_var);
		__this->set_results_2(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PathRequestManager/<>c__DisplayClass5_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass5_0__ctor_m3126442359 (U3CU3Ec__DisplayClass5_0_t786576806 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PathRequestManager/<>c__DisplayClass5_0::<RequestPath>b__0()
extern "C"  void U3CU3Ec__DisplayClass5_0_U3CRequestPathU3Eb__0_m3190873041 (U3CU3Ec__DisplayClass5_0_t786576806 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass5_0_U3CRequestPathU3Eb__0_m3190873041_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// instance.pathfinding.FindPath(request, instance.FinishedProcessingPath);
		PathRequestManager_t4163282249 * L_0 = ((PathRequestManager_t4163282249_StaticFields*)il2cpp_codegen_static_fields_for(PathRequestManager_t4163282249_il2cpp_TypeInfo_var))->get_instance_3();
		NullCheck(L_0);
		Pathfinding_t1696161914 * L_1 = L_0->get_pathfinding_4();
		PathRequest_t2117613800  L_2 = __this->get_request_0();
		PathRequestManager_t4163282249 * L_3 = ((PathRequestManager_t4163282249_StaticFields*)il2cpp_codegen_static_fields_for(PathRequestManager_t4163282249_il2cpp_TypeInfo_var))->get_instance_3();
		intptr_t L_4 = (intptr_t)PathRequestManager_FinishedProcessingPath_m1346038950_RuntimeMethod_var;
		Action_1_t1700480823 * L_5 = (Action_1_t1700480823 *)il2cpp_codegen_object_new(Action_1_t1700480823_il2cpp_TypeInfo_var);
		Action_1__ctor_m1884953144(L_5, L_3, L_4, /*hidden argument*/Action_1__ctor_m1884953144_RuntimeMethod_var);
		NullCheck(L_1);
		Pathfinding_FindPath_m1666106048(L_1, L_2, L_5, /*hidden argument*/NULL);
		// };
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: PathResult
extern "C" void PathResult_t1528013228_marshal_pinvoke(const PathResult_t1528013228& unmarshaled, PathResult_t1528013228_marshaled_pinvoke& marshaled)
{
	Exception_t* ___path_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'path' of type 'PathResult'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___path_0Exception, NULL, NULL);
}
extern "C" void PathResult_t1528013228_marshal_pinvoke_back(const PathResult_t1528013228_marshaled_pinvoke& marshaled, PathResult_t1528013228& unmarshaled)
{
	Exception_t* ___path_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'path' of type 'PathResult'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___path_0Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: PathResult
extern "C" void PathResult_t1528013228_marshal_pinvoke_cleanup(PathResult_t1528013228_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: PathResult
extern "C" void PathResult_t1528013228_marshal_com(const PathResult_t1528013228& unmarshaled, PathResult_t1528013228_marshaled_com& marshaled)
{
	Exception_t* ___path_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'path' of type 'PathResult'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___path_0Exception, NULL, NULL);
}
extern "C" void PathResult_t1528013228_marshal_com_back(const PathResult_t1528013228_marshaled_com& marshaled, PathResult_t1528013228& unmarshaled)
{
	Exception_t* ___path_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'path' of type 'PathResult'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___path_0Exception, NULL, NULL);
}
// Conversion method for clean up from marshalling of: PathResult
extern "C" void PathResult_t1528013228_marshal_com_cleanup(PathResult_t1528013228_marshaled_com& marshaled)
{
}
// System.Void PathResult::.ctor(UnityEngine.Vector3[],System.Boolean,System.Action`2<UnityEngine.Vector3[],System.Boolean>)
extern "C"  void PathResult__ctor_m1065815828 (PathResult_t1528013228 * __this, Vector3U5BU5D_t1718750761* ___path0, bool ___success1, Action_2_t1484661638 * ___callback2, const RuntimeMethod* method)
{
	{
		// this.path = path;
		Vector3U5BU5D_t1718750761* L_0 = ___path0;
		__this->set_path_0(L_0);
		// this.success = success;
		bool L_1 = ___success1;
		__this->set_success_1(L_1);
		// this.callback = callback;
		Action_2_t1484661638 * L_2 = ___callback2;
		__this->set_callback_2(L_2);
		// }
		return;
	}
}
extern "C"  void PathResult__ctor_m1065815828_AdjustorThunk (RuntimeObject * __this, Vector3U5BU5D_t1718750761* ___path0, bool ___success1, Action_2_t1484661638 * ___callback2, const RuntimeMethod* method)
{
	PathResult_t1528013228 * _thisAdjusted = reinterpret_cast<PathResult_t1528013228 *>(__this + 1);
	PathResult__ctor_m1065815828(_thisAdjusted, ___path0, ___success1, ___callback2, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PauseMenu::Start()
extern "C"  void PauseMenu_Start_m517218302 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PauseMenu_Start_m517218302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// index = 0;
		__this->set_index_5(0);
		// DeselectAll();
		PauseMenu_DeselectAll_m424026074(__this, /*hidden argument*/NULL);
		// options[index].GetComponent<Image>().sprite = optionBox[1];
		GameObjectU5BU5D_t3328599146* L_0 = __this->get_options_8();
		int32_t L_1 = __this->get_index_5();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_t1113636619 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Image_t2670269651 * L_4 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_3, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_5 = __this->get_optionBox_9();
		NullCheck(L_5);
		int32_t L_6 = 1;
		Sprite_t280657092 * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_4);
		Image_set_sprite_m2369174689(L_4, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PauseMenu::Update()
extern "C"  void PauseMenu_Update_m1844139191 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method)
{
	{
		// DetectInput();
		PauseMenu_DetectInput_m2353324690(__this, /*hidden argument*/NULL);
		// WhenPaused();
		PauseMenu_WhenPaused_m3668710605(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PauseMenu::DetectInput()
extern "C"  void PauseMenu_DetectInput_m2353324690 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PauseMenu_DetectInput_m2353324690_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Input.GetAxisRaw("Cancel") > 0 && pausingEnabled)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral1985564044, /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PauseMenu_t3916167947_il2cpp_TypeInfo_var);
		bool L_1 = ((PauseMenu_t3916167947_StaticFields*)il2cpp_codegen_static_fields_for(PauseMenu_t3916167947_il2cpp_TypeInfo_var))->get_pausingEnabled_3();
		if (!L_1)
		{
			goto IL_0043;
		}
	}
	{
		// if (!cancelAxesInUse)
		bool L_2 = __this->get_cancelAxesInUse_12();
		if (L_2)
		{
			goto IL_004a;
		}
	}
	{
		// if (!gameIsPaused)
		IL2CPP_RUNTIME_CLASS_INIT(PauseMenu_t3916167947_il2cpp_TypeInfo_var);
		bool L_3 = ((PauseMenu_t3916167947_StaticFields*)il2cpp_codegen_static_fields_for(PauseMenu_t3916167947_il2cpp_TypeInfo_var))->get_gameIsPaused_2();
		if (L_3)
		{
			goto IL_0035;
		}
	}
	{
		// cancelAxesInUse = true;
		__this->set_cancelAxesInUse_12((bool)1);
		// PauseGame();
		PauseMenu_PauseGame_m3294073383(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0035:
	{
		// cancelAxesInUse = true;
		__this->set_cancelAxesInUse_12((bool)1);
		// ResumeGame();
		PauseMenu_ResumeGame_m1593775081(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0043:
	{
		// cancelAxesInUse = false;
		__this->set_cancelAxesInUse_12((bool)0);
	}

IL_004a:
	{
		// }
		return;
	}
}
// System.Void PauseMenu::WhenPaused()
extern "C"  void PauseMenu_WhenPaused_m3668710605 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PauseMenu_WhenPaused_m3668710605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (gameIsPaused)
		IL2CPP_RUNTIME_CLASS_INIT(PauseMenu_t3916167947_il2cpp_TypeInfo_var);
		bool L_0 = ((PauseMenu_t3916167947_StaticFields*)il2cpp_codegen_static_fields_for(PauseMenu_t3916167947_il2cpp_TypeInfo_var))->get_gameIsPaused_2();
		if (!L_0)
		{
			goto IL_01cd;
		}
	}
	{
		// if (index >= options.Length)
		int32_t L_1 = __this->get_index_5();
		GameObjectU5BU5D_t3328599146* L_2 = __this->get_options_8();
		NullCheck(L_2);
		if ((((int32_t)L_1) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		// GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);
		AudioSource_t3935305588 * L_3 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_4 = __this->get_selectSound_6();
		NullCheck(L_3);
		AudioSource_PlayOneShot_m2678069419(L_3, L_4, (0.6f), /*hidden argument*/NULL);
		// index = 0;
		__this->set_index_5(0);
		// DeselectAll();
		PauseMenu_DeselectAll_m424026074(__this, /*hidden argument*/NULL);
		// options[index].GetComponent<Image>().sprite = optionBox[1];
		GameObjectU5BU5D_t3328599146* L_5 = __this->get_options_8();
		int32_t L_6 = __this->get_index_5();
		NullCheck(L_5);
		int32_t L_7 = L_6;
		GameObject_t1113636619 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		Image_t2670269651 * L_9 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_8, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_10 = __this->get_optionBox_9();
		NullCheck(L_10);
		int32_t L_11 = 1;
		Sprite_t280657092 * L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_9);
		Image_set_sprite_m2369174689(L_9, L_12, /*hidden argument*/NULL);
	}

IL_005c:
	{
		// if (index < 0)
		int32_t L_13 = __this->get_index_5();
		if ((((int32_t)L_13) >= ((int32_t)0)))
		{
			goto IL_00b0;
		}
	}
	{
		// GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);
		AudioSource_t3935305588 * L_14 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_15 = __this->get_selectSound_6();
		NullCheck(L_14);
		AudioSource_PlayOneShot_m2678069419(L_14, L_15, (0.6f), /*hidden argument*/NULL);
		// index = options.Length - 1;
		GameObjectU5BU5D_t3328599146* L_16 = __this->get_options_8();
		NullCheck(L_16);
		__this->set_index_5(((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_16)->max_length)))), (int32_t)1)));
		// DeselectAll();
		PauseMenu_DeselectAll_m424026074(__this, /*hidden argument*/NULL);
		// options[index].GetComponent<Image>().sprite = optionBox[1];
		GameObjectU5BU5D_t3328599146* L_17 = __this->get_options_8();
		int32_t L_18 = __this->get_index_5();
		NullCheck(L_17);
		int32_t L_19 = L_18;
		GameObject_t1113636619 * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck(L_20);
		Image_t2670269651 * L_21 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_20, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_22 = __this->get_optionBox_9();
		NullCheck(L_22);
		int32_t L_23 = 1;
		Sprite_t280657092 * L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_21);
		Image_set_sprite_m2369174689(L_21, L_24, /*hidden argument*/NULL);
	}

IL_00b0:
	{
		// if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetAxisRaw("DPadVertical") > 0 || Input.GetAxisRaw("Vertical") > 0)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_25 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)273), /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00de;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_26 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral767005334, /*hidden argument*/NULL);
		if ((((float)L_26) > ((float)(0.0f))))
		{
			goto IL_00de;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_27 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral2984908384, /*hidden argument*/NULL);
		if ((!(((float)L_27) > ((float)(0.0f)))))
		{
			goto IL_0138;
		}
	}

IL_00de:
	{
		// if (!upAxesInUse)
		bool L_28 = __this->get_upAxesInUse_10();
		if (L_28)
		{
			goto IL_013f;
		}
	}
	{
		// upAxesInUse = true;
		__this->set_upAxesInUse_10((bool)1);
		// GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);
		AudioSource_t3935305588 * L_29 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_30 = __this->get_selectSound_6();
		NullCheck(L_29);
		AudioSource_PlayOneShot_m2678069419(L_29, L_30, (0.6f), /*hidden argument*/NULL);
		// index -= 1;
		int32_t L_31 = __this->get_index_5();
		__this->set_index_5(((int32_t)il2cpp_codegen_subtract((int32_t)L_31, (int32_t)1)));
		// DeselectAll();
		PauseMenu_DeselectAll_m424026074(__this, /*hidden argument*/NULL);
		// options[index].GetComponent<Image>().sprite = optionBox[1];
		GameObjectU5BU5D_t3328599146* L_32 = __this->get_options_8();
		int32_t L_33 = __this->get_index_5();
		NullCheck(L_32);
		int32_t L_34 = L_33;
		GameObject_t1113636619 * L_35 = (L_32)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		NullCheck(L_35);
		Image_t2670269651 * L_36 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_35, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_37 = __this->get_optionBox_9();
		NullCheck(L_37);
		int32_t L_38 = 1;
		Sprite_t280657092 * L_39 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		NullCheck(L_36);
		Image_set_sprite_m2369174689(L_36, L_39, /*hidden argument*/NULL);
		// }
		goto IL_013f;
	}

IL_0138:
	{
		// upAxesInUse = false;
		__this->set_upAxesInUse_10((bool)0);
	}

IL_013f:
	{
		// if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetAxisRaw("DPadVertical") < 0 || Input.GetAxisRaw("Vertical") < 0)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_40 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)274), /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_016d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_41 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral767005334, /*hidden argument*/NULL);
		if ((((float)L_41) < ((float)(0.0f))))
		{
			goto IL_016d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_42 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral2984908384, /*hidden argument*/NULL);
		if ((!(((float)L_42) < ((float)(0.0f)))))
		{
			goto IL_01c6;
		}
	}

IL_016d:
	{
		// if (!downAxesInUse)
		bool L_43 = __this->get_downAxesInUse_11();
		if (L_43)
		{
			goto IL_01cd;
		}
	}
	{
		// downAxesInUse = true;
		__this->set_downAxesInUse_11((bool)1);
		// GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);
		AudioSource_t3935305588 * L_44 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_45 = __this->get_selectSound_6();
		NullCheck(L_44);
		AudioSource_PlayOneShot_m2678069419(L_44, L_45, (0.6f), /*hidden argument*/NULL);
		// index += 1;
		int32_t L_46 = __this->get_index_5();
		__this->set_index_5(((int32_t)il2cpp_codegen_add((int32_t)L_46, (int32_t)1)));
		// DeselectAll();
		PauseMenu_DeselectAll_m424026074(__this, /*hidden argument*/NULL);
		// options[index].GetComponent<Image>().sprite = optionBox[1];
		GameObjectU5BU5D_t3328599146* L_47 = __this->get_options_8();
		int32_t L_48 = __this->get_index_5();
		NullCheck(L_47);
		int32_t L_49 = L_48;
		GameObject_t1113636619 * L_50 = (L_47)->GetAt(static_cast<il2cpp_array_size_t>(L_49));
		NullCheck(L_50);
		Image_t2670269651 * L_51 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_50, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_52 = __this->get_optionBox_9();
		NullCheck(L_52);
		int32_t L_53 = 1;
		Sprite_t280657092 * L_54 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_53));
		NullCheck(L_51);
		Image_set_sprite_m2369174689(L_51, L_54, /*hidden argument*/NULL);
		// }
		return;
	}

IL_01c6:
	{
		// downAxesInUse = false;
		__this->set_downAxesInUse_11((bool)0);
	}

IL_01cd:
	{
		// }
		return;
	}
}
// System.Void PauseMenu::PauseGame()
extern "C"  void PauseMenu_PauseGame_m3294073383 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PauseMenu_PauseGame_m3294073383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// pauseMenuUI.SetActive(true);
		GameObject_t1113636619 * L_0 = __this->get_pauseMenuUI_4();
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)1, /*hidden argument*/NULL);
		// gameIsPaused = true;
		IL2CPP_RUNTIME_CLASS_INIT(PauseMenu_t3916167947_il2cpp_TypeInfo_var);
		((PauseMenu_t3916167947_StaticFields*)il2cpp_codegen_static_fields_for(PauseMenu_t3916167947_il2cpp_TypeInfo_var))->set_gameIsPaused_2((bool)1);
		// }
		return;
	}
}
// System.Void PauseMenu::ResumeGame()
extern "C"  void PauseMenu_ResumeGame_m1593775081 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PauseMenu_ResumeGame_m1593775081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// pauseMenuUI.SetActive(false);
		GameObject_t1113636619 * L_0 = __this->get_pauseMenuUI_4();
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)0, /*hidden argument*/NULL);
		// gameIsPaused = false;
		IL2CPP_RUNTIME_CLASS_INIT(PauseMenu_t3916167947_il2cpp_TypeInfo_var);
		((PauseMenu_t3916167947_StaticFields*)il2cpp_codegen_static_fields_for(PauseMenu_t3916167947_il2cpp_TypeInfo_var))->set_gameIsPaused_2((bool)0);
		// }
		return;
	}
}
// System.Void PauseMenu::DeselectAll()
extern "C"  void PauseMenu_DeselectAll_m424026074 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PauseMenu_DeselectAll_m424026074_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < options.Length; i++)
		V_0 = 0;
		goto IL_0022;
	}

IL_0004:
	{
		// options[i].GetComponent<Image>().sprite = optionBox[0];
		GameObjectU5BU5D_t3328599146* L_0 = __this->get_options_8();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_t1113636619 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Image_t2670269651 * L_4 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_3, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_5 = __this->get_optionBox_9();
		NullCheck(L_5);
		int32_t L_6 = 0;
		Sprite_t280657092 * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_4);
		Image_set_sprite_m2369174689(L_4, L_7, /*hidden argument*/NULL);
		// for (int i = 0; i < options.Length; i++)
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0022:
	{
		// for (int i = 0; i < options.Length; i++)
		int32_t L_9 = V_0;
		GameObjectU5BU5D_t3328599146* L_10 = __this->get_options_8();
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void PauseMenu::.ctor()
extern "C"  void PauseMenu__ctor_m3854158124 (PauseMenu_t3916167947 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseMenu::.cctor()
extern "C"  void PauseMenu__cctor_m1544404692 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PauseMenu__cctor_m1544404692_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static bool gameIsPaused = false;    // True if the game is paused
		((PauseMenu_t3916167947_StaticFields*)il2cpp_codegen_static_fields_for(PauseMenu_t3916167947_il2cpp_TypeInfo_var))->set_gameIsPaused_2((bool)0);
		// public static bool pausingEnabled = true;   // False when pausing is disabled
		((PauseMenu_t3916167947_StaticFields*)il2cpp_codegen_static_fields_for(PauseMenu_t3916167947_il2cpp_TypeInfo_var))->set_pausingEnabled_3((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerController::Start()
extern "C"  void PlayerController_Start_m1746698410 (PlayerController_t2064355688 * __this, const RuntimeMethod* method)
{
	{
		// ourCamera = Camera.main;
		Camera_t4157153871 * L_0 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_ourCamera_8(L_0);
		// }
		return;
	}
}
// System.Void PlayerController::Update()
extern "C"  void PlayerController_Update_m848427540 (PlayerController_t2064355688 * __this, const RuntimeMethod* method)
{
	{
		// MovePlayer();
		PlayerController_MovePlayer_m2560970386(__this, /*hidden argument*/NULL);
		// FireGun();
		PlayerController_FireGun_m2156033678(__this, /*hidden argument*/NULL);
		// UpdateReloadTime();
		PlayerController_UpdateReloadTime_m2370383035(__this, /*hidden argument*/NULL);
		// UseWeapon();
		PlayerController_UseWeapon_m3970795754(__this, /*hidden argument*/NULL);
		// UseShield();
		PlayerController_UseShield_m304736563(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerController::FireGun()
extern "C"  void PlayerController_FireGun_m2156033678 (PlayerController_t2064355688 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_FireGun_m2156033678_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Input.GetButton("Shoot") && !laserGunEnabled)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetButton_m2064261504(NULL /*static, unused*/, _stringLiteral1198113798, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		bool L_1 = __this->get_laserGunEnabled_3();
		if (L_1)
		{
			goto IL_0020;
		}
	}
	{
		// gun.Shoot();
		Gun_t783153271 * L_2 = __this->get_gun_5();
		NullCheck(L_2);
		Gun_Shoot_m3160136974(L_2, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0020:
	{
		// else if (Input.GetButtonDown("Shoot") && laserGunEnabled)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_3 = Input_GetButtonDown_m3074252807(NULL /*static, unused*/, _stringLiteral1198113798, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		bool L_4 = __this->get_laserGunEnabled_3();
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		// gun.ShootLaser();
		Gun_t783153271 * L_5 = __this->get_gun_5();
		NullCheck(L_5);
		Gun_ShootLaser_m1916414356(L_5, /*hidden argument*/NULL);
	}

IL_003f:
	{
		// }
		return;
	}
}
// System.Void PlayerController::UpdateReloadTime()
extern "C"  void PlayerController_UpdateReloadTime_m2370383035 (PlayerController_t2064355688 * __this, const RuntimeMethod* method)
{
	{
		// if (reloadTime >= 2.0f && !laserGunEnabled)
		float L_0 = __this->get_reloadTime_9();
		if ((!(((float)L_0) >= ((float)(2.0f)))))
		{
			goto IL_002c;
		}
	}
	{
		bool L_1 = __this->get_laserGunEnabled_3();
		if (L_1)
		{
			goto IL_002c;
		}
	}
	{
		// gun.rounds = 5;
		Gun_t783153271 * L_2 = __this->get_gun_5();
		NullCheck(L_2);
		L_2->set_rounds_9(5);
		// reloadTime = 0.0f;
		__this->set_reloadTime_9((0.0f));
	}

IL_002c:
	{
		// if (gun.uses > 10 && laserGunEnabled)
		Gun_t783153271 * L_3 = __this->get_gun_5();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_uses_10();
		if ((((int32_t)L_4) <= ((int32_t)((int32_t)10))))
		{
			goto IL_0083;
		}
	}
	{
		bool L_5 = __this->get_laserGunEnabled_3();
		if (!L_5)
		{
			goto IL_0083;
		}
	}
	{
		// inventory.RemoveItem(inventory.weapon);
		Inventory_t1050226016 * L_6 = __this->get_inventory_2();
		Inventory_t1050226016 * L_7 = __this->get_inventory_2();
		NullCheck(L_7);
		Item_t2953980098 * L_8 = L_7->get_weapon_6();
		NullCheck(L_6);
		Inventory_RemoveItem_m325135610(L_6, L_8, /*hidden argument*/NULL);
		// laserGunEnabled = false;
		__this->set_laserGunEnabled_3((bool)0);
		// gun.rounds = 0;
		Gun_t783153271 * L_9 = __this->get_gun_5();
		NullCheck(L_9);
		L_9->set_rounds_9(0);
		// gun.uses = 0;
		Gun_t783153271 * L_10 = __this->get_gun_5();
		NullCheck(L_10);
		L_10->set_uses_10(0);
		// reloadTime = 0.0f;
		__this->set_reloadTime_9((0.0f));
	}

IL_0083:
	{
		// reloadTime += Time.deltaTime;
		float L_11 = __this->get_reloadTime_9();
		float L_12 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_reloadTime_9(((float)il2cpp_codegen_add((float)L_11, (float)L_12)));
		// }
		return;
	}
}
// System.Void PlayerController::MovePlayer()
extern "C"  void PlayerController_MovePlayer_m2560970386 (PlayerController_t2064355688 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_MovePlayer_m2560970386_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	PlayerController_t2064355688 * G_B3_0 = NULL;
	PlayerController_t2064355688 * G_B2_0 = NULL;
	float G_B4_0 = 0.0f;
	PlayerController_t2064355688 * G_B4_1 = NULL;
	{
		// if (!PauseMenu.gameIsPaused)
		IL2CPP_RUNTIME_CLASS_INIT(PauseMenu_t3916167947_il2cpp_TypeInfo_var);
		bool L_0 = ((PauseMenu_t3916167947_StaticFields*)il2cpp_codegen_static_fields_for(PauseMenu_t3916167947_il2cpp_TypeInfo_var))->get_gameIsPaused_2();
		if (L_0)
		{
			goto IL_0154;
		}
	}
	{
		// moveSpeed = (Input.GetButton("Run")) ? 8.0f : 5.5f;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetButton_m2064261504(NULL /*static, unused*/, _stringLiteral4073296194, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if (L_1)
		{
			G_B3_0 = __this;
			goto IL_001e;
		}
	}
	{
		G_B4_0 = (5.5f);
		G_B4_1 = G_B2_0;
		goto IL_0023;
	}

IL_001e:
	{
		G_B4_0 = (8.0f);
		G_B4_1 = G_B3_0;
	}

IL_0023:
	{
		NullCheck(G_B4_1);
		G_B4_1->set_moveSpeed_6(G_B4_0);
		// if (Mathf.Abs(Input.GetAxisRaw("Vertical")) > 0)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_2 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral2984908384, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_3 = fabsf(L_2);
		if ((!(((float)L_3) > ((float)(0.0f)))))
		{
			goto IL_0072;
		}
	}
	{
		// transform.Translate(Vector3.forward * moveSpeed * Input.GetAxisRaw("Vertical") * Time.deltaTime);
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_5 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = __this->get_moveSpeed_6();
		Vector3_t3722313464  L_7 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_8 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral2984908384, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		float L_10 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_Translate_m1810197270(L_4, L_11, /*hidden argument*/NULL);
	}

IL_0072:
	{
		// if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) > 0)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_12 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral1828639942, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_13 = fabsf(L_12);
		if ((!(((float)L_13) > ((float)(0.0f)))))
		{
			goto IL_00bc;
		}
	}
	{
		// transform.Translate(Vector3.right * moveSpeed * Input.GetAxisRaw("Horizontal") * Time.deltaTime);
		Transform_t3600365921 * L_14 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_15 = Vector3_get_right_m1913784872(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_16 = __this->get_moveSpeed_6();
		Vector3_t3722313464  L_17 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_18 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral1828639942, /*hidden argument*/NULL);
		Vector3_t3722313464  L_19 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		float L_20 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_21 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_Translate_m1810197270(L_14, L_21, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		// if (Mathf.Abs(Input.GetAxisRaw("Turn")) > 0)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_22 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral3596229116, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_23 = fabsf(L_22);
		if ((!(((float)L_23) > ((float)(0.0f)))))
		{
			goto IL_0108;
		}
	}
	{
		// float axis = Input.GetAxisRaw("Turn");
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_24 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral3596229116, /*hidden argument*/NULL);
		V_0 = L_24;
		// transform.Rotate(Vector3.up * turnSpeed * axis * Time.deltaTime);
		Transform_t3600365921 * L_25 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_26 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_27 = __this->get_turnSpeed_7();
		Vector3_t3722313464  L_28 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		float L_29 = V_0;
		Vector3_t3722313464  L_30 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		float L_31 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_32 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_Rotate_m720511863(L_25, L_32, /*hidden argument*/NULL);
	}

IL_0108:
	{
		// if (Mathf.Abs(Input.GetAxisRaw("RightStickHorizontal")) > 0)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_33 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral2483366004, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_34 = fabsf(L_33);
		if ((!(((float)L_34) > ((float)(0.0f)))))
		{
			goto IL_0154;
		}
	}
	{
		// float axis = Input.GetAxisRaw("RightStickHorizontal");
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_35 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral2483366004, /*hidden argument*/NULL);
		V_1 = L_35;
		// transform.Rotate(Vector3.up * turnSpeed * axis * Time.deltaTime);
		Transform_t3600365921 * L_36 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_37 = Vector3_get_up_m3584168373(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_38 = __this->get_turnSpeed_7();
		Vector3_t3722313464  L_39 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_37, L_38, /*hidden argument*/NULL);
		float L_40 = V_1;
		Vector3_t3722313464  L_41 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_39, L_40, /*hidden argument*/NULL);
		float L_42 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_43 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_41, L_42, /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_Rotate_m720511863(L_36, L_43, /*hidden argument*/NULL);
	}

IL_0154:
	{
		// }
		return;
	}
}
// System.Void PlayerController::UseWeapon()
extern "C"  void PlayerController_UseWeapon_m3970795754 (PlayerController_t2064355688 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_UseWeapon_m3970795754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Input.GetButtonDown("EquipWeapon"))
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetButtonDown_m3074252807(NULL /*static, unused*/, _stringLiteral524066506, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		// if (inventory.weapon != null && inventory.weapon.name == "Laser Gun")
		Inventory_t1050226016 * L_1 = __this->get_inventory_2();
		NullCheck(L_1);
		Item_t2953980098 * L_2 = L_1->get_weapon_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0042;
		}
	}
	{
		Inventory_t1050226016 * L_4 = __this->get_inventory_2();
		NullCheck(L_4);
		Item_t2953980098 * L_5 = L_4->get_weapon_6();
		NullCheck(L_5);
		String_t* L_6 = L_5->get_name_2();
		bool L_7 = String_op_Equality_m920492651(NULL /*static, unused*/, L_6, _stringLiteral4119010799, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		// laserGunEnabled = true;
		__this->set_laserGunEnabled_3((bool)1);
	}

IL_0042:
	{
		// }
		return;
	}
}
// System.Void PlayerController::UseShield()
extern "C"  void PlayerController_UseShield_m304736563 (PlayerController_t2064355688 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerController_UseShield_m304736563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Input.GetButtonDown("EquipShield"))
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetButtonDown_m3074252807(NULL /*static, unused*/, _stringLiteral717525408, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		// if (inventory.shield != null && inventory.shield.name == "Laser Shield")
		Inventory_t1050226016 * L_1 = __this->get_inventory_2();
		NullCheck(L_1);
		Item_t2953980098 * L_2 = L_1->get_shield_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_2, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0042;
		}
	}
	{
		Inventory_t1050226016 * L_4 = __this->get_inventory_2();
		NullCheck(L_4);
		Item_t2953980098 * L_5 = L_4->get_shield_7();
		NullCheck(L_5);
		String_t* L_6 = L_5->get_name_2();
		bool L_7 = String_op_Equality_m920492651(NULL /*static, unused*/, L_6, _stringLiteral3040548699, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		// laserShieldEnabled = true;
		__this->set_laserShieldEnabled_4((bool)1);
	}

IL_0042:
	{
		// }
		return;
	}
}
// System.Void PlayerController::.ctor()
extern "C"  void PlayerController__ctor_m1333951952 (PlayerController_t2064355688 * __this, const RuntimeMethod* method)
{
	{
		// private float moveSpeed = 5.5f;                 // The speed at which the player moves
		__this->set_moveSpeed_6((5.5f));
		// private float turnSpeed = 120.0f;               // The speed at which the player turns
		__this->set_turnSpeed_7((120.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerHealthUI::Start()
extern "C"  void PlayerHealthUI_Start_m1212927940 (PlayerHealthUI_t636700963 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void PlayerHealthUI::Update()
extern "C"  void PlayerHealthUI_Update_m1728854074 (PlayerHealthUI_t636700963 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerHealthUI_Update_m1728854074_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// pHealthValue.text = "Player Health: " + pStats.pCurrentHealth.ToString();
		Text_t1901882714 * L_0 = __this->get_pHealthValue_3();
		PlayerStats_t2044123780 * L_1 = __this->get_pStats_2();
		NullCheck(L_1);
		int32_t* L_2 = L_1->get_address_of_pCurrentHealth_2();
		String_t* L_3 = Int32_ToString_m141394615((int32_t*)L_2, /*hidden argument*/NULL);
		String_t* L_4 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral833246398, L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_4);
		// }
		return;
	}
}
// System.Void PlayerHealthUI::.ctor()
extern "C"  void PlayerHealthUI__ctor_m3783794714 (PlayerHealthUI_t636700963 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerManager::Awake()
extern "C"  void PlayerManager_Awake_m3341273811 (PlayerManager_t1349889689 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerManager_Awake_m3341273811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// instance = this;
		((PlayerManager_t1349889689_StaticFields*)il2cpp_codegen_static_fields_for(PlayerManager_t1349889689_il2cpp_TypeInfo_var))->set_instance_2(__this);
		// }
		return;
	}
}
// System.Void PlayerManager::.ctor()
extern "C"  void PlayerManager__ctor_m1020197347 (PlayerManager_t1349889689 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerStats::Start()
extern "C"  void PlayerStats_Start_m1108282675 (PlayerStats_t2044123780 * __this, const RuntimeMethod* method)
{
	{
		// pCurrentHealth = pMaximumHealth;
		int32_t L_0 = __this->get_pMaximumHealth_3();
		__this->set_pCurrentHealth_2(L_0);
		// }
		return;
	}
}
// System.Void PlayerStats::Update()
extern "C"  void PlayerStats_Update_m4084213154 (PlayerStats_t2044123780 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerStats_Update_m4084213154_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (pCurrentHealth >= pMaximumHealth)
		int32_t L_0 = __this->get_pCurrentHealth_2();
		int32_t L_1 = __this->get_pMaximumHealth_3();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_001a;
		}
	}
	{
		// pCurrentHealth = pMaximumHealth;
		int32_t L_2 = __this->get_pMaximumHealth_3();
		__this->set_pCurrentHealth_2(L_2);
	}

IL_001a:
	{
		// if (pCurrentHealth <= 0)
		int32_t L_3 = __this->get_pCurrentHealth_2();
		if ((((int32_t)L_3) > ((int32_t)0)))
		{
			goto IL_0045;
		}
	}
	{
		// gameObject.SetActive(false);
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m796801857(L_4, (bool)0, /*hidden argument*/NULL);
		// Destroy(gameObject);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		// SceneManager.LoadScene(gameOverScene); //"Scene1");
		String_t* L_6 = __this->get_gameOverScene_4();
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0045:
	{
		// }
		return;
	}
}
// System.Void PlayerStats::.ctor()
extern "C"  void PlayerStats__ctor_m3090625065 (PlayerStats_t2044123780 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void RotatingLights::Start()
extern "C"  void RotatingLights_Start_m2584051933 (RotatingLights_t2402942693 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void RotatingLights::Update()
extern "C"  void RotatingLights_Update_m3387491593 (RotatingLights_t2402942693 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RotatingLights_Update_m3387491593_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (lights1 != null)
		GameObject_t1113636619 * L_0 = __this->get_lights1_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		// lights1.transform.Rotate(0.0f, rotationSpeed * Time.deltaTime, 0.0f);
		GameObject_t1113636619 * L_2 = __this->get_lights1_2();
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = GameObject_get_transform_m1369836730(L_2, /*hidden argument*/NULL);
		float L_4 = __this->get_rotationSpeed_4();
		float L_5 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_Rotate_m3172098886(L_3, (0.0f), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), (0.0f), /*hidden argument*/NULL);
	}

IL_0034:
	{
		// if (lights2 != null)
		GameObject_t1113636619 * L_6 = __this->get_lights2_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_6, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0069;
		}
	}
	{
		// lights2.transform.Rotate(0.0f, -rotationSpeed * Time.deltaTime, 0.0f);
		GameObject_t1113636619 * L_8 = __this->get_lights2_3();
		NullCheck(L_8);
		Transform_t3600365921 * L_9 = GameObject_get_transform_m1369836730(L_8, /*hidden argument*/NULL);
		float L_10 = __this->get_rotationSpeed_4();
		float L_11 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_Rotate_m3172098886(L_9, (0.0f), ((float)il2cpp_codegen_multiply((float)((-L_10)), (float)L_11)), (0.0f), /*hidden argument*/NULL);
	}

IL_0069:
	{
		// }
		return;
	}
}
// System.Void RotatingLights::.ctor()
extern "C"  void RotatingLights__ctor_m1955726543 (RotatingLights_t2402942693 * __this, const RuntimeMethod* method)
{
	{
		// public float rotationSpeed = 15.0f;
		__this->set_rotationSpeed_4((15.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SelectTitleOptions::Start()
extern "C"  void SelectTitleOptions_Start_m504681833 (SelectTitleOptions_t2341392585 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectTitleOptions_Start_m504681833_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// levelChanger = FindObjectOfType<LevelChanger>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		LevelChanger_t225386971 * L_0 = Object_FindObjectOfType_TisLevelChanger_t225386971_m2918287313(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisLevelChanger_t225386971_m2918287313_RuntimeMethod_var);
		__this->set_levelChanger_8(L_0);
		// index = 0;
		__this->set_index_2(0);
		// DeselectAll();
		SelectTitleOptions_DeselectAll_m2671487348(__this, /*hidden argument*/NULL);
		// options[index].GetComponent<Image>().sprite = titleOptionBox[1];
		GameObjectU5BU5D_t3328599146* L_1 = __this->get_options_6();
		int32_t L_2 = __this->get_index_2();
		NullCheck(L_1);
		int32_t L_3 = L_2;
		GameObject_t1113636619 * L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		Image_t2670269651 * L_5 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_4, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_6 = __this->get_titleOptionBox_7();
		NullCheck(L_6);
		int32_t L_7 = 1;
		Sprite_t280657092 * L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_5);
		Image_set_sprite_m2369174689(L_5, L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SelectTitleOptions::Update()
extern "C"  void SelectTitleOptions_Update_m368740719 (SelectTitleOptions_t2341392585 * __this, const RuntimeMethod* method)
{
	{
		// UpdateOptions();
		SelectTitleOptions_UpdateOptions_m3436397335(__this, /*hidden argument*/NULL);
		// SelectOptions();
		SelectTitleOptions_SelectOptions_m1091832534(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SelectTitleOptions::UpdateOptions()
extern "C"  void SelectTitleOptions_UpdateOptions_m3436397335 (SelectTitleOptions_t2341392585 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectTitleOptions_UpdateOptions_m3436397335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (index >= options.Length)
		int32_t L_0 = __this->get_index_2();
		GameObjectU5BU5D_t3328599146* L_1 = __this->get_options_6();
		NullCheck(L_1);
		if ((((int32_t)L_0) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1)->max_length)))))))
		{
			goto IL_0052;
		}
	}
	{
		// GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);
		AudioSource_t3935305588 * L_2 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_3 = __this->get_selectSound_3();
		NullCheck(L_2);
		AudioSource_PlayOneShot_m2678069419(L_2, L_3, (0.6f), /*hidden argument*/NULL);
		// index = 0;
		__this->set_index_2(0);
		// DeselectAll();
		SelectTitleOptions_DeselectAll_m2671487348(__this, /*hidden argument*/NULL);
		// options[index].GetComponent<Image>().sprite = titleOptionBox[1];
		GameObjectU5BU5D_t3328599146* L_4 = __this->get_options_6();
		int32_t L_5 = __this->get_index_2();
		NullCheck(L_4);
		int32_t L_6 = L_5;
		GameObject_t1113636619 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		Image_t2670269651 * L_8 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_7, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_9 = __this->get_titleOptionBox_7();
		NullCheck(L_9);
		int32_t L_10 = 1;
		Sprite_t280657092 * L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_8);
		Image_set_sprite_m2369174689(L_8, L_11, /*hidden argument*/NULL);
	}

IL_0052:
	{
		// if (index < 0)
		int32_t L_12 = __this->get_index_2();
		if ((((int32_t)L_12) >= ((int32_t)0)))
		{
			goto IL_00a6;
		}
	}
	{
		// GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);
		AudioSource_t3935305588 * L_13 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_14 = __this->get_selectSound_3();
		NullCheck(L_13);
		AudioSource_PlayOneShot_m2678069419(L_13, L_14, (0.6f), /*hidden argument*/NULL);
		// index = options.Length - 1;
		GameObjectU5BU5D_t3328599146* L_15 = __this->get_options_6();
		NullCheck(L_15);
		__this->set_index_2(((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_15)->max_length)))), (int32_t)1)));
		// DeselectAll();
		SelectTitleOptions_DeselectAll_m2671487348(__this, /*hidden argument*/NULL);
		// options[index].GetComponent<Image>().sprite = titleOptionBox[1];
		GameObjectU5BU5D_t3328599146* L_16 = __this->get_options_6();
		int32_t L_17 = __this->get_index_2();
		NullCheck(L_16);
		int32_t L_18 = L_17;
		GameObject_t1113636619 * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_19);
		Image_t2670269651 * L_20 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_19, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_21 = __this->get_titleOptionBox_7();
		NullCheck(L_21);
		int32_t L_22 = 1;
		Sprite_t280657092 * L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_20);
		Image_set_sprite_m2369174689(L_20, L_23, /*hidden argument*/NULL);
	}

IL_00a6:
	{
		// if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetAxisRaw("DPadHorizontal") > 0 || Input.GetAxisRaw("Horizontal") > 0)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_24 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)275), /*hidden argument*/NULL);
		if (L_24)
		{
			goto IL_00d4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_25 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral2219833165, /*hidden argument*/NULL);
		if ((((float)L_25) > ((float)(0.0f))))
		{
			goto IL_00d4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_26 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral1828639942, /*hidden argument*/NULL);
		if ((!(((float)L_26) > ((float)(0.0f)))))
		{
			goto IL_012e;
		}
	}

IL_00d4:
	{
		// if (!rightAxesInUse)
		bool L_27 = __this->get_rightAxesInUse_10();
		if (L_27)
		{
			goto IL_0135;
		}
	}
	{
		// rightAxesInUse = true;
		__this->set_rightAxesInUse_10((bool)1);
		// GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);
		AudioSource_t3935305588 * L_28 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_29 = __this->get_selectSound_3();
		NullCheck(L_28);
		AudioSource_PlayOneShot_m2678069419(L_28, L_29, (0.6f), /*hidden argument*/NULL);
		// index += 1;
		int32_t L_30 = __this->get_index_2();
		__this->set_index_2(((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1)));
		// DeselectAll();
		SelectTitleOptions_DeselectAll_m2671487348(__this, /*hidden argument*/NULL);
		// options[index].GetComponent<Image>().sprite = titleOptionBox[1];
		GameObjectU5BU5D_t3328599146* L_31 = __this->get_options_6();
		int32_t L_32 = __this->get_index_2();
		NullCheck(L_31);
		int32_t L_33 = L_32;
		GameObject_t1113636619 * L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		NullCheck(L_34);
		Image_t2670269651 * L_35 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_34, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_36 = __this->get_titleOptionBox_7();
		NullCheck(L_36);
		int32_t L_37 = 1;
		Sprite_t280657092 * L_38 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		NullCheck(L_35);
		Image_set_sprite_m2369174689(L_35, L_38, /*hidden argument*/NULL);
		// }
		goto IL_0135;
	}

IL_012e:
	{
		// rightAxesInUse = false;
		__this->set_rightAxesInUse_10((bool)0);
	}

IL_0135:
	{
		// if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetAxisRaw("DPadHorizontal") < 0 || Input.GetAxisRaw("Horizontal") < 0)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_39 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)276), /*hidden argument*/NULL);
		if (L_39)
		{
			goto IL_0163;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_40 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral2219833165, /*hidden argument*/NULL);
		if ((((float)L_40) < ((float)(0.0f))))
		{
			goto IL_0163;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_41 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral1828639942, /*hidden argument*/NULL);
		if ((!(((float)L_41) < ((float)(0.0f)))))
		{
			goto IL_01bc;
		}
	}

IL_0163:
	{
		// if (!leftAxesInUse)
		bool L_42 = __this->get_leftAxesInUse_9();
		if (L_42)
		{
			goto IL_01c3;
		}
	}
	{
		// leftAxesInUse = true;
		__this->set_leftAxesInUse_9((bool)1);
		// GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);
		AudioSource_t3935305588 * L_43 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_44 = __this->get_selectSound_3();
		NullCheck(L_43);
		AudioSource_PlayOneShot_m2678069419(L_43, L_44, (0.6f), /*hidden argument*/NULL);
		// index -= 1;
		int32_t L_45 = __this->get_index_2();
		__this->set_index_2(((int32_t)il2cpp_codegen_subtract((int32_t)L_45, (int32_t)1)));
		// DeselectAll();
		SelectTitleOptions_DeselectAll_m2671487348(__this, /*hidden argument*/NULL);
		// options[index].GetComponent<Image>().sprite = titleOptionBox[1];
		GameObjectU5BU5D_t3328599146* L_46 = __this->get_options_6();
		int32_t L_47 = __this->get_index_2();
		NullCheck(L_46);
		int32_t L_48 = L_47;
		GameObject_t1113636619 * L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		NullCheck(L_49);
		Image_t2670269651 * L_50 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_49, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_51 = __this->get_titleOptionBox_7();
		NullCheck(L_51);
		int32_t L_52 = 1;
		Sprite_t280657092 * L_53 = (L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
		NullCheck(L_50);
		Image_set_sprite_m2369174689(L_50, L_53, /*hidden argument*/NULL);
		// }
		return;
	}

IL_01bc:
	{
		// leftAxesInUse = false;
		__this->set_leftAxesInUse_9((bool)0);
	}

IL_01c3:
	{
		// }
		return;
	}
}
// System.Void SelectTitleOptions::SelectOptions()
extern "C"  void SelectTitleOptions_SelectOptions_m1091832534 (SelectTitleOptions_t2341392585 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectTitleOptions_SelectOptions_m1091832534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (Input.GetKeyDown(KeyCode.Space) || Input.GetAxisRaw("Submit") > 0)
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)32), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_1 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral1187062204, /*hidden argument*/NULL);
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_00db;
		}
	}

IL_001d:
	{
		// switch (index)
		int32_t L_2 = __this->get_index_2();
		V_0 = L_2;
		int32_t L_3 = V_0;
		switch (L_3)
		{
			case 0:
			{
				goto IL_0037;
			}
			case 1:
			{
				goto IL_006a;
			}
			case 2:
			{
				goto IL_00ab;
			}
		}
	}
	{
		return;
	}

IL_0037:
	{
		// if (!confirmAxesInUse)
		bool L_4 = __this->get_confirmAxesInUse_11();
		if (L_4)
		{
			goto IL_00e2;
		}
	}
	{
		// confirmAxesInUse = true;
		__this->set_confirmAxesInUse_11((bool)1);
		// GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);
		AudioSource_t3935305588 * L_5 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_6 = __this->get_confirmSound_4();
		NullCheck(L_5);
		AudioSource_PlayOneShot_m2678069419(L_5, L_6, (0.6f), /*hidden argument*/NULL);
		// Debug.Log("Continue");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral1055811909, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_006a:
	{
		// if (!confirmAxesInUse)
		bool L_7 = __this->get_confirmAxesInUse_11();
		if (L_7)
		{
			goto IL_00e2;
		}
	}
	{
		// confirmAxesInUse = true;
		__this->set_confirmAxesInUse_11((bool)1);
		// GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);
		AudioSource_t3935305588 * L_8 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_9 = __this->get_confirmSound_4();
		NullCheck(L_8);
		AudioSource_PlayOneShot_m2678069419(L_8, L_9, (0.6f), /*hidden argument*/NULL);
		// levelChanger.FadeToLevel(newGameSceneName);
		LevelChanger_t225386971 * L_10 = __this->get_levelChanger_8();
		String_t* L_11 = __this->get_newGameSceneName_5();
		NullCheck(L_10);
		LevelChanger_FadeToLevel_m2444280689(L_10, L_11, /*hidden argument*/NULL);
		// Debug.Log("New Game");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral515201583, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_00ab:
	{
		// if (!confirmAxesInUse)
		bool L_12 = __this->get_confirmAxesInUse_11();
		if (L_12)
		{
			goto IL_00e2;
		}
	}
	{
		// confirmAxesInUse = true;
		__this->set_confirmAxesInUse_11((bool)1);
		// GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);
		AudioSource_t3935305588 * L_13 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		AudioClip_t3680889665 * L_14 = __this->get_confirmSound_4();
		NullCheck(L_13);
		AudioSource_PlayOneShot_m2678069419(L_13, L_14, (0.6f), /*hidden argument*/NULL);
		// Debug.Log("Settings");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3588955337, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_00db:
	{
		// confirmAxesInUse = false;
		__this->set_confirmAxesInUse_11((bool)0);
	}

IL_00e2:
	{
		// }
		return;
	}
}
// System.Void SelectTitleOptions::DeselectAll()
extern "C"  void SelectTitleOptions_DeselectAll_m2671487348 (SelectTitleOptions_t2341392585 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SelectTitleOptions_DeselectAll_m2671487348_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < options.Length; i++)
		V_0 = 0;
		goto IL_0022;
	}

IL_0004:
	{
		// options[i].GetComponent<Image>().sprite = titleOptionBox[0];
		GameObjectU5BU5D_t3328599146* L_0 = __this->get_options_6();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		GameObject_t1113636619 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Image_t2670269651 * L_4 = GameObject_GetComponent_TisImage_t2670269651_m2486712510(L_3, /*hidden argument*/GameObject_GetComponent_TisImage_t2670269651_m2486712510_RuntimeMethod_var);
		SpriteU5BU5D_t2581906349* L_5 = __this->get_titleOptionBox_7();
		NullCheck(L_5);
		int32_t L_6 = 0;
		Sprite_t280657092 * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_4);
		Image_set_sprite_m2369174689(L_4, L_7, /*hidden argument*/NULL);
		// for (int i = 0; i < options.Length; i++)
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0022:
	{
		// for (int i = 0; i < options.Length; i++)
		int32_t L_9 = V_0;
		GameObjectU5BU5D_t3328599146* L_10 = __this->get_options_6();
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_10)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void SelectTitleOptions::.ctor()
extern "C"  void SelectTitleOptions__ctor_m1346275337 (SelectTitleOptions_t2341392585 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Shield::Start()
extern "C"  void Shield_Start_m291065101 (Shield_t1860136854 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Shield::Update()
extern "C"  void Shield_Update_m4291634055 (Shield_t1860136854 * __this, const RuntimeMethod* method)
{
	{
		// transform.Rotate(0, 15.0f * Time.deltaTime, 0);
		Transform_t3600365921 * L_0 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		float L_1 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_Rotate_m3172098886(L_0, (0.0f), ((float)il2cpp_codegen_multiply((float)(15.0f), (float)L_1)), (0.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Shield::.ctor()
extern "C"  void Shield__ctor_m186127875 (Shield_t1860136854 * __this, const RuntimeMethod* method)
{
	{
		// public int uses = 10;                   // The number of uses the shield has before it breaks
		__this->set_uses_2(((int32_t)10));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SoftNormalsToVertexColor::OnDrawGizmos()
extern "C"  void SoftNormalsToVertexColor_OnDrawGizmos_m3858838007 (SoftNormalsToVertexColor_t279782399 * __this, const RuntimeMethod* method)
{
	{
		// if( generateNow ) {
		bool L_0 = __this->get_generateNow_4();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		// generateNow = false;
		__this->set_generateNow_4((bool)0);
		// TryGenerate();
		SoftNormalsToVertexColor_TryGenerate_m1356071037(__this, /*hidden argument*/NULL);
	}

IL_0015:
	{
		// }
		return;
	}
}
// System.Void SoftNormalsToVertexColor::Awake()
extern "C"  void SoftNormalsToVertexColor_Awake_m3045773727 (SoftNormalsToVertexColor_t279782399 * __this, const RuntimeMethod* method)
{
	{
		// if(generateOnAwake)
		bool L_0 = __this->get_generateOnAwake_3();
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		// TryGenerate();
		SoftNormalsToVertexColor_TryGenerate_m1356071037(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		// }
		return;
	}
}
// System.Void SoftNormalsToVertexColor::TryGenerate()
extern "C"  void SoftNormalsToVertexColor_TryGenerate_m1356071037 (SoftNormalsToVertexColor_t279782399 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftNormalsToVertexColor_TryGenerate_m1356071037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshFilter_t3523625662 * V_0 = NULL;
	{
		// MeshFilter mf = GetComponent<MeshFilter>();
		MeshFilter_t3523625662 * L_0 = Component_GetComponent_TisMeshFilter_t3523625662_m1718783796(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3523625662_m1718783796_RuntimeMethod_var);
		V_0 = L_0;
		// if( mf == null ) {
		MeshFilter_t3523625662 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		// Debug.LogError( "MeshFilter missing on the vertex color generator", gameObject );
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m1665621915(NULL /*static, unused*/, _stringLiteral3233433636, L_3, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_0021:
	{
		// if( mf.sharedMesh == null ) {
		MeshFilter_t3523625662 * L_4 = V_0;
		NullCheck(L_4);
		Mesh_t3648964284 * L_5 = MeshFilter_get_sharedMesh_m1726897210(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_5, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0040;
		}
	}
	{
		// Debug.LogError( "Assign a mesh to the MeshFilter before generating vertex colors", gameObject );
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m1665621915(NULL /*static, unused*/, _stringLiteral907351806, L_7, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_0040:
	{
		// Generate(mf.sharedMesh);
		MeshFilter_t3523625662 * L_8 = V_0;
		NullCheck(L_8);
		Mesh_t3648964284 * L_9 = MeshFilter_get_sharedMesh_m1726897210(L_8, /*hidden argument*/NULL);
		SoftNormalsToVertexColor_Generate_m3390156695(__this, L_9, /*hidden argument*/NULL);
		// Debug.Log("Vertex colors generated", gameObject);
		GameObject_t1113636619 * L_10 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m2288605041(NULL /*static, unused*/, _stringLiteral2904180494, L_10, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SoftNormalsToVertexColor::Generate(UnityEngine.Mesh)
extern "C"  void SoftNormalsToVertexColor_Generate_m3390156695 (SoftNormalsToVertexColor_t279782399 * __this, Mesh_t3648964284 * ___m0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SoftNormalsToVertexColor_Generate_m3390156695_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_t1718750761* V_0 = NULL;
	Vector3U5BU5D_t1718750761* V_1 = NULL;
	ColorU5BU5D_t941916413* V_2 = NULL;
	List_1_t1600127941 * V_3 = NULL;
	int32_t V_4 = 0;
	bool V_5 = false;
	Enumerator_t3489371818  V_6;
	memset(&V_6, 0, sizeof(V_6));
	List_1_t128053199 * V_7 = NULL;
	List_1_t128053199 * V_8 = NULL;
	List_1_t128053199 * V_9 = NULL;
	Vector3_t3722313464  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Enumerator_t2017297076  V_11;
	memset(&V_11, 0, sizeof(V_11));
	int32_t V_12 = 0;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// Vector3[] n = m.normals;
		Mesh_t3648964284 * L_0 = ___m0;
		NullCheck(L_0);
		Vector3U5BU5D_t1718750761* L_1 = Mesh_get_normals_m4099615978(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// Vector3[] v = m.vertices;
		Mesh_t3648964284 * L_2 = ___m0;
		NullCheck(L_2);
		Vector3U5BU5D_t1718750761* L_3 = Mesh_get_vertices_m3585684815(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		// Color[] colors = new Color[n.Length];
		Vector3U5BU5D_t1718750761* L_4 = V_0;
		NullCheck(L_4);
		V_2 = ((ColorU5BU5D_t941916413*)SZArrayNew(ColorU5BU5D_t941916413_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_4)->max_length))))));
		// List<List<int>> groups = new List<List<int>>();
		List_1_t1600127941 * L_5 = (List_1_t1600127941 *)il2cpp_codegen_object_new(List_1_t1600127941_il2cpp_TypeInfo_var);
		List_1__ctor_m612749431(L_5, /*hidden argument*/List_1__ctor_m612749431_RuntimeMethod_var);
		V_3 = L_5;
		// for( int i = 0; i < v.Length; i++ ) {        // Group verts at the same location
		V_4 = 0;
		goto IL_009e;
	}

IL_0022:
	{
		// bool added = false;
		V_5 = (bool)0;
		// foreach( List<int> group in groups ) {    // Add to exsisting group if possible
		List_1_t1600127941 * L_6 = V_3;
		NullCheck(L_6);
		Enumerator_t3489371818  L_7 = List_1_GetEnumerator_m784752458(L_6, /*hidden argument*/List_1_GetEnumerator_m784752458_RuntimeMethod_var);
		V_6 = L_7;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0063;
		}

IL_002f:
		{
			// foreach( List<int> group in groups ) {    // Add to exsisting group if possible
			List_1_t128053199 * L_8 = Enumerator_get_Current_m2870147329((Enumerator_t3489371818 *)(&V_6), /*hidden argument*/Enumerator_get_Current_m2870147329_RuntimeMethod_var);
			V_7 = L_8;
			// if( v[group[0]] == v[i] ) {
			Vector3U5BU5D_t1718750761* L_9 = V_1;
			List_1_t128053199 * L_10 = V_7;
			NullCheck(L_10);
			int32_t L_11 = List_1_get_Item_m888956288(L_10, 0, /*hidden argument*/List_1_get_Item_m888956288_RuntimeMethod_var);
			NullCheck(L_9);
			int32_t L_12 = L_11;
			Vector3_t3722313464  L_13 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
			Vector3U5BU5D_t1718750761* L_14 = V_1;
			int32_t L_15 = V_4;
			NullCheck(L_14);
			int32_t L_16 = L_15;
			Vector3_t3722313464  L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
			IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
			bool L_18 = Vector3_op_Equality_m4231250055(NULL /*static, unused*/, L_13, L_17, /*hidden argument*/NULL);
			if (!L_18)
			{
				goto IL_0063;
			}
		}

IL_0055:
		{
			// group.Add(i);
			List_1_t128053199 * L_19 = V_7;
			int32_t L_20 = V_4;
			NullCheck(L_19);
			List_1_Add_m2080863212(L_19, L_20, /*hidden argument*/List_1_Add_m2080863212_RuntimeMethod_var);
			// added = true;
			V_5 = (bool)1;
			// break;
			IL2CPP_LEAVE(0x7C, FINALLY_006e);
		}

IL_0063:
		{
			// foreach( List<int> group in groups ) {    // Add to exsisting group if possible
			bool L_21 = Enumerator_MoveNext_m1436172384((Enumerator_t3489371818 *)(&V_6), /*hidden argument*/Enumerator_MoveNext_m1436172384_RuntimeMethod_var);
			if (L_21)
			{
				goto IL_002f;
			}
		}

IL_006c:
		{
			IL2CPP_LEAVE(0x7C, FINALLY_006e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_006e;
	}

FINALLY_006e:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m28313383((Enumerator_t3489371818 *)(&V_6), /*hidden argument*/Enumerator_Dispose_m28313383_RuntimeMethod_var);
		IL2CPP_END_FINALLY(110)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(110)
	{
		IL2CPP_JUMP_TBL(0x7C, IL_007c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_007c:
	{
		// if( !added ) {                            // Create new group if necessary
		bool L_22 = V_5;
		if (L_22)
		{
			goto IL_0098;
		}
	}
	{
		// List<int> newList = new List<int>();
		List_1_t128053199 * L_23 = (List_1_t128053199 *)il2cpp_codegen_object_new(List_1_t128053199_il2cpp_TypeInfo_var);
		List_1__ctor_m1204004817(L_23, /*hidden argument*/List_1__ctor_m1204004817_RuntimeMethod_var);
		V_8 = L_23;
		// newList.Add( i );
		List_1_t128053199 * L_24 = V_8;
		int32_t L_25 = V_4;
		NullCheck(L_24);
		List_1_Add_m2080863212(L_24, L_25, /*hidden argument*/List_1_Add_m2080863212_RuntimeMethod_var);
		// groups.Add( newList );
		List_1_t1600127941 * L_26 = V_3;
		List_1_t128053199 * L_27 = V_8;
		NullCheck(L_26);
		List_1_Add_m3830006445(L_26, L_27, /*hidden argument*/List_1_Add_m3830006445_RuntimeMethod_var);
	}

IL_0098:
	{
		// for( int i = 0; i < v.Length; i++ ) {        // Group verts at the same location
		int32_t L_28 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)1));
	}

IL_009e:
	{
		// for( int i = 0; i < v.Length; i++ ) {        // Group verts at the same location
		int32_t L_29 = V_4;
		Vector3U5BU5D_t1718750761* L_30 = V_1;
		NullCheck(L_30);
		if ((((int32_t)L_29) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_30)->max_length)))))))
		{
			goto IL_0022;
		}
	}
	{
		// foreach( List<int> group in groups ) { // Calculate soft normals
		List_1_t1600127941 * L_31 = V_3;
		NullCheck(L_31);
		Enumerator_t3489371818  L_32 = List_1_GetEnumerator_m784752458(L_31, /*hidden argument*/List_1_GetEnumerator_m784752458_RuntimeMethod_var);
		V_6 = L_32;
	}

IL_00b0:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01f9;
		}

IL_00b5:
		{
			// foreach( List<int> group in groups ) { // Calculate soft normals
			List_1_t128053199 * L_33 = Enumerator_get_Current_m2870147329((Enumerator_t3489371818 *)(&V_6), /*hidden argument*/Enumerator_get_Current_m2870147329_RuntimeMethod_var);
			V_9 = L_33;
			// Vector3 avgNrm = Vector3.zero;
			IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
			Vector3_t3722313464  L_34 = Vector3_get_zero_m1409827619(NULL /*static, unused*/, /*hidden argument*/NULL);
			V_10 = L_34;
			// foreach( int i in group ) { // TODO: This can actually be improved. Averaging will not give the best outline.
			List_1_t128053199 * L_35 = V_9;
			NullCheck(L_35);
			Enumerator_t2017297076  L_36 = List_1_GetEnumerator_m2838255531(L_35, /*hidden argument*/List_1_GetEnumerator_m2838255531_RuntimeMethod_var);
			V_11 = L_36;
		}

IL_00ce:
		try
		{ // begin try (depth: 2)
			{
				goto IL_00ea;
			}

IL_00d0:
			{
				// foreach( int i in group ) { // TODO: This can actually be improved. Averaging will not give the best outline.
				int32_t L_37 = Enumerator_get_Current_m2612064142((Enumerator_t2017297076 *)(&V_11), /*hidden argument*/Enumerator_get_Current_m2612064142_RuntimeMethod_var);
				V_12 = L_37;
				// avgNrm += n[i];
				Vector3_t3722313464  L_38 = V_10;
				Vector3U5BU5D_t1718750761* L_39 = V_0;
				int32_t L_40 = V_12;
				NullCheck(L_39);
				int32_t L_41 = L_40;
				Vector3_t3722313464  L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
				IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
				Vector3_t3722313464  L_43 = Vector3_op_Addition_m779775034(NULL /*static, unused*/, L_38, L_42, /*hidden argument*/NULL);
				V_10 = L_43;
			}

IL_00ea:
			{
				// foreach( int i in group ) { // TODO: This can actually be improved. Averaging will not give the best outline.
				bool L_44 = Enumerator_MoveNext_m1050804954((Enumerator_t2017297076 *)(&V_11), /*hidden argument*/Enumerator_MoveNext_m1050804954_RuntimeMethod_var);
				if (L_44)
				{
					goto IL_00d0;
				}
			}

IL_00f3:
			{
				IL2CPP_LEAVE(0x103, FINALLY_00f5);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_00f5;
		}

FINALLY_00f5:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m222348240((Enumerator_t2017297076 *)(&V_11), /*hidden argument*/Enumerator_Dispose_m222348240_RuntimeMethod_var);
			IL2CPP_END_FINALLY(245)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(245)
		{
			IL2CPP_JUMP_TBL(0x103, IL_0103)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_0103:
		{
			// avgNrm.Normalize(); // Average normal done
			Vector3_Normalize_m914904454((Vector3_t3722313464 *)(&V_10), /*hidden argument*/NULL);
			// if( method == Method.AngularDeviation ) {
			int32_t L_45 = __this->get_method_2();
			if ((!(((uint32_t)L_45) == ((uint32_t)1))))
			{
				goto IL_01aa;
			}
		}

IL_0116:
		{
			// float avgDot = 0f; // Calculate deviation to alter length
			V_13 = (0.0f);
			// foreach( int i in group ) {
			List_1_t128053199 * L_46 = V_9;
			NullCheck(L_46);
			Enumerator_t2017297076  L_47 = List_1_GetEnumerator_m2838255531(L_46, /*hidden argument*/List_1_GetEnumerator_m2838255531_RuntimeMethod_var);
			V_11 = L_47;
		}

IL_0126:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0145;
			}

IL_0128:
			{
				// foreach( int i in group ) {
				int32_t L_48 = Enumerator_get_Current_m2612064142((Enumerator_t2017297076 *)(&V_11), /*hidden argument*/Enumerator_get_Current_m2612064142_RuntimeMethod_var);
				V_17 = L_48;
				// avgDot += Vector3.Dot( n[i], avgNrm );
				float L_49 = V_13;
				Vector3U5BU5D_t1718750761* L_50 = V_0;
				int32_t L_51 = V_17;
				NullCheck(L_50);
				int32_t L_52 = L_51;
				Vector3_t3722313464  L_53 = (L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_52));
				Vector3_t3722313464  L_54 = V_10;
				IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
				float L_55 = Vector3_Dot_m606404487(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
				V_13 = ((float)il2cpp_codegen_add((float)L_49, (float)L_55));
			}

IL_0145:
			{
				// foreach( int i in group ) {
				bool L_56 = Enumerator_MoveNext_m1050804954((Enumerator_t2017297076 *)(&V_11), /*hidden argument*/Enumerator_MoveNext_m1050804954_RuntimeMethod_var);
				if (L_56)
				{
					goto IL_0128;
				}
			}

IL_014e:
			{
				IL2CPP_LEAVE(0x15E, FINALLY_0150);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0150;
		}

FINALLY_0150:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m222348240((Enumerator_t2017297076 *)(&V_11), /*hidden argument*/Enumerator_Dispose_m222348240_RuntimeMethod_var);
			IL2CPP_END_FINALLY(336)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(336)
		{
			IL2CPP_JUMP_TBL(0x15E, IL_015e)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_015e:
		{
			// avgDot /= group.Count;
			float L_57 = V_13;
			List_1_t128053199 * L_58 = V_9;
			NullCheck(L_58);
			int32_t L_59 = List_1_get_Count_m361000296(L_58, /*hidden argument*/List_1_get_Count_m361000296_RuntimeMethod_var);
			V_13 = ((float)((float)L_57/(float)(((float)((float)L_59)))));
			// float angDeviation = Mathf.Acos( avgDot ) * Mathf.Rad2Deg;
			float L_60 = V_13;
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
			float L_61 = acosf(L_60);
			V_14 = ((float)il2cpp_codegen_multiply((float)L_61, (float)(57.29578f)));
			// float aAng = 180f - angDeviation - 90;
			float L_62 = V_14;
			V_15 = ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)(180.0f), (float)L_62)), (float)(90.0f)));
			// float lMult = 0.5f / Mathf.Sin( aAng * Mathf.Deg2Rad );  // 0.5f looks better empirically, but mathematically it should be 1f
			float L_63 = V_15;
			float L_64 = sinf(((float)il2cpp_codegen_multiply((float)L_63, (float)(0.0174532924f))));
			V_16 = ((float)((float)(0.5f)/(float)L_64));
			// avgNrm *= lMult;
			Vector3_t3722313464  L_65 = V_10;
			float L_66 = V_16;
			IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
			Vector3_t3722313464  L_67 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_65, L_66, /*hidden argument*/NULL);
			V_10 = L_67;
		}

IL_01aa:
		{
			// foreach( int i in group ) {
			List_1_t128053199 * L_68 = V_9;
			NullCheck(L_68);
			Enumerator_t2017297076  L_69 = List_1_GetEnumerator_m2838255531(L_68, /*hidden argument*/List_1_GetEnumerator_m2838255531_RuntimeMethod_var);
			V_11 = L_69;
		}

IL_01b3:
		try
		{ // begin try (depth: 2)
			{
				goto IL_01e0;
			}

IL_01b5:
			{
				// foreach( int i in group ) {
				int32_t L_70 = Enumerator_get_Current_m2612064142((Enumerator_t2017297076 *)(&V_11), /*hidden argument*/Enumerator_get_Current_m2612064142_RuntimeMethod_var);
				V_18 = L_70;
				// colors[i] = new Color( avgNrm.x, avgNrm.y, avgNrm.z ); // Save normals as colors
				ColorU5BU5D_t941916413* L_71 = V_2;
				int32_t L_72 = V_18;
				Vector3_t3722313464  L_73 = V_10;
				float L_74 = L_73.get_x_1();
				Vector3_t3722313464  L_75 = V_10;
				float L_76 = L_75.get_y_2();
				Vector3_t3722313464  L_77 = V_10;
				float L_78 = L_77.get_z_3();
				Color_t2555686324  L_79;
				memset(&L_79, 0, sizeof(L_79));
				Color__ctor_m286683560((&L_79), L_74, L_76, L_78, /*hidden argument*/NULL);
				NullCheck(L_71);
				(L_71)->SetAt(static_cast<il2cpp_array_size_t>(L_72), (Color_t2555686324 )L_79);
			}

IL_01e0:
			{
				// foreach( int i in group ) {
				bool L_80 = Enumerator_MoveNext_m1050804954((Enumerator_t2017297076 *)(&V_11), /*hidden argument*/Enumerator_MoveNext_m1050804954_RuntimeMethod_var);
				if (L_80)
				{
					goto IL_01b5;
				}
			}

IL_01e9:
			{
				IL2CPP_LEAVE(0x1F9, FINALLY_01eb);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_01eb;
		}

FINALLY_01eb:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m222348240((Enumerator_t2017297076 *)(&V_11), /*hidden argument*/Enumerator_Dispose_m222348240_RuntimeMethod_var);
			IL2CPP_END_FINALLY(491)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(491)
		{
			IL2CPP_JUMP_TBL(0x1F9, IL_01f9)
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		}

IL_01f9:
		{
			// foreach( List<int> group in groups ) { // Calculate soft normals
			bool L_81 = Enumerator_MoveNext_m1436172384((Enumerator_t3489371818 *)(&V_6), /*hidden argument*/Enumerator_MoveNext_m1436172384_RuntimeMethod_var);
			if (L_81)
			{
				goto IL_00b5;
			}
		}

IL_0205:
		{
			IL2CPP_LEAVE(0x215, FINALLY_0207);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0207;
	}

FINALLY_0207:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m28313383((Enumerator_t3489371818 *)(&V_6), /*hidden argument*/Enumerator_Dispose_m28313383_RuntimeMethod_var);
		IL2CPP_END_FINALLY(519)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(519)
	{
		IL2CPP_JUMP_TBL(0x215, IL_0215)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0215:
	{
		// m.colors = colors; // Assign as vertex colors
		Mesh_t3648964284 * L_82 = ___m0;
		ColorU5BU5D_t941916413* L_83 = V_2;
		NullCheck(L_82);
		Mesh_set_colors_m2218481242(L_82, L_83, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SoftNormalsToVertexColor::.ctor()
extern "C"  void SoftNormalsToVertexColor__ctor_m2069340330 (SoftNormalsToVertexColor_t279782399 * __this, const RuntimeMethod* method)
{
	{
		// public Method method = Method.AngularDeviation;
		__this->set_method_2(1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unit::Start()
extern "C"  void Unit_Start_m1180624514 (Unit_t4139495810 * __this, const RuntimeMethod* method)
{
	{
		// StartCoroutine(UpdatePath());
		RuntimeObject* L_0 = Unit_UpdatePath_m2481450807(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Unit::OnPathFound(UnityEngine.Vector3[],System.Boolean)
extern "C"  void Unit_OnPathFound_m1311327391 (Unit_t4139495810 * __this, Vector3U5BU5D_t1718750761* ___waypoints0, bool ___pathSuccessful1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Unit_OnPathFound_m1311327391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (pathSuccessful)
		bool L_0 = ___pathSuccessful1;
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		// path = new Path(waypoints, transform.position, turnDst, stoppingDst);
		Vector3U5BU5D_t1718750761* L_1 = ___waypoints0;
		Transform_t3600365921 * L_2 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3722313464  L_3 = Transform_get_position_m36019626(L_2, /*hidden argument*/NULL);
		float L_4 = __this->get_turnDst_7();
		float L_5 = __this->get_stoppingDst_8();
		Path_t2615110272 * L_6 = (Path_t2615110272 *)il2cpp_codegen_object_new(Path_t2615110272_il2cpp_TypeInfo_var);
		Path__ctor_m3753908269(L_6, L_1, L_3, L_4, L_5, /*hidden argument*/NULL);
		__this->set_path_9(L_6);
		// StopCoroutine("FollowPath");
		MonoBehaviour_StopCoroutine_m1962070247(__this, _stringLiteral3056858982, /*hidden argument*/NULL);
		// StartCoroutine("FollowPath");
		MonoBehaviour_StartCoroutine_m2618285814(__this, _stringLiteral3056858982, /*hidden argument*/NULL);
	}

IL_003d:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator Unit::UpdatePath()
extern "C"  RuntimeObject* Unit_UpdatePath_m2481450807 (Unit_t4139495810 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Unit_UpdatePath_m2481450807_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CUpdatePathU3Ed__10_t1880251700 * L_0 = (U3CUpdatePathU3Ed__10_t1880251700 *)il2cpp_codegen_object_new(U3CUpdatePathU3Ed__10_t1880251700_il2cpp_TypeInfo_var);
		U3CUpdatePathU3Ed__10__ctor_m794121603(L_0, 0, /*hidden argument*/NULL);
		U3CUpdatePathU3Ed__10_t1880251700 * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Collections.IEnumerator Unit::FollowPath()
extern "C"  RuntimeObject* Unit_FollowPath_m1231934346 (Unit_t4139495810 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Unit_FollowPath_m1231934346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CFollowPathU3Ed__11_t3675468882 * L_0 = (U3CFollowPathU3Ed__11_t3675468882 *)il2cpp_codegen_object_new(U3CFollowPathU3Ed__11_t3675468882_il2cpp_TypeInfo_var);
		U3CFollowPathU3Ed__11__ctor_m2685063142(L_0, 0, /*hidden argument*/NULL);
		U3CFollowPathU3Ed__11_t3675468882 * L_1 = L_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void Unit::OnDrawGizmos()
extern "C"  void Unit_OnDrawGizmos_m707220317 (Unit_t4139495810 * __this, const RuntimeMethod* method)
{
	{
		// if (path != null)
		Path_t2615110272 * L_0 = __this->get_path_9();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		// path.DrawWithGizmos();
		Path_t2615110272 * L_1 = __this->get_path_9();
		NullCheck(L_1);
		Path_DrawWithGizmos_m2056486393(L_1, /*hidden argument*/NULL);
	}

IL_0013:
	{
		// }
		return;
	}
}
// System.Void Unit::.ctor()
extern "C"  void Unit__ctor_m3808420862 (Unit_t4139495810 * __this, const RuntimeMethod* method)
{
	{
		// public float speed = 5.0f;                      // The speed at which the GameObject will move
		__this->set_speed_5((5.0f));
		// public float turnSpeed = 3.0f;                  // The turning speed of the GameObject
		__this->set_turnSpeed_6((3.0f));
		// public float turnDst = 2.5f;                    // The turning distance of the GameObject
		__this->set_turnDst_7((2.5f));
		// public float stoppingDst = 10.0f;               // The distance from the target that the GameObject will decelerate
		__this->set_stoppingDst_8((10.0f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unit/<FollowPath>d__11::.ctor(System.Int32)
extern "C"  void U3CFollowPathU3Ed__11__ctor_m2685063142 (U3CFollowPathU3Ed__11_t3675468882 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Unit/<FollowPath>d__11::System.IDisposable.Dispose()
extern "C"  void U3CFollowPathU3Ed__11_System_IDisposable_Dispose_m3376097739 (U3CFollowPathU3Ed__11_t3675468882 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Unit/<FollowPath>d__11::MoveNext()
extern "C"  bool U3CFollowPathU3Ed__11_MoveNext_m796168624 (U3CFollowPathU3Ed__11_t3675468882 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFollowPathU3Ed__11_MoveNext_m796168624_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Quaternion_t2301928331  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0228;
		}
	}
	{
		return (bool)0;
	}

IL_0013:
	{
		__this->set_U3CU3E1__state_0((-1));
		// bool followingPath = true;
		__this->set_U3CfollowingPathU3E5__2_4((bool)1);
		// int pathIndex = 0;
		__this->set_U3CpathIndexU3E5__1_3(0);
		// transform.LookAt(path.lookPoints[0]);
		Unit_t4139495810 * L_3 = __this->get_U3CU3E4__this_2();
		NullCheck(L_3);
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(L_3, /*hidden argument*/NULL);
		Unit_t4139495810 * L_5 = __this->get_U3CU3E4__this_2();
		NullCheck(L_5);
		Path_t2615110272 * L_6 = L_5->get_path_9();
		NullCheck(L_6);
		Vector3U5BU5D_t1718750761* L_7 = L_6->get_lookPoints_0();
		NullCheck(L_7);
		int32_t L_8 = 0;
		Vector3_t3722313464  L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_4);
		Transform_LookAt_m3649447396(L_4, L_9, /*hidden argument*/NULL);
		// float speedPercent = 1.0f;
		__this->set_U3CspeedPercentU3E5__3_5((1.0f));
		goto IL_022f;
	}

IL_005e:
	{
		// Vector2 pos2D = new Vector2(transform.position.x, transform.position.z);
		Unit_t4139495810 * L_10 = __this->get_U3CU3E4__this_2();
		NullCheck(L_10);
		Transform_t3600365921 * L_11 = Component_get_transform_m3162698980(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t3722313464  L_12 = Transform_get_position_m36019626(L_11, /*hidden argument*/NULL);
		float L_13 = L_12.get_x_1();
		Unit_t4139495810 * L_14 = __this->get_U3CU3E4__this_2();
		NullCheck(L_14);
		Transform_t3600365921 * L_15 = Component_get_transform_m3162698980(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t3722313464  L_16 = Transform_get_position_m36019626(L_15, /*hidden argument*/NULL);
		float L_17 = L_16.get_z_3();
		Vector2__ctor_m3970636864((Vector2_t2156229523 *)(&V_1), L_13, L_17, /*hidden argument*/NULL);
		goto IL_00c2;
	}

IL_0091:
	{
		// if (pathIndex == path.finishLineIndex)
		int32_t L_18 = __this->get_U3CpathIndexU3E5__1_3();
		Unit_t4139495810 * L_19 = __this->get_U3CU3E4__this_2();
		NullCheck(L_19);
		Path_t2615110272 * L_20 = L_19->get_path_9();
		NullCheck(L_20);
		int32_t L_21 = L_20->get_finishLineIndex_2();
		if ((!(((uint32_t)L_18) == ((uint32_t)L_21))))
		{
			goto IL_00b2;
		}
	}
	{
		// followingPath = false;
		__this->set_U3CfollowingPathU3E5__2_4((bool)0);
		// break;
		goto IL_00e5;
	}

IL_00b2:
	{
		// pathIndex++;
		int32_t L_22 = __this->get_U3CpathIndexU3E5__1_3();
		V_2 = L_22;
		int32_t L_23 = V_2;
		__this->set_U3CpathIndexU3E5__1_3(((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1)));
	}

IL_00c2:
	{
		// while (path.turnBoundaries[pathIndex].HasCrossedLine(pos2D))
		Unit_t4139495810 * L_24 = __this->get_U3CU3E4__this_2();
		NullCheck(L_24);
		Path_t2615110272 * L_25 = L_24->get_path_9();
		NullCheck(L_25);
		LineU5BU5D_t3212499767* L_26 = L_25->get_turnBoundaries_1();
		int32_t L_27 = __this->get_U3CpathIndexU3E5__1_3();
		NullCheck(L_26);
		Vector2_t2156229523  L_28 = V_1;
		bool L_29 = Line_HasCrossedLine_m2779751333((Line_t3796957314 *)((L_26)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_27))), L_28, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_0091;
		}
	}

IL_00e5:
	{
		// if (followingPath)
		bool L_30 = __this->get_U3CfollowingPathU3E5__2_4();
		if (!L_30)
		{
			goto IL_0218;
		}
	}
	{
		// if (pathIndex >= path.slowDownIndex && stoppingDst > 0)
		int32_t L_31 = __this->get_U3CpathIndexU3E5__1_3();
		Unit_t4139495810 * L_32 = __this->get_U3CU3E4__this_2();
		NullCheck(L_32);
		Path_t2615110272 * L_33 = L_32->get_path_9();
		NullCheck(L_33);
		int32_t L_34 = L_33->get_slowDownIndex_3();
		if ((((int32_t)L_31) < ((int32_t)L_34)))
		{
			goto IL_0170;
		}
	}
	{
		Unit_t4139495810 * L_35 = __this->get_U3CU3E4__this_2();
		NullCheck(L_35);
		float L_36 = L_35->get_stoppingDst_8();
		if ((!(((float)L_36) > ((float)(0.0f)))))
		{
			goto IL_0170;
		}
	}
	{
		// speedPercent = Mathf.Clamp01(path.turnBoundaries[path.finishLineIndex].DistanceFromPoint(pos2D) / stoppingDst);
		Unit_t4139495810 * L_37 = __this->get_U3CU3E4__this_2();
		NullCheck(L_37);
		Path_t2615110272 * L_38 = L_37->get_path_9();
		NullCheck(L_38);
		LineU5BU5D_t3212499767* L_39 = L_38->get_turnBoundaries_1();
		Unit_t4139495810 * L_40 = __this->get_U3CU3E4__this_2();
		NullCheck(L_40);
		Path_t2615110272 * L_41 = L_40->get_path_9();
		NullCheck(L_41);
		int32_t L_42 = L_41->get_finishLineIndex_2();
		NullCheck(L_39);
		Vector2_t2156229523  L_43 = V_1;
		float L_44 = Line_DistanceFromPoint_m4294230093((Line_t3796957314 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_42))), L_43, /*hidden argument*/NULL);
		Unit_t4139495810 * L_45 = __this->get_U3CU3E4__this_2();
		NullCheck(L_45);
		float L_46 = L_45->get_stoppingDst_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_47 = Mathf_Clamp01_m56433566(NULL /*static, unused*/, ((float)((float)L_44/(float)L_46)), /*hidden argument*/NULL);
		__this->set_U3CspeedPercentU3E5__3_5(L_47);
		// if (speedPercent < 0.01f)
		float L_48 = __this->get_U3CspeedPercentU3E5__3_5();
		if ((!(((float)L_48) < ((float)(0.01f)))))
		{
			goto IL_0170;
		}
	}
	{
		// followingPath = false;
		__this->set_U3CfollowingPathU3E5__2_4((bool)0);
	}

IL_0170:
	{
		// Quaternion targetRotation = Quaternion.LookRotation(path.lookPoints[pathIndex] - transform.position);
		Unit_t4139495810 * L_49 = __this->get_U3CU3E4__this_2();
		NullCheck(L_49);
		Path_t2615110272 * L_50 = L_49->get_path_9();
		NullCheck(L_50);
		Vector3U5BU5D_t1718750761* L_51 = L_50->get_lookPoints_0();
		int32_t L_52 = __this->get_U3CpathIndexU3E5__1_3();
		NullCheck(L_51);
		int32_t L_53 = L_52;
		Vector3_t3722313464  L_54 = (L_51)->GetAt(static_cast<il2cpp_array_size_t>(L_53));
		Unit_t4139495810 * L_55 = __this->get_U3CU3E4__this_2();
		NullCheck(L_55);
		Transform_t3600365921 * L_56 = Component_get_transform_m3162698980(L_55, /*hidden argument*/NULL);
		NullCheck(L_56);
		Vector3_t3722313464  L_57 = Transform_get_position_m36019626(L_56, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_58 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_54, L_57, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_59 = Quaternion_LookRotation_m4040767668(NULL /*static, unused*/, L_58, /*hidden argument*/NULL);
		V_3 = L_59;
		// transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * turnSpeed);
		Unit_t4139495810 * L_60 = __this->get_U3CU3E4__this_2();
		NullCheck(L_60);
		Transform_t3600365921 * L_61 = Component_get_transform_m3162698980(L_60, /*hidden argument*/NULL);
		Unit_t4139495810 * L_62 = __this->get_U3CU3E4__this_2();
		NullCheck(L_62);
		Transform_t3600365921 * L_63 = Component_get_transform_m3162698980(L_62, /*hidden argument*/NULL);
		NullCheck(L_63);
		Quaternion_t2301928331  L_64 = Transform_get_rotation_m3502953881(L_63, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_65 = V_3;
		float L_66 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Unit_t4139495810 * L_67 = __this->get_U3CU3E4__this_2();
		NullCheck(L_67);
		float L_68 = L_67->get_turnSpeed_6();
		Quaternion_t2301928331  L_69 = Quaternion_Lerp_m1238806789(NULL /*static, unused*/, L_64, L_65, ((float)il2cpp_codegen_multiply((float)L_66, (float)L_68)), /*hidden argument*/NULL);
		NullCheck(L_61);
		Transform_set_rotation_m3524318132(L_61, L_69, /*hidden argument*/NULL);
		// transform.Translate(Vector3.forward * Time.deltaTime * speed * speedPercent, Space.Self);
		Unit_t4139495810 * L_70 = __this->get_U3CU3E4__this_2();
		NullCheck(L_70);
		Transform_t3600365921 * L_71 = Component_get_transform_m3162698980(L_70, /*hidden argument*/NULL);
		Vector3_t3722313464  L_72 = Vector3_get_forward_m3100859705(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_73 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_74 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_72, L_73, /*hidden argument*/NULL);
		Unit_t4139495810 * L_75 = __this->get_U3CU3E4__this_2();
		NullCheck(L_75);
		float L_76 = L_75->get_speed_5();
		Vector3_t3722313464  L_77 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_74, L_76, /*hidden argument*/NULL);
		float L_78 = __this->get_U3CspeedPercentU3E5__3_5();
		Vector3_t3722313464  L_79 = Vector3_op_Multiply_m3376773913(NULL /*static, unused*/, L_77, L_78, /*hidden argument*/NULL);
		NullCheck(L_71);
		Transform_Translate_m1990195114(L_71, L_79, 1, /*hidden argument*/NULL);
	}

IL_0218:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0228:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_022f:
	{
		// while (followingPath)
		bool L_80 = __this->get_U3CfollowingPathU3E5__2_4();
		if (L_80)
		{
			goto IL_005e;
		}
	}
	{
		// }
		return (bool)0;
	}
}
// System.Object Unit/<FollowPath>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C"  RuntimeObject * U3CFollowPathU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m590083208 (U3CFollowPathU3Ed__11_t3675468882 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Unit/<FollowPath>d__11::System.Collections.IEnumerator.Reset()
extern "C"  void U3CFollowPathU3Ed__11_System_Collections_IEnumerator_Reset_m1351258946 (U3CFollowPathU3Ed__11_t3675468882 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFollowPathU3Ed__11_System_Collections_IEnumerator_Reset_m1351258946_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CFollowPathU3Ed__11_System_Collections_IEnumerator_Reset_m1351258946_RuntimeMethod_var);
	}
}
// System.Object Unit/<FollowPath>d__11::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CFollowPathU3Ed__11_System_Collections_IEnumerator_get_Current_m956326452 (U3CFollowPathU3Ed__11_t3675468882 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unit/<UpdatePath>d__10::.ctor(System.Int32)
extern "C"  void U3CUpdatePathU3Ed__10__ctor_m794121603 (U3CUpdatePathU3Ed__10_t1880251700 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void Unit/<UpdatePath>d__10::System.IDisposable.Dispose()
extern "C"  void U3CUpdatePathU3Ed__10_System_IDisposable_Dispose_m1234688066 (U3CUpdatePathU3Ed__10_t1880251700 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean Unit/<UpdatePath>d__10::MoveNext()
extern "C"  bool U3CUpdatePathU3Ed__10_MoveNext_m3059736206 (U3CUpdatePathU3Ed__10_t1880251700 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CUpdatePathU3Ed__10_MoveNext_m3059736206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001b;
			}
			case 1:
			{
				goto IL_0047;
			}
			case 2:
			{
				goto IL_00c3;
			}
		}
	}
	{
		return (bool)0;
	}

IL_001b:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (Time.timeSinceLevelLoad < 0.3f)
		float L_2 = Time_get_timeSinceLevelLoad_m2224611026(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)(0.3f)))))
		{
			goto IL_004e;
		}
	}
	{
		// yield return new WaitForSeconds(0.3f);
		WaitForSeconds_t1699091251 * L_3 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_3, (0.3f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_3);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0047:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_004e:
	{
		// PathRequestManager.RequestPath(new PathRequest(transform.position, target.position, OnPathFound));
		Unit_t4139495810 * L_4 = __this->get_U3CU3E4__this_2();
		NullCheck(L_4);
		Transform_t3600365921 * L_5 = Component_get_transform_m3162698980(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Vector3_t3722313464  L_6 = Transform_get_position_m36019626(L_5, /*hidden argument*/NULL);
		Unit_t4139495810 * L_7 = __this->get_U3CU3E4__this_2();
		NullCheck(L_7);
		Transform_t3600365921 * L_8 = L_7->get_target_4();
		NullCheck(L_8);
		Vector3_t3722313464  L_9 = Transform_get_position_m36019626(L_8, /*hidden argument*/NULL);
		Unit_t4139495810 * L_10 = __this->get_U3CU3E4__this_2();
		intptr_t L_11 = (intptr_t)Unit_OnPathFound_m1311327391_RuntimeMethod_var;
		Action_2_t1484661638 * L_12 = (Action_2_t1484661638 *)il2cpp_codegen_object_new(Action_2_t1484661638_il2cpp_TypeInfo_var);
		Action_2__ctor_m2824747351(L_12, L_10, L_11, /*hidden argument*/Action_2__ctor_m2824747351_RuntimeMethod_var);
		PathRequest_t2117613800  L_13;
		memset(&L_13, 0, sizeof(L_13));
		PathRequest__ctor_m1589651504((&L_13), L_6, L_9, L_12, /*hidden argument*/NULL);
		PathRequestManager_RequestPath_m3072144720(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		// float squareMoveThreshhold = pathUpdateMoveThreshhold * pathUpdateMoveThreshhold;
		__this->set_U3CsquareMoveThreshholdU3E5__2_4((0.25f));
		// Vector3 targetPosOld = target.position;
		Unit_t4139495810 * L_14 = __this->get_U3CU3E4__this_2();
		NullCheck(L_14);
		Transform_t3600365921 * L_15 = L_14->get_target_4();
		NullCheck(L_15);
		Vector3_t3722313464  L_16 = Transform_get_position_m36019626(L_15, /*hidden argument*/NULL);
		__this->set_U3CtargetPosOldU3E5__1_3(L_16);
	}

IL_00aa:
	{
		// yield return new WaitForSeconds(minPathUpdateTime);
		WaitForSeconds_t1699091251 * L_17 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_17, (0.2f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_17);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_00c3:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if ((target.position - targetPosOld).sqrMagnitude > squareMoveThreshhold)
		Unit_t4139495810 * L_18 = __this->get_U3CU3E4__this_2();
		NullCheck(L_18);
		Transform_t3600365921 * L_19 = L_18->get_target_4();
		NullCheck(L_19);
		Vector3_t3722313464  L_20 = Transform_get_position_m36019626(L_19, /*hidden argument*/NULL);
		Vector3_t3722313464  L_21 = __this->get_U3CtargetPosOldU3E5__1_3();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_22 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		float L_23 = Vector3_get_sqrMagnitude_m1474274574((Vector3_t3722313464 *)(&V_1), /*hidden argument*/NULL);
		float L_24 = __this->get_U3CsquareMoveThreshholdU3E5__2_4();
		if ((!(((float)L_23) > ((float)L_24))))
		{
			goto IL_00aa;
		}
	}
	{
		// PathRequestManager.RequestPath(new PathRequest(transform.position, target.position, OnPathFound));
		Unit_t4139495810 * L_25 = __this->get_U3CU3E4__this_2();
		NullCheck(L_25);
		Transform_t3600365921 * L_26 = Component_get_transform_m3162698980(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		Vector3_t3722313464  L_27 = Transform_get_position_m36019626(L_26, /*hidden argument*/NULL);
		Unit_t4139495810 * L_28 = __this->get_U3CU3E4__this_2();
		NullCheck(L_28);
		Transform_t3600365921 * L_29 = L_28->get_target_4();
		NullCheck(L_29);
		Vector3_t3722313464  L_30 = Transform_get_position_m36019626(L_29, /*hidden argument*/NULL);
		Unit_t4139495810 * L_31 = __this->get_U3CU3E4__this_2();
		intptr_t L_32 = (intptr_t)Unit_OnPathFound_m1311327391_RuntimeMethod_var;
		Action_2_t1484661638 * L_33 = (Action_2_t1484661638 *)il2cpp_codegen_object_new(Action_2_t1484661638_il2cpp_TypeInfo_var);
		Action_2__ctor_m2824747351(L_33, L_31, L_32, /*hidden argument*/Action_2__ctor_m2824747351_RuntimeMethod_var);
		PathRequest_t2117613800  L_34;
		memset(&L_34, 0, sizeof(L_34));
		PathRequest__ctor_m1589651504((&L_34), L_27, L_30, L_33, /*hidden argument*/NULL);
		PathRequestManager_RequestPath_m3072144720(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		// targetPosOld = target.position;
		Unit_t4139495810 * L_35 = __this->get_U3CU3E4__this_2();
		NullCheck(L_35);
		Transform_t3600365921 * L_36 = L_35->get_target_4();
		NullCheck(L_36);
		Vector3_t3722313464  L_37 = Transform_get_position_m36019626(L_36, /*hidden argument*/NULL);
		__this->set_U3CtargetPosOldU3E5__1_3(L_37);
		// while (true)
		goto IL_00aa;
	}
}
// System.Object Unit/<UpdatePath>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C"  RuntimeObject * U3CUpdatePathU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1638355899 (U3CUpdatePathU3Ed__10_t1880251700 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void Unit/<UpdatePath>d__10::System.Collections.IEnumerator.Reset()
extern "C"  void U3CUpdatePathU3Ed__10_System_Collections_IEnumerator_Reset_m2433452750 (U3CUpdatePathU3Ed__10_t1880251700 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CUpdatePathU3Ed__10_System_Collections_IEnumerator_Reset_m2433452750_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CUpdatePathU3Ed__10_System_Collections_IEnumerator_Reset_m2433452750_RuntimeMethod_var);
	}
}
// System.Object Unit/<UpdatePath>d__10::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CUpdatePathU3Ed__10_System_Collections_IEnumerator_get_Current_m2302489488 (U3CUpdatePathU3Ed__10_t1880251700 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
