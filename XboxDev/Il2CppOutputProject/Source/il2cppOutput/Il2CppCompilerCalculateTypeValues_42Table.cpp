﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// Line[]
struct LineU5BU5D_t3212499767;
// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct ParticleSystemDestroyer_t558680695;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t3089334924;
// UnityStandardAssets.Utility.ObjectResetter
struct ObjectResetter_t639177103;
// Unit
struct Unit_t4139495810;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct Entry_t2725803170;
// UnityStandardAssets.Utility.WaypointCircuit
struct WaypointCircuit_t445075330;
// UnityEngine.Transform[]
struct TransformU5BU5D_t807237628;
// UnityStandardAssets.Utility.TimedObjectActivator/Entry[]
struct EntryU5BU5D_t3574483607;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityStandardAssets.Utility.FOVKick
struct FOVKick_t120370150;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[]
struct ReplacementDefinitionU5BU5D_t2596446823;
// Bullet
struct Bullet_t1042140031;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityStandardAssets.Utility.DragRigidbody
struct DragRigidbody_t1600652016;
// UnityStandardAssets.Utility.LerpControlledBob
struct LerpControlledBob_t1895875871;
// System.Void
struct Void_t1185182177;
// System.Action`2<UnityEngine.Vector3[],System.Boolean>
struct Action_2_t1484661638;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String
struct String_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Material
struct Material_t340375123;
// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct WaypointList_t2584574554;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// UnityEngine.GUIText
struct GUIText_t402233326;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.UI.Text
struct Text_t1901882714;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t777473367;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2007329276;
// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct Entries_t3168066469;
// UnityEngine.Light
struct Light_t3756812086;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// EnemyCount
struct EnemyCount_t3730988989;
// LevelClear
struct LevelClear_t1949285084;
// EnemyStats
struct EnemyStats_t4187841152;
// UnityEngine.UI.Image
struct Image_t2670269651;
// PlayerStats
struct PlayerStats_t2044123780;
// UnityEngine.AI.NavMeshAgent
struct NavMeshAgent_t1276799816;
// Gun
struct Gun_t783153271;
// UnityEngine.BoxCollider
struct BoxCollider_t1640800422;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t2439009922;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t2581906349;
// LevelChanger
struct LevelChanger_t225386971;
// Inventory
struct Inventory_t1050226016;
// BossTankController
struct BossTankController_t1902331492;
// EnemyShoot
struct EnemyShoot_t243830779;
// HealthUI
struct HealthUI_t1908446248;
// Inventory/OnItemChanged
struct OnItemChanged_t22848112;
// Item
struct Item_t2953980098;
// Path
struct Path_t2615110272;
// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct Vector3andSpace_t219844479;
// UnityEngine.SpringJoint
struct SpringJoint_t1912369980;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
struct ReplacementList_t1887104210;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// InventorySlot
struct InventorySlot_t3299309524;
// Grid
struct Grid_t1081586032;
// System.Collections.Generic.Queue`1<PathResult>
struct Queue_1_t1374272722;
// Pathfinding
struct Pathfinding_t1696161914;
// Grid/TerrainType[]
struct TerrainTypeU5BU5D_t1748095520;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// Node[0...,0...]
struct NodeU5B0___U2C0___U5D_t1457036568;
// LevelSelectPanel[]
struct LevelSelectPanelU5BU5D_t1747102860;

struct Vector3_t3722313464 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745563_H
#define U3CMODULEU3E_T692745563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745563 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745563_H
#ifndef PATH_T2615110272_H
#define PATH_T2615110272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Path
struct  Path_t2615110272  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] Path::lookPoints
	Vector3U5BU5D_t1718750761* ___lookPoints_0;
	// Line[] Path::turnBoundaries
	LineU5BU5D_t3212499767* ___turnBoundaries_1;
	// System.Int32 Path::finishLineIndex
	int32_t ___finishLineIndex_2;
	// System.Int32 Path::slowDownIndex
	int32_t ___slowDownIndex_3;

public:
	inline static int32_t get_offset_of_lookPoints_0() { return static_cast<int32_t>(offsetof(Path_t2615110272, ___lookPoints_0)); }
	inline Vector3U5BU5D_t1718750761* get_lookPoints_0() const { return ___lookPoints_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_lookPoints_0() { return &___lookPoints_0; }
	inline void set_lookPoints_0(Vector3U5BU5D_t1718750761* value)
	{
		___lookPoints_0 = value;
		Il2CppCodeGenWriteBarrier((&___lookPoints_0), value);
	}

	inline static int32_t get_offset_of_turnBoundaries_1() { return static_cast<int32_t>(offsetof(Path_t2615110272, ___turnBoundaries_1)); }
	inline LineU5BU5D_t3212499767* get_turnBoundaries_1() const { return ___turnBoundaries_1; }
	inline LineU5BU5D_t3212499767** get_address_of_turnBoundaries_1() { return &___turnBoundaries_1; }
	inline void set_turnBoundaries_1(LineU5BU5D_t3212499767* value)
	{
		___turnBoundaries_1 = value;
		Il2CppCodeGenWriteBarrier((&___turnBoundaries_1), value);
	}

	inline static int32_t get_offset_of_finishLineIndex_2() { return static_cast<int32_t>(offsetof(Path_t2615110272, ___finishLineIndex_2)); }
	inline int32_t get_finishLineIndex_2() const { return ___finishLineIndex_2; }
	inline int32_t* get_address_of_finishLineIndex_2() { return &___finishLineIndex_2; }
	inline void set_finishLineIndex_2(int32_t value)
	{
		___finishLineIndex_2 = value;
	}

	inline static int32_t get_offset_of_slowDownIndex_3() { return static_cast<int32_t>(offsetof(Path_t2615110272, ___slowDownIndex_3)); }
	inline int32_t get_slowDownIndex_3() const { return ___slowDownIndex_3; }
	inline int32_t* get_address_of_slowDownIndex_3() { return &___slowDownIndex_3; }
	inline void set_slowDownIndex_3(int32_t value)
	{
		___slowDownIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATH_T2615110272_H
#ifndef U3CSTARTU3ED__4_T3571467226_H
#define U3CSTARTU3ED__4_T3571467226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4
struct  U3CStartU3Ed__4_t3571467226  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.ParticleSystemDestroyer UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::<>4__this
	ParticleSystemDestroyer_t558680695 * ___U3CU3E4__this_2;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::<stopTime>5__1
	float ___U3CstopTimeU3E5__1_3;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::<systems>5__2
	ParticleSystemU5BU5D_t3089334924* ___U3CsystemsU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t3571467226, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t3571467226, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t3571467226, ___U3CU3E4__this_2)); }
	inline ParticleSystemDestroyer_t558680695 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ParticleSystemDestroyer_t558680695 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ParticleSystemDestroyer_t558680695 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CstopTimeU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t3571467226, ___U3CstopTimeU3E5__1_3)); }
	inline float get_U3CstopTimeU3E5__1_3() const { return ___U3CstopTimeU3E5__1_3; }
	inline float* get_address_of_U3CstopTimeU3E5__1_3() { return &___U3CstopTimeU3E5__1_3; }
	inline void set_U3CstopTimeU3E5__1_3(float value)
	{
		___U3CstopTimeU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CsystemsU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t3571467226, ___U3CsystemsU3E5__2_4)); }
	inline ParticleSystemU5BU5D_t3089334924* get_U3CsystemsU3E5__2_4() const { return ___U3CsystemsU3E5__2_4; }
	inline ParticleSystemU5BU5D_t3089334924** get_address_of_U3CsystemsU3E5__2_4() { return &___U3CsystemsU3E5__2_4; }
	inline void set_U3CsystemsU3E5__2_4(ParticleSystemU5BU5D_t3089334924* value)
	{
		___U3CsystemsU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsystemsU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__4_T3571467226_H
#ifndef U3CRESETCOROUTINEU3ED__6_T3318545224_H
#define U3CRESETCOROUTINEU3ED__6_T3318545224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6
struct  U3CResetCoroutineU3Ed__6_t3318545224  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::delay
	float ___delay_2;
	// UnityStandardAssets.Utility.ObjectResetter UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::<>4__this
	ObjectResetter_t639177103 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ed__6_t3318545224, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ed__6_t3318545224, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ed__6_t3318545224, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ed__6_t3318545224, ___U3CU3E4__this_3)); }
	inline ObjectResetter_t639177103 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ObjectResetter_t639177103 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ObjectResetter_t639177103 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESETCOROUTINEU3ED__6_T3318545224_H
#ifndef U3CFOLLOWPATHU3ED__11_T3675468882_H
#define U3CFOLLOWPATHU3ED__11_T3675468882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unit/<FollowPath>d__11
struct  U3CFollowPathU3Ed__11_t3675468882  : public RuntimeObject
{
public:
	// System.Int32 Unit/<FollowPath>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Unit/<FollowPath>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Unit Unit/<FollowPath>d__11::<>4__this
	Unit_t4139495810 * ___U3CU3E4__this_2;
	// System.Int32 Unit/<FollowPath>d__11::<pathIndex>5__1
	int32_t ___U3CpathIndexU3E5__1_3;
	// System.Boolean Unit/<FollowPath>d__11::<followingPath>5__2
	bool ___U3CfollowingPathU3E5__2_4;
	// System.Single Unit/<FollowPath>d__11::<speedPercent>5__3
	float ___U3CspeedPercentU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFollowPathU3Ed__11_t3675468882, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFollowPathU3Ed__11_t3675468882, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFollowPathU3Ed__11_t3675468882, ___U3CU3E4__this_2)); }
	inline Unit_t4139495810 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Unit_t4139495810 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Unit_t4139495810 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CpathIndexU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CFollowPathU3Ed__11_t3675468882, ___U3CpathIndexU3E5__1_3)); }
	inline int32_t get_U3CpathIndexU3E5__1_3() const { return ___U3CpathIndexU3E5__1_3; }
	inline int32_t* get_address_of_U3CpathIndexU3E5__1_3() { return &___U3CpathIndexU3E5__1_3; }
	inline void set_U3CpathIndexU3E5__1_3(int32_t value)
	{
		___U3CpathIndexU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CfollowingPathU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CFollowPathU3Ed__11_t3675468882, ___U3CfollowingPathU3E5__2_4)); }
	inline bool get_U3CfollowingPathU3E5__2_4() const { return ___U3CfollowingPathU3E5__2_4; }
	inline bool* get_address_of_U3CfollowingPathU3E5__2_4() { return &___U3CfollowingPathU3E5__2_4; }
	inline void set_U3CfollowingPathU3E5__2_4(bool value)
	{
		___U3CfollowingPathU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CspeedPercentU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CFollowPathU3Ed__11_t3675468882, ___U3CspeedPercentU3E5__3_5)); }
	inline float get_U3CspeedPercentU3E5__3_5() const { return ___U3CspeedPercentU3E5__3_5; }
	inline float* get_address_of_U3CspeedPercentU3E5__3_5() { return &___U3CspeedPercentU3E5__3_5; }
	inline void set_U3CspeedPercentU3E5__3_5(float value)
	{
		___U3CspeedPercentU3E5__3_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOLLOWPATHU3ED__11_T3675468882_H
#ifndef U3CRELOADLEVELU3ED__7_T2023328258_H
#define U3CRELOADLEVELU3ED__7_T2023328258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7
struct  U3CReloadLevelU3Ed__7_t2023328258  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<ReloadLevel>d__7::entry
	Entry_t2725803170 * ___entry_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ed__7_t2023328258, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ed__7_t2023328258, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_entry_2() { return static_cast<int32_t>(offsetof(U3CReloadLevelU3Ed__7_t2023328258, ___entry_2)); }
	inline Entry_t2725803170 * get_entry_2() const { return ___entry_2; }
	inline Entry_t2725803170 ** get_address_of_entry_2() { return &___entry_2; }
	inline void set_entry_2(Entry_t2725803170 * value)
	{
		___entry_2 = value;
		Il2CppCodeGenWriteBarrier((&___entry_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRELOADLEVELU3ED__7_T2023328258_H
#ifndef WAYPOINTLIST_T2584574554_H
#define WAYPOINTLIST_T2584574554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit/WaypointList
struct  WaypointList_t2584574554  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit UnityStandardAssets.Utility.WaypointCircuit/WaypointList::circuit
	WaypointCircuit_t445075330 * ___circuit_0;
	// UnityEngine.Transform[] UnityStandardAssets.Utility.WaypointCircuit/WaypointList::items
	TransformU5BU5D_t807237628* ___items_1;

public:
	inline static int32_t get_offset_of_circuit_0() { return static_cast<int32_t>(offsetof(WaypointList_t2584574554, ___circuit_0)); }
	inline WaypointCircuit_t445075330 * get_circuit_0() const { return ___circuit_0; }
	inline WaypointCircuit_t445075330 ** get_address_of_circuit_0() { return &___circuit_0; }
	inline void set_circuit_0(WaypointCircuit_t445075330 * value)
	{
		___circuit_0 = value;
		Il2CppCodeGenWriteBarrier((&___circuit_0), value);
	}

	inline static int32_t get_offset_of_items_1() { return static_cast<int32_t>(offsetof(WaypointList_t2584574554, ___items_1)); }
	inline TransformU5BU5D_t807237628* get_items_1() const { return ___items_1; }
	inline TransformU5BU5D_t807237628** get_address_of_items_1() { return &___items_1; }
	inline void set_items_1(TransformU5BU5D_t807237628* value)
	{
		___items_1 = value;
		Il2CppCodeGenWriteBarrier((&___items_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTLIST_T2584574554_H
#ifndef U3CDEACTIVATEU3ED__6_T2861800224_H
#define U3CDEACTIVATEU3ED__6_T2861800224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6
struct  U3CDeactivateU3Ed__6_t2861800224  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<Deactivate>d__6::entry
	Entry_t2725803170 * ___entry_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ed__6_t2861800224, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ed__6_t2861800224, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_entry_2() { return static_cast<int32_t>(offsetof(U3CDeactivateU3Ed__6_t2861800224, ___entry_2)); }
	inline Entry_t2725803170 * get_entry_2() const { return ___entry_2; }
	inline Entry_t2725803170 ** get_address_of_entry_2() { return &___entry_2; }
	inline void set_entry_2(Entry_t2725803170 * value)
	{
		___entry_2 = value;
		Il2CppCodeGenWriteBarrier((&___entry_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDEACTIVATEU3ED__6_T2861800224_H
#ifndef ENTRIES_T3168066469_H
#define ENTRIES_T3168066469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct  Entries_t3168066469  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry[] UnityStandardAssets.Utility.TimedObjectActivator/Entries::entries
	EntryU5BU5D_t3574483607* ___entries_0;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(Entries_t3168066469, ___entries_0)); }
	inline EntryU5BU5D_t3574483607* get_entries_0() const { return ___entries_0; }
	inline EntryU5BU5D_t3574483607** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(EntryU5BU5D_t3574483607* value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier((&___entries_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRIES_T3168066469_H
#ifndef U3CACTIVATEU3ED__5_T4263739043_H
#define U3CACTIVATEU3ED__5_T4263739043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5
struct  U3CActivateU3Ed__5_t4263739043  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.TimedObjectActivator/Entry UnityStandardAssets.Utility.TimedObjectActivator/<Activate>d__5::entry
	Entry_t2725803170 * ___entry_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CActivateU3Ed__5_t4263739043, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CActivateU3Ed__5_t4263739043, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_entry_2() { return static_cast<int32_t>(offsetof(U3CActivateU3Ed__5_t4263739043, ___entry_2)); }
	inline Entry_t2725803170 * get_entry_2() const { return ___entry_2; }
	inline Entry_t2725803170 ** get_address_of_entry_2() { return &___entry_2; }
	inline void set_entry_2(Entry_t2725803170 * value)
	{
		___entry_2 = value;
		Il2CppCodeGenWriteBarrier((&___entry_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CACTIVATEU3ED__5_T4263739043_H
#ifndef REPLACEMENTDEFINITION_T2693741842_H
#define REPLACEMENTDEFINITION_T2693741842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
struct  ReplacementDefinition_t2693741842  : public RuntimeObject
{
public:
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::original
	Shader_t4151988712 * ___original_0;
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::replacement
	Shader_t4151988712 * ___replacement_1;

public:
	inline static int32_t get_offset_of_original_0() { return static_cast<int32_t>(offsetof(ReplacementDefinition_t2693741842, ___original_0)); }
	inline Shader_t4151988712 * get_original_0() const { return ___original_0; }
	inline Shader_t4151988712 ** get_address_of_original_0() { return &___original_0; }
	inline void set_original_0(Shader_t4151988712 * value)
	{
		___original_0 = value;
		Il2CppCodeGenWriteBarrier((&___original_0), value);
	}

	inline static int32_t get_offset_of_replacement_1() { return static_cast<int32_t>(offsetof(ReplacementDefinition_t2693741842, ___replacement_1)); }
	inline Shader_t4151988712 * get_replacement_1() const { return ___replacement_1; }
	inline Shader_t4151988712 ** get_address_of_replacement_1() { return &___replacement_1; }
	inline void set_replacement_1(Shader_t4151988712 * value)
	{
		___replacement_1 = value;
		Il2CppCodeGenWriteBarrier((&___replacement_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLACEMENTDEFINITION_T2693741842_H
#ifndef U3CFOVKICKUPU3ED__9_T1152317412_H
#define U3CFOVKICKUPU3ED__9_T1152317412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9
struct  U3CFOVKickUpU3Ed__9_t1152317412  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::<>4__this
	FOVKick_t120370150 * ___U3CU3E4__this_2;
	// System.Single UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::<t>5__1
	float ___U3CtU3E5__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ed__9_t1152317412, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ed__9_t1152317412, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ed__9_t1152317412, ___U3CU3E4__this_2)); }
	inline FOVKick_t120370150 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FOVKick_t120370150 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FOVKick_t120370150 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ed__9_t1152317412, ___U3CtU3E5__1_3)); }
	inline float get_U3CtU3E5__1_3() const { return ___U3CtU3E5__1_3; }
	inline float* get_address_of_U3CtU3E5__1_3() { return &___U3CtU3E5__1_3; }
	inline void set_U3CtU3E5__1_3(float value)
	{
		___U3CtU3E5__1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOVKICKUPU3ED__9_T1152317412_H
#ifndef REPLACEMENTLIST_T1887104210_H
#define REPLACEMENTLIST_T1887104210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
struct  ReplacementList_t1887104210  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[] UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList::items
	ReplacementDefinitionU5BU5D_t2596446823* ___items_0;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(ReplacementList_t1887104210, ___items_0)); }
	inline ReplacementDefinitionU5BU5D_t2596446823* get_items_0() const { return ___items_0; }
	inline ReplacementDefinitionU5BU5D_t2596446823** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(ReplacementDefinitionU5BU5D_t2596446823* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLACEMENTLIST_T1887104210_H
#ifndef U3CFADEU3ED__7_T626104978_H
#define U3CFADEU3ED__7_T626104978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bullet/<Fade>d__7
struct  U3CFadeU3Ed__7_t626104978  : public RuntimeObject
{
public:
	// System.Int32 Bullet/<Fade>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Bullet/<Fade>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Bullet Bullet/<Fade>d__7::<>4__this
	Bullet_t1042140031 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFadeU3Ed__7_t626104978, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFadeU3Ed__7_t626104978, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFadeU3Ed__7_t626104978, ___U3CU3E4__this_2)); }
	inline Bullet_t1042140031 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Bullet_t1042140031 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Bullet_t1042140031 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEU3ED__7_T626104978_H
#ifndef FOVKICK_T120370150_H
#define FOVKICK_T120370150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick
struct  FOVKick_t120370150  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityStandardAssets.Utility.FOVKick::Camera
	Camera_t4157153871 * ___Camera_0;
	// System.Single UnityStandardAssets.Utility.FOVKick::originalFov
	float ___originalFov_1;
	// System.Single UnityStandardAssets.Utility.FOVKick::FOVIncrease
	float ___FOVIncrease_2;
	// System.Single UnityStandardAssets.Utility.FOVKick::TimeToIncrease
	float ___TimeToIncrease_3;
	// System.Single UnityStandardAssets.Utility.FOVKick::TimeToDecrease
	float ___TimeToDecrease_4;
	// UnityEngine.AnimationCurve UnityStandardAssets.Utility.FOVKick::IncreaseCurve
	AnimationCurve_t3046754366 * ___IncreaseCurve_5;

public:
	inline static int32_t get_offset_of_Camera_0() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___Camera_0)); }
	inline Camera_t4157153871 * get_Camera_0() const { return ___Camera_0; }
	inline Camera_t4157153871 ** get_address_of_Camera_0() { return &___Camera_0; }
	inline void set_Camera_0(Camera_t4157153871 * value)
	{
		___Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_0), value);
	}

	inline static int32_t get_offset_of_originalFov_1() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___originalFov_1)); }
	inline float get_originalFov_1() const { return ___originalFov_1; }
	inline float* get_address_of_originalFov_1() { return &___originalFov_1; }
	inline void set_originalFov_1(float value)
	{
		___originalFov_1 = value;
	}

	inline static int32_t get_offset_of_FOVIncrease_2() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___FOVIncrease_2)); }
	inline float get_FOVIncrease_2() const { return ___FOVIncrease_2; }
	inline float* get_address_of_FOVIncrease_2() { return &___FOVIncrease_2; }
	inline void set_FOVIncrease_2(float value)
	{
		___FOVIncrease_2 = value;
	}

	inline static int32_t get_offset_of_TimeToIncrease_3() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___TimeToIncrease_3)); }
	inline float get_TimeToIncrease_3() const { return ___TimeToIncrease_3; }
	inline float* get_address_of_TimeToIncrease_3() { return &___TimeToIncrease_3; }
	inline void set_TimeToIncrease_3(float value)
	{
		___TimeToIncrease_3 = value;
	}

	inline static int32_t get_offset_of_TimeToDecrease_4() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___TimeToDecrease_4)); }
	inline float get_TimeToDecrease_4() const { return ___TimeToDecrease_4; }
	inline float* get_address_of_TimeToDecrease_4() { return &___TimeToDecrease_4; }
	inline void set_TimeToDecrease_4(float value)
	{
		___TimeToDecrease_4 = value;
	}

	inline static int32_t get_offset_of_IncreaseCurve_5() { return static_cast<int32_t>(offsetof(FOVKick_t120370150, ___IncreaseCurve_5)); }
	inline AnimationCurve_t3046754366 * get_IncreaseCurve_5() const { return ___IncreaseCurve_5; }
	inline AnimationCurve_t3046754366 ** get_address_of_IncreaseCurve_5() { return &___IncreaseCurve_5; }
	inline void set_IncreaseCurve_5(AnimationCurve_t3046754366 * value)
	{
		___IncreaseCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___IncreaseCurve_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOVKICK_T120370150_H
#ifndef U3CDRAGOBJECTU3ED__8_T4113469309_H
#define U3CDRAGOBJECTU3ED__8_T4113469309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8
struct  U3CDragObjectU3Ed__8_t4113469309  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.DragRigidbody UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<>4__this
	DragRigidbody_t1600652016 * ___U3CU3E4__this_2;
	// UnityEngine.Camera UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<mainCamera>5__1
	Camera_t4157153871 * ___U3CmainCameraU3E5__1_3;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::distance
	float ___distance_4;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<oldDrag>5__2
	float ___U3ColdDragU3E5__2_5;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<oldAngularDrag>5__3
	float ___U3ColdAngularDragU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t4113469309, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t4113469309, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t4113469309, ___U3CU3E4__this_2)); }
	inline DragRigidbody_t1600652016 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DragRigidbody_t1600652016 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DragRigidbody_t1600652016 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CmainCameraU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t4113469309, ___U3CmainCameraU3E5__1_3)); }
	inline Camera_t4157153871 * get_U3CmainCameraU3E5__1_3() const { return ___U3CmainCameraU3E5__1_3; }
	inline Camera_t4157153871 ** get_address_of_U3CmainCameraU3E5__1_3() { return &___U3CmainCameraU3E5__1_3; }
	inline void set_U3CmainCameraU3E5__1_3(Camera_t4157153871 * value)
	{
		___U3CmainCameraU3E5__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmainCameraU3E5__1_3), value);
	}

	inline static int32_t get_offset_of_distance_4() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t4113469309, ___distance_4)); }
	inline float get_distance_4() const { return ___distance_4; }
	inline float* get_address_of_distance_4() { return &___distance_4; }
	inline void set_distance_4(float value)
	{
		___distance_4 = value;
	}

	inline static int32_t get_offset_of_U3ColdDragU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t4113469309, ___U3ColdDragU3E5__2_5)); }
	inline float get_U3ColdDragU3E5__2_5() const { return ___U3ColdDragU3E5__2_5; }
	inline float* get_address_of_U3ColdDragU3E5__2_5() { return &___U3ColdDragU3E5__2_5; }
	inline void set_U3ColdDragU3E5__2_5(float value)
	{
		___U3ColdDragU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3ColdAngularDragU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t4113469309, ___U3ColdAngularDragU3E5__3_6)); }
	inline float get_U3ColdAngularDragU3E5__3_6() const { return ___U3ColdAngularDragU3E5__3_6; }
	inline float* get_address_of_U3ColdAngularDragU3E5__3_6() { return &___U3ColdAngularDragU3E5__3_6; }
	inline void set_U3ColdAngularDragU3E5__3_6(float value)
	{
		___U3ColdAngularDragU3E5__3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDRAGOBJECTU3ED__8_T4113469309_H
#ifndef U3CDOBOBCYCLEU3ED__4_T3986606630_H
#define U3CDOBOBCYCLEU3ED__4_T3986606630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4
struct  U3CDoBobCycleU3Ed__4_t3986606630  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::<>4__this
	LerpControlledBob_t1895875871 * ___U3CU3E4__this_2;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::<t>5__1
	float ___U3CtU3E5__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ed__4_t3986606630, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ed__4_t3986606630, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ed__4_t3986606630, ___U3CU3E4__this_2)); }
	inline LerpControlledBob_t1895875871 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LerpControlledBob_t1895875871 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LerpControlledBob_t1895875871 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ed__4_t3986606630, ___U3CtU3E5__1_3)); }
	inline float get_U3CtU3E5__1_3() const { return ___U3CtU3E5__1_3; }
	inline float* get_address_of_U3CtU3E5__1_3() { return &___U3CtU3E5__1_3; }
	inline void set_U3CtU3E5__1_3(float value)
	{
		___U3CtU3E5__1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOBOBCYCLEU3ED__4_T3986606630_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef LERPCONTROLLEDBOB_T1895875871_H
#define LERPCONTROLLEDBOB_T1895875871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.LerpControlledBob
struct  LerpControlledBob_t1895875871  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::BobDuration
	float ___BobDuration_0;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::BobAmount
	float ___BobAmount_1;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::m_Offset
	float ___m_Offset_2;

public:
	inline static int32_t get_offset_of_BobDuration_0() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___BobDuration_0)); }
	inline float get_BobDuration_0() const { return ___BobDuration_0; }
	inline float* get_address_of_BobDuration_0() { return &___BobDuration_0; }
	inline void set_BobDuration_0(float value)
	{
		___BobDuration_0 = value;
	}

	inline static int32_t get_offset_of_BobAmount_1() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___BobAmount_1)); }
	inline float get_BobAmount_1() const { return ___BobAmount_1; }
	inline float* get_address_of_BobAmount_1() { return &___BobAmount_1; }
	inline void set_BobAmount_1(float value)
	{
		___BobAmount_1 = value;
	}

	inline static int32_t get_offset_of_m_Offset_2() { return static_cast<int32_t>(offsetof(LerpControlledBob_t1895875871, ___m_Offset_2)); }
	inline float get_m_Offset_2() const { return ___m_Offset_2; }
	inline float* get_address_of_m_Offset_2() { return &___m_Offset_2; }
	inline void set_m_Offset_2(float value)
	{
		___m_Offset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LERPCONTROLLEDBOB_T1895875871_H
#ifndef U3CFOVKICKDOWNU3ED__10_T494950160_H
#define U3CFOVKICKDOWNU3ED__10_T494950160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10
struct  U3CFOVKickDownU3Ed__10_t494950160  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::<>4__this
	FOVKick_t120370150 * ___U3CU3E4__this_2;
	// System.Single UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::<t>5__1
	float ___U3CtU3E5__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ed__10_t494950160, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ed__10_t494950160, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ed__10_t494950160, ___U3CU3E4__this_2)); }
	inline FOVKick_t120370150 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FOVKick_t120370150 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FOVKick_t120370150 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ed__10_t494950160, ___U3CtU3E5__1_3)); }
	inline float get_U3CtU3E5__1_3() const { return ___U3CtU3E5__1_3; }
	inline float* get_address_of_U3CtU3E5__1_3() { return &___U3CtU3E5__1_3; }
	inline void set_U3CtU3E5__1_3(float value)
	{
		___U3CtU3E5__1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOVKICKDOWNU3ED__10_T494950160_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef PATHRESULT_T1528013228_H
#define PATHRESULT_T1528013228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PathResult
struct  PathResult_t1528013228 
{
public:
	// UnityEngine.Vector3[] PathResult::path
	Vector3U5BU5D_t1718750761* ___path_0;
	// System.Boolean PathResult::success
	bool ___success_1;
	// System.Action`2<UnityEngine.Vector3[],System.Boolean> PathResult::callback
	Action_2_t1484661638 * ___callback_2;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(PathResult_t1528013228, ___path_0)); }
	inline Vector3U5BU5D_t1718750761* get_path_0() const { return ___path_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(Vector3U5BU5D_t1718750761* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier((&___path_0), value);
	}

	inline static int32_t get_offset_of_success_1() { return static_cast<int32_t>(offsetof(PathResult_t1528013228, ___success_1)); }
	inline bool get_success_1() const { return ___success_1; }
	inline bool* get_address_of_success_1() { return &___success_1; }
	inline void set_success_1(bool value)
	{
		___success_1 = value;
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(PathResult_t1528013228, ___callback_2)); }
	inline Action_2_t1484661638 * get_callback_2() const { return ___callback_2; }
	inline Action_2_t1484661638 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_2_t1484661638 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PathResult
struct PathResult_t1528013228_marshaled_pinvoke
{
	Vector3_t3722313464 * ___path_0;
	int32_t ___success_1;
	Il2CppMethodPointer ___callback_2;
};
// Native definition for COM marshalling of PathResult
struct PathResult_t1528013228_marshaled_com
{
	Vector3_t3722313464 * ___path_0;
	int32_t ___success_1;
	Il2CppMethodPointer ___callback_2;
};
#endif // PATHRESULT_T1528013228_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef LINE_T3796957314_H
#define LINE_T3796957314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Line
struct  Line_t3796957314 
{
public:
	// System.Single Line::gradient
	float ___gradient_1;
	// System.Single Line::yIntercept
	float ___yIntercept_2;
	// UnityEngine.Vector2 Line::pointOnLine_1
	Vector2_t2156229523  ___pointOnLine_1_3;
	// UnityEngine.Vector2 Line::pointOnLine_2
	Vector2_t2156229523  ___pointOnLine_2_4;
	// System.Single Line::gradientPerpendicular
	float ___gradientPerpendicular_5;
	// System.Boolean Line::approachSide
	bool ___approachSide_6;

public:
	inline static int32_t get_offset_of_gradient_1() { return static_cast<int32_t>(offsetof(Line_t3796957314, ___gradient_1)); }
	inline float get_gradient_1() const { return ___gradient_1; }
	inline float* get_address_of_gradient_1() { return &___gradient_1; }
	inline void set_gradient_1(float value)
	{
		___gradient_1 = value;
	}

	inline static int32_t get_offset_of_yIntercept_2() { return static_cast<int32_t>(offsetof(Line_t3796957314, ___yIntercept_2)); }
	inline float get_yIntercept_2() const { return ___yIntercept_2; }
	inline float* get_address_of_yIntercept_2() { return &___yIntercept_2; }
	inline void set_yIntercept_2(float value)
	{
		___yIntercept_2 = value;
	}

	inline static int32_t get_offset_of_pointOnLine_1_3() { return static_cast<int32_t>(offsetof(Line_t3796957314, ___pointOnLine_1_3)); }
	inline Vector2_t2156229523  get_pointOnLine_1_3() const { return ___pointOnLine_1_3; }
	inline Vector2_t2156229523 * get_address_of_pointOnLine_1_3() { return &___pointOnLine_1_3; }
	inline void set_pointOnLine_1_3(Vector2_t2156229523  value)
	{
		___pointOnLine_1_3 = value;
	}

	inline static int32_t get_offset_of_pointOnLine_2_4() { return static_cast<int32_t>(offsetof(Line_t3796957314, ___pointOnLine_2_4)); }
	inline Vector2_t2156229523  get_pointOnLine_2_4() const { return ___pointOnLine_2_4; }
	inline Vector2_t2156229523 * get_address_of_pointOnLine_2_4() { return &___pointOnLine_2_4; }
	inline void set_pointOnLine_2_4(Vector2_t2156229523  value)
	{
		___pointOnLine_2_4 = value;
	}

	inline static int32_t get_offset_of_gradientPerpendicular_5() { return static_cast<int32_t>(offsetof(Line_t3796957314, ___gradientPerpendicular_5)); }
	inline float get_gradientPerpendicular_5() const { return ___gradientPerpendicular_5; }
	inline float* get_address_of_gradientPerpendicular_5() { return &___gradientPerpendicular_5; }
	inline void set_gradientPerpendicular_5(float value)
	{
		___gradientPerpendicular_5 = value;
	}

	inline static int32_t get_offset_of_approachSide_6() { return static_cast<int32_t>(offsetof(Line_t3796957314, ___approachSide_6)); }
	inline bool get_approachSide_6() const { return ___approachSide_6; }
	inline bool* get_address_of_approachSide_6() { return &___approachSide_6; }
	inline void set_approachSide_6(bool value)
	{
		___approachSide_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Line
struct Line_t3796957314_marshaled_pinvoke
{
	float ___gradient_1;
	float ___yIntercept_2;
	Vector2_t2156229523  ___pointOnLine_1_3;
	Vector2_t2156229523  ___pointOnLine_2_4;
	float ___gradientPerpendicular_5;
	int32_t ___approachSide_6;
};
// Native definition for COM marshalling of Line
struct Line_t3796957314_marshaled_com
{
	float ___gradient_1;
	float ___yIntercept_2;
	Vector2_t2156229523  ___pointOnLine_1_3;
	Vector2_t2156229523  ___pointOnLine_2_4;
	float ___gradientPerpendicular_5;
	int32_t ___approachSide_6;
};
#endif // LINE_T3796957314_H
#ifndef U3CUPDATEPATHU3ED__10_T1880251700_H
#define U3CUPDATEPATHU3ED__10_T1880251700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unit/<UpdatePath>d__10
struct  U3CUpdatePathU3Ed__10_t1880251700  : public RuntimeObject
{
public:
	// System.Int32 Unit/<UpdatePath>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Unit/<UpdatePath>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Unit Unit/<UpdatePath>d__10::<>4__this
	Unit_t4139495810 * ___U3CU3E4__this_2;
	// UnityEngine.Vector3 Unit/<UpdatePath>d__10::<targetPosOld>5__1
	Vector3_t3722313464  ___U3CtargetPosOldU3E5__1_3;
	// System.Single Unit/<UpdatePath>d__10::<squareMoveThreshhold>5__2
	float ___U3CsquareMoveThreshholdU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CUpdatePathU3Ed__10_t1880251700, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CUpdatePathU3Ed__10_t1880251700, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CUpdatePathU3Ed__10_t1880251700, ___U3CU3E4__this_2)); }
	inline Unit_t4139495810 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Unit_t4139495810 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Unit_t4139495810 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CtargetPosOldU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CUpdatePathU3Ed__10_t1880251700, ___U3CtargetPosOldU3E5__1_3)); }
	inline Vector3_t3722313464  get_U3CtargetPosOldU3E5__1_3() const { return ___U3CtargetPosOldU3E5__1_3; }
	inline Vector3_t3722313464 * get_address_of_U3CtargetPosOldU3E5__1_3() { return &___U3CtargetPosOldU3E5__1_3; }
	inline void set_U3CtargetPosOldU3E5__1_3(Vector3_t3722313464  value)
	{
		___U3CtargetPosOldU3E5__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CsquareMoveThreshholdU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CUpdatePathU3Ed__10_t1880251700, ___U3CsquareMoveThreshholdU3E5__2_4)); }
	inline float get_U3CsquareMoveThreshholdU3E5__2_4() const { return ___U3CsquareMoveThreshholdU3E5__2_4; }
	inline float* get_address_of_U3CsquareMoveThreshholdU3E5__2_4() { return &___U3CsquareMoveThreshholdU3E5__2_4; }
	inline void set_U3CsquareMoveThreshholdU3E5__2_4(float value)
	{
		___U3CsquareMoveThreshholdU3E5__2_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEPATHU3ED__10_T1880251700_H
#ifndef TERRAINTYPE_T3627529997_H
#define TERRAINTYPE_T3627529997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Grid/TerrainType
struct  TerrainType_t3627529997  : public RuntimeObject
{
public:
	// UnityEngine.LayerMask Grid/TerrainType::terrainMask
	LayerMask_t3493934918  ___terrainMask_0;
	// System.Int32 Grid/TerrainType::terrainPenalty
	int32_t ___terrainPenalty_1;

public:
	inline static int32_t get_offset_of_terrainMask_0() { return static_cast<int32_t>(offsetof(TerrainType_t3627529997, ___terrainMask_0)); }
	inline LayerMask_t3493934918  get_terrainMask_0() const { return ___terrainMask_0; }
	inline LayerMask_t3493934918 * get_address_of_terrainMask_0() { return &___terrainMask_0; }
	inline void set_terrainMask_0(LayerMask_t3493934918  value)
	{
		___terrainMask_0 = value;
	}

	inline static int32_t get_offset_of_terrainPenalty_1() { return static_cast<int32_t>(offsetof(TerrainType_t3627529997, ___terrainPenalty_1)); }
	inline int32_t get_terrainPenalty_1() const { return ___terrainPenalty_1; }
	inline int32_t* get_address_of_terrainPenalty_1() { return &___terrainPenalty_1; }
	inline void set_terrainPenalty_1(int32_t value)
	{
		___terrainPenalty_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TERRAINTYPE_T3627529997_H
#ifndef NODE_T2989995042_H
#define NODE_T2989995042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Node
struct  Node_t2989995042  : public RuntimeObject
{
public:
	// System.Boolean Node::walkable
	bool ___walkable_0;
	// UnityEngine.Vector3 Node::worldPosition
	Vector3_t3722313464  ___worldPosition_1;
	// System.Int32 Node::gridX
	int32_t ___gridX_2;
	// System.Int32 Node::gridY
	int32_t ___gridY_3;
	// System.Int32 Node::movementPenalty
	int32_t ___movementPenalty_4;
	// System.Int32 Node::gCost
	int32_t ___gCost_5;
	// System.Int32 Node::hCost
	int32_t ___hCost_6;
	// Node Node::parent
	Node_t2989995042 * ___parent_7;
	// System.Int32 Node::heapIndex
	int32_t ___heapIndex_8;

public:
	inline static int32_t get_offset_of_walkable_0() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___walkable_0)); }
	inline bool get_walkable_0() const { return ___walkable_0; }
	inline bool* get_address_of_walkable_0() { return &___walkable_0; }
	inline void set_walkable_0(bool value)
	{
		___walkable_0 = value;
	}

	inline static int32_t get_offset_of_worldPosition_1() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___worldPosition_1)); }
	inline Vector3_t3722313464  get_worldPosition_1() const { return ___worldPosition_1; }
	inline Vector3_t3722313464 * get_address_of_worldPosition_1() { return &___worldPosition_1; }
	inline void set_worldPosition_1(Vector3_t3722313464  value)
	{
		___worldPosition_1 = value;
	}

	inline static int32_t get_offset_of_gridX_2() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___gridX_2)); }
	inline int32_t get_gridX_2() const { return ___gridX_2; }
	inline int32_t* get_address_of_gridX_2() { return &___gridX_2; }
	inline void set_gridX_2(int32_t value)
	{
		___gridX_2 = value;
	}

	inline static int32_t get_offset_of_gridY_3() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___gridY_3)); }
	inline int32_t get_gridY_3() const { return ___gridY_3; }
	inline int32_t* get_address_of_gridY_3() { return &___gridY_3; }
	inline void set_gridY_3(int32_t value)
	{
		___gridY_3 = value;
	}

	inline static int32_t get_offset_of_movementPenalty_4() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___movementPenalty_4)); }
	inline int32_t get_movementPenalty_4() const { return ___movementPenalty_4; }
	inline int32_t* get_address_of_movementPenalty_4() { return &___movementPenalty_4; }
	inline void set_movementPenalty_4(int32_t value)
	{
		___movementPenalty_4 = value;
	}

	inline static int32_t get_offset_of_gCost_5() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___gCost_5)); }
	inline int32_t get_gCost_5() const { return ___gCost_5; }
	inline int32_t* get_address_of_gCost_5() { return &___gCost_5; }
	inline void set_gCost_5(int32_t value)
	{
		___gCost_5 = value;
	}

	inline static int32_t get_offset_of_hCost_6() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___hCost_6)); }
	inline int32_t get_hCost_6() const { return ___hCost_6; }
	inline int32_t* get_address_of_hCost_6() { return &___hCost_6; }
	inline void set_hCost_6(int32_t value)
	{
		___hCost_6 = value;
	}

	inline static int32_t get_offset_of_parent_7() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___parent_7)); }
	inline Node_t2989995042 * get_parent_7() const { return ___parent_7; }
	inline Node_t2989995042 ** get_address_of_parent_7() { return &___parent_7; }
	inline void set_parent_7(Node_t2989995042 * value)
	{
		___parent_7 = value;
		Il2CppCodeGenWriteBarrier((&___parent_7), value);
	}

	inline static int32_t get_offset_of_heapIndex_8() { return static_cast<int32_t>(offsetof(Node_t2989995042, ___heapIndex_8)); }
	inline int32_t get_heapIndex_8() const { return ___heapIndex_8; }
	inline int32_t* get_address_of_heapIndex_8() { return &___heapIndex_8; }
	inline void set_heapIndex_8(int32_t value)
	{
		___heapIndex_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODE_T2989995042_H
#ifndef PATHREQUEST_T2117613800_H
#define PATHREQUEST_T2117613800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PathRequest
struct  PathRequest_t2117613800 
{
public:
	// UnityEngine.Vector3 PathRequest::pathStart
	Vector3_t3722313464  ___pathStart_0;
	// UnityEngine.Vector3 PathRequest::pathEnd
	Vector3_t3722313464  ___pathEnd_1;
	// System.Action`2<UnityEngine.Vector3[],System.Boolean> PathRequest::callback
	Action_2_t1484661638 * ___callback_2;

public:
	inline static int32_t get_offset_of_pathStart_0() { return static_cast<int32_t>(offsetof(PathRequest_t2117613800, ___pathStart_0)); }
	inline Vector3_t3722313464  get_pathStart_0() const { return ___pathStart_0; }
	inline Vector3_t3722313464 * get_address_of_pathStart_0() { return &___pathStart_0; }
	inline void set_pathStart_0(Vector3_t3722313464  value)
	{
		___pathStart_0 = value;
	}

	inline static int32_t get_offset_of_pathEnd_1() { return static_cast<int32_t>(offsetof(PathRequest_t2117613800, ___pathEnd_1)); }
	inline Vector3_t3722313464  get_pathEnd_1() const { return ___pathEnd_1; }
	inline Vector3_t3722313464 * get_address_of_pathEnd_1() { return &___pathEnd_1; }
	inline void set_pathEnd_1(Vector3_t3722313464  value)
	{
		___pathEnd_1 = value;
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(PathRequest_t2117613800, ___callback_2)); }
	inline Action_2_t1484661638 * get_callback_2() const { return ___callback_2; }
	inline Action_2_t1484661638 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_2_t1484661638 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PathRequest
struct PathRequest_t2117613800_marshaled_pinvoke
{
	Vector3_t3722313464  ___pathStart_0;
	Vector3_t3722313464  ___pathEnd_1;
	Il2CppMethodPointer ___callback_2;
};
// Native definition for COM marshalling of PathRequest
struct PathRequest_t2117613800_marshaled_com
{
	Vector3_t3722313464  ___pathStart_0;
	Vector3_t3722313464  ___pathEnd_1;
	Il2CppMethodPointer ___callback_2;
};
#endif // PATHREQUEST_T2117613800_H
#ifndef AISTATES_T2852279327_H
#define AISTATES_T2852279327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AIStateMachine/AIStates
struct  AIStates_t2852279327 
{
public:
	// System.Int32 AIStateMachine/AIStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AIStates_t2852279327, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AISTATES_T2852279327_H
#ifndef BUILDTARGETGROUP_T72322187_H
#define BUILDTARGETGROUP_T72322187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup
struct  BuildTargetGroup_t72322187 
{
public:
	// System.Int32 UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BuildTargetGroup_t72322187, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDTARGETGROUP_T72322187_H
#ifndef SPACE_T654135784_H
#define SPACE_T654135784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t654135784 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Space_t654135784, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T654135784_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef FUZZYSTATES_T3314355612_H
#define FUZZYSTATES_T3314355612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyFuzzyState/FuzzyStates
struct  FuzzyStates_t3314355612 
{
public:
	// System.Int32 EnemyFuzzyState/FuzzyStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FuzzyStates_t3314355612, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUZZYSTATES_T3314355612_H
#ifndef SLOTTYPE_T413958767_H
#define SLOTTYPE_T413958767_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventorySlot/SlotType
struct  SlotType_t413958767 
{
public:
	// System.Int32 InventorySlot/SlotType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SlotType_t413958767, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTTYPE_T413958767_H
#ifndef CAMERAREFOCUS_T4263235746_H
#define CAMERAREFOCUS_T4263235746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.CameraRefocus
struct  CameraRefocus_t4263235746  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityStandardAssets.Utility.CameraRefocus::Camera
	Camera_t4157153871 * ___Camera_0;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CameraRefocus::Lookatpoint
	Vector3_t3722313464  ___Lookatpoint_1;
	// UnityEngine.Transform UnityStandardAssets.Utility.CameraRefocus::Parent
	Transform_t3600365921 * ___Parent_2;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CameraRefocus::m_OrigCameraPos
	Vector3_t3722313464  ___m_OrigCameraPos_3;
	// System.Boolean UnityStandardAssets.Utility.CameraRefocus::m_Refocus
	bool ___m_Refocus_4;

public:
	inline static int32_t get_offset_of_Camera_0() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Camera_0)); }
	inline Camera_t4157153871 * get_Camera_0() const { return ___Camera_0; }
	inline Camera_t4157153871 ** get_address_of_Camera_0() { return &___Camera_0; }
	inline void set_Camera_0(Camera_t4157153871 * value)
	{
		___Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_0), value);
	}

	inline static int32_t get_offset_of_Lookatpoint_1() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Lookatpoint_1)); }
	inline Vector3_t3722313464  get_Lookatpoint_1() const { return ___Lookatpoint_1; }
	inline Vector3_t3722313464 * get_address_of_Lookatpoint_1() { return &___Lookatpoint_1; }
	inline void set_Lookatpoint_1(Vector3_t3722313464  value)
	{
		___Lookatpoint_1 = value;
	}

	inline static int32_t get_offset_of_Parent_2() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___Parent_2)); }
	inline Transform_t3600365921 * get_Parent_2() const { return ___Parent_2; }
	inline Transform_t3600365921 ** get_address_of_Parent_2() { return &___Parent_2; }
	inline void set_Parent_2(Transform_t3600365921 * value)
	{
		___Parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_2), value);
	}

	inline static int32_t get_offset_of_m_OrigCameraPos_3() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___m_OrigCameraPos_3)); }
	inline Vector3_t3722313464  get_m_OrigCameraPos_3() const { return ___m_OrigCameraPos_3; }
	inline Vector3_t3722313464 * get_address_of_m_OrigCameraPos_3() { return &___m_OrigCameraPos_3; }
	inline void set_m_OrigCameraPos_3(Vector3_t3722313464  value)
	{
		___m_OrigCameraPos_3 = value;
	}

	inline static int32_t get_offset_of_m_Refocus_4() { return static_cast<int32_t>(offsetof(CameraRefocus_t4263235746, ___m_Refocus_4)); }
	inline bool get_m_Refocus_4() const { return ___m_Refocus_4; }
	inline bool* get_address_of_m_Refocus_4() { return &___m_Refocus_4; }
	inline void set_m_Refocus_4(bool value)
	{
		___m_Refocus_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAREFOCUS_T4263235746_H
#ifndef ROUTEPOINT_T3880028948_H
#define ROUTEPOINT_T3880028948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit/RoutePoint
struct  RoutePoint_t3880028948 
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::position
	Vector3_t3722313464  ___position_0;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit/RoutePoint::direction
	Vector3_t3722313464  ___direction_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(RoutePoint_t3880028948, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_direction_1() { return static_cast<int32_t>(offsetof(RoutePoint_t3880028948, ___direction_1)); }
	inline Vector3_t3722313464  get_direction_1() const { return ___direction_1; }
	inline Vector3_t3722313464 * get_address_of_direction_1() { return &___direction_1; }
	inline void set_direction_1(Vector3_t3722313464  value)
	{
		___direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROUTEPOINT_T3880028948_H
#ifndef CURVECONTROLLEDBOB_T2679313829_H
#define CURVECONTROLLEDBOB_T2679313829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.CurveControlledBob
struct  CurveControlledBob_t2679313829  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::HorizontalBobRange
	float ___HorizontalBobRange_0;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::VerticalBobRange
	float ___VerticalBobRange_1;
	// UnityEngine.AnimationCurve UnityStandardAssets.Utility.CurveControlledBob::Bobcurve
	AnimationCurve_t3046754366 * ___Bobcurve_2;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::VerticaltoHorizontalRatio
	float ___VerticaltoHorizontalRatio_3;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_CyclePositionX
	float ___m_CyclePositionX_4;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_CyclePositionY
	float ___m_CyclePositionY_5;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_BobBaseInterval
	float ___m_BobBaseInterval_6;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CurveControlledBob::m_OriginalCameraPosition
	Vector3_t3722313464  ___m_OriginalCameraPosition_7;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_Time
	float ___m_Time_8;

public:
	inline static int32_t get_offset_of_HorizontalBobRange_0() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___HorizontalBobRange_0)); }
	inline float get_HorizontalBobRange_0() const { return ___HorizontalBobRange_0; }
	inline float* get_address_of_HorizontalBobRange_0() { return &___HorizontalBobRange_0; }
	inline void set_HorizontalBobRange_0(float value)
	{
		___HorizontalBobRange_0 = value;
	}

	inline static int32_t get_offset_of_VerticalBobRange_1() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___VerticalBobRange_1)); }
	inline float get_VerticalBobRange_1() const { return ___VerticalBobRange_1; }
	inline float* get_address_of_VerticalBobRange_1() { return &___VerticalBobRange_1; }
	inline void set_VerticalBobRange_1(float value)
	{
		___VerticalBobRange_1 = value;
	}

	inline static int32_t get_offset_of_Bobcurve_2() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___Bobcurve_2)); }
	inline AnimationCurve_t3046754366 * get_Bobcurve_2() const { return ___Bobcurve_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_Bobcurve_2() { return &___Bobcurve_2; }
	inline void set_Bobcurve_2(AnimationCurve_t3046754366 * value)
	{
		___Bobcurve_2 = value;
		Il2CppCodeGenWriteBarrier((&___Bobcurve_2), value);
	}

	inline static int32_t get_offset_of_VerticaltoHorizontalRatio_3() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___VerticaltoHorizontalRatio_3)); }
	inline float get_VerticaltoHorizontalRatio_3() const { return ___VerticaltoHorizontalRatio_3; }
	inline float* get_address_of_VerticaltoHorizontalRatio_3() { return &___VerticaltoHorizontalRatio_3; }
	inline void set_VerticaltoHorizontalRatio_3(float value)
	{
		___VerticaltoHorizontalRatio_3 = value;
	}

	inline static int32_t get_offset_of_m_CyclePositionX_4() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_CyclePositionX_4)); }
	inline float get_m_CyclePositionX_4() const { return ___m_CyclePositionX_4; }
	inline float* get_address_of_m_CyclePositionX_4() { return &___m_CyclePositionX_4; }
	inline void set_m_CyclePositionX_4(float value)
	{
		___m_CyclePositionX_4 = value;
	}

	inline static int32_t get_offset_of_m_CyclePositionY_5() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_CyclePositionY_5)); }
	inline float get_m_CyclePositionY_5() const { return ___m_CyclePositionY_5; }
	inline float* get_address_of_m_CyclePositionY_5() { return &___m_CyclePositionY_5; }
	inline void set_m_CyclePositionY_5(float value)
	{
		___m_CyclePositionY_5 = value;
	}

	inline static int32_t get_offset_of_m_BobBaseInterval_6() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_BobBaseInterval_6)); }
	inline float get_m_BobBaseInterval_6() const { return ___m_BobBaseInterval_6; }
	inline float* get_address_of_m_BobBaseInterval_6() { return &___m_BobBaseInterval_6; }
	inline void set_m_BobBaseInterval_6(float value)
	{
		___m_BobBaseInterval_6 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_7() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_OriginalCameraPosition_7)); }
	inline Vector3_t3722313464  get_m_OriginalCameraPosition_7() const { return ___m_OriginalCameraPosition_7; }
	inline Vector3_t3722313464 * get_address_of_m_OriginalCameraPosition_7() { return &___m_OriginalCameraPosition_7; }
	inline void set_m_OriginalCameraPosition_7(Vector3_t3722313464  value)
	{
		___m_OriginalCameraPosition_7 = value;
	}

	inline static int32_t get_offset_of_m_Time_8() { return static_cast<int32_t>(offsetof(CurveControlledBob_t2679313829, ___m_Time_8)); }
	inline float get_m_Time_8() const { return ___m_Time_8; }
	inline float* get_address_of_m_Time_8() { return &___m_Time_8; }
	inline void set_m_Time_8(float value)
	{
		___m_Time_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVECONTROLLEDBOB_T2679313829_H
#ifndef MODE_T3024470803_H
#define MODE_T3024470803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ActivateTrigger/Mode
struct  Mode_t3024470803 
{
public:
	// System.Int32 UnityStandardAssets.Utility.ActivateTrigger/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3024470803, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3024470803_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef GUNTYPE_T2644971432_H
#define GUNTYPE_T2644971432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gun/GunType
struct  GunType_t2644971432 
{
public:
	// System.Int32 Gun/GunType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GunType_t2644971432, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUNTYPE_T2644971432_H
#ifndef ACTION_T837364808_H
#define ACTION_T837364808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Action
struct  Action_t837364808 
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/Action::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Action_t837364808, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T837364808_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef ENTRY_T2725803170_H
#define ENTRY_T2725803170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Entry
struct  Entry_t2725803170  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityStandardAssets.Utility.TimedObjectActivator/Entry::target
	GameObject_t1113636619 * ___target_0;
	// UnityStandardAssets.Utility.TimedObjectActivator/Action UnityStandardAssets.Utility.TimedObjectActivator/Entry::action
	int32_t ___action_1;
	// System.Single UnityStandardAssets.Utility.TimedObjectActivator/Entry::delay
	float ___delay_2;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(Entry_t2725803170, ___target_0)); }
	inline GameObject_t1113636619 * get_target_0() const { return ___target_0; }
	inline GameObject_t1113636619 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(GameObject_t1113636619 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(Entry_t2725803170, ___action_1)); }
	inline int32_t get_action_1() const { return ___action_1; }
	inline int32_t* get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(int32_t value)
	{
		___action_1 = value;
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(Entry_t2725803170, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T2725803170_H
#ifndef VECTOR3ANDSPACE_T219844479_H
#define VECTOR3ANDSPACE_T219844479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct  Vector3andSpace_t219844479  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::value
	Vector3_t3722313464  ___value_0;
	// UnityEngine.Space UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::space
	int32_t ___space_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Vector3andSpace_t219844479, ___value_0)); }
	inline Vector3_t3722313464  get_value_0() const { return ___value_0; }
	inline Vector3_t3722313464 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_t3722313464  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_space_1() { return static_cast<int32_t>(offsetof(Vector3andSpace_t219844479, ___space_1)); }
	inline int32_t get_space_1() const { return ___space_1; }
	inline int32_t* get_address_of_space_1() { return &___space_1; }
	inline void set_space_1(int32_t value)
	{
		___space_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3ANDSPACE_T219844479_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T786576806_H
#define U3CU3EC__DISPLAYCLASS5_0_T786576806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PathRequestManager/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t786576806  : public RuntimeObject
{
public:
	// PathRequest PathRequestManager/<>c__DisplayClass5_0::request
	PathRequest_t2117613800  ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t786576806, ___request_0)); }
	inline PathRequest_t2117613800  get_request_0() const { return ___request_0; }
	inline PathRequest_t2117613800 * get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(PathRequest_t2117613800  value)
	{
		___request_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T786576806_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ONITEMCHANGED_T22848112_H
#define ONITEMCHANGED_T22848112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Inventory/OnItemChanged
struct  OnItemChanged_t22848112  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONITEMCHANGED_T22848112_H
#ifndef ITEM_T2953980098_H
#define ITEM_T2953980098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Item
struct  Item_t2953980098  : public ScriptableObject_t2528358522
{
public:
	// System.String Item::name
	String_t* ___name_2;
	// UnityEngine.Sprite Item::icon
	Sprite_t280657092 * ___icon_3;
	// System.Boolean Item::isWeapon
	bool ___isWeapon_4;
	// System.Boolean Item::isDefaultItem
	bool ___isDefaultItem_5;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Item_t2953980098, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_icon_3() { return static_cast<int32_t>(offsetof(Item_t2953980098, ___icon_3)); }
	inline Sprite_t280657092 * get_icon_3() const { return ___icon_3; }
	inline Sprite_t280657092 ** get_address_of_icon_3() { return &___icon_3; }
	inline void set_icon_3(Sprite_t280657092 * value)
	{
		___icon_3 = value;
		Il2CppCodeGenWriteBarrier((&___icon_3), value);
	}

	inline static int32_t get_offset_of_isWeapon_4() { return static_cast<int32_t>(offsetof(Item_t2953980098, ___isWeapon_4)); }
	inline bool get_isWeapon_4() const { return ___isWeapon_4; }
	inline bool* get_address_of_isWeapon_4() { return &___isWeapon_4; }
	inline void set_isWeapon_4(bool value)
	{
		___isWeapon_4 = value;
	}

	inline static int32_t get_offset_of_isDefaultItem_5() { return static_cast<int32_t>(offsetof(Item_t2953980098, ___isDefaultItem_5)); }
	inline bool get_isDefaultItem_5() const { return ___isDefaultItem_5; }
	inline bool* get_address_of_isDefaultItem_5() { return &___isDefaultItem_5; }
	inline void set_isDefaultItem_5(bool value)
	{
		___isDefaultItem_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEM_T2953980098_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef TIMEDOBJECTDESTRUCTOR_T3438860414_H
#define TIMEDOBJECTDESTRUCTOR_T3438860414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectDestructor
struct  TimedObjectDestructor_t3438860414  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Utility.TimedObjectDestructor::m_TimeOut
	float ___m_TimeOut_2;
	// System.Boolean UnityStandardAssets.Utility.TimedObjectDestructor::m_DetachChildren
	bool ___m_DetachChildren_3;

public:
	inline static int32_t get_offset_of_m_TimeOut_2() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t3438860414, ___m_TimeOut_2)); }
	inline float get_m_TimeOut_2() const { return ___m_TimeOut_2; }
	inline float* get_address_of_m_TimeOut_2() { return &___m_TimeOut_2; }
	inline void set_m_TimeOut_2(float value)
	{
		___m_TimeOut_2 = value;
	}

	inline static int32_t get_offset_of_m_DetachChildren_3() { return static_cast<int32_t>(offsetof(TimedObjectDestructor_t3438860414, ___m_DetachChildren_3)); }
	inline bool get_m_DetachChildren_3() const { return ___m_DetachChildren_3; }
	inline bool* get_address_of_m_DetachChildren_3() { return &___m_DetachChildren_3; }
	inline void set_m_DetachChildren_3(bool value)
	{
		___m_DetachChildren_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTDESTRUCTOR_T3438860414_H
#ifndef PARTICLESYSTEMDESTROYER_T558680695_H
#define PARTICLESYSTEMDESTROYER_T558680695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct  ParticleSystemDestroyer_t558680695  : public MonoBehaviour_t3962482529
{
public:
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::minDuration
	float ___minDuration_2;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::maxDuration
	float ___maxDuration_3;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::m_MaxLifetime
	float ___m_MaxLifetime_4;
	// System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer::m_EarlyStop
	bool ___m_EarlyStop_5;

public:
	inline static int32_t get_offset_of_minDuration_2() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___minDuration_2)); }
	inline float get_minDuration_2() const { return ___minDuration_2; }
	inline float* get_address_of_minDuration_2() { return &___minDuration_2; }
	inline void set_minDuration_2(float value)
	{
		___minDuration_2 = value;
	}

	inline static int32_t get_offset_of_maxDuration_3() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___maxDuration_3)); }
	inline float get_maxDuration_3() const { return ___maxDuration_3; }
	inline float* get_address_of_maxDuration_3() { return &___maxDuration_3; }
	inline void set_maxDuration_3(float value)
	{
		___maxDuration_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxLifetime_4() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___m_MaxLifetime_4)); }
	inline float get_m_MaxLifetime_4() const { return ___m_MaxLifetime_4; }
	inline float* get_address_of_m_MaxLifetime_4() { return &___m_MaxLifetime_4; }
	inline void set_m_MaxLifetime_4(float value)
	{
		___m_MaxLifetime_4 = value;
	}

	inline static int32_t get_offset_of_m_EarlyStop_5() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t558680695, ___m_EarlyStop_5)); }
	inline bool get_m_EarlyStop_5() const { return ___m_EarlyStop_5; }
	inline bool* get_address_of_m_EarlyStop_5() { return &___m_EarlyStop_5; }
	inline void set_m_EarlyStop_5(bool value)
	{
		___m_EarlyStop_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMDESTROYER_T558680695_H
#ifndef BULLET_T1042140031_H
#define BULLET_T1042140031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bullet
struct  Bullet_t1042140031  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Bullet::lifeTime
	float ___lifeTime_2;
	// UnityEngine.Material Bullet::material
	Material_t340375123 * ___material_3;
	// UnityEngine.Color Bullet::originalCol
	Color_t2555686324  ___originalCol_4;
	// System.Single Bullet::fadePercent
	float ___fadePercent_5;
	// System.Single Bullet::disposeTime
	float ___disposeTime_6;
	// System.Boolean Bullet::fading
	bool ___fading_7;

public:
	inline static int32_t get_offset_of_lifeTime_2() { return static_cast<int32_t>(offsetof(Bullet_t1042140031, ___lifeTime_2)); }
	inline float get_lifeTime_2() const { return ___lifeTime_2; }
	inline float* get_address_of_lifeTime_2() { return &___lifeTime_2; }
	inline void set_lifeTime_2(float value)
	{
		___lifeTime_2 = value;
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(Bullet_t1042140031, ___material_3)); }
	inline Material_t340375123 * get_material_3() const { return ___material_3; }
	inline Material_t340375123 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t340375123 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_originalCol_4() { return static_cast<int32_t>(offsetof(Bullet_t1042140031, ___originalCol_4)); }
	inline Color_t2555686324  get_originalCol_4() const { return ___originalCol_4; }
	inline Color_t2555686324 * get_address_of_originalCol_4() { return &___originalCol_4; }
	inline void set_originalCol_4(Color_t2555686324  value)
	{
		___originalCol_4 = value;
	}

	inline static int32_t get_offset_of_fadePercent_5() { return static_cast<int32_t>(offsetof(Bullet_t1042140031, ___fadePercent_5)); }
	inline float get_fadePercent_5() const { return ___fadePercent_5; }
	inline float* get_address_of_fadePercent_5() { return &___fadePercent_5; }
	inline void set_fadePercent_5(float value)
	{
		___fadePercent_5 = value;
	}

	inline static int32_t get_offset_of_disposeTime_6() { return static_cast<int32_t>(offsetof(Bullet_t1042140031, ___disposeTime_6)); }
	inline float get_disposeTime_6() const { return ___disposeTime_6; }
	inline float* get_address_of_disposeTime_6() { return &___disposeTime_6; }
	inline void set_disposeTime_6(float value)
	{
		___disposeTime_6 = value;
	}

	inline static int32_t get_offset_of_fading_7() { return static_cast<int32_t>(offsetof(Bullet_t1042140031, ___fading_7)); }
	inline bool get_fading_7() const { return ___fading_7; }
	inline bool* get_address_of_fading_7() { return &___fading_7; }
	inline void set_fading_7(bool value)
	{
		___fading_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BULLET_T1042140031_H
#ifndef SMOOTHFOLLOW_T4204731361_H
#define SMOOTHFOLLOW_T4204731361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SmoothFollow
struct  SmoothFollow_t4204731361  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Utility.SmoothFollow::target
	Transform_t3600365921 * ___target_2;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::distance
	float ___distance_3;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::height
	float ___height_4;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::rotationDamping
	float ___rotationDamping_5;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::heightDamping
	float ___heightDamping_6;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___target_2)); }
	inline Transform_t3600365921 * get_target_2() const { return ___target_2; }
	inline Transform_t3600365921 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3600365921 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_distance_3() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___distance_3)); }
	inline float get_distance_3() const { return ___distance_3; }
	inline float* get_address_of_distance_3() { return &___distance_3; }
	inline void set_distance_3(float value)
	{
		___distance_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_rotationDamping_5() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___rotationDamping_5)); }
	inline float get_rotationDamping_5() const { return ___rotationDamping_5; }
	inline float* get_address_of_rotationDamping_5() { return &___rotationDamping_5; }
	inline void set_rotationDamping_5(float value)
	{
		___rotationDamping_5 = value;
	}

	inline static int32_t get_offset_of_heightDamping_6() { return static_cast<int32_t>(offsetof(SmoothFollow_t4204731361, ___heightDamping_6)); }
	inline float get_heightDamping_6() const { return ___heightDamping_6; }
	inline float* get_address_of_heightDamping_6() { return &___heightDamping_6; }
	inline void set_heightDamping_6(float value)
	{
		___heightDamping_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHFOLLOW_T4204731361_H
#ifndef FOLLOWTARGET_T166153614_H
#define FOLLOWTARGET_T166153614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FollowTarget
struct  FollowTarget_t166153614  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform UnityStandardAssets.Utility.FollowTarget::target
	Transform_t3600365921 * ___target_2;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.FollowTarget::offset
	Vector3_t3722313464  ___offset_3;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(FollowTarget_t166153614, ___target_2)); }
	inline Transform_t3600365921 * get_target_2() const { return ___target_2; }
	inline Transform_t3600365921 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3600365921 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(FollowTarget_t166153614, ___offset_3)); }
	inline Vector3_t3722313464  get_offset_3() const { return ___offset_3; }
	inline Vector3_t3722313464 * get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(Vector3_t3722313464  value)
	{
		___offset_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWTARGET_T166153614_H
#ifndef WAYPOINTCIRCUIT_T445075330_H
#define WAYPOINTCIRCUIT_T445075330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.WaypointCircuit
struct  WaypointCircuit_t445075330  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.WaypointCircuit/WaypointList UnityStandardAssets.Utility.WaypointCircuit::waypointList
	WaypointList_t2584574554 * ___waypointList_2;
	// System.Boolean UnityStandardAssets.Utility.WaypointCircuit::smoothRoute
	bool ___smoothRoute_3;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::numPoints
	int32_t ___numPoints_4;
	// UnityEngine.Vector3[] UnityStandardAssets.Utility.WaypointCircuit::points
	Vector3U5BU5D_t1718750761* ___points_5;
	// System.Single[] UnityStandardAssets.Utility.WaypointCircuit::distances
	SingleU5BU5D_t1444911251* ___distances_6;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::editorVisualisationSubsteps
	float ___editorVisualisationSubsteps_7;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::<Length>k__BackingField
	float ___U3CLengthU3Ek__BackingField_8;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p0n
	int32_t ___p0n_9;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p1n
	int32_t ___p1n_10;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p2n
	int32_t ___p2n_11;
	// System.Int32 UnityStandardAssets.Utility.WaypointCircuit::p3n
	int32_t ___p3n_12;
	// System.Single UnityStandardAssets.Utility.WaypointCircuit::i
	float ___i_13;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P0
	Vector3_t3722313464  ___P0_14;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P1
	Vector3_t3722313464  ___P1_15;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P2
	Vector3_t3722313464  ___P2_16;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.WaypointCircuit::P3
	Vector3_t3722313464  ___P3_17;

public:
	inline static int32_t get_offset_of_waypointList_2() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___waypointList_2)); }
	inline WaypointList_t2584574554 * get_waypointList_2() const { return ___waypointList_2; }
	inline WaypointList_t2584574554 ** get_address_of_waypointList_2() { return &___waypointList_2; }
	inline void set_waypointList_2(WaypointList_t2584574554 * value)
	{
		___waypointList_2 = value;
		Il2CppCodeGenWriteBarrier((&___waypointList_2), value);
	}

	inline static int32_t get_offset_of_smoothRoute_3() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___smoothRoute_3)); }
	inline bool get_smoothRoute_3() const { return ___smoothRoute_3; }
	inline bool* get_address_of_smoothRoute_3() { return &___smoothRoute_3; }
	inline void set_smoothRoute_3(bool value)
	{
		___smoothRoute_3 = value;
	}

	inline static int32_t get_offset_of_numPoints_4() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___numPoints_4)); }
	inline int32_t get_numPoints_4() const { return ___numPoints_4; }
	inline int32_t* get_address_of_numPoints_4() { return &___numPoints_4; }
	inline void set_numPoints_4(int32_t value)
	{
		___numPoints_4 = value;
	}

	inline static int32_t get_offset_of_points_5() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___points_5)); }
	inline Vector3U5BU5D_t1718750761* get_points_5() const { return ___points_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of_points_5() { return &___points_5; }
	inline void set_points_5(Vector3U5BU5D_t1718750761* value)
	{
		___points_5 = value;
		Il2CppCodeGenWriteBarrier((&___points_5), value);
	}

	inline static int32_t get_offset_of_distances_6() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___distances_6)); }
	inline SingleU5BU5D_t1444911251* get_distances_6() const { return ___distances_6; }
	inline SingleU5BU5D_t1444911251** get_address_of_distances_6() { return &___distances_6; }
	inline void set_distances_6(SingleU5BU5D_t1444911251* value)
	{
		___distances_6 = value;
		Il2CppCodeGenWriteBarrier((&___distances_6), value);
	}

	inline static int32_t get_offset_of_editorVisualisationSubsteps_7() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___editorVisualisationSubsteps_7)); }
	inline float get_editorVisualisationSubsteps_7() const { return ___editorVisualisationSubsteps_7; }
	inline float* get_address_of_editorVisualisationSubsteps_7() { return &___editorVisualisationSubsteps_7; }
	inline void set_editorVisualisationSubsteps_7(float value)
	{
		___editorVisualisationSubsteps_7 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___U3CLengthU3Ek__BackingField_8)); }
	inline float get_U3CLengthU3Ek__BackingField_8() const { return ___U3CLengthU3Ek__BackingField_8; }
	inline float* get_address_of_U3CLengthU3Ek__BackingField_8() { return &___U3CLengthU3Ek__BackingField_8; }
	inline void set_U3CLengthU3Ek__BackingField_8(float value)
	{
		___U3CLengthU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_p0n_9() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p0n_9)); }
	inline int32_t get_p0n_9() const { return ___p0n_9; }
	inline int32_t* get_address_of_p0n_9() { return &___p0n_9; }
	inline void set_p0n_9(int32_t value)
	{
		___p0n_9 = value;
	}

	inline static int32_t get_offset_of_p1n_10() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p1n_10)); }
	inline int32_t get_p1n_10() const { return ___p1n_10; }
	inline int32_t* get_address_of_p1n_10() { return &___p1n_10; }
	inline void set_p1n_10(int32_t value)
	{
		___p1n_10 = value;
	}

	inline static int32_t get_offset_of_p2n_11() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p2n_11)); }
	inline int32_t get_p2n_11() const { return ___p2n_11; }
	inline int32_t* get_address_of_p2n_11() { return &___p2n_11; }
	inline void set_p2n_11(int32_t value)
	{
		___p2n_11 = value;
	}

	inline static int32_t get_offset_of_p3n_12() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___p3n_12)); }
	inline int32_t get_p3n_12() const { return ___p3n_12; }
	inline int32_t* get_address_of_p3n_12() { return &___p3n_12; }
	inline void set_p3n_12(int32_t value)
	{
		___p3n_12 = value;
	}

	inline static int32_t get_offset_of_i_13() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___i_13)); }
	inline float get_i_13() const { return ___i_13; }
	inline float* get_address_of_i_13() { return &___i_13; }
	inline void set_i_13(float value)
	{
		___i_13 = value;
	}

	inline static int32_t get_offset_of_P0_14() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P0_14)); }
	inline Vector3_t3722313464  get_P0_14() const { return ___P0_14; }
	inline Vector3_t3722313464 * get_address_of_P0_14() { return &___P0_14; }
	inline void set_P0_14(Vector3_t3722313464  value)
	{
		___P0_14 = value;
	}

	inline static int32_t get_offset_of_P1_15() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P1_15)); }
	inline Vector3_t3722313464  get_P1_15() const { return ___P1_15; }
	inline Vector3_t3722313464 * get_address_of_P1_15() { return &___P1_15; }
	inline void set_P1_15(Vector3_t3722313464  value)
	{
		___P1_15 = value;
	}

	inline static int32_t get_offset_of_P2_16() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P2_16)); }
	inline Vector3_t3722313464  get_P2_16() const { return ___P2_16; }
	inline Vector3_t3722313464 * get_address_of_P2_16() { return &___P2_16; }
	inline void set_P2_16(Vector3_t3722313464  value)
	{
		___P2_16 = value;
	}

	inline static int32_t get_offset_of_P3_17() { return static_cast<int32_t>(offsetof(WaypointCircuit_t445075330, ___P3_17)); }
	inline Vector3_t3722313464  get_P3_17() const { return ___P3_17; }
	inline Vector3_t3722313464 * get_address_of_P3_17() { return &___P3_17; }
	inline void set_P3_17(Vector3_t3722313464  value)
	{
		___P3_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAYPOINTCIRCUIT_T445075330_H
#ifndef SIMPLEACTIVATORMENU_T1387811551_H
#define SIMPLEACTIVATORMENU_T1387811551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SimpleActivatorMenu
struct  SimpleActivatorMenu_t1387811551  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GUIText UnityStandardAssets.Utility.SimpleActivatorMenu::camSwitchButton
	GUIText_t402233326 * ___camSwitchButton_2;
	// UnityEngine.GameObject[] UnityStandardAssets.Utility.SimpleActivatorMenu::objects
	GameObjectU5BU5D_t3328599146* ___objects_3;
	// System.Int32 UnityStandardAssets.Utility.SimpleActivatorMenu::m_CurrentActiveObject
	int32_t ___m_CurrentActiveObject_4;

public:
	inline static int32_t get_offset_of_camSwitchButton_2() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t1387811551, ___camSwitchButton_2)); }
	inline GUIText_t402233326 * get_camSwitchButton_2() const { return ___camSwitchButton_2; }
	inline GUIText_t402233326 ** get_address_of_camSwitchButton_2() { return &___camSwitchButton_2; }
	inline void set_camSwitchButton_2(GUIText_t402233326 * value)
	{
		___camSwitchButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___camSwitchButton_2), value);
	}

	inline static int32_t get_offset_of_objects_3() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t1387811551, ___objects_3)); }
	inline GameObjectU5BU5D_t3328599146* get_objects_3() const { return ___objects_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_objects_3() { return &___objects_3; }
	inline void set_objects_3(GameObjectU5BU5D_t3328599146* value)
	{
		___objects_3 = value;
		Il2CppCodeGenWriteBarrier((&___objects_3), value);
	}

	inline static int32_t get_offset_of_m_CurrentActiveObject_4() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t1387811551, ___m_CurrentActiveObject_4)); }
	inline int32_t get_m_CurrentActiveObject_4() const { return ___m_CurrentActiveObject_4; }
	inline int32_t* get_address_of_m_CurrentActiveObject_4() { return &___m_CurrentActiveObject_4; }
	inline void set_m_CurrentActiveObject_4(int32_t value)
	{
		___m_CurrentActiveObject_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEACTIVATORMENU_T1387811551_H
#ifndef SIMPLEMOUSEROTATOR_T2364742953_H
#define SIMPLEMOUSEROTATOR_T2364742953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SimpleMouseRotator
struct  SimpleMouseRotator_t2364742953  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector2 UnityStandardAssets.Utility.SimpleMouseRotator::rotationRange
	Vector2_t2156229523  ___rotationRange_2;
	// System.Single UnityStandardAssets.Utility.SimpleMouseRotator::rotationSpeed
	float ___rotationSpeed_3;
	// System.Single UnityStandardAssets.Utility.SimpleMouseRotator::dampingTime
	float ___dampingTime_4;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::autoZeroVerticalOnMobile
	bool ___autoZeroVerticalOnMobile_5;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::autoZeroHorizontalOnMobile
	bool ___autoZeroHorizontalOnMobile_6;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::relative
	bool ___relative_7;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_TargetAngles
	Vector3_t3722313464  ___m_TargetAngles_8;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_FollowAngles
	Vector3_t3722313464  ___m_FollowAngles_9;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_FollowVelocity
	Vector3_t3722313464  ___m_FollowVelocity_10;
	// UnityEngine.Quaternion UnityStandardAssets.Utility.SimpleMouseRotator::m_OriginalRotation
	Quaternion_t2301928331  ___m_OriginalRotation_11;

public:
	inline static int32_t get_offset_of_rotationRange_2() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___rotationRange_2)); }
	inline Vector2_t2156229523  get_rotationRange_2() const { return ___rotationRange_2; }
	inline Vector2_t2156229523 * get_address_of_rotationRange_2() { return &___rotationRange_2; }
	inline void set_rotationRange_2(Vector2_t2156229523  value)
	{
		___rotationRange_2 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_3() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___rotationSpeed_3)); }
	inline float get_rotationSpeed_3() const { return ___rotationSpeed_3; }
	inline float* get_address_of_rotationSpeed_3() { return &___rotationSpeed_3; }
	inline void set_rotationSpeed_3(float value)
	{
		___rotationSpeed_3 = value;
	}

	inline static int32_t get_offset_of_dampingTime_4() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___dampingTime_4)); }
	inline float get_dampingTime_4() const { return ___dampingTime_4; }
	inline float* get_address_of_dampingTime_4() { return &___dampingTime_4; }
	inline void set_dampingTime_4(float value)
	{
		___dampingTime_4 = value;
	}

	inline static int32_t get_offset_of_autoZeroVerticalOnMobile_5() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___autoZeroVerticalOnMobile_5)); }
	inline bool get_autoZeroVerticalOnMobile_5() const { return ___autoZeroVerticalOnMobile_5; }
	inline bool* get_address_of_autoZeroVerticalOnMobile_5() { return &___autoZeroVerticalOnMobile_5; }
	inline void set_autoZeroVerticalOnMobile_5(bool value)
	{
		___autoZeroVerticalOnMobile_5 = value;
	}

	inline static int32_t get_offset_of_autoZeroHorizontalOnMobile_6() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___autoZeroHorizontalOnMobile_6)); }
	inline bool get_autoZeroHorizontalOnMobile_6() const { return ___autoZeroHorizontalOnMobile_6; }
	inline bool* get_address_of_autoZeroHorizontalOnMobile_6() { return &___autoZeroHorizontalOnMobile_6; }
	inline void set_autoZeroHorizontalOnMobile_6(bool value)
	{
		___autoZeroHorizontalOnMobile_6 = value;
	}

	inline static int32_t get_offset_of_relative_7() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___relative_7)); }
	inline bool get_relative_7() const { return ___relative_7; }
	inline bool* get_address_of_relative_7() { return &___relative_7; }
	inline void set_relative_7(bool value)
	{
		___relative_7 = value;
	}

	inline static int32_t get_offset_of_m_TargetAngles_8() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_TargetAngles_8)); }
	inline Vector3_t3722313464  get_m_TargetAngles_8() const { return ___m_TargetAngles_8; }
	inline Vector3_t3722313464 * get_address_of_m_TargetAngles_8() { return &___m_TargetAngles_8; }
	inline void set_m_TargetAngles_8(Vector3_t3722313464  value)
	{
		___m_TargetAngles_8 = value;
	}

	inline static int32_t get_offset_of_m_FollowAngles_9() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_FollowAngles_9)); }
	inline Vector3_t3722313464  get_m_FollowAngles_9() const { return ___m_FollowAngles_9; }
	inline Vector3_t3722313464 * get_address_of_m_FollowAngles_9() { return &___m_FollowAngles_9; }
	inline void set_m_FollowAngles_9(Vector3_t3722313464  value)
	{
		___m_FollowAngles_9 = value;
	}

	inline static int32_t get_offset_of_m_FollowVelocity_10() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_FollowVelocity_10)); }
	inline Vector3_t3722313464  get_m_FollowVelocity_10() const { return ___m_FollowVelocity_10; }
	inline Vector3_t3722313464 * get_address_of_m_FollowVelocity_10() { return &___m_FollowVelocity_10; }
	inline void set_m_FollowVelocity_10(Vector3_t3722313464  value)
	{
		___m_FollowVelocity_10 = value;
	}

	inline static int32_t get_offset_of_m_OriginalRotation_11() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t2364742953, ___m_OriginalRotation_11)); }
	inline Quaternion_t2301928331  get_m_OriginalRotation_11() const { return ___m_OriginalRotation_11; }
	inline Quaternion_t2301928331 * get_address_of_m_OriginalRotation_11() { return &___m_OriginalRotation_11; }
	inline void set_m_OriginalRotation_11(Quaternion_t2301928331  value)
	{
		___m_OriginalRotation_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEMOUSEROTATOR_T2364742953_H
#ifndef FPSCOUNTER_T2351221284_H
#define FPSCOUNTER_T2351221284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FPSCounter
struct  FPSCounter_t2351221284  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UnityStandardAssets.Utility.FPSCounter::m_FpsAccumulator
	int32_t ___m_FpsAccumulator_3;
	// System.Single UnityStandardAssets.Utility.FPSCounter::m_FpsNextPeriod
	float ___m_FpsNextPeriod_4;
	// System.Int32 UnityStandardAssets.Utility.FPSCounter::m_CurrentFps
	int32_t ___m_CurrentFps_5;
	// UnityEngine.UI.Text UnityStandardAssets.Utility.FPSCounter::m_Text
	Text_t1901882714 * ___m_Text_7;

public:
	inline static int32_t get_offset_of_m_FpsAccumulator_3() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_FpsAccumulator_3)); }
	inline int32_t get_m_FpsAccumulator_3() const { return ___m_FpsAccumulator_3; }
	inline int32_t* get_address_of_m_FpsAccumulator_3() { return &___m_FpsAccumulator_3; }
	inline void set_m_FpsAccumulator_3(int32_t value)
	{
		___m_FpsAccumulator_3 = value;
	}

	inline static int32_t get_offset_of_m_FpsNextPeriod_4() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_FpsNextPeriod_4)); }
	inline float get_m_FpsNextPeriod_4() const { return ___m_FpsNextPeriod_4; }
	inline float* get_address_of_m_FpsNextPeriod_4() { return &___m_FpsNextPeriod_4; }
	inline void set_m_FpsNextPeriod_4(float value)
	{
		___m_FpsNextPeriod_4 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFps_5() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_CurrentFps_5)); }
	inline int32_t get_m_CurrentFps_5() const { return ___m_CurrentFps_5; }
	inline int32_t* get_address_of_m_CurrentFps_5() { return &___m_CurrentFps_5; }
	inline void set_m_CurrentFps_5(int32_t value)
	{
		___m_CurrentFps_5 = value;
	}

	inline static int32_t get_offset_of_m_Text_7() { return static_cast<int32_t>(offsetof(FPSCounter_t2351221284, ___m_Text_7)); }
	inline Text_t1901882714 * get_m_Text_7() const { return ___m_Text_7; }
	inline Text_t1901882714 ** get_address_of_m_Text_7() { return &___m_Text_7; }
	inline void set_m_Text_7(Text_t1901882714 * value)
	{
		___m_Text_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTER_T2351221284_H
#ifndef OBJECTRESETTER_T639177103_H
#define OBJECTRESETTER_T639177103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ObjectResetter
struct  ObjectResetter_t639177103  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.ObjectResetter::originalPosition
	Vector3_t3722313464  ___originalPosition_2;
	// UnityEngine.Quaternion UnityStandardAssets.Utility.ObjectResetter::originalRotation
	Quaternion_t2301928331  ___originalRotation_3;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityStandardAssets.Utility.ObjectResetter::originalStructure
	List_1_t777473367 * ___originalStructure_4;
	// UnityEngine.Rigidbody UnityStandardAssets.Utility.ObjectResetter::Rigidbody
	Rigidbody_t3916780224 * ___Rigidbody_5;

public:
	inline static int32_t get_offset_of_originalPosition_2() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalPosition_2)); }
	inline Vector3_t3722313464  get_originalPosition_2() const { return ___originalPosition_2; }
	inline Vector3_t3722313464 * get_address_of_originalPosition_2() { return &___originalPosition_2; }
	inline void set_originalPosition_2(Vector3_t3722313464  value)
	{
		___originalPosition_2 = value;
	}

	inline static int32_t get_offset_of_originalRotation_3() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalRotation_3)); }
	inline Quaternion_t2301928331  get_originalRotation_3() const { return ___originalRotation_3; }
	inline Quaternion_t2301928331 * get_address_of_originalRotation_3() { return &___originalRotation_3; }
	inline void set_originalRotation_3(Quaternion_t2301928331  value)
	{
		___originalRotation_3 = value;
	}

	inline static int32_t get_offset_of_originalStructure_4() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___originalStructure_4)); }
	inline List_1_t777473367 * get_originalStructure_4() const { return ___originalStructure_4; }
	inline List_1_t777473367 ** get_address_of_originalStructure_4() { return &___originalStructure_4; }
	inline void set_originalStructure_4(List_1_t777473367 * value)
	{
		___originalStructure_4 = value;
		Il2CppCodeGenWriteBarrier((&___originalStructure_4), value);
	}

	inline static int32_t get_offset_of_Rigidbody_5() { return static_cast<int32_t>(offsetof(ObjectResetter_t639177103, ___Rigidbody_5)); }
	inline Rigidbody_t3916780224 * get_Rigidbody_5() const { return ___Rigidbody_5; }
	inline Rigidbody_t3916780224 ** get_address_of_Rigidbody_5() { return &___Rigidbody_5; }
	inline void set_Rigidbody_5(Rigidbody_t3916780224 * value)
	{
		___Rigidbody_5 = value;
		Il2CppCodeGenWriteBarrier((&___Rigidbody_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTRESETTER_T639177103_H
#ifndef PLATFORMSPECIFICCONTENT_T1404549723_H
#define PLATFORMSPECIFICCONTENT_T1404549723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.PlatformSpecificContent
struct  PlatformSpecificContent_t1404549723  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup UnityStandardAssets.Utility.PlatformSpecificContent::m_BuildTargetGroup
	int32_t ___m_BuildTargetGroup_2;
	// UnityEngine.GameObject[] UnityStandardAssets.Utility.PlatformSpecificContent::m_Content
	GameObjectU5BU5D_t3328599146* ___m_Content_3;
	// UnityEngine.MonoBehaviour[] UnityStandardAssets.Utility.PlatformSpecificContent::m_MonoBehaviours
	MonoBehaviourU5BU5D_t2007329276* ___m_MonoBehaviours_4;
	// System.Boolean UnityStandardAssets.Utility.PlatformSpecificContent::m_ChildrenOfThisObject
	bool ___m_ChildrenOfThisObject_5;

public:
	inline static int32_t get_offset_of_m_BuildTargetGroup_2() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_BuildTargetGroup_2)); }
	inline int32_t get_m_BuildTargetGroup_2() const { return ___m_BuildTargetGroup_2; }
	inline int32_t* get_address_of_m_BuildTargetGroup_2() { return &___m_BuildTargetGroup_2; }
	inline void set_m_BuildTargetGroup_2(int32_t value)
	{
		___m_BuildTargetGroup_2 = value;
	}

	inline static int32_t get_offset_of_m_Content_3() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_Content_3)); }
	inline GameObjectU5BU5D_t3328599146* get_m_Content_3() const { return ___m_Content_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_m_Content_3() { return &___m_Content_3; }
	inline void set_m_Content_3(GameObjectU5BU5D_t3328599146* value)
	{
		___m_Content_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_3), value);
	}

	inline static int32_t get_offset_of_m_MonoBehaviours_4() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_MonoBehaviours_4)); }
	inline MonoBehaviourU5BU5D_t2007329276* get_m_MonoBehaviours_4() const { return ___m_MonoBehaviours_4; }
	inline MonoBehaviourU5BU5D_t2007329276** get_address_of_m_MonoBehaviours_4() { return &___m_MonoBehaviours_4; }
	inline void set_m_MonoBehaviours_4(MonoBehaviourU5BU5D_t2007329276* value)
	{
		___m_MonoBehaviours_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MonoBehaviours_4), value);
	}

	inline static int32_t get_offset_of_m_ChildrenOfThisObject_5() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t1404549723, ___m_ChildrenOfThisObject_5)); }
	inline bool get_m_ChildrenOfThisObject_5() const { return ___m_ChildrenOfThisObject_5; }
	inline bool* get_address_of_m_ChildrenOfThisObject_5() { return &___m_ChildrenOfThisObject_5; }
	inline void set_m_ChildrenOfThisObject_5(bool value)
	{
		___m_ChildrenOfThisObject_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMSPECIFICCONTENT_T1404549723_H
#ifndef TIMEDOBJECTACTIVATOR_T1846709985_H
#define TIMEDOBJECTACTIVATOR_T1846709985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator
struct  TimedObjectActivator_t1846709985  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entries UnityStandardAssets.Utility.TimedObjectActivator::entries
	Entries_t3168066469 * ___entries_2;

public:
	inline static int32_t get_offset_of_entries_2() { return static_cast<int32_t>(offsetof(TimedObjectActivator_t1846709985, ___entries_2)); }
	inline Entries_t3168066469 * get_entries_2() const { return ___entries_2; }
	inline Entries_t3168066469 ** get_address_of_entries_2() { return &___entries_2; }
	inline void set_entries_2(Entries_t3168066469 * value)
	{
		___entries_2 = value;
		Il2CppCodeGenWriteBarrier((&___entries_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTACTIVATOR_T1846709985_H
#ifndef DYNAMICSHADOWSETTINGS_T59119858_H
#define DYNAMICSHADOWSETTINGS_T59119858_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DynamicShadowSettings
struct  DynamicShadowSettings_t59119858  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Light UnityStandardAssets.Utility.DynamicShadowSettings::sunLight
	Light_t3756812086 * ___sunLight_2;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minHeight
	float ___minHeight_3;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minShadowDistance
	float ___minShadowDistance_4;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minShadowBias
	float ___minShadowBias_5;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxHeight
	float ___maxHeight_6;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxShadowDistance
	float ___maxShadowDistance_7;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxShadowBias
	float ___maxShadowBias_8;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::adaptTime
	float ___adaptTime_9;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_SmoothHeight
	float ___m_SmoothHeight_10;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_ChangeSpeed
	float ___m_ChangeSpeed_11;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_OriginalStrength
	float ___m_OriginalStrength_12;

public:
	inline static int32_t get_offset_of_sunLight_2() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___sunLight_2)); }
	inline Light_t3756812086 * get_sunLight_2() const { return ___sunLight_2; }
	inline Light_t3756812086 ** get_address_of_sunLight_2() { return &___sunLight_2; }
	inline void set_sunLight_2(Light_t3756812086 * value)
	{
		___sunLight_2 = value;
		Il2CppCodeGenWriteBarrier((&___sunLight_2), value);
	}

	inline static int32_t get_offset_of_minHeight_3() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minHeight_3)); }
	inline float get_minHeight_3() const { return ___minHeight_3; }
	inline float* get_address_of_minHeight_3() { return &___minHeight_3; }
	inline void set_minHeight_3(float value)
	{
		___minHeight_3 = value;
	}

	inline static int32_t get_offset_of_minShadowDistance_4() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minShadowDistance_4)); }
	inline float get_minShadowDistance_4() const { return ___minShadowDistance_4; }
	inline float* get_address_of_minShadowDistance_4() { return &___minShadowDistance_4; }
	inline void set_minShadowDistance_4(float value)
	{
		___minShadowDistance_4 = value;
	}

	inline static int32_t get_offset_of_minShadowBias_5() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___minShadowBias_5)); }
	inline float get_minShadowBias_5() const { return ___minShadowBias_5; }
	inline float* get_address_of_minShadowBias_5() { return &___minShadowBias_5; }
	inline void set_minShadowBias_5(float value)
	{
		___minShadowBias_5 = value;
	}

	inline static int32_t get_offset_of_maxHeight_6() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxHeight_6)); }
	inline float get_maxHeight_6() const { return ___maxHeight_6; }
	inline float* get_address_of_maxHeight_6() { return &___maxHeight_6; }
	inline void set_maxHeight_6(float value)
	{
		___maxHeight_6 = value;
	}

	inline static int32_t get_offset_of_maxShadowDistance_7() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxShadowDistance_7)); }
	inline float get_maxShadowDistance_7() const { return ___maxShadowDistance_7; }
	inline float* get_address_of_maxShadowDistance_7() { return &___maxShadowDistance_7; }
	inline void set_maxShadowDistance_7(float value)
	{
		___maxShadowDistance_7 = value;
	}

	inline static int32_t get_offset_of_maxShadowBias_8() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___maxShadowBias_8)); }
	inline float get_maxShadowBias_8() const { return ___maxShadowBias_8; }
	inline float* get_address_of_maxShadowBias_8() { return &___maxShadowBias_8; }
	inline void set_maxShadowBias_8(float value)
	{
		___maxShadowBias_8 = value;
	}

	inline static int32_t get_offset_of_adaptTime_9() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___adaptTime_9)); }
	inline float get_adaptTime_9() const { return ___adaptTime_9; }
	inline float* get_address_of_adaptTime_9() { return &___adaptTime_9; }
	inline void set_adaptTime_9(float value)
	{
		___adaptTime_9 = value;
	}

	inline static int32_t get_offset_of_m_SmoothHeight_10() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_SmoothHeight_10)); }
	inline float get_m_SmoothHeight_10() const { return ___m_SmoothHeight_10; }
	inline float* get_address_of_m_SmoothHeight_10() { return &___m_SmoothHeight_10; }
	inline void set_m_SmoothHeight_10(float value)
	{
		___m_SmoothHeight_10 = value;
	}

	inline static int32_t get_offset_of_m_ChangeSpeed_11() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_ChangeSpeed_11)); }
	inline float get_m_ChangeSpeed_11() const { return ___m_ChangeSpeed_11; }
	inline float* get_address_of_m_ChangeSpeed_11() { return &___m_ChangeSpeed_11; }
	inline void set_m_ChangeSpeed_11(float value)
	{
		___m_ChangeSpeed_11 = value;
	}

	inline static int32_t get_offset_of_m_OriginalStrength_12() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t59119858, ___m_OriginalStrength_12)); }
	inline float get_m_OriginalStrength_12() const { return ___m_OriginalStrength_12; }
	inline float* get_address_of_m_OriginalStrength_12() { return &___m_OriginalStrength_12; }
	inline void set_m_OriginalStrength_12(float value)
	{
		___m_OriginalStrength_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICSHADOWSETTINGS_T59119858_H
#ifndef ENEMYSHOOT_T243830779_H
#define ENEMYSHOOT_T243830779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyShoot
struct  EnemyShoot_t243830779  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioClip EnemyShoot::gunShot
	AudioClip_t3680889665 * ___gunShot_2;
	// UnityEngine.AudioClip EnemyShoot::laserShot
	AudioClip_t3680889665 * ___laserShot_3;
	// UnityEngine.GameObject EnemyShoot::bullet
	GameObject_t1113636619 * ___bullet_4;
	// UnityEngine.GameObject EnemyShoot::laser
	GameObject_t1113636619 * ___laser_5;
	// UnityEngine.Transform EnemyShoot::spawn
	Transform_t3600365921 * ___spawn_6;
	// System.Single EnemyShoot::bulletSpeed
	float ___bulletSpeed_7;
	// System.Single EnemyShoot::cooldownTime
	float ___cooldownTime_8;
	// System.Boolean EnemyShoot::firedGun
	bool ___firedGun_9;
	// System.Single EnemyShoot::cooldownTimeMax
	float ___cooldownTimeMax_10;

public:
	inline static int32_t get_offset_of_gunShot_2() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___gunShot_2)); }
	inline AudioClip_t3680889665 * get_gunShot_2() const { return ___gunShot_2; }
	inline AudioClip_t3680889665 ** get_address_of_gunShot_2() { return &___gunShot_2; }
	inline void set_gunShot_2(AudioClip_t3680889665 * value)
	{
		___gunShot_2 = value;
		Il2CppCodeGenWriteBarrier((&___gunShot_2), value);
	}

	inline static int32_t get_offset_of_laserShot_3() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___laserShot_3)); }
	inline AudioClip_t3680889665 * get_laserShot_3() const { return ___laserShot_3; }
	inline AudioClip_t3680889665 ** get_address_of_laserShot_3() { return &___laserShot_3; }
	inline void set_laserShot_3(AudioClip_t3680889665 * value)
	{
		___laserShot_3 = value;
		Il2CppCodeGenWriteBarrier((&___laserShot_3), value);
	}

	inline static int32_t get_offset_of_bullet_4() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___bullet_4)); }
	inline GameObject_t1113636619 * get_bullet_4() const { return ___bullet_4; }
	inline GameObject_t1113636619 ** get_address_of_bullet_4() { return &___bullet_4; }
	inline void set_bullet_4(GameObject_t1113636619 * value)
	{
		___bullet_4 = value;
		Il2CppCodeGenWriteBarrier((&___bullet_4), value);
	}

	inline static int32_t get_offset_of_laser_5() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___laser_5)); }
	inline GameObject_t1113636619 * get_laser_5() const { return ___laser_5; }
	inline GameObject_t1113636619 ** get_address_of_laser_5() { return &___laser_5; }
	inline void set_laser_5(GameObject_t1113636619 * value)
	{
		___laser_5 = value;
		Il2CppCodeGenWriteBarrier((&___laser_5), value);
	}

	inline static int32_t get_offset_of_spawn_6() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___spawn_6)); }
	inline Transform_t3600365921 * get_spawn_6() const { return ___spawn_6; }
	inline Transform_t3600365921 ** get_address_of_spawn_6() { return &___spawn_6; }
	inline void set_spawn_6(Transform_t3600365921 * value)
	{
		___spawn_6 = value;
		Il2CppCodeGenWriteBarrier((&___spawn_6), value);
	}

	inline static int32_t get_offset_of_bulletSpeed_7() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___bulletSpeed_7)); }
	inline float get_bulletSpeed_7() const { return ___bulletSpeed_7; }
	inline float* get_address_of_bulletSpeed_7() { return &___bulletSpeed_7; }
	inline void set_bulletSpeed_7(float value)
	{
		___bulletSpeed_7 = value;
	}

	inline static int32_t get_offset_of_cooldownTime_8() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___cooldownTime_8)); }
	inline float get_cooldownTime_8() const { return ___cooldownTime_8; }
	inline float* get_address_of_cooldownTime_8() { return &___cooldownTime_8; }
	inline void set_cooldownTime_8(float value)
	{
		___cooldownTime_8 = value;
	}

	inline static int32_t get_offset_of_firedGun_9() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___firedGun_9)); }
	inline bool get_firedGun_9() const { return ___firedGun_9; }
	inline bool* get_address_of_firedGun_9() { return &___firedGun_9; }
	inline void set_firedGun_9(bool value)
	{
		___firedGun_9 = value;
	}

	inline static int32_t get_offset_of_cooldownTimeMax_10() { return static_cast<int32_t>(offsetof(EnemyShoot_t243830779, ___cooldownTimeMax_10)); }
	inline float get_cooldownTimeMax_10() const { return ___cooldownTimeMax_10; }
	inline float* get_address_of_cooldownTimeMax_10() { return &___cooldownTimeMax_10; }
	inline void set_cooldownTimeMax_10(float value)
	{
		___cooldownTimeMax_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYSHOOT_T243830779_H
#ifndef ENEMYSTATS_T4187841152_H
#define ENEMYSTATS_T4187841152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyStats
struct  EnemyStats_t4187841152  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 EnemyStats::eCurrentHealth
	int32_t ___eCurrentHealth_2;
	// System.Int32 EnemyStats::eMaximumHealth
	int32_t ___eMaximumHealth_3;
	// EnemyCount EnemyStats::eCount
	EnemyCount_t3730988989 * ___eCount_4;

public:
	inline static int32_t get_offset_of_eCurrentHealth_2() { return static_cast<int32_t>(offsetof(EnemyStats_t4187841152, ___eCurrentHealth_2)); }
	inline int32_t get_eCurrentHealth_2() const { return ___eCurrentHealth_2; }
	inline int32_t* get_address_of_eCurrentHealth_2() { return &___eCurrentHealth_2; }
	inline void set_eCurrentHealth_2(int32_t value)
	{
		___eCurrentHealth_2 = value;
	}

	inline static int32_t get_offset_of_eMaximumHealth_3() { return static_cast<int32_t>(offsetof(EnemyStats_t4187841152, ___eMaximumHealth_3)); }
	inline int32_t get_eMaximumHealth_3() const { return ___eMaximumHealth_3; }
	inline int32_t* get_address_of_eMaximumHealth_3() { return &___eMaximumHealth_3; }
	inline void set_eMaximumHealth_3(int32_t value)
	{
		___eMaximumHealth_3 = value;
	}

	inline static int32_t get_offset_of_eCount_4() { return static_cast<int32_t>(offsetof(EnemyStats_t4187841152, ___eCount_4)); }
	inline EnemyCount_t3730988989 * get_eCount_4() const { return ___eCount_4; }
	inline EnemyCount_t3730988989 ** get_address_of_eCount_4() { return &___eCount_4; }
	inline void set_eCount_4(EnemyCount_t3730988989 * value)
	{
		___eCount_4 = value;
		Il2CppCodeGenWriteBarrier((&___eCount_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYSTATS_T4187841152_H
#ifndef ENEMYCOUNT_T3730988989_H
#define ENEMYCOUNT_T3730988989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyCount
struct  EnemyCount_t3730988989  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] EnemyCount::enemy
	GameObjectU5BU5D_t3328599146* ___enemy_2;
	// System.Int32 EnemyCount::enemyNumber
	int32_t ___enemyNumber_3;
	// LevelClear EnemyCount::levelClear
	LevelClear_t1949285084 * ___levelClear_4;

public:
	inline static int32_t get_offset_of_enemy_2() { return static_cast<int32_t>(offsetof(EnemyCount_t3730988989, ___enemy_2)); }
	inline GameObjectU5BU5D_t3328599146* get_enemy_2() const { return ___enemy_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_enemy_2() { return &___enemy_2; }
	inline void set_enemy_2(GameObjectU5BU5D_t3328599146* value)
	{
		___enemy_2 = value;
		Il2CppCodeGenWriteBarrier((&___enemy_2), value);
	}

	inline static int32_t get_offset_of_enemyNumber_3() { return static_cast<int32_t>(offsetof(EnemyCount_t3730988989, ___enemyNumber_3)); }
	inline int32_t get_enemyNumber_3() const { return ___enemyNumber_3; }
	inline int32_t* get_address_of_enemyNumber_3() { return &___enemyNumber_3; }
	inline void set_enemyNumber_3(int32_t value)
	{
		___enemyNumber_3 = value;
	}

	inline static int32_t get_offset_of_levelClear_4() { return static_cast<int32_t>(offsetof(EnemyCount_t3730988989, ___levelClear_4)); }
	inline LevelClear_t1949285084 * get_levelClear_4() const { return ___levelClear_4; }
	inline LevelClear_t1949285084 ** get_address_of_levelClear_4() { return &___levelClear_4; }
	inline void set_levelClear_4(LevelClear_t1949285084 * value)
	{
		___levelClear_4 = value;
		Il2CppCodeGenWriteBarrier((&___levelClear_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYCOUNT_T3730988989_H
#ifndef BOSSUI_T1795207708_H
#define BOSSUI_T1795207708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BossUI
struct  BossUI_t1795207708  : public MonoBehaviour_t3962482529
{
public:
	// EnemyStats BossUI::enemyTank
	EnemyStats_t4187841152 * ___enemyTank_2;
	// UnityEngine.UI.Image BossUI::enemyHealthBar
	Image_t2670269651 * ___enemyHealthBar_3;
	// System.Single BossUI::enemyHealthBarValue
	float ___enemyHealthBarValue_4;
	// PlayerStats BossUI::playerTank
	PlayerStats_t2044123780 * ___playerTank_5;
	// UnityEngine.UI.Text BossUI::playerHealthText
	Text_t1901882714 * ___playerHealthText_6;

public:
	inline static int32_t get_offset_of_enemyTank_2() { return static_cast<int32_t>(offsetof(BossUI_t1795207708, ___enemyTank_2)); }
	inline EnemyStats_t4187841152 * get_enemyTank_2() const { return ___enemyTank_2; }
	inline EnemyStats_t4187841152 ** get_address_of_enemyTank_2() { return &___enemyTank_2; }
	inline void set_enemyTank_2(EnemyStats_t4187841152 * value)
	{
		___enemyTank_2 = value;
		Il2CppCodeGenWriteBarrier((&___enemyTank_2), value);
	}

	inline static int32_t get_offset_of_enemyHealthBar_3() { return static_cast<int32_t>(offsetof(BossUI_t1795207708, ___enemyHealthBar_3)); }
	inline Image_t2670269651 * get_enemyHealthBar_3() const { return ___enemyHealthBar_3; }
	inline Image_t2670269651 ** get_address_of_enemyHealthBar_3() { return &___enemyHealthBar_3; }
	inline void set_enemyHealthBar_3(Image_t2670269651 * value)
	{
		___enemyHealthBar_3 = value;
		Il2CppCodeGenWriteBarrier((&___enemyHealthBar_3), value);
	}

	inline static int32_t get_offset_of_enemyHealthBarValue_4() { return static_cast<int32_t>(offsetof(BossUI_t1795207708, ___enemyHealthBarValue_4)); }
	inline float get_enemyHealthBarValue_4() const { return ___enemyHealthBarValue_4; }
	inline float* get_address_of_enemyHealthBarValue_4() { return &___enemyHealthBarValue_4; }
	inline void set_enemyHealthBarValue_4(float value)
	{
		___enemyHealthBarValue_4 = value;
	}

	inline static int32_t get_offset_of_playerTank_5() { return static_cast<int32_t>(offsetof(BossUI_t1795207708, ___playerTank_5)); }
	inline PlayerStats_t2044123780 * get_playerTank_5() const { return ___playerTank_5; }
	inline PlayerStats_t2044123780 ** get_address_of_playerTank_5() { return &___playerTank_5; }
	inline void set_playerTank_5(PlayerStats_t2044123780 * value)
	{
		___playerTank_5 = value;
		Il2CppCodeGenWriteBarrier((&___playerTank_5), value);
	}

	inline static int32_t get_offset_of_playerHealthText_6() { return static_cast<int32_t>(offsetof(BossUI_t1795207708, ___playerHealthText_6)); }
	inline Text_t1901882714 * get_playerHealthText_6() const { return ___playerHealthText_6; }
	inline Text_t1901882714 ** get_address_of_playerHealthText_6() { return &___playerHealthText_6; }
	inline void set_playerHealthText_6(Text_t1901882714 * value)
	{
		___playerHealthText_6 = value;
		Il2CppCodeGenWriteBarrier((&___playerHealthText_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOSSUI_T1795207708_H
#ifndef ENEMYCONTROLLER_T2191660454_H
#define ENEMYCONTROLLER_T2191660454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyController
struct  EnemyController_t2191660454  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AI.NavMeshAgent EnemyController::agent
	NavMeshAgent_t1276799816 * ___agent_2;
	// Gun EnemyController::gun
	Gun_t783153271 * ___gun_3;
	// System.Single EnemyController::lookRadius
	float ___lookRadius_4;
	// System.Single EnemyController::reloadTime
	float ___reloadTime_5;
	// UnityEngine.Transform EnemyController::target
	Transform_t3600365921 * ___target_6;
	// UnityEngine.BoxCollider EnemyController::fieldOfViewBox
	BoxCollider_t1640800422 * ___fieldOfViewBox_7;
	// System.Boolean EnemyController::spottedPlayer
	bool ___spottedPlayer_8;

public:
	inline static int32_t get_offset_of_agent_2() { return static_cast<int32_t>(offsetof(EnemyController_t2191660454, ___agent_2)); }
	inline NavMeshAgent_t1276799816 * get_agent_2() const { return ___agent_2; }
	inline NavMeshAgent_t1276799816 ** get_address_of_agent_2() { return &___agent_2; }
	inline void set_agent_2(NavMeshAgent_t1276799816 * value)
	{
		___agent_2 = value;
		Il2CppCodeGenWriteBarrier((&___agent_2), value);
	}

	inline static int32_t get_offset_of_gun_3() { return static_cast<int32_t>(offsetof(EnemyController_t2191660454, ___gun_3)); }
	inline Gun_t783153271 * get_gun_3() const { return ___gun_3; }
	inline Gun_t783153271 ** get_address_of_gun_3() { return &___gun_3; }
	inline void set_gun_3(Gun_t783153271 * value)
	{
		___gun_3 = value;
		Il2CppCodeGenWriteBarrier((&___gun_3), value);
	}

	inline static int32_t get_offset_of_lookRadius_4() { return static_cast<int32_t>(offsetof(EnemyController_t2191660454, ___lookRadius_4)); }
	inline float get_lookRadius_4() const { return ___lookRadius_4; }
	inline float* get_address_of_lookRadius_4() { return &___lookRadius_4; }
	inline void set_lookRadius_4(float value)
	{
		___lookRadius_4 = value;
	}

	inline static int32_t get_offset_of_reloadTime_5() { return static_cast<int32_t>(offsetof(EnemyController_t2191660454, ___reloadTime_5)); }
	inline float get_reloadTime_5() const { return ___reloadTime_5; }
	inline float* get_address_of_reloadTime_5() { return &___reloadTime_5; }
	inline void set_reloadTime_5(float value)
	{
		___reloadTime_5 = value;
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(EnemyController_t2191660454, ___target_6)); }
	inline Transform_t3600365921 * get_target_6() const { return ___target_6; }
	inline Transform_t3600365921 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Transform_t3600365921 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_fieldOfViewBox_7() { return static_cast<int32_t>(offsetof(EnemyController_t2191660454, ___fieldOfViewBox_7)); }
	inline BoxCollider_t1640800422 * get_fieldOfViewBox_7() const { return ___fieldOfViewBox_7; }
	inline BoxCollider_t1640800422 ** get_address_of_fieldOfViewBox_7() { return &___fieldOfViewBox_7; }
	inline void set_fieldOfViewBox_7(BoxCollider_t1640800422 * value)
	{
		___fieldOfViewBox_7 = value;
		Il2CppCodeGenWriteBarrier((&___fieldOfViewBox_7), value);
	}

	inline static int32_t get_offset_of_spottedPlayer_8() { return static_cast<int32_t>(offsetof(EnemyController_t2191660454, ___spottedPlayer_8)); }
	inline bool get_spottedPlayer_8() const { return ___spottedPlayer_8; }
	inline bool* get_address_of_spottedPlayer_8() { return &___spottedPlayer_8; }
	inline void set_spottedPlayer_8(bool value)
	{
		___spottedPlayer_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYCONTROLLER_T2191660454_H
#ifndef ENEMYFUZZYSTATE_T3026123389_H
#define ENEMYFUZZYSTATE_T3026123389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyFuzzyState
struct  EnemyFuzzyState_t3026123389  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AnimationCurve EnemyFuzzyState::critical
	AnimationCurve_t3046754366 * ___critical_2;
	// UnityEngine.AnimationCurve EnemyFuzzyState::hurt
	AnimationCurve_t3046754366 * ___hurt_3;
	// UnityEngine.AnimationCurve EnemyFuzzyState::healthy
	AnimationCurve_t3046754366 * ___healthy_4;
	// EnemyStats EnemyFuzzyState::enemyTank
	EnemyStats_t4187841152 * ___enemyTank_5;
	// UnityEngine.UI.Text EnemyFuzzyState::fuzzyState
	Text_t1901882714 * ___fuzzyState_6;
	// System.Single EnemyFuzzyState::enemyHealth
	float ___enemyHealth_7;
	// System.Single EnemyFuzzyState::valueHealthy
	float ___valueHealthy_9;
	// System.Single EnemyFuzzyState::valueHurt
	float ___valueHurt_10;
	// System.Single EnemyFuzzyState::valueCritical
	float ___valueCritical_11;

public:
	inline static int32_t get_offset_of_critical_2() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___critical_2)); }
	inline AnimationCurve_t3046754366 * get_critical_2() const { return ___critical_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_critical_2() { return &___critical_2; }
	inline void set_critical_2(AnimationCurve_t3046754366 * value)
	{
		___critical_2 = value;
		Il2CppCodeGenWriteBarrier((&___critical_2), value);
	}

	inline static int32_t get_offset_of_hurt_3() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___hurt_3)); }
	inline AnimationCurve_t3046754366 * get_hurt_3() const { return ___hurt_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_hurt_3() { return &___hurt_3; }
	inline void set_hurt_3(AnimationCurve_t3046754366 * value)
	{
		___hurt_3 = value;
		Il2CppCodeGenWriteBarrier((&___hurt_3), value);
	}

	inline static int32_t get_offset_of_healthy_4() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___healthy_4)); }
	inline AnimationCurve_t3046754366 * get_healthy_4() const { return ___healthy_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_healthy_4() { return &___healthy_4; }
	inline void set_healthy_4(AnimationCurve_t3046754366 * value)
	{
		___healthy_4 = value;
		Il2CppCodeGenWriteBarrier((&___healthy_4), value);
	}

	inline static int32_t get_offset_of_enemyTank_5() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___enemyTank_5)); }
	inline EnemyStats_t4187841152 * get_enemyTank_5() const { return ___enemyTank_5; }
	inline EnemyStats_t4187841152 ** get_address_of_enemyTank_5() { return &___enemyTank_5; }
	inline void set_enemyTank_5(EnemyStats_t4187841152 * value)
	{
		___enemyTank_5 = value;
		Il2CppCodeGenWriteBarrier((&___enemyTank_5), value);
	}

	inline static int32_t get_offset_of_fuzzyState_6() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___fuzzyState_6)); }
	inline Text_t1901882714 * get_fuzzyState_6() const { return ___fuzzyState_6; }
	inline Text_t1901882714 ** get_address_of_fuzzyState_6() { return &___fuzzyState_6; }
	inline void set_fuzzyState_6(Text_t1901882714 * value)
	{
		___fuzzyState_6 = value;
		Il2CppCodeGenWriteBarrier((&___fuzzyState_6), value);
	}

	inline static int32_t get_offset_of_enemyHealth_7() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___enemyHealth_7)); }
	inline float get_enemyHealth_7() const { return ___enemyHealth_7; }
	inline float* get_address_of_enemyHealth_7() { return &___enemyHealth_7; }
	inline void set_enemyHealth_7(float value)
	{
		___enemyHealth_7 = value;
	}

	inline static int32_t get_offset_of_valueHealthy_9() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___valueHealthy_9)); }
	inline float get_valueHealthy_9() const { return ___valueHealthy_9; }
	inline float* get_address_of_valueHealthy_9() { return &___valueHealthy_9; }
	inline void set_valueHealthy_9(float value)
	{
		___valueHealthy_9 = value;
	}

	inline static int32_t get_offset_of_valueHurt_10() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___valueHurt_10)); }
	inline float get_valueHurt_10() const { return ___valueHurt_10; }
	inline float* get_address_of_valueHurt_10() { return &___valueHurt_10; }
	inline void set_valueHurt_10(float value)
	{
		___valueHurt_10 = value;
	}

	inline static int32_t get_offset_of_valueCritical_11() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389, ___valueCritical_11)); }
	inline float get_valueCritical_11() const { return ___valueCritical_11; }
	inline float* get_address_of_valueCritical_11() { return &___valueCritical_11; }
	inline void set_valueCritical_11(float value)
	{
		___valueCritical_11 = value;
	}
};

struct EnemyFuzzyState_t3026123389_StaticFields
{
public:
	// EnemyFuzzyState/FuzzyStates EnemyFuzzyState::currentFuzzyState
	int32_t ___currentFuzzyState_8;

public:
	inline static int32_t get_offset_of_currentFuzzyState_8() { return static_cast<int32_t>(offsetof(EnemyFuzzyState_t3026123389_StaticFields, ___currentFuzzyState_8)); }
	inline int32_t get_currentFuzzyState_8() const { return ___currentFuzzyState_8; }
	inline int32_t* get_address_of_currentFuzzyState_8() { return &___currentFuzzyState_8; }
	inline void set_currentFuzzyState_8(int32_t value)
	{
		___currentFuzzyState_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMYFUZZYSTATE_T3026123389_H
#ifndef HEALTHUI_T1908446248_H
#define HEALTHUI_T1908446248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HealthUI
struct  HealthUI_t1908446248  : public MonoBehaviour_t3962482529
{
public:
	// PlayerStats HealthUI::playerStats
	PlayerStats_t2044123780 * ___playerStats_2;
	// UnityEngine.AudioClip HealthUI::destroyBullet
	AudioClip_t3680889665 * ___destroyBullet_3;
	// UnityEngine.UI.Image[] HealthUI::healthHearts
	ImageU5BU5D_t2439009922* ___healthHearts_4;

public:
	inline static int32_t get_offset_of_playerStats_2() { return static_cast<int32_t>(offsetof(HealthUI_t1908446248, ___playerStats_2)); }
	inline PlayerStats_t2044123780 * get_playerStats_2() const { return ___playerStats_2; }
	inline PlayerStats_t2044123780 ** get_address_of_playerStats_2() { return &___playerStats_2; }
	inline void set_playerStats_2(PlayerStats_t2044123780 * value)
	{
		___playerStats_2 = value;
		Il2CppCodeGenWriteBarrier((&___playerStats_2), value);
	}

	inline static int32_t get_offset_of_destroyBullet_3() { return static_cast<int32_t>(offsetof(HealthUI_t1908446248, ___destroyBullet_3)); }
	inline AudioClip_t3680889665 * get_destroyBullet_3() const { return ___destroyBullet_3; }
	inline AudioClip_t3680889665 ** get_address_of_destroyBullet_3() { return &___destroyBullet_3; }
	inline void set_destroyBullet_3(AudioClip_t3680889665 * value)
	{
		___destroyBullet_3 = value;
		Il2CppCodeGenWriteBarrier((&___destroyBullet_3), value);
	}

	inline static int32_t get_offset_of_healthHearts_4() { return static_cast<int32_t>(offsetof(HealthUI_t1908446248, ___healthHearts_4)); }
	inline ImageU5BU5D_t2439009922* get_healthHearts_4() const { return ___healthHearts_4; }
	inline ImageU5BU5D_t2439009922** get_address_of_healthHearts_4() { return &___healthHearts_4; }
	inline void set_healthHearts_4(ImageU5BU5D_t2439009922* value)
	{
		___healthHearts_4 = value;
		Il2CppCodeGenWriteBarrier((&___healthHearts_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEALTHUI_T1908446248_H
#ifndef INTERACTIBLE_T1060880155_H
#define INTERACTIBLE_T1060880155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Interactible
struct  Interactible_t1060880155  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Interactible::radius
	float ___radius_2;
	// UnityEngine.Transform Interactible::interactionTransform
	Transform_t3600365921 * ___interactionTransform_3;

public:
	inline static int32_t get_offset_of_radius_2() { return static_cast<int32_t>(offsetof(Interactible_t1060880155, ___radius_2)); }
	inline float get_radius_2() const { return ___radius_2; }
	inline float* get_address_of_radius_2() { return &___radius_2; }
	inline void set_radius_2(float value)
	{
		___radius_2 = value;
	}

	inline static int32_t get_offset_of_interactionTransform_3() { return static_cast<int32_t>(offsetof(Interactible_t1060880155, ___interactionTransform_3)); }
	inline Transform_t3600365921 * get_interactionTransform_3() const { return ___interactionTransform_3; }
	inline Transform_t3600365921 ** get_address_of_interactionTransform_3() { return &___interactionTransform_3; }
	inline void set_interactionTransform_3(Transform_t3600365921 * value)
	{
		___interactionTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___interactionTransform_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERACTIBLE_T1060880155_H
#ifndef HEALINGSPOT_T2443304191_H
#define HEALINGSPOT_T2443304191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HealingSpot
struct  HealingSpot_t2443304191  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 HealingSpot::positionsIndex
	int32_t ___positionsIndex_2;
	// UnityEngine.Vector3[] HealingSpot::positions
	Vector3U5BU5D_t1718750761* ___positions_3;

public:
	inline static int32_t get_offset_of_positionsIndex_2() { return static_cast<int32_t>(offsetof(HealingSpot_t2443304191, ___positionsIndex_2)); }
	inline int32_t get_positionsIndex_2() const { return ___positionsIndex_2; }
	inline int32_t* get_address_of_positionsIndex_2() { return &___positionsIndex_2; }
	inline void set_positionsIndex_2(int32_t value)
	{
		___positionsIndex_2 = value;
	}

	inline static int32_t get_offset_of_positions_3() { return static_cast<int32_t>(offsetof(HealingSpot_t2443304191, ___positions_3)); }
	inline Vector3U5BU5D_t1718750761* get_positions_3() const { return ___positions_3; }
	inline Vector3U5BU5D_t1718750761** get_address_of_positions_3() { return &___positions_3; }
	inline void set_positions_3(Vector3U5BU5D_t1718750761* value)
	{
		___positions_3 = value;
		Il2CppCodeGenWriteBarrier((&___positions_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEALINGSPOT_T2443304191_H
#ifndef FUZZYSAMPLE_T2008138351_H
#define FUZZYSAMPLE_T2008138351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FuzzySample
struct  FuzzySample_t2008138351  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AnimationCurve FuzzySample::critical
	AnimationCurve_t3046754366 * ___critical_2;
	// UnityEngine.AnimationCurve FuzzySample::hurt
	AnimationCurve_t3046754366 * ___hurt_3;
	// UnityEngine.AnimationCurve FuzzySample::healthy
	AnimationCurve_t3046754366 * ___healthy_4;
	// UnityEngine.UI.InputField FuzzySample::healthInput
	InputField_t3762917431 * ___healthInput_5;
	// UnityEngine.UI.Text FuzzySample::labelHealthy
	Text_t1901882714 * ___labelHealthy_6;
	// UnityEngine.UI.Text FuzzySample::labelHurt
	Text_t1901882714 * ___labelHurt_7;
	// UnityEngine.UI.Text FuzzySample::labelCritical
	Text_t1901882714 * ___labelCritical_8;
	// UnityEngine.UI.Text FuzzySample::fuzzyState
	Text_t1901882714 * ___fuzzyState_9;
	// System.Single FuzzySample::valueHealthy
	float ___valueHealthy_11;
	// System.Single FuzzySample::valueHurt
	float ___valueHurt_12;
	// System.Single FuzzySample::valueCritical
	float ___valueCritical_13;

public:
	inline static int32_t get_offset_of_critical_2() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___critical_2)); }
	inline AnimationCurve_t3046754366 * get_critical_2() const { return ___critical_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_critical_2() { return &___critical_2; }
	inline void set_critical_2(AnimationCurve_t3046754366 * value)
	{
		___critical_2 = value;
		Il2CppCodeGenWriteBarrier((&___critical_2), value);
	}

	inline static int32_t get_offset_of_hurt_3() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___hurt_3)); }
	inline AnimationCurve_t3046754366 * get_hurt_3() const { return ___hurt_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_hurt_3() { return &___hurt_3; }
	inline void set_hurt_3(AnimationCurve_t3046754366 * value)
	{
		___hurt_3 = value;
		Il2CppCodeGenWriteBarrier((&___hurt_3), value);
	}

	inline static int32_t get_offset_of_healthy_4() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___healthy_4)); }
	inline AnimationCurve_t3046754366 * get_healthy_4() const { return ___healthy_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_healthy_4() { return &___healthy_4; }
	inline void set_healthy_4(AnimationCurve_t3046754366 * value)
	{
		___healthy_4 = value;
		Il2CppCodeGenWriteBarrier((&___healthy_4), value);
	}

	inline static int32_t get_offset_of_healthInput_5() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___healthInput_5)); }
	inline InputField_t3762917431 * get_healthInput_5() const { return ___healthInput_5; }
	inline InputField_t3762917431 ** get_address_of_healthInput_5() { return &___healthInput_5; }
	inline void set_healthInput_5(InputField_t3762917431 * value)
	{
		___healthInput_5 = value;
		Il2CppCodeGenWriteBarrier((&___healthInput_5), value);
	}

	inline static int32_t get_offset_of_labelHealthy_6() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___labelHealthy_6)); }
	inline Text_t1901882714 * get_labelHealthy_6() const { return ___labelHealthy_6; }
	inline Text_t1901882714 ** get_address_of_labelHealthy_6() { return &___labelHealthy_6; }
	inline void set_labelHealthy_6(Text_t1901882714 * value)
	{
		___labelHealthy_6 = value;
		Il2CppCodeGenWriteBarrier((&___labelHealthy_6), value);
	}

	inline static int32_t get_offset_of_labelHurt_7() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___labelHurt_7)); }
	inline Text_t1901882714 * get_labelHurt_7() const { return ___labelHurt_7; }
	inline Text_t1901882714 ** get_address_of_labelHurt_7() { return &___labelHurt_7; }
	inline void set_labelHurt_7(Text_t1901882714 * value)
	{
		___labelHurt_7 = value;
		Il2CppCodeGenWriteBarrier((&___labelHurt_7), value);
	}

	inline static int32_t get_offset_of_labelCritical_8() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___labelCritical_8)); }
	inline Text_t1901882714 * get_labelCritical_8() const { return ___labelCritical_8; }
	inline Text_t1901882714 ** get_address_of_labelCritical_8() { return &___labelCritical_8; }
	inline void set_labelCritical_8(Text_t1901882714 * value)
	{
		___labelCritical_8 = value;
		Il2CppCodeGenWriteBarrier((&___labelCritical_8), value);
	}

	inline static int32_t get_offset_of_fuzzyState_9() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___fuzzyState_9)); }
	inline Text_t1901882714 * get_fuzzyState_9() const { return ___fuzzyState_9; }
	inline Text_t1901882714 ** get_address_of_fuzzyState_9() { return &___fuzzyState_9; }
	inline void set_fuzzyState_9(Text_t1901882714 * value)
	{
		___fuzzyState_9 = value;
		Il2CppCodeGenWriteBarrier((&___fuzzyState_9), value);
	}

	inline static int32_t get_offset_of_valueHealthy_11() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___valueHealthy_11)); }
	inline float get_valueHealthy_11() const { return ___valueHealthy_11; }
	inline float* get_address_of_valueHealthy_11() { return &___valueHealthy_11; }
	inline void set_valueHealthy_11(float value)
	{
		___valueHealthy_11 = value;
	}

	inline static int32_t get_offset_of_valueHurt_12() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___valueHurt_12)); }
	inline float get_valueHurt_12() const { return ___valueHurt_12; }
	inline float* get_address_of_valueHurt_12() { return &___valueHurt_12; }
	inline void set_valueHurt_12(float value)
	{
		___valueHurt_12 = value;
	}

	inline static int32_t get_offset_of_valueCritical_13() { return static_cast<int32_t>(offsetof(FuzzySample_t2008138351, ___valueCritical_13)); }
	inline float get_valueCritical_13() const { return ___valueCritical_13; }
	inline float* get_address_of_valueCritical_13() { return &___valueCritical_13; }
	inline void set_valueCritical_13(float value)
	{
		___valueCritical_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUZZYSAMPLE_T2008138351_H
#ifndef GAMEOVERSCREEN_T2285777029_H
#define GAMEOVERSCREEN_T2285777029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameOverScreen
struct  GameOverScreen_t2285777029  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 GameOverScreen::index
	int32_t ___index_2;
	// UnityEngine.AudioClip GameOverScreen::selectSound
	AudioClip_t3680889665 * ___selectSound_3;
	// UnityEngine.AudioClip GameOverScreen::confirmSound
	AudioClip_t3680889665 * ___confirmSound_4;
	// UnityEngine.GameObject[] GameOverScreen::options
	GameObjectU5BU5D_t3328599146* ___options_5;
	// UnityEngine.Sprite[] GameOverScreen::optionBox
	SpriteU5BU5D_t2581906349* ___optionBox_6;
	// LevelChanger GameOverScreen::levelChanger
	LevelChanger_t225386971 * ___levelChanger_7;
	// System.Boolean GameOverScreen::leftAxesInUse
	bool ___leftAxesInUse_8;
	// System.Boolean GameOverScreen::rightAxesInUse
	bool ___rightAxesInUse_9;
	// System.Boolean GameOverScreen::confirmAxesInUse
	bool ___confirmAxesInUse_10;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_selectSound_3() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___selectSound_3)); }
	inline AudioClip_t3680889665 * get_selectSound_3() const { return ___selectSound_3; }
	inline AudioClip_t3680889665 ** get_address_of_selectSound_3() { return &___selectSound_3; }
	inline void set_selectSound_3(AudioClip_t3680889665 * value)
	{
		___selectSound_3 = value;
		Il2CppCodeGenWriteBarrier((&___selectSound_3), value);
	}

	inline static int32_t get_offset_of_confirmSound_4() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___confirmSound_4)); }
	inline AudioClip_t3680889665 * get_confirmSound_4() const { return ___confirmSound_4; }
	inline AudioClip_t3680889665 ** get_address_of_confirmSound_4() { return &___confirmSound_4; }
	inline void set_confirmSound_4(AudioClip_t3680889665 * value)
	{
		___confirmSound_4 = value;
		Il2CppCodeGenWriteBarrier((&___confirmSound_4), value);
	}

	inline static int32_t get_offset_of_options_5() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___options_5)); }
	inline GameObjectU5BU5D_t3328599146* get_options_5() const { return ___options_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_options_5() { return &___options_5; }
	inline void set_options_5(GameObjectU5BU5D_t3328599146* value)
	{
		___options_5 = value;
		Il2CppCodeGenWriteBarrier((&___options_5), value);
	}

	inline static int32_t get_offset_of_optionBox_6() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___optionBox_6)); }
	inline SpriteU5BU5D_t2581906349* get_optionBox_6() const { return ___optionBox_6; }
	inline SpriteU5BU5D_t2581906349** get_address_of_optionBox_6() { return &___optionBox_6; }
	inline void set_optionBox_6(SpriteU5BU5D_t2581906349* value)
	{
		___optionBox_6 = value;
		Il2CppCodeGenWriteBarrier((&___optionBox_6), value);
	}

	inline static int32_t get_offset_of_levelChanger_7() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___levelChanger_7)); }
	inline LevelChanger_t225386971 * get_levelChanger_7() const { return ___levelChanger_7; }
	inline LevelChanger_t225386971 ** get_address_of_levelChanger_7() { return &___levelChanger_7; }
	inline void set_levelChanger_7(LevelChanger_t225386971 * value)
	{
		___levelChanger_7 = value;
		Il2CppCodeGenWriteBarrier((&___levelChanger_7), value);
	}

	inline static int32_t get_offset_of_leftAxesInUse_8() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___leftAxesInUse_8)); }
	inline bool get_leftAxesInUse_8() const { return ___leftAxesInUse_8; }
	inline bool* get_address_of_leftAxesInUse_8() { return &___leftAxesInUse_8; }
	inline void set_leftAxesInUse_8(bool value)
	{
		___leftAxesInUse_8 = value;
	}

	inline static int32_t get_offset_of_rightAxesInUse_9() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___rightAxesInUse_9)); }
	inline bool get_rightAxesInUse_9() const { return ___rightAxesInUse_9; }
	inline bool* get_address_of_rightAxesInUse_9() { return &___rightAxesInUse_9; }
	inline void set_rightAxesInUse_9(bool value)
	{
		___rightAxesInUse_9 = value;
	}

	inline static int32_t get_offset_of_confirmAxesInUse_10() { return static_cast<int32_t>(offsetof(GameOverScreen_t2285777029, ___confirmAxesInUse_10)); }
	inline bool get_confirmAxesInUse_10() const { return ___confirmAxesInUse_10; }
	inline bool* get_address_of_confirmAxesInUse_10() { return &___confirmAxesInUse_10; }
	inline void set_confirmAxesInUse_10(bool value)
	{
		___confirmAxesInUse_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOVERSCREEN_T2285777029_H
#ifndef PLAYERHEALTHUI_T636700963_H
#define PLAYERHEALTHUI_T636700963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerHealthUI
struct  PlayerHealthUI_t636700963  : public MonoBehaviour_t3962482529
{
public:
	// PlayerStats PlayerHealthUI::pStats
	PlayerStats_t2044123780 * ___pStats_2;
	// UnityEngine.UI.Text PlayerHealthUI::pHealthValue
	Text_t1901882714 * ___pHealthValue_3;

public:
	inline static int32_t get_offset_of_pStats_2() { return static_cast<int32_t>(offsetof(PlayerHealthUI_t636700963, ___pStats_2)); }
	inline PlayerStats_t2044123780 * get_pStats_2() const { return ___pStats_2; }
	inline PlayerStats_t2044123780 ** get_address_of_pStats_2() { return &___pStats_2; }
	inline void set_pStats_2(PlayerStats_t2044123780 * value)
	{
		___pStats_2 = value;
		Il2CppCodeGenWriteBarrier((&___pStats_2), value);
	}

	inline static int32_t get_offset_of_pHealthValue_3() { return static_cast<int32_t>(offsetof(PlayerHealthUI_t636700963, ___pHealthValue_3)); }
	inline Text_t1901882714 * get_pHealthValue_3() const { return ___pHealthValue_3; }
	inline Text_t1901882714 ** get_address_of_pHealthValue_3() { return &___pHealthValue_3; }
	inline void set_pHealthValue_3(Text_t1901882714 * value)
	{
		___pHealthValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___pHealthValue_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERHEALTHUI_T636700963_H
#ifndef PLAYERMANAGER_T1349889689_H
#define PLAYERMANAGER_T1349889689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerManager
struct  PlayerManager_t1349889689  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PlayerManager::player
	GameObject_t1113636619 * ___player_3;

public:
	inline static int32_t get_offset_of_player_3() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689, ___player_3)); }
	inline GameObject_t1113636619 * get_player_3() const { return ___player_3; }
	inline GameObject_t1113636619 ** get_address_of_player_3() { return &___player_3; }
	inline void set_player_3(GameObject_t1113636619 * value)
	{
		___player_3 = value;
		Il2CppCodeGenWriteBarrier((&___player_3), value);
	}
};

struct PlayerManager_t1349889689_StaticFields
{
public:
	// PlayerManager PlayerManager::instance
	PlayerManager_t1349889689 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(PlayerManager_t1349889689_StaticFields, ___instance_2)); }
	inline PlayerManager_t1349889689 * get_instance_2() const { return ___instance_2; }
	inline PlayerManager_t1349889689 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(PlayerManager_t1349889689 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMANAGER_T1349889689_H
#ifndef PLAYERCONTROLLER_T2064355688_H
#define PLAYERCONTROLLER_T2064355688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerController
struct  PlayerController_t2064355688  : public MonoBehaviour_t3962482529
{
public:
	// Inventory PlayerController::inventory
	Inventory_t1050226016 * ___inventory_2;
	// System.Boolean PlayerController::laserGunEnabled
	bool ___laserGunEnabled_3;
	// System.Boolean PlayerController::laserShieldEnabled
	bool ___laserShieldEnabled_4;
	// Gun PlayerController::gun
	Gun_t783153271 * ___gun_5;
	// System.Single PlayerController::moveSpeed
	float ___moveSpeed_6;
	// System.Single PlayerController::turnSpeed
	float ___turnSpeed_7;
	// UnityEngine.Camera PlayerController::ourCamera
	Camera_t4157153871 * ___ourCamera_8;
	// System.Single PlayerController::reloadTime
	float ___reloadTime_9;

public:
	inline static int32_t get_offset_of_inventory_2() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___inventory_2)); }
	inline Inventory_t1050226016 * get_inventory_2() const { return ___inventory_2; }
	inline Inventory_t1050226016 ** get_address_of_inventory_2() { return &___inventory_2; }
	inline void set_inventory_2(Inventory_t1050226016 * value)
	{
		___inventory_2 = value;
		Il2CppCodeGenWriteBarrier((&___inventory_2), value);
	}

	inline static int32_t get_offset_of_laserGunEnabled_3() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___laserGunEnabled_3)); }
	inline bool get_laserGunEnabled_3() const { return ___laserGunEnabled_3; }
	inline bool* get_address_of_laserGunEnabled_3() { return &___laserGunEnabled_3; }
	inline void set_laserGunEnabled_3(bool value)
	{
		___laserGunEnabled_3 = value;
	}

	inline static int32_t get_offset_of_laserShieldEnabled_4() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___laserShieldEnabled_4)); }
	inline bool get_laserShieldEnabled_4() const { return ___laserShieldEnabled_4; }
	inline bool* get_address_of_laserShieldEnabled_4() { return &___laserShieldEnabled_4; }
	inline void set_laserShieldEnabled_4(bool value)
	{
		___laserShieldEnabled_4 = value;
	}

	inline static int32_t get_offset_of_gun_5() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___gun_5)); }
	inline Gun_t783153271 * get_gun_5() const { return ___gun_5; }
	inline Gun_t783153271 ** get_address_of_gun_5() { return &___gun_5; }
	inline void set_gun_5(Gun_t783153271 * value)
	{
		___gun_5 = value;
		Il2CppCodeGenWriteBarrier((&___gun_5), value);
	}

	inline static int32_t get_offset_of_moveSpeed_6() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___moveSpeed_6)); }
	inline float get_moveSpeed_6() const { return ___moveSpeed_6; }
	inline float* get_address_of_moveSpeed_6() { return &___moveSpeed_6; }
	inline void set_moveSpeed_6(float value)
	{
		___moveSpeed_6 = value;
	}

	inline static int32_t get_offset_of_turnSpeed_7() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___turnSpeed_7)); }
	inline float get_turnSpeed_7() const { return ___turnSpeed_7; }
	inline float* get_address_of_turnSpeed_7() { return &___turnSpeed_7; }
	inline void set_turnSpeed_7(float value)
	{
		___turnSpeed_7 = value;
	}

	inline static int32_t get_offset_of_ourCamera_8() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___ourCamera_8)); }
	inline Camera_t4157153871 * get_ourCamera_8() const { return ___ourCamera_8; }
	inline Camera_t4157153871 ** get_address_of_ourCamera_8() { return &___ourCamera_8; }
	inline void set_ourCamera_8(Camera_t4157153871 * value)
	{
		___ourCamera_8 = value;
		Il2CppCodeGenWriteBarrier((&___ourCamera_8), value);
	}

	inline static int32_t get_offset_of_reloadTime_9() { return static_cast<int32_t>(offsetof(PlayerController_t2064355688, ___reloadTime_9)); }
	inline float get_reloadTime_9() const { return ___reloadTime_9; }
	inline float* get_address_of_reloadTime_9() { return &___reloadTime_9; }
	inline void set_reloadTime_9(float value)
	{
		___reloadTime_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCONTROLLER_T2064355688_H
#ifndef GAMECAMERA_T2564829307_H
#define GAMECAMERA_T2564829307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameCamera
struct  GameCamera_t2564829307  : public MonoBehaviour_t3962482529
{
public:
	// System.Single GameCamera::xModifier
	float ___xModifier_2;
	// System.Single GameCamera::yModifier
	float ___yModifier_3;
	// System.Single GameCamera::zModifier
	float ___zModifier_4;
	// UnityEngine.Vector3 GameCamera::cameraTarget
	Vector3_t3722313464  ___cameraTarget_5;
	// UnityEngine.Transform GameCamera::target
	Transform_t3600365921 * ___target_6;

public:
	inline static int32_t get_offset_of_xModifier_2() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307, ___xModifier_2)); }
	inline float get_xModifier_2() const { return ___xModifier_2; }
	inline float* get_address_of_xModifier_2() { return &___xModifier_2; }
	inline void set_xModifier_2(float value)
	{
		___xModifier_2 = value;
	}

	inline static int32_t get_offset_of_yModifier_3() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307, ___yModifier_3)); }
	inline float get_yModifier_3() const { return ___yModifier_3; }
	inline float* get_address_of_yModifier_3() { return &___yModifier_3; }
	inline void set_yModifier_3(float value)
	{
		___yModifier_3 = value;
	}

	inline static int32_t get_offset_of_zModifier_4() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307, ___zModifier_4)); }
	inline float get_zModifier_4() const { return ___zModifier_4; }
	inline float* get_address_of_zModifier_4() { return &___zModifier_4; }
	inline void set_zModifier_4(float value)
	{
		___zModifier_4 = value;
	}

	inline static int32_t get_offset_of_cameraTarget_5() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307, ___cameraTarget_5)); }
	inline Vector3_t3722313464  get_cameraTarget_5() const { return ___cameraTarget_5; }
	inline Vector3_t3722313464 * get_address_of_cameraTarget_5() { return &___cameraTarget_5; }
	inline void set_cameraTarget_5(Vector3_t3722313464  value)
	{
		___cameraTarget_5 = value;
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(GameCamera_t2564829307, ___target_6)); }
	inline Transform_t3600365921 * get_target_6() const { return ___target_6; }
	inline Transform_t3600365921 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Transform_t3600365921 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECAMERA_T2564829307_H
#ifndef GUN_T783153271_H
#define GUN_T783153271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gun
struct  Gun_t783153271  : public MonoBehaviour_t3962482529
{
public:
	// Gun/GunType Gun::gunType
	int32_t ___gunType_2;
	// System.Single Gun::rpm
	float ___rpm_3;
	// UnityEngine.AudioClip Gun::gunShot
	AudioClip_t3680889665 * ___gunShot_4;
	// UnityEngine.AudioClip Gun::laserShot
	AudioClip_t3680889665 * ___laserShot_5;
	// UnityEngine.Transform Gun::spawn
	Transform_t3600365921 * ___spawn_6;
	// UnityEngine.GameObject Gun::shell
	GameObject_t1113636619 * ___shell_7;
	// UnityEngine.GameObject Gun::laser
	GameObject_t1113636619 * ___laser_8;
	// System.Int32 Gun::rounds
	int32_t ___rounds_9;
	// System.Int32 Gun::uses
	int32_t ___uses_10;
	// System.Single Gun::secondsBetweenShots
	float ___secondsBetweenShots_11;
	// System.Single Gun::nextPossibleShootTime
	float ___nextPossibleShootTime_12;

public:
	inline static int32_t get_offset_of_gunType_2() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___gunType_2)); }
	inline int32_t get_gunType_2() const { return ___gunType_2; }
	inline int32_t* get_address_of_gunType_2() { return &___gunType_2; }
	inline void set_gunType_2(int32_t value)
	{
		___gunType_2 = value;
	}

	inline static int32_t get_offset_of_rpm_3() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___rpm_3)); }
	inline float get_rpm_3() const { return ___rpm_3; }
	inline float* get_address_of_rpm_3() { return &___rpm_3; }
	inline void set_rpm_3(float value)
	{
		___rpm_3 = value;
	}

	inline static int32_t get_offset_of_gunShot_4() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___gunShot_4)); }
	inline AudioClip_t3680889665 * get_gunShot_4() const { return ___gunShot_4; }
	inline AudioClip_t3680889665 ** get_address_of_gunShot_4() { return &___gunShot_4; }
	inline void set_gunShot_4(AudioClip_t3680889665 * value)
	{
		___gunShot_4 = value;
		Il2CppCodeGenWriteBarrier((&___gunShot_4), value);
	}

	inline static int32_t get_offset_of_laserShot_5() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___laserShot_5)); }
	inline AudioClip_t3680889665 * get_laserShot_5() const { return ___laserShot_5; }
	inline AudioClip_t3680889665 ** get_address_of_laserShot_5() { return &___laserShot_5; }
	inline void set_laserShot_5(AudioClip_t3680889665 * value)
	{
		___laserShot_5 = value;
		Il2CppCodeGenWriteBarrier((&___laserShot_5), value);
	}

	inline static int32_t get_offset_of_spawn_6() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___spawn_6)); }
	inline Transform_t3600365921 * get_spawn_6() const { return ___spawn_6; }
	inline Transform_t3600365921 ** get_address_of_spawn_6() { return &___spawn_6; }
	inline void set_spawn_6(Transform_t3600365921 * value)
	{
		___spawn_6 = value;
		Il2CppCodeGenWriteBarrier((&___spawn_6), value);
	}

	inline static int32_t get_offset_of_shell_7() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___shell_7)); }
	inline GameObject_t1113636619 * get_shell_7() const { return ___shell_7; }
	inline GameObject_t1113636619 ** get_address_of_shell_7() { return &___shell_7; }
	inline void set_shell_7(GameObject_t1113636619 * value)
	{
		___shell_7 = value;
		Il2CppCodeGenWriteBarrier((&___shell_7), value);
	}

	inline static int32_t get_offset_of_laser_8() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___laser_8)); }
	inline GameObject_t1113636619 * get_laser_8() const { return ___laser_8; }
	inline GameObject_t1113636619 ** get_address_of_laser_8() { return &___laser_8; }
	inline void set_laser_8(GameObject_t1113636619 * value)
	{
		___laser_8 = value;
		Il2CppCodeGenWriteBarrier((&___laser_8), value);
	}

	inline static int32_t get_offset_of_rounds_9() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___rounds_9)); }
	inline int32_t get_rounds_9() const { return ___rounds_9; }
	inline int32_t* get_address_of_rounds_9() { return &___rounds_9; }
	inline void set_rounds_9(int32_t value)
	{
		___rounds_9 = value;
	}

	inline static int32_t get_offset_of_uses_10() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___uses_10)); }
	inline int32_t get_uses_10() const { return ___uses_10; }
	inline int32_t* get_address_of_uses_10() { return &___uses_10; }
	inline void set_uses_10(int32_t value)
	{
		___uses_10 = value;
	}

	inline static int32_t get_offset_of_secondsBetweenShots_11() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___secondsBetweenShots_11)); }
	inline float get_secondsBetweenShots_11() const { return ___secondsBetweenShots_11; }
	inline float* get_address_of_secondsBetweenShots_11() { return &___secondsBetweenShots_11; }
	inline void set_secondsBetweenShots_11(float value)
	{
		___secondsBetweenShots_11 = value;
	}

	inline static int32_t get_offset_of_nextPossibleShootTime_12() { return static_cast<int32_t>(offsetof(Gun_t783153271, ___nextPossibleShootTime_12)); }
	inline float get_nextPossibleShootTime_12() const { return ___nextPossibleShootTime_12; }
	inline float* get_address_of_nextPossibleShootTime_12() { return &___nextPossibleShootTime_12; }
	inline void set_nextPossibleShootTime_12(float value)
	{
		___nextPossibleShootTime_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUN_T783153271_H
#ifndef PLAYERSTATS_T2044123780_H
#define PLAYERSTATS_T2044123780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerStats
struct  PlayerStats_t2044123780  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 PlayerStats::pCurrentHealth
	int32_t ___pCurrentHealth_2;
	// System.Int32 PlayerStats::pMaximumHealth
	int32_t ___pMaximumHealth_3;
	// System.String PlayerStats::gameOverScene
	String_t* ___gameOverScene_4;

public:
	inline static int32_t get_offset_of_pCurrentHealth_2() { return static_cast<int32_t>(offsetof(PlayerStats_t2044123780, ___pCurrentHealth_2)); }
	inline int32_t get_pCurrentHealth_2() const { return ___pCurrentHealth_2; }
	inline int32_t* get_address_of_pCurrentHealth_2() { return &___pCurrentHealth_2; }
	inline void set_pCurrentHealth_2(int32_t value)
	{
		___pCurrentHealth_2 = value;
	}

	inline static int32_t get_offset_of_pMaximumHealth_3() { return static_cast<int32_t>(offsetof(PlayerStats_t2044123780, ___pMaximumHealth_3)); }
	inline int32_t get_pMaximumHealth_3() const { return ___pMaximumHealth_3; }
	inline int32_t* get_address_of_pMaximumHealth_3() { return &___pMaximumHealth_3; }
	inline void set_pMaximumHealth_3(int32_t value)
	{
		___pMaximumHealth_3 = value;
	}

	inline static int32_t get_offset_of_gameOverScene_4() { return static_cast<int32_t>(offsetof(PlayerStats_t2044123780, ___gameOverScene_4)); }
	inline String_t* get_gameOverScene_4() const { return ___gameOverScene_4; }
	inline String_t** get_address_of_gameOverScene_4() { return &___gameOverScene_4; }
	inline void set_gameOverScene_4(String_t* value)
	{
		___gameOverScene_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameOverScene_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERSTATS_T2044123780_H
#ifndef AISTATEMACHINE_T2425577084_H
#define AISTATEMACHINE_T2425577084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AIStateMachine
struct  AIStateMachine_t2425577084  : public MonoBehaviour_t3962482529
{
public:
	// BossTankController AIStateMachine::controller
	BossTankController_t1902331492 * ___controller_6;

public:
	inline static int32_t get_offset_of_controller_6() { return static_cast<int32_t>(offsetof(AIStateMachine_t2425577084, ___controller_6)); }
	inline BossTankController_t1902331492 * get_controller_6() const { return ___controller_6; }
	inline BossTankController_t1902331492 ** get_address_of_controller_6() { return &___controller_6; }
	inline void set_controller_6(BossTankController_t1902331492 * value)
	{
		___controller_6 = value;
		Il2CppCodeGenWriteBarrier((&___controller_6), value);
	}
};

struct AIStateMachine_t2425577084_StaticFields
{
public:
	// AIStateMachine/AIStates AIStateMachine::currentState
	int32_t ___currentState_2;
	// System.Boolean AIStateMachine::phaseOne
	bool ___phaseOne_3;
	// System.Boolean AIStateMachine::phaseTwo
	bool ___phaseTwo_4;
	// System.Boolean AIStateMachine::phaseThree
	bool ___phaseThree_5;

public:
	inline static int32_t get_offset_of_currentState_2() { return static_cast<int32_t>(offsetof(AIStateMachine_t2425577084_StaticFields, ___currentState_2)); }
	inline int32_t get_currentState_2() const { return ___currentState_2; }
	inline int32_t* get_address_of_currentState_2() { return &___currentState_2; }
	inline void set_currentState_2(int32_t value)
	{
		___currentState_2 = value;
	}

	inline static int32_t get_offset_of_phaseOne_3() { return static_cast<int32_t>(offsetof(AIStateMachine_t2425577084_StaticFields, ___phaseOne_3)); }
	inline bool get_phaseOne_3() const { return ___phaseOne_3; }
	inline bool* get_address_of_phaseOne_3() { return &___phaseOne_3; }
	inline void set_phaseOne_3(bool value)
	{
		___phaseOne_3 = value;
	}

	inline static int32_t get_offset_of_phaseTwo_4() { return static_cast<int32_t>(offsetof(AIStateMachine_t2425577084_StaticFields, ___phaseTwo_4)); }
	inline bool get_phaseTwo_4() const { return ___phaseTwo_4; }
	inline bool* get_address_of_phaseTwo_4() { return &___phaseTwo_4; }
	inline void set_phaseTwo_4(bool value)
	{
		___phaseTwo_4 = value;
	}

	inline static int32_t get_offset_of_phaseThree_5() { return static_cast<int32_t>(offsetof(AIStateMachine_t2425577084_StaticFields, ___phaseThree_5)); }
	inline bool get_phaseThree_5() const { return ___phaseThree_5; }
	inline bool* get_address_of_phaseThree_5() { return &___phaseThree_5; }
	inline void set_phaseThree_5(bool value)
	{
		___phaseThree_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AISTATEMACHINE_T2425577084_H
#ifndef BOSSTANKCONTROLLER_T1902331492_H
#define BOSSTANKCONTROLLER_T1902331492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BossTankController
struct  BossTankController_t1902331492  : public MonoBehaviour_t3962482529
{
public:
	// EnemyStats BossTankController::eStats
	EnemyStats_t4187841152 * ___eStats_2;
	// EnemyShoot BossTankController::eShoot
	EnemyShoot_t243830779 * ___eShoot_3;
	// Unit BossTankController::unit
	Unit_t4139495810 * ___unit_4;
	// UnityEngine.Transform BossTankController::player
	Transform_t3600365921 * ___player_5;
	// UnityEngine.Transform BossTankController::healingSpot
	Transform_t3600365921 * ___healingSpot_6;
	// UnityEngine.GameObject BossTankController::bullet
	GameObject_t1113636619 * ___bullet_7;
	// UnityEngine.GameObject BossTankController::laser
	GameObject_t1113636619 * ___laser_8;
	// UnityEngine.GameObject BossTankController::centerPoint
	GameObject_t1113636619 * ___centerPoint_9;

public:
	inline static int32_t get_offset_of_eStats_2() { return static_cast<int32_t>(offsetof(BossTankController_t1902331492, ___eStats_2)); }
	inline EnemyStats_t4187841152 * get_eStats_2() const { return ___eStats_2; }
	inline EnemyStats_t4187841152 ** get_address_of_eStats_2() { return &___eStats_2; }
	inline void set_eStats_2(EnemyStats_t4187841152 * value)
	{
		___eStats_2 = value;
		Il2CppCodeGenWriteBarrier((&___eStats_2), value);
	}

	inline static int32_t get_offset_of_eShoot_3() { return static_cast<int32_t>(offsetof(BossTankController_t1902331492, ___eShoot_3)); }
	inline EnemyShoot_t243830779 * get_eShoot_3() const { return ___eShoot_3; }
	inline EnemyShoot_t243830779 ** get_address_of_eShoot_3() { return &___eShoot_3; }
	inline void set_eShoot_3(EnemyShoot_t243830779 * value)
	{
		___eShoot_3 = value;
		Il2CppCodeGenWriteBarrier((&___eShoot_3), value);
	}

	inline static int32_t get_offset_of_unit_4() { return static_cast<int32_t>(offsetof(BossTankController_t1902331492, ___unit_4)); }
	inline Unit_t4139495810 * get_unit_4() const { return ___unit_4; }
	inline Unit_t4139495810 ** get_address_of_unit_4() { return &___unit_4; }
	inline void set_unit_4(Unit_t4139495810 * value)
	{
		___unit_4 = value;
		Il2CppCodeGenWriteBarrier((&___unit_4), value);
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(BossTankController_t1902331492, ___player_5)); }
	inline Transform_t3600365921 * get_player_5() const { return ___player_5; }
	inline Transform_t3600365921 ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(Transform_t3600365921 * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier((&___player_5), value);
	}

	inline static int32_t get_offset_of_healingSpot_6() { return static_cast<int32_t>(offsetof(BossTankController_t1902331492, ___healingSpot_6)); }
	inline Transform_t3600365921 * get_healingSpot_6() const { return ___healingSpot_6; }
	inline Transform_t3600365921 ** get_address_of_healingSpot_6() { return &___healingSpot_6; }
	inline void set_healingSpot_6(Transform_t3600365921 * value)
	{
		___healingSpot_6 = value;
		Il2CppCodeGenWriteBarrier((&___healingSpot_6), value);
	}

	inline static int32_t get_offset_of_bullet_7() { return static_cast<int32_t>(offsetof(BossTankController_t1902331492, ___bullet_7)); }
	inline GameObject_t1113636619 * get_bullet_7() const { return ___bullet_7; }
	inline GameObject_t1113636619 ** get_address_of_bullet_7() { return &___bullet_7; }
	inline void set_bullet_7(GameObject_t1113636619 * value)
	{
		___bullet_7 = value;
		Il2CppCodeGenWriteBarrier((&___bullet_7), value);
	}

	inline static int32_t get_offset_of_laser_8() { return static_cast<int32_t>(offsetof(BossTankController_t1902331492, ___laser_8)); }
	inline GameObject_t1113636619 * get_laser_8() const { return ___laser_8; }
	inline GameObject_t1113636619 ** get_address_of_laser_8() { return &___laser_8; }
	inline void set_laser_8(GameObject_t1113636619 * value)
	{
		___laser_8 = value;
		Il2CppCodeGenWriteBarrier((&___laser_8), value);
	}

	inline static int32_t get_offset_of_centerPoint_9() { return static_cast<int32_t>(offsetof(BossTankController_t1902331492, ___centerPoint_9)); }
	inline GameObject_t1113636619 * get_centerPoint_9() const { return ___centerPoint_9; }
	inline GameObject_t1113636619 ** get_address_of_centerPoint_9() { return &___centerPoint_9; }
	inline void set_centerPoint_9(GameObject_t1113636619 * value)
	{
		___centerPoint_9 = value;
		Il2CppCodeGenWriteBarrier((&___centerPoint_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOSSTANKCONTROLLER_T1902331492_H
#ifndef DAMAGEPLAYER_T2033828012_H
#define DAMAGEPLAYER_T2033828012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DamagePlayer
struct  DamagePlayer_t2033828012  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DamagePlayer::explosionSmall
	GameObject_t1113636619 * ___explosionSmall_2;
	// UnityEngine.GameObject DamagePlayer::explosionBig
	GameObject_t1113636619 * ___explosionBig_3;
	// System.Int32 DamagePlayer::damageValue
	int32_t ___damageValue_4;
	// HealthUI DamagePlayer::healthUI
	HealthUI_t1908446248 * ___healthUI_5;

public:
	inline static int32_t get_offset_of_explosionSmall_2() { return static_cast<int32_t>(offsetof(DamagePlayer_t2033828012, ___explosionSmall_2)); }
	inline GameObject_t1113636619 * get_explosionSmall_2() const { return ___explosionSmall_2; }
	inline GameObject_t1113636619 ** get_address_of_explosionSmall_2() { return &___explosionSmall_2; }
	inline void set_explosionSmall_2(GameObject_t1113636619 * value)
	{
		___explosionSmall_2 = value;
		Il2CppCodeGenWriteBarrier((&___explosionSmall_2), value);
	}

	inline static int32_t get_offset_of_explosionBig_3() { return static_cast<int32_t>(offsetof(DamagePlayer_t2033828012, ___explosionBig_3)); }
	inline GameObject_t1113636619 * get_explosionBig_3() const { return ___explosionBig_3; }
	inline GameObject_t1113636619 ** get_address_of_explosionBig_3() { return &___explosionBig_3; }
	inline void set_explosionBig_3(GameObject_t1113636619 * value)
	{
		___explosionBig_3 = value;
		Il2CppCodeGenWriteBarrier((&___explosionBig_3), value);
	}

	inline static int32_t get_offset_of_damageValue_4() { return static_cast<int32_t>(offsetof(DamagePlayer_t2033828012, ___damageValue_4)); }
	inline int32_t get_damageValue_4() const { return ___damageValue_4; }
	inline int32_t* get_address_of_damageValue_4() { return &___damageValue_4; }
	inline void set_damageValue_4(int32_t value)
	{
		___damageValue_4 = value;
	}

	inline static int32_t get_offset_of_healthUI_5() { return static_cast<int32_t>(offsetof(DamagePlayer_t2033828012, ___healthUI_5)); }
	inline HealthUI_t1908446248 * get_healthUI_5() const { return ___healthUI_5; }
	inline HealthUI_t1908446248 ** get_address_of_healthUI_5() { return &___healthUI_5; }
	inline void set_healthUI_5(HealthUI_t1908446248 * value)
	{
		___healthUI_5 = value;
		Il2CppCodeGenWriteBarrier((&___healthUI_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAMAGEPLAYER_T2033828012_H
#ifndef SHIELD_T1860136854_H
#define SHIELD_T1860136854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shield
struct  Shield_t1860136854  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 Shield::uses
	int32_t ___uses_2;

public:
	inline static int32_t get_offset_of_uses_2() { return static_cast<int32_t>(offsetof(Shield_t1860136854, ___uses_2)); }
	inline int32_t get_uses_2() const { return ___uses_2; }
	inline int32_t* get_address_of_uses_2() { return &___uses_2; }
	inline void set_uses_2(int32_t value)
	{
		___uses_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIELD_T1860136854_H
#ifndef DAMAGEENEMY_T2198349935_H
#define DAMAGEENEMY_T2198349935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DamageEnemy
struct  DamageEnemy_t2198349935  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject DamageEnemy::explosionSmall
	GameObject_t1113636619 * ___explosionSmall_2;
	// UnityEngine.GameObject DamageEnemy::explosionBig
	GameObject_t1113636619 * ___explosionBig_3;
	// System.Int32 DamageEnemy::damageValue
	int32_t ___damageValue_4;
	// HealthUI DamageEnemy::healthUI
	HealthUI_t1908446248 * ___healthUI_5;

public:
	inline static int32_t get_offset_of_explosionSmall_2() { return static_cast<int32_t>(offsetof(DamageEnemy_t2198349935, ___explosionSmall_2)); }
	inline GameObject_t1113636619 * get_explosionSmall_2() const { return ___explosionSmall_2; }
	inline GameObject_t1113636619 ** get_address_of_explosionSmall_2() { return &___explosionSmall_2; }
	inline void set_explosionSmall_2(GameObject_t1113636619 * value)
	{
		___explosionSmall_2 = value;
		Il2CppCodeGenWriteBarrier((&___explosionSmall_2), value);
	}

	inline static int32_t get_offset_of_explosionBig_3() { return static_cast<int32_t>(offsetof(DamageEnemy_t2198349935, ___explosionBig_3)); }
	inline GameObject_t1113636619 * get_explosionBig_3() const { return ___explosionBig_3; }
	inline GameObject_t1113636619 ** get_address_of_explosionBig_3() { return &___explosionBig_3; }
	inline void set_explosionBig_3(GameObject_t1113636619 * value)
	{
		___explosionBig_3 = value;
		Il2CppCodeGenWriteBarrier((&___explosionBig_3), value);
	}

	inline static int32_t get_offset_of_damageValue_4() { return static_cast<int32_t>(offsetof(DamageEnemy_t2198349935, ___damageValue_4)); }
	inline int32_t get_damageValue_4() const { return ___damageValue_4; }
	inline int32_t* get_address_of_damageValue_4() { return &___damageValue_4; }
	inline void set_damageValue_4(int32_t value)
	{
		___damageValue_4 = value;
	}

	inline static int32_t get_offset_of_healthUI_5() { return static_cast<int32_t>(offsetof(DamageEnemy_t2198349935, ___healthUI_5)); }
	inline HealthUI_t1908446248 * get_healthUI_5() const { return ___healthUI_5; }
	inline HealthUI_t1908446248 ** get_address_of_healthUI_5() { return &___healthUI_5; }
	inline void set_healthUI_5(HealthUI_t1908446248 * value)
	{
		___healthUI_5 = value;
		Il2CppCodeGenWriteBarrier((&___healthUI_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAMAGEENEMY_T2198349935_H
#ifndef INVENTORY_T1050226016_H
#define INVENTORY_T1050226016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Inventory
struct  Inventory_t1050226016  : public MonoBehaviour_t3962482529
{
public:
	// Inventory/OnItemChanged Inventory::onItemChangedCallback
	OnItemChanged_t22848112 * ___onItemChangedCallback_3;
	// System.Int32 Inventory::weaponSpace
	int32_t ___weaponSpace_4;
	// System.Int32 Inventory::shieldSpace
	int32_t ___shieldSpace_5;
	// Item Inventory::weapon
	Item_t2953980098 * ___weapon_6;
	// Item Inventory::shield
	Item_t2953980098 * ___shield_7;

public:
	inline static int32_t get_offset_of_onItemChangedCallback_3() { return static_cast<int32_t>(offsetof(Inventory_t1050226016, ___onItemChangedCallback_3)); }
	inline OnItemChanged_t22848112 * get_onItemChangedCallback_3() const { return ___onItemChangedCallback_3; }
	inline OnItemChanged_t22848112 ** get_address_of_onItemChangedCallback_3() { return &___onItemChangedCallback_3; }
	inline void set_onItemChangedCallback_3(OnItemChanged_t22848112 * value)
	{
		___onItemChangedCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___onItemChangedCallback_3), value);
	}

	inline static int32_t get_offset_of_weaponSpace_4() { return static_cast<int32_t>(offsetof(Inventory_t1050226016, ___weaponSpace_4)); }
	inline int32_t get_weaponSpace_4() const { return ___weaponSpace_4; }
	inline int32_t* get_address_of_weaponSpace_4() { return &___weaponSpace_4; }
	inline void set_weaponSpace_4(int32_t value)
	{
		___weaponSpace_4 = value;
	}

	inline static int32_t get_offset_of_shieldSpace_5() { return static_cast<int32_t>(offsetof(Inventory_t1050226016, ___shieldSpace_5)); }
	inline int32_t get_shieldSpace_5() const { return ___shieldSpace_5; }
	inline int32_t* get_address_of_shieldSpace_5() { return &___shieldSpace_5; }
	inline void set_shieldSpace_5(int32_t value)
	{
		___shieldSpace_5 = value;
	}

	inline static int32_t get_offset_of_weapon_6() { return static_cast<int32_t>(offsetof(Inventory_t1050226016, ___weapon_6)); }
	inline Item_t2953980098 * get_weapon_6() const { return ___weapon_6; }
	inline Item_t2953980098 ** get_address_of_weapon_6() { return &___weapon_6; }
	inline void set_weapon_6(Item_t2953980098 * value)
	{
		___weapon_6 = value;
		Il2CppCodeGenWriteBarrier((&___weapon_6), value);
	}

	inline static int32_t get_offset_of_shield_7() { return static_cast<int32_t>(offsetof(Inventory_t1050226016, ___shield_7)); }
	inline Item_t2953980098 * get_shield_7() const { return ___shield_7; }
	inline Item_t2953980098 ** get_address_of_shield_7() { return &___shield_7; }
	inline void set_shield_7(Item_t2953980098 * value)
	{
		___shield_7 = value;
		Il2CppCodeGenWriteBarrier((&___shield_7), value);
	}
};

struct Inventory_t1050226016_StaticFields
{
public:
	// Inventory Inventory::instance
	Inventory_t1050226016 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Inventory_t1050226016_StaticFields, ___instance_2)); }
	inline Inventory_t1050226016 * get_instance_2() const { return ___instance_2; }
	inline Inventory_t1050226016 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(Inventory_t1050226016 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORY_T1050226016_H
#ifndef SELECTTITLEOPTIONS_T2341392585_H
#define SELECTTITLEOPTIONS_T2341392585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectTitleOptions
struct  SelectTitleOptions_t2341392585  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 SelectTitleOptions::index
	int32_t ___index_2;
	// UnityEngine.AudioClip SelectTitleOptions::selectSound
	AudioClip_t3680889665 * ___selectSound_3;
	// UnityEngine.AudioClip SelectTitleOptions::confirmSound
	AudioClip_t3680889665 * ___confirmSound_4;
	// System.String SelectTitleOptions::newGameSceneName
	String_t* ___newGameSceneName_5;
	// UnityEngine.GameObject[] SelectTitleOptions::options
	GameObjectU5BU5D_t3328599146* ___options_6;
	// UnityEngine.Sprite[] SelectTitleOptions::titleOptionBox
	SpriteU5BU5D_t2581906349* ___titleOptionBox_7;
	// LevelChanger SelectTitleOptions::levelChanger
	LevelChanger_t225386971 * ___levelChanger_8;
	// System.Boolean SelectTitleOptions::leftAxesInUse
	bool ___leftAxesInUse_9;
	// System.Boolean SelectTitleOptions::rightAxesInUse
	bool ___rightAxesInUse_10;
	// System.Boolean SelectTitleOptions::confirmAxesInUse
	bool ___confirmAxesInUse_11;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_selectSound_3() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___selectSound_3)); }
	inline AudioClip_t3680889665 * get_selectSound_3() const { return ___selectSound_3; }
	inline AudioClip_t3680889665 ** get_address_of_selectSound_3() { return &___selectSound_3; }
	inline void set_selectSound_3(AudioClip_t3680889665 * value)
	{
		___selectSound_3 = value;
		Il2CppCodeGenWriteBarrier((&___selectSound_3), value);
	}

	inline static int32_t get_offset_of_confirmSound_4() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___confirmSound_4)); }
	inline AudioClip_t3680889665 * get_confirmSound_4() const { return ___confirmSound_4; }
	inline AudioClip_t3680889665 ** get_address_of_confirmSound_4() { return &___confirmSound_4; }
	inline void set_confirmSound_4(AudioClip_t3680889665 * value)
	{
		___confirmSound_4 = value;
		Il2CppCodeGenWriteBarrier((&___confirmSound_4), value);
	}

	inline static int32_t get_offset_of_newGameSceneName_5() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___newGameSceneName_5)); }
	inline String_t* get_newGameSceneName_5() const { return ___newGameSceneName_5; }
	inline String_t** get_address_of_newGameSceneName_5() { return &___newGameSceneName_5; }
	inline void set_newGameSceneName_5(String_t* value)
	{
		___newGameSceneName_5 = value;
		Il2CppCodeGenWriteBarrier((&___newGameSceneName_5), value);
	}

	inline static int32_t get_offset_of_options_6() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___options_6)); }
	inline GameObjectU5BU5D_t3328599146* get_options_6() const { return ___options_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_options_6() { return &___options_6; }
	inline void set_options_6(GameObjectU5BU5D_t3328599146* value)
	{
		___options_6 = value;
		Il2CppCodeGenWriteBarrier((&___options_6), value);
	}

	inline static int32_t get_offset_of_titleOptionBox_7() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___titleOptionBox_7)); }
	inline SpriteU5BU5D_t2581906349* get_titleOptionBox_7() const { return ___titleOptionBox_7; }
	inline SpriteU5BU5D_t2581906349** get_address_of_titleOptionBox_7() { return &___titleOptionBox_7; }
	inline void set_titleOptionBox_7(SpriteU5BU5D_t2581906349* value)
	{
		___titleOptionBox_7 = value;
		Il2CppCodeGenWriteBarrier((&___titleOptionBox_7), value);
	}

	inline static int32_t get_offset_of_levelChanger_8() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___levelChanger_8)); }
	inline LevelChanger_t225386971 * get_levelChanger_8() const { return ___levelChanger_8; }
	inline LevelChanger_t225386971 ** get_address_of_levelChanger_8() { return &___levelChanger_8; }
	inline void set_levelChanger_8(LevelChanger_t225386971 * value)
	{
		___levelChanger_8 = value;
		Il2CppCodeGenWriteBarrier((&___levelChanger_8), value);
	}

	inline static int32_t get_offset_of_leftAxesInUse_9() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___leftAxesInUse_9)); }
	inline bool get_leftAxesInUse_9() const { return ___leftAxesInUse_9; }
	inline bool* get_address_of_leftAxesInUse_9() { return &___leftAxesInUse_9; }
	inline void set_leftAxesInUse_9(bool value)
	{
		___leftAxesInUse_9 = value;
	}

	inline static int32_t get_offset_of_rightAxesInUse_10() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___rightAxesInUse_10)); }
	inline bool get_rightAxesInUse_10() const { return ___rightAxesInUse_10; }
	inline bool* get_address_of_rightAxesInUse_10() { return &___rightAxesInUse_10; }
	inline void set_rightAxesInUse_10(bool value)
	{
		___rightAxesInUse_10 = value;
	}

	inline static int32_t get_offset_of_confirmAxesInUse_11() { return static_cast<int32_t>(offsetof(SelectTitleOptions_t2341392585, ___confirmAxesInUse_11)); }
	inline bool get_confirmAxesInUse_11() const { return ___confirmAxesInUse_11; }
	inline bool* get_address_of_confirmAxesInUse_11() { return &___confirmAxesInUse_11; }
	inline void set_confirmAxesInUse_11(bool value)
	{
		___confirmAxesInUse_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTTITLEOPTIONS_T2341392585_H
#ifndef ALPHABUTTONCLICKMASK_T141136539_H
#define ALPHABUTTONCLICKMASK_T141136539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AlphaButtonClickMask
struct  AlphaButtonClickMask_t141136539  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image AlphaButtonClickMask::_image
	Image_t2670269651 * ____image_2;

public:
	inline static int32_t get_offset_of__image_2() { return static_cast<int32_t>(offsetof(AlphaButtonClickMask_t141136539, ____image_2)); }
	inline Image_t2670269651 * get__image_2() const { return ____image_2; }
	inline Image_t2670269651 ** get_address_of__image_2() { return &____image_2; }
	inline void set__image_2(Image_t2670269651 * value)
	{
		____image_2 = value;
		Il2CppCodeGenWriteBarrier((&____image_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALPHABUTTONCLICKMASK_T141136539_H
#ifndef ROTATINGLIGHTS_T2402942693_H
#define ROTATINGLIGHTS_T2402942693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotatingLights
struct  RotatingLights_t2402942693  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject RotatingLights::lights1
	GameObject_t1113636619 * ___lights1_2;
	// UnityEngine.GameObject RotatingLights::lights2
	GameObject_t1113636619 * ___lights2_3;
	// System.Single RotatingLights::rotationSpeed
	float ___rotationSpeed_4;

public:
	inline static int32_t get_offset_of_lights1_2() { return static_cast<int32_t>(offsetof(RotatingLights_t2402942693, ___lights1_2)); }
	inline GameObject_t1113636619 * get_lights1_2() const { return ___lights1_2; }
	inline GameObject_t1113636619 ** get_address_of_lights1_2() { return &___lights1_2; }
	inline void set_lights1_2(GameObject_t1113636619 * value)
	{
		___lights1_2 = value;
		Il2CppCodeGenWriteBarrier((&___lights1_2), value);
	}

	inline static int32_t get_offset_of_lights2_3() { return static_cast<int32_t>(offsetof(RotatingLights_t2402942693, ___lights2_3)); }
	inline GameObject_t1113636619 * get_lights2_3() const { return ___lights2_3; }
	inline GameObject_t1113636619 ** get_address_of_lights2_3() { return &___lights2_3; }
	inline void set_lights2_3(GameObject_t1113636619 * value)
	{
		___lights2_3 = value;
		Il2CppCodeGenWriteBarrier((&___lights2_3), value);
	}

	inline static int32_t get_offset_of_rotationSpeed_4() { return static_cast<int32_t>(offsetof(RotatingLights_t2402942693, ___rotationSpeed_4)); }
	inline float get_rotationSpeed_4() const { return ___rotationSpeed_4; }
	inline float* get_address_of_rotationSpeed_4() { return &___rotationSpeed_4; }
	inline void set_rotationSpeed_4(float value)
	{
		___rotationSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATINGLIGHTS_T2402942693_H
#ifndef UNIT_T4139495810_H
#define UNIT_T4139495810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unit
struct  Unit_t4139495810  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform Unit::target
	Transform_t3600365921 * ___target_4;
	// System.Single Unit::speed
	float ___speed_5;
	// System.Single Unit::turnSpeed
	float ___turnSpeed_6;
	// System.Single Unit::turnDst
	float ___turnDst_7;
	// System.Single Unit::stoppingDst
	float ___stoppingDst_8;
	// Path Unit::path
	Path_t2615110272 * ___path_9;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(Unit_t4139495810, ___target_4)); }
	inline Transform_t3600365921 * get_target_4() const { return ___target_4; }
	inline Transform_t3600365921 ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_t3600365921 * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(Unit_t4139495810, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_turnSpeed_6() { return static_cast<int32_t>(offsetof(Unit_t4139495810, ___turnSpeed_6)); }
	inline float get_turnSpeed_6() const { return ___turnSpeed_6; }
	inline float* get_address_of_turnSpeed_6() { return &___turnSpeed_6; }
	inline void set_turnSpeed_6(float value)
	{
		___turnSpeed_6 = value;
	}

	inline static int32_t get_offset_of_turnDst_7() { return static_cast<int32_t>(offsetof(Unit_t4139495810, ___turnDst_7)); }
	inline float get_turnDst_7() const { return ___turnDst_7; }
	inline float* get_address_of_turnDst_7() { return &___turnDst_7; }
	inline void set_turnDst_7(float value)
	{
		___turnDst_7 = value;
	}

	inline static int32_t get_offset_of_stoppingDst_8() { return static_cast<int32_t>(offsetof(Unit_t4139495810, ___stoppingDst_8)); }
	inline float get_stoppingDst_8() const { return ___stoppingDst_8; }
	inline float* get_address_of_stoppingDst_8() { return &___stoppingDst_8; }
	inline void set_stoppingDst_8(float value)
	{
		___stoppingDst_8 = value;
	}

	inline static int32_t get_offset_of_path_9() { return static_cast<int32_t>(offsetof(Unit_t4139495810, ___path_9)); }
	inline Path_t2615110272 * get_path_9() const { return ___path_9; }
	inline Path_t2615110272 ** get_address_of_path_9() { return &___path_9; }
	inline void set_path_9(Path_t2615110272 * value)
	{
		___path_9 = value;
		Il2CppCodeGenWriteBarrier((&___path_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIT_T4139495810_H
#ifndef PAUSEMENU_T3916167947_H
#define PAUSEMENU_T3916167947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PauseMenu
struct  PauseMenu_t3916167947  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject PauseMenu::pauseMenuUI
	GameObject_t1113636619 * ___pauseMenuUI_4;
	// System.Int32 PauseMenu::index
	int32_t ___index_5;
	// UnityEngine.AudioClip PauseMenu::selectSound
	AudioClip_t3680889665 * ___selectSound_6;
	// UnityEngine.AudioClip PauseMenu::confirmSound
	AudioClip_t3680889665 * ___confirmSound_7;
	// UnityEngine.GameObject[] PauseMenu::options
	GameObjectU5BU5D_t3328599146* ___options_8;
	// UnityEngine.Sprite[] PauseMenu::optionBox
	SpriteU5BU5D_t2581906349* ___optionBox_9;
	// System.Boolean PauseMenu::upAxesInUse
	bool ___upAxesInUse_10;
	// System.Boolean PauseMenu::downAxesInUse
	bool ___downAxesInUse_11;
	// System.Boolean PauseMenu::cancelAxesInUse
	bool ___cancelAxesInUse_12;

public:
	inline static int32_t get_offset_of_pauseMenuUI_4() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___pauseMenuUI_4)); }
	inline GameObject_t1113636619 * get_pauseMenuUI_4() const { return ___pauseMenuUI_4; }
	inline GameObject_t1113636619 ** get_address_of_pauseMenuUI_4() { return &___pauseMenuUI_4; }
	inline void set_pauseMenuUI_4(GameObject_t1113636619 * value)
	{
		___pauseMenuUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___pauseMenuUI_4), value);
	}

	inline static int32_t get_offset_of_index_5() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___index_5)); }
	inline int32_t get_index_5() const { return ___index_5; }
	inline int32_t* get_address_of_index_5() { return &___index_5; }
	inline void set_index_5(int32_t value)
	{
		___index_5 = value;
	}

	inline static int32_t get_offset_of_selectSound_6() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___selectSound_6)); }
	inline AudioClip_t3680889665 * get_selectSound_6() const { return ___selectSound_6; }
	inline AudioClip_t3680889665 ** get_address_of_selectSound_6() { return &___selectSound_6; }
	inline void set_selectSound_6(AudioClip_t3680889665 * value)
	{
		___selectSound_6 = value;
		Il2CppCodeGenWriteBarrier((&___selectSound_6), value);
	}

	inline static int32_t get_offset_of_confirmSound_7() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___confirmSound_7)); }
	inline AudioClip_t3680889665 * get_confirmSound_7() const { return ___confirmSound_7; }
	inline AudioClip_t3680889665 ** get_address_of_confirmSound_7() { return &___confirmSound_7; }
	inline void set_confirmSound_7(AudioClip_t3680889665 * value)
	{
		___confirmSound_7 = value;
		Il2CppCodeGenWriteBarrier((&___confirmSound_7), value);
	}

	inline static int32_t get_offset_of_options_8() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___options_8)); }
	inline GameObjectU5BU5D_t3328599146* get_options_8() const { return ___options_8; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_options_8() { return &___options_8; }
	inline void set_options_8(GameObjectU5BU5D_t3328599146* value)
	{
		___options_8 = value;
		Il2CppCodeGenWriteBarrier((&___options_8), value);
	}

	inline static int32_t get_offset_of_optionBox_9() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___optionBox_9)); }
	inline SpriteU5BU5D_t2581906349* get_optionBox_9() const { return ___optionBox_9; }
	inline SpriteU5BU5D_t2581906349** get_address_of_optionBox_9() { return &___optionBox_9; }
	inline void set_optionBox_9(SpriteU5BU5D_t2581906349* value)
	{
		___optionBox_9 = value;
		Il2CppCodeGenWriteBarrier((&___optionBox_9), value);
	}

	inline static int32_t get_offset_of_upAxesInUse_10() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___upAxesInUse_10)); }
	inline bool get_upAxesInUse_10() const { return ___upAxesInUse_10; }
	inline bool* get_address_of_upAxesInUse_10() { return &___upAxesInUse_10; }
	inline void set_upAxesInUse_10(bool value)
	{
		___upAxesInUse_10 = value;
	}

	inline static int32_t get_offset_of_downAxesInUse_11() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___downAxesInUse_11)); }
	inline bool get_downAxesInUse_11() const { return ___downAxesInUse_11; }
	inline bool* get_address_of_downAxesInUse_11() { return &___downAxesInUse_11; }
	inline void set_downAxesInUse_11(bool value)
	{
		___downAxesInUse_11 = value;
	}

	inline static int32_t get_offset_of_cancelAxesInUse_12() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947, ___cancelAxesInUse_12)); }
	inline bool get_cancelAxesInUse_12() const { return ___cancelAxesInUse_12; }
	inline bool* get_address_of_cancelAxesInUse_12() { return &___cancelAxesInUse_12; }
	inline void set_cancelAxesInUse_12(bool value)
	{
		___cancelAxesInUse_12 = value;
	}
};

struct PauseMenu_t3916167947_StaticFields
{
public:
	// System.Boolean PauseMenu::gameIsPaused
	bool ___gameIsPaused_2;
	// System.Boolean PauseMenu::pausingEnabled
	bool ___pausingEnabled_3;

public:
	inline static int32_t get_offset_of_gameIsPaused_2() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947_StaticFields, ___gameIsPaused_2)); }
	inline bool get_gameIsPaused_2() const { return ___gameIsPaused_2; }
	inline bool* get_address_of_gameIsPaused_2() { return &___gameIsPaused_2; }
	inline void set_gameIsPaused_2(bool value)
	{
		___gameIsPaused_2 = value;
	}

	inline static int32_t get_offset_of_pausingEnabled_3() { return static_cast<int32_t>(offsetof(PauseMenu_t3916167947_StaticFields, ___pausingEnabled_3)); }
	inline bool get_pausingEnabled_3() const { return ___pausingEnabled_3; }
	inline bool* get_address_of_pausingEnabled_3() { return &___pausingEnabled_3; }
	inline void set_pausingEnabled_3(bool value)
	{
		___pausingEnabled_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSEMENU_T3916167947_H
#ifndef EVENTSYSTEMCHECKER_T1882757729_H
#define EVENTSYSTEMCHECKER_T1882757729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventSystemChecker
struct  EventSystemChecker_t1882757729  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEMCHECKER_T1882757729_H
#ifndef AUTOMOVEANDROTATE_T2437913015_H
#define AUTOMOVEANDROTATE_T2437913015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMoveAndRotate
struct  AutoMoveAndRotate_t2437913015  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace UnityStandardAssets.Utility.AutoMoveAndRotate::moveUnitsPerSecond
	Vector3andSpace_t219844479 * ___moveUnitsPerSecond_2;
	// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace UnityStandardAssets.Utility.AutoMoveAndRotate::rotateDegreesPerSecond
	Vector3andSpace_t219844479 * ___rotateDegreesPerSecond_3;
	// System.Boolean UnityStandardAssets.Utility.AutoMoveAndRotate::ignoreTimescale
	bool ___ignoreTimescale_4;
	// System.Single UnityStandardAssets.Utility.AutoMoveAndRotate::m_LastRealTime
	float ___m_LastRealTime_5;

public:
	inline static int32_t get_offset_of_moveUnitsPerSecond_2() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___moveUnitsPerSecond_2)); }
	inline Vector3andSpace_t219844479 * get_moveUnitsPerSecond_2() const { return ___moveUnitsPerSecond_2; }
	inline Vector3andSpace_t219844479 ** get_address_of_moveUnitsPerSecond_2() { return &___moveUnitsPerSecond_2; }
	inline void set_moveUnitsPerSecond_2(Vector3andSpace_t219844479 * value)
	{
		___moveUnitsPerSecond_2 = value;
		Il2CppCodeGenWriteBarrier((&___moveUnitsPerSecond_2), value);
	}

	inline static int32_t get_offset_of_rotateDegreesPerSecond_3() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___rotateDegreesPerSecond_3)); }
	inline Vector3andSpace_t219844479 * get_rotateDegreesPerSecond_3() const { return ___rotateDegreesPerSecond_3; }
	inline Vector3andSpace_t219844479 ** get_address_of_rotateDegreesPerSecond_3() { return &___rotateDegreesPerSecond_3; }
	inline void set_rotateDegreesPerSecond_3(Vector3andSpace_t219844479 * value)
	{
		___rotateDegreesPerSecond_3 = value;
		Il2CppCodeGenWriteBarrier((&___rotateDegreesPerSecond_3), value);
	}

	inline static int32_t get_offset_of_ignoreTimescale_4() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___ignoreTimescale_4)); }
	inline bool get_ignoreTimescale_4() const { return ___ignoreTimescale_4; }
	inline bool* get_address_of_ignoreTimescale_4() { return &___ignoreTimescale_4; }
	inline void set_ignoreTimescale_4(bool value)
	{
		___ignoreTimescale_4 = value;
	}

	inline static int32_t get_offset_of_m_LastRealTime_5() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t2437913015, ___m_LastRealTime_5)); }
	inline float get_m_LastRealTime_5() const { return ___m_LastRealTime_5; }
	inline float* get_address_of_m_LastRealTime_5() { return &___m_LastRealTime_5; }
	inline void set_m_LastRealTime_5(float value)
	{
		___m_LastRealTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOVEANDROTATE_T2437913015_H
#ifndef DRAGRIGIDBODY_T1600652016_H
#define DRAGRIGIDBODY_T1600652016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DragRigidbody
struct  DragRigidbody_t1600652016  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.SpringJoint UnityStandardAssets.Utility.DragRigidbody::m_SpringJoint
	SpringJoint_t1912369980 * ___m_SpringJoint_8;

public:
	inline static int32_t get_offset_of_m_SpringJoint_8() { return static_cast<int32_t>(offsetof(DragRigidbody_t1600652016, ___m_SpringJoint_8)); }
	inline SpringJoint_t1912369980 * get_m_SpringJoint_8() const { return ___m_SpringJoint_8; }
	inline SpringJoint_t1912369980 ** get_address_of_m_SpringJoint_8() { return &___m_SpringJoint_8; }
	inline void set_m_SpringJoint_8(SpringJoint_t1912369980 * value)
	{
		___m_SpringJoint_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpringJoint_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGRIGIDBODY_T1600652016_H
#ifndef AUTOMOBILESHADERSWITCH_T568447889_H
#define AUTOMOBILESHADERSWITCH_T568447889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch
struct  AutoMobileShaderSwitch_t568447889  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList UnityStandardAssets.Utility.AutoMobileShaderSwitch::m_ReplacementList
	ReplacementList_t1887104210 * ___m_ReplacementList_2;

public:
	inline static int32_t get_offset_of_m_ReplacementList_2() { return static_cast<int32_t>(offsetof(AutoMobileShaderSwitch_t568447889, ___m_ReplacementList_2)); }
	inline ReplacementList_t1887104210 * get_m_ReplacementList_2() const { return ___m_ReplacementList_2; }
	inline ReplacementList_t1887104210 ** get_address_of_m_ReplacementList_2() { return &___m_ReplacementList_2; }
	inline void set_m_ReplacementList_2(ReplacementList_t1887104210 * value)
	{
		___m_ReplacementList_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReplacementList_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOBILESHADERSWITCH_T568447889_H
#ifndef FORCEDRESET_T301124368_H
#define FORCEDRESET_T301124368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ForcedReset
struct  ForcedReset_t301124368  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEDRESET_T301124368_H
#ifndef ACTIVATETRIGGER_T3349759092_H
#define ACTIVATETRIGGER_T3349759092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ActivateTrigger
struct  ActivateTrigger_t3349759092  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.Utility.ActivateTrigger/Mode UnityStandardAssets.Utility.ActivateTrigger::action
	int32_t ___action_2;
	// UnityEngine.Object UnityStandardAssets.Utility.ActivateTrigger::target
	Object_t631007953 * ___target_3;
	// UnityEngine.GameObject UnityStandardAssets.Utility.ActivateTrigger::source
	GameObject_t1113636619 * ___source_4;
	// System.Int32 UnityStandardAssets.Utility.ActivateTrigger::triggerCount
	int32_t ___triggerCount_5;
	// System.Boolean UnityStandardAssets.Utility.ActivateTrigger::repeatTrigger
	bool ___repeatTrigger_6;

public:
	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___action_2)); }
	inline int32_t get_action_2() const { return ___action_2; }
	inline int32_t* get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(int32_t value)
	{
		___action_2 = value;
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___target_3)); }
	inline Object_t631007953 * get_target_3() const { return ___target_3; }
	inline Object_t631007953 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Object_t631007953 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___source_4)); }
	inline GameObject_t1113636619 * get_source_4() const { return ___source_4; }
	inline GameObject_t1113636619 ** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(GameObject_t1113636619 * value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier((&___source_4), value);
	}

	inline static int32_t get_offset_of_triggerCount_5() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___triggerCount_5)); }
	inline int32_t get_triggerCount_5() const { return ___triggerCount_5; }
	inline int32_t* get_address_of_triggerCount_5() { return &___triggerCount_5; }
	inline void set_triggerCount_5(int32_t value)
	{
		___triggerCount_5 = value;
	}

	inline static int32_t get_offset_of_repeatTrigger_6() { return static_cast<int32_t>(offsetof(ActivateTrigger_t3349759092, ___repeatTrigger_6)); }
	inline bool get_repeatTrigger_6() const { return ___repeatTrigger_6; }
	inline bool* get_address_of_repeatTrigger_6() { return &___repeatTrigger_6; }
	inline void set_repeatTrigger_6(bool value)
	{
		___repeatTrigger_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATETRIGGER_T3349759092_H
#ifndef LEVELCLEAR_T1949285084_H
#define LEVELCLEAR_T1949285084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelClear
struct  LevelClear_t1949285084  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator LevelClear::animator
	Animator_t434523843 * ___animator_2;
	// LevelChanger LevelClear::levelChanger
	LevelChanger_t225386971 * ___levelChanger_3;
	// System.String LevelClear::levelNameToLoad
	String_t* ___levelNameToLoad_4;
	// System.Int32 LevelClear::levelIndexToLoad
	int32_t ___levelIndexToLoad_5;
	// System.Boolean LevelClear::loadByName
	bool ___loadByName_6;

public:
	inline static int32_t get_offset_of_animator_2() { return static_cast<int32_t>(offsetof(LevelClear_t1949285084, ___animator_2)); }
	inline Animator_t434523843 * get_animator_2() const { return ___animator_2; }
	inline Animator_t434523843 ** get_address_of_animator_2() { return &___animator_2; }
	inline void set_animator_2(Animator_t434523843 * value)
	{
		___animator_2 = value;
		Il2CppCodeGenWriteBarrier((&___animator_2), value);
	}

	inline static int32_t get_offset_of_levelChanger_3() { return static_cast<int32_t>(offsetof(LevelClear_t1949285084, ___levelChanger_3)); }
	inline LevelChanger_t225386971 * get_levelChanger_3() const { return ___levelChanger_3; }
	inline LevelChanger_t225386971 ** get_address_of_levelChanger_3() { return &___levelChanger_3; }
	inline void set_levelChanger_3(LevelChanger_t225386971 * value)
	{
		___levelChanger_3 = value;
		Il2CppCodeGenWriteBarrier((&___levelChanger_3), value);
	}

	inline static int32_t get_offset_of_levelNameToLoad_4() { return static_cast<int32_t>(offsetof(LevelClear_t1949285084, ___levelNameToLoad_4)); }
	inline String_t* get_levelNameToLoad_4() const { return ___levelNameToLoad_4; }
	inline String_t** get_address_of_levelNameToLoad_4() { return &___levelNameToLoad_4; }
	inline void set_levelNameToLoad_4(String_t* value)
	{
		___levelNameToLoad_4 = value;
		Il2CppCodeGenWriteBarrier((&___levelNameToLoad_4), value);
	}

	inline static int32_t get_offset_of_levelIndexToLoad_5() { return static_cast<int32_t>(offsetof(LevelClear_t1949285084, ___levelIndexToLoad_5)); }
	inline int32_t get_levelIndexToLoad_5() const { return ___levelIndexToLoad_5; }
	inline int32_t* get_address_of_levelIndexToLoad_5() { return &___levelIndexToLoad_5; }
	inline void set_levelIndexToLoad_5(int32_t value)
	{
		___levelIndexToLoad_5 = value;
	}

	inline static int32_t get_offset_of_loadByName_6() { return static_cast<int32_t>(offsetof(LevelClear_t1949285084, ___loadByName_6)); }
	inline bool get_loadByName_6() const { return ___loadByName_6; }
	inline bool* get_address_of_loadByName_6() { return &___loadByName_6; }
	inline void set_loadByName_6(bool value)
	{
		___loadByName_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCLEAR_T1949285084_H
#ifndef BACKGROUNDSCROLLER_T2508866296_H
#define BACKGROUNDSCROLLER_T2508866296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackgroundScroller
struct  BackgroundScroller_t2508866296  : public MonoBehaviour_t3962482529
{
public:
	// System.Single BackgroundScroller::scrollSpeed
	float ___scrollSpeed_2;
	// System.Single BackgroundScroller::tileSizeY
	float ___tileSizeY_3;
	// UnityEngine.RectTransform BackgroundScroller::rect
	RectTransform_t3704657025 * ___rect_4;
	// UnityEngine.Vector3 BackgroundScroller::startPosition
	Vector3_t3722313464  ___startPosition_5;

public:
	inline static int32_t get_offset_of_scrollSpeed_2() { return static_cast<int32_t>(offsetof(BackgroundScroller_t2508866296, ___scrollSpeed_2)); }
	inline float get_scrollSpeed_2() const { return ___scrollSpeed_2; }
	inline float* get_address_of_scrollSpeed_2() { return &___scrollSpeed_2; }
	inline void set_scrollSpeed_2(float value)
	{
		___scrollSpeed_2 = value;
	}

	inline static int32_t get_offset_of_tileSizeY_3() { return static_cast<int32_t>(offsetof(BackgroundScroller_t2508866296, ___tileSizeY_3)); }
	inline float get_tileSizeY_3() const { return ___tileSizeY_3; }
	inline float* get_address_of_tileSizeY_3() { return &___tileSizeY_3; }
	inline void set_tileSizeY_3(float value)
	{
		___tileSizeY_3 = value;
	}

	inline static int32_t get_offset_of_rect_4() { return static_cast<int32_t>(offsetof(BackgroundScroller_t2508866296, ___rect_4)); }
	inline RectTransform_t3704657025 * get_rect_4() const { return ___rect_4; }
	inline RectTransform_t3704657025 ** get_address_of_rect_4() { return &___rect_4; }
	inline void set_rect_4(RectTransform_t3704657025 * value)
	{
		___rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___rect_4), value);
	}

	inline static int32_t get_offset_of_startPosition_5() { return static_cast<int32_t>(offsetof(BackgroundScroller_t2508866296, ___startPosition_5)); }
	inline Vector3_t3722313464  get_startPosition_5() const { return ___startPosition_5; }
	inline Vector3_t3722313464 * get_address_of_startPosition_5() { return &___startPosition_5; }
	inline void set_startPosition_5(Vector3_t3722313464  value)
	{
		___startPosition_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDSCROLLER_T2508866296_H
#ifndef LEVELCHANGER_T225386971_H
#define LEVELCHANGER_T225386971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelChanger
struct  LevelChanger_t225386971  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator LevelChanger::animator
	Animator_t434523843 * ___animator_2;
	// System.String LevelChanger::levelNameToLoad
	String_t* ___levelNameToLoad_3;
	// System.Int32 LevelChanger::levelIndexToLoad
	int32_t ___levelIndexToLoad_4;
	// System.Boolean LevelChanger::loadByName
	bool ___loadByName_5;

public:
	inline static int32_t get_offset_of_animator_2() { return static_cast<int32_t>(offsetof(LevelChanger_t225386971, ___animator_2)); }
	inline Animator_t434523843 * get_animator_2() const { return ___animator_2; }
	inline Animator_t434523843 ** get_address_of_animator_2() { return &___animator_2; }
	inline void set_animator_2(Animator_t434523843 * value)
	{
		___animator_2 = value;
		Il2CppCodeGenWriteBarrier((&___animator_2), value);
	}

	inline static int32_t get_offset_of_levelNameToLoad_3() { return static_cast<int32_t>(offsetof(LevelChanger_t225386971, ___levelNameToLoad_3)); }
	inline String_t* get_levelNameToLoad_3() const { return ___levelNameToLoad_3; }
	inline String_t** get_address_of_levelNameToLoad_3() { return &___levelNameToLoad_3; }
	inline void set_levelNameToLoad_3(String_t* value)
	{
		___levelNameToLoad_3 = value;
		Il2CppCodeGenWriteBarrier((&___levelNameToLoad_3), value);
	}

	inline static int32_t get_offset_of_levelIndexToLoad_4() { return static_cast<int32_t>(offsetof(LevelChanger_t225386971, ___levelIndexToLoad_4)); }
	inline int32_t get_levelIndexToLoad_4() const { return ___levelIndexToLoad_4; }
	inline int32_t* get_address_of_levelIndexToLoad_4() { return &___levelIndexToLoad_4; }
	inline void set_levelIndexToLoad_4(int32_t value)
	{
		___levelIndexToLoad_4 = value;
	}

	inline static int32_t get_offset_of_loadByName_5() { return static_cast<int32_t>(offsetof(LevelChanger_t225386971, ___loadByName_5)); }
	inline bool get_loadByName_5() const { return ___loadByName_5; }
	inline bool* get_address_of_loadByName_5() { return &___loadByName_5; }
	inline void set_loadByName_5(bool value)
	{
		___loadByName_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELCHANGER_T225386971_H
#ifndef INVENTORYSLOT_T3299309524_H
#define INVENTORYSLOT_T3299309524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventorySlot
struct  InventorySlot_t3299309524  : public MonoBehaviour_t3962482529
{
public:
	// Item InventorySlot::item
	Item_t2953980098 * ___item_2;
	// InventorySlot/SlotType InventorySlot::slotType
	int32_t ___slotType_3;
	// UnityEngine.UI.Image InventorySlot::icon
	Image_t2670269651 * ___icon_4;
	// UnityEngine.GameObject InventorySlot::button
	GameObject_t1113636619 * ___button_5;
	// UnityEngine.UI.Text InventorySlot::itemName
	Text_t1901882714 * ___itemName_6;

public:
	inline static int32_t get_offset_of_item_2() { return static_cast<int32_t>(offsetof(InventorySlot_t3299309524, ___item_2)); }
	inline Item_t2953980098 * get_item_2() const { return ___item_2; }
	inline Item_t2953980098 ** get_address_of_item_2() { return &___item_2; }
	inline void set_item_2(Item_t2953980098 * value)
	{
		___item_2 = value;
		Il2CppCodeGenWriteBarrier((&___item_2), value);
	}

	inline static int32_t get_offset_of_slotType_3() { return static_cast<int32_t>(offsetof(InventorySlot_t3299309524, ___slotType_3)); }
	inline int32_t get_slotType_3() const { return ___slotType_3; }
	inline int32_t* get_address_of_slotType_3() { return &___slotType_3; }
	inline void set_slotType_3(int32_t value)
	{
		___slotType_3 = value;
	}

	inline static int32_t get_offset_of_icon_4() { return static_cast<int32_t>(offsetof(InventorySlot_t3299309524, ___icon_4)); }
	inline Image_t2670269651 * get_icon_4() const { return ___icon_4; }
	inline Image_t2670269651 ** get_address_of_icon_4() { return &___icon_4; }
	inline void set_icon_4(Image_t2670269651 * value)
	{
		___icon_4 = value;
		Il2CppCodeGenWriteBarrier((&___icon_4), value);
	}

	inline static int32_t get_offset_of_button_5() { return static_cast<int32_t>(offsetof(InventorySlot_t3299309524, ___button_5)); }
	inline GameObject_t1113636619 * get_button_5() const { return ___button_5; }
	inline GameObject_t1113636619 ** get_address_of_button_5() { return &___button_5; }
	inline void set_button_5(GameObject_t1113636619 * value)
	{
		___button_5 = value;
		Il2CppCodeGenWriteBarrier((&___button_5), value);
	}

	inline static int32_t get_offset_of_itemName_6() { return static_cast<int32_t>(offsetof(InventorySlot_t3299309524, ___itemName_6)); }
	inline Text_t1901882714 * get_itemName_6() const { return ___itemName_6; }
	inline Text_t1901882714 ** get_address_of_itemName_6() { return &___itemName_6; }
	inline void set_itemName_6(Text_t1901882714 * value)
	{
		___itemName_6 = value;
		Il2CppCodeGenWriteBarrier((&___itemName_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORYSLOT_T3299309524_H
#ifndef INVENTORYUI_T1983315844_H
#define INVENTORYUI_T1983315844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventoryUI
struct  InventoryUI_t1983315844  : public MonoBehaviour_t3962482529
{
public:
	// Inventory InventoryUI::inventory
	Inventory_t1050226016 * ___inventory_2;
	// InventorySlot InventoryUI::weaponSlot
	InventorySlot_t3299309524 * ___weaponSlot_3;
	// InventorySlot InventoryUI::shieldSlot
	InventorySlot_t3299309524 * ___shieldSlot_4;

public:
	inline static int32_t get_offset_of_inventory_2() { return static_cast<int32_t>(offsetof(InventoryUI_t1983315844, ___inventory_2)); }
	inline Inventory_t1050226016 * get_inventory_2() const { return ___inventory_2; }
	inline Inventory_t1050226016 ** get_address_of_inventory_2() { return &___inventory_2; }
	inline void set_inventory_2(Inventory_t1050226016 * value)
	{
		___inventory_2 = value;
		Il2CppCodeGenWriteBarrier((&___inventory_2), value);
	}

	inline static int32_t get_offset_of_weaponSlot_3() { return static_cast<int32_t>(offsetof(InventoryUI_t1983315844, ___weaponSlot_3)); }
	inline InventorySlot_t3299309524 * get_weaponSlot_3() const { return ___weaponSlot_3; }
	inline InventorySlot_t3299309524 ** get_address_of_weaponSlot_3() { return &___weaponSlot_3; }
	inline void set_weaponSlot_3(InventorySlot_t3299309524 * value)
	{
		___weaponSlot_3 = value;
		Il2CppCodeGenWriteBarrier((&___weaponSlot_3), value);
	}

	inline static int32_t get_offset_of_shieldSlot_4() { return static_cast<int32_t>(offsetof(InventoryUI_t1983315844, ___shieldSlot_4)); }
	inline InventorySlot_t3299309524 * get_shieldSlot_4() const { return ___shieldSlot_4; }
	inline InventorySlot_t3299309524 ** get_address_of_shieldSlot_4() { return &___shieldSlot_4; }
	inline void set_shieldSlot_4(InventorySlot_t3299309524 * value)
	{
		___shieldSlot_4 = value;
		Il2CppCodeGenWriteBarrier((&___shieldSlot_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORYUI_T1983315844_H
#ifndef PATHFINDING_T1696161914_H
#define PATHFINDING_T1696161914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding
struct  Pathfinding_t1696161914  : public MonoBehaviour_t3962482529
{
public:
	// Grid Pathfinding::grid
	Grid_t1081586032 * ___grid_2;

public:
	inline static int32_t get_offset_of_grid_2() { return static_cast<int32_t>(offsetof(Pathfinding_t1696161914, ___grid_2)); }
	inline Grid_t1081586032 * get_grid_2() const { return ___grid_2; }
	inline Grid_t1081586032 ** get_address_of_grid_2() { return &___grid_2; }
	inline void set_grid_2(Grid_t1081586032 * value)
	{
		___grid_2 = value;
		Il2CppCodeGenWriteBarrier((&___grid_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHFINDING_T1696161914_H
#ifndef PATHREQUESTMANAGER_T4163282249_H
#define PATHREQUESTMANAGER_T4163282249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PathRequestManager
struct  PathRequestManager_t4163282249  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.Queue`1<PathResult> PathRequestManager::results
	Queue_1_t1374272722 * ___results_2;
	// Pathfinding PathRequestManager::pathfinding
	Pathfinding_t1696161914 * ___pathfinding_4;

public:
	inline static int32_t get_offset_of_results_2() { return static_cast<int32_t>(offsetof(PathRequestManager_t4163282249, ___results_2)); }
	inline Queue_1_t1374272722 * get_results_2() const { return ___results_2; }
	inline Queue_1_t1374272722 ** get_address_of_results_2() { return &___results_2; }
	inline void set_results_2(Queue_1_t1374272722 * value)
	{
		___results_2 = value;
		Il2CppCodeGenWriteBarrier((&___results_2), value);
	}

	inline static int32_t get_offset_of_pathfinding_4() { return static_cast<int32_t>(offsetof(PathRequestManager_t4163282249, ___pathfinding_4)); }
	inline Pathfinding_t1696161914 * get_pathfinding_4() const { return ___pathfinding_4; }
	inline Pathfinding_t1696161914 ** get_address_of_pathfinding_4() { return &___pathfinding_4; }
	inline void set_pathfinding_4(Pathfinding_t1696161914 * value)
	{
		___pathfinding_4 = value;
		Il2CppCodeGenWriteBarrier((&___pathfinding_4), value);
	}
};

struct PathRequestManager_t4163282249_StaticFields
{
public:
	// PathRequestManager PathRequestManager::instance
	PathRequestManager_t4163282249 * ___instance_3;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(PathRequestManager_t4163282249_StaticFields, ___instance_3)); }
	inline PathRequestManager_t4163282249 * get_instance_3() const { return ___instance_3; }
	inline PathRequestManager_t4163282249 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(PathRequestManager_t4163282249 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHREQUESTMANAGER_T4163282249_H
#ifndef GRID_T1081586032_H
#define GRID_T1081586032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Grid
struct  Grid_t1081586032  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Grid::displayGridGizmos
	bool ___displayGridGizmos_2;
	// UnityEngine.LayerMask Grid::unwalkableMask
	LayerMask_t3493934918  ___unwalkableMask_3;
	// UnityEngine.Vector2 Grid::gridWorldSize
	Vector2_t2156229523  ___gridWorldSize_4;
	// System.Single Grid::nodeRadius
	float ___nodeRadius_5;
	// Grid/TerrainType[] Grid::walkableRegions
	TerrainTypeU5BU5D_t1748095520* ___walkableRegions_6;
	// System.Int32 Grid::obstacleProximityPenalty
	int32_t ___obstacleProximityPenalty_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> Grid::walkableRegionsDict
	Dictionary_2_t1839659084 * ___walkableRegionsDict_8;
	// UnityEngine.LayerMask Grid::walkableMask
	LayerMask_t3493934918  ___walkableMask_9;
	// Node[0...,0...] Grid::grid
	NodeU5B0___U2C0___U5D_t1457036568* ___grid_10;
	// System.Single Grid::nodeDiameter
	float ___nodeDiameter_11;
	// System.Int32 Grid::gridSizeX
	int32_t ___gridSizeX_12;
	// System.Int32 Grid::gridSizeY
	int32_t ___gridSizeY_13;
	// System.Int32 Grid::penaltyMin
	int32_t ___penaltyMin_14;
	// System.Int32 Grid::penaltyMax
	int32_t ___penaltyMax_15;

public:
	inline static int32_t get_offset_of_displayGridGizmos_2() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___displayGridGizmos_2)); }
	inline bool get_displayGridGizmos_2() const { return ___displayGridGizmos_2; }
	inline bool* get_address_of_displayGridGizmos_2() { return &___displayGridGizmos_2; }
	inline void set_displayGridGizmos_2(bool value)
	{
		___displayGridGizmos_2 = value;
	}

	inline static int32_t get_offset_of_unwalkableMask_3() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___unwalkableMask_3)); }
	inline LayerMask_t3493934918  get_unwalkableMask_3() const { return ___unwalkableMask_3; }
	inline LayerMask_t3493934918 * get_address_of_unwalkableMask_3() { return &___unwalkableMask_3; }
	inline void set_unwalkableMask_3(LayerMask_t3493934918  value)
	{
		___unwalkableMask_3 = value;
	}

	inline static int32_t get_offset_of_gridWorldSize_4() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___gridWorldSize_4)); }
	inline Vector2_t2156229523  get_gridWorldSize_4() const { return ___gridWorldSize_4; }
	inline Vector2_t2156229523 * get_address_of_gridWorldSize_4() { return &___gridWorldSize_4; }
	inline void set_gridWorldSize_4(Vector2_t2156229523  value)
	{
		___gridWorldSize_4 = value;
	}

	inline static int32_t get_offset_of_nodeRadius_5() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___nodeRadius_5)); }
	inline float get_nodeRadius_5() const { return ___nodeRadius_5; }
	inline float* get_address_of_nodeRadius_5() { return &___nodeRadius_5; }
	inline void set_nodeRadius_5(float value)
	{
		___nodeRadius_5 = value;
	}

	inline static int32_t get_offset_of_walkableRegions_6() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___walkableRegions_6)); }
	inline TerrainTypeU5BU5D_t1748095520* get_walkableRegions_6() const { return ___walkableRegions_6; }
	inline TerrainTypeU5BU5D_t1748095520** get_address_of_walkableRegions_6() { return &___walkableRegions_6; }
	inline void set_walkableRegions_6(TerrainTypeU5BU5D_t1748095520* value)
	{
		___walkableRegions_6 = value;
		Il2CppCodeGenWriteBarrier((&___walkableRegions_6), value);
	}

	inline static int32_t get_offset_of_obstacleProximityPenalty_7() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___obstacleProximityPenalty_7)); }
	inline int32_t get_obstacleProximityPenalty_7() const { return ___obstacleProximityPenalty_7; }
	inline int32_t* get_address_of_obstacleProximityPenalty_7() { return &___obstacleProximityPenalty_7; }
	inline void set_obstacleProximityPenalty_7(int32_t value)
	{
		___obstacleProximityPenalty_7 = value;
	}

	inline static int32_t get_offset_of_walkableRegionsDict_8() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___walkableRegionsDict_8)); }
	inline Dictionary_2_t1839659084 * get_walkableRegionsDict_8() const { return ___walkableRegionsDict_8; }
	inline Dictionary_2_t1839659084 ** get_address_of_walkableRegionsDict_8() { return &___walkableRegionsDict_8; }
	inline void set_walkableRegionsDict_8(Dictionary_2_t1839659084 * value)
	{
		___walkableRegionsDict_8 = value;
		Il2CppCodeGenWriteBarrier((&___walkableRegionsDict_8), value);
	}

	inline static int32_t get_offset_of_walkableMask_9() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___walkableMask_9)); }
	inline LayerMask_t3493934918  get_walkableMask_9() const { return ___walkableMask_9; }
	inline LayerMask_t3493934918 * get_address_of_walkableMask_9() { return &___walkableMask_9; }
	inline void set_walkableMask_9(LayerMask_t3493934918  value)
	{
		___walkableMask_9 = value;
	}

	inline static int32_t get_offset_of_grid_10() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___grid_10)); }
	inline NodeU5B0___U2C0___U5D_t1457036568* get_grid_10() const { return ___grid_10; }
	inline NodeU5B0___U2C0___U5D_t1457036568** get_address_of_grid_10() { return &___grid_10; }
	inline void set_grid_10(NodeU5B0___U2C0___U5D_t1457036568* value)
	{
		___grid_10 = value;
		Il2CppCodeGenWriteBarrier((&___grid_10), value);
	}

	inline static int32_t get_offset_of_nodeDiameter_11() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___nodeDiameter_11)); }
	inline float get_nodeDiameter_11() const { return ___nodeDiameter_11; }
	inline float* get_address_of_nodeDiameter_11() { return &___nodeDiameter_11; }
	inline void set_nodeDiameter_11(float value)
	{
		___nodeDiameter_11 = value;
	}

	inline static int32_t get_offset_of_gridSizeX_12() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___gridSizeX_12)); }
	inline int32_t get_gridSizeX_12() const { return ___gridSizeX_12; }
	inline int32_t* get_address_of_gridSizeX_12() { return &___gridSizeX_12; }
	inline void set_gridSizeX_12(int32_t value)
	{
		___gridSizeX_12 = value;
	}

	inline static int32_t get_offset_of_gridSizeY_13() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___gridSizeY_13)); }
	inline int32_t get_gridSizeY_13() const { return ___gridSizeY_13; }
	inline int32_t* get_address_of_gridSizeY_13() { return &___gridSizeY_13; }
	inline void set_gridSizeY_13(int32_t value)
	{
		___gridSizeY_13 = value;
	}

	inline static int32_t get_offset_of_penaltyMin_14() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___penaltyMin_14)); }
	inline int32_t get_penaltyMin_14() const { return ___penaltyMin_14; }
	inline int32_t* get_address_of_penaltyMin_14() { return &___penaltyMin_14; }
	inline void set_penaltyMin_14(int32_t value)
	{
		___penaltyMin_14 = value;
	}

	inline static int32_t get_offset_of_penaltyMax_15() { return static_cast<int32_t>(offsetof(Grid_t1081586032, ___penaltyMax_15)); }
	inline int32_t get_penaltyMax_15() const { return ___penaltyMax_15; }
	inline int32_t* get_address_of_penaltyMax_15() { return &___penaltyMax_15; }
	inline void set_penaltyMax_15(int32_t value)
	{
		___penaltyMax_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRID_T1081586032_H
#ifndef LEVELSELECT_T2669120976_H
#define LEVELSELECT_T2669120976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelSelect
struct  LevelSelect_t2669120976  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 LevelSelect::index
	int32_t ___index_2;
	// UnityEngine.AudioClip LevelSelect::selectSound
	AudioClip_t3680889665 * ___selectSound_3;
	// UnityEngine.AudioClip LevelSelect::confirmSound
	AudioClip_t3680889665 * ___confirmSound_4;
	// UnityEngine.AudioClip LevelSelect::deniedSound
	AudioClip_t3680889665 * ___deniedSound_5;
	// LevelSelectPanel[] LevelSelect::levelSelectPanels
	LevelSelectPanelU5BU5D_t1747102860* ___levelSelectPanels_6;
	// LevelChanger LevelSelect::levelChanger
	LevelChanger_t225386971 * ___levelChanger_7;
	// System.Boolean LevelSelect::leftAxesInUse
	bool ___leftAxesInUse_8;
	// System.Boolean LevelSelect::rightAxesInUse
	bool ___rightAxesInUse_9;
	// System.Boolean LevelSelect::confirmAxesInUse
	bool ___confirmAxesInUse_10;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_selectSound_3() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___selectSound_3)); }
	inline AudioClip_t3680889665 * get_selectSound_3() const { return ___selectSound_3; }
	inline AudioClip_t3680889665 ** get_address_of_selectSound_3() { return &___selectSound_3; }
	inline void set_selectSound_3(AudioClip_t3680889665 * value)
	{
		___selectSound_3 = value;
		Il2CppCodeGenWriteBarrier((&___selectSound_3), value);
	}

	inline static int32_t get_offset_of_confirmSound_4() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___confirmSound_4)); }
	inline AudioClip_t3680889665 * get_confirmSound_4() const { return ___confirmSound_4; }
	inline AudioClip_t3680889665 ** get_address_of_confirmSound_4() { return &___confirmSound_4; }
	inline void set_confirmSound_4(AudioClip_t3680889665 * value)
	{
		___confirmSound_4 = value;
		Il2CppCodeGenWriteBarrier((&___confirmSound_4), value);
	}

	inline static int32_t get_offset_of_deniedSound_5() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___deniedSound_5)); }
	inline AudioClip_t3680889665 * get_deniedSound_5() const { return ___deniedSound_5; }
	inline AudioClip_t3680889665 ** get_address_of_deniedSound_5() { return &___deniedSound_5; }
	inline void set_deniedSound_5(AudioClip_t3680889665 * value)
	{
		___deniedSound_5 = value;
		Il2CppCodeGenWriteBarrier((&___deniedSound_5), value);
	}

	inline static int32_t get_offset_of_levelSelectPanels_6() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___levelSelectPanels_6)); }
	inline LevelSelectPanelU5BU5D_t1747102860* get_levelSelectPanels_6() const { return ___levelSelectPanels_6; }
	inline LevelSelectPanelU5BU5D_t1747102860** get_address_of_levelSelectPanels_6() { return &___levelSelectPanels_6; }
	inline void set_levelSelectPanels_6(LevelSelectPanelU5BU5D_t1747102860* value)
	{
		___levelSelectPanels_6 = value;
		Il2CppCodeGenWriteBarrier((&___levelSelectPanels_6), value);
	}

	inline static int32_t get_offset_of_levelChanger_7() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___levelChanger_7)); }
	inline LevelChanger_t225386971 * get_levelChanger_7() const { return ___levelChanger_7; }
	inline LevelChanger_t225386971 ** get_address_of_levelChanger_7() { return &___levelChanger_7; }
	inline void set_levelChanger_7(LevelChanger_t225386971 * value)
	{
		___levelChanger_7 = value;
		Il2CppCodeGenWriteBarrier((&___levelChanger_7), value);
	}

	inline static int32_t get_offset_of_leftAxesInUse_8() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___leftAxesInUse_8)); }
	inline bool get_leftAxesInUse_8() const { return ___leftAxesInUse_8; }
	inline bool* get_address_of_leftAxesInUse_8() { return &___leftAxesInUse_8; }
	inline void set_leftAxesInUse_8(bool value)
	{
		___leftAxesInUse_8 = value;
	}

	inline static int32_t get_offset_of_rightAxesInUse_9() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___rightAxesInUse_9)); }
	inline bool get_rightAxesInUse_9() const { return ___rightAxesInUse_9; }
	inline bool* get_address_of_rightAxesInUse_9() { return &___rightAxesInUse_9; }
	inline void set_rightAxesInUse_9(bool value)
	{
		___rightAxesInUse_9 = value;
	}

	inline static int32_t get_offset_of_confirmAxesInUse_10() { return static_cast<int32_t>(offsetof(LevelSelect_t2669120976, ___confirmAxesInUse_10)); }
	inline bool get_confirmAxesInUse_10() const { return ___confirmAxesInUse_10; }
	inline bool* get_address_of_confirmAxesInUse_10() { return &___confirmAxesInUse_10; }
	inline void set_confirmAxesInUse_10(bool value)
	{
		___confirmAxesInUse_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSELECT_T2669120976_H
#ifndef LEVELSELECTPANEL_T2626311185_H
#define LEVELSELECTPANEL_T2626311185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelSelectPanel
struct  LevelSelectPanel_t2626311185  : public MonoBehaviour_t3962482529
{
public:
	// System.String LevelSelectPanel::levelName
	String_t* ___levelName_2;
	// System.Boolean LevelSelectPanel::levelUnlocked
	bool ___levelUnlocked_3;
	// System.Boolean LevelSelectPanel::levelCompleted
	bool ___levelCompleted_4;
	// System.Boolean LevelSelectPanel::selected
	bool ___selected_5;
	// UnityEngine.UI.Image LevelSelectPanel::panelImage
	Image_t2670269651 * ___panelImage_6;
	// UnityEngine.UI.Image LevelSelectPanel::levelStatus
	Image_t2670269651 * ___levelStatus_7;
	// UnityEngine.Sprite[] LevelSelectPanel::panelSprites
	SpriteU5BU5D_t2581906349* ___panelSprites_8;

public:
	inline static int32_t get_offset_of_levelName_2() { return static_cast<int32_t>(offsetof(LevelSelectPanel_t2626311185, ___levelName_2)); }
	inline String_t* get_levelName_2() const { return ___levelName_2; }
	inline String_t** get_address_of_levelName_2() { return &___levelName_2; }
	inline void set_levelName_2(String_t* value)
	{
		___levelName_2 = value;
		Il2CppCodeGenWriteBarrier((&___levelName_2), value);
	}

	inline static int32_t get_offset_of_levelUnlocked_3() { return static_cast<int32_t>(offsetof(LevelSelectPanel_t2626311185, ___levelUnlocked_3)); }
	inline bool get_levelUnlocked_3() const { return ___levelUnlocked_3; }
	inline bool* get_address_of_levelUnlocked_3() { return &___levelUnlocked_3; }
	inline void set_levelUnlocked_3(bool value)
	{
		___levelUnlocked_3 = value;
	}

	inline static int32_t get_offset_of_levelCompleted_4() { return static_cast<int32_t>(offsetof(LevelSelectPanel_t2626311185, ___levelCompleted_4)); }
	inline bool get_levelCompleted_4() const { return ___levelCompleted_4; }
	inline bool* get_address_of_levelCompleted_4() { return &___levelCompleted_4; }
	inline void set_levelCompleted_4(bool value)
	{
		___levelCompleted_4 = value;
	}

	inline static int32_t get_offset_of_selected_5() { return static_cast<int32_t>(offsetof(LevelSelectPanel_t2626311185, ___selected_5)); }
	inline bool get_selected_5() const { return ___selected_5; }
	inline bool* get_address_of_selected_5() { return &___selected_5; }
	inline void set_selected_5(bool value)
	{
		___selected_5 = value;
	}

	inline static int32_t get_offset_of_panelImage_6() { return static_cast<int32_t>(offsetof(LevelSelectPanel_t2626311185, ___panelImage_6)); }
	inline Image_t2670269651 * get_panelImage_6() const { return ___panelImage_6; }
	inline Image_t2670269651 ** get_address_of_panelImage_6() { return &___panelImage_6; }
	inline void set_panelImage_6(Image_t2670269651 * value)
	{
		___panelImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___panelImage_6), value);
	}

	inline static int32_t get_offset_of_levelStatus_7() { return static_cast<int32_t>(offsetof(LevelSelectPanel_t2626311185, ___levelStatus_7)); }
	inline Image_t2670269651 * get_levelStatus_7() const { return ___levelStatus_7; }
	inline Image_t2670269651 ** get_address_of_levelStatus_7() { return &___levelStatus_7; }
	inline void set_levelStatus_7(Image_t2670269651 * value)
	{
		___levelStatus_7 = value;
		Il2CppCodeGenWriteBarrier((&___levelStatus_7), value);
	}

	inline static int32_t get_offset_of_panelSprites_8() { return static_cast<int32_t>(offsetof(LevelSelectPanel_t2626311185, ___panelSprites_8)); }
	inline SpriteU5BU5D_t2581906349* get_panelSprites_8() const { return ___panelSprites_8; }
	inline SpriteU5BU5D_t2581906349** get_address_of_panelSprites_8() { return &___panelSprites_8; }
	inline void set_panelSprites_8(SpriteU5BU5D_t2581906349* value)
	{
		___panelSprites_8 = value;
		Il2CppCodeGenWriteBarrier((&___panelSprites_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVELSELECTPANEL_T2626311185_H
#ifndef ITEMPICKUP_T4277059944_H
#define ITEMPICKUP_T4277059944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemPickup
struct  ItemPickup_t4277059944  : public Interactible_t1060880155
{
public:
	// Item ItemPickup::item
	Item_t2953980098 * ___item_4;
	// UnityEngine.AudioClip ItemPickup::pickupSound
	AudioClip_t3680889665 * ___pickupSound_5;

public:
	inline static int32_t get_offset_of_item_4() { return static_cast<int32_t>(offsetof(ItemPickup_t4277059944, ___item_4)); }
	inline Item_t2953980098 * get_item_4() const { return ___item_4; }
	inline Item_t2953980098 ** get_address_of_item_4() { return &___item_4; }
	inline void set_item_4(Item_t2953980098 * value)
	{
		___item_4 = value;
		Il2CppCodeGenWriteBarrier((&___item_4), value);
	}

	inline static int32_t get_offset_of_pickupSound_5() { return static_cast<int32_t>(offsetof(ItemPickup_t4277059944, ___pickupSound_5)); }
	inline AudioClip_t3680889665 * get_pickupSound_5() const { return ___pickupSound_5; }
	inline AudioClip_t3680889665 ** get_address_of_pickupSound_5() { return &___pickupSound_5; }
	inline void set_pickupSound_5(AudioClip_t3680889665 * value)
	{
		___pickupSound_5 = value;
		Il2CppCodeGenWriteBarrier((&___pickupSound_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMPICKUP_T4277059944_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4200 = { sizeof (Bullet_t1042140031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4200[6] = 
{
	Bullet_t1042140031::get_offset_of_lifeTime_2(),
	Bullet_t1042140031::get_offset_of_material_3(),
	Bullet_t1042140031::get_offset_of_originalCol_4(),
	Bullet_t1042140031::get_offset_of_fadePercent_5(),
	Bullet_t1042140031::get_offset_of_disposeTime_6(),
	Bullet_t1042140031::get_offset_of_fading_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4201 = { sizeof (U3CFadeU3Ed__7_t626104978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4201[3] = 
{
	U3CFadeU3Ed__7_t626104978::get_offset_of_U3CU3E1__state_0(),
	U3CFadeU3Ed__7_t626104978::get_offset_of_U3CU3E2__current_1(),
	U3CFadeU3Ed__7_t626104978::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4202 = { sizeof (GameCamera_t2564829307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4202[5] = 
{
	GameCamera_t2564829307::get_offset_of_xModifier_2(),
	GameCamera_t2564829307::get_offset_of_yModifier_3(),
	GameCamera_t2564829307::get_offset_of_zModifier_4(),
	GameCamera_t2564829307::get_offset_of_cameraTarget_5(),
	GameCamera_t2564829307::get_offset_of_target_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4203 = { sizeof (Gun_t783153271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4203[11] = 
{
	Gun_t783153271::get_offset_of_gunType_2(),
	Gun_t783153271::get_offset_of_rpm_3(),
	Gun_t783153271::get_offset_of_gunShot_4(),
	Gun_t783153271::get_offset_of_laserShot_5(),
	Gun_t783153271::get_offset_of_spawn_6(),
	Gun_t783153271::get_offset_of_shell_7(),
	Gun_t783153271::get_offset_of_laser_8(),
	Gun_t783153271::get_offset_of_rounds_9(),
	Gun_t783153271::get_offset_of_uses_10(),
	Gun_t783153271::get_offset_of_secondsBetweenShots_11(),
	Gun_t783153271::get_offset_of_nextPossibleShootTime_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4204 = { sizeof (GunType_t2644971432)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4204[4] = 
{
	GunType_t2644971432::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4205 = { sizeof (PlayerController_t2064355688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4205[8] = 
{
	PlayerController_t2064355688::get_offset_of_inventory_2(),
	PlayerController_t2064355688::get_offset_of_laserGunEnabled_3(),
	PlayerController_t2064355688::get_offset_of_laserShieldEnabled_4(),
	PlayerController_t2064355688::get_offset_of_gun_5(),
	PlayerController_t2064355688::get_offset_of_moveSpeed_6(),
	PlayerController_t2064355688::get_offset_of_turnSpeed_7(),
	PlayerController_t2064355688::get_offset_of_ourCamera_8(),
	PlayerController_t2064355688::get_offset_of_reloadTime_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4206 = { sizeof (PlayerHealthUI_t636700963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4206[2] = 
{
	PlayerHealthUI_t636700963::get_offset_of_pStats_2(),
	PlayerHealthUI_t636700963::get_offset_of_pHealthValue_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4207 = { sizeof (PlayerManager_t1349889689), -1, sizeof(PlayerManager_t1349889689_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4207[2] = 
{
	PlayerManager_t1349889689_StaticFields::get_offset_of_instance_2(),
	PlayerManager_t1349889689::get_offset_of_player_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4208 = { sizeof (PlayerStats_t2044123780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4208[3] = 
{
	PlayerStats_t2044123780::get_offset_of_pCurrentHealth_2(),
	PlayerStats_t2044123780::get_offset_of_pMaximumHealth_3(),
	PlayerStats_t2044123780::get_offset_of_gameOverScene_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4209 = { sizeof (Shield_t1860136854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4209[1] = 
{
	Shield_t1860136854::get_offset_of_uses_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4210 = { sizeof (DamageEnemy_t2198349935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4210[4] = 
{
	DamageEnemy_t2198349935::get_offset_of_explosionSmall_2(),
	DamageEnemy_t2198349935::get_offset_of_explosionBig_3(),
	DamageEnemy_t2198349935::get_offset_of_damageValue_4(),
	DamageEnemy_t2198349935::get_offset_of_healthUI_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4211 = { sizeof (DamagePlayer_t2033828012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4211[4] = 
{
	DamagePlayer_t2033828012::get_offset_of_explosionSmall_2(),
	DamagePlayer_t2033828012::get_offset_of_explosionBig_3(),
	DamagePlayer_t2033828012::get_offset_of_damageValue_4(),
	DamagePlayer_t2033828012::get_offset_of_healthUI_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4212 = { sizeof (AIStateMachine_t2425577084), -1, sizeof(AIStateMachine_t2425577084_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4212[5] = 
{
	AIStateMachine_t2425577084_StaticFields::get_offset_of_currentState_2(),
	AIStateMachine_t2425577084_StaticFields::get_offset_of_phaseOne_3(),
	AIStateMachine_t2425577084_StaticFields::get_offset_of_phaseTwo_4(),
	AIStateMachine_t2425577084_StaticFields::get_offset_of_phaseThree_5(),
	AIStateMachine_t2425577084::get_offset_of_controller_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4213 = { sizeof (AIStates_t2852279327)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4213[7] = 
{
	AIStates_t2852279327::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4214 = { sizeof (BossTankController_t1902331492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4214[8] = 
{
	BossTankController_t1902331492::get_offset_of_eStats_2(),
	BossTankController_t1902331492::get_offset_of_eShoot_3(),
	BossTankController_t1902331492::get_offset_of_unit_4(),
	BossTankController_t1902331492::get_offset_of_player_5(),
	BossTankController_t1902331492::get_offset_of_healingSpot_6(),
	BossTankController_t1902331492::get_offset_of_bullet_7(),
	BossTankController_t1902331492::get_offset_of_laser_8(),
	BossTankController_t1902331492::get_offset_of_centerPoint_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4215 = { sizeof (BossUI_t1795207708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4215[5] = 
{
	BossUI_t1795207708::get_offset_of_enemyTank_2(),
	BossUI_t1795207708::get_offset_of_enemyHealthBar_3(),
	BossUI_t1795207708::get_offset_of_enemyHealthBarValue_4(),
	BossUI_t1795207708::get_offset_of_playerTank_5(),
	BossUI_t1795207708::get_offset_of_playerHealthText_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4216 = { sizeof (EnemyController_t2191660454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4216[7] = 
{
	EnemyController_t2191660454::get_offset_of_agent_2(),
	EnemyController_t2191660454::get_offset_of_gun_3(),
	EnemyController_t2191660454::get_offset_of_lookRadius_4(),
	EnemyController_t2191660454::get_offset_of_reloadTime_5(),
	EnemyController_t2191660454::get_offset_of_target_6(),
	EnemyController_t2191660454::get_offset_of_fieldOfViewBox_7(),
	EnemyController_t2191660454::get_offset_of_spottedPlayer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4217 = { sizeof (EnemyCount_t3730988989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4217[3] = 
{
	EnemyCount_t3730988989::get_offset_of_enemy_2(),
	EnemyCount_t3730988989::get_offset_of_enemyNumber_3(),
	EnemyCount_t3730988989::get_offset_of_levelClear_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4218 = { sizeof (EnemyShoot_t243830779), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4218[9] = 
{
	EnemyShoot_t243830779::get_offset_of_gunShot_2(),
	EnemyShoot_t243830779::get_offset_of_laserShot_3(),
	EnemyShoot_t243830779::get_offset_of_bullet_4(),
	EnemyShoot_t243830779::get_offset_of_laser_5(),
	EnemyShoot_t243830779::get_offset_of_spawn_6(),
	EnemyShoot_t243830779::get_offset_of_bulletSpeed_7(),
	EnemyShoot_t243830779::get_offset_of_cooldownTime_8(),
	EnemyShoot_t243830779::get_offset_of_firedGun_9(),
	EnemyShoot_t243830779::get_offset_of_cooldownTimeMax_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4219 = { sizeof (EnemyStats_t4187841152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4219[3] = 
{
	EnemyStats_t4187841152::get_offset_of_eCurrentHealth_2(),
	EnemyStats_t4187841152::get_offset_of_eMaximumHealth_3(),
	EnemyStats_t4187841152::get_offset_of_eCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4220 = { sizeof (EnemyFuzzyState_t3026123389), -1, sizeof(EnemyFuzzyState_t3026123389_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4220[10] = 
{
	EnemyFuzzyState_t3026123389::get_offset_of_critical_2(),
	EnemyFuzzyState_t3026123389::get_offset_of_hurt_3(),
	EnemyFuzzyState_t3026123389::get_offset_of_healthy_4(),
	EnemyFuzzyState_t3026123389::get_offset_of_enemyTank_5(),
	EnemyFuzzyState_t3026123389::get_offset_of_fuzzyState_6(),
	EnemyFuzzyState_t3026123389::get_offset_of_enemyHealth_7(),
	EnemyFuzzyState_t3026123389_StaticFields::get_offset_of_currentFuzzyState_8(),
	EnemyFuzzyState_t3026123389::get_offset_of_valueHealthy_9(),
	EnemyFuzzyState_t3026123389::get_offset_of_valueHurt_10(),
	EnemyFuzzyState_t3026123389::get_offset_of_valueCritical_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4221 = { sizeof (FuzzyStates_t3314355612)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4221[4] = 
{
	FuzzyStates_t3314355612::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4222 = { sizeof (FuzzySample_t2008138351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4222[12] = 
{
	FuzzySample_t2008138351::get_offset_of_critical_2(),
	FuzzySample_t2008138351::get_offset_of_hurt_3(),
	FuzzySample_t2008138351::get_offset_of_healthy_4(),
	FuzzySample_t2008138351::get_offset_of_healthInput_5(),
	FuzzySample_t2008138351::get_offset_of_labelHealthy_6(),
	FuzzySample_t2008138351::get_offset_of_labelHurt_7(),
	FuzzySample_t2008138351::get_offset_of_labelCritical_8(),
	FuzzySample_t2008138351::get_offset_of_fuzzyState_9(),
	0,
	FuzzySample_t2008138351::get_offset_of_valueHealthy_11(),
	FuzzySample_t2008138351::get_offset_of_valueHurt_12(),
	FuzzySample_t2008138351::get_offset_of_valueCritical_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4223 = { sizeof (GameOverScreen_t2285777029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4223[9] = 
{
	GameOverScreen_t2285777029::get_offset_of_index_2(),
	GameOverScreen_t2285777029::get_offset_of_selectSound_3(),
	GameOverScreen_t2285777029::get_offset_of_confirmSound_4(),
	GameOverScreen_t2285777029::get_offset_of_options_5(),
	GameOverScreen_t2285777029::get_offset_of_optionBox_6(),
	GameOverScreen_t2285777029::get_offset_of_levelChanger_7(),
	GameOverScreen_t2285777029::get_offset_of_leftAxesInUse_8(),
	GameOverScreen_t2285777029::get_offset_of_rightAxesInUse_9(),
	GameOverScreen_t2285777029::get_offset_of_confirmAxesInUse_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4224 = { sizeof (HealingSpot_t2443304191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4224[2] = 
{
	HealingSpot_t2443304191::get_offset_of_positionsIndex_2(),
	HealingSpot_t2443304191::get_offset_of_positions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4225 = { sizeof (HealthUI_t1908446248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4225[3] = 
{
	HealthUI_t1908446248::get_offset_of_playerStats_2(),
	HealthUI_t1908446248::get_offset_of_destroyBullet_3(),
	HealthUI_t1908446248::get_offset_of_healthHearts_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4226 = { sizeof (Interactible_t1060880155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4226[2] = 
{
	Interactible_t1060880155::get_offset_of_radius_2(),
	Interactible_t1060880155::get_offset_of_interactionTransform_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4227 = { sizeof (Inventory_t1050226016), -1, sizeof(Inventory_t1050226016_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4227[6] = 
{
	Inventory_t1050226016_StaticFields::get_offset_of_instance_2(),
	Inventory_t1050226016::get_offset_of_onItemChangedCallback_3(),
	Inventory_t1050226016::get_offset_of_weaponSpace_4(),
	Inventory_t1050226016::get_offset_of_shieldSpace_5(),
	Inventory_t1050226016::get_offset_of_weapon_6(),
	Inventory_t1050226016::get_offset_of_shield_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4228 = { sizeof (OnItemChanged_t22848112), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4229 = { sizeof (InventorySlot_t3299309524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4229[5] = 
{
	InventorySlot_t3299309524::get_offset_of_item_2(),
	InventorySlot_t3299309524::get_offset_of_slotType_3(),
	InventorySlot_t3299309524::get_offset_of_icon_4(),
	InventorySlot_t3299309524::get_offset_of_button_5(),
	InventorySlot_t3299309524::get_offset_of_itemName_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4230 = { sizeof (SlotType_t413958767)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4230[3] = 
{
	SlotType_t413958767::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4231 = { sizeof (InventoryUI_t1983315844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4231[3] = 
{
	InventoryUI_t1983315844::get_offset_of_inventory_2(),
	InventoryUI_t1983315844::get_offset_of_weaponSlot_3(),
	InventoryUI_t1983315844::get_offset_of_shieldSlot_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4232 = { sizeof (Item_t2953980098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4232[4] = 
{
	Item_t2953980098::get_offset_of_name_2(),
	Item_t2953980098::get_offset_of_icon_3(),
	Item_t2953980098::get_offset_of_isWeapon_4(),
	Item_t2953980098::get_offset_of_isDefaultItem_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4233 = { sizeof (ItemPickup_t4277059944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4233[2] = 
{
	ItemPickup_t4277059944::get_offset_of_item_4(),
	ItemPickup_t4277059944::get_offset_of_pickupSound_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4234 = { sizeof (LevelChanger_t225386971), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4234[4] = 
{
	LevelChanger_t225386971::get_offset_of_animator_2(),
	LevelChanger_t225386971::get_offset_of_levelNameToLoad_3(),
	LevelChanger_t225386971::get_offset_of_levelIndexToLoad_4(),
	LevelChanger_t225386971::get_offset_of_loadByName_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4235 = { sizeof (LevelClear_t1949285084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4235[5] = 
{
	LevelClear_t1949285084::get_offset_of_animator_2(),
	LevelClear_t1949285084::get_offset_of_levelChanger_3(),
	LevelClear_t1949285084::get_offset_of_levelNameToLoad_4(),
	LevelClear_t1949285084::get_offset_of_levelIndexToLoad_5(),
	LevelClear_t1949285084::get_offset_of_loadByName_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4236 = { sizeof (BackgroundScroller_t2508866296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4236[4] = 
{
	BackgroundScroller_t2508866296::get_offset_of_scrollSpeed_2(),
	BackgroundScroller_t2508866296::get_offset_of_tileSizeY_3(),
	BackgroundScroller_t2508866296::get_offset_of_rect_4(),
	BackgroundScroller_t2508866296::get_offset_of_startPosition_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4237 = { sizeof (LevelSelect_t2669120976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4237[9] = 
{
	LevelSelect_t2669120976::get_offset_of_index_2(),
	LevelSelect_t2669120976::get_offset_of_selectSound_3(),
	LevelSelect_t2669120976::get_offset_of_confirmSound_4(),
	LevelSelect_t2669120976::get_offset_of_deniedSound_5(),
	LevelSelect_t2669120976::get_offset_of_levelSelectPanels_6(),
	LevelSelect_t2669120976::get_offset_of_levelChanger_7(),
	LevelSelect_t2669120976::get_offset_of_leftAxesInUse_8(),
	LevelSelect_t2669120976::get_offset_of_rightAxesInUse_9(),
	LevelSelect_t2669120976::get_offset_of_confirmAxesInUse_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4238 = { sizeof (LevelSelectPanel_t2626311185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4238[7] = 
{
	LevelSelectPanel_t2626311185::get_offset_of_levelName_2(),
	LevelSelectPanel_t2626311185::get_offset_of_levelUnlocked_3(),
	LevelSelectPanel_t2626311185::get_offset_of_levelCompleted_4(),
	LevelSelectPanel_t2626311185::get_offset_of_selected_5(),
	LevelSelectPanel_t2626311185::get_offset_of_panelImage_6(),
	LevelSelectPanel_t2626311185::get_offset_of_levelStatus_7(),
	LevelSelectPanel_t2626311185::get_offset_of_panelSprites_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4239 = { sizeof (Grid_t1081586032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4239[14] = 
{
	Grid_t1081586032::get_offset_of_displayGridGizmos_2(),
	Grid_t1081586032::get_offset_of_unwalkableMask_3(),
	Grid_t1081586032::get_offset_of_gridWorldSize_4(),
	Grid_t1081586032::get_offset_of_nodeRadius_5(),
	Grid_t1081586032::get_offset_of_walkableRegions_6(),
	Grid_t1081586032::get_offset_of_obstacleProximityPenalty_7(),
	Grid_t1081586032::get_offset_of_walkableRegionsDict_8(),
	Grid_t1081586032::get_offset_of_walkableMask_9(),
	Grid_t1081586032::get_offset_of_grid_10(),
	Grid_t1081586032::get_offset_of_nodeDiameter_11(),
	Grid_t1081586032::get_offset_of_gridSizeX_12(),
	Grid_t1081586032::get_offset_of_gridSizeY_13(),
	Grid_t1081586032::get_offset_of_penaltyMin_14(),
	Grid_t1081586032::get_offset_of_penaltyMax_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4240 = { sizeof (TerrainType_t3627529997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4240[2] = 
{
	TerrainType_t3627529997::get_offset_of_terrainMask_0(),
	TerrainType_t3627529997::get_offset_of_terrainPenalty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4241 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable4241[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4242 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4243 = { sizeof (Line_t3796957314)+ sizeof (RuntimeObject), sizeof(Line_t3796957314_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4243[7] = 
{
	0,
	Line_t3796957314::get_offset_of_gradient_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Line_t3796957314::get_offset_of_yIntercept_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Line_t3796957314::get_offset_of_pointOnLine_1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Line_t3796957314::get_offset_of_pointOnLine_2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Line_t3796957314::get_offset_of_gradientPerpendicular_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Line_t3796957314::get_offset_of_approachSide_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4244 = { sizeof (Node_t2989995042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4244[9] = 
{
	Node_t2989995042::get_offset_of_walkable_0(),
	Node_t2989995042::get_offset_of_worldPosition_1(),
	Node_t2989995042::get_offset_of_gridX_2(),
	Node_t2989995042::get_offset_of_gridY_3(),
	Node_t2989995042::get_offset_of_movementPenalty_4(),
	Node_t2989995042::get_offset_of_gCost_5(),
	Node_t2989995042::get_offset_of_hCost_6(),
	Node_t2989995042::get_offset_of_parent_7(),
	Node_t2989995042::get_offset_of_heapIndex_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4245 = { sizeof (Path_t2615110272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4245[4] = 
{
	Path_t2615110272::get_offset_of_lookPoints_0(),
	Path_t2615110272::get_offset_of_turnBoundaries_1(),
	Path_t2615110272::get_offset_of_finishLineIndex_2(),
	Path_t2615110272::get_offset_of_slowDownIndex_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4246 = { sizeof (Pathfinding_t1696161914), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4246[1] = 
{
	Pathfinding_t1696161914::get_offset_of_grid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4247 = { sizeof (PathRequestManager_t4163282249), -1, sizeof(PathRequestManager_t4163282249_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4247[3] = 
{
	PathRequestManager_t4163282249::get_offset_of_results_2(),
	PathRequestManager_t4163282249_StaticFields::get_offset_of_instance_3(),
	PathRequestManager_t4163282249::get_offset_of_pathfinding_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4248 = { sizeof (U3CU3Ec__DisplayClass5_0_t786576806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4248[1] = 
{
	U3CU3Ec__DisplayClass5_0_t786576806::get_offset_of_request_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4249 = { sizeof (PathResult_t1528013228)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4249[3] = 
{
	PathResult_t1528013228::get_offset_of_path_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathResult_t1528013228::get_offset_of_success_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathResult_t1528013228::get_offset_of_callback_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4250 = { sizeof (PathRequest_t2117613800)+ sizeof (RuntimeObject), sizeof(PathRequest_t2117613800_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable4250[3] = 
{
	PathRequest_t2117613800::get_offset_of_pathStart_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathRequest_t2117613800::get_offset_of_pathEnd_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathRequest_t2117613800::get_offset_of_callback_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4251 = { sizeof (Unit_t4139495810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4251[8] = 
{
	0,
	0,
	Unit_t4139495810::get_offset_of_target_4(),
	Unit_t4139495810::get_offset_of_speed_5(),
	Unit_t4139495810::get_offset_of_turnSpeed_6(),
	Unit_t4139495810::get_offset_of_turnDst_7(),
	Unit_t4139495810::get_offset_of_stoppingDst_8(),
	Unit_t4139495810::get_offset_of_path_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4252 = { sizeof (U3CUpdatePathU3Ed__10_t1880251700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4252[5] = 
{
	U3CUpdatePathU3Ed__10_t1880251700::get_offset_of_U3CU3E1__state_0(),
	U3CUpdatePathU3Ed__10_t1880251700::get_offset_of_U3CU3E2__current_1(),
	U3CUpdatePathU3Ed__10_t1880251700::get_offset_of_U3CU3E4__this_2(),
	U3CUpdatePathU3Ed__10_t1880251700::get_offset_of_U3CtargetPosOldU3E5__1_3(),
	U3CUpdatePathU3Ed__10_t1880251700::get_offset_of_U3CsquareMoveThreshholdU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4253 = { sizeof (U3CFollowPathU3Ed__11_t3675468882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4253[6] = 
{
	U3CFollowPathU3Ed__11_t3675468882::get_offset_of_U3CU3E1__state_0(),
	U3CFollowPathU3Ed__11_t3675468882::get_offset_of_U3CU3E2__current_1(),
	U3CFollowPathU3Ed__11_t3675468882::get_offset_of_U3CU3E4__this_2(),
	U3CFollowPathU3Ed__11_t3675468882::get_offset_of_U3CpathIndexU3E5__1_3(),
	U3CFollowPathU3Ed__11_t3675468882::get_offset_of_U3CfollowingPathU3E5__2_4(),
	U3CFollowPathU3Ed__11_t3675468882::get_offset_of_U3CspeedPercentU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4254 = { sizeof (PauseMenu_t3916167947), -1, sizeof(PauseMenu_t3916167947_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable4254[11] = 
{
	PauseMenu_t3916167947_StaticFields::get_offset_of_gameIsPaused_2(),
	PauseMenu_t3916167947_StaticFields::get_offset_of_pausingEnabled_3(),
	PauseMenu_t3916167947::get_offset_of_pauseMenuUI_4(),
	PauseMenu_t3916167947::get_offset_of_index_5(),
	PauseMenu_t3916167947::get_offset_of_selectSound_6(),
	PauseMenu_t3916167947::get_offset_of_confirmSound_7(),
	PauseMenu_t3916167947::get_offset_of_options_8(),
	PauseMenu_t3916167947::get_offset_of_optionBox_9(),
	PauseMenu_t3916167947::get_offset_of_upAxesInUse_10(),
	PauseMenu_t3916167947::get_offset_of_downAxesInUse_11(),
	PauseMenu_t3916167947::get_offset_of_cancelAxesInUse_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4255 = { sizeof (RotatingLights_t2402942693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4255[3] = 
{
	RotatingLights_t2402942693::get_offset_of_lights1_2(),
	RotatingLights_t2402942693::get_offset_of_lights2_3(),
	RotatingLights_t2402942693::get_offset_of_rotationSpeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4256 = { sizeof (SelectTitleOptions_t2341392585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4256[10] = 
{
	SelectTitleOptions_t2341392585::get_offset_of_index_2(),
	SelectTitleOptions_t2341392585::get_offset_of_selectSound_3(),
	SelectTitleOptions_t2341392585::get_offset_of_confirmSound_4(),
	SelectTitleOptions_t2341392585::get_offset_of_newGameSceneName_5(),
	SelectTitleOptions_t2341392585::get_offset_of_options_6(),
	SelectTitleOptions_t2341392585::get_offset_of_titleOptionBox_7(),
	SelectTitleOptions_t2341392585::get_offset_of_levelChanger_8(),
	SelectTitleOptions_t2341392585::get_offset_of_leftAxesInUse_9(),
	SelectTitleOptions_t2341392585::get_offset_of_rightAxesInUse_10(),
	SelectTitleOptions_t2341392585::get_offset_of_confirmAxesInUse_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4257 = { sizeof (U3CModuleU3E_t692745563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4258 = { sizeof (AlphaButtonClickMask_t141136539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4258[1] = 
{
	AlphaButtonClickMask_t141136539::get_offset_of__image_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4259 = { sizeof (EventSystemChecker_t1882757729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4260 = { sizeof (ForcedReset_t301124368), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4261 = { sizeof (ActivateTrigger_t3349759092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4261[5] = 
{
	ActivateTrigger_t3349759092::get_offset_of_action_2(),
	ActivateTrigger_t3349759092::get_offset_of_target_3(),
	ActivateTrigger_t3349759092::get_offset_of_source_4(),
	ActivateTrigger_t3349759092::get_offset_of_triggerCount_5(),
	ActivateTrigger_t3349759092::get_offset_of_repeatTrigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4262 = { sizeof (Mode_t3024470803)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4262[7] = 
{
	Mode_t3024470803::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4263 = { sizeof (AutoMobileShaderSwitch_t568447889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4263[1] = 
{
	AutoMobileShaderSwitch_t568447889::get_offset_of_m_ReplacementList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4264 = { sizeof (ReplacementDefinition_t2693741842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4264[2] = 
{
	ReplacementDefinition_t2693741842::get_offset_of_original_0(),
	ReplacementDefinition_t2693741842::get_offset_of_replacement_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4265 = { sizeof (ReplacementList_t1887104210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4265[1] = 
{
	ReplacementList_t1887104210::get_offset_of_items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4266 = { sizeof (AutoMoveAndRotate_t2437913015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4266[4] = 
{
	AutoMoveAndRotate_t2437913015::get_offset_of_moveUnitsPerSecond_2(),
	AutoMoveAndRotate_t2437913015::get_offset_of_rotateDegreesPerSecond_3(),
	AutoMoveAndRotate_t2437913015::get_offset_of_ignoreTimescale_4(),
	AutoMoveAndRotate_t2437913015::get_offset_of_m_LastRealTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4267 = { sizeof (Vector3andSpace_t219844479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4267[2] = 
{
	Vector3andSpace_t219844479::get_offset_of_value_0(),
	Vector3andSpace_t219844479::get_offset_of_space_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4268 = { sizeof (CameraRefocus_t4263235746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4268[5] = 
{
	CameraRefocus_t4263235746::get_offset_of_Camera_0(),
	CameraRefocus_t4263235746::get_offset_of_Lookatpoint_1(),
	CameraRefocus_t4263235746::get_offset_of_Parent_2(),
	CameraRefocus_t4263235746::get_offset_of_m_OrigCameraPos_3(),
	CameraRefocus_t4263235746::get_offset_of_m_Refocus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4269 = { sizeof (CurveControlledBob_t2679313829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4269[9] = 
{
	CurveControlledBob_t2679313829::get_offset_of_HorizontalBobRange_0(),
	CurveControlledBob_t2679313829::get_offset_of_VerticalBobRange_1(),
	CurveControlledBob_t2679313829::get_offset_of_Bobcurve_2(),
	CurveControlledBob_t2679313829::get_offset_of_VerticaltoHorizontalRatio_3(),
	CurveControlledBob_t2679313829::get_offset_of_m_CyclePositionX_4(),
	CurveControlledBob_t2679313829::get_offset_of_m_CyclePositionY_5(),
	CurveControlledBob_t2679313829::get_offset_of_m_BobBaseInterval_6(),
	CurveControlledBob_t2679313829::get_offset_of_m_OriginalCameraPosition_7(),
	CurveControlledBob_t2679313829::get_offset_of_m_Time_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4270 = { sizeof (DragRigidbody_t1600652016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4270[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DragRigidbody_t1600652016::get_offset_of_m_SpringJoint_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4271 = { sizeof (U3CDragObjectU3Ed__8_t4113469309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4271[7] = 
{
	U3CDragObjectU3Ed__8_t4113469309::get_offset_of_U3CU3E1__state_0(),
	U3CDragObjectU3Ed__8_t4113469309::get_offset_of_U3CU3E2__current_1(),
	U3CDragObjectU3Ed__8_t4113469309::get_offset_of_U3CU3E4__this_2(),
	U3CDragObjectU3Ed__8_t4113469309::get_offset_of_U3CmainCameraU3E5__1_3(),
	U3CDragObjectU3Ed__8_t4113469309::get_offset_of_distance_4(),
	U3CDragObjectU3Ed__8_t4113469309::get_offset_of_U3ColdDragU3E5__2_5(),
	U3CDragObjectU3Ed__8_t4113469309::get_offset_of_U3ColdAngularDragU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4272 = { sizeof (DynamicShadowSettings_t59119858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4272[11] = 
{
	DynamicShadowSettings_t59119858::get_offset_of_sunLight_2(),
	DynamicShadowSettings_t59119858::get_offset_of_minHeight_3(),
	DynamicShadowSettings_t59119858::get_offset_of_minShadowDistance_4(),
	DynamicShadowSettings_t59119858::get_offset_of_minShadowBias_5(),
	DynamicShadowSettings_t59119858::get_offset_of_maxHeight_6(),
	DynamicShadowSettings_t59119858::get_offset_of_maxShadowDistance_7(),
	DynamicShadowSettings_t59119858::get_offset_of_maxShadowBias_8(),
	DynamicShadowSettings_t59119858::get_offset_of_adaptTime_9(),
	DynamicShadowSettings_t59119858::get_offset_of_m_SmoothHeight_10(),
	DynamicShadowSettings_t59119858::get_offset_of_m_ChangeSpeed_11(),
	DynamicShadowSettings_t59119858::get_offset_of_m_OriginalStrength_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4273 = { sizeof (FollowTarget_t166153614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4273[2] = 
{
	FollowTarget_t166153614::get_offset_of_target_2(),
	FollowTarget_t166153614::get_offset_of_offset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4274 = { sizeof (FOVKick_t120370150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4274[6] = 
{
	FOVKick_t120370150::get_offset_of_Camera_0(),
	FOVKick_t120370150::get_offset_of_originalFov_1(),
	FOVKick_t120370150::get_offset_of_FOVIncrease_2(),
	FOVKick_t120370150::get_offset_of_TimeToIncrease_3(),
	FOVKick_t120370150::get_offset_of_TimeToDecrease_4(),
	FOVKick_t120370150::get_offset_of_IncreaseCurve_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4275 = { sizeof (U3CFOVKickUpU3Ed__9_t1152317412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4275[4] = 
{
	U3CFOVKickUpU3Ed__9_t1152317412::get_offset_of_U3CU3E1__state_0(),
	U3CFOVKickUpU3Ed__9_t1152317412::get_offset_of_U3CU3E2__current_1(),
	U3CFOVKickUpU3Ed__9_t1152317412::get_offset_of_U3CU3E4__this_2(),
	U3CFOVKickUpU3Ed__9_t1152317412::get_offset_of_U3CtU3E5__1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4276 = { sizeof (U3CFOVKickDownU3Ed__10_t494950160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4276[4] = 
{
	U3CFOVKickDownU3Ed__10_t494950160::get_offset_of_U3CU3E1__state_0(),
	U3CFOVKickDownU3Ed__10_t494950160::get_offset_of_U3CU3E2__current_1(),
	U3CFOVKickDownU3Ed__10_t494950160::get_offset_of_U3CU3E4__this_2(),
	U3CFOVKickDownU3Ed__10_t494950160::get_offset_of_U3CtU3E5__1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4277 = { sizeof (FPSCounter_t2351221284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4277[6] = 
{
	0,
	FPSCounter_t2351221284::get_offset_of_m_FpsAccumulator_3(),
	FPSCounter_t2351221284::get_offset_of_m_FpsNextPeriod_4(),
	FPSCounter_t2351221284::get_offset_of_m_CurrentFps_5(),
	0,
	FPSCounter_t2351221284::get_offset_of_m_Text_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4278 = { sizeof (LerpControlledBob_t1895875871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4278[3] = 
{
	LerpControlledBob_t1895875871::get_offset_of_BobDuration_0(),
	LerpControlledBob_t1895875871::get_offset_of_BobAmount_1(),
	LerpControlledBob_t1895875871::get_offset_of_m_Offset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4279 = { sizeof (U3CDoBobCycleU3Ed__4_t3986606630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4279[4] = 
{
	U3CDoBobCycleU3Ed__4_t3986606630::get_offset_of_U3CU3E1__state_0(),
	U3CDoBobCycleU3Ed__4_t3986606630::get_offset_of_U3CU3E2__current_1(),
	U3CDoBobCycleU3Ed__4_t3986606630::get_offset_of_U3CU3E4__this_2(),
	U3CDoBobCycleU3Ed__4_t3986606630::get_offset_of_U3CtU3E5__1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4280 = { sizeof (ObjectResetter_t639177103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4280[4] = 
{
	ObjectResetter_t639177103::get_offset_of_originalPosition_2(),
	ObjectResetter_t639177103::get_offset_of_originalRotation_3(),
	ObjectResetter_t639177103::get_offset_of_originalStructure_4(),
	ObjectResetter_t639177103::get_offset_of_Rigidbody_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4281 = { sizeof (U3CResetCoroutineU3Ed__6_t3318545224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4281[4] = 
{
	U3CResetCoroutineU3Ed__6_t3318545224::get_offset_of_U3CU3E1__state_0(),
	U3CResetCoroutineU3Ed__6_t3318545224::get_offset_of_U3CU3E2__current_1(),
	U3CResetCoroutineU3Ed__6_t3318545224::get_offset_of_delay_2(),
	U3CResetCoroutineU3Ed__6_t3318545224::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4282 = { sizeof (ParticleSystemDestroyer_t558680695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4282[4] = 
{
	ParticleSystemDestroyer_t558680695::get_offset_of_minDuration_2(),
	ParticleSystemDestroyer_t558680695::get_offset_of_maxDuration_3(),
	ParticleSystemDestroyer_t558680695::get_offset_of_m_MaxLifetime_4(),
	ParticleSystemDestroyer_t558680695::get_offset_of_m_EarlyStop_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4283 = { sizeof (U3CStartU3Ed__4_t3571467226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4283[5] = 
{
	U3CStartU3Ed__4_t3571467226::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__4_t3571467226::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__4_t3571467226::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__4_t3571467226::get_offset_of_U3CstopTimeU3E5__1_3(),
	U3CStartU3Ed__4_t3571467226::get_offset_of_U3CsystemsU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4284 = { sizeof (PlatformSpecificContent_t1404549723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4284[4] = 
{
	PlatformSpecificContent_t1404549723::get_offset_of_m_BuildTargetGroup_2(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_Content_3(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_MonoBehaviours_4(),
	PlatformSpecificContent_t1404549723::get_offset_of_m_ChildrenOfThisObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4285 = { sizeof (BuildTargetGroup_t72322187)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4285[3] = 
{
	BuildTargetGroup_t72322187::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4286 = { sizeof (SimpleActivatorMenu_t1387811551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4286[3] = 
{
	SimpleActivatorMenu_t1387811551::get_offset_of_camSwitchButton_2(),
	SimpleActivatorMenu_t1387811551::get_offset_of_objects_3(),
	SimpleActivatorMenu_t1387811551::get_offset_of_m_CurrentActiveObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4287 = { sizeof (SimpleMouseRotator_t2364742953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4287[10] = 
{
	SimpleMouseRotator_t2364742953::get_offset_of_rotationRange_2(),
	SimpleMouseRotator_t2364742953::get_offset_of_rotationSpeed_3(),
	SimpleMouseRotator_t2364742953::get_offset_of_dampingTime_4(),
	SimpleMouseRotator_t2364742953::get_offset_of_autoZeroVerticalOnMobile_5(),
	SimpleMouseRotator_t2364742953::get_offset_of_autoZeroHorizontalOnMobile_6(),
	SimpleMouseRotator_t2364742953::get_offset_of_relative_7(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_TargetAngles_8(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_FollowAngles_9(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_FollowVelocity_10(),
	SimpleMouseRotator_t2364742953::get_offset_of_m_OriginalRotation_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4288 = { sizeof (SmoothFollow_t4204731361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4288[5] = 
{
	SmoothFollow_t4204731361::get_offset_of_target_2(),
	SmoothFollow_t4204731361::get_offset_of_distance_3(),
	SmoothFollow_t4204731361::get_offset_of_height_4(),
	SmoothFollow_t4204731361::get_offset_of_rotationDamping_5(),
	SmoothFollow_t4204731361::get_offset_of_heightDamping_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4289 = { sizeof (TimedObjectActivator_t1846709985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4289[1] = 
{
	TimedObjectActivator_t1846709985::get_offset_of_entries_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4290 = { sizeof (Action_t837364808)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable4290[6] = 
{
	Action_t837364808::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4291 = { sizeof (Entry_t2725803170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4291[3] = 
{
	Entry_t2725803170::get_offset_of_target_0(),
	Entry_t2725803170::get_offset_of_action_1(),
	Entry_t2725803170::get_offset_of_delay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4292 = { sizeof (Entries_t3168066469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4292[1] = 
{
	Entries_t3168066469::get_offset_of_entries_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4293 = { sizeof (U3CActivateU3Ed__5_t4263739043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4293[3] = 
{
	U3CActivateU3Ed__5_t4263739043::get_offset_of_U3CU3E1__state_0(),
	U3CActivateU3Ed__5_t4263739043::get_offset_of_U3CU3E2__current_1(),
	U3CActivateU3Ed__5_t4263739043::get_offset_of_entry_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4294 = { sizeof (U3CDeactivateU3Ed__6_t2861800224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4294[3] = 
{
	U3CDeactivateU3Ed__6_t2861800224::get_offset_of_U3CU3E1__state_0(),
	U3CDeactivateU3Ed__6_t2861800224::get_offset_of_U3CU3E2__current_1(),
	U3CDeactivateU3Ed__6_t2861800224::get_offset_of_entry_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4295 = { sizeof (U3CReloadLevelU3Ed__7_t2023328258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4295[3] = 
{
	U3CReloadLevelU3Ed__7_t2023328258::get_offset_of_U3CU3E1__state_0(),
	U3CReloadLevelU3Ed__7_t2023328258::get_offset_of_U3CU3E2__current_1(),
	U3CReloadLevelU3Ed__7_t2023328258::get_offset_of_entry_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4296 = { sizeof (TimedObjectDestructor_t3438860414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4296[2] = 
{
	TimedObjectDestructor_t3438860414::get_offset_of_m_TimeOut_2(),
	TimedObjectDestructor_t3438860414::get_offset_of_m_DetachChildren_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4297 = { sizeof (WaypointCircuit_t445075330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4297[16] = 
{
	WaypointCircuit_t445075330::get_offset_of_waypointList_2(),
	WaypointCircuit_t445075330::get_offset_of_smoothRoute_3(),
	WaypointCircuit_t445075330::get_offset_of_numPoints_4(),
	WaypointCircuit_t445075330::get_offset_of_points_5(),
	WaypointCircuit_t445075330::get_offset_of_distances_6(),
	WaypointCircuit_t445075330::get_offset_of_editorVisualisationSubsteps_7(),
	WaypointCircuit_t445075330::get_offset_of_U3CLengthU3Ek__BackingField_8(),
	WaypointCircuit_t445075330::get_offset_of_p0n_9(),
	WaypointCircuit_t445075330::get_offset_of_p1n_10(),
	WaypointCircuit_t445075330::get_offset_of_p2n_11(),
	WaypointCircuit_t445075330::get_offset_of_p3n_12(),
	WaypointCircuit_t445075330::get_offset_of_i_13(),
	WaypointCircuit_t445075330::get_offset_of_P0_14(),
	WaypointCircuit_t445075330::get_offset_of_P1_15(),
	WaypointCircuit_t445075330::get_offset_of_P2_16(),
	WaypointCircuit_t445075330::get_offset_of_P3_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4298 = { sizeof (WaypointList_t2584574554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable4298[2] = 
{
	WaypointList_t2584574554::get_offset_of_circuit_0(),
	WaypointList_t2584574554::get_offset_of_items_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4299 = { sizeof (RoutePoint_t3880028948)+ sizeof (RuntimeObject), sizeof(RoutePoint_t3880028948 ), 0, 0 };
extern const int32_t g_FieldOffsetTable4299[2] = 
{
	RoutePoint_t3880028948::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoutePoint_t3880028948::get_offset_of_direction_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
