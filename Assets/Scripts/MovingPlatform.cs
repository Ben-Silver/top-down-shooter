﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public BoxCollider triggerCollider;             // The box collider trigger
    public MovementDir movementDirection;           // Whether the platform will move vertically or horizontally
    public float movementSpeed;                     // How fast the platform moves
    public float movementTime;                      // How long the platform moves for (in seconds)
    public float deadTime;                          // How long the platform stops for (in seconds)

    private float distCounter;                      // Counts how long the platform has been moving for
    private float deadCounter;                      // Counts how long the platform has been dead for
    private bool dead;                              // True when the platform is dead
    private bool reversing;                         // True when the platform is reversing

	// Use this for initialization
	void Start()
    {
		
	}
	
	// Update is called once per frame
	void Update()
    {
		switch (movementDirection)
        {
            // Horizontal movement
            case MovementDir.RIGHT:

                // Moving right
                if (distCounter < movementTime && !reversing)
                {
                    transform.Translate(new Vector3(movementSpeed, 0, 0) * Time.deltaTime);
                    distCounter += Time.deltaTime;
                }
                else if (distCounter > movementTime && !reversing)
                {
                    dead = true;
                }
                
                // Moving left
                if (distCounter > 0 && reversing)
                {
                    transform.Translate(new Vector3(-movementSpeed, 0, 0) * Time.deltaTime);
                    distCounter -= Time.deltaTime;
                }
                else if (distCounter < movementTime && reversing)
                {
                    dead = true;
                    distCounter = 0;
                }

                // Stationary
                if (dead && !reversing)
                {
                    if (deadCounter < deadTime)
                    {
                        deadCounter += Time.deltaTime;
                    }
                    else
                    {
                        dead = false;
                        reversing = true;
                        deadCounter = 0;
                    }
                }
                else if (dead && reversing)
                {
                    if (deadCounter < deadTime)
                    {
                        deadCounter += Time.deltaTime;
                    }
                    else
                    {
                        dead = false;
                        reversing = false;
                        deadCounter = 0;
                    }
                }
            break;

            // Vertical movement
            case MovementDir.UP:
                
                // Moving up
                if (distCounter < movementTime && !reversing)
                {
                    transform.Translate(new Vector3(0, movementTime, 0) * Time.deltaTime);
                    distCounter += Time.deltaTime;
                }
                else if (distCounter > movementTime && !reversing)
                {
                    dead = true;
                }

                // Moving down
                if (distCounter > 0 && reversing)
                {
                    transform.Translate(new Vector3(0, -movementTime, 0) * Time.deltaTime);
                    distCounter -= Time.deltaTime;
                }
                else if (distCounter < movementTime && reversing)
                {
                    dead = true;
                    distCounter = 0;
                }

                // Stationary
                if (dead && !reversing)
                {
                    if (deadCounter < deadTime)
                    {
                        deadCounter += Time.deltaTime;
                    }
                    else
                    {
                        dead = false;
                        reversing = true;
                        deadCounter = 0;
                    }
                }
                else if (dead && reversing)
                {
                    if (deadCounter < deadTime)
                    {
                        deadCounter += Time.deltaTime;
                    }
                    else
                    {
                        dead = false;
                        reversing = false;
                        deadCounter = 0;
                    }
                }
            break;

            // Forward movement
            case MovementDir.FORWARD:

                // Moving forward
                if (distCounter < movementTime && !reversing)
                {
                    transform.Translate(new Vector3(0, 0, movementSpeed) * Time.deltaTime);
                    distCounter += Time.deltaTime;
                }
                else if (distCounter > movementTime && !reversing)
                {
                    dead = true;
                }

                // Moving Backward
                if (distCounter > 0 && reversing)
                {
                    transform.Translate(new Vector3(0, 0, -movementSpeed) * Time.deltaTime);
                    distCounter -= Time.deltaTime;
                }
                else if (distCounter < movementTime && reversing)
                {
                    dead = true;
                    distCounter = 0;
                }

                // Stationary
                if (dead && !reversing)
                {
                    if (deadCounter < deadTime)
                    {
                        deadCounter += Time.deltaTime;
                    }
                    else
                    {
                        dead = false;
                        reversing = true;
                        deadCounter = 0;
                    }
                }
                else if (dead && reversing)
                {
                    if (deadCounter < deadTime)
                    {
                        deadCounter += Time.deltaTime;
                    }
                    else
                    {
                        dead = false;
                        reversing = false;
                        deadCounter = 0;
                    }
                }
                break;
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.transform.SetParent(gameObject.transform);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.transform.SetParent(null);
        }
    }
}

public enum MovementDir
{
    UP,
    RIGHT,
    FORWARD
}