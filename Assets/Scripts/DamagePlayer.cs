﻿/*-------------------------------------------------*/
/*                DamagePlayer.cs                  */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePlayer : MonoBehaviour
{
    /* Public Variables */
    public GameObject explosionSmall;   // Reference to the small explosion particle effect
    public GameObject explosionBig;     // Reference to the big explosion particle effect
    public int damageValue = 1;         // How much damage each bullet will do

    /* Private Variables */
    private HealthUI healthUI;          // The health UI in the scene

    // Use this for initialization
    void Start()
    {
        // Find the health UI in the scene
        healthUI = FindObjectOfType<HealthUI>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    /* Function for when a bullet hits the player */
    void OnTriggerEnter(Collider collider)
    {
        // Make the gun bouncing sound
        healthUI.GetComponent<AudioSource>().PlayOneShot(healthUI.destroyBullet, 0.2f);

        // Checking if the collider has the Player tag
        if (collider.gameObject.tag == "Player")
        {
            // Create a damage burst
            Instantiate(explosionBig, transform.position, transform.rotation);

            // Decrease the player GameObject's health by 1
            collider.gameObject.GetComponent<PlayerStats>().pCurrentHealth -= damageValue;

            // Destroy the bullet
            Destroy(gameObject);
        }
        else if (collider.gameObject.tag == "Enemy")
        {
            // Create a damage burst
            Instantiate(explosionBig, transform.position, transform.rotation);

            // Decrease the player GameObject's health by 1
            collider.gameObject.GetComponent<EnemyStats>().eCurrentHealth -= damageValue;

            // Destroy the bullet
            Destroy(gameObject);
        }
        else if (collider.gameObject.name == "Shield")
        {
            // Add a hit to the shield
            collider.gameObject.GetComponent<Shield>().hits += 1;

            // Create a damage burst
            Instantiate(explosionBig, transform.position, transform.rotation);

            // Destroy the bullet
            Destroy(gameObject);
        }
        else
        {
            // Create a damage burst
            Instantiate(explosionSmall, transform.position, transform.rotation);

            // Destroy the bullet
            Destroy(gameObject);
        }
    }
}
