﻿/*-------------------------------------------------*/
/*                 Interactible.cs                 */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactible : MonoBehaviour
{
    /* Public Variables */
    public float radius = 3.0f;             // How close the player needs to be to interact with the GameObject
    public Transform interactionTransform;

    /// <summary>
    /// Function to be overriden later
    /// </summary>
    public virtual void Interact()
    {
        Debug.Log("Interacting with " + transform.name);
    }

    /// <summary>
    /// Visualise the radius
    /// </summary>
    void OnDrawGizmos()
    {
        // If there is no interaction transform assigned
        if (interactionTransform == null)
        {
            // Assign a transform
            interactionTransform = transform;
        }

        // Draw the colour
        Gizmos.color = Color.yellow;

        // Draw a wire sphere
        Gizmos.DrawWireSphere(interactionTransform.position, radius);
    }
}
