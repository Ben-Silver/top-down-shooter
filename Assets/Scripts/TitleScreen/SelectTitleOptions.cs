﻿/*-------------------------------------------------*/
/*              SelectTitleOptions.cs              */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectTitleOptions : MonoBehaviour
{
    /* Public Variables */
    public GameObject titlePanel;               // The title in the scene
    public GameObject settingsPanel;            // The settings menu in the scene
    public int index = 0;                       // The current index of the selected option
    public AudioClip selectSound;               // The sound to play when scrolling through the options
    public AudioClip confirmSound;              // The sound to play when confirming an option
    public AudioClip denySound;                 // The sound to play when being denied
    public string newGameSceneName;             // The name of the scene to load when new game is selected
    public GameObject errorPanel;               // The error message that displays when a save file already exists
    public SelectableOption[] options;          // The array of options available

    /* Private Variables */
    private FileSystem fileSystem;              // The FileSystem in the scene
    private LevelChanger levelChanger;          // The LevelChanger in the scene
    private bool leftAxesInUse = false;         // True when the left axes is being used
    private bool rightAxesInUse = false;        // True when the right axes is being used
    private bool confirmAxesInUse = false;      // True when the confirm button is being pressed

    // Use this for initialization
    void Start()
    {
        // Find the FileSystem in the scene
        fileSystem = FindObjectOfType<FileSystem>();

        // Load the data from the FileSystem
        if (FileSystem.SaveExists())
        {
            fileSystem.LoadData();
        }

        // Find the level changer in the scene
        levelChanger = FindObjectOfType<LevelChanger>();

        // Reset index
        index = 0;

        // Deselect all options
        DeselectAll();

        // Select the option at index
        options[index].selected = true;
    }
	
	// Update is called once per frame
	void Update()
    {
        // Check to see if the title panel is currently active
        if (titlePanel.activeInHierarchy)
        {
            // Update the selection
            UpdateOptions();

            // Update the confirmation input
            SelectOptions();
        }
    }

    /// <summary>
    /// Function to update the title options
    /// </summary>
    void UpdateOptions()
    {
        // Select the option at index
        options[index].selected = true;

        // Make sure the index doesn't exceed the upper bounds of the array
        if (index >= options.Length)
        {
            // Make the select sound
            GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);

            // Reset index
            index = 0;

            // Deselect all options
            DeselectAll();
        }

        // Make sure the index doesn't fall below the lower bounds of the array
        if (index < 0)
        {
            // Make the select sound
            GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);

            // Reset index
            index = options.Length - 1;

            // Deselect all options
            DeselectAll();
        }

        // When the player presses the right arrow key
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetAxisRaw("DPadHorizontal") > 0 || Input.GetAxisRaw("Horizontal") > 0)
        {
            // Prevent scrolling multiple times in one button press
            if (!rightAxesInUse)
            {
                if (index < options.Length - 1)
                {
                    // Mark the axes as used
                    rightAxesInUse = true;

                    // Make the select sound
                    GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);

                    // Increment index
                    index += 1;

                    // Deselect all options
                    DeselectAll();
                }
            }
        }
        else
        {
            // Mark the axes as unused
            rightAxesInUse = false;
        }

        // When the player presses the left arrow key
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetAxisRaw("DPadHorizontal") < 0 || Input.GetAxisRaw("Horizontal") < 0)
        {
            // Prevent scrolling multiple times in one button press
            if (!leftAxesInUse)
            {
                if (index > 0)
                {
                    // Mark the axes as used
                    leftAxesInUse = true;

                    // Make the select sound
                    GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);

                    // Decrement index
                    index -= 1;

                    // Deselect all options
                    DeselectAll();
                }
            }
        }
        else
        {
            // Mark the axes as unused
            leftAxesInUse = false;
        }
    }

    /// <summary>
    /// Function to enter a selected option
    /// </summary>
    void SelectOptions()
    {
        // If the confirm button is pressed
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetAxisRaw("Submit") > 0)
        {
            // Get the value of index
            switch (index)
            {
                // When index is 0
                case 0:

                    // Prevent scrolling multiple times in one button press
                    if (!confirmAxesInUse)
                    {
                        // Mark the axes as used
                        confirmAxesInUse = true;

                        // Check to make sure the save exists
                        if (FileSystem.SaveExists())
                        {
                            // Load the save file
                            fileSystem.LoadData();

                            // Make the confirm sound
                            GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);

                            // Open up the level select screen
                            levelChanger.FadeToLevel("LevelSelection");

                            // Open up the level select screen 
                            Debug.Log("Loading save from " + Application.dataPath + FileSystem.fileName + FileSystem.fileExe);
                        }
                        else
                        {
                            // Make the deny sound
                            GetComponent<AudioSource>().PlayOneShot(denySound, 0.6f);

                            // Open up the level select screen 
                            Debug.Log("Failed to load " + Application.dataPath + FileSystem.fileName + FileSystem.fileExe);
                        }
                    }

                break;

                // When index is 1
                case 1:

                    // Prevent scrolling multiple times in one button press
                    if (!confirmAxesInUse)
                    {
                        // Mark the axes as used
                        confirmAxesInUse = true;

                        // Check to see if a save already exists
                        if (!FileSystem.SaveExists() || Input.GetKey(KeyCode.LeftShift))
                        {
                            // Erase the save file
                            FileSystem.instance.EraseSave();

                            // Make the confirm sound
                            GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);

                            // Make it so level 1 is unlocked
                            //FileSystem.instance.SetSwitch(0, true);
                            //FileSystem.instance.SetSwitch(12, true);
                            FileSystem.instance.AddGameSwitches();

                            // Automatically create save file
                            fileSystem.SaveData();

                            // Open up the level select screen
                            levelChanger.FadeToLevel(newGameSceneName);

                            Debug.Log("Starting a New Game");
                        }
                        else
                        {
                            // Make the deny sound
                            GetComponent<AudioSource>().PlayOneShot(denySound, 0.6f);

                            // Show the error panel
                            errorPanel.SetActive(true);

                            Debug.Log("Cannot start a new game, save already exists");
                        }
                    }

                break;

                // When index is 2
                case 2:

                    // Prevent scrolling multiple times in one button press
                    if (!confirmAxesInUse)
                    {
                        // Mark the axes as used
                        confirmAxesInUse = true;

                        // Make the confirm sound
                        GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);

                        // Hide the current GUI
                        titlePanel.SetActive(false);

                        // Show the settings GUI
                        settingsPanel.SetActive(true);

                        // Allow settings input
                        SettingsMenu.visible = true;

                        Debug.Log("Settings");
                    }

                break;
            }
        }
        else
        {
            // Hide the error panel
            errorPanel.SetActive(false);

            // Mark the axes as used
            confirmAxesInUse = false;
        }
    }

    /// <summary>
    /// Function to deselect all options
    /// </summary>
    public void DeselectAll()
    {
        // Loop through the array
        for (int i = 0; i < options.Length; i++)
        {
            // Deselect all options
            options[i].selected = false;
        }
    }
}
