﻿/*-------------------------------------------------*/
/*                   HealthUI.cs                   */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour
{
    /* Public Variables */
    public PlayerStats playerStats;                 // The player in the scene
    public AudioClip destroyBullet;                 // The bullet hitting SE
    public Image[] healthHearts;                    // Contains the player's health hearts in the scene
    
	// Use this for initialization
	void Start()
    {
		
	}
	
	// Update is called once per frame
	void Update()
    {
        // Loop through the current health
        for (int i = 0; i < playerStats.pCurrentHealth; i++)
        {
            // Check the value of the player's health against the total lenght of the hearts array
            if (playerStats.pCurrentHealth < healthHearts.Length)
            {
                // Disable any hearts that are of an index greater than the remaining health
                healthHearts[playerStats.pCurrentHealth].enabled = false;
            }
            else
            {
                // Enable all the hearts that are within this range
                healthHearts[i].enabled = true;
            }
        }
	}
}
