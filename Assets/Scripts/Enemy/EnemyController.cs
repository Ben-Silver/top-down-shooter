﻿/*-------------------------------------------------*/
/*               EnemyController.cs                */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    /* Public Variables */
    public NavMeshAgent agent;                      // Reference to the NavMeshAgent component on the gameObject
    public Gun gun;                                 // Referencing the gun component
    public float lookRadius = 10.0f;                // The FOV of the Enemy

    /* Private Variables */
    private float reloadTime;                       // Time it takes for gun to reload

    /* Variables */
    Transform target;                               // The player's transform
    BoxCollider fieldOfViewBox;
    bool spottedPlayer = false;
    
	// Use this for initialization
	void Start()
    {
        // Add an FOV box collider to the enemy
        //CreateFOV();

        // Set the target to the player in the PlayerManager
        target = PlayerManager.instance.player.transform;

        // Get the NavMeshAgent component on the gameObject
        agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update()
    {
        // Move towards a viable target
        MoveTowardsTarget();

        // Update the time until reloading
        UpdateReloadTime();
    }

    /// <summary>
    /// Function to move the enemy towards a target
    /// </summary>
    public void MoveTowardsTarget()
    {
        // Local variable to hold the distance from the player to the enemy
        float distance = Vector3.Distance(target.position, transform.position);

        // Check to see if the player is within the lookRadius
        if (distance <= lookRadius)
        {
            // Navigate towards the player
            agent.SetDestination(target.position);

            // Attack the target
            gun.Shoot();
            
            // Check to see if the distance between the player and the enemy is less than the stopping distance
            if (distance <= agent.stoppingDistance)
            {
                // Face the target
                FaceTarget();
            }
        }
    }

    /// <summary>
    /// Function to make the enemy face a target
    /// </summary>
    void FaceTarget()
    {
        // Get the normalised direction for the gameObject to face
        Vector3 direction = (target.position - transform.position).normalized;

        // Set the look rotation
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));

        // Set the rotation to face the player
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5.0f);
    }

    /// <summary>
    /// Function to update the reload time
    /// </summary>
    void UpdateReloadTime()
    {
        // Check the current reload time
        if (reloadTime >= 2.0f)
        {
            // Set the current rounds to 1
            gun.rounds = 1;

            // Reset the reload time
            reloadTime = 0.0f;
        }

        // Increase the reload time
        reloadTime += Time.deltaTime;
    }

    /// <summary>
    /// Function to create the fov box collider
    /// </summary>
    void CreateFOV()
    {
        // Add a box collider to the enemy
        fieldOfViewBox = gameObject.AddComponent<BoxCollider>();

        // Set the center of the box collider
        fieldOfViewBox.center = new Vector3(0, 0, lookRadius / 2);

        // Set the size of the box collider
        fieldOfViewBox.size = new Vector3(2, 1, lookRadius);

        // Set the box to be a trigger
        fieldOfViewBox.isTrigger = true;
    }

    /// <summary>
    /// Function to visualise the lookRadius in the editor
    /// </summary>
    void OnDrawGizmosSelected()
    {
        // Set the colour to red
        Gizmos.color = Color.red;

        // Draw a red wire sphere with the radius of the lookRadius
        Gizmos.DrawWireSphere(transform.position, lookRadius);
        //Gizmos.DrawWireCube(new Vector3(transform.position.x, transform.position.y, transform.position.z + (lookRadius / 2)), new Vector3(2, 1, lookRadius));
    }
}
