﻿/*-------------------------------------------------*/
/*                AIStateMachine.cs                */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIStateMachine : MonoBehaviour
{
    /* Public Variables */
    public enum AIStates
    {
        PHASEONE,       // The primary state for the boss
        PHASETWO,       // The secondary state for the boss
        PHASETHREE,     // The final state for the boss
        HEAL,           // When the boss' health percentage drops below a certain level
        WIN,            // When the boss has defeated the player
        DEFEATED        // When the boss has been defeated  by the player
    }
    public static AIStates currentState;      // The current state of the AI
    public static bool phaseOne;              // True when the AI is in phase 1 of battle
    public static bool phaseTwo;              // True when the AI is in phase 2 of battle
    public static bool phaseThree;            // True when the AI is in phase 3 of battle

    /* Private Variables */
    private BossTankController controller;    // 

    // Use this for initialization
    void Start()
    {
        // 
        controller = gameObject.GetComponent<BossTankController>();

        // Set phaseOne to true in the state machine
        phaseOne = true;

        // Set the current state to PHASEONE
        currentState = AIStates.PHASEONE;
    }

    // Update is called once per frame
    void Update()
    {
        // Checking through all current states
        switch (currentState)
        {
            // When the current state is PHASEONE
            case (AIStates.PHASEONE):

                // Tell the controller to move towards the player
                controller.FindPlayer();

            break;

            // When the current state is PHASETWO
            case (AIStates.PHASETWO):

                // Tell the controller to go to execute phase 2 commands
                controller.PhaseTwo();

            break;

            // When the current state is PHASETHREE
            case (AIStates.PHASETHREE):

                // Tell the controller to go to execute phase 3 commands
                controller.PhaseThree();

            break;

            // When the current state is HEAL
            case (AIStates.HEAL):

                // Tell the controller to flee to a healing spot
                controller.FindHealingSpot();
            break;

            // When the current state is DEFEATED
            case (AIStates.DEFEATED):

            break;
        }
    }
}
