﻿/*-------------------------------------------------*/
/*                  EnemyCount.cs                  */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCount : MonoBehaviour
{
    /* Public Variables */
    public Text targetText;         // Text displaying the targetNumber in the UI
    public GameObject[] enemy;      // Array of enemy GameObjects on each level
    public int enemyNumber;         // An integer holding the total count of enemies
    public int targetNumber;        // An integer holding the required number of kills for the goal door to open

    /* Private Variables */
    private LevelClear levelClear;  // The LevelClear in the scene

	// Use this for initialization
	void Start()
    {
        // Find the LevelClear in the scene
        levelClear = FindObjectOfType<LevelClear>();

        // Display the target number of kills in the UI
        targetText.text = "Target: " + targetNumber.ToString() + "Kills";

        // Find all enemies in the scene and add them into the array
        enemy = GameObject.FindGameObjectsWithTag("Enemy");

        // Set the enemyNumber equal to the number of enemies in the array
        enemyNumber = enemy.Length;
	}
	
	// Update is called once per frame
	void Update()
    {
        // Update the target number of kills in the UI
        targetText.text = "Target: " + targetNumber.ToString() + " Kills";

        // If there are no enemies left
        if (targetNumber <= 0)
        {
            // Open the goal door
            FindObjectOfType<LevelDoor>().SetDoorOpen(true);
        }
	}
}
