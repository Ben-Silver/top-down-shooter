﻿/*-------------------------------------------------*/
/*                  EnemyStats.cs                  */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour
{
    /* Public Variables */
    public int eCurrentHealth;          // Current health of the enemy
    public int eMaximumHealth;          // Maximum health of the enemy

    /* Private Variables */
    private EnemyCount eCount;          // Reference to EnemyCount in the scene

    // Use this for initialization
    void Start()
    {
        // Set the current to the maximum
        eCurrentHealth = eMaximumHealth;

        // Find the EnemyCount in the scene
        eCount = FindObjectOfType<EnemyCount>();
	}
	
	// Update is called once per frame
	void Update()
    {
        // Check to see if the enemy health has exceeded the maximum value
        if (eCurrentHealth >= eMaximumHealth)
        {
            // Set the enemy health to the maximum value
            eCurrentHealth = eMaximumHealth;
        }

        // Check to see if the enemy's health has reached 0
        if (eCurrentHealth <= 0)
        {
            // Make the enemy invisible
            gameObject.SetActive(false);

            // Reduce the number of enemies in the scene
            eCount.enemyNumber -= 1;

            // Reduce the target number of kills to complete the level
            eCount.targetNumber -= eCount.targetNumber > 0 ? 1 : 0;

            // Destroy the GameObject
            Destroy(gameObject);
        }
	}
}
