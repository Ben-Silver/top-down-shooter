﻿/*-------------------------------------------------*/
/*                    BossUI.cs                    */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossUI : MonoBehaviour
{
    /* Public Variables */
    public EnemyStats enemyTank;                // Reference to EnemyStats.cs
    public Image enemyHealthBar;                // The health bar UI component in the scene
    public float enemyHealthBarValue;           // Responsible for controlling the health bar component
    public PlayerStats playerTank;              // Reference to PlayerStats.cs
    public Text playerHealthText;               // The player's health
    
	// Use this for initialization
	void Start()
    {
        // Set the health bar value to be equal to the current health of the enemy
        enemyHealthBarValue = enemyTank.eCurrentHealth;
    }
	
	// Update is called once per frame
	void Update()
    {
        // Get the health percentage of the boss and assign it to enemyHealthBarValue
        enemyHealthBarValue = (float)enemyTank.eCurrentHealth / enemyTank.eMaximumHealth;

        // Set the fill amount of the health bar according to the value of enemyHealthBarValue
        enemyHealthBar.fillAmount = enemyHealthBarValue;

        // Set the text of playerHealthText to the current health of the player
        playerHealthText.text = "Player Health: " + playerTank.pCurrentHealth.ToString();
	}
}
