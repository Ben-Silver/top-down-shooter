﻿/*-------------------------------------------------*/
/*              BossTankController.cs              */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyStats))]
public class BossTankController : MonoBehaviour
{
    /* Public Variables */
    public EnemyStats eStats;                       // Reference to EnemyStats.cs
    public EnemyShoot eShoot;                       // Reference to EnemyShoot.cs
    //public Unit unit;                               // Reference to A* pathfinding algorithm followed by the BossAI
    public Transform player;                        // The transform of the player for A* pathfinding
    public Transform healingSpot;                   // The transform of the healing spot for A* pathfinding
    public GameObject bullet;                       // Bullet prefab
    public GameObject laser;                        // Laser prefab
    public GameObject centerPoint;                  // Center Point GameObject

	// Use this for initialization
	void Start()
    {

	}
	
	// Update is called once per frame
	void Update()
    {
        // Check the boss' stats to see if the AI state must be changed
        CheckBossStats();

        // Debug purposes
        Debug.Log(AIStateMachine.currentState.ToString());
        Debug.Log(EnemyFuzzyState.currentFuzzyState.ToString() + " health");
    }

    /* Function to check the boss health to determine the phase of the battle */
    public void CheckBossStats()
    {
        // Check the current fuzzy states
        switch (EnemyFuzzyState.currentFuzzyState)
        {
            // When the current health is HIGH
            case (EnemyFuzzyState.FuzzyStates.HIGH):

                // Check to see if the current phase is phase one
                if (AIStateMachine.phaseOne)
                {
                    // Change the current state to PHASEONE
                    AIStateMachine.currentState = AIStateMachine.AIStates.PHASEONE;
                }

                // Check to see if the current phase is phase two
                if (AIStateMachine.phaseTwo)
                {
                    // Change the current state to PHASETWO
                    AIStateMachine.currentState = AIStateMachine.AIStates.PHASETWO;
                }

                // Check to see if the current phase is phase three
                if (AIStateMachine.phaseThree)
                {
                    // Change the current state to PHASETHREE
                    AIStateMachine.currentState = AIStateMachine.AIStates.PHASETHREE;
                }

            break;

            // When the current health is MEDIUM
            case (EnemyFuzzyState.FuzzyStates.MEDIUM):

                // Check to see if the current phase is phase one
                if (AIStateMachine.phaseOne)
                {
                    // Change the current state to PHASEONE
                    AIStateMachine.currentState = AIStateMachine.AIStates.PHASEONE;
                }

                // Check to see if the current phase is phase two
                if (AIStateMachine.phaseTwo)
                {
                    // Change the current state to PHASETWO
                    AIStateMachine.currentState = AIStateMachine.AIStates.PHASETWO;
                }

                // Check to see if the current phase is phase three
                if (AIStateMachine.phaseThree)
                {
                    // Change the current state to PHASETHREE
                    AIStateMachine.currentState = AIStateMachine.AIStates.PHASETHREE;
                }

            break;

            // When the current health is HIGH
            case (EnemyFuzzyState.FuzzyStates.LOW):
                // Change the current state to HEAL
                AIStateMachine.currentState = AIStateMachine.AIStates.HEAL;
            break;
        }

        // When the current health is equal to or below 0
        if (eStats.eCurrentHealth <= 0)
        {
            // Change the current state to DEFEATED
            AIStateMachine.currentState = AIStateMachine.AIStates.DEFEATED;
        }
    }

    /* Function to move the boss towards the player */
    public void FindPlayer()
    {
        // Set the target to the player
        //unit.target = player;

        // Shoot
        eShoot.Shoot();
    }

    /* Function to move the boss towards a healing spot */
    public void FindHealingSpot()
    {
        // Set the target to the healing point
        //unit.target = healingSpot;

        // Increase the AI's speed
        //unit.speed = 10;
    }

    /* Function to move the boss to the centre of the map */
    public void FindCenterPoint()
    {
        // Set the turn distance to 0
        //unit.turnDst = 0;

        // Set the stopping distance to 0 (boss will stop abruptly)
        //unit.stoppingDst = 0;

        //unit.turnSpeed = 10;

        // Move towards the center
        //unit.target = centerPoint.transform;
    }

    /* Function to control what the AI does in Phase 2 */
    public void PhaseTwo()
    {
        // Increase the AI's speed
        //unit.speed = 10;

        // Increase the AI's bullet speed (bullets also do more damage)
        eShoot.bulletSpeed = 12;

        // Move towards the player
        FindPlayer();
    }

    /* Function to control what the AI does in Phase 3 */
    public void PhaseThree()
    {
        // Go to the center of the map
        FindCenterPoint();

        // Start spinning
        transform.Rotate(new Vector3(0, 3));

        // 
        eShoot.cooldownTimeMax = 0.085f;

        // Revert bullet speed
        eShoot.bulletSpeed = 6;

        // Continuously fire bullets
        eShoot.ShootLaser();
    }
}
