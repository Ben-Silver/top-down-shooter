﻿/*-------------------------------------------------*/
/*                  EnemyShoot.cs                  */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{
    /* Public Variables */
    public AudioClip gunShot;                       // The bullet SE
    public AudioClip laserShot;                     // The laser SE
    public GameObject bullet;                       // Bullet prefab
    public GameObject laser;                        // Laser prefab
    public Transform spawn;                         // The transform of each bullet instance
    public float bulletSpeed = 6;                   // The speed of the bullet
    public float cooldownTime;                      // The time taken to reload
    public bool firedGun = false;                   // Whether the gun has been fired
    public float cooldownTimeMax = 0.5f;            // Maximum cooldown time
    
    // Use this for initialization
    void Start()
    {
		
	}
	
	// Update is called once per frame
	void Update()
    {
        // Function call to shoot
        Shoot();

        // Check to see if the gun has been fired
        if (firedGun)
        {
            // Start the cooldown
            cooldownTime -= Time.deltaTime;
        }

        // Check to see if the cooldown period has ended
        if (cooldownTime <= 0)
        {
            // Reset firedGun
            firedGun = false;

            // Reset cooldown time
            cooldownTime = cooldownTimeMax;
        }
    }

    /// <summary>
    /// Function to fire a bullet
    /// </summary>
    public void Shoot()
    {
        // Check to see if the cooldown period is active
        if (!firedGun)// && EnemyFuzzyState.currentFuzzyState == EnemyFuzzyState.FuzzyStates.LOW)
        {
            // Create a new bullet instance
            GameObject bulletFire = Instantiate(bullet, spawn.transform.position, spawn.transform.rotation);

            // Set the velocity of the bullet
            bulletFire.GetComponent<Rigidbody>().velocity = transform.forward * bulletSpeed;

            // Make the gun shooting sound
            GetComponent<AudioSource>().PlayOneShot(gunShot, 1.0f);

            // Enemy has now fired their gun
            firedGun = true;
        }
    }

    /// <summary>
    /// Function to fire a laser beam
    /// </summary>
    public void ShootLaser()
    {
        if (!firedGun)
        {
            // Create a new bullet instance
            GameObject bulletFire = Instantiate(laser, spawn.transform.position, transform.rotation);

            // Set the velocity of the bullet
            bulletFire.GetComponent<Rigidbody>().velocity = transform.forward * 32.0f;

            // Make the gun shooting sound
            GetComponent<AudioSource>().PlayOneShot(laserShot, 1.0f);

            // Enemy has now fired their gun
            firedGun = true;
        }
    }

    /* Function to shoot a bullet rapidly */
    public void ShootContinuous()
    {
        // 
        Ray ray = new Ray(spawn.position, spawn.forward);

        // 
        RaycastHit hit;

        // 
        float shotDistance = 20.0f;

        // If the player crosses the enemy's line of site
        if (Physics.Raycast(ray, out hit, shotDistance))
        {
            // Set shot distance to be equal to distance of hit
            shotDistance = hit.distance;
        }

        // Create a new bullet instance
        GameObject bulletFire = Instantiate(bullet, spawn.transform.position, spawn.transform.rotation);

        // Set the velocity of the bullet
        bulletFire.GetComponent<Rigidbody>().velocity = transform.forward * bulletSpeed;
    }
}
