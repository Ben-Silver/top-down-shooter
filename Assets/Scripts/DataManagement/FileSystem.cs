﻿/*-------------------------------------------------*/
/*                   FileSystem.cs                   */
/*-------------------------------------------------*/

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class FileSystem : MonoBehaviour
{
    #region Singleton

    // The current instance of this object
    public static FileSystem instance;

    // Before Start()
    void Awake()
    {
        // Check to see if there's already an instance of this object in the scene
        if (instance)
        {
            // Warning message
            Debug.LogWarning("More than 1 instance of SaveLoad found!");

            // Destroy the extra instance
            Destroy(gameObject);
        }
        else
        {
            // Make this the current instance
            instance = this;

            // Make sure this object remains
            DontDestroyOnLoad(gameObject);

            // Check to see if there is a save file to load
            if (SaveExists())
            {
                // Load the save file
                LoadData();
            }
        }
    }

    #endregion

    /* Public Variables */
    public Data fileSystem = new Data();            // The data we're reading from/writing to
    public static string fileName = "/save";        // The name we give the file
    public static string fileExe = ".tdata";        // The extension we give the file
    public static string versionNumber = "0.0";     // The version number of the game

    /* Game Switches */
    public static List<GameSwitch> gameSwitches;           // All the game event flags

    /*public static bool[] levelsUnlockedWorld1 = new bool[6];
    public static bool[] levelsCompletedWorld1 = new bool[6];

    public static bool[] levelsUnlockedWorld2 = new bool[6];
    public static bool[] levelsCompletedWorld2 = new bool[6];

    public static bool[] levelsUnlockedWorld3 = new bool[6];
    public static bool[] levelsCompletedWorld3 = new bool[6];

    public static bool[] levelsUnlockedWorld4 = new bool[6];
    public static bool[] levelsCompletedWorld4 = new bool[6];

    public static bool[] levelsUnlockedWorld5 = new bool[6];
    public static bool[] levelsCompletedWorld5 = new bool[6];

    public static bool[] levelsUnlockedWorld6 = new bool[6];
    public static bool[] levelsCompletedWorld6 = new bool[6];*/

    /// <summary>
    /// Function to save data to a file
    /// </summary>
    public void SaveData()
    {
        // Create a new file or open it if it already exists, using the filename and extension
        Stream stream = File.Open(Application.dataPath + fileName + fileExe, FileMode.OpenOrCreate);

        // Create a new binary formatter
        BinaryFormatter bf = new BinaryFormatter();

        // Save the version number
        SaveVersionData();

        // Save level data
        SaveLevelData();

        // Save the game switches
        fileSystem.gameSwitches = gameSwitches;

        // Serialise the stream and data
        bf.Serialize(stream, fileSystem);

        // Close the file
        stream.Close();
    }

    /// <summary>
    /// Function to load data from a file
    /// </summary>
    public void LoadData()
    {
        // Create a new file or open it if it already exists, using the filename and extension
        Stream stream = File.Open(Application.dataPath + fileName + fileExe, FileMode.Open);

        // Create a new binary formatter
        BinaryFormatter bf = new BinaryFormatter();

        // Deserialise the file
        fileSystem = (Data)bf.Deserialize(stream);

        // Close the file
        stream.Close();

        // Load the level data
        LoadLevelData();

        // Load the game switches
        gameSwitches = fileSystem.gameSwitches;
    }

    /// <summary>
    /// Function to erase the save file
    /// </summary>
    public void EraseSave()
    {
        // Check to make sure there is a save file first
        if (SaveExists())
        {
            // Delete the save file
            File.Delete(Application.dataPath + fileName + fileExe);

            // Delete the save meta file
            File.Delete(Application.dataPath + fileName + fileExe + ".meta");

            // Reset Flags
            /*levelsUnlockedWorld1 = new bool[6];
            levelsCompletedWorld1 = new bool[6];
            levelsUnlockedWorld2 = new bool[6];
            levelsCompletedWorld2 = new bool[6];
            levelsUnlockedWorld3 = new bool[6];
            levelsCompletedWorld3 = new bool[6];
            levelsUnlockedWorld4 = new bool[6];
            levelsCompletedWorld4 = new bool[6];
            levelsUnlockedWorld5 = new bool[6];
            levelsCompletedWorld5 = new bool[6];
            levelsUnlockedWorld6 = new bool[6];
            levelsCompletedWorld6 = new bool[6];*/

            // Add game switches
            AddGameSwitches();
        }
    }

    /// <summary>
    /// Function to determine if a save file exists already (for starting a new game)
    /// </summary>
    /// <returns>True if a file exists</returns>
    public static bool SaveExists()
    {
        // Return true or false depending on whether a save exists
        return File.Exists(Application.dataPath + fileName + fileExe);
    }

    /// <summary>
    /// Function to determine whether a level exists in the game directory
    /// </summary>
    /// <param name="levelName">The name of the level we want to find</param>
    /// <returns>True if the level exists</returns>
    public static bool LevelExists(string levelName)
    {
        // Return true or false depending on whether a level file exists
        return File.Exists(Application.dataPath + "/Scenes/" + levelName + ".unity");
    }

    /// <summary>
    /// Function to save the version number
    /// </summary>
    void SaveVersionData()
    {
        // Set the number in the save to the number in the game data
        fileSystem.versionNumber = versionNumber;
    }

    /// <summary>
    /// Function to load the version number
    /// </summary>
    void LoadVersionData()
    {
        // Set the number in the game data to the number in the save
        versionNumber = fileSystem.versionNumber;
    }

    /// <summary>
    /// Function to write the level data to the fileSystem
    /// </summary>
    void SaveLevelData()
    {
        // Set the database levels unlocked to the current levels unlocked
        /*fileSystem.levelsUnlockedWorld1 = levelsUnlockedWorld1;
        fileSystem.levelsUnlockedWorld2 = levelsUnlockedWorld2;
        fileSystem.levelsUnlockedWorld3 = levelsUnlockedWorld3;
        fileSystem.levelsUnlockedWorld4 = levelsUnlockedWorld4;
        fileSystem.levelsUnlockedWorld5 = levelsUnlockedWorld5;
        fileSystem.levelsUnlockedWorld6 = levelsUnlockedWorld6;*/

        // Set the database levels completed to the current levels completed
        /*fileSystem.levelsCompletedWorld1 = levelsCompletedWorld1;
        fileSystem.levelsCompletedWorld2 = levelsCompletedWorld2;
        fileSystem.levelsCompletedWorld3 = levelsCompletedWorld3;
        fileSystem.levelsCompletedWorld4 = levelsCompletedWorld4;
        fileSystem.levelsCompletedWorld5 = levelsCompletedWorld5;
        fileSystem.levelsCompletedWorld6 = levelsCompletedWorld6;*/
    }

    /// <summary>
    /// Function to read level data from the fileSystem
    /// </summary>
    void LoadLevelData()
    {
        // Set the current levels unlocked to the database levels unlocked
        /*levelsUnlockedWorld1 = fileSystem.levelsUnlockedWorld1;
        levelsUnlockedWorld2 = fileSystem.levelsUnlockedWorld2;
        levelsUnlockedWorld3 = fileSystem.levelsUnlockedWorld3;
        levelsUnlockedWorld4 = fileSystem.levelsUnlockedWorld4;
        levelsUnlockedWorld5 = fileSystem.levelsUnlockedWorld5;
        levelsUnlockedWorld6 = fileSystem.levelsUnlockedWorld6;*/

        // Set the current levels completed to the database levels completed
        /*levelsCompletedWorld1 = fileSystem.levelsCompletedWorld1;
        levelsCompletedWorld2 = fileSystem.levelsCompletedWorld2;
        levelsCompletedWorld3 = fileSystem.levelsCompletedWorld3;
        levelsCompletedWorld4 = fileSystem.levelsCompletedWorld4;
        levelsCompletedWorld5 = fileSystem.levelsCompletedWorld5;
        levelsCompletedWorld6 = fileSystem.levelsCompletedWorld6;*/
    }

    /// <summary>
    /// Function to make old version saves compatible with new versions
    /// </summary>
    void TransferData()
    {

    }

    /// <summary>
    /// Function to set the boolean value of a GameSwitch
    /// </summary>
    /// <param name="sIndex">The GameSwitch index number</param>
    /// <param name="sValue">The updated boolean value</param>
    public void SetSwitch(int sIndex, bool sValue)
    {
        // Set the switch in the corresponding index to the value passed in
        gameSwitches[sIndex].switchValue = sValue;

        Debug.Log("Setting Switch: " + gameSwitches[sIndex].switchName + "\nTo be: " + sValue);
    }

    /// <summary>
    /// Function to return the boolean value of a GameSwitch
    /// </summary>
    /// <param name="sIndex">The index of the GameSwitch we want to return</param>
    /// <returns></returns>
    public bool GetSwitch(int sIndex)
    {
        // Get the boolean of the GameSwitch at the specified index and assign it to a local variable
        bool sValue = gameSwitches[sIndex].switchValue;

        Debug.Log("Game Switch: " + gameSwitches[sIndex].switchName + "\nHas a value of: " + sValue);

        // Return the value of the sValue
        return sValue;
    }

    /// <summary>
    /// Creates a new GameSwitch (mainly for testing)
    /// </summary>
    /// <param name="sName">The name of the new GameSwitch</param>
    /// <param name="sValue">The boolean value of the new GameSwitch</param>
    public void AddSwitch(string sName, bool sValue)
    {
        // Add the new GameSwitch to the gameSwitches list
        gameSwitches.Add(new GameSwitch(sName, sValue));
    }

    /// <summary>
    /// Function to fill the game with GameSwitches
    /// </summary>
    public void AddGameSwitches()
    {
        /* Make a new list of GameSwitches */
        gameSwitches = new List<GameSwitch>();

        /* Worlds unlocked */
        AddSwitch("World 1 Unlocked", true);                    // 0
        AddSwitch("World 2 Unlocked", false);                   // 1
        AddSwitch("World 3 Unlocked", false);                   // 2
        AddSwitch("World 4 Unlocked", false);                   // 3
        AddSwitch("World 5 Unlocked", false);                   // 4
        AddSwitch("World 6 Unlocked", false);                   // 5

        /* Worlds completed */
        AddSwitch("World 1 Complete", false);                   // 6
        AddSwitch("World 2 Complete", false);                   // 7
        AddSwitch("World 3 Complete", false);                   // 8
        AddSwitch("World 4 Complete", false);                   // 9
        AddSwitch("World 5 Complete", false);                   // 10
        AddSwitch("World 6 Complete", false);                   // 11

        /* Levels unlocked */
        AddSwitch("World 1 Level 1 Unlocked", true);            // 12
        AddSwitch("World 1 Level 2 Unlocked", false);           // 13
        AddSwitch("World 1 Level 3 Unlocked", false);           // 14
        AddSwitch("World 1 Level 4 Unlocked", false);           // 15
        AddSwitch("World 1 Level 5 Unlocked", false);           // 16
        AddSwitch("World 1 Level 6 Unlocked", false);           // 17

        AddSwitch("World 2 Level 1 Unlocked", false);           // 18
        AddSwitch("World 2 Level 2 Unlocked", false);           // 19
        AddSwitch("World 2 Level 3 Unlocked", false);           // 20
        AddSwitch("World 2 Level 4 Unlocked", false);           // 21
        AddSwitch("World 2 Level 5 Unlocked", false);           // 22
        AddSwitch("World 2 Level 6 Unlocked", false);           // 23

        AddSwitch("World 3 Level 1 Unlocked", false);           // 24
        AddSwitch("World 3 Level 2 Unlocked", false);           // 25
        AddSwitch("World 3 Level 3 Unlocked", false);           // 26
        AddSwitch("World 3 Level 4 Unlocked", false);           // 27
        AddSwitch("World 3 Level 5 Unlocked", false);           // 28
        AddSwitch("World 3 Level 6 Unlocked", false);           // 29

        AddSwitch("World 4 Level 1 Unlocked", false);           // 30
        AddSwitch("World 4 Level 2 Unlocked", false);           // 31
        AddSwitch("World 4 Level 3 Unlocked", false);           // 32
        AddSwitch("World 4 Level 4 Unlocked", false);           // 33
        AddSwitch("World 4 Level 5 Unlocked", false);           // 34
        AddSwitch("World 4 Level 6 Unlocked", false);           // 35

        AddSwitch("World 5 Level 1 Unlocked", false);           // 36
        AddSwitch("World 5 Level 2 Unlocked", false);           // 37
        AddSwitch("World 5 Level 3 Unlocked", false);           // 38
        AddSwitch("World 5 Level 4 Unlocked", false);           // 39
        AddSwitch("World 5 Level 5 Unlocked", false);           // 40
        AddSwitch("World 5 Level 6 Unlocked", false);           // 41

        AddSwitch("World 6 Level 1 Unlocked", false);           // 42
        AddSwitch("World 6 Level 2 Unlocked", false);           // 43
        AddSwitch("World 6 Level 3 Unlocked", false);           // 44
        AddSwitch("World 6 Level 4 Unlocked", false);           // 45
        AddSwitch("World 6 Level 5 Unlocked", false);           // 46
        AddSwitch("World 6 Level 6 Unlocked", false);           // 47

        /* Levels completed */
        AddSwitch("World 1 Level 1 Complete", false);           // 48
        AddSwitch("World 1 Level 2 Complete", false);           // 49
        AddSwitch("World 1 Level 3 Complete", false);           // 50
        AddSwitch("World 1 Level 4 Complete", false);           // 51
        AddSwitch("World 1 Level 5 Complete", false);           // 52
        AddSwitch("World 1 Level 6 Complete", false);           // 53

        AddSwitch("World 2 Level 1 Complete", false);           // 54
        AddSwitch("World 2 Level 2 Complete", false);           // 55
        AddSwitch("World 2 Level 3 Complete", false);           // 56
        AddSwitch("World 2 Level 4 Complete", false);           // 57
        AddSwitch("World 2 Level 5 Complete", false);           // 58
        AddSwitch("World 2 Level 6 Complete", false);           // 59

        AddSwitch("World 3 Level 1 Complete", false);           // 60
        AddSwitch("World 3 Level 2 Complete", false);           // 61
        AddSwitch("World 3 Level 3 Complete", false);           // 62
        AddSwitch("World 3 Level 4 Complete", false);           // 63
        AddSwitch("World 3 Level 5 Complete", false);           // 64
        AddSwitch("World 3 Level 6 Complete", false);           // 65

        AddSwitch("World 4 Level 1 Complete", false);           // 66
        AddSwitch("World 4 Level 2 Complete", false);           // 67
        AddSwitch("World 4 Level 3 Complete", false);           // 68
        AddSwitch("World 4 Level 4 Complete", false);           // 69
        AddSwitch("World 4 Level 5 Complete", false);           // 70
        AddSwitch("World 4 Level 6 Complete", false);           // 71

        AddSwitch("World 5 Level 1 Complete", false);           // 72
        AddSwitch("World 5 Level 2 Complete", false);           // 73
        AddSwitch("World 5 Level 3 Complete", false);           // 74
        AddSwitch("World 5 Level 4 Complete", false);           // 75
        AddSwitch("World 5 Level 5 Complete", false);           // 76
        AddSwitch("World 5 Level 6 Complete", false);           // 77

        AddSwitch("World 6 Level 1 Complete", false);           // 78
        AddSwitch("World 6 Level 2 Complete", false);           // 79
        AddSwitch("World 6 Level 3 Complete", false);           // 80
        AddSwitch("World 6 Level 4 Complete", false);           // 81
        AddSwitch("World 6 Level 5 Complete", false);           // 82
        AddSwitch("World 6 Level 6 Complete", false);           // 83
    }
}
