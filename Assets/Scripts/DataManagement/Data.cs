﻿/*-------------------------------------------------*/
/*                   SaveLoad.cs                   */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Data
{
    /* Version Number */
    public string versionNumber = "0.0";            // The version number of the save

    /* Game Switches */
    public List<GameSwitch> gameSwitches;           // All the game event flags

    /*public bool worldUnlocked1;                     // World 1 unlock flag
    public bool worldUnlocked2;                     // World 2 unlock flag
    public bool worldUnlocked3;                     // World 3 unlock flag
    public bool worldUnlocked4;                     // World 4 unlock flag
    public bool worldUnlocked5;                     // World 5 unlock flag
    public bool worldUnlocked6;                     // World 6 unlock flag

    public bool[] levelsUnlockedWorld1 = new bool[6];
    public bool[] levelsCompletedWorld1 = new bool[6];

    public bool[] levelsUnlockedWorld2 = new bool[6];
    public bool[] levelsCompletedWorld2 = new bool[6];

    public bool[] levelsUnlockedWorld3 = new bool[6];
    public bool[] levelsCompletedWorld3 = new bool[6];

    public bool[] levelsUnlockedWorld4 = new bool[6];
    public bool[] levelsCompletedWorld4 = new bool[6];

    public bool[] levelsUnlockedWorld5 = new bool[6];
    public bool[] levelsCompletedWorld5 = new bool[6];

    public bool[] levelsUnlockedWorld6 = new bool[6];
    public bool[] levelsCompletedWorld6 = new bool[6];*/
}
