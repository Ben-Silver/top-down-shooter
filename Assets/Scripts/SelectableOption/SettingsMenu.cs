﻿/*-------------------------------------------------*/
/*                 SettingsMenu.cs                 */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SettingsMenu : MonoBehaviour
{
    /* Public Variables */
    public int index = 0;                       // The current selection index
    public static bool visible;                 // true if the menu is visible
    public AudioClip selectSound;               // The sound to play when scrolling through the options
    public AudioClip confirmSound;              // The sound to play when confirming an option
    public AudioClip denySound;                 // The sound to play when being denied
    public GameObject titlePanel;               // The title in the scene
    public GameObject settingsPanel;            // The settings menu in the scene
    public AudioMixer audioMixer;               // The audio mixer that controls the main volume
    public float audioVolume = 0;               // The current master volume
    public Slider audioSlider;                  // The audio slider in the scene
    public SelectableOption[] options;          // The array of options available

    /* Private Variables */
    private FileSystem fileSystem;              // The FileSystem in the scene
    private bool upAxesInUse = false;           // True when the up axes is being used
    private bool downAxesInUse = false;         // True when the down axes is being used
    private bool confirmAxesInUse = false;      // True when the confirm button is being pressed

    // Use this for initialization
    void Start()
    {
        // Find the FileSystem in the scene
        fileSystem = FindObjectOfType<FileSystem>();

        // Deselect all panels
        DeselectAll();

        // Select the first option
        options[index].selected = true;
	}
	
	// Update is called once per frame
	void Update()
    {
        // If the settings menu is visible in the scene
        if (visible)
        {
            // Check to see if the player is closing the menu
            CloseMenu();

            // Check to see if the player is selecting an option
            UpdateOptions();

            // Check to see if the player wants to choose an option
            SelectOptions();
        }
	}

    /// <summary>
    /// Function to update the settings options
    /// </summary>
    void UpdateOptions()
    {
        // Select the option at index
        options[index].selected = true;

        // When the player presses the up arrow key
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetAxisRaw("DPadVertical") > 0 || Input.GetAxisRaw("Vertical") > 0)
        {
            // Prevent scrolling multiple times in one button press
            if (!upAxesInUse)
            {
                if (index > 0)
                {
                    // Mark the axes as used
                    upAxesInUse = true;

                    // Make the select sound
                    GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);

                    // Increment index
                    index -= 1;

                    // Deselect all options
                    DeselectAll();
                }
            }
        }
        else
        {
            // Mark the axes as unused
            upAxesInUse = false;
        }

        // When the player presses the down arrow key
        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetAxisRaw("DPadVertical") < 0 || Input.GetAxisRaw("Vertical") < 0)
        {
            // Prevent scrolling multiple times in one button press
            if (!downAxesInUse)
            {
                if (index < options.Length - 1)
                {
                    // Mark the axes as used
                    downAxesInUse = true;

                    // Make the select sound
                    GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);

                    // Decrement index
                    index += 1;

                    // Deselect all options
                    DeselectAll();
                }
            }
        }
        else
        {
            // Mark the axes as unused
            downAxesInUse = false;
        }
    }

    /// <summary>
    /// Function for when an option is selected
    /// </summary>
    void SelectOptions()
    {
        // Clamp the volume
        CorrectVolume(-80, 0);

        // Receive input for volume
        if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) > 0 && options[0].selected)
        {
            // Check to make sure the volume stays within the boundaries
            if (audioVolume <= 0 && audioVolume >= -80)
            {
                // Change the audio according to the input
                audioVolume += Input.GetAxisRaw("Horizontal");

                // Change the slider value according to the input
                audioSlider.value = audioVolume;

                // Set the value of the mixer according to the current volume
                SetVolume(audioVolume);
            }
        }

        // If the confirm button is pressed
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetAxisRaw("Submit") > 0)
        {
            // Get the value of index
            switch (index)
            {
                // When index is 1
                case 1:

                    // Prevent scrolling multiple times in one button press
                    if (!confirmAxesInUse)
                    {
                        // Mark the axes as used
                        confirmAxesInUse = true;

                        // Check to make sure the save exists
                        if (FileSystem.SaveExists())
                        {
                            Debug.Log("Deleting save at " + Application.dataPath + FileSystem.fileName + FileSystem.fileExe);

                            // Load the save file
                            FileSystem.instance.EraseSave();

                            // Make the confirm sound
                            GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);
                        }
                        else
                        {
                            // Make the deny sound
                            GetComponent<AudioSource>().PlayOneShot(denySound, 0.6f);

                            // Open up the level select screen 
                            Debug.Log("No save file found");
                        }
                    }

                    break;
            }
        }
        else
        {
            // Mark the axes as unused
            confirmAxesInUse = false;
        }
    }

    /// <summary>
    /// Function to exit the settings menu
    /// </summary>
    public void CloseMenu()
    {
        // Check to make sure the Cancel button is being pressed
        if (Input.GetAxisRaw("Cancel") > 0)
        {
            // Make the title visible again
            titlePanel.SetActive(true);

            // Make the menu invisible
            settingsPanel.SetActive(false);

            // Break the update loop
            visible = false;
        }
    }

    /// <summary>
    /// Function to clamp the volume between two values
    /// </summary>
    /// <param name="minimum">The minumum value</param>
    /// <param name="maximum">The maximum value</param>
    public void CorrectVolume(float minimum, float maximum)
    {
        // Check the maximum value
        if (audioVolume > maximum)
        {
            // Set it to the maximum if breached
            audioVolume = maximum;
        }

        // Check the minimum value
        if (audioVolume < minimum)
        {
            // Set it to the minimum if breached
            audioVolume = minimum;
        }
    }

    /// <summary>
    /// Function to set the master volume
    /// </summary>
    /// <param name="volume">The value we're passing in</param>
    public void SetVolume(float volume)
    {
        // Set the mixer volume
        audioMixer.SetFloat("volume", volume);

        // Reflect the change in the audioVolume
        audioVolume = volume;
    }

    /// <summary>
    /// Function to deselect all options
    /// </summary>
    void DeselectAll()
    {
        // Loop through the array
        for (int i = 0; i < options.Length; i++)
        {
            // Deselect all the panels
            options[i].selected = false;
        }
    }
}
