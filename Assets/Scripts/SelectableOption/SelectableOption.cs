﻿/*-------------------------------------------------*/
/*                 LevelSelect.cs                  */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectableOption : MonoBehaviour
{
    /* Public Variables */
    public bool selected;                       // True if this is the selected option
    public Image panelImage;                    // The panel sprite
    public Sprite[] panelSprites;               // Array of sprites

    // Use this for initialization
    void Start()
    {
        // Get the component on the gameObject
        panelImage = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update()
    {
        // If this option is currently selected
        if (selected)
        {
            // Visualise that it is selected
            panelImage.sprite = panelSprites[1];
        }
        else
        {
            // Visualise that it isn't selected
            panelImage.sprite = panelSprites[0];
        }
	}
}
