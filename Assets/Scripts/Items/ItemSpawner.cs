﻿/*-------------------------------------------------*/
/*                 ItemSpawner.cs                  */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    /* Public Variables */
    public GameObject[] itemPrefab;             // An array of the item prefabs that can spawn
    public Vector3[] spawnLocations;            // An array of possible locations for the item to spawn
    public bool itemSpawned;                    // Prevents more than 1 item spawning at a time

    // Use this for initialization
    void Start()
    {
		
	}
	
	// Update is called once per frame
	void Update()
    {
        // Try to spawn an item
        SpawnItem();
	}

    /// <summary>
    /// Function to spawn an item
    /// </summary>
    void SpawnItem()
    {
        // Check to see if an item has spawned yet
        if (!itemSpawned)
        {
            // Assign a random number between the first and last element in the itemPrefab array
            int randItem = Random.Range(0, itemPrefab.Length);

            // Assign a random number between the first and last element in the spawnLocations array 
            int randPosition = Random.Range(0, spawnLocations.Length);
        
            // Create a new random item at a random location
            GameObject newItem = Instantiate(itemPrefab[randItem], spawnLocations[randPosition], new Quaternion(0, 0, 0, 0));

            // Prevent a new item from spawning
            itemSpawned = true;
        }

    }
}
