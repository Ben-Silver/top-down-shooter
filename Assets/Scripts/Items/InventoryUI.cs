﻿/*-------------------------------------------------*/
/*                 InventoryUI.cs                  */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour
{
    /* Variables */
    Inventory inventory;                    // Reference to the Inventory class
    
    /* Public Variables */
    public InventorySlot weaponSlot;        // The weapon slot
    public InventorySlot shieldSlot;        // The shield slot

	// Use this for initialization
	void Start()
    {
        // Set inventory to a new instance of the inventory
        inventory = Inventory.instance;

        // Callback to update the UI
        inventory.onItemChangedCallback += UpdateUI;
    }
	
	// Update is called once per frame
	void Update()
    {

    }

    /// <summary>
    /// Function to update the UI
    /// </summary>
    void UpdateUI()
    {
        // Debug message
        Debug.Log("Updating UI");

        // Check to see if the player has a weapon
        if (inventory.weapon != null)
        {
            // Make the weapon visible on the UI
            weaponSlot.AddItem(inventory.weapon);
        }
        else
        {
            // Clear the weapon from the UI
            weaponSlot.ClearSlot();
        }

        // Check to see if the player has a shield
        if (inventory.shield != null)
        {
            // Make the shield visible on the UI
            shieldSlot.AddItem(inventory.shield);
        }
        else
        {
            // Clear the shield from the UI
            shieldSlot.ClearSlot();
        }
    }
}
