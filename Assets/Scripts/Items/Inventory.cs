﻿/*-------------------------------------------------*/
/*                  Inventory.cs                   */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    #region Singleton

    public static Inventory instance;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than 1 instance of Inventory found!");
            return;
        }

        instance = this;
    }

    #endregion

    public delegate void OnItemChanged();       // OnItemChanged delegate
    public OnItemChanged onItemChangedCallback; // Callback to OnItemChanged delegate

    /* Public Variables */
    public int weaponSpace = 1;                 // The number of weapon slots available
    public int shieldSpace = 1;                 // The number of shield slots available
    public Item weapon;                         // The weapon currently being held
    public Item shield;                         // The shield currently being held

    /// <summary>
    /// Function to add an item to the inventory
    /// </summary>
    /// <param name="item">The item we're adding</param>
    public bool AddItem(Item item)
    {
        // Check to see if the item is a weapon or a shield and determine if there's space for it
        if (item.isWeapon && weapon == null)
        {
            // Add the item to the weapon slot
            weapon = item;

            // Make sure the callback exists
            if (onItemChangedCallback != null)
            {
                // Update the UI
                onItemChangedCallback.Invoke();
            }

            // The item has been picked up
            return true;
        }
        else if (!item.isWeapon && shield == null)
        {
            // Add the item to the shield slot
            shield = item;

            // Make sure the callback exists
            if (onItemChangedCallback != null)
            {
                // Update the UI
                onItemChangedCallback.Invoke();
            }

            // The item has been picked up
            return true;
        }
        else
        {
            // Debug message
            Debug.Log("No room at the inn");

            // The item has not been picked up
            return false;
        }        
    }

    /// <summary>
    /// Function to remove an item from the inventory
    /// </summary>
    /// <param name="item">The item we're removing</param>
    public void RemoveItem(Item item)
    {
        // Remove the item
        //items.Remove(item);

        // Check to see if the item is a weapon or shield
        if (item.isWeapon)
        {
            // Remove the item from the weapon slot
            weapon = null;
        }
        else
        {
            // Remove the item from the shield slot
            shield = null;
        }

        // Make sure the callback exists
        if (onItemChangedCallback != null)
        {
            // Update the UI
            onItemChangedCallback.Invoke();
        }
    }
}
