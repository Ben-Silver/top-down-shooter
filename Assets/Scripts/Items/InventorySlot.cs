﻿/*-------------------------------------------------*/
/*                InventorySlot.cs                 */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    /* Variables */
    Item item;                  // The item currently occupying the slot

    /* Public Variables */
    public enum SlotType
    {
        Weapon,
        Shield
    }     // The types of item slot
    public SlotType slotType;   // The type of item this slot can hold
    public Image icon;          // The icon of the item currently occupying the slot
    public GameObject button;   // The button for the item slot
    public Text itemName;       // The name of the item in the slot

    /// <summary>
    /// Function to add an item to the slot
    /// </summary>
    /// <param name="newItem">The item being added</param>
    public void AddItem(Item newItem)
    {
        // Set the current item to the item being added
        item = newItem;

        // Set the icon of the new item
        icon.sprite = item.icon;

        // Enable the icon
        icon.enabled = true;

        // Make the button visible
        button.SetActive(true);

        // Set the item name on the UI
        itemName.text = item.name;

        // Make the item name visible
        itemName.gameObject.SetActive(true);
    }

    /// <summary>
    /// Function to clear the item slot
    /// </summary>
    public void ClearSlot()
    {
        // Remove the item from the slot
        item = null;

        // Set the icon to nothing
        icon.sprite = null;

        // Disable the icon
        icon.enabled = false;

        // Make the button invisible
        button.SetActive(false);

        // Make the item name invisible
        itemName.gameObject.SetActive(false);

        // Set the item name on the UI to nothing
        itemName.text = "";
    }
}
