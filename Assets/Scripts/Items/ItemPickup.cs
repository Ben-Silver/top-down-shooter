﻿/*-------------------------------------------------*/
/*                  ItemPickup.cs                  */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : Interactible
{
    /* Public Variables */
    public Item item;               // The item file
    public AudioClip pickupSound;   // The sound played when the item is picked up
    
    /// <summary>
    /// Overide function for interacting
    /// </summary>
    public override void Interact()
    {
        // Function call from base class
        base.Interact();

        // Pick up the item
        PickUp();
    }

    /// <summary>
    /// Function to pick up the item
    /// </summary>
    void PickUp()
    {
        // Debug message
        Debug.Log("Picking up " + item.name);


        // Add the item to the inventory
        bool pickedUpItem = Inventory.instance.AddItem(item);

        // Destroy the gameObject if the item was picked up
        if (pickedUpItem)
        {
            // Make the pickup sound
            Inventory.instance.GetComponent<AudioSource>().PlayOneShot(pickupSound, 0.5f);

            // Destroy the gameObject
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Function for when the player enters the item's proximity
    /// </summary>
    /// <param name="collider">The collider on the object that collided with this gameObject</param>
    void OnTriggerEnter(Collider collider)
    {
        // Check to make sure it was the player that entered the item's proximity
        if (collider.gameObject.tag == "Player")
        {
            // Function call
            Interact();
        }
    }
}
