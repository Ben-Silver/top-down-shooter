﻿/*-------------------------------------------------*/
/*                RotatingLights.cs                */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingLights : MonoBehaviour
{
    /* Public Variables */
    public GameObject lights1;                      
    public GameObject lights2;
    public float rotationSpeed = 15.0f;

    // Use this for initialization
    void Start()
    {
		
	}
	
	// Update is called once per frame
	void Update()
    {
        // Check to make sure lights1 isn't empty
        if (lights1 != null)
        {
            // Rotate the lights clockwise
            lights1.transform.Rotate(0.0f, rotationSpeed * Time.deltaTime, 0.0f);
        }

        // Check to make sure lights2 isn't empty
        if (lights2 != null)
        {
            // Rotate the lights anti-clockwise
            lights2.transform.Rotate(0.0f, -rotationSpeed * Time.deltaTime, 0.0f);
        }
    }
}
