﻿/*-------------------------------------------------*/
/*                GameOverScreen.cs                */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScreen : MonoBehaviour
{
    /* Public Variables */
    public int index = 0;                       // The current index of the selected option
    public AudioClip selectSound;               // The sound to play when scrolling through the options
    public AudioClip confirmSound;              // The sound to play when confirming an option
    public GameObject[] options;                // The array of options
    public Sprite[] optionBox;                  // The possible boxes

    /* Private Variables */
    private LevelChanger levelChanger;          // The LevelChanger in the scene
    private bool leftAxesInUse = false;         // True when the left axes is being used
    private bool rightAxesInUse = false;        // True when the right axes is being used
    private bool confirmAxesInUse = false;      // True when the confirm button is being pressed

    // Use this for initialization
    void Start()
    {
        // Find the level changer in the scene
        levelChanger = FindObjectOfType<LevelChanger>();

        // Reset index
        index = 0;

        // Deselect all options
        DeselectAll();

        // Select the option at index
        options[index].GetComponent<Image>().sprite = optionBox[1];
    }
	
	// Update is called once per frame
	void Update()
    {
        // Update the selection
        UpdateOptions();

        // Update the confirmation input
        SelectOptions();
    }

    /// <summary>
    /// Function to update the title options
    /// </summary>
    void UpdateOptions()
    {
        // Make sure the index doesn't exceed the upper bounds of the array
        if (index >= options.Length)
        {
            // Make the select sound
            GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);

            // Reset index
            index = 0;

            // Deselect all options
            DeselectAll();

            // Select the option at index
            options[index].GetComponent<Image>().sprite = optionBox[1];
        }

        // Make sure the index doesn't fall below the lower bounds of the array
        if (index < 0)
        {
            // Make the select sound
            GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);

            // Reset index
            index = options.Length - 1;

            // Deselect all options
            DeselectAll();

            // Select the option at index
            options[index].GetComponent<Image>().sprite = optionBox[1];
        }

        // When the player presses the right arrow key
        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetAxisRaw("DPadVertical") > 0 || Input.GetAxisRaw("Vertical") > 0)
        {
            // Prevent scrolling multiple times in one button press
            if (!rightAxesInUse)
            {
                // Mark the axes as used
                rightAxesInUse = true;

                // Make the select sound
                GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);

                // Increment index
                index += 1;

                // Deselect all options
                DeselectAll();

                // Select the option at index
                options[index].GetComponent<Image>().sprite = optionBox[1];
            }
        }
        else
        {
            // Mark the axes as unused
            rightAxesInUse = false;
        }

        // When the player presses the left arrow key
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetAxisRaw("DPadVertical") < 0 || Input.GetAxisRaw("Vertical") < 0)
        {
            // Prevent scrolling multiple times in one button press
            if (!leftAxesInUse)
            {
                // Mark the axes as used
                leftAxesInUse = true;

                // Make the select sound
                GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);

                // Decrement index
                index -= 1;

                // Deselect all options
                DeselectAll();

                // Select the option at index
                options[index].GetComponent<Image>().sprite = optionBox[1];
            }
        }
        else
        {
            // Mark the axes as unused
            leftAxesInUse = false;
        }
    }

    /// <summary>
    /// Function to enter a selected option
    /// </summary>
    void SelectOptions()
    {
        // If the confirm button is pressed
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetAxisRaw("Submit") > 0)
        {
            // Get the value of index
            switch (index)
            {
                // When index is 0
                case 0:

                    // Prevent scrolling multiple times in one button press
                    if (!confirmAxesInUse)
                    {
                        // Mark the axes as used
                        confirmAxesInUse = true;

                        // Make the confirm sound
                        GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);

                        // Open up the level selection screen
                        levelChanger.FadeToLevel("LevelSelection");
                        Debug.Log("To Level Selection");

                        // Open up the level select screen 
                        Debug.Log("Continue");
                    }

                    break;

                // When index is 1
                case 1:

                    // Prevent scrolling multiple times in one button press
                    if (!confirmAxesInUse)
                    {
                        // Mark the axes as used
                        confirmAxesInUse = true;

                        // Make the confirm sound
                        GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);

                        // Open up the title screen
                        levelChanger.FadeToLevel("Title");
                        Debug.Log("To title");
                    }

                    break;

                // When index is 2
                case 2:

                    // Prevent scrolling multiple times in one button press
                    if (!confirmAxesInUse)
                    {
                        // Mark the axes as used
                        confirmAxesInUse = true;

                        // Make the confirm sound
                        GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);

                        // Hide the current GUI

                        // Unhide the settings GUI
                        Debug.Log("Settings");
                    }

                    break;
            }
        }
        else
        {
            // Mark the axes as used
            confirmAxesInUse = false;
        }
    }

    /// <summary>
    /// Function to deselect all options
    /// </summary>
    public void DeselectAll()
    {
        // Loop through the array
        for (int i = 0; i < options.Length; i++)
        {
            // Deselect all options
            options[i].GetComponent<Image>().sprite = optionBox[0];
        }
    }
}
