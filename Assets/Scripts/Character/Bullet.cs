﻿/*-------------------------------------------------*/
/*                    Bullet.cs                    */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    /* Private Variables */
    private float lifeTime = 0.5f;
    private Material material;
    private Color originalCol;
    private float fadePercent;
    private float disposeTime;
    private bool fading;

    // Use this for initialization
    void Start()
    {
        // Get find the Renderer in the scene
        material = GetComponent<Renderer>().material;

        // Set originalCol to the current colour of the material
        originalCol = material.color;

        // Set disposeTime to the current delta time + the value of lifeTime
        disposeTime = Time.time + lifeTime;

        // Function call to Fade()
        StartCoroutine("Fade");
	}
	
    // Function to make the bullet fade
	IEnumerator Fade()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.2f);

            if (fading)
            {
                fadePercent += Time.deltaTime;
                material.color = Color.Lerp(originalCol, Color.clear, fadePercent);

                if (fadePercent >= 1)
                {
                    Destroy(gameObject);
                } 
            }
            else
            {
                if (Time.time > disposeTime)
                {
                    fading = true;
                }
            }
        }
    }

    /* When the bullet hits the ground */
    void OnTriggerEnter(Collider c)
    {
        // Check to see if the collider has the Ground tag
        if (c.tag == "Ground")
        {
            // 
            GetComponent<Rigidbody>().Sleep();
        }
    }
}
