﻿/*-------------------------------------------------*/
/*                PlayerHealthUI.cs                */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthUI : MonoBehaviour
{
    /* Public Variables */
    public PlayerStats pStats;          // Reference to the Player's stats
    public Text pHealthValue;           // Player's health value in the UI
    
    // Use this for initialization
    void Start()
    {
		
	}
	
	// Update is called once per frame
	void Update()
    {
        // Set the text of playerHealthText to the current health of the player
        pHealthValue.text = "Player Health: " + pStats.pCurrentHealth.ToString();
    }
}
