﻿/*-------------------------------------------------*/
/*               PlayerController.cs               */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    /* Variables */
    public Inventory inventory;                    // Reference to the Inventory class

    /* Public Variables */
    public bool laserGunEnabled = false;            // Determines whether the player can use the Laser Gun or not
    public bool laserShieldEnabled = false;         // Determines whether the player can use the Laser Shield or not
    public Gun gun;                                 // Reference to the gun component
    public Shield shield;                           // Reference to the shield component

    /* Private Variables */
    private float moveSpeed = 5.5f;                 // The speed at which the player moves
    private float turnSpeed = 120.0f;               // The speed at which the player turns
    private float reloadTime;                       // Time it takes for gun to reload

    // Use this for initialization
    void Start()
    {

    }
	
	// Update is called once per frame
	void Update()
    {
        // Control the player
        MovePlayer();

        // Fire the gun
        FireGun();

        // Enable the shield
        DeployShield();

        // Update the current reload time
        UpdateReloadTime();

        // Check to see if the player wants to equip a weapon from their inventory
        UseWeapon();

        // Check to see if the player wants to equip a shield from their inventory
        UseShield();
    }

    /// <summary>
    /// Function to fire the gun
    /// </summary>
    void FireGun()
    {
        // Gets input from the space bar or mouse to call the Shoot() function
        if (Input.GetButton("Shoot") && !laserGunEnabled)
        {
            // Shoot a bullet once
            gun.Shoot();
        }
        else if (Input.GetButtonDown("Shoot") && laserGunEnabled)
        {
            // Shoot a laser once
            gun.ShootLaser();
        }
    }

    /// <summary>
    /// Function to enable the shield
    /// </summary>
    void DeployShield()
    {
        // Check to see if the shield needs to be enabled
        if (laserShieldEnabled)
        {
            // Make the shield gameObject visible
            shield.gameObject.SetActive(true);
        }

        // Check to see if the shield needs to be disabled
        if (!laserShieldEnabled)
        {
            // Make the shield gameObject invisible
            shield.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Function to update the reload time
    /// </summary>
    void UpdateReloadTime()
    {
        // Check the current reload time
        if (reloadTime >= 2.0f && !laserGunEnabled)
        {
            // Set the current rounds to 5
            gun.rounds = 5;

            // Reset the reload time
            reloadTime = 0.0f;
        }

        // Check to see if the gun is out of rounds while the laser is enabled
        if (gun.uses > 10 && laserGunEnabled)
        {
            // Remove the weapon from the player
            inventory.RemoveItem(inventory.weapon);

            // Disable the laser
            laserGunEnabled = false;

            // Set the current rounds to 0
            gun.rounds = 0;

            // Set the current uses to 0
            gun.uses = 0;

            // Reset the reload time
            reloadTime = 0.0f;
        }

        // Increase the reload time
        reloadTime += Time.deltaTime;
    }

    /// <summary>
    /// Function to control the player
    /// </summary>
    void MovePlayer()
    {
        if (!PauseMenu.gameIsPaused)
        {
            // Change the moveSpeed based on whether the Run button is being pressed or not
            moveSpeed = (Input.GetButton("Run")) ? 8.0f : 5.5f;

            // Check the Vertical input
            if (Mathf.Abs(Input.GetAxisRaw("Vertical")) > 0)
            {
                // Move the player forward or backward depending on the input
                transform.Translate(Vector3.forward * moveSpeed * Input.GetAxisRaw("Vertical") * Time.deltaTime);
            }

            // Check the Horizontal input
            if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) > 0)
            {
                // Move the player left or right depending on the input
                transform.Translate(Vector3.right * moveSpeed * Input.GetAxisRaw("Horizontal") * Time.deltaTime);
            }

            // Check the Turn input for the keyboard
            if (Mathf.Abs(Input.GetAxisRaw("Turn")) > 0)
            {
                // Temporary variable
                float axis = Input.GetAxisRaw("Turn");

                // Rotate the player left or right depending on the input
                transform.Rotate(Vector3.up * turnSpeed * axis * Time.deltaTime);
            }

            // Check the Turn input for the gamepad
            if (Mathf.Abs(Input.GetAxisRaw("RightStickHorizontal")) > 0)
            {
                // Temporary variable
                float axis = Input.GetAxisRaw("RightStickHorizontal");

                // Rotate the player left or right depending on the input
                transform.Rotate(Vector3.up * turnSpeed * axis * Time.deltaTime);
            }
        }
    }

    /// <summary>
    /// Function to equip a weapon
    /// </summary>
    void UseWeapon()
    {
        // Check to see if the Q key is being pressed
        if (Input.GetButtonDown("EquipWeapon"))
        {
            // Check to see if the player has the Laser Gun in their inventory
            if (inventory.weapon != null && inventory.weapon.name == "Laser Gun")
            {
                // Enable the Laser Gun
                laserGunEnabled = true;
            }
        }
    }

    /// <summary>
    /// Function to equip a shield
    /// </summary>
    void UseShield()
    {
        // Check to see if the E key is being pressed
        if (Input.GetButtonDown("EquipShield"))
        {
            // Check to see if the player has the Laser Shield in their inventory
            if (inventory.shield != null && inventory.shield.name == "Laser Shield")
            {
                // Enable the Laser Shield
                laserShieldEnabled = true;
            }
        }
    }
}
