﻿/*-------------------------------------------------*/
/*                    Shield.cs                    */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    /* Public Variables */
    public PlayerController pC;             // The PlayerController component on the GameObject
    public Transform player;                // The player's transform
    public int uses = 5;                    // The number of uses the shield has before it breaks
    public int hits;                        // The number of hits the shield has taken

	// Use this for initialization
	void Start()
    {
        // Get the player's transform
        player = pC.transform; //FindObjectOfType<PlayerController>().transform;

        // Reset hits
        hits = 0;
    }
	
	// Update is called once per frame
	void Update()
    {
        // Keep the shield following the player
        transform.position = player.transform.position;

        // Animate the shield
        transform.Rotate(0, 15.0f * Time.deltaTime, 0);

        // Check to see if hits is greater than/equal to uses
        if (hits >= uses)
        {
            // Reset hits
            hits = 0;

            // Disable the shield
            pC.laserShieldEnabled = false;
        }
	}
}
