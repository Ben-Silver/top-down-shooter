﻿/*-------------------------------------------------*/
/*                  GameCamera.cs                  */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCamera : MonoBehaviour
{
    /* Public Variables */
    public float xModifier;                     // The mod value for the camera's X offset
    public float yModifier;                     // The mod value for the camera's Y offset
    public float zModifier;                     // The mod value for the camera's Z offset

    /* Private Variables */
    private Vector3 cameraTarget;
    private Transform target;

	// Use this for initialization
	void Start()
    {
        // Finds the player, which will have the "Player" tag
        target = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update()
    {
        // Keeps the camera tracking the player's position
        //cameraTarget = new Vector3(target.position.x, target.position.y + 16, target.position.z - 13.0f);
        cameraTarget = new Vector3(target.position.x + xModifier, target.position.y + yModifier, target.position.z + zModifier);
        transform.position = Vector3.Lerp(transform.position, cameraTarget, Time.deltaTime * 8);
	}
}
