﻿/*-------------------------------------------------*/
/*                     Gun.cs                      */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (AudioSource))]
public class Gun : MonoBehaviour
{
    /* Public Variables */
    public enum GunType
    {
        Normal,
        Auto,
        Laser
    };                          // All possible gun types
    public GunType gunType;                         // The current gun type
    public float rpm;                               // How many rounds can be fired in a minute
    public AudioClip gunShot;                       // The bullet SE
    public AudioClip laserShot;                     // The laser SE
    public Transform spawn;                         // The position of the bullet instances
    public GameObject shell;                        // Bullet prefab
    public GameObject laser;                        // Laser prefab
    public int rounds = 5;                          // Max number of rounds
    public int uses = 0;                            // Number of times the laser has been used

    /* Private Variables */
    private float secondsBetweenShots;              // 
    private float nextPossibleShootTime;            // 

    // Use this for initialization
    void Start()
    {
        secondsBetweenShots = 60 / rpm;
    }

    /// <summary>
    /// Function to fire a bullet
    /// </summary>
    public void Shoot()
    {
        // Check to see if the player can fire their gun
        if (CanShoot())
        {
            // Create a new bullet instance
            GameObject bulletFire = Instantiate(shell, spawn.transform.position, transform.rotation);

            // Set the velocity of the bullet
            bulletFire.GetComponent<Rigidbody>().velocity = transform.forward * 32.0f;
            
            // Set the next possible time to shoot
            nextPossibleShootTime = Time.time + secondsBetweenShots;

            // Make the gun shooting sound
            GetComponent<AudioSource>().PlayOneShot(gunShot, 1.0f);

            // Decrease the number of rounds left by 1
            rounds -= 1;
        }
    }

    /// <summary>
    /// Function to fire a laser beam
    /// </summary>
    public void ShootLaser()
    {
        // Check to see if the player can fire their gun
        if (CanShoot())
        {
            // Create a new bullet instance
            GameObject bulletFire = Instantiate(laser, spawn.transform.position, transform.rotation);

            // Set the velocity of the bullet
            bulletFire.GetComponent<Rigidbody>().velocity = transform.forward * 32.0f;

            // Set the next possible time to shoot
            nextPossibleShootTime = Time.time + secondsBetweenShots;

            // Make the gun shooting sound
            GetComponent<AudioSource>().PlayOneShot(laserShot, 0.2f);

            // Decrease the number of rounds left by 1
            uses += 1;
        }
    }

    /// <summary>
    /// Function for automatic fire
    /// </summary>
    public void ShootContinuous()
    {
        // Checking if the gun is automatic
        if (gunType == GunType.Auto)
        {
            // Function call
            Shoot();
        }
    }

    /// <summary>
    /// Function to determine whether the gun can be fired
    /// </summary>
    private bool CanShoot()
    {
        // Local variable
        bool canShoot = true;

        // Check to see if the current time is less than the next possible shoot time or if there are no rounds left in the gun
        if ((Time.time < nextPossibleShootTime) || (rounds < 1))
        {
            // Make it so the player cannot fire their gun
            canShoot = false;
        }

        // Return the value of canShoot
        return canShoot;
    }
}
