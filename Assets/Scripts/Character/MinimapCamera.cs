﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapCamera : MonoBehaviour
{
	/* Public Variables */
	public LevelInfo levelInfo;
	
	/* Private Variables */
	private Vector3 bottomLeftLimit;
	private Vector3 topRightLimit;
	private float halfWidth;
	private float halfHeight;

	// Use this for initialization
	void Start()
	{
		// Find the LevelInfo component in the scene
		levelInfo = FindObjectOfType<LevelInfo>();

		// Set the half height
		halfHeight = gameObject.GetComponent<Camera>().orthographicSize;
		
		// Set the half width
		halfWidth = halfHeight * gameObject.GetComponent<Camera>().aspect;

		// Get the upper and lower limits of the map
		bottomLeftLimit = -(levelInfo.mapDimensions + new Vector3(halfWidth, halfHeight, 0));
		topRightLimit = levelInfo.mapDimensions + new Vector3(-halfWidth, -halfHeight, 0);
	}
	
	// Update is called once per frame
	void Update()
	{
		// Ensure the camera stays within the bounds of the map
		transform.position = new Vector3(Mathf.Clamp(transform.position.x, bottomLeftLimit.x, topRightLimit.x), 
										 transform.position.y,
										 Mathf.Clamp(transform.position.z, bottomLeftLimit.z, topRightLimit.z));
	}
}
