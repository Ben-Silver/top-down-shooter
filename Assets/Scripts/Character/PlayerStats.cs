﻿/*-------------------------------------------------*/
/*                  PlayerStats.cs                 */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerStats : MonoBehaviour
{
    /* Public Variables */
    public int pCurrentHealth;          // Current health of the player
    public int pMaximumHealth;          // Maximum health of the player
    public string gameOverScene;        // The name of the scene to load when the player dies
    //public Scene gameOverScene;         // The scene to load when the player dies

    // Use this for initialization
    void Start()
    {
        // Set the current to the maximum
        pCurrentHealth = pMaximumHealth;
    }

    // Update is called once per frame
    void Update()
    {
        // Check to see if the player health has exceeded the maximum value
        if (pCurrentHealth >= pMaximumHealth)
        {
            // Set the player health to the maximum value
            pCurrentHealth = pMaximumHealth;
        }

        // Check to see if the players's health has reached 0
        if (pCurrentHealth <= 0)
        {
            // Make the enemy invisible
            gameObject.SetActive(false);

            // Destroy the GameObject
            Destroy(gameObject);

            // Reload the player
            SceneManager.LoadScene(gameOverScene); //"Scene1");
        }
    }
}
