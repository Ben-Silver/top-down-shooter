﻿/*-------------------------------------------------*/
/*                PlayerManager.cs                 */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    #region Singleton

    public static PlayerManager instance;

    void Awake()
    {
        instance = this;
    }

    #endregion

    /* Public Variables */
    public GameObject player;                   // The player gameObject in the scene
}
