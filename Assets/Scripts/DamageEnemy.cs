﻿/*-------------------------------------------------*/
/*                 DamageEnemy.cs                  */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageEnemy : MonoBehaviour
{
    /* Public Variables */
    public GameObject explosionSmall;   // Reference to the small explosion particle effect
    public GameObject explosionBig;     // Reference to the big explosion particle effect
    public int damageValue = 1;         // How much damage each bullet will do

    /* Private Variables */
    private HealthUI healthUI;          // The health UI in the scene

    // Use this for initialization
    void Start()
    {
        // Find the health UI in the scene
        healthUI = FindObjectOfType<HealthUI>();
    }

    // Update is called once per frame
    void Update()
    {
		
	}

    /* Function for when a bullet hits the enemy */
    void OnTriggerEnter(Collider collider)
    {
        // Checking if the collider has the Enemy tag
        if (collider.gameObject.tag == "Enemy")
        {
            // Make the gun bouncing sound
            healthUI.GetComponent<AudioSource>().PlayOneShot(healthUI.destroyBullet, 0.2f);

            // Destroy the bullet
            Destroy(gameObject);

            // Create a damage burst
            Instantiate(explosionBig, transform.position, transform.rotation);

            // Decrease the enemy GameObject's health by 5
            collider.gameObject.GetComponent<EnemyStats>().eCurrentHealth -= damageValue;
        }
        else
        {
            // Make the gun bouncing sound
            healthUI.GetComponent<AudioSource>().PlayOneShot(healthUI.destroyBullet, 0.2f);

            // Create a damage burst
            Instantiate(explosionSmall, transform.position, transform.rotation);

            // Destroy the bullet
            Destroy(gameObject);
        }
    }
}
