﻿/*-------------------------------------------------*/
/*                 HealingSpot.cs                  */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingSpot : MonoBehaviour
{
    /* Private Variables */
    private int positionsIndex;                     // Current index of the positions array
    private Vector3[] positions = new Vector3[4]    // Array of Vector3 positions for the healing spot to spawn
    {
        new Vector3(-17.0f, 0.5f, 17.0f),           // Top Left
        new Vector3(17.0f, 0.5f, 17.0f),            // Top Right
        new Vector3(-17.0f, 0.5f, -17.0f),          // Bottom Left
        new Vector3(17.0f, 0.5f, -17.0f)            // Bottom Right
    };

    // Use this for initialization
    void Start()
    {
        // Spawn the healing spot at a random position
        positionsIndex = Random.Range(0, positions.Length);
    }
	
	// Update is called once per frame
	void Update()
    {
        // Update the transform of the GameObject according to the value of positionsIndex
        transform.position = positions[positionsIndex];
	}

    /* When another object collides with this GameObject */
    void OnTriggerEnter(Collider collider)
    {
        // Check to see if the GameObject is the enemy
        if (collider.gameObject.tag == "Enemy")
        {
            // Restore 30 health to the enemy
            collider.GetComponent<EnemyStats>().eCurrentHealth += 30;

            // Check to see if the current enemy health is less than the maximum value
            if (collider.GetComponent<EnemyStats>().eCurrentHealth < collider.GetComponent<EnemyStats>().eMaximumHealth)
            {
                // Set the current health to the maximum
                collider.GetComponent<EnemyStats>().eCurrentHealth = collider.GetComponent<EnemyStats>().eMaximumHealth;
            }

            // Set the healing spot position to another random position
            positionsIndex = Random.Range(0, positions.Length);

            // Check to see what phase the AI State Machine is in
            if (AIStateMachine.phaseOne)
            {
                // Go to phase 2
                AIStateMachine.phaseOne = false;
                AIStateMachine.phaseTwo = true;

            }
            else if (AIStateMachine.phaseTwo)
            {
                // Go to phase 3
                //collider.transform.position = new Vector3(0, 0, 0);
                AIStateMachine.phaseTwo = false;
                AIStateMachine.phaseThree = true;
            }
        }

        // Check to see if the GameObject is the player
        if (collider.gameObject.tag == "Player")
        {
            // Restore 10 health to the player
            collider.GetComponent<PlayerStats>().pCurrentHealth += 10;
            
            // Check to see if the current player health is less than the maximum value
            if (collider.GetComponent<PlayerStats>().pCurrentHealth < collider.GetComponent<PlayerStats>().pMaximumHealth)
            {
                // Set the current health to the maximum
                collider.GetComponent<PlayerStats>().pCurrentHealth = collider.GetComponent<PlayerStats>().pMaximumHealth;
            }

            // Set the healing spot position to another random position
            positionsIndex = Random.Range(0, positions.Length);
        }

        // Output the new position to the console
        Debug.Log(transform.position.x + ", "
                + transform.position.y + ", "
                + transform.position.z);
    }
}
