﻿/*-------------------------------------------------*/
/*                 FuzzySample.cs                  */
/*-------------------------------------------------*/
/* Unity 2017 Game AI Programming - Third Edition  */
/*  Ray Barrera, Aung Sithu Kyaw, Thet Naing Swe   */
/*                    Chapter 7                    */
/*   https://www.youtube.com/watch?v=BLqIJampTuM   */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FuzzySample : MonoBehaviour
{
    /* Public Variables */
    public AnimationCurve critical;                 // Curve of critical values
    public AnimationCurve hurt;                     // Curve of hurt values
    public AnimationCurve healthy;                  // Curve of healthy values
    public InputField healthInput;                  // Takes a number between 0 and 100
    public Text labelHealthy;                       // Percentage healthy
    public Text labelHurt;                          // Percentage hurt
    public Text labelCritical;                      // Percentage critical
    public Text fuzzyState;                         // 

    /* Private Variables */
    private const string labelText = "{0} true";    // Percentage true for healthy, hurt and critical
    private float valueHealthy = 0.0f;              // Given value from the evaluation of healthy curve
    private float valueHurt = 0.0f;                 // Given value from the evaluation of hurt curve 
    private float valueCritical = 0.0f;             // Given value from the evaluation of critical curve

    // Use this for initialization
    void Start()
    {
        // Set all values to 0 at start
        SetLabels();
	}

    /* Function to evaluate all the curves and return float values */
    public void EvaluateStatements()
    {
        // Check to see if healthInput is an empty string
        if (string.IsNullOrEmpty(healthInput.text))
        {
            // Return nothing
            return;
        }

        // Local variable to parse healthInput into a float
        float valueInput = float.Parse(healthInput.text);

        // Evaluate the valueInput curve to see if it matches with valueHealthy
        valueHealthy = healthy.Evaluate(valueInput);

        // Evaluate the valueInput curve to see if it matches with valueHurt
        valueHurt = hurt.Evaluate(valueInput);

        // Evaluate the valueInput curve to see if it matches with valueCritical
        valueCritical = critical.Evaluate(valueInput);

        // Function call
        SetLabels();
    }

    /* Function to update the GUI with evaluated values based on input */
    private void SetLabels()
    {
        // Set the healthy label to the healthy value in string format
        labelHealthy.text = string.Format(labelText, valueHealthy);

        // Set the healthy label to the hurt value in string format
        labelHurt.text = string.Format(labelText, valueHurt);

        // Set the healthy label to the critical value in string format
        labelCritical.text = string.Format(labelText, valueCritical);

        // Check to see if valueHealthy is greater than valueHurt and valueCritical
        if (valueHealthy > valueHurt && valueHealthy > valueCritical)
        {
            // Set the text of fuzzyState to say "Healthy"
            fuzzyState.text = "Healthy";
        }
        // Check to see if valueHurt is greater than valueHealthy and valueCritical
        else if (valueHurt > valueHealthy && valueHurt > valueCritical)
        {
            // Set the text of fuzzyState to say "Hurt"
            fuzzyState.text = "Hurt";
        }
        // Check to see if valueCritical is greater than valueHealthy and valueHurt
        else if (valueCritical > valueHealthy && valueCritical > valueHurt)
        {
            // Set the text of fuzzyState to say "Critical"
            fuzzyState.text = "Critical";
        }
        else
        {

        }
    }
}
