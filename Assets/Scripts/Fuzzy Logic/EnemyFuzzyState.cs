﻿/*-------------------------------------------------*/
/*               EnemyFuzzyState.cs                */
/*-------------------------------------------------*/
/* Unity 2017 Game AI Programming - Third Edition  */
/*  Ray Barrera, Aung Sithu Kyaw, Thet Naing Swe   */
/*                    Chapter 7                    */
/*   https://www.youtube.com/watch?v=BLqIJampTuM   */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyFuzzyState : MonoBehaviour
{
    /* Public Variables */
    public AnimationCurve critical;                 // Curve of critical values
    public AnimationCurve hurt;                     // Curve of hurt values
    public AnimationCurve healthy;                  // Curve of healthy values
    public EnemyStats enemyTank;                    // The enemy tank GameObject
    public Text fuzzyState;                         // Percentage healthy
    public float enemyHealth;                       // Current health of the enemy
    public enum FuzzyStates
    {
        HIGH,
        MEDIUM,
        LOW
    }                      // Health states
    public static FuzzyStates currentFuzzyState;    // Current health state

    /* Private Variables */
    private float valueHealthy = 0.0f;              // Given value from the evaluation of healthy curve
    private float valueHurt = 0.0f;                 // Given value from the evaluation of hurt curve 
    private float valueCritical = 0.0f;             // Given value from the evaluation of critical curve

    // Use this for initialization
    void Start()
    {
        // Set all values to 0 at start
        SetLabels();
    }

    // Update is called once per frame
    public void Update()
    {
        // Evaluate health value against curves
        EvaluateStatements();
    }

    /* Function to evaluate all the curves and return float values */
    public void EvaluateStatements()
    {
        // Check to see if healthInput is an empty string
        if (enemyTank.eCurrentHealth <= 0)
        {
            // Return nothing
            return;
        }

        // Local variable to get the health percentage of the boss
        enemyHealth = ((float)enemyTank.eCurrentHealth / enemyTank.eMaximumHealth) * 100;

        // Evaluate the valueInput curve to see if it matches with valueHealthy
        valueHealthy = healthy.Evaluate(enemyHealth);

        // Evaluate the valueInput curve to see if it matches with valueHurt
        valueHurt = hurt.Evaluate(enemyHealth);

        // Evaluate the valueInput curve to see if it matches with valueCritical
        valueCritical = critical.Evaluate(enemyHealth);

        // Function call
        SetLabels();
    }

    /* Function to update the GUI with evaluated values based on input */
    private void SetLabels()
    {
        // Check to see if valueHealthy is greater than valueHurt and valueCritical
        if (valueHealthy > valueHurt && valueHealthy > valueCritical)
        {
            // Set the currentFuzzyState to HIGH
            currentFuzzyState = FuzzyStates.HIGH;

            // Set the text of fuzzyState to say "Healthy"
            fuzzyState.text = "Health: High";
        }
        // Check to see if valueHurt is greater than valueHealthy and valueCritical
        else if (valueHurt > valueHealthy && valueHurt > valueCritical)
        {
            // Set the currentFuzzyState to MEDIUM
            currentFuzzyState = FuzzyStates.MEDIUM;

            // Set the text of fuzzyState to say "Hurt"
            fuzzyState.text = "Health: Medium";
        }
        // Check to see if valueCritical is greater than valueHealthy and valueHurt
        else if (valueCritical > valueHealthy && valueCritical > valueHurt)
        {
            // Set the currentFuzzyState to LOW
            currentFuzzyState = FuzzyStates.LOW;

            // Set the text of fuzzyState to say "Critical"
            fuzzyState.text = "Health: Low";
        }
    }
}
