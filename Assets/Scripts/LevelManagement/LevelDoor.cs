﻿/*-------------------------------------------------*/
/*                   LevelDoor.cs                  */
/*-------------------------------------------------*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDoor : MonoBehaviour
{
    /* Public Variables */
    public GameObject[] door;       // Array holding the sliding door panels
    public SpriteRenderer[] digits; // Array of digits depicting the remaining enemies
    public Sprite[] numbers;
    public float positionCounter;   // The counter for when the doors are opening

    /* Private Variables */
    private LevelClear levelClear;  // The LevelClear in the scene
    private bool doorOpen;          // Only set to true when the required number of enemies are defeated
    private string enemyCount;

    // Use this for initialization
    void Start()
    {
        // Close the door
        SetDoorOpen(false);

        // Find the LevelClear in the scene
        levelClear = FindObjectOfType<LevelClear>();
    }
	
	// Update is called once per frame
	void Update()
    {
        // Update the door
        OpenDoor(doorOpen);

        // 
        enemyCount = FindObjectOfType<EnemyCount>().targetNumber.ToString();

        // Get the first digit of the enemCount
        string digit1 = FindObjectOfType<EnemyCount>().targetNumber < 10 ? "0" : enemyCount.Substring(0, 1);

        // Get the last digit of the enemCount
        string digit2 = enemyCount.Substring(enemyCount.Length - 1, 1);

        digits[0].sprite = numbers[int.Parse(digit1)];
        digits[1].sprite = numbers[int.Parse(digit2)];
    }

    /// <summary>
    /// Sets value of doorOpen
    /// </summary>
    /// <param name="open">The bool that is passed in</param>
    public void SetDoorOpen(bool open)
    {
        // Set doorOpen
        doorOpen = open;
    }

    /// <summary>
    /// Returns the value of doorOpen
    /// </summary>
    public bool GetDoorOpen()
    {
        // Return doorOpen
        return doorOpen;
    }

    /// <summary>
    /// Function to open the door
    /// </summary>
    /// <param name="openDoor">The value that is passed in</param>
    public void OpenDoor(bool openDoor)
    {
        // Check to see if the door is open
        if (openDoor && positionCounter < 0.6f)
        {
            // Animate the door
            for (int i = 0; i < 2; i++)
            {
                // Translate the door
                door[0].transform.Translate(Vector3.left * 2 * Time.deltaTime);
                door[1].transform.Translate(Vector3.right * 2 * Time.deltaTime);
                positionCounter += Time.deltaTime;
            }
        }
    }

    /// <summary>
    /// Function for when the player has entered the door
    /// </summary>
    /// <param name="other">The collider of the player</param>
    public void CompleteLevel(Collider other)
    {
        // Ensure that the intruding object is the player and that the door is open
        if (other.gameObject.tag == "Player" && GetDoorOpen())
        {
            // Level complete
            levelClear.FadeToLevel("LevelSelection");

            Debug.Log("Level Complete");
        }
    }
}
