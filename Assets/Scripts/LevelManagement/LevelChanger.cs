﻿/*-------------------------------------------------*/
/*                 LevelChanger.cs                 */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour
{
    /* Public Variables */
    public Animator animator;                   // The animator component on the gameObject

    /* Private Variables */
    private string levelNameToLoad;             // The name of the level to load
    private int levelIndexToLoad;               // The index of the level to load
    private bool loadByName;                    // Determines how the parameters are passed into the scene loader
	
	// Update is called once per frame
	void Update()
    {
		/*if (Input.GetMouseButtonDown(1))
        {
            FadeToLevel(1);
        }*/
	}

    /// <summary>
    /// Function to fade out and load a level by name
    /// </summary>
    /// <param name="levelName">The name of the level to load</param>
    public void FadeToLevel(string levelName)
    {
        // Assign levelName to levelNameToLoad
        levelNameToLoad = levelName;

        // A level is being loaded by name
        loadByName = true;

        // Start fading out
        animator.SetTrigger("FadeOut");
    }

    /// <summary>
    /// Function to fade out and load a level by index
    /// </summary>
    /// <param name="levelIndex">The index of the level to load</param>
    public void FadeToLevel(int levelIndex)
    {
        // Assign levelIndex to levelIndexToLoad
        levelIndexToLoad = levelIndex;

        // A level is being loaded by index
        loadByName = false;

        // Start fading out
        animator.SetTrigger("FadeOut");
    }

    /// <summary>
    /// Function called in an animation event when the fade out has completed
    /// </summary>
    public void OnFadeComplete()
    {
        // If the level is being loaded by name
        if (loadByName)
        {
            // Load the level by name
            SceneManager.LoadScene(levelNameToLoad);
        }
        else
        {
            // Load the level by index
            SceneManager.LoadScene(levelIndexToLoad);
        }
    }
}
