﻿/*-------------------------------------------------*/
/*                  LevelInfo.cs                   */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelInfo : MonoBehaviour
{
    /* Public Variables */
    [TextArea(1,2)] public string description = "Type the switch values here and they'll be passed elsewhere";
    public int levelCompleteSwitch;                 // The index of the GameSwitch for when this level is complete
    public int levelUnlockedSwitch;                 // The index of the GameSwitch for the next level to be unlocked when this level is complete
    public bool levelCompleted;                     // Determines whether the level has been completed
    public Vector3 mapDimensions;

    /* Internal Variables */
    public static int levelCompleteSwitchInternal;  // The value of levelCompleteSwitch to be passed to other scripts
    public static int levelUnlockedSwitchInternal;  // The value of levelUnlockedSwitch to be passed to other scripts
    public static bool levelCompletedInternal;      // The value of levelCompleted to be passed to other scripts

    public string levelName;                        //-- To be deleted later
    public static string levelNameInternal;         //-- To be deleted later

    // Use this for initialization
    void Start()
    {
        // Set the internal name to the current name
        levelNameInternal = levelName;              //-- To be deleted later

        // Set the internal complete switch to the current complete switch
        levelCompleteSwitchInternal = levelCompleteSwitch;

        // Set the internal unlock switch to the current unlock switch
        levelUnlockedSwitchInternal = levelUnlockedSwitch;
        
        // Set the internal bool to the current bool
        levelCompletedInternal = levelCompleted;
    }
	
	// Update is called once per frame
	void Update()
    {
		
	}

    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(new Vector3(0,0,0), new Vector3(mapDimensions.x, mapDimensions.y, mapDimensions.z));
    }
}
