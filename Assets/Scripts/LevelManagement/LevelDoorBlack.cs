﻿/*-------------------------------------------------*/
/*                LevelDoorBlack.cs                */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelDoorBlack : MonoBehaviour
{
    /// <summary>
    /// Function for when the player enter's the gameObject's proximity
    /// </summary>
    /// <param name="other">The collider of the intruding object</param>
    void OnTriggerEnter(Collider other)
    {
        // Complete the level
        FindObjectOfType<LevelDoor>().CompleteLevel(other);
    }
}
