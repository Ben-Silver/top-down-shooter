﻿/*-------------------------------------------------*/
/*                  PauseMenu.cs                   */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    /* Public Variables */
    public static bool gameIsPaused = false;    // True if the game is paused
    public static bool pausingEnabled = true;   // False when pausing is disabled
    public GameObject pauseMenuUI;              // Reference to the UI in the scene
    public int index = 0;                       // The current index of the selected option
    public AudioClip selectSound;               // The sound to play when scrolling through the options
    public AudioClip confirmSound;              // The sound to play when confirming an option
    public SelectableOption[] options;          // The array of options

    /* Private Variables */
    private LevelChanger levelChanger;          // The LevelChanger in the scene
    private bool upAxesInUse = false;           // True if the up button is held down
    private bool downAxesInUse = false;         // True if the down button is held down
    private bool cancelAxesInUse = false;       // True if the cancel button is held down
    private bool confirmAxesInUse = false;       // True if the confirm button is held down

    // Use this for initialization
    void Start()
    {
        // Find the level changer in the scene
        levelChanger = FindObjectOfType<LevelChanger>();

        // Reset index
        index = 0;

        // Deselect all options
        DeselectAll();

        // Select the option at index
        options[index].selected = true;

        // Make it so pausing is possible
        pausingEnabled = true;
    }
	
	// Update is called once per frame
	void Update()
    {
        // Detect input
        DetectInput();

        // Detect input when paused
        WhenPaused();
    }

    /// <summary>
    /// Function to detect input
    /// </summary>
    public void DetectInput()
    {
        // Check to see if the cancel button is being pressed
        if (Input.GetAxisRaw("Cancel") > 0 && pausingEnabled)
        {
            Debug.Log("You should be fucking paused");

            // Prevent multiple inputs being registered in one button press
            if (!cancelAxesInUse)
            {
                // Check to see if the game is paused
                if (!gameIsPaused)
                {
                    // Mark the axes as used
                    cancelAxesInUse = true;

                    // Pause the game
                    PauseGame();
                }
                else
                {
                    // Mark the axes as used
                    cancelAxesInUse = true;

                    // Resume the game
                    ResumeGame();
                }
            }
        }
        else
        {
            // Mark the axes as unused
            cancelAxesInUse = false;
        }
    }

    /// <summary>
    /// Function for input when the game is paused
    /// </summary>
    public void WhenPaused()
    {
        // When the game is paused
        if (gameIsPaused)
        {
            // Select the option at index
            options[index].selected = true;

            // When the player presses the right arrow key
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetAxisRaw("DPadVertical") > 0 || Input.GetAxisRaw("Vertical") > 0 && index > 0)
            {
                // Prevent scrolling multiple times in one button press
                if (!upAxesInUse)
                {
                    // Mark the axes as used
                    upAxesInUse = true;

                    // Make the select sound
                    GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);

                    // Increment index
                    index -= 1;

                    // Deselect all options
                    DeselectAll();
                }
            }
            else
            {
                // Mark the axes as unused
                upAxesInUse = false;
            }

            // When the player presses the left arrow key
            if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetAxisRaw("DPadVertical") < 0 || Input.GetAxisRaw("Vertical") < 0 && index < options.Length - 1)
            {
                // Prevent scrolling multiple times in one button press
                if (!downAxesInUse)
                {
                    // Mark the axes as used
                    downAxesInUse = true;

                    // Make the select sound
                    GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);

                    // Decrement index
                    index += 1;

                    // Deselect all options
                    DeselectAll();
                }
            }
            else
            {
                // Mark the axes as unused
                downAxesInUse = false;
            }

            // When the confirm key is pressed
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetAxisRaw("Submit") > 0)
            {
                switch (index)
                {
                    case 0:

                        // Prevent confirming multiple times in one button press
                        if (!confirmAxesInUse)
                        {
                            // Mark the axes as used
                            confirmAxesInUse = true;

                            // Make the confirm sound
                            GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);

                            // Resume
                            ResumeGame();
                        }

                    break;

                    case 1:

                        // Prevent confirming multiple times in one button press
                        if (!confirmAxesInUse)
                        {
                            // Mark the axes as used
                            confirmAxesInUse = true;

                            // Make the confirm sound
                            GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);

                            // Resume
                            ResumeGame();

                            // Open up the level select screen
                            levelChanger.FadeToLevel("LevelSelection");
                        }

                    break;
                }
            }
            else
            {
                // Mark the axes as unused
                confirmAxesInUse = false;
            }
        }
    }

    /// <summary>
    /// Function to Pause the game
    /// </summary>
    public void PauseGame()
    {
        // Enable the UI
        pauseMenuUI.SetActive(true);

        // Mark the game as paused
        gameIsPaused = true;

        // Freeze time
        //Time.timeScale = 0.0f;
    }

    /// <summary>
    /// Function to Resume the game
    /// </summary>
    public void ResumeGame()
    {
        // Disable the UI
        pauseMenuUI.SetActive(false);

        // Mark the game as unpaused
        gameIsPaused = false;

        // Freeze time
        //Time.timeScale = 1.0f;
    }

    /// <summary>
    /// Function to deselect all options
    /// </summary>
    public void DeselectAll()
    {
        // Loop through the array
        for (int i = 0; i < options.Length; i++)
        {
            // Deselect all the options in the array
            options[i].selected = false;
        }
    }
}
