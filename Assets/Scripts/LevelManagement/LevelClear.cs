﻿/*-------------------------------------------------*/
/*                  LevelClear.cs                  */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelClear : MonoBehaviour
{
    /* Public Variables */
    public Animator animator;                   // The animator component on the gameObject

    /* Private Variables */
    private FileSystem fileSystem;
    private LevelChanger levelChanger;          // The LevelChanger in the scene
    private string levelNameToLoad;             // The name of the level to load
    private int levelIndexToLoad;               // The index of the level to load
    private bool loadByName;                    // Determines how the parameters are passed into the scene loader

    // Use this for initialization
    void Start()
    {
        // Find the SaveLoad in the scene
        fileSystem = FindObjectOfType<FileSystem>();

        // Find the level changer in the scene
        levelChanger = FindObjectOfType<LevelChanger>();
    }

    /// <summary>
    /// Function to load the level clear transition
    /// </summary>
    /// <param name="levelName">The name of the level to load</param>
    public void FadeToLevel(string levelName)
    {
        // Assign levelName to levelNameToLoad
        levelNameToLoad = levelName;

        // A level is being loaded by name
        loadByName = true;

        // Enable the animator
        animator.enabled = true;

        // Disable pausing
        PauseMenu.pausingEnabled = false;

        // Start fading out
        animator.SetTrigger("ClearedLevel");
    }

    /// <summary>
    /// Function to load the level clear transition
    /// </summary>
    /// <param name="levelIndex">The index of the level to load</param>
    public void FadeToLevel(int levelIndex)
    {
        // Assign levelIndex to levelIndexToLoad
        levelIndexToLoad = levelIndex;

        // A level is being loaded by index
        loadByName = false;

        // Enable the animator
        animator.enabled = true;

        // Start fading out
        animator.SetTrigger("ClearedLevel");
    }

    /// <summary>
    /// Function called in an animation event when the level clear animation has completed
    /// </summary>
    public void OnFadeComplete()
    {
        // Check the level data and set flags accordingly
        CheckLevelData();

        // If the level is being loaded by name
        if (loadByName)
        {
            // Load the level by name
            levelChanger.FadeToLevel(levelNameToLoad);
        }
        else
        {
            // Load the level by index
            levelChanger.FadeToLevel(levelIndexToLoad);
        }
    }

    /// <summary>
    /// Function to set level flags at the end of each level.
    /// ~~Needs to be changed in the future to be more efficient~~
    /// </summary>
    public void CheckLevelData()
    {
        // Set the level as complete
        FileSystem.instance.SetSwitch(LevelInfo.levelCompleteSwitchInternal, true);

        // Unlock the next level
        FileSystem.instance.SetSwitch(LevelInfo.levelUnlockedSwitchInternal, true);

        // Auto save
        fileSystem.SaveData();

        // Check to see if the level has already been completed
        /*switch (LevelInfo.levelNameInternal)
        {
            case "Level 1":
                // Complete the current level
                FileSystem.instance.SetSwitch(48, true);

                // Unlock the next level
                FileSystem.instance.SetSwitch(13, true);

                // Auto save
                fileSystem.SaveData();

            break;

            case "Level 2":
                // Complete the current level
                FileSystem.instance.SetSwitch(49, true);

                // Unlock the next level
                FileSystem.instance.SetSwitch(14, true);

                // Auto save
                fileSystem.SaveData();

            break;

            case "Level 3":
                // Complete the current level
                FileSystem.instance.SetSwitch(50, true);

                // Unlock the next level
                FileSystem.instance.SetSwitch(15, true);

                // Auto save
                fileSystem.SaveData();

            break;

            case "Level 4":
                // Complete the current level
                FileSystem.instance.SetSwitch(51, true);

                // Unlock the next level
                FileSystem.instance.SetSwitch(16, true);

                // Auto save
                fileSystem.SaveData();

            break;

            case "Level 5":
                // Complete the current level
                FileSystem.instance.SetSwitch(52, true);

                // Unlock the next level
                FileSystem.instance.SetSwitch(17, true);

                // Auto save
                fileSystem.SaveData();

            break;

            case "Level 6":
                // Complete the current level
                FileSystem.instance.SetSwitch(53, true);

                // Unlock the next world


                // Unlock the next level


                // Auto save
                fileSystem.SaveData();

            break;
        }*/
    }
}
