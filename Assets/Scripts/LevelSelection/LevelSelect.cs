﻿/*-------------------------------------------------*/
/*                 LevelSelect.cs                  */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelect : MonoBehaviour
{
    /* Public Variables */
    public int index = 0;                       // The current index of the selected option
    public AudioClip selectSound;               // The sound to play when scrolling through the options
    public AudioClip confirmSound;              // The sound to play when confirming an option
    public AudioClip deniedSound;               // The sound to play when a confirmation is denied
    public LevelSelectPanel[] levelSelectPanels;// The array of panels

    /* Private Variables */
    private FileSystem fileSystem;              // The FileSystem in the scene
    private LevelChanger levelChanger;          // The LevelChanger in the scene
    private bool leftAxesInUse = false;         // True when the left axes is being used
    private bool rightAxesInUse = false;        // True when the right axes is being used
    private bool confirmAxesInUse = false;      // True when the confirm button is being pressed

    // Before Start()
    void Awake()
    {
        // Find the FileSystem in the scene
        fileSystem = FindObjectOfType<FileSystem>();

        // Load the data from the FileSystem
        fileSystem.LoadData();
    }

    // Use this for initialization
    void Start()
    {
        // Find the level changer in the scene
        levelChanger = FindObjectOfType<LevelChanger>();

        // Set index to the latest unlocked level
        index = 0; //FileSystem.levelsUnlocked - 1;

        // Deselect all panels
        DeselectAll();

        // Select the first panel
        levelSelectPanels[index].selected = true;

        // Mark the level as unlocked
        for (int i = 0; i < levelSelectPanels.Length; i++)
        {
            levelSelectPanels[i].levelUnlocked = FileSystem.instance.GetSwitch(i + 12);
        }

        // Mark the level as completed
        for (int i = 0; i < levelSelectPanels.Length; i++)
        {
            levelSelectPanels[i].levelCompleted = FileSystem.instance.GetSwitch(i + 48);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Update the selection
        UpdateOptions();

        // Update the confirmation input
        SelectOptions();
    }

    /// <summary>
    /// Function to update the selection
    /// </summary>
    void UpdateOptions()
    {
        // Ensure index does not exceed the upper bounds of the array
        if (index >= levelSelectPanels.Length)
        {
            // Reset index
            index = 0;

            // Deselect all panels
            DeselectAll();

            // Select the current panel
            levelSelectPanels[index].selected = true;
        }

        // Ensure index does not fall below the lower bounds of the array
        if (index < 0)
        {
            // Reset index
            index = levelSelectPanels.Length - 1;

            // Deselect all panels
            DeselectAll();

            // Select the current panel
            levelSelectPanels[index].selected = true;
        }

        // When the player presses the right dpad button
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetAxisRaw("DPadHorizontal") > 0 || Input.GetAxisRaw("Horizontal") > 0)
        {
            // Prevent scrolling multiple times in one button press
            if (!rightAxesInUse)
            {
                if (index < levelSelectPanels.Length - 1)
                {
                    // Mark the axes as used
                    rightAxesInUse = true;

                    // Make the select sound
                    GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);

                    // Increment index
                    index += 1;

                    // Deselect all panels
                    DeselectAll();

                    // Select the current panel
                    levelSelectPanels[index].selected = true;
                }
            }
        }
        else
        {
            // Mark the axes as unused
            rightAxesInUse = false;
        }

        // When the player presses the left dpad button
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetAxisRaw("DPadHorizontal") < 0 || Input.GetAxisRaw("Horizontal") < 0)
        {
            // Prevent scrolling multiple times in one button press
            if (!leftAxesInUse)
            {
                if (index > 0)
                {
                    // Mark the axes as used
                    leftAxesInUse = true;

                    // Make the select sound
                    GetComponent<AudioSource>().PlayOneShot(selectSound, 0.6f);

                    // Increment index
                    index -= 1;

                    // Deselect all panels
                    DeselectAll();

                    // Select the current panel
                    levelSelectPanels[index].selected = true;
                }
            }
        }
        else
        {
            // Mark the axes as unused
            leftAxesInUse = false;
        }
    }

    /// <summary>
    /// Function to update the input
    /// </summary>
    void SelectOptions()
    {
        // If the confirm button is pressed
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetAxisRaw("Submit") > 0)
        {
            // Prevent scrolling multiple times in one button press
            if (!confirmAxesInUse)
            {
                // Make sure the level is unlocked an exists in the game directory
                if (levelSelectPanels[index].levelUnlocked && FileSystem.LevelExists(levelSelectPanels[index].levelName))
                {
                    // Mark the axes as used
                    confirmAxesInUse = true;

                    // Make the confirm sound
                    GetComponent<AudioSource>().PlayOneShot(confirmSound, 0.6f);

                    // Go to the level specified
                    levelChanger.FadeToLevel(levelSelectPanels[index].levelName);

                    // For debug purposes
                    Debug.Log("Loading level: " + levelSelectPanels[index].levelName);
                }
                else if (!levelSelectPanels[index].levelUnlocked && FileSystem.LevelExists(levelSelectPanels[index].levelName))
                {
                    // Mark the axes as used
                    confirmAxesInUse = true;

                    // Make the denied sound
                    GetComponent<AudioSource>().PlayOneShot(deniedSound, 0.6f);

                    // For debug purposes
                    Debug.Log("Level " + levelSelectPanels[index].levelName + " is not unlocked yet");
                }
                else
                {
                    // Mark the axes as used
                    confirmAxesInUse = true;

                    // Make the denied sound
                    GetComponent<AudioSource>().PlayOneShot(deniedSound, 0.6f);

                    // For debug purposes
                    Debug.Log("Level " + levelSelectPanels[index].levelName + " is not implemented yet");
                }
            }
        }
        else
        {
            // Mark the axes as unused
            confirmAxesInUse = false;
        }
    }

    /// <summary>
    /// Function to deselect all panels
    /// </summary>
    void DeselectAll()
    {
        // Loop through the array
        for (int i = 0; i < levelSelectPanels.Length; i++)
        {
            // Deselect all the panels
            levelSelectPanels[i].selected = false;
        }
    }
}
