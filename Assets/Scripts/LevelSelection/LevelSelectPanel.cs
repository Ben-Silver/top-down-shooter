﻿/*-------------------------------------------------*/
/*               LevelSelectPanel.cs               */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectPanel : MonoBehaviour
{
    /* Public Variables */
    public string levelName;                    // The name of the level this panel corresponds to
    public bool levelUnlocked;                  // Determines whether the level can be played
    public bool levelCompleted;                 // Determines whether the level has been completed
    public bool selected;                       // True if the level has been selected in the selection screen
    public Image panelImage;                    // The panel sprite
    public Image levelStatus;                   // The padlock sprite
    public Sprite[] panelSprites;               // Array of sprites

    // Use this for initialization
    void Start()
    {
        // Get the Image component on the GameObject
        panelImage = GetComponent<Image>();

        // Get the Image component on the child of the GameObject
        levelStatus = transform.GetChild(0).GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update()
    {
        // Check to see if this panel is currently being selected
        if (!selected)
        {
            // Check to see if the level has been completed
            if (!levelCompleted)
            {
                // Show the normal sprite if false
                panelImage.sprite = panelSprites[0];
            }
            else
            {
                // Show the completed sprite if true
                panelImage.sprite = panelSprites[1];
            }
        }
        else
        {
            // Check to see if the level has been completed
            if (!levelCompleted)
            {
                // Show the normal sprite if false
                panelImage.sprite = panelSprites[4];
            }
            else
            {
                // Show the completed sprite if true
                panelImage.sprite = panelSprites[5];
            }
        }

        // Check to see if the level has been unlocked
        if (!levelUnlocked)
        {
            // Show the locked sprite if false
            levelStatus.sprite = panelSprites[2];
        }
        else
        {
            // Show the unlocked sprite if true
            levelStatus.sprite = panelSprites[3];
        }
	}
}
