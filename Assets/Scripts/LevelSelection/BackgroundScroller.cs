﻿/*-------------------------------------------------*/
/*              BackgroundScroller.cs              */
/*-------------------------------------------------*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroller : MonoBehaviour
{
    /* Public Variables */
    public float scrollSpeed;                   // The speed at which the background will scroll
    public float tileSizeY;                     // At what point the background image will loop back around

    /* Private Variables */
    public RectTransform rect;                  // The RectTransform of the background image
    private Vector3 startPosition;              // The background's starting position
    
	// Use this for initialization
	void Start()
    {
        // Get the RectTranform component on the image
        rect = GetComponent<RectTransform>();

        // Set the start position to the current position in the scene
        startPosition = rect.position; //transform.position;
	}
	
	// Update is called once per frame
	void Update()
    {
        // Scroll the background on repeat
        float newPosition = Mathf.Repeat(Time.time * scrollSpeed, tileSizeY);

        // Move the background accordingly
        rect.position = startPosition + Vector3.up * newPosition;
	}
}
